<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - PODERÁ SER MODIFICADA FUTURAMENTE
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 * Actives Model
 *
 *
 * @method \App\Model\Entity\Brand get($primaryKey, $options = [])
 * @method \App\Model\Entity\Brand newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Brand[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Brand|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Brand patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Brand[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Brand findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActivesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);        
        $this->setTable('actives');        
        $this->setDisplayField('nome_ativo');        
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->searchManager()
        ->like('id',[
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['id']
        ])
        ->like('nome_ativo', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['nome_ativo']
        ])
        ->like('patrimonio', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['patrimonio']
        ])
        ->like('user_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['responsaveis']
        ])
        ->like('ativo', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['ativo']
        ])
        ->value('client_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['client_id']
        ])
        ->value('location_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['location_id']
        ])
        ->value('brand_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['brand_id']
        ])
        ->value('brand_model_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['brand_model_id']
        ]);
        
        $this->belongsTo('Brands', [
            'foreignKey' => 'brand_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('brandModels', [
            'foreignKey' => 'brand_model_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UserActives', [
            'foreignKey' => 'active_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {   
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome_ativo')
            ->maxLength('nome_ativo', 200)
            ->requirePresence('nome_ativo')
            ->notEmpty('nome_ativo');

        $validator
            ->scalar('patrimonio')
            ->maxLength('patrimonio', 200)
            ->requirePresence('patrimonio')
            ->notEmpty('patrimonio');

        $validator->integer('prop_id')
            ->requirePresence('prop_id', 'create')
            ->notEmpty('prop_id');

        return $validator;
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['brand_id'], 'Brands'));
        $rules->add($rules->existsIn(['brand_model_id'], 'BrandModels'));
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        return $rules;
    }
}
