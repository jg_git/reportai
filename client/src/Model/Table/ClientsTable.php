<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ClientsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('razao_social');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
		->like('cnpjcpf', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['cnpj_cpf']])

                ->like('user_id', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['responsaveis']])

                ->like('ativo', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['ativo']])
                    
                ->like('razao_social', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['razao_social']])

                ->like('logradouro', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['logradouro']])

                ->like('email', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['email']]);
        $this->hasMany('ClientEmails', [
            'foreignKey' => 'client_id',
            'dependent' => true
        ]);
	$this->hasMany('UserClients', [
            'foreignKey' => 'client_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
	$this->hasMany('Actives', [
            'foreignKey' => 'client_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
	$this->hasMany('Locations', [
            'foreignKey' => 'client_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('ClientPhones', [
            'foreignKey' => 'client_id',
            'dependent' => true
        ]);
        $this->hasMany('ClientLogs', [
            'foreignKey' => 'client_id',
            'dependent' => true
        ]);
        $this->hasMany('Deals', [
            'foreignKey' => 'client_id',
            'dependent' => true
        ]);
    }

    public function validationPart1(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('razao_social')
                ->maxLength('razao_social', 100)
                ->requirePresence('razao_social', 'create')
                ->notEmpty('razao_social');

        $validator
                ->scalar('nome_fantasia')
                ->maxLength('nome_fantasia', 100)
                ->requirePresence('nome_fantasia', 'create')
                ->allowEmpty('nome_fantasia');
        
        $validator
                ->scalar('cnpj_cpf')
                ->maxLength('cnpj_cpf', 20)
                ->requirePresence('cnpj_cpf', 'create')
                ->notEmpty('cnpj_cpf');

        $validator
                ->scalar('inscricao_estadual')
                ->maxLength('inscricao_estadual', 15)
                ->allowEmpty('inscricao_estadual');

        $validator
                ->scalar('inscricao_municipal')
                ->maxLength('inscricao_municipal', 15)
                ->allowEmpty('inscricao_municipal');

        $validator
                ->scalar('email')
                ->maxLength('email', 200)
                ->requirePresence('email', 'create')
                ->notEmpty('email'); 

        return $validator;
    }

    public function validationPart2(Validator $validator) {
        $validator
                ->scalar('cep')
                ->maxLength('cep', 10)
                ->requirePresence('cep', 'create')
                ->notEmpty('cep');

        $validator
                ->scalar('cep')
                ->maxLength('cep', 10)
                ->requirePresence('cep', 'create')
                ->notEmpty('cep');

        $validator
                ->scalar('logradouro')
                ->maxLength('logradouro', 200)
                ->requirePresence('logradouro', 'create')
                ->notEmpty('logradouro');

        $validator
                ->scalar('numero')
                ->maxLength('numero', 15)
                ->requirePresence('numero', 'create')
                ->notEmpty('numero');

        $validator
                ->scalar('complemento')
                ->maxLength('complemento', 100)
                ->requirePresence('complemento', 'create')
                ->allowEmpty('complemento');

        $validator
                ->scalar('bairro')
                ->maxLength('bairro', 50)
                ->requirePresence('bairro', 'create')
                ->notEmpty('bairro');

        $validator
                ->scalar('cidade')
                ->maxLength('cidade', 50)
                ->requirePresence('cidade', 'create')
                ->notEmpty('cidade');

        $validator
                ->scalar('estado')
                ->maxLength('estado', 2)
                ->requirePresence('estado', 'create')
                ->notEmpty('estado');

        
        // $validator
        //         ->scalar('latitude')
        //         ->maxLength('latitude', 200)
        //         ->requirePresence('latitude', 'create')
        //         ->notEmpty('latitude');   


        // $validator
        //         ->scalar('longitude')
        //         ->maxLength('longitude', 200)
        //         ->requirePresence('longitude', 'create')
        //         ->notEmpty('longitude');
        $validator
                ->scalar('contato')
                ->maxLength('contato', 100)
                ->requirePresence('contato', 'create')
                ->notEmpty('contato');        

        $validator
                ->scalar('info_add')
                ->maxLength('info_add', 256)
                ->requirePresence('info_add', 'create')
                ->allowEmpty('info_add');

        return $validator;
    }

//     public function buildRules(RulesChecker $rules) {
//         $rules->add($rules->existsIn(['status_id'], 'Status'));

//         return $rules;
//     }

}
