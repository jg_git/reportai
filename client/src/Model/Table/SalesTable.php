<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class SalesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('sales');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
                ->compare('data_venda_de', [
                    'operator' => '>=',
                    'field' => 'data_venda'
                ])
                ->compare('data_venda_ate', [
                    'operator' => '<=',
                    'field' => 'data_venda'
                ])
                ->compare('data_referencia_de', [
                    'operator' => '>=',
                    'field' => 'data_venda'
                ])
                ->compare('data_referencia_ate', [
                    'operator' => '<=',
                    'field' => 'data_venda'
                ])
        ;

        $this->belongsTo('Carriers', [
            'foreignKey' => 'carrier_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Drivers', [
            'foreignKey' => 'driver_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Vehicles', [
            'foreignKey' => 'vehicle_id',
            'joinType' => 'INNER'
        ]);
//        $this->belongsTo('Purchases', [
//            'foreignKey' => 'purchase_id',
//            'joinType' => 'INNER'
//        ]);
        $this->hasMany('SaleDetails', [
            'foreignKey' => 'sale_id',
            'saveStrategy' => 'replace',
            'dependent' => true
        ]);
        $this->belongsToMany('Purchases', [
            'foreignKey' => 'sale_id',
            'targetForeignKey' => 'purchase_id',
            'joinTable' => 'sales_purchases',
            'saveStrategy' => 'replace'
        ]);
        $this->hasMany('SalesPurchases', [
            'foreignKey' => 'sale_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->date('data_venda', 'dmy')
                ->requirePresence('data_venda', 'create')
                ->notEmpty('data_venda');

        $validator
                ->numeric('spread')
//                ->requirePresence('spread', 'create')
                ->allowEmpty('spread');

        $validator
                ->numeric('frete')
//                ->requirePresence('frete', 'create')
                ->allowEmpty('frete');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['carrier_id'], 'Carriers'));
        $rules->add($rules->existsIn(['driver_id'], 'Drivers'));
        $rules->add($rules->existsIn(['vehicle_id'], 'Vehicles'));
        $rules->add($rules->existsIn(['purchase_id'], 'Purchases'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (array_key_exists('spread', $data)) {
            $data['spread'] = str_replace(',', '.', str_replace('.', '', $data['spread']));
        }
        if (array_key_exists('frete', $data)) {
            $data['frete'] = str_replace(',', '.', str_replace('.', '', $data['frete']));
        }
    }

}
