<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Drivers Model
 *
 * @method \App\Model\Entity\Driver get($primaryKey, $options = [])
 * @method \App\Model\Entity\Driver newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Driver[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Driver|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Driver patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Driver[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Driver findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DriversTable extends Table
{

        /**
         * Initialize method
         *
         * @param array $config The configuration for the Table.
         * @return void
         */
        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('drivers');
                $this->setDisplayField('nome');
                $this->setPrimaryKey('id');

                $this->addBehavior('Timestamp');

                $this->addBehavior('Search.Search');
                $this->searchManager()
                        ->like('nome', [
                                'multiValue' => true,
                                'before' => true,
                                'after' => true,
                                'field' => ['nome']
                        ])
                        ->like('cnh', [
                                'multiValue' => true,
                                'before' => true,
                                'after' => true,
                                'field' => ['cnh']
                        ]);

                $this->belongsToMany('Carriers', [
                        'foreignKey' => 'driver_id',
                        'targetForeignKey' => 'carrier_id',
                        'joinTable' => 'carriers_drivers'
                ]);
        }

        /**
         * Default validation rules.
         *
         * @param \Cake\Validation\Validator $validator Validator instance.
         * @return \Cake\Validation\Validator
         */
        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmpty('id', 'create');

                $validator
                        ->scalar('nome')
                        ->maxLength('nome', 100)
                        ->requirePresence('nome', 'create')
                        ->notEmpty('nome');

                $validator
                        ->scalar('cnh')
                        ->maxLength('cnh', 30)
                        ->requirePresence('cnh', 'create')
                        ->notEmpty('cnh');

                $validator
                        ->scalar('cpf')
                        ->maxLength('cpf', 20)
                        ->requirePresence('cpf', 'create')
                        ->allowEmpty('cpf');

                $validator
                        ->scalar('rg')
                        ->maxLength('rg', 20)
                        ->requirePresence('rg', 'create')
                        ->allowEmpty('rg');

                $validator
                        ->scalar('celular1')
                        ->maxLength('celular1', 20)
                        ->requirePresence('celular1', 'create')
                        ->allowEmpty('celular1');

                $validator
                        ->scalar('celular2')
                        ->maxLength('celular2', 20)
                        ->requirePresence('celular2', 'create')
                        ->allowEmpty('celular2');

                $validator
                        ->scalar('celular3')
                        ->maxLength('celular3', 20)
                        ->requirePresence('celular3', 'create')
                        ->allowEmpty('celular3');

                return $validator;
        }
}
