<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class ClientsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
	    $userClients = TableRegistry::get('UserClients');
        $userclientes = $userClients->query();
        $Users = TableRegistry::get('Users');
        $users = $Users->query();
        $users2 = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['cnpj_cpf', 'razao_social', 'logradouro', 'cidade', 'estado', 'email'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        if($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id')){
            $query = $this->Clients->find('search', ['search' => $data])->contain(['Locations', 'Actives', 'UserClients'])->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        }else{
            $query = $this->Clients->find('search', ['search' => $data])->contain(['Locations', 'Actives', 'UserClients'])->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND responsaveis LIKE "%, '.$this->request->session()->read('Auth.User.id').' ,%"');
        }
        $clients = $this->paginate($query);

        $this->set(compact('users', 'users2', 'userclientes', 'clients'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => [
                    'ClientEmails',
                    'ClientPhones',
                    //'ClientLogs' => ['sort' => ['ClientLogs.modified' => 'DESC']],
                    //'ClientLogs.Users'
            ]]);
        } else {
            $client = $this->Clients->newEntity();  
        }
        
        $this->set(compact('client'));
    }

    public function view($id) {
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => [
                    'ClientEmails',
                    'ClientPhones',
                    //'ClientLogs' => ['sort' => ['ClientLogs.modified' => 'DESC']],
                    //'ClientLogs.Users'
            ]]);
        } else {
            $client = $this->Clients->newEntity();
        }
        $this->set(compact('client'));
    }

    public function part1($id) {
        $cliente_id="";
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => []]);
        } else {
            $client = $this->Clients->newEntity();
            $client->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }
            
            $cpf_prealteracoes = $data["cnpj_cpf"];
            $cpf_alteracoes = str_replace([',', '.', '-', '/'], '', $cpf_prealteracoes);
            $data["cnpj_cpf"] = $cpf_alteracoes;
            $client = $this->Clients->patchEntity($client, $data, ['validate' => 'part1']);
            $client->ativo = '1';
            
            $client->prop_id = $this->request->session()->read('Auth.User.prop_id');
            if ($this->Clients->save($client)) { 
            } else {
                $this->Flash->error('Campos obrigatórios não preenchidos');
            }
        }
        
        $this->set(compact('client'));
    }


    public function part2($id) {
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => []]);
        } else {
            $client = $this->Clients->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            $id_user_add= array();
            $id_user_rem= array();

            $checagem = false;
            foreach($data as $key => $dadinho){
                //array_push($id_user_add, $key);
                if ($checagem == true){
                    if($dadinho != 0 or $dadinho != '0'){
                        array_push($id_user_add, $key);
                    }else{
                        array_push($id_user_rem, $key);
                    }
                }
                if($key == "info_add"){
                    $checagem = true;
                }
            }

            $conn = ConnectionManager::get('default');
            $client = $this->Clients->patchEntity($client, $data, ['validate' => 'part2']);
            $client->endereco_full = $client->logradouro." ".$client->cidade." ".$client->estado;
            if ($this->Clients->save($client)) {
                $prop_id=$this->request->session()->read('Auth.User.prop_id');
                $cliente_id = $client->id;
                $userClients = TableRegistry::get('UserClients');
                $query = $userClients->query();
                $insercoes = array();
                $remocoes = array();
                $insert_string = " , ";
                //adicionando registros no user_clients
                foreach($id_user_add as $adicao){
                    $query_search_add = $conn->execute('SELECT * FROM user_clients');
                    $result_sel_add = $query_search_add ->fetchAll('assoc');
                    $contagem = 0;
                    foreach($result_sel_add as $checagem){
                        if($checagem['client_id']==$cliente_id AND $checagem['user_id'] == $adicao){
                            $contagem = $contagem+1;
                        }
                    }
                    $insert_string = $insert_string." , ".$adicao." , ";
                    if ($contagem==0){
                        if(is_numeric($adicao)){
                            $insercao=[
                                'client_id' => $cliente_id,
                                'user_id' => $adicao,
                                'proprietary_id' => $prop_id
                            ];
                            array_push($insercoes, $insercao);
                        }
                    }
                }

                $user_cli_add = TableRegistry::getTableLocator()->get('UserClients');
                $entities = $user_cli_add->newEntities($insercoes);
                $result = $user_cli_add->saveMany($entities);
                
                //removendo registros no user_clients
                foreach($id_user_rem as $removendo){
                    $query_search_rem = $conn->execute('SELECT * FROM user_clients');
                    $result_sel_rem = $query_search_rem ->fetchAll('assoc');

                    foreach($result_sel_rem as $checagem){
                        if($checagem['client_id']==$cliente_id AND $checagem['user_id'] == $removendo){
                            $entity = $userClients->get($checagem['id']);
                            //$result = $userClients->delete($entity);
                            if($userClients->delete($entity)){
                            }
                        }
                    }
                }
                $client->responsaveis = $insert_string;
                $this->Clients->save($client);
            } else {
                $this->Flash->error('Campos obrigatórios não preenchidos'. print_r($data)." -------- ".print_r($client));
                ///
                $remover = $this->Clients->get($id);
                if ($this->Clients->delete($remover)) {

                }
                ///
            }
        }
        $conn2 = ConnectionManager::get('default');
        $user_cli = $conn2->execute('SELECT * FROM user_clients');
        $usercli = $user_cli ->fetchAll('assoc');
	    $validacao = $this->request->session()->read('Auth.User.prop_id');
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$validacao);

        $this->set(compact('client', 'usercli', 'users'));
    }

    public function email($client_id, $id, $anchor) {
        $table = TableRegistry::get('ClientEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->client_id = $client_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
            } else {
                $this->Flash->error('Campos obrigatórios não preenchidos');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($client_id, $id, $anchor) {
        $table = TableRegistry::get('ClientPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->client_id = $client_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
            } else {
                $this->Flash->error('Campos obrigatórios não preenchidos');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    // public function contact($client_id, $id, $anchor) {
    //     $table = TableRegistry::get('ClientContacts');
    //     if ($id != 'new') {
    //         $contact = $table->get($id);
    //     } else {
    //         $contact = $table->newEntity();
    //         $contact->client_id = $client_id;
    //         $contact->tipo = 'D';
    //     }
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $data = $this->request->getData();
    //         $contact = $table->patchEntity($contact, $data);
    //         if ($table->save($contact)) {
    //         } else {
    //             $this->Flash->error('Erro no salvamento do cadastro');
    //         }
    //     }
    //     $this->set(compact('contact', 'anchor'));
    // }

    // public function clog($client_id, $id, $anchor) {
    //     $user = $this->request->session()->read('Auth.User');
    //     $table = TableRegistry::get('ClientLogs');
    //     if ($id != 'new') {
    //         $log = $table->get($id, ['contain' => ['Users']]);
    //     } else {
    //         $log = $table->newEntity();
    //         $log->client_id = $client_id;
    //         $log->user_id = $user['id'];
    //     }
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $log = $table->patchEntity($log, $this->request->getData());
    //         if ($log->getOriginal('file') != $log->file) {
    //             array_map('unlink', glob(getcwd() . '/files/ClientLogs/file/' . '*' . $log->getOriginal('file')));
    //         }
    //         if ($table->save($log)) {
    //             $newid = $log->id;
    //             $log = $table->get($newid, ['contain' => ['Users']]);
    //         } else {
    //             $this->Flash->error('Erro no salvamento do cadastro');
    //         }
    //     }
    //     $this->set(compact('log', 'anchor'));
    // }

    public function emailDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('ClientEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('ClientPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    // public function contactDelete() {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $data = $this->request->getData();
    //     foreach ($data['delete-contact-list'] as $id) {
    //         $table = TableRegistry::get('ClientContacts');
    //         $contact = $table->get($id);
    //         $table->delete($contact);
    //     }
    //     $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    // }

    // public function logDelete() {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $data = $this->request->getData();
    //     foreach ($data['delete-log-list'] as $id) {
    //         $table = TableRegistry::get('ClientLogs');
    //         $log = $table->get($id);
    //         array_map('unlink', glob(getcwd() . '/files/ClientLogs/file/' . '*' . $log->file));
    //         $table->delete($log);
    //     }
    //     $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    // }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if(strpos($id, 'i')){
            $id = str_replace('i','', $id);
            $this->request->allowMethod(['post', 'deactivate']);
            $clientsTable = TableRegistry::getTableLocator()->get('Clients');
            $locationsTable = TableRegistry::getTableLocator()->get('Locations');
            $client = $clientsTable->get($id);

            $response = file_get_contents('http://projetovistoria.com.br/webservices/APIdesativaLocations.php?&api_clientID='.$id);

            $client->ativo='0';
            $clientsTable->save($client);
            $this->Flash->success('Cadastro desativado com sucesso');
        }else if(strpos($id, 'add')){
            $id = str_replace('add','', $id);
            $clientsTable = TableRegistry::getTableLocator()->get('Clients');
            $locationsTable = TableRegistry::getTableLocator()->get('Locations');

            $client = $clientsTable->get($id);

            $response = file_get_contents('http://projetovistoria.com.br/webservices/APIativaLocations.php?&api_clientID='.$id);

            $client->ativo='1';

            $clientsTable->save($client);
            $this->Flash->success('Cadastro ativado com sucesso');
        }else{
            $client = $this->Clients->get($id, ['contain' => ['ClientLogs']]);
            foreach ($client->client_logs as $log) {
                array_map('unlink', glob(getcwd() . '/files/ClientLogs/file/' . '*' . $log->file));
            }
            if ($this->Clients->delete($client)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        }
        
        return $this->redirect($this->referer());
    }

    public function deleteIndex($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        $this->Clients->delete($client);
    }
}
