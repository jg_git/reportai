<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class TransfersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('transfers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->value('conta_origem_id')
            ->value('conta_destino_id')
            ->compare('data_transferencia_de', [
                'operator' => '>=',
                'field' => 'data_transferencia'
            ])
            ->compare('data_transferencia_ate', [
                'operator' => '<=',
                'field' => 'data_transferencia'
            ]);
        $this->belongsTo('SourceAccounts', [
            'className' => 'Accounts',
            'foreignKey' => 'conta_origem_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DestinationAccounts', [
            'className' => 'Accounts',
            'foreignKey' => 'conta_destino_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PaymentMethods', [
            'foreignKey' => 'payment_method_id',
            'joinType' => 'INNER'
        ]);

        // $this->belongsTo('LedgerEntries', [
        //      'foreignKey' => 'id',

        //     'joinType' => 'LEFT'
        //  ]);

        // $this->belongsTo('LedgerEntries.SourceAccounts', [
        //     'className' => 'Accounts',
        //     'foreignKey' => 'conta_origem_id',
        //     'joinType' => 'INNER'
        // ]);

        // $this->belongsToMany('LedgerEntries', [
        //     'foreignKey' => 'conta_origem_id',
        //     'targetForeignKey' => 'ledger_',
        //     'joinTable' => 'accounts_distributor_contacts',
        //     'dependent' => true
        // ]);

        // $this->belongsTo('LedgerEntries', [
        //     'className' => 'Accounts',
        //     'foreignKey' => 'conta_origem_id',
        //     'joinType' => 'INNER'
        // ]);

        // $this->belongsTo('Accounts', [
        //     'foreignKey' => 'conta_destino_id',
        //     'joinType' => 'LEFT'
        // ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('data_transferencia', 'dmy')
            ->requirePresence('data_transferencia', 'create')
            ->notEmpty('data_transferencia');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 100)
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->scalar('observacao')
            ->allowEmpty('observacao');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['conta_origem_id'], 'SourceAccounts'));
        $rules->add($rules->existsIn(['conta_destino_id'], 'DestinationAccounts'));
        $rules->add($rules->existsIn(['payment_method_id'], 'PaymentMethods'));

        return $rules;
    }
}
