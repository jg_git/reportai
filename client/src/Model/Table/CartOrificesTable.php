<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class CartOrificesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('cart_orifices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Carts', [
            'foreignKey' => 'cart_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->integer('numero')
                ->requirePresence('numero', 'create')
                ->notEmpty('numero');

        $validator
                ->integer('quantidade_litros')
                ->requirePresence('quantidade_litros', 'create')
                ->notEmpty('quantidade_litros');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['cart_id'], 'Carts'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        $data['quantidade_litros'] = str_replace(',', '.', str_replace('.', '', $data['quantidade_litros']));
    }

}
