<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - PODERÁ SER MODIFICADA FUTURAMENTE
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 * Locations Model
 *
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 *
 * @method \App\Model\Entity\Location get($primaryKey, $options = [])
 * @method \App\Model\Entity\Location newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Location[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Location|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Location patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Location[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Location findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LocationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);        
        $this->setTable('locations');        
        $this->setDisplayField('nome_local');        
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->searchManager()
        ->value('ativo', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['ativo']
        ])
        ->like('user_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['responsaveis']
        ])
        ->value('client_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['client_id']
        ])
        ->like('nome_local', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['nome_local']
        ]);
        
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UserLocations', [
            'foreignKey' => 'location_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Actives', [
            'foreignKey' => 'location_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        $this->hasMany('Polls', [
            'foreignKey' => 'location_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {        
        $validator->integer('id')
            ->allowEmpty('id', 'create');  

        $validator
            ->scalar('nome_local')
            ->maxLength('nome_local', 150)
            ->requirePresence('nome_local', 'create')
            ->notEmpty('nome_local');
        
        $validator
            ->scalar('cep')
            ->maxLength('cep', 10)
            ->requirePresence('cep', 'create')
            ->notEmpty('cep');

        $validator
            ->scalar('logradouro')
            ->maxLength('logradouro', 200)
             ->requirePresence('logradouro', 'create')
            ->notEmpty('logradouro');

        $validator
            ->scalar('numero')
            ->maxLength('numero', 15)
            ->requirePresence('numero', 'create')
            ->notEmpty('numero');

        $validator
            ->scalar('complemento')
            ->maxLength('complemento', 100)
            ->requirePresence('complemento', 'create')
            ->allowEmpty('complemento');

        $validator
            ->scalar('bairro')
            ->maxLength('bairro', 50)
            ->requirePresence('bairro', 'create')
            ->notEmpty('bairro');

        $validator
            ->scalar('cidade')
            ->maxLength('cidade', 50)
            ->requirePresence('cidade', 'create')
            ->notEmpty('cidade');

        $validator
            ->scalar('estado')
            ->maxLength('estado', 2)
            ->requirePresence('estado', 'create')
            ->notEmpty('estado');

        
        // $validator
        //     ->scalar('latitude')
        //     ->maxLength('latitude', 200)
        //     ->requirePresence('latitude', 'create')
        //     ->notEmpty('latitude');   


        // $validator
        //     ->scalar('longitude')
        //     ->maxLength('longitude', 200)
        //     ->requirePresence('longitude', 'create')
        //     ->notEmpty('longitude');

        $validator
            ->scalar('nome_contato')
            ->maxLength('nome_contato', 150)
            ->requirePresence('nome_contato', 'create')
            ->notEmpty('nome_contato');
  
        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        return $rules;
    }
}
