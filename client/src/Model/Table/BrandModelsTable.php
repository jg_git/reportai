<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - PODERÁ SER MODIFICADA FUTURAMENTE
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 * Deals Model
 *
 *
 *
 * @method \App\Model\Entity\Deal get($primaryKey, $options = [])
 * @method \App\Model\Entity\Deal newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Deal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Deal|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Deal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Deal[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Deal findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BrandModelsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);        
        $this->setTable('brand_models');        
        $this->setDisplayField('nome_modelo');        
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->searchManager();  
        $this->belongsTo('Brands', [
            'foreignKey' => 'brand_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Actives', [
            'foreignKey' => 'brand_model_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {   
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome_modelo')
            ->maxLength('nome_modelo', 200)
            ->requirePresence('nome_modelo')
            ->notEmpty('nome_modelo');;

        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['brand_id'], 'Brands'));
        return $rules;
    }
}
