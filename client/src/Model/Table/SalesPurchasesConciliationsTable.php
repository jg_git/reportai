<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class SalesPurchasesConciliationsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('sales_purchases_conciliations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('SalesPurchases', [
            'foreignKey' => 'sales_purchases_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('nota_fiscal')
                ->maxLength('nota_fiscal', 50)
                ->allowEmpty('nota_fiscal');

        $validator
                ->integer('volume_conciliado')
                ->requirePresence('volume_conciliado', 'create')
                ->notEmpty('volume_conciliado');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['sales_purchases_id'], 'SalesPurchases'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (array_key_exists('volume_conciliado', $data)) {
            $data['volume_conciliado'] = str_replace(',', '.', str_replace('.', '', $data['volume_conciliado']));
        }
    }

}
