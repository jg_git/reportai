<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class SaleDetailsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('sale_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
        ]);
        $this->belongsTo('Presales', [
            'foreignKey' => 'presale_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasMany('Receivables', [
            'foreignKey' => 'sale_detail_id',
            'joinType' => 'LEFT'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nota_fiscal')
            ->maxLength('nota_fiscal', 50)
            ->requirePresence('nota_fiscal', 'create')
            ->allowEmpty('nota_fiscal');

        $validator
            ->integer('volume')
            ->requirePresence('volume', 'create')
            ->notEmpty('volume');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sale_id'], 'Sales'));
        // $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['presale_id'], 'Presales'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        $data['volume'] = str_replace(',', '.', str_replace('.', '', $data['volume']));
    }
}
