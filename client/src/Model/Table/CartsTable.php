<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;
use ArrayObject;
use Cake\Event\Event;

class CartsTable extends Table
{

        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('carts');
                $this->setDisplayField('placa');
                $this->setPrimaryKey('id');

                $this->addBehavior('Timestamp');

                $this->addBehavior('Search.Search');
                $this->searchManager()
                        ->like('marca_modelo', [
                                'multiValue' => true,
                                'before' => true,
                                'after' => true,
                                'field' => ['marca_modelo']
                        ]);

                $this->hasMany('CartOrifices', [
                        'foreignKey' => 'cart_id'
                ]);
        }

        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmpty('id', 'create');

                $validator
                        ->scalar('marca_modelo')
                        ->maxLength('marca_modelo', 50)
                        ->requirePresence('marca_modelo', 'create')
                        ->notEmpty('marca_modelo');

                $validator
                        ->scalar('placa')
                        ->maxLength('placa', 10)
                        ->requirePresence('placa', 'create')
                        ->allowEmpty('placa');


                $validator
                        ->scalar('ano')
                        ->maxLength('ano', 10)
                        ->requirePresence('ano', 'create')
                        ->allowEmpty('ano');

                $validator
                        ->integer('quantidade_litros')
                        ->requirePresence('quantidade_litros', 'create')
                        ->notEmpty('quantidade_litros');


                return $validator;
        }

        public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
        {
                $data['quantidade_litros'] = str_replace(',', '.', str_replace('.', '', $data['quantidade_litros']));
        }
}
