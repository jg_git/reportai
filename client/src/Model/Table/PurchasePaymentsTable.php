<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchasePaymentsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('purchase_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Purchases', [
            'foreignKey' => 'purchase_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasOne('Payables', [
            'foreignKey' => 'purchase_payment_id'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->numeric('volume_provisionado')
                ->requirePresence('volume_provisionado', 'create')
                ->notEmpty('volume_provisionado');

        $validator
                ->date('data_vencimento', 'dmy')
                ->requirePresence('data_vencimento', 'create')
                ->notEmpty('data_vencimento');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['purchase_id'], 'Purchases'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
//        $data['valor_provisionado'] = str_replace(',', '.', str_replace('.', '', $data['valor_provisionado']));
    }

}
