<?php

namespace App\Model\Table;

use ArrayObject;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchasesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('purchases');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
                ->value('numero_contrato')
                ->value('distributor_id')
                ->value('plant_id')
                ->compare('data_compra_de', [
                    'operator' => '>=',
                    'field' => 'data_compra'
                ])
                ->compare('data_compra_ate', [
                    'operator' => '<=',
                    'field' => 'data_compra'
                ])
                ->compare('data_referencia_de', [
                    'operator' => '>=',
                    'field' => 'data_compra'
                ])
                ->compare('data_referencia_ate', [
                    'operator' => '<=',
                    'field' => 'data_compra'
                ])
        ;

        $this->belongsTo('Plants', [
            'foreignKey' => 'plant_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('TransitPlants', [
            'className' => 'Plants',
            'foreignKey' => 'transito_carrier_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Distributors', [
            'foreignKey' => 'distributor_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Brokers', [
            'foreignKey' => 'broker_id'
        ]);
        $this->hasMany('PurchasePayments', [
            'foreignKey' => 'purchase_id',
            'saveStrategy' => 'replace',
            'dependent' => true
        ]);
        $this->hasMany('Sales', [
            'foreignKey' => 'purchase_id',
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('numero_contrato')
                ->maxLength('numero_contrato', 20)
//                ->requirePresence('numero_contrato', 'create')
                ->notEmpty('numero_contrato');

        $validator
                ->date('data_compra', 'dmy')
//                ->requirePresence('data_compra', 'create')
                ->notEmpty('data_compra');

        $validator
                ->integer('volume_comprado')
//                ->requirePresence('volume_comprado', 'create')
                ->notEmpty('volume_comprado');

        $validator
                ->scalar('preco_litro')
//                ->requirePresence('preco_litro', 'create')
                ->notEmpty('preco_litro');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['plant_id'], 'Plants'));
        $rules->add($rules->existsIn(['distributor_id'], 'Distributors'));
        $rules->add($rules->existsIn(['broker_id'], 'Brokers'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (array_key_exists('purchase_payments', $data)) {
            foreach ($data['purchase_payments'] as $index => $payment) {
                $payment['volume_provisionado'] = str_replace(',', '.', str_replace('.', '', $payment['volume_provisionado']));
                $data['purchase_payments'][$index] = $payment;
            }
        }
        if (array_key_exists('volume_comprado', $data)) {
            $data['volume_comprado'] = str_replace(',', '.', str_replace('.', '', $data['volume_comprado']));
        }
        if (array_key_exists('preco_litro', $data)) {
            $data['preco_litro'] = str_replace(',', '.', str_replace('.', '', $data['preco_litro']));
        }
    }

}
