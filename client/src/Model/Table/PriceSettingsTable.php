<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PriceSettings Model
 *
 * @method \App\Model\Entity\PriceSetting get($primaryKey, $options = [])
 * @method \App\Model\Entity\PriceSetting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PriceSetting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PriceSetting|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PriceSetting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PriceSetting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PriceSetting findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PriceSettingsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('price_settings');
        $this->setDisplayField('tag');
        $this->setPrimaryKey('tag');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('tag')
            ->maxLength('tag', 50)
            ->allowEmpty('tag', 'create');

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->integer('dias_de')
            ->allowEmpty('dias_de');

        $validator
            ->integer('dias_ate')
            ->allowEmpty('dias_ate');

        return $validator;
    }
}
