<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AccountsDistributorContacts Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 * @property \App\Model\Table\DistributorContactsTable|\Cake\ORM\Association\BelongsTo $DistributorContacts
 *
 * @method \App\Model\Entity\AccountsDistributorContact get($primaryKey, $options = [])
 * @method \App\Model\Entity\AccountsDistributorContact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AccountsDistributorContact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AccountsDistributorContact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AccountsDistributorContact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AccountsDistributorContact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AccountsDistributorContact findOrCreate($search, callable $callback = null, $options = [])
 */
class AccountsDistributorContactsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('accounts_distributor_contacts');
        $this->setDisplayField('account_id');
        $this->setPrimaryKey(['account_id', 'distributor_contact_id']);

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('DistributorContacts', [
            'foreignKey' => 'distributor_contact_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['distributor_contact_id'], 'DistributorContacts'));

        return $rules;
    }

}
