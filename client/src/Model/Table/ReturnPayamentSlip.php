<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class PayamentSlipTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('return_payament_slips');
        $this->setDisplayField('payament_id');
        //        $this->setPrimaryKey(['sale_id', 'purchase_id']);
        $this->setPrimaryKey(['id']);

        $this->addBehavior('Timestamp');
    }

    // public function validationDefault(Validator $validator)
    // {
    //     $validator
    //         ->integer('volume')
    //         //                ->requirePresence('volume', 'create')
    //         ->notEmpty('volume');

    //     return $validator;
    // }

    // public function buildRules(RulesChecker $rules)
    // {
    //     $rules->add($rules->existsIn(['sale_id'], 'Sales'));
    //     $rules->add($rules->existsIn(['purchase_id'], 'Purchases'));

    //     return $rules;
    // }

    // public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    // {
    //     if (array_key_exists('volume', $data)) {
    //         $data['volume'] = str_replace(',', '.', str_replace('.', '', $data['volume']));
    //     }
    // }
}
