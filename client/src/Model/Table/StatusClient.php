<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class StatusClient extends Entity {

    protected $_accessible = [
        'id'=> true,
        'descricao' => true,        
    ];

}
