<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BrokerBankAccounts Model
 *
 * @property \App\Model\Table\BrokersTable|\Cake\ORM\Association\BelongsTo $Brokers
 * @property \App\Model\Table\BanksTable|\Cake\ORM\Association\BelongsTo $Banks
 *
 * @method \App\Model\Entity\BrokerBankAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\BrokerBankAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BrokerBankAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BrokerBankAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BrokerBankAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BrokerBankAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BrokerBankAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BrokerBankAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('broker_bank_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Brokers', [
            'foreignKey' => 'broker_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('numero_agencia')
            ->maxLength('numero_agencia', 50)
            ->requirePresence('numero_agencia', 'create')
            ->notEmpty('numero_agencia');

        $validator
            ->scalar('numero_conta')
            ->maxLength('numero_conta', 10)
            ->requirePresence('numero_conta', 'create')
            ->notEmpty('numero_conta');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['broker_id'], 'Brokers'));
        $rules->add($rules->existsIn(['bank_id'], 'Banks'));

        return $rules;
    }
}
