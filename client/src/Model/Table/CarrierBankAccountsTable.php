<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CarrierBankAccounts Model
 *
 * @property \App\Model\Table\CarriersTable|\Cake\ORM\Association\BelongsTo $Carriers
 * @property \App\Model\Table\BanksTable|\Cake\ORM\Association\BelongsTo $Banks
 *
 * @method \App\Model\Entity\CarrierBankAccount get($primaryKey, $options = [])
 * @method \App\Model\Entity\CarrierBankAccount newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CarrierBankAccount[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CarrierBankAccount|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CarrierBankAccount patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CarrierBankAccount[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CarrierBankAccount findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CarrierBankAccountsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('carrier_bank_accounts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Carriers', [
            'foreignKey' => 'carrier_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('numero_agencia')
            ->maxLength('numero_agencia', 10)
            ->requirePresence('numero_agencia', 'create')
            ->notEmpty('numero_agencia');

        $validator
            ->scalar('numero_conta')
            ->maxLength('numero_conta', 20)
            ->requirePresence('numero_conta', 'create')
            ->notEmpty('numero_conta');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['carrier_id'], 'Carriers'));
        $rules->add($rules->existsIn(['bank_id'], 'Banks'));

        return $rules;
    }
}
