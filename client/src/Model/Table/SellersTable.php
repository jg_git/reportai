<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class SellersTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('sellers');
        $this->setDisplayField('razao_social');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
                ->like('razao_social', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['razao_social']])
                ->like('cnpj_cpf', [
                    'multiValue' => false,
                    'before' => true,
                    'after' => true,
                    'field' => ['cnpj_cpf']]);
        $this->hasMany('SellerEmails', [
            'foreignKey' => 'seller_id',
            'dependent' => true
        ]);
        $this->hasMany('SellerPhones', [
            'foreignKey' => 'seller_id',
            'dependent' => true
        ]);
        $this->hasMany('SellerBankAccounts', [
            'foreignKey' => 'seller_id',
            'dependent' => true
        ]);
    }

    public function validationPart1(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('razao_social')
                ->maxLength('razao_social', 100)
                ->requirePresence('razao_social', 'create')
                ->notEmpty('razao_social');

        $validator
                ->scalar('nome_fantasia')
                ->maxLength('nome_fantasia', 100)
                ->requirePresence('nome_fantasia', 'create')
                ->allowEmpty('nome_fantasia');

        $validator
                ->scalar('cnpj_cpf')
                ->maxLength('cnpj_cpf', 20)
                ->requirePresence('cnpj_cpf', 'create')
                ->notEmpty('cnpj_cpf');

        $validator
                ->scalar('inscricao_estadual')
                ->maxLength('inscricao_estadual', 15)
                ->allowEmpty('inscricao_estadual');

        $validator
                ->scalar('inscricao_municipal')
                ->maxLength('inscricao_municipal', 15)
                ->allowEmpty('inscricao_municipal');

        return $validator;
    }

    public function validationPart2(Validator $validator) {
        $validator
                ->scalar('cep')
                ->maxLength('cep', 10)
                ->requirePresence('cep', 'create')
                ->notEmpty('cep');

        $validator
                ->scalar('logradouro')
                ->maxLength('logradouro', 200)
                ->requirePresence('logradouro', 'create')
                ->notEmpty('logradouro');

        $validator
                ->scalar('numero')
                ->maxLength('numero', 15)
                ->requirePresence('numero', 'create')
                ->notEmpty('numero');

        $validator
                ->scalar('complemento')
                ->maxLength('complemento', 100)
                ->requirePresence('complemento', 'create')
                ->allowEmpty('complemento');

        $validator
                ->scalar('bairro')
                ->maxLength('bairro', 50)
                ->requirePresence('bairro', 'create')
                ->notEmpty('bairro');

        $validator
                ->scalar('cidade')
                ->maxLength('cidade', 50)
                ->requirePresence('cidade', 'create')
                ->notEmpty('cidade');

        $validator
                ->scalar('estado')
                ->maxLength('estado', 2)
                ->requirePresence('estado', 'create')
                ->notEmpty('estado');

        $validator
                ->scalar('agencia')
                ->maxLength('agencia', 100)
                ->allowEmpty('agencia');

        $validator
                ->scalar('conta')
                ->maxLength('conta', 10)
                ->allowEmpty('conta');

        $validator
                ->scalar('tipo_conta')
                ->maxLength('tipo_conta', 1)
                ->allowEmpty('tipo_conta');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {

        return $rules;
    }

}
