<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class VehiclesTable extends Table
{

        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('vehicles');
                $this->setDisplayField('id');
                $this->setPrimaryKey('id');

                $this->addBehavior('Timestamp');

                $this->addBehavior('Search.Search');
                $this->searchManager()
                        ->like('marca_modelo', [
                                'multiValue' => true,
                                'before' => true,
                                'after' => true,
                                'field' => ['marca_modelo']
                        ])
                        ->like('placa', [
                                'multiValue' => false,
                                'before' => true,
                                'after' => true,
                                'field' => ['placa']
                        ]);

                $this->belongsToMany('Carriers', [
                        'foreignKey' => 'vehicle_id',
                        'targetForeignKey' => 'carrier_id',
                        'joinTable' => 'carriers_vehicles'
                ]);

                $this->hasMany('VehicleOrifices', [
                        'foreignKey' => 'vehicle_id',
                        'dependent' => true
                ]);

                $this->hasMany('Carts', [
                        'foreignKey' => 'vehicle_id',
                ]);
        }

        public function validationDefault(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmpty('id', 'create');

                $validator
                        ->scalar('tipo')
                        ->maxLength('tipo', 1)
                        ->requirePresence('tipo', 'create')
                        ->notEmpty('tipo');

                $validator
                        ->scalar('marca_modelo')
                        ->maxLength('marca_modelo', 100)
                        ->requirePresence('marca_modelo', 'create')
                        ->notEmpty('marca_modelo');

                $validator
                        ->scalar('ano')
                        ->maxLength('ano', 10)
                        ->requirePresence('ano', 'create')
                        ->notEmpty('ano');

                $validator
                        ->scalar('placa')
                        ->maxLength('placa', 10)
                        ->requirePresence('placa', 'create')
                        ->notEmpty('placa');

                $validator
                        ->date('cronotacografo', 'dmy')
                        ->requirePresence('cronotacografo', 'create')
                        ->allowEmpty('cronotacografo');






                $validator
                        ->scalar('marca_modelo_tanque')
                        ->maxLength('marca_modelo_tanque', 200)
                        ->requirePresence('marca_modelo_tanque', 'create')
                        ->allowEmpty('marca_modelo_tanque');

                $validator
                        ->scalar('ano_fabricacao_tanque')
                        ->maxLength('ano_fabricacao_tanque', 20)
                        ->requirePresence('ano_fabricacao_tanque', 'create')
                        ->allowEmpty('ano_fabricacao_tanque');




                $validator
                        ->integer('quantidade_litros')
                        ->requirePresence('quantidade_litros', 'create')
                        ->allowEmpty('quantidade_litros');

                return $validator;
        }

        public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
        {
                $data['quantidade_litros'] = str_replace(',', '.', str_replace('.', '', $data['quantidade_litros']));

                if (array_key_exists('ipva1', $data)) {
                        $data['ipva1'] = str_replace(',', '.', str_replace('.', '', $data['ipva1']));
                }
                if (array_key_exists('ipva2', $data)) {
                        $data['ipva2'] = str_replace(',', '.', str_replace('.', '', $data['ipva2']));
                }
                if (array_key_exists('ipva3', $data)) {
                        $data['ipva3'] = str_replace(',', '.', str_replace('.', '', $data['ipva3']));
                }
        }
}
