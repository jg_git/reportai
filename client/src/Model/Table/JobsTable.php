<?php
//PÁGINA ADICIONADA POR Guilherme Domingues - PODERÁ SER MODIFICADA FUTURAMENTE
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class JobsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);        
        $this->setTable('jobs');   
        $this->setDisplayField('nome_servico');        
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->searchManager()
        ->like('id',[
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['id']
        ])
        ->like('nome_servico', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['nome_servico']
        ])
        ->like('responsaveis', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['responsaveis']
        ])
        ->like('formulario_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['formulario_id']
        ])
        ->like('local_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['local_id']
        ])
        ->value('instrucoes', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['instrucoes']
        ])
        ->value('inicio', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['inicio']
        ])
        ->value('final', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['final']
        ])
        ->value('status', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['status']
        ])
        ->value('client_id', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['client_id']
        ]);
        $this->belongsTo('Locations', [
            'foreignKey' => 'local_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Polls', [
            'foreignKey' => 'formulario_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('UserJobs', [
            'foreignKey' => 'job_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {   
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nome_servico')
            ->maxLength('nome_servico', 200)
            ->requirePresence('nome_servico')
            ->notEmpty('nome_servico');

        $validator
            ->scalar('instrucoes')
            ->maxLength('instrucoes', 500)
            ->requirePresence('instrucoes')
            ->notEmpty('instrucoes');

        $validator->integer('prop_id')
            ->requirePresence('prop_id', 'create')
            ->notEmpty('prop_id');

        return $validator;
    }
    
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */

    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['formulario_id'], 'Polls'));
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        return $rules;
    }
}
