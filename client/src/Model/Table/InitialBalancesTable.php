<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

/**
 * InitialBalances Model
 *
 * @property \App\Model\Table\AccountsTable|\Cake\ORM\Association\BelongsTo $Accounts
 *
 * @method \App\Model\Entity\InitialBalance get($primaryKey, $options = [])
 * @method \App\Model\Entity\InitialBalance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\InitialBalance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\InitialBalance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\InitialBalance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\InitialBalance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\InitialBalance findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class InitialBalancesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('initial_balances');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        
        $this->addBehavior('Search.Search');
        $this->searchManager()
                ->value('account_id');

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('dc')
                ->maxLength('dc', 1)
                ->requirePresence('dc', 'create')
                ->notEmpty('dc');

        $validator
                ->numeric('valor')
                ->requirePresence('valor', 'create')
                ->notEmpty('valor');

        $validator
                ->date('data_saldo', 'dmy')
                ->requirePresence('data_saldo', 'create')
                ->notEmpty('data_saldo');

        $validator
                ->scalar('observacao')
                ->allowEmpty('observacao');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));

        return $rules;
    }
    
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        $data['valor'] = str_replace(',', '.', str_replace('.', '', $data['valor']));
    }
    
}
