<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ReceivablesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('receivables');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->value('baixa')
            ->value('tipo')
            ->compare('data_vencimento_de', [
                'operator' => '>=',
                'field' => 'data_vencimento'
            ])
            ->compare('data_vencimento_ate', [
                'operator' => '<=',
                'field' => 'data_vencimento'
            ])
            ->compare('data_baixa_de', [
                'operator' => '>=',
                'field' => 'data_baixa'
            ])
            ->compare('data_baixa_ate', [
                'operator' => '<=',
                'field' => 'data_baixa'
            ])
            ->add('account_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('SaleDetails', function ($q) use ($args) {
                        return $q->where(['SaleDetails.account_id' => $args['account_id']]);
                    });
                }
            ]);

        $this->belongsTo('SaleDetails', [
            'foreignKey' => 'sale_detail_id'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 2)
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->date('data_vencimento')
            ->allowEmpty('data_vencimento');

        $validator
            ->date('data_baixa')
            ->allowEmpty('data_baixa');

        $validator
            ->requirePresence('baixa', 'create')
            ->notEmpty('baixa');

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 200)
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['sale_detail_id'], 'SaleDetails'));

        return $rules;
    }
}
