<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj_cpf
 * @property string $ie
 * @property string $email
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $latitude
 * @property string $longitude
 * @property string $contato
 * @property string $email_adm
 * @property int $status_id
 * @property string $url_dashboard
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\SituationClients[] $situation_clients
 * @property \App\Model\Entity\ClientEmail[] $client_emails
 * @property \App\Model\Entity\ClientPhone[] $client_phones
 */
class Client extends Entity {

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tipo' => true,
        'razao_social' => true,
        'nome_fantasia' => true,
        'cnpj_cpf' => true,
        'inscricao_estadual' => true,
        'inscricao_municipal' => true,
        'email' => true,
        'cep' => true,
        'logradouro' => true,
        'numero' => true,
        'complemento' => true,
        'bairro' => true,
        'cidade' => true,
        'estado' => true,
        'latitude' => true,
        'longitude' => true,
        'contato' => true,
        'created' => true,
        'modified' => true,
        'responsaveis' => true,
        'status_id' => true,
        'email_adm' => true,
        'url_chamados' => true,
        'client_emails' => true,
        'client_phones' => true,
        'situation_clients'=>true
    ];

}
