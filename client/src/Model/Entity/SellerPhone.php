<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SellerPhone Entity
 *
 * @property int $id
 * @property int $seller_id
 * @property string $ddd
 * @property string $phone
 * @property string $tipo
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Seller[] $sellers
 */
class SellerPhone extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'seller_id' => true,
        'ddd' => true,
        'telefone' => true,
        'tipo' => true,
        'created' => true,
        'sellers' => true
    ];
}
