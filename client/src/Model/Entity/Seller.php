<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Seller Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj_cpf
 * @property string $ie
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $banco
 * @property string $agencia
 * @property string $conta
 * @property string $tipo_conta
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\SellerEmail $seller_email
 * @property \App\Model\Entity\SellerPhone $seller_phone
 */
class Seller extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'tipo' => true,
        'razao_social' => true,
        'nome_fantasia' => true,
        'cnpj_cpf' => true,
        'inscricao_estadual' => true,
        'inscricao_municipal' => true,
        'cep' => true,
        'logradouro' => true,
        'numero' => true,
        'complemento' => true,
        'bairro' => true,
        'cidade' => true,
        'estado' => true,
        'bank_id' => true,
        'agencia' => true,
        'conta' => true,
        'tipo_conta' => true,
        'created' => true,
        'modified' => true,
        'seller_email' => true,
        'seller_phone' => true
    ];
}
