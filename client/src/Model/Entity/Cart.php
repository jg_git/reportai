<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class Cart extends Entity {

    protected $_accessible = [
        'marca_modelo' => true,
        'placa' => true,
        'ano' => true,
        'licenciamento' => true,
        'quantidade_litros' => true,
        'quantidade_bocas' => true,
        'civ' => true,
        'cipp' => true,
        'calibragem' => true,
        'letpp' => true,
        'seguro_seguradora' => true,
        'seguro_corretora' => true,
        'seguro_apolice' => true,
        'seguro_vencimento' => true,
        'created' => true,
        'modified' => true,
        'cart_orifices' => true
    ];

}
