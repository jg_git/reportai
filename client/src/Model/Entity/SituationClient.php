<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class SituationClient extends Entity {

    protected $_accessible = [
        'id'=> true,
        'nome' => true,
        'clients' => true        
    ];

}
