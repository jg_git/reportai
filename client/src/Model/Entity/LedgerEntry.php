<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * LedgerEntry Entity
 *
 * @property int $id
 * @property int $account_id
 * @property int $ledger_entry_type_id
 * @property \Cake\I18n\Date $data_lancamento
 * @property string $descricao
 * @property string $numero_documento
 * @property int $payment_method_id
 * @property float $valor
 * @property string $observacao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\LedgerEntryType $ledger_entry_type
 * @property \App\Model\Entity\PaymentMethod $payment_method
 */
class LedgerEntry extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'ledger_entry_type_id' => true,
        'data_lancamento' => true,
        'descricao' => true,
        'numero_documento' => true,
        'payment_method_id' => true,
        'valor' => true,
        'observacao' => true,
        'created' => true,
        'modified' => true,
        'account' => true,
        'ledger_entry_type' => true,
        'payment_method' => true
    ];
}
