<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DistributorEmail Entity
 *
 * @property int $id
 * @property int $distributor_id
 * @property string $email
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Distributor $distributor
 */
class DistributorEmail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'distributor_id' => true,
        'email' => true,
        'created' => true,
        'distributor' => true
    ];
}
