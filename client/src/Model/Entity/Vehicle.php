<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Vehicle Entity
 *
 * @property int $id
 * @property int $carrier_id
 * @property int $driver_id
 * @property string $nome
 * @property string $placa_cavalo
 * @property string $tipo_carreta
 * @property string $placa1
 * @property string $placa2
 * @property string $placa3
 * @property int $quantidade_litros
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Carrier $carrier
 * @property \App\Model\Entity\Driver $driver
 * @property \App\Model\Entity\VehicleOrifice[] $vehicle_orifices
 */
class Vehicle extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}
