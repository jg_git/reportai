<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Plant Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj
 * @property string $ie
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $banco
 * @property string $agencia
 * @property string $conta
 * @property string $tipo_conta
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\PlantContact[] $plant_contacts
 * @property \App\Model\Entity\PlantEmail[] $plant_emails
 * @property \App\Model\Entity\PlantPhone[] $plant_phones
 */
class Plant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'razao_social' => true,
        'nome_fantasia' => true,
        'cnpj' => true,
        'inscricao_estadual' => true,
        'inscricao_municipal' => true,
        'cep' => true,
        'logradouro' => true,
        'numero' => true,
        'complemento' => true,
        'bairro' => true,
        'cidade' => true,
        'estado' => true,
        'bank_id' => true,
        'agencia' => true,
        'conta' => true,
        'tipo_conta' => true,
        'created' => true,
        'modified' => true,
        'plant_contacts' => true,
        'plant_emails' => true,
        'plant_phones' => true
    ];
}
