<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PlantContact Entity
 *
 * @property int $id
 * @property int $plant_id
 * @property string $nome
 * @property string $cpf
 * @property string $email
 * @property string $telefone
 * @property string $celular
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Plant $plant
 */
class PlantContact extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'plant_id' => true,
        'nome' => true,
        'cpf' => true,
        'email' => true,
        'telefone' => true,
        'celular' => true,
        'cep' => true,
        'logradouro' => true,
        'numero' => true,
        'bairro' => true,
        'cidade' => true,
        'estado' => true,
        'created' => true,
        'modified' => true,
        'plant' => true
    ];
}
