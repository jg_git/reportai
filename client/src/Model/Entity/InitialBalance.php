<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * InitialBalance Entity
 *
 * @property int $id
 * @property int $account_id
 * @property string $tipo
 * @property float $valor
 * @property \Cake\I18n\Date $data_saldo
 * @property string $observacao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Account $account
 */
class InitialBalance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'account_id' => true,
        'dc' => true,
        'valor' => true,
        'data_saldo' => true,
        'observacao' => true,
        'created' => true,
        'modified' => true,
        'account' => true
    ];
}
