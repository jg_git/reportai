<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CartOrifice Entity
 *
 * @property int $id
 * @property int $cart_id
 * @property int $numero
 * @property int $quantidade_litros
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Cart $cart
 */
class CartOrifice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cart_id' => true,
        'numero' => true,
        'quantidade_litros' => true,
        'created' => true,
        'modified' => true,
        'cart' => true
    ];
}
