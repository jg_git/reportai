<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PriceSetting Entity
 *
 * @property string $tag
 * @property float $valor
 * @property int $dias_de
 * @property int $dias_ate
 * @property \Cake\I18n\Time $modified
 */
class PriceSetting extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'valor' => true,
        'dias_de' => true,
        'dias_ate' => true,
        'modified' => true
    ];
}
