<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

class StatusClient extends Entity {

    protected $_accessible = [
        'id'=> true,
        'nome' => true,
        'clients' => true        
    ];

}
