<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PurchaseDevolution Entity
 *
 * @property int $id
 * @property int $purchase_id
 * @property int $volume
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Purchase $purchase
 */
class PurchaseDevolution extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'purchase_id' => true,
        'volume' => true,
        'created' => true,
        'purchase' => true
    ];
}
