<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Carrier Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj
 * @property string $ie
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\CarrierContact[] $carrier_contacts
 * @property \App\Model\Entity\CarrierEmail[] $carrier_emails
 * @property \App\Model\Entity\CarrierPhone[] $carrier_phones
 */
class Carrier extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
//        'razao_social' => true,
//        'nome_fantasia' => true,
//        'cnpj' => true,
//        'inscricao_estadual' => true,
//        'inscricao_municipal' => true,
//        'cep' => true,
//        'logradouro' => true,
//        'numero' => true,
//        'complemento' => true,
//        'bairro' => true,
//        'cidade' => true,
//        'estado' => true,
//        'antt' => true,
//        'antt_vencimento' => true,
//        'ibama_cert_regularidade_vencimento' => true,
//        'ibama_autorizacao_ambiental_vencimento' => true,
//        'plano_atend_emergencial_vencimento' => true,
//        'created' => true,
//        'modified' => true,
//        'carrier_contacts' => true,
//        'carrier_emails' => true,
//        'carrier_phones' => true
    ];
}
