<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transfer Entity
 *
 * @property int $id
 * @property \Cake\I18n\Date $data_transferencia
 * @property int $conta_origem_id
 * @property int $conta_destino_id
 * @property string $descricao
 * @property int $payment_method_id
 * @property float $valor
 * @property string $observacao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Account $account
 * @property \App\Model\Entity\PaymentMethod $payment_method
 */
class Transfer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'data_transferencia' => true,
        'conta_origem_id' => true,
        'conta_destino_id' => true,
        'descricao' => true,
        'payment_method_id' => true,
        'valor' => true,
        'observacao' => true,
        'created' => true,
        'modified' => true,
        'account' => true,
        'payment_method' => true
    ];
}
