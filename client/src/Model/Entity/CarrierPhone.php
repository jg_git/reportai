<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CarrierPhone Entity
 *
 * @property int $id
 * @property int $carrier_id
 * @property string $tipo
 * @property string $ddd
 * @property string $telefone
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\Carrier $carrier
 */
class CarrierPhone extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'carrier_id' => true,
        'tipo' => true,
        'ddd' => true,
        'telefone' => true,
        'created' => true,
        'carrier' => true
    ];
}
