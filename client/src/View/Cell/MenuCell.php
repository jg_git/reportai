<?php

namespace App\View\Cell;

use Cake\View\Cell;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Log\Log;

class MenuCell extends Cell {

    public function display() {
        $user = $this->request->session()->read('Auth.User');
        if ($user) {
            if (($menus = Cache::read('menus')) === false) {
                $menus = TableRegistry::get('Menus')->find()->orderAsc('sequence')->toArray();
                Cache::write('menus', $menus);
            }
            $session = $this->request->getSession();
            if ($session->check('Security.role')) {
                $role = $session->read('Security.role');
            } else {
                $role = TableRegistry::get('Roles')
                        ->find()
                        ->where(['id' => $user['role_id']])
                        ->contain(['Modules' => ['sort' => ['Modules.sequence' => 'ASC']], 'Modules.ModuleActions'])
                        ->first();
                $session->write('Security.role', $role);
            }
            $this->set(compact('menus', 'role'));
        } else {
            $this->viewBuilder()->template('login');
        }
    }

}
