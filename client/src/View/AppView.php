<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.0.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace App\View;

use Cake\View\View;

class AppView extends View {

    public function initialize() {
        parent::initialize();
        $this->loadHelper('Thumber.Thumb');

        $this->Form->setTemplates([
            'checkbox' => '<input type="checkbox" class="form-check-input" name="{{name}}" value="{{value}}"{{attrs}}>',
            'nestingLabel' => '<div class="icheck-flat checkbox">{{hidden}}<label class="form-check-label"{{attrs}}>{{input}}{{text}}</label></div>',
            'inputContainer' => '<div class="form-group">{{content}}</div>',
            'input' => '<input type="{{type}}" name="{{name}}" class="form-control"{{attrs}}/>',
            'inputSubmit' => '<input type="{{type}}" class="btn btn-primary"{{attrs}}/>',
            'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
            'select' => '<select name="{{name}}" class="form-control"{{attrs}}>{{content}}</select>',
            'selectMultiple' => '<select name="{{name}}[]" multiple="multiple" class="form-control js-multiple"{{attrs}}>{{content}}</select>',
            'textarea' => '<textarea name="{{name}}" class="form-control"{{attrs}}>{{value}}</textarea>',
        ]);
        $this->Paginator->setTemplates([
            'nextActive' => '<li class="page-item"><a rel="next" class="page-link" href="{{url}}">{{text}}</a></li>',
            'nextDisabled' => '<li class="page-item disabled"><a class="page-link" href="" onclick="return false;">{{text}}</a></li>',
            'prevActive' => '<li class="page-item"><a rel="prev" class="page-link" href="{{url}}">{{text}}</a></li>',
            'prevDisabled' => '<li class="page-item disabled"><a class="page-link" href="" onclick="return false;">{{text}}</a></li>',
            'counterRange' => '{{start}} - {{end}} of {{count}}',
            'counterPages' => '{{page}} of {{pages}}',
            'first' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'last' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
            'current' => '<li class="page-item active"><a class="page-link" href="">{{text}}<span class="sr-only">(current)</span></a></li>',
            'ellipsis' => '<li class="page-item">&hellip;</li>',
        ]);
        $this->Breadcrumbs->templates([
            'wrapper' => '<nav aria-label="breadcrumb"><ol class="breadcrumb"{{attrs}}>{{content}}</ol></nav>',
            'item' => '<li class="breadcrumb-item"{{attrs}}><a href="{{url}}"{{innerAttrs}}>{{title}}</a></li>{{separator}}',
            'itemWithoutLink' => '<li class="breadcrumb-item active"{{attrs}}><span{{innerAttrs}}>{{title}}</span></li>{{separator}}',
        ]);
    }

}
