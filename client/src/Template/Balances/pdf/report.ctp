<style>
    @font-face {
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
        src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
    }

    body {
        font-family: Verdana;
        font-size: 10px;
        margin-left: 0.0cm;
        margin-right: 0.0cm;
    }
</style>
<?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
<hr>

<h5>Contas Financeiras</h5>
<table class="table table-hover report-row">
    <tr>
        <th width="28%">Banco</th>
        <th width="25%">Titular</th>
        <th width="15%">Agência</th>
        <th width="15%">Conta</th>
        <th width="17%" class="money">Saldo</th>
    </tr>
    <?php
    foreach ($balances as $balance) {
        if ($balance['tipo'] == 'F') {
            ?>
            <tr>
                <td><?= $balance['nome'] ?></td>
                <td><?= $balance['titular'] ?></td>
                <td><?= $balance['numero_agencia'] ?></td>
                <td><?= $balance['numero_conta'] ?></td>
                <td>
                    <div class="money <?= $balance['balance'] > 0 ? 'credit' : 'debit' ?>">
                        <?= $this->Number->currency(abs($balance['balance'])) ?> 
                        <?= $balance['balance'] > 0 ? 'C' : 'D' ?>
                    </div>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>
<h5>Caixa</h5>
<table class="table table-hover report-row">
    <tr>
        <th>Titular</th>
        <th class="money">Saldo</th>
    </tr>
    <?php
    foreach ($balances as $balance) {
        if ($balance['tipo'] == 'C') {
            ?>
            <tr>
                <td><?= $balance['titular'] ?></td>
                <td>
                    <div class="money <?= $balance['balance'] > 0 ? 'credit' : 'debit' ?>">
                        <?= $this->Number->currency(abs($balance['balance'])) ?> 
                        <?= $balance['balance'] > 0 ? 'C' : 'D' ?>
                    </div>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>
<h5>Contas Internas</h5>
<table class="table table-hover report-row">
    <tr>
        <th>Razão Social</th>
        <th>Titular</th>
        <th class="money">Saldo</th>
    </tr>
    <?php
    foreach ($balances as $balance) {
        if ($balance['tipo'] == 'I') {
            ?>
            <tr>
                <td><?= $balance['nome'] ?></td>
                <td><?= $balance['titular'] ?></td>
                <td>
                    <div class="money <?= $balance['balance'] > 0 ? 'credit' : 'debit' ?>">
                        <?= $this->Number->currency(abs($balance['balance'])) ?> 
                        <?= $balance['balance'] > 0 ? 'C' : 'D' ?>
                    </div>
                </td>
            </tr>
            <?php
        }
    }
    ?>
</table>

Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
