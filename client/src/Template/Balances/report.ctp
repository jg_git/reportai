<script>
    $(document).ready(function () {
        $("#pdf").click(function () {
            window.open("<?= $this->Url->build(['controller' => 'Balances', 'action' => 'report']) ?>/report.pdf");
        });
    });
</script>

<section class="p-3">
    <?php
    $this->Breadcrumbs->add([
        ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
        ['title' => 'Saldos']
    ]);
    echo $this->Breadcrumbs->render();
    ?>
    <div class="card">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-8 col-sm-12">
                    <h5>Contas Financeiras</h5>
                    <table class="table table-hover extra-slim-row">
                        <tr>
                            <th width="30%">Banco</th>
                            <th width="25%">Titular</th>
                            <th width="15%">Agência</th>
                            <th width="15%">Conta</th>
                            <th width="15%" class="money">Saldo</th>
                        </tr>
                        <?php
                        foreach ($balances as $balance) {
                            if ($balance['tipo'] == 'F') {
                                ?>
                                <tr>
                                    <td><?= $balance['nome'] ?></td>
                                    <td><?= $balance['titular'] ?></td>
                                    <td><?= $balance['numero_agencia'] ?></td>
                                    <td><?= $balance['numero_conta'] ?></td>
                                    <td>
                                        <div class="money <?= $balance['balance'] > 0 ? 'credit' : 'debit' ?>">
                                            <?= $this->Number->currency(abs($balance['balance'])) ?> 
                                            <?= $balance['balance'] > 0 ? 'C' : 'D' ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                    <h5>Caixa</h5>
                    <table class="table table-hover extra-slim-row">
                        <tr>
                            <th>Titular</th>
                            <th class="money">Saldo</th>
                        </tr>
                        <?php
                        foreach ($balances as $balance) {
                            if ($balance['tipo'] == 'C') {
                                ?>
                                <tr>
                                    <td><?= $balance['titular'] ?></td>
                                    <td>
                                        <div class="money <?= $balance['balance'] > 0 ? 'credit' : 'debit' ?>">
                                            <?= $this->Number->currency(abs($balance['balance'])) ?> 
                                            <?= $balance['balance'] > 0 ? 'C' : 'D' ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                    <h5>Contas Internas</h5>
                    <table class="table table-hover extra-slim-row">
                        <tr>
                            <th>Razão Social</th>
                            <th>Titular</th>
                            <th class="money">Saldo</th>
                        </tr>
                        <?php
                        foreach ($balances as $balance) {
                            if ($balance['tipo'] == 'I') {
                                ?>
                                <tr>
                                    <td><?= $balance['nome'] ?></td>
                                    <td><?= $balance['titular'] ?></td>
                                    <td>
                                        <div class="money <?= $balance['balance'] > 0 ? 'credit' : 'debit' ?>">
                                            <?= $this->Number->currency(abs($balance['balance'])) ?> 
                                            <?= $balance['balance'] > 0 ? 'C' : 'D' ?>
                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                    <button id="pdf" type="button" class="btn btn-primary pull-right">Gerar PDF</button>
                </div>
            </div>
        </div>
    </div>
</section>
