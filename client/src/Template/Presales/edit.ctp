<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<?= $this->Html->script('/js/jQuery.blockUI.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {
        preco_mercado = <?= $preco ?>;

        $("input[id$=retira]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            pindex = $(this).data("index");
            pm = parseFloat($("#presales-" + pindex + "-preco-mercado").val().replace(",", "."));
            for (i = 0; i < price_settings.length; i++) {
                if (price_settings[i].tag === "desconto_retira") {
                    preco_mercado = pm - price_settings[i].valor;
                    $("#presales-" + pindex + "-preco-mercado").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', preco_mercado);
                }
            }
        }).on('ifUnchecked', function (e) {
            pindex = $(this).data("index");
            pm = parseFloat($("#presales-" + pindex + "-preco-mercado").val().replace(",", "."));
            for (i = 0; i < price_settings.length; i++) {
                if (price_settings[i].tag === "desconto_retira") {
                    preco_mercado = pm + price_settings[i].valor;
                    $("#presales-" + pindex + "-preco-mercado").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', preco_mercado);
                }
            }
        });

        $("input[name*=data]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom",
            autoclose: true
        });

        $("input[name=data_pedido]").change(function () {
            data = $(this).val().split("/");
            d = new Date(data[2], data[1] - 1, data[0]);
            data_pedido = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
            window.location = "<?= $this->Url->build(['controller' => 'Presales', 'action' => 'edit']) ?>/" + data_pedido;
        });

        typeaheads = [];
        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: "<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'client']) ?>/%QUERY",
                wildcard: '%QUERY'
            }
        });
        $("input[data-client]").each(function () {
            client = $(this).data("client");
            typeaheads.push({
                client: client,
                obj: $('#client-' + client).typeahead(null, {
                    source: engine,
                    display: "name",
                    templates: {
                        notFound: '<div style="margin-left:10px">Cliente não encontrado</div>',
                        suggestion: Handlebars.compile(
                                '<div class="typeahead-item">' +
                                '{{name}}<br>' +
                                '</div>')
                    }
                }).on('typeahead:asyncrequest', null, {client: client}, function (event) {
                    $('#client-' + event.data.client).addClass("loading");
                }).on('typeahead:asynccancel typeahead:asyncreceive', null, {client: client}, function (event) {
                    $('#client-' + event.data.client).removeClass("loading");
                }).on('typeahead:selected', null, {client: client}, function (event, suggestion) {
                    $("#presales-" + event.data.client + "-client-id").val(suggestion.id);
                }).on('typeahead:idle', function () {
                    if ($(this).typeahead('val') === "") {
                        $("#presales-" + event.data.client + "-client-id").val(null);
                    }
                }).on('typeahead:render', function (ev, suggestion, flag, ds) {
                    if (flag && suggestion.length === 0) {
                        $("#presales-" + event.data.client + "-client-id").val(null);
                    }
                }).keyup(function () {
                    if ($(this).val() === '') {
                        $("#presales-" + event.data.client + "-client-id").val(null);
                    }
                })
            });
        });
<?php
$index = 0;
foreach ($presales as $presale) {
    ?>
            $("#presales-<?= $index ?>-preco-venda").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', <?= $presale->preco_venda ?>);
            $("#presales-<?= $index ?>-preco-mercado").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', <?= $presale->preco_mercado ?>);
            $("#presales-<?= $index ?>-volume-pedido").maskMoney({thousands: '.', decimal: ',', precision: 0}).maskMoney('mask', <?= $presale->volume_pedido / 1000 ?>);
    <?php
    $index++;
}
?>

        presale_index = <?= sizeof($presales) ?>;
        $("#button-add-sale").click(function () {
            console.log(presale_index);
            $("#no-data").hide();
            $.get("<?= $this->Url->build(['controller' => 'Presales', 'action' => 'new-sale']) ?>/" + presale_index,
                    function (data) {
                        $(data).insertBefore("#new-anchor");
                        $("#presales-" + presale_index + "-data-pedido").val("<?= $data_pedido ?>");
                        typeaheads.push({
                            client: presale_index,
                            obj: $('#client-' + presale_index).typeahead(null, {
                                source: engine,
                                display: "name",
                                templates: {
                                    notFound: '<div style="margin-left:10px">Cliente não encontrado</div>',
                                    suggestion: Handlebars.compile(
                                            '<div class="typeahead-item">' +
                                            '{{name}}<br>' +
                                            '</div>')
                                }
                            }).on('typeahead:asyncrequest', null, {client: presale_index}, function (event) {
                                $('#client-' + event.data.client).addClass("loading");
                            }).on('typeahead:asynccancel typeahead:asyncreceive', null, {client: presale_index}, function (event) {
                                $('#client-' + event.data.client).removeClass("loading");
                            }).on('typeahead:selected', null, {client: presale_index}, function (event, suggestion) {
                                $("#presales-" + event.data.client + "-client-id").val(suggestion.id);
                            })
                        });
                        $("#presales-" + presale_index + "-preco-venda").maskMoney({thousands: '.', decimal: ',', precision: 4});
                        $("#presales-" + presale_index + "-preco-mercado").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', preco_mercado);
                        $("#presales-" + presale_index + "-volume-pedido").maskMoney({thousands: '.', decimal: ',', precision: 0});
                        $("input[id|=presales-" + presale_index + "-data-pagamento]").each(function () {
                            $(this).datepicker({
                                format: "dd/mm/yyyy",
                                todayBtn: true,
                                language: "pt-BR",
                                orientation: "bottom",
                                autoclose: true
                            });
                        });
                        $("input[id=presales-" + presale_index + "-retira]").iCheck({
                            checkboxClass: 'icheckbox_flat-blue',
                            radioClass: 'iradio_flat',
                            increaseArea: '20%'
                        }).on('ifChecked', function (e) {
                            pindex = $(this).data("index");
                            pm = parseFloat($("#presales-" + pindex + "-preco-mercado").val().replace(",", "."));
                            for (i = 0; i < price_settings.length; i++) {
                                if (price_settings[i].tag === "desconto_retira") {
                                    preco_mercado = pm - price_settings[i].valor;
                                    $("#presales-" + pindex + "-preco-mercado").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', preco_mercado);
                                }
                            }
                        }).on('ifUnchecked', function (e) {
                            pindex = $(this).data("index");
                            pm = parseFloat($("#presales-" + pindex + "-preco-mercado").val().replace(",", "."));
                            for (i = 0; i < price_settings.length; i++) {
                                if (price_settings[i].tag === "desconto_retira") {
                                    preco_mercado = pm + price_settings[i].valor;
                                    $("#presales-" + pindex + "-preco-mercado").maskMoney({thousands: '.', decimal: ',', precision: 4}).maskMoney('mask', preco_mercado);
                                }
                            }
                        });
                        presale_index++;
                    });
        });

        price_settings = [];
<?php
foreach ($price_settings as $setting) {
    ?>
            price_settings.push({
                tag: "<?= $setting->tag ?>",
                valor: <?= $setting->valor ?>,
                dias_de: <?= !is_null($setting->dias_de) ? $setting->dias_de : 0 ?>,
                dias_ate: <?= !is_null($setting->dias_ate) ? $setting->dias_ate : 0 ?>
            });
    <?php
}
?>
        $("#price-now").maskMoney({thousands: '.', decimal: ',', precision: 4});

        $("#edit-price-now").click(function () {
            $("#display-price-now").hide();
            $("#edit-price-now").hide();
            $("#price-now").show();
            $("#price-now").focus();
        });

        $("#price-now").keypress(function (e) {
            if (e.keyCode === 13) {
                price_now = $(this).val();
                e.preventDefault();
                $.post("<?= $this->Url->build(['controller' => 'Presales', 'action' => 'updatePrice']) ?>",
                        $("#form-price-now").serialize(),
                        function (data, code) {
                            preco_mercado = Math.round(parseFloat(price_now.replace(",", ".")) * 10000) / 10000;
                            $("#display-price-now").html(formatMoney(preco_mercado, 4, ',', '.'));
                            $("#display-price-now").show();
                            $("#edit-price-now").show();
                            $("#price-now").hide();
                        });
            }
        }).focusout(function (e) {
            price_now = $(this).val();
            e.preventDefault();
            $.post("<?= $this->Url->build(['controller' => 'Presales', 'action' => 'updatePrice']) ?>",
                    $("#form-price-now").serialize(),
                    function (data, code) {
                        preco_mercado = Math.round(parseFloat(price_now.replace(",", ".")) * 10000) / 10000;
                        $("#display-price-now").html(formatMoney(preco_mercado, 4, ',', '.'));
                        $("#display-price-now").show();
                        $("#edit-price-now").show();
                        $("#price-now").hide();
                    });
        });

        $("#presales").on("click", ".remove-presale", function () {
            swal({
                title: "Deseja remover essa pré-venda?",
                text: "Essa ação não pode ser revertida",
                showCancelButton: true,
                confirmButtonColor: "#079dff",
                cancelButtonColor: "lightgray",
                confirmButtonText: "SIM",
                cancelButtonText: "NÃO",
                confirmButtonClass: "btn btn-lg btn-primary",
                cancelButtonClass: "btn btn-lg btn-outline-dark mr-3",
                buttonsStyling: false,
                reverseButtons: true,
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    pindex = $(this).data("index");
                    presale_id = $(this).data("presale-id");
                    if (presale_id !== "new") {
                        $("#delete-presale-list").append("<option value='" + presale_id + "' selected></option>");
                    }
                    $("tr[data-index=" + pindex + "]").remove();
                }
            });
        });

        $("input[id|=calendar]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom",
            autoclose: true
        }).on("changeDate", function (e) {
            data_pedido = $(this).val();
            presale_id = $(this).data("presale-id")
            swal({
                title: "Mudar Data do Pedido",
                text: "Mudar para " + data_pedido + "?",
                showCancelButton: true,
                confirmButtonColor: "#079dff",
                cancelButtonColor: "lightgray",
                confirmButtonText: "SIM",
                cancelButtonText: "NÃO",
                confirmButtonClass: "btn btn-lg btn-primary",
                cancelButtonClass: "btn btn-lg btn-outline-dark mr-3",
                buttonsStyling: false,
                reverseButtons: true,
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    $("#form-change-date input[name=presale_id]").val(presale_id);
                    $("#form-change-date input[name=data_pedido]").val(data_pedido);
                    $.post("<?= $this->Url->build(['controller' => 'Presales', 'action' => 'changeDate']) ?>",
                            $("#form-change-date").serialize(),
                            function (data, code) {
                                window.location = "<?= $this->Url->build(['controller' => 'Presales', 'action' => 'edit']) ?>/" + "<?= date('Y-m-d', strtotime(str_replace('/', '-', $data_pedido))) ?>";
                            });
                }
            });
        });

        $("#presales").on("click", ".fa-calendar", function () {
            index = $(this).data("index");
            $("input[id=calendar-" + index + "]").datepicker("show");
        });
<?php
foreach ($presales as $index => $presale) {
//    $found = false;
//    foreach ($sales as $sale) {
//        foreach ($sale->sale_details as $detail) {
//            if ($detail->presale_id == $presale->id) {
//                $found = true;
//                break 2;
//            }
//        }
//    }
//    if ($found) {
    if (($presale->volume_pedido == $presale->volume_vendido) or ( $presale->cancelado)) {
        ?>
                $("tr[data-index=<?= $index ?>]").addClass("<?= $presale->cancelado ? 'bg-success' : 'bg-secondary' ?>");
                $("#client-<?= $index ?>").typeahead("destroy");
                $("tr[data-index=<?= $index ?>]").find("input[id*=data]").datepicker("destroy");
                $("tr[data-index=<?= $index ?>]").find("input").prop("readOnly", true);
                $("tr[data-index=<?= $index ?>]").find("input[id$=retira]").iCheck("disable");
                $("tr[data-index=<?= $index ?>]").find(".fa-trash").remove();
                $("tr[data-index=<?= $index ?>]").find(".fa-calendar").remove();
                $("tr[data-index=<?= $index ?>]").find(".fa-plus-circle").remove();
    <?php } else if ($presale->volume_vendido > 0) { ?>
                $("tr[data-index=<?= $index ?>]").addClass("bg-info");
                $("#client-<?= $index ?>").typeahead("destroy");
                $("tr[data-index=<?= $index ?>]").find("input[id*=data]").datepicker("destroy");
                $("tr[data-index=<?= $index ?>]").find("input").prop("readOnly", true);
                $("tr[data-index=<?= $index ?>]").find("input[id$=retira]").iCheck("disable");
                $("tr[data-index=<?= $index ?>]").find(".fa-trash").remove();
                $("tr[data-index=<?= $index ?>]").find(".fa-calendar").remove();
                $("tr[data-index=<?= $index ?>]").find(".fa-plus-circle").remove();
                $("tr[data-index=<?= $index ?>]").find(".fa-window-close").show();
        <?php
    }
}
?>
        $("#presales").on("click", ".fa-plus-circle", function () {
            index = $(this).data("index");
            $("tr[id|=payment-" + index).each(function () {
                if ($(this).is(":hidden")) {
                    $(this).show();
                    return false;
                }
            });
        });

        $("#presales").on("click", ".delete-payment", function () {
            index = $(this).data("index");
            payment = $(this).data("payment");
            $("#presales-" + index + "-data-pagamento-" + payment).val("");
            $("tr[id=payment-" + index + "-" + payment).hide();
        });

        $("#presales").on("click", ".cancel-presale", function () {
            swal({
                title: "Deseja cancelar o saldo dessa pré-venda?",
                showCancelButton: true,
                confirmButtonColor: "#079dff",
                cancelButtonColor: "lightgray",
                confirmButtonText: "SIM",
                cancelButtonText: "NÃO",
                confirmButtonClass: "btn btn-lg btn-primary",
                cancelButtonClass: "btn btn-lg btn-outline-dark mr-3",
                buttonsStyling: false,
                reverseButtons: true,
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    pindex = $(this).data("index");
                    presale_id = $(this).data("presale-id");
                    $("#cancel-presale-list").append("<option value='" + presale_id + "' selected></option>");
                    $("tr[data-index=" + pindex + "]").removeClass("bg-info").addClass("bg-success");
                    $("tr[data-index=" + pindex + "]").find(".fa-window-close").hide();
                }
            });
        });

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Pré Vendas']
]);
echo $this->Breadcrumbs->render();
?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <?= $this->Form->control('data_pedido', ['type' => 'text', 'label' => 'Data Pedido', 'default' => $data_pedido]) ?>
                    </div>
                    <div class="col-md-2 col-sm-12">
                        <label>Preço Atual</label><br>
                        <div id="display-price-now" style="font-size: x-large; font-weight: bold; display: inline"><?= number_format($preco, 4, ',', '.') ?></div>
                        <i id="edit-price-now" class="fa fa-edit ml-2"></i>
                        <?= $this->Form->create(null, ['id' => 'form-price-now']) ?>
                        <?= $this->Form->control('price_now', ['type' => 'text', 'label' => false, 'style' => 'display: none']) ?>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
                <?= $this->Form->create(sizeof($presales) > 0 ? $presales : null, ['id' => 'form-presales']) ?>
                <table id="presales" class="table table-hover slim-row">
                    <tr>
                        <th width="46%">Cliente</th>
                        <th width="8%">Volume</th>
                        <th width="8%">Preço Venda</th>
                        <th width="8%">Preço Mercado</th>
                        <th width="15%">Data Pagamento</th>
                        <th width="4%">Retira</th>
                        <th width="6%" style="text-align: right">Vendido</th>
                        <th width="2%"></th>
                        <th width="2%"></th>
                        <th width="1%"></th>
                    </tr>
                    <?php
                    if (sizeof($presales) > 0) {
                        foreach ($presales as $index => $presale) {
                            ?>
                            <tr data-index="<?= $index ?>" class="">
                                <?= $this->Form->control('Presales.' . $index . '.id', ['type' => 'hidden', 'value' => $presale->id]) ?>
                                <?= $this->Form->control('Presales.' . $index . '.data_pedido', ['type' => 'hidden']) ?>
                                <td>
                                    <?php
                                    if (is_null($previous_data)) {
                                        echo $this->Form->control('client-' . $index, ['type' => 'text', 'label' => false, 'value' => $presale->client->nome_fantasia, 'data-client' => $index]);
                                    } else {
                                        echo $this->Form->control('client-' . $index, ['type' => 'text', 'label' => false, 'value' => $previous_data['client-' . $index], 'data-client' => $index]);
                                    }
                                    ?>
                                    <?php if (array_key_exists('client_id', $presale->getErrors())) { ?>
                                        <div class="error-message">Obrigatório</div>
                                    <?php } ?>
                                    <?= $this->Form->control('Presales.' . $index . '.client_id', ['type' => 'hidden', 'label' => false]) ?>
                                </td>
                                <td><?= $this->Form->control('Presales.' . $index . '.volume_pedido', ['type' => 'text', 'label' => false]) ?></td>
                                <td><?= $this->Form->control('Presales.' . $index . '.preco_venda', ['type' => 'text', 'label' => false]) ?></td>
                                <td><?= $this->Form->control('Presales.' . $index . '.preco_mercado', ['type' => 'text', 'label' => false]) ?></td>
                                <td>
                                    <table style="border: 0px">
                                        <?php
                                        $count = 0;
                                        for ($i = 1; $i <= 4; $i++) {
                                            if (is_null($previous_data)) {
                                                $style = is_null($presale->get('data_pagamento_' . $i)) ? 'display:none;' : '';
                                            } else {
                                                if ($i == 1) {
                                                    $style = '';
                                                } else {
                                                    $style = ($previous_data['Presales'][$index]['data_pagamento_' . $i] != '') ? '' : 'display:none';
                                                }
                                            }
                                            ?>
                                            <tr id="payment-<?= $index ?>-<?= $i ?>" style="border: 0px;<?= $style ?>">
                                                <td width="80%" style="border: 0px; <?= ($i > 1) ? 'padding-top:5px;padding-right:0px;padding-bottom:0px;padding-left:0px;' : 'padding:0px;' ?> ">
                                                    <?= $this->Form->control('Presales.' . $index . '.data_pagamento_' . $i, ['type' => 'text', 'label' => false, 'data-index' => $index]) ?>
                                                </td>
                                                <td width="20%" style="border: 0px;">
                                                    <?php if ($i > 1) { ?>
                                                        <i class="fa fa-trash delete-payment" data-index="<?= $index ?>" data-payment="<?= $i ?>"></i>
                                                    <?php } ?>
                                                    <?php if ($i == 1) { ?>
                                                        <i id="button-add-date" class="fa fa-plus-circle" data-index="<?= $index ?>"></i>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </table>
                                </td>
                                <td><?= $this->Form->control('Presales.' . $index . '.retira', ['type' => 'checkbox', 'label' => false, 'data-index' => $index]) ?></td>
                                <td class="standout" style="text-align: right"><?= number_format($presale->volume_vendido, 0, ',', '.') ?></td>
                                <td style="text-align: center">
                                    <i class="fa fa-trash mt-2 remove-presale" data-index="<?= $index ?>" data-presale-id="<?= $presale->id ?>"></i>
                                    <i class="fa fa-window-close mt-2 cancel-presale" data-index="<?= $index ?>" data-presale-id="<?= $presale->id ?>" style="display: none"></i>
                                </td>
                                <td style="text-align: center"><i class="fa fa-calendar mt-2" data-index="<?= $index ?>"></i></td>
                                <td><input type="text" id="calendar-<?= $index ?>" style="visibility: hidden; width: 0px;" data-presale-id="<?= $presale->id ?>" value="<?= $presale->data_pedido ?>"></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <tr id="new-anchor" style="display:none"><td></td></tr>
                        <?php
                    } else {
                        ?>
                        <tr id="new-anchor" style="display:none"><td></td></tr>
                        <tr id="no-data">
                            <td colspan="13" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td colspan="13">
                            <i id="button-add-sale" class="fa fa-2x fa-plus-square"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->control('delete-presale-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->control('cancel-presale-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->Form->create(null, ['id' => 'form-change-date']) ?>
                <?= $this->Form->control('presale_id', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_pedido', ['type' => 'hidden']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>

<?php
//dump($presales);
?>