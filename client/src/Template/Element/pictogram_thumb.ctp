<?php
if ($pictogram != '' and file_exists(getcwd() . '/files/Pictograms/file/' . $pictogram)) {
    echo $this->Thumb->resize('../files/Pictograms/file/' . $pictogram, ['height' => $height, 'width' => $width]);
} else {
    echo $this->Thumb->resize('no-image.jpg', ['height' => $height, 'width' => $width]);
}