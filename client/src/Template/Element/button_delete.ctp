<?php $uid = rand() ?>
<script>
    $(document).ready(function () {
        $("#delete-<?= $uid ?>").click(function () {
            swal({
                title: 'Remover esse registro?',
                text: 'Essa operação não poderá ser revertida',
                showCancelButton: true,
                confirmButtonColor: '#079dff',
                cancelButtonColor: 'lightgray',
                confirmButtonText: 'REMOVER',
                cancelButtonText: 'CANCELAR',
                confirmButtonClass: 'btn btn-lg btn-primary',
                cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $("#form-delete-<?= $uid ?>").submit();
                }
            });
        });
    });
</script>
<?= $this->Form->create('', ['id' => 'form-delete-' . $uid, 'url' => ['action' => isset($action) ? $action : 'delete', $id]]) ?>
<?= $this->Form->end() ?>
<button id="delete-<?= $uid ?>" class="btn btn-lg btn-outline-dark pull-right mr-3">Remover</button>
