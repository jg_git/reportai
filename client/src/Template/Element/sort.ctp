<?php

if ($request->getQuery('sort') == $field) {
    if ($request->getQuery('direction') == 'asc') {
        echo $this->Html->image('az_black.png', ['class' => 'ml-2']);
    } else if ($request->getQuery('direction') == 'desc') {
        echo $this->Html->image('za_black.png', ['class' => 'ml-2']);
    }
} else {
    echo $this->Html->image('az_gray.png', ['class' => 'ml-2']);
}
                    