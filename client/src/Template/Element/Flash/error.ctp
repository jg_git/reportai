<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script>
    $(document).ready(function () {
        swal({
            title: '<?= array_key_exists('title', $params) ? $params['title'] : "Erro" ?>',
            text: '<?= $message ?>',
            confirmButtonColor: '#079dff',
            confirmButtonText: 'OK',
            confirmButtonClass: 'btn btn-lg btn-primary',
            buttonsStyling: false
        });
    });
</script>
