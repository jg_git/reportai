<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script>
    $(document).ready(function () {
        $.toast({
            heading: 'Informação',
            text: '<?= $message ?>',
            showHideTransition: 'slide',
            icon: 'info',
            loaderBg: '#5cb85c'
        });
    });
</script>
