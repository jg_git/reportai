<?php $uid = rand() ?>
<script>
    $(document).ready(function () {
        $("#delete-<?= $uid ?>").click(function () {
            swal({
                title: 'Clonar este Registro?',
                text: 'Esta operação irá copiar os dados da linha selecionada e Criar uma outra com dados idênticos.',
                showCancelButton: true,
                confirmButtonColor: '#079dff',
                cancelButtonColor: 'lightgray',
                confirmButtonText: 'Clonar',
                cancelButtonText: 'CANCELAR',
                confirmButtonClass: 'btn btn-lg btn-primary',
                cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $("#form-delete-<?= $uid ?>").submit();
                }
            });
        });
    });
</script>
<?= $this->Form->create('', ['id' => 'form-delete-' . $uid, 'url' => ['action' => isset($action) ? $action : 'delete', $id]]) ?>
<?= $this->Form->end() ?>
<a href="#" id="delete-<?= $uid ?>" class="dropdown-item">
    <i class="fa fa-clone"></i>
    <span class="notification-text">
        Clonar
    </span>
</a>

