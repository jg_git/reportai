<script>
    $(document).ready(function () {
        
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
    });   
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($pollsection, [
    'enctype' => 'multipart/form-data',
    'id' => 'form-pollsection-' . $anchor,
    'data-id' => $pollsection->isNew() ? 'new' : $pollsection->id,
    'data-error' => sizeof($pollsection->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
    
    
?>
<?php
    $opcoes =[ 
        '0' => 'Opções de Checklist',
        '1' => 'Número',
        '2' => 'Data e Hora',
        '3' => 'Opções para Seleção Única',
        '4' => 'Opções para Seleção Múltipla',
        '5' => 'Fotos',
        '6' => 'Localização GPS',
        '7' => 'Texto Grande',
        '8' => 'QR Code / Código de Barra',
        '9' => 'Data',
        '10' => 'Hora',
        '11' => 'Endereço',
        '12' => 'Envios Destinatários'
    ];
?>
<div class="row">
    <div class="col">
        <?php
            $checagem = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        ?>
        <?= $this->Form->control('pergunta', ['type' => 'text', 'label' => 'Pergunta' ]) ?>
    </div>
</div>
<div class="row">
    <div class="col">
        <?= $this->Form->control('tipo', ['type' => 'select', 'options' => $opcoes, 'label' => 'Tipo de Pergunta']) ?>
    </div>
</div>
<div class="row">
    <div class="col">
        <?= $this->Form->control('opcoes', ['type' => 'text', 'label' => 'Opções' ]) ?>  
    </div>
</div>
<div class="row">
    <div class="col">
        <?= $this->Form->control('obrigatorio', ['type' => 'checkbox', 'options' => ['obrigatorio' => 'Obrigatório']]) ?>
        <?= $this->Form->control('ordem', ['type' => 'hidden', 'label' => false]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
