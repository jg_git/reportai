<script>
    $(document).ready(function () {
        $("button").click(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
        $("button").focus(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Locais']
]);
echo $this->Breadcrumbs->render();
?>
<?php
    $tipos_form = [
        '0' => 'Auditoria',
        '1' => 'Inventário',
        '2' => 'Laudo',
        '3' => 'Pesquisa',
        '4' => 'Ordem de Serviço',
        '5' => 'Outro'
    ];
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('nome_formulario', ['label' => 'Nome do Formulário']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('location_id', ['label' => 'Local', 'options' => $locations, 'empty' => true]) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('client_id', ['label' => 'Cliente', 'options' => $clients, 'empty' => true]) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('tipo_formulario', ['label' => 'Tipo do Formulário', 'options' => $tipos_form, 'empty' => true]) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('userid', ['type' => 'select', 'options' => $users2, 'empty' => 'SELECIONE UM USUÁRIO', 'label' => 'Usuário Responsável']) ?>
                            <?= $this->Form->control('user_id', ['type' => 'hidden', 'label' => false]) ?>
                        </div>
                        <div class="col-md-2 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>

                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', []) ?> 
                    </div>
                </div>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('Polls.nome_formulario', 'Nome do Formulário') ?></th>
                        <th><?= $this->Paginator->sort('Polls.tipo_formulario', 'Tipo do formulário') ?></th>
                        <th><?= $this->Paginator->sort('Locations.nome_local', 'Nome do Local') ?></th>
                        <th>Usuários Responsáveis</th>
                        <th>Ações</th>
                    </tr>
                    <?php

                    //$id_userlocations = array();
                    
                    //foreach($user_locations as $userloc){
                    //    $data= [ 'user_id' => $userloc->user_id,
                    //             'location_id' => $userloc->location_id,
                    //             'prop_id' => $userloc->prop_id
                    //           ];
                    //    array_push($id_userlocations, $data);
                    //}

                    $contagem=0;
                    if (sizeof($polls) > 0) {
                        foreach ($polls as $poll) {
                            //if((in_array(['user_id' => $this->request->session()->read('Auth.User.id'), 'location_id' => $location->id, 'prop_id' => $this->request->session()->read('Auth.User.prop_id')], $id_userlocations)) OR (($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id') AND $this->request->session()->read('Auth.User.prop_id') == $location->prop_id))){
                                $contagem=$contagem+1;
                    ?>
                            <tr>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $poll->nome_formulario ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?php
                                    switch($poll->tipo_formulario){
                                        case 0:
                                            echo 'Auditoria';
                                            break;
                                        case 1:
                                            echo 'Inventário';
                                            break;
                                        case 2:
                                            echo 'Laudo';
                                            break;
                                        case 3:
                                            echo 'Pesquisa';
                                            break;
                                        case 4:
                                            echo 'Ordem de Serviço';
                                            break;
                                        case 5:
                                            echo 'Outro';
                                            break;

                                    }
                                ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $poll->location->nome_local ?></div></nav></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-user-circle"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?php 
                                                            foreach($userpolls as $userpoll_hov){
                                                                if($userpoll_hov->poll_id == $poll->id){
                                                                    $nome_proprio="";
                                                                    foreach($users as $user){
                                                                        if ($user->id == $userpoll_hov->user_id){
                                                                            $nome_proprio=$user->nome;
                                                                        }
                                                                    }
                                                                    echo '<a class="dropdown-item">';
                                                                        echo '<span>'. $nome_proprio.'</span>';
                                                                    echo '</a>';
                                                                }
                                                            }
                                                        ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Polls', 'action' => 'view', $poll->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Polls', 'action' => 'edit', $poll->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?php 
                                                            //if(!$poll->client AND !$location->actives){
                                                                echo $this->element('a_deactivate', ['id' => $poll->id."i"]);
                                                                echo $this->element('a_clone', ['id' => $poll->id."c"]);
                                                            //}else{
                                                            //    echo $this->element('a_ndelete', ['id' => $location->id]);
                                                            //}
                                                        ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                            <?php
                            //}
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>