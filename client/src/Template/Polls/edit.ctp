<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>
<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>
<script>
    
    $(document).ready(function() {

        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        $("#part1").load("<?= $this->Url->build(['action' => 'part1', $poll->isNew() ? 'new' : $poll->id]) ?>");
        $('#part1').on('ifUnchecked', '#check-all', function (event) {
            $('#part1 .icheck-flat input').iCheck('uncheck');
        });

        // Make "All" checked if all checkboxes are checked
        $('#part1').on('ifChecked', '#check-all', function (event) {
            $('#part1 .icheck-flat input').iCheck('check');
        });

        $("#pollsection").on("click", ".fa-clone", function() {
            var this_anchor = $(this).data("anchor");
            var table = document.getElementById("pollsection");
            var linhas = table.rows.length;
            anchor = linhas;
            $("#pollsection-add").before(
                '<tr id="edit-pollsection-' + anchor + '">' +
                '<td>' +
                '<div id="edit-pollsection-area-' + anchor + '"></div>' +
                '<i class="fa fa-sort-up pull-right" data-type="pollsection" data-ordem="'+(anchor-1)+'" data-anchor="'+anchor+'"></i>'+
                '<i class="fa fa-sort-down pull-right" data-type="pollsection"  data-ordem="'+(anchor-1)+'" data-anchor="'+anchor+'"></i>'+
                '<i class="fa fa-clone pull-right" data-type="pollsection"  data-ordem="'+(anchor-1)+'" data-id = "new" data-anchor="'+anchor+'"></i>'+
                '<i id="trash-' + anchor + '" data-ordem="'+(anchor-1)+'" class="fa fa-trash pull-right" onclick="remove(\'pollsection\', \'' + anchor + '\', \'new\')"></i>' +
                //'<i class="fa fa-check-square pull-right mr-2" data-type="pollsection" data-anchor="' + anchor + '"></i>' +
                '</td>' +
                '</tr>'
                );
            $(".fa-clone").hide();
            if($(this).attr("data-id")!== "new"){
                $("#edit-pollsection-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Polls', 'action' => 'pollsection']) ?>/<?= $poll->isNew() ? 'new' : $poll->id ?>/" +
                    $(this).attr("data-id") + "/" +
                        anchor);
                        $(".fa-clone").show();
            }else{
                $("#edit-pollsection-area-" + anchor).load(
                "<?= $this->Url->build(['controller' => 'Polls', 'action' => 'pollsection']) ?>/<?= $poll->isNew() ? 'new' : $poll->id ?>/" +
                "new" + "/" + anchor);
                
                var this_pergunta = $("#edit-pollsection-" + this_anchor +" input[id=pergunta]").attr("value");
                var this_tipo = $("#edit-pollsection-" + this_anchor +" input[id=tipo]").attr("value");
                var this_opcoes = $("#edit-pollsection-" + this_anchor +" input[id=opcoes]").attr("value");
                var this_obrigatorio = $("#edit-pollsection-" + this_anchor +" input[id=obrigatorio]").attr("value");

                $("#edit-pollsection-area-" + anchor).ready(function(){
                    setTimeout(function(){
                        $("#edit-pollsection-area-" + anchor +" input[id=pergunta]").val(this_pergunta);
                        $("#edit-pollsection-area-" + anchor +" input[id=pergunta]").attr("value", this_pergunta);

                        $("#edit-pollsection-area-" + anchor+" input[id=tipo]").val(this_tipo);
                        $("#edit-pollsection-area-" + anchor+" input[id=tipo]").attr("value", this_tipo);

                        $("#edit-pollsection-area-" + anchor+" input[id=opcoes]").val(this_opcoes);
                        $("#edit-pollsection-area-" + anchor+" input[id=opcoes]").attr("value", this_opcoes);

                        $("#edit-pollsection-area-" + anchor+" input[id=obrigatorio]").val(this_obrigatorio);
                        $("#edit-pollsection-area-" + anchor+" input[id=obrigatorio]").attr("value", this_obrigatorio);

                        $("#edit-pollsection-area-" + anchor +" input[id=ordem]").val(anchor);
                        $("#edit-pollsection-area-" + anchor +" input[id=ordem]").attr("value", anchor);

                        $(".fa-clone").show();
                    }, 1000)
                });
            }
        });

        $(".fa-sort-up").click(function() {
            var direction = "up";
            index = $(this).data("ordem");
            var anchor = $(this).data("anchor");
            var rows = document.getElementById("pollsection").rows,
            parent = rows[index].parentNode;
            var index_futuro = 0;
            if(direction === "up"){
                if(index > 0){
                    index_futuro = index-1;

                    $('i[data-ordem='+index+']').data('ordem', "gen2");
                    $('i[data-ordem='+index+']').attr('data-ordem', "gen2");

                    $('i[data-ordem='+index_futuro+']').data('ordem', index);
                    $('i[data-ordem='+index_futuro+']').attr('data-ordem', index);

                    $(this).data("ordem", index_futuro);
                    $(this).attr("data-ordem", index_futuro);

                    $('i[data-ordem=gen2]').data('ordem',index_futuro);
                    $('i[data-ordem=gen2]').attr('data-ordem',index_futuro);

                    $("#edit-pollsection-" + anchor+" input[id=ordem]").val(index_futuro+1);
                    $("#edit-pollsection-" + anchor+" input[id=ordem]").attr("value", index_futuro+1);

                    var segunda_ancora = 0;
                    segunda_ancora = $("i[data-ordem="+index+"]").data("anchor");
                    $("#edit-pollsection-" + segunda_ancora +" input[id=ordem]").val(index_futuro);
                    $("#edit-pollsection-" + segunda_ancora +" input[id=ordem]").attr("value", index_futuro);
                    parent.insertBefore(rows[index],rows[index - 1]);
                }
            }     
        });

        $(".fa-sort-down").click(function() {
            var direction = "down";
            var index = $(this).data("ordem");
            var anchor = $(this).data("anchor");
            var rows = document.getElementById("pollsection").rows,
            parent = rows[index].parentNode; 
            var index_futuro = 0;  
            if(direction === "down"){
                if(index < rows.length -2){
                    index_futuro = index+1;
                    
                    $('i[data-ordem='+index+']').data('ordem', "gen2");
                    $('i[data-ordem='+index+']').attr('data-ordem', "gen2");

                    $('i[data-ordem='+index_futuro+']').data('ordem', index);
                    $('i[data-ordem='+index_futuro+']').attr('data-ordem', index);

                    $(this).data("ordem", index_futuro);
                    $(this).attr("data-ordem", index_futuro);

                    $('i[data-ordem=gen2]').data('ordem',index_futuro);
                    $('i[data-ordem=gen2]').attr('data-ordem',index_futuro);

                    $("#edit-pollsection-" + anchor+" input[id=ordem]").val(index_futuro+1);
                    $("#edit-pollsection-" + anchor+" input[id=ordem]").attr("value", index_futuro+1);

                    var segunda_ancora = 0;
                    segunda_ancora = $("i[data-ordem="+index+"]").data("anchor");
                    $("#edit-pollsection-" + segunda_ancora +" input[id=ordem]").val(index_futuro);
                    $("#edit-pollsection-" + segunda_ancora +" input[id=ordem]").attr("value", index_futuro);
                    parent.insertBefore(rows[index+1],rows[index]);
                }
            }
        });



        $(".fa-plus-square").click(function() {
            var table = document.getElementById("pollsection");
            var linhas = table.rows.length;
            anchor = linhas;
            $("#no-pollsection").remove();
            $("#pollsection-add").before(
                '<tr id="edit-pollsection-' + anchor + '">' +
                '<td>' +
                '<div id="edit-pollsection-area-' + anchor + '"></div>' +
                '<i class="fa fa-sort-up pull-right" data-type="pollsection" data-ordem="'+(anchor-1)+'" data-anchor="'+anchor+'"></i>'+
                '<i class="fa fa-sort-down pull-right" data-type="pollsection"  data-ordem="'+(anchor-1)+'" data-anchor="'+anchor+'"></i>'+
                '<i class="fa fa-clone pull-right" data-type="pollsection"  data-ordem="'+(anchor-1)+'" data-id = "new" data-anchor="'+anchor+'"></i>'+
                '<i id="trash-' + anchor + '" data-ordem="'+(anchor-1)+'" class="fa fa-trash pull-right" onclick="remove(\'pollsection\', \'' + anchor + '\', \'new\')"></i>' +
                //'<i class="fa fa-check-square pull-right mr-2" data-type="pollsection" data-anchor="' + anchor + '"></i>' +
                '</td>' +
                '</tr>'
                );
            $(".fa-plus-square").hide();
            $("#edit-pollsection-area-" + anchor).load(
                "<?= $this->Url->build(['controller' => 'Polls', 'action' => 'pollsection']) ?>/<?= $poll->isNew() ? 'new' : $poll->id ?>/" +
                "new" + "/" + anchor);

            var table = document.getElementById("pollsection");

            $("#edit-pollsection-area-" + anchor).ready(function(){
                setTimeout(function(){
                    $("#edit-pollsection-area-" + anchor + " input[id=ordem]").val(linhas);
                    $("#edit-pollsection-area-" + anchor + " input[id=ordem]").attr('value', linhas);
                    $(".fa-plus-square").show();
                }, 1000);
            });
            
        });
        $()
        $("#save").click(function() {
            HoldOn.open({
                theme: "sk-cube-grid"
            });
            var part1 = $.Deferred();
            var pollsection = $.Deferred();
            poll_id = $("#form-part1").data("id");
            $.post("<?= $this->Url->build(['action' => 'part1']) ?>/" + poll_id,
                $("#form-part1").serialize(),
                function(data) {
                    $("#part1").html(data);
                    if ($("<div>" + data + "</div>").find("#form-part1").data("error") === "N") {
                        part1.resolve("OK");
                        poll_id = $("<div>" + data + "</div>").find("#form-part1").data("id");
                        //======== Salvar Formulários ======
                        var req_pollsection = [];
                        $("form[id|='form-pollsection']").each(function() {
                            anchor = $(this).data("anchor");
                            req_pollsection.push($.post("<?= $this->Url->build(['controller' => 'Polls', 'action' => 'pollsection']) ?>/" + poll_id + "/" + $(this).data("id") + "/" + anchor,
                                $("#form-pollsection-" + anchor).serialize()));
                        });
                        if (req_pollsection.length > 0) {
                            $.when.apply(undefined, req_pollsection).then(function() {
                                var objects = arguments;
                                var saved = true;
                                var data = [];
                                if (Array.isArray(objects[0])) {
                                    for (i = 0; i < objects.length; i++) {
                                        data.push(objects[i][0]);
                                    }
                                } else {
                                    data.push(objects[0]);
                                }
                                for (i = 0; i < data.length; i++) {
                                    anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-pollsection']").data("anchor");
                                    id = $("<div>" + data[i] + "</div>").find("form[id|='form-pollsection']").data("id");
                                    $("#edit-pollsection-area-" + anchor).html(data[i]);
                                    $("#trash-" + anchor).attr("onclick", "remove('pollsection', '" + anchor + "', " + id + ")");
                                    if ($("<div>" + data[i] + "</div>").find("#form-pollsection-" + anchor).data("error") === "N") {} else {
                                        saved = false;
                                    }
                                }
                                pollsection.resolve(saved ? "OK" : "NG");
                            });
                        } else {
                            pollsection.resolve("OK");
                        }
                        if ($("#form-delete-pollsection option").length > 0) {
                            $.post("<?= $this->Url->build(['controller' => 'Polls', 'action' => 'pollsection-delete']) ?>",
                                $("#form-delete-pollsection").serialize());
                        }
                    } else {
                        part1.resolve("NG");
                        pollsection.resolve("NG");
                    }
                });
            $.when(part1, pollsection).done(function(v1, v2) {
                $("#save").attr("disabled", false);
                console.log("part1: " + v1 + " pollsection: " + v2);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK") {
                    toast("Cadastro salvo com sucesso");
                    var url = window.location.href;
                    if (url.includes('new')===true){
                        window.history.back() 
                    }else{
                        location.reload(true);
                    }
                }
            });
        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Este registro só será removido após o salvamento da página, tem certeza que deseja adicioná-lo a lista de exclusão?',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $("#edit-pollsection-" + anchor).remove();
                $("#view-pollsection-" + anchor).remove();
                if (id !== "new") {
                    $("#delete-pollsection-list").append("<option value='" + id + "' selected></option>");
                }
                var table = document.getElementById("pollsection");
                var contagem = 0;
                var ordemPos = 0;
                    
                $("#pollsection tr").each(function(index) {
                    ordemPos = $(this).find(".fa-trash").attr("data-ordem");
                    
                    $("i[data-ordem="+ordemPos+"]").data("ordem", contagem);
                    $("i[data-ordem="+ordemPos+"]").attr("data-ordem", contagem);
                    
                    $(this).find("#ordem").val(contagem+1);
                    $(this).find("#ordem").attr("value", contagem+1);
                    contagem = contagem +1;
                });     
            }
        });
        
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Formulários', 'url' => ['action' => 'index']],
    ['title' => 'Cadastro de Formulários']
]);
echo $this->Breadcrumbs->render();
?>
    <div class="card">
        <div class="card-body">
            <div id="part1">
                
            </div>
            <?php
                $somatoria = 0;
                $contagem = 0;
            ?>
            <div class="row">
                <div class="col-md-12">
                    <p class="mt-3">Perguntas</p>
                    <table id="pollsection" class="table table-hover">
                        <?php
                        if (isset($poll->poll_sections) and sizeof($poll->poll_sections) > 0) {
                            $i=0;
                            $poll_sections = $poll->poll_sections;

                            usort($poll_sections, function($a, $b) {
                                return $a['ordem'] - $b['ordem'];
                            });
                            foreach ($poll_sections as $pollsection) {
                                
                                $anchor = $pollsection->ordem;
                                ?>
                                <tr id="edit-pollsection-<?= $anchor ?>">
                                    <td>
                                        <div id="edit-pollsection-area-<?= $anchor ?>">
                                            <script>
                                            $(document).ready(function() {
                                                setTimeout(function(){ 
                                                    anchor = <?= $anchor ?>;
                                                    if ($("#edit-pollsection-area-" + anchor + " form[id|=form-pollsection]").length === 0) {
                                                        $("#edit-pollsection-area-" + anchor).load(
                                                            "<?= $this->Url->build(['controller' => 'Polls', 'action' => 'pollsection']) ?>/<?= $poll->isNew() ? 'new' : $poll->id ?>/" +
                                                            <?= $pollsection->id ?> + "/" +
                                                            anchor);
                                                    }
                                                }, 100);
                                            });
                                            </script>
                                        </div>
                                        <i class="fa fa-sort-up pull-right" data-type="pollsection" data-ordem="<?= $i ?>" data-anchor="<?= $anchor ?>" data-id="<?= $pollsection->id ?>"></i>
                                        <i class="fa fa-sort-down pull-right" data-type="pollsection"  data-ordem="<?= $i ?>" data-anchor="<?= $anchor ?>" data-id="<?= $pollsection->id ?>"></i>
                                        <i class="fa fa-clone pull-right" data-type="pollsection" data-anchor="<?= $anchor ?>" data-id="<?= $pollsection->id ?>"></i>
                                        <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" data-ordem="<?= $i ?>" onclick="remove('pollsection', '<?= $anchor ?>', <?= $pollsection->id ?>)"></i>
                                    </td>
                                </tr>
                            <?php
                                $i++;
                                }
                            } else {
                                ?>
                            <tr id="no-pollsection">
                                <td class="no-data-found">Nenhuma Pergunta Cadastrada</td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr id="pollsection-add">
                            <td>
                                <i class="fa fa-plus-square" data-type="pollsection"></i>
                            </td>
                        </tr>
                    </table>
                    <?= $this->Form->create(null, ['id' => 'form-delete-pollsection']) ?>
                    <?= $this->Form->control('delete-pollsection-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col">
                    <?= $this->element('button_save', ['id' => 'save']) ?>
                    <?= $this->element('button_return') ?>
                </div>
            </div>
        </div>
    </div>