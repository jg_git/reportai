<script>

    $(document).ready(function () {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
    });
</script>

<?= $this->Flash->render() //PÁGINA CRIADA POR MATHEUS MIELLY?>
<?=
$this->Form->create($poll, [
    'id' => 'form-part1',
    'data-id' => $poll->isNew() ? 'new' : $poll->id,
    'data-error' => sizeof($poll->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Formulário</h5>
<div class="row mb-3 mt-3">
</div>
<div class="row">
<?php
    $tipos_form = [
        '0' => 'Auditoria',
        '1' => 'Inventário',
        '2' => 'Laudo',
        '3' => 'Pesquisa',
        '4' => 'Ordem de Serviço',
        '5' => 'Outro'
    ];
?>
    <div class="col-md-4 col-sm-12">
        <?php
            $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('nome_formulario', ['type' => 'text', 'label' => 'Nome do Formulário', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="nome_formulario" style="display: inline; margin-bottom: .5rem">Nome do Formulário</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('nome_formulario', ['type' => 'text', 'label' => false ]);
            }
        ?>
    </div>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('location_id', ['type' => 'select', 'options' => $locations, 'label' => 'Locais', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="location_id" style="display: inline; margin-bottom: .5rem">Locais</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('location_id', ['type' => 'select', 'options' => $locations, 'empty' => 'SELECIONE UM LOCAL', 'label' => false]);
            }
        ?>    
    </div>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('tipo_formulario', ['type' => 'select', 'options' => $tipos_form, 'label' => 'Tipo de Formulário', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="tipo_formulario" style="display: inline; margin-bottom: .5rem">Tipo de Formulário</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('tipo_formulario', ['type' => 'select', 'options' => $tipos_form, 'empty' => 'SELECIONE UM TIPO DE FORMULÁRIO', 'label' => false]);
            }
        ?>    
    </div>
</div>
<div class="row">
        <div class="col-md-3 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label>';
                $usuariospresentes = array();
                foreach($userpoll as $user_unico){
                    if ($poll->id == $user_unico['poll_id']){
                        array_push($usuariospresentes, $user_unico['user_id']);
                    }
                }
                foreach($users as $id => $nome){
                    if(in_array($id, $usuariospresentes)){
                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true, 'disabled' => true])."</div>";
                    }else{
                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'disabled' => true])."</div>";
                    }
                }
        ?>
        <div style="margin-bottom: .5rem">
        </div>
        <?php } else {  ?>
        <div style="margin-bottom: .5rem">
            <?php
                echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label>';
                echo "<div>". $this->Form->control('check-all', ['type' => 'checkbox', 'value' => 'todos', 'label' => 'Selecionar Todos'])."</div>";
                $usuariospresentes = array();
                foreach($userpoll as $user_unico){
                    if ($poll->id == $user_unico['poll_id']){
                        array_push($usuariospresentes, $user_unico['user_id']);
                    }
                }
                foreach($users as $id => $nome){
                    if(in_array($id, $usuariospresentes)){
                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true])."</div>";
                    }else{
                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome])."</div>";
                    }
                }
            ?>
        </div>
        <?php } 
        ?>
    </div>
<?= $this->Form->end() ?>
