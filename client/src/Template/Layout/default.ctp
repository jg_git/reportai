<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ReportAí Cliente </title>
        <?= $this->Html->charset() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <?= $this->Html->css('/node_modules/font-awesome/css/font-awesome.min.css'); ?>
        <?= $this->Html->css('/node_modules/sweetalert2/dist/sweetalert2.min.css?v=2'); ?>
        <?= $this->Html->css('/node_modules/icheck/skins/all.css'); ?>
        <?= $this->Html->css('/plugins/jQuery-Toast-Message-Plugin-Toastr/build/toastr.css'); ?>
        <?= $this->fetch('css-block'); ?>
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('custom-style.css?v=' . rand()); ?>
        <?= $this->Html->meta('icon') ?>
    </head>
    <!--<body style="background-color: #D5FFD5">-->
    <body style="background-color: #F4F5F7">
        <?= $this->Html->script('/node_modules/jquery/dist/jquery.min.js') ?>
        <?php if ($this->request->session()->read('Auth.User.id')) { ?>
            <nav class="navbar navbar-expand-sm" style="background-color: black">
                <div class="d-flex align-items-center justify-content-center h-100" style="width: 100%; color: white;">
                    <?php 
                        $tipoLogUser="";
                        
                        if ($this->request->session()->read('Auth.User.role_id') == 1){
                            $tipoLogUser="Administrador";
                        }else{
                            $tipoLogUser="Suporte";
                        }

                        echo $this->request->session()->read('Auth.User.nome'). " - ". $tipoLogUser;
                    ?>
                </div>
            </nav>
            <nav class="navbar sticky-top navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="<?= $this->Url->build(['controller' => 'Home', 'action' => 'index']) ?>">
                    <?= $this->Html->image('logoReportAiCinza.png', array('width'=>'150px')) ?>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item mr-4">
                            <?= $this->Html->link('Home', ['controller' => 'Home', 'action' => 'index'], ['class' => 'nav-link']) ?>
                        </li>
                        <?= $this->cell('Menu') ?>
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                                    <i class="fa fa-bell"></i>
                                    <span class="badge badge-light">1</span>
                                    <!--<i class="fa fa-xs fa-chevron-down" style="color: #159450"></i>-->
                                    <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="notificationDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fa fa-bell text-success"></i>
                                        <span class="notification-text">Reservado para futuro uso</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-user-circle"></i>
                                <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-lock']) . ' ' . $this->Html->tag('span', 'Senha', ['class' => 'notification-text']), ['controller' => 'Users', 'action' => 'password'], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-sign-out']) . ' ' . $this->Html->tag('span', 'Sair', ['class' => 'notification-text']), ['controller' => 'Users', 'action' => 'logout'], ['class' => 'dropdown-item', 'escape' => false]) ?>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        <?php } ?>
        <div id="main-canvas" class="p-4">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
        <?= $this->Html->script('/node_modules/sweetalert2/dist/sweetalert2.min.js?v=2') ?>
        <?= $this->Html->script('/node_modules/icheck/icheck.min.js') ?>
        <?= $this->Html->script('/plugins/jQuery-Toast-Message-Plugin-Toastr/build/toastr.min.js') ?>
        <?= $this->Html->script('/js/utility.js?v=6') ?>
        <?= $this->fetch('script-block'); ?>
    </body>
</html>