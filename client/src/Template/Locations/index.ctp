<script>
    $(document).ready(function () {
        $("button").click(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
        $("button").focus(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Locais']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('nome_local', ['label' => 'Nome']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('client_id', ['label' => 'Cliente', 'options' => $clients2, 'empty' => true]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('ativo', ['type' => 'select', 'options' =>['1' => 'Ativo', '0' => 'Inativo'], 'label' => 'Situação do Local']) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('userid', ['type' => 'select', 'options' => $users2, 'empty' => 'SELECIONE UM USUÁRIO', 'label' => 'Usuário Responsável']) ?>
                            <?= $this->Form->control('user_id', ['type' => 'hidden', 'label' => false]) ?>
                        </div>
                        <div class="col-md-2 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear_client') ?>
                        </div>
                    </div>

                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', []) ?> 
                    </div>
                </div>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('nome_local', 'Nome do Local') ?></th>
                        <th><?= $this->Paginator->sort('endereco_full', 'Endereço') ?></th>
                        <th>Usuários Responsáveis</th>
                        <th>Ações</th>
                    </tr>
                    <?php

                    if (sizeof($locations) > 0) {
                        foreach ($locations as $location) {
                    ?>
                            <tr>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $location->nome_local ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $location->endereco_full  ?></div></nav></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-user-circle"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?php 
                                                            foreach($user_locations as $userloc_hov){
                                                                if($userloc_hov->location_id == $location->id){
                                                                    $nome_proprio="";
                                                                    foreach($users as $user){
                                                                        if ($user->id == $userloc_hov->user_id){
                                                                            $nome_proprio=$user->nome;
                                                                        }
                                                                    }
                                                                    echo '<a class="dropdown-item">';
                                                                        echo '<span>'. $nome_proprio.'</span>';
                                                                    echo '</a>';
                                                                }
                                                            }
                                                        ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Locations', 'action' => 'view', $location->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Locations', 'action' => 'edit', $location->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?php 
                                                            if(!$location->polls AND !$location->actives){
                                                                echo $this->element('a_delete', ['id' => $location->id]);
                                                            }else{
                                                                echo $this->element('a_nfdelete', ['id' => $location->id]);
                                                            }
                                                        ?>
                                                        <?php if($location->ativo == '1' or $location->ativo == 1){ ?>
                                                            <?= $this->element('a_deactivate', ['id' => $location->id.'i'])?>
                                                        <?php } ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                            <?php
                            
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>