<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

<script>
    function initMap(myLatLng) {
        if (!myLatLng) {
            var vlat = parseFloat($("input[id=latitude]").val());
            var vlng = parseFloat($("input[id=longitude]").val());
            var myLatLng = {
                lat: -23.5965987,
                lng: -48.0706708
            };
            myLatLng.lat = vlat;
            myLatLng.lng = vlng;
        }
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 16,
            center: myLatLng,
        });
        new google.maps.Marker({
            position: myLatLng,
            map,
            title: "Endereço",
        });
    }

    $(document).ready(function() {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        $("#telefoneum").inputmask({
            mask: ["(99) 9999-9999", "(99) 99999-9999", ],
            keepStatic: true
        });
        $("#telefonedois").inputmask({
            mask: ["(99) 9999-9999", "(99) 99999-9999", ],
            keepStatic: true
        });
        $("#telefonetres").inputmask({
            mask: ["(99) 9999-9999", "(99) 99999-9999", ],
            keepStatic: true
        });

        $("#cep").inputmask("99999-999");

        $("#cep").focusout(function() {
            var cep_value = $(this).val();
            var cep_stripped = cep_value.replace(/[^A-Za-z0-9]/g, '');
            var error = false;
            $.getJSON("<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'cep']) ?>" + "/" + cep_stripped, function(data) {
                $.each(data, function(key, val) {
                    switch (key) {
                        case "logradouro":
                            $("#logradouro").val(val);
                            break;
                        case "bairro":
                            $("#bairro").val(val);
                            break;
                        case "localidade":
                            $("#cidade").val(val);
                            break;
                        case "uf":
                            $("#form-location select[id=estado").val(val);
                            break;
                        case "latitude":
                            $("#latitude").val(val);
                            break;
                        case "longitude":
                            $("#longitude").val(val);
                        case "email_adm":
                            $("#email_adm").val(val);
                        case "url_dashboard":
                            $("#url_dashboard").val(val);
                        case "erro":
                            error = true;
                    }
                });
                if (!error) {
                    $("#numero").focus();
                }
            });
        });
    });

    $("#telefoneum").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });
    $("#telefonedois").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });
    $("#telefonetres").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });

    $('#check-all').on('ifUnchecked', function(event) {
        $('.icheck-flat input').iCheck('uncheck');
    });

    // Make "All" checked if all checkboxes are checked
    $('#check-all').on('ifChecked', function(event) {
        $('.icheck-flat input').iCheck('check');
    });

    $("div[id=pesquisar]").click(function() {
        var numero_value = $("input[id=numero]").val();
        var city_value = $("input[id=cidade]").val();
        var street_value = $("input[id=logradouro]").val();
        var state_value = $("input[id=estado]").val();
        var hood_value = $("input[id=bairro]").val();

        var error = false;
        var address = street_value + ", " + numero_value + " " + hood_value + ", " + city_value + " - " + state_value + ", Brazil";
        var apiKey = "AIzaSyAlijF-i2tTacU13MYfDCfOjTnB-WA46so";
        var error = false;
        $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "CA&key=" + apiKey, function(data) {
            $.each(data, function(key, val) {
                if (key != 'status') {
                    $("input[id=latitude]").val(val[0]["geometry"]["location"]["lat"]);
                    $("input[id=latitude]").prop("disabled", false);

                    $("input[id=longitude]").val(val[0]["geometry"]["location"]["lng"]);
                    $("input[id=longitude]").prop("disabled", false);

                    var vlat = parseFloat(val[0]["geometry"]["location"]["lat"]);
                    var vlng = parseFloat(val[0]["geometry"]["location"]["lng"]);
                    var milat = {
                        lat: -23.5965987,
                        lng: -48.0706708
                    };
                    milat.lat = vlat;
                    milat.lng = vlng;
                    initMap(milat);
                }
            });
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Locais', 'url' => ['action' => 'index?ativo=1']],
    ['title' => 'Cadastro de Locais']
]);
echo $this->Breadcrumbs->render();
?>
<?= $this->Flash->render() ?>
<div class="card">
    <div class="card-body">
        <?=
        $this->Form->create($location, [
            'id' => 'form-location',
            'data-id' => $location->isNew() ? 'new' : $location->id,
            'data-error' => sizeof($location->getErrors()) > 0 ? 'Y' : 'N'
        ])
        ?>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?php
                $checagem = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('nome_local', ['type' => 'text', 'label' => 'Nome do Local', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="nome_local" style="display: inline; margin-bottom: .5rem">Nome do Local</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('nome_local', ['type' => 'text', 'label' => false]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php

                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'label' => 'Cliente Proprietário', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="client_id" style="display: inline; margin-bottom: .5rem">Cliente Proprietário</label>';
                    echo '</div>';
                    echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'empty' => 'SELECIONE UM CLIENTE', 'label' => false]);
                }
                ?>
            </div>

        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?php

                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('cep', ['type' => 'text', 'label' => 'CEP', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="cep" style="display: inline; margin-bottom: .5rem">CEP</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('cep', ['type' => 'text', 'label' => false]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('logradouro', ['type' => 'text', 'label' => 'Logradouro', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="logradouro" style="display: inline; margin-bottom: .5rem">Logradouro</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('logradouro', ['type' => 'text', 'label' => false]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('numero', ['type' => 'text', 'label' => 'Número', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="numero" style="display: inline; margin-bottom: .5rem">Número</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('numero', ['type' => 'text', 'label' => false]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento', 'disabled' => true]);
                } else {
                    echo $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento']);
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('bairro', ['type' => 'text', 'label' => 'Bairro', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="bairro" style="display: inline; margin-bottom: .5rem">Bairro</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('bairro', ['type' => 'text', 'label' => false]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('cidade', ['type' => 'text', 'label' => 'Cidade', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="cidade" style="display: inline; margin-bottom: .5rem">Cidade</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('cidade', ['type' => 'text', 'label' => false]);
                }

                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('estado', ['options' => $states, 'label' => 'Estado', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="estado" style="display: inline; margin-bottom: .5rem">Estado</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('estado', ['options' => $states, 'label' => false]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                } else {
                ?>
                    <div style="margin-top: .3rem; height: 80px; width: auto; display: flex; align-items: center; justify-content: center">
                        <div class="btn btn-lg btn-dark" id="pesquisar">CONSULTAR LATITUDE E LONGITUDE</div>
                    </div>
                <?php
                }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('latitude', ['type' => 'text', 'label' => 'Latitude', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="latitude" style="display: inline; margin-bottom: .5rem">Latitude</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('latitude', ['type' => 'text', 'label' => false, 'disabled' => $location->isNew()]);
                }
                ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('longitude', ['type' => 'text', 'label' => 'Longitude', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="longitude" style="display: inline; margin-bottom: .5rem">Longitude</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('longitude', ['type' => 'text', 'label' => false, 'disabled' => $location->isNew()]);
                }
                ?>
            </div>
            <div class="col-md-6 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('info_add', ['type' => 'textarea', 'label' => 'Informações Adicionais', 'disabled' => true]);
                } else {
                    echo $this->Form->control('info_add', ['type' => 'textarea', 'label' => 'Informações Adicionais']);
                }
                ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) { ?>
                    <?= $this->Form->control('telefoneum', ['label' => 'Primeiro Telefone', 'disabled' => true]) ?>
                <?php } else { ?>
                    <label> Primeiro Telefone </label><span style="color: red">*</span>
                    <?= $this->Form->control('telefoneum', ['label' => false]) ?>
                <?php } ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) { ?>
                    <?= $this->Form->control('telefonedois', ['label' => 'Segundo Telefone', 'disabled' => true]) ?>
                <?php } else { ?>
                    <?= $this->Form->control('telefonedois', ['label' => 'Segundo Telefone']) ?>
                <?php } ?>
            </div>
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) { ?>
                    <?= $this->Form->control('telefonetres', ['label' => 'Terceiro Telefone', 'disabled' => true]) ?>
                <?php } else { ?>
                    <?= $this->Form->control('telefonetres', ['label' => 'Terceiro Telefone']) ?>
                <?php } ?>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3 col-sm-12">
                <?php

                if (strpos($checagem, "view") == true) {
                    echo $this->Form->control('nome_contato', ['type' => 'text', 'label' => 'Nome do Contato', 'disabled' => true]);
                } else {
                    echo '<div style="margin-bottom: .5rem">';
                    echo '<label for="nome_contato" style="display: inline; margin-bottom: .5rem">Nome do Contato</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    echo '</div>';
                    echo $this->Form->control('nome_contato', ['type' => 'text', 'label' => false]);
                }
                ?>
            </div>

        </div>
        <br>
        <br>
        <br>
        <div class="row">
            <div class="col-md-3 col-sm-12">
            </div>
            <div class="col-md-6 col-sm-12">
                <div id="map" style="height: 20rem">
                </div>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlijF-i2tTacU13MYfDCfOjTnB-WA46so&callback=initMap&libraries=&v=weekly" async></script>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?php
                if (strpos($checagem, "view") == true) {
                    echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    $usuariospresentes = array();
                    foreach ($userloc as $user_unico) {
                        if ($location->id == $user_unico['location_id']) {
                            array_push($usuariospresentes, $user_unico['user_id']);
                        }
                    }
                    foreach ($users as $id => $nome) {
                        if (in_array($id, $usuariospresentes)) {
                            echo "<div>" . $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true, 'disabled' => true]) . "</div>";
                        } else {
                            echo "<div>" . $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'disabled' => true]) . "</div>";
                        }
                    }
                ?>
                    <div style="margin-bottom: .5rem">
                    </div>
                <?php } else {  ?>
                    <div style="margin-bottom: .5rem">
                        <?php
                        echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                        echo "<div>" . $this->Form->control('check-all', ['type' => 'checkbox', 'value' => 'todos', 'label' => 'Selecionar Todos']) . "</div>";
                        $usuariospresentes = array();
                        foreach ($userloc as $user_unico) {
                            if ($location->id == $user_unico['location_id']) {
                                array_push($usuariospresentes, $user_unico['user_id']);
                            }
                        }
                        foreach ($users as $id => $nome) {
                            if (in_array($id, $usuariospresentes)) {
                                echo "<div>" . $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true]) . "</div>";
                            } else {
                                echo "<div>" . $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome]) . "</div>";
                            }
                        }
                        ?>
                    </div>
                <?php }
                ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
        <?= $this->element('button_return_client') ?>
    </div>
</div>