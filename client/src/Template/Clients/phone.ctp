<script>
    $(document).ready(function () {
        $('#tipo-phone input[type=radio]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            if ($(this)[0]['name'] === 'tipo') {
                switch ($(this).val()) {
                    case "F":
                        $("#form-phone-<?= $anchor ?> input[id=telefone]").inputmask("9999-9999");
                        break;
                    case "C":
                        $("#form-phone-<?= $anchor ?> input[id=telefone]").inputmask("99999-9999");
                        break;
                }
            }
        });
    });

<?php if ($phone->tipo == 'F') { ?>
        $("#form-phone-<?= $anchor ?> input[id=telefone]").inputmask("9999-9999");
<?php } else { ?>
        $("#form-phone-<?= $anchor ?> input[id=telefone]").inputmask("99999-9999");
<?php } ?>
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($phone, [
    'id' => 'form-phone-' . $anchor,
    'data-id' => $phone->isNew() ? 'new' : $phone->id,
    'data-error' => sizeof($phone->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row">
    <div id="tipo-phone" class="col-md-4 col-sm-12">
        <?= $this->Form->radio('tipo', $phone_types, ['default' => 'F']) ?>
    </div>
    <div class="col-md-4 col-sm-12">
        <?= $this->Form->control('ddd', ['type' => 'text', 'label' => 'DDD']) ?>
    </div>
    <div class="col-md-4 col-sm-12">
        <?= $this->Form->control('telefone', ['type' => 'text', 'label' => 'Telefone']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
