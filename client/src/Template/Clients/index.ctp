<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script>
    $(document).ready(function () {
        $("button").focus(function(){
            var item = $('#cnpjcpf').val();
            item = item.replace(' ','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace(',','');
            $('#cnpjcpf').val(item);
            var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });

        $("button").click(function(){
            var item = $('#cnpjcpf').val();
            item = item.replace(' ','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace(',','');
            $('#cnpjcpf').val(item);

            var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Clientes']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                    
                        <div id="cnpj-cpf" class="col-md-2 col-sm-12">
                            <?= $this->Form->control('cnpjcpf', ['type' => 'text', 'label' => 'CPF/ CNPJ']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('razao_social', ['label' => 'Nome do cliente / Razão Social']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('ativo', ['type' => 'select', 'options' =>['1' => 'Ativo', '0' => 'Inativo'], 'label' => 'Situação do Cliente']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('userid', ['type' => 'select', 'options' => $users2, 'empty' => 'SELECIONE UM USUÁRIO', 'label' => 'Usuário Responsável']) ?>
                            <?= $this->Form->control('user_id', ['type' => 'hidden', 'label' => false]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear_client') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', []) ?> 
                    </div>
                </div>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                    
                        <th><?= $this->Paginator->sort('cnpj_cpf', 'CPF/CNPJ') ?></th>
                        <th><?= $this->Paginator->sort('razao_social', 'Nome do cliente / Razão Social') ?></th>
                        <th><?= $this->Paginator->sort('endereco_full', 'Endereço') ?></th>
                        <th><?= $this->Paginator->sort('email', 'E-mail') ?></th>
                        <th>Usuários Responsáveis</th>
                        <th>Ações</th>
                    </tr>
                    <?php

                    if (sizeof($clients) > 0) {
                        foreach ($clients as $client) {
                            ?>
                            <tr>
                            <?php 
                                $cpf_prealterado = $client->cnpj_cpf;
                                if(strlen($cpf_prealterado)>11){
                                    $cpf_alterado = substr($cpf_prealterado,0,2).'.'.substr($cpf_prealterado,2,3).'.'.substr($cpf_prealterado,5,3).'/'.substr($cpf_prealterado,8,4).'-'.substr($cpf_prealterado,12,2);
                                }else{
                                    $cpf_alterado = substr($cpf_prealterado,0,3).'.'.substr($cpf_prealterado,3,3).'.'.substr($cpf_prealterado,6,3).'-'.substr($cpf_prealterado,9,2);
                                }
                            ?>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $cpf_alterado ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $client->razao_social ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $client->endereco_full  ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $client->email ?></div></nav></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-user-circle"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?php 
                                                            foreach($userclientes as $usercli_hov){
                                                                if($usercli_hov->client_id == $client->id){
                                                                    $nome_proprio="";
                                                                    foreach($users as $user){
                                                                        if ($user->id == $usercli_hov->user_id){
                                                                            $nome_proprio=$user->nome;
                                                                        }
                                                                    }
                                                                    echo '<a class="dropdown-item">';
                                                                        echo '<span>'. $nome_proprio.'</span>';
                                                                    echo '</a>';
                                                                }
                                                            }
                                                        ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                                
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Clients', 'action' => 'view', $client->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Clients', 'action' => 'edit', $client->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?php 
                                                            if(implode(', ', $client->locations) == ''){
                                                                echo $this->element('a_delete', ['id' => $client->id]);
                                                            }else{
                                                                echo $this->element('a_ndelete', ['id' => $client->id]);
                                                            }
                                                        ?>
                                                        <?php if($client->ativo == '1' or $client->ativo == 1){ ?>
                                                            <?= $this->element('a_deactivate', ['id' => $client->id.'i'])?>
                                                        <?php } else { ?>
                                                            <?= $this->element('a_activate', ['id' => $client->id.'add'])?>
                                                        <?php } ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>