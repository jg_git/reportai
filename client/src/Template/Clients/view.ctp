<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        phone_types = [];
        <?php foreach ($phone_types as $key => $type) { ?>
            phone_types["<?= $key ?>"] = "<?= $type ?>";
        <?php } ?>  

        $("#part1").load("<?= $this->Url->build(['action' => 'part1', $client->isNew() ? 'new' : $client->id, 'view',]) ?>");
        $("#part2").load("<?= $this->Url->build(['action' => 'part2', $client->isNew() ? 'new' : $client->id, 'view',]) ?>");
        $('#part1').prop('readonly', true);
      
        $("#email").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-email-area-" + anchor).html($("#form-email-" + anchor + " input[id=email]").val());
            $("#edit-email-" + anchor).hide();
            $("#view-email-" + anchor).show();
        });

        $("#phone").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-phone-area-" + anchor + " span[id=type]").html(phone_types[$("#form-phone-" + anchor + " input[name=tipo]:checked").val()]);
            $("#view-phone-area-" + anchor + " span[id=ddd]").html("(" + $("#form-phone-" + anchor + " input[id=ddd]").val() + ")");
            $("#view-phone-area-" + anchor + " span[id=phone]").html($("#form-phone-" + anchor + " input[id=telefone]").val());
            $("#edit-phone-" + anchor).hide();
            $("#view-phone-" + anchor).show();
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Clientes', 'url' => ['action' => 'index']],
    ['title' => 'Visualização do Cliente']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div id="part1"></div>
        <div class="row">
            <div class="col-md-3">
                <p class="mt-3">Emails Secundários</p>
                <table id="email" class="table table-hover">
                    <?php
                    if (isset($client->client_emails) and sizeof($client->client_emails) > 0) {
                        foreach ($client->client_emails as $email) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-email-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-email-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('email', '<?= $anchor ?>', <?= $email->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-email-<?= $anchor ?>">
                                <td>
                                    <span id="view-email-area-<?= $anchor ?>" style="font-size: 14px">
                                        <?= $email->email ?>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="email" data-anchor="<?= $anchor ?>" data-id="<?= $email->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr id="no-email">
                            <td class="no-data-found">Nenhum Email Cadastrado</td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr id="email-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="email"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-email']) ?>
                <?= $this->Form->control('delete-email-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-5">
                <p class="mt-3">Telefones</p>
                <table id="phone" class="table table-hover">
                    <?php
                    if (isset($client->client_phones) and sizeof($client->client_phones) > 0) {
                        foreach ($client->client_phones as $phone) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-phone-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-phone-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('phone', '<?= $anchor ?>', <?= $phone->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-phone-<?= $anchor ?>">
                                <td>
                                    <span id="view-phone-area-<?= $anchor ?>" style="font-size: 14px">
                                        <span class="mr-2" id="ddd">(<?= $phone->ddd ?>)</span>
                                        <span class="mr-2" id="phone"><?= $phone->telefone ?></span>
                                        <span class="mr-2" id="type"><?= $phone_types[$phone->tipo] ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="phone" data-anchor="<?= $anchor ?>" data-id="<?= $phone->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr id="no-phone">
                            <td class="no-data-found">Nenhum Telefone Cadastrado</td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr id="phone-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="phone"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-phone']) ?>
                <?= $this->Form->control('delete-phone-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div id="part2"></div>
        <div class="row mt-4">
            <div class="col">
                <?= $this->element('button_return') ?>

            </div>
        </div>
    </div>
</div>