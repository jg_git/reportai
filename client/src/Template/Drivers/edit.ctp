<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        $("input[name$=vencimento]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
        $("#cpf").inputmask("999.999.999-99");
        $("#rg").inputmask("99.999.999-9|X");
        $("input[id^=celular]").inputmask("(99) 99999-9999");
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Motoristas', 'url' => ['action' => 'index']],
    ['title' => 'Motorista']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <?=
                    $this->Form->create($driver, [
                        'id' => 'form-driver',
                        'data-id' => $driver->isNew() ? 'new' : $driver->id,
                        'data-error' => sizeof($driver->getErrors()) > 0 ? 'Y' : 'N'
                    ])
                ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <?= $this->Form->control('nome') ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('cpf', ['label' => 'CPF']) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('rg', ['label' => 'RG']) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('cnh', ['label' => 'CNH']) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('celular1', ['type' => 'text', 'label' => 'Celular 1']) ?>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('celular2', ['type' => 'text', 'label' => 'Celular 2']) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('celular3', ['type' => 'text', 'label' => 'Celular 3']) ?>
                    </div>

                </div>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_delete', ['id' => $driver->id]) ?>
                <?= $this->element('button_return') ?>
                <button class="btn btn-lg btn-primary nohover pull-left" onClick="redirectReport()">GERAR PDF</button>
                <script>
                    function redirectReport() {
                        //     window.open("/" + $(this).data("client-id") + "/report.pdf")
                        window.print();
                    }
                    //window.open(routing.find(x => x.key === route).route + "/report.pdf"); 
                </script>
            </div>
        </div>
    </div>
</div>