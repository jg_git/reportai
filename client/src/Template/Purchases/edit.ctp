<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        $("input[name=preco_litro]")
            .maskMoney({
                thousands: '.',
                decimal: ',',
                precision: 4
            })
            .maskMoney('mask', <?= $purchase->preco_litro ?>);
        $("input[name=volume_comprado]")
            .maskMoney({
                thousands: '.',
                decimal: ',',
                precision: 0
            })
            .maskMoney('mask', $(this).val());
        <?php
        if (isset($purchase->purchase_payments) and sizeof($purchase->purchase_payments) > 0) {
            foreach ($purchase->purchase_payments as $index => $payment) {
                ?>
                $("input[id=purchase-payments-<?= $index ?>-volume-provisionado]")
                    .maskMoney({
                        thousands: '.',
                        decimal: ',',
                        precision: 0
                    })
                    .maskMoney('mask', $(this).val());
        <?php
            }
        }
        ?>
        updateTotals();

        $("input[name*=data]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });

        $("input[name=corretada]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function(e) {
            $("select[name=broker_id]").prop("disabled", false);
            $("#commision").html(formatMoney(parseInt($("input[name=volume_comprado]").val().replace(/\./g, "").replace(",", ".")) * 0.003));
        }).on('ifUnchecked', function(e) {
            $("select[name=broker_id]").prop("disabled", true);
            $("#commision").html("0,00");
        });

        $("input[name=volume_comprado]").change(function() {
            volume = parseInt($(this).val().replace(/\./g, ""));
            preco = parseFloat($("input[name=preco_litro]").val().replace(/\./g, "").replace(",", "."));
            $("#comprado-volume").html(formatMoney(volume));
            $("#comprado-valor").html("R$ " + formatMoney(volume * preco, 2));
        });

        $("input[name=preco_litro]").change(function() {
            volume = parseInt($("input[name=volume_comprado]").val().replace(/\./g, "").replace(",", "."));
            preco = parseFloat($(this).val().replace(/\./g, "").replace(",", "."));
            $("#comprado-volume").html(formatMoney(volume));
            $("#comprado-valor").html("R$ " + formatMoney(volume * preco, 2));
        });

        $(".fa-plus-square").click(function() {
            anchor = anchorize();
            $("#no-payment").remove();
            index = $("#index").val();
            $.get("<?= $this->Url->build(['controller' => 'Purchases', 'action' => 'payment']) ?>/" + index + "/" + anchor,
                function(data) {
                    $("#payment-add").before(data);
                    $("input[id=purchase-payments-" + index + "-volume-provisionado]")
                        .maskMoney({
                            thousands: '.',
                            decimal: ',',
                            precision: 0
                        });
                    $("#edit-payment-" + anchor + " input[name*=data]").datepicker({
                        format: "dd/mm/yyyy",
                        todayBtn: true,
                        language: "pt-BR"
                    });
                    index++;
                    $("#index").val(index);
                });
        });

        $("#payments").on("change", "input[name*=volume_provisionado]", function() {
            index = $(this).data("index");
            volume = parseFloat($(this).val().replace(/\./g, "").replace(",", "."));
            preco_litro = Math.round(parseFloat($("input[name=preco_litro]").val().replace(/\./g, "").replace(",", ".")) * 10000) / 10000;
            valor = Math.round(volume * preco_litro * 100) / 100;
            $("#parcela-" + index).html(formatMoney(valor));
            $("#purchase-payments-" + index + "-valor-provisionado").val(valor);
            updateTotals();
        });

        <?php
        if (isset($purchase->purchase_payments) and sizeof($purchase->purchase_payments) > 0) {
            foreach ($purchase->purchase_payments as $index => $payment) {
                if (!is_null($payment->payable) and $payment->payable->baixa) {
                    ?>
                    $("tr[data-index=<?= $index ?>]").addClass("bg-secondary");
                    $("#purchase-payments-<?= $index ?>-data-vencimento").datepicker("destroy");
                    $("tr[data-index=<?= $index ?>]").find("input").prop("readOnly", true);
                    $("tr[data-index=<?= $index ?>]").find(".fa-trash").remove();
        <?php
                }
            }
        }
        ?>
        $("input[name=volume_devolver]")
            .maskMoney({
                thousands: '.',
                decimal: ',',
                precision: 0
            })
            .maskMoney('mask', $(this).val());

        $("#devolver").click(function() {
            $.post("<?= $this->Url->build(['controller' => 'Purchases', 'action' => 'devolucao']) ?>",
                $("#form-devolucao").serialize(),
                function(data, code) {
                    window.location = "<?= $this->Url->build(['action' => 'edit', $purchase->id]) ?>";
                    $(".modal").modal('hide');
                });
        });

        $("table[id=payments]").on("click", ".fa-trash", function() {
            anchor = $(this).data("anchor");
            $("#delete-payment-list").append("<option value='" + $(this).data("payment-id") + "' selected></option>");
            $("#edit-payment-" + anchor).remove();
            updateTotals();
        });

    });

    function updateTotals() {
        volume_provisionado = 0;
        $("input[name*=volume_provisionado]").each(function() {
            volume_provisionado += parseFloat($(this).val().replace(/\./g, "").replace(",", "."));
        });
        $("#volume_provisionado").html(formatMoney(volume_provisionado, 0));

        valor_provisionado = 0;
        $("input[name*=valor_provisionado]").each(function() {
            valor_provisionado += parseFloat($(this).val());
        });
        $("#valor_provisionado").html("R$ " + formatMoney(valor_provisionado));

        price = $("input[name=preco_litro]").val().replace(/\./g, "").replace(",", ".");

        <?php if ($purchase->isNew()) { ?>
            volume_compra = parseInt($("input[name=volume_comprado]").val().replace(/\./g, "").replace(",", "."));
            valor_compra = volume_compra * price;
        <?php } else { ?>
            volume_compra = <?= $purchase->volume_comprado - $purchase->volume_devolvido ?>;
            valor_compra = volume_compra * <?= $purchase->preco_litro ?>;
        <?php } ?>

        $("#volume_diferenca").html(formatMoney(volume_provisionado - volume_compra));
        $("#valor_diferenca").html("R$ " + formatMoney(valor_provisionado - valor_compra));
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Compras', 'url' => ['action' => 'index']],
    ['title' => 'Compra']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <?=
                    $this->Form->create($purchase, [
                        'id' => 'form-purchase',
                        'data-id' => $purchase->isNew() ? 'new' : $purchase->id,
                        'data-error' => sizeof($purchase->getErrors()) > 0 ? 'Y' : 'N'
                    ])
                ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?= $this->Form->control('numero_contrato', ['type' => 'text', 'label' => 'Número do Contrato', 'disabled' => !$purchase->isNew()]) ?>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if (!$purchase->isNew()) {
                                    echo $this->Form->control('data_compra', ['type' => 'text', 'label' => 'Data da Compra', 'disabled' => !$purchase->isNew()]);
                                } else {
                                    echo $this->Form->control('data_compra', ['type' => 'text', 'label' => 'Data da Compra', 'value' => date('d/m/Y')]);
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <?= $this->Form->control('plant_id', ['type' => 'select', 'options' => $plants, 'label' => 'Usina', 'disabled' => !$purchase->isNew()]) ?>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <?= $this->Form->control('distributor_id', ['type' => 'select', 'options' => $distributors, 'label' => 'Distribuidora', 'disabled' => !$purchase->isNew()]) ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 col-sm-12 mt-4">
                                <?= $this->Form->control('corretada', ['type' => 'checkbox', 'label' => 'Corretada', 'disabled' => !$purchase->isNew()]) ?>
                            </div>
                            <div class="col-md-9 col-sm-12">
                                <?=
                                    $this->Form->control('broker_id', [
                                        'type' => 'select',
                                        'options' => $brokers,
                                        'label' => 'Corretora',
                                        'empty' => true,
                                        'disabled' => !$purchase->corretada or !$purchase->isNew()
                                    ])
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                                <?= $this->Form->control('volume_comprado', ['type' => 'text', 'label' => 'Volume (Litros)', 'disabled' => !$purchase->isNew()]) ?>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <?= $this->Form->control('preco_litro', ['type' => 'text', 'label' => 'Preço/Litro', 'disabled' => !$purchase->isNew()]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="row mb-3">
                            <?php $volume = $purchase->volume_comprado - $purchase->volume_devolvido ?>
                            <div class="col-md-3 col-sm-12">
                                <label>Volume Comprado</label>
                                <div id="comprado-volume" style="font-size: large; font-weight: bold"><?= number_format($volume, 0, ',', '.') ?></div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label>Valor Financeiro</label>
                                <div id="comprado-valor" style="font-size: large; font-weight: bold">R$ <?= number_format($volume * $purchase->preco_litro, 2, ',', '.') ?></div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-3 col-sm-12">
                                <label>Volume Provisionado</label>
                                <div id="volume_provisionado" style="font-size: large; font-weight: bold">
                                    <?= number_format(0, 2, ',', '.') ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label>Diferença</label>
                                <div id="volume_diferenca" style="font-size: large; font-weight: bold">
                                    <?= number_format(0, 2, ',', '.') ?>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label>Valor Provisionado</label>
                                <div id="valor_provisionado" style="font-size: large; font-weight: bold">R$ <?= number_format(0, 2, ',', '.') ?></div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <label>Diferença</label>
                                <div id="valor_diferenca" style="font-size: large; font-weight: bold">R$ <?= number_format(0, 2, ',', '.') ?></div>
                            </div>
                        </div>
                        <div class="row">
                            <table id="payments" class="table table-hover slim-row">
                                <tr>
                                    <th>Volume Provisionado</th>
                                    <th>Valor Provisionado</th>
                                    <th>Data Vencimento</th>
                                    <th></th>
                                </tr>
                                <?php
                                if (isset($purchase->purchase_payments) and sizeof($purchase->purchase_payments) > 0) {
                                    foreach ($purchase->purchase_payments as $index => $payment) {
                                        $anchor = rand();
                                        ?>
                                        <tr id="edit-payment-<?= $anchor ?>" data-index="<?= $index ?>" data-anchor="<?= $anchor ?>">
                                            <?=
                                                        $this->Form->control('purchase_payments.' . $index . '.id', [
                                                            'type' => 'hidden',
                                                            'value' => $payment->id
                                                        ])
                                                    ?>
                                            <td>
                                                <?=
                                                            $this->Form->control('purchase_payments.' . $index . '.volume_provisionado', [
                                                                'type' => 'text',
                                                                'label' => false,
                                                                'data-index' => $index
                                                            ])
                                                        ?>
                                            </td>
                                            <td style="text-align: center">
                                                <div id="parcela-<?= $index ?>" class="standout">
                                                    <?= number_format($payment->valor_provisionado, 2, ',', '.') ?>
                                                </div>
                                                <?= $this->Form->control('purchase_payments.' . $index . '.valor_provisionado', ['type' => 'hidden']) ?>
                                            </td>
                                            <td>
                                                <?=
                                                            $this->Form->control('purchase_payments.' . $index . '.data_vencimento', [
                                                                'type' => 'text',
                                                                'value' => $payment->data_vencimento,
                                                                'label' => false
                                                            ])
                                                        ?>
                                            </td>
                                            <td>
                                                <i class="fa fa-trash pull-right" data-payment-id="<?= $payment->id ?>" data-anchor="<?= $anchor ?>"></i>
                                            </td>
                                        </tr>
                                    <?php
                                        }
                                    } else {
                                        ?>
                                    <tr id="no-payment">
                                        <td colspan="7" class="no-data-found">Nenhum registro encontrado</td>
                                    </tr>
                                <?php } ?>
                                <tr id="payment-add">
                                    <td colspan="7">
                                        <i class="fa fa-plus-square" data-type="payment"></i>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <?= $this->Form->control('delete-payment-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?php echo $this->element('button_save', ['id' => 'save']); ?>
                <?= $this->Form->end() ?>
                <?= $this->Form->control('index', ['type' => 'hidden', 'value' => isset($purchase->purchase_payments) ? sizeof($purchase->purchase_payments) : 0]) ?>
                <?= $this->element('button_delete', ['id' => $purchase->id]) ?>
                <?= $this->element('button_return') ?>
                <button class="btn btn-lg btn-primary nohover pull-left" onClick="redirectReport()">GERAR PDF</button>
                <script>
                    function redirectReport() {
                        //     window.open("/" + $(this).data("client-id") + "/report.pdf")
                        window.print();
                    }
                    //window.open(routing.find(x => x.key === route).route + "/report.pdf"); 
                </script>
            </div>
        </div>
        <?php if (!$purchase->isNew()) { ?>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="card mt-2">
                        <div class="card-body">
                            <?php $estoque = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito - $purchase->volume_vendido ?>
                            <table class="table table-hover extra-slim-row">
                                <tr>
                                    <td>(A) Volume Comprado</td>
                                    <td class="money"><?= number_format($purchase->volume_comprado, 0, ',', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>(B) Volume Transferido</td>
                                    <td class="money"> <?= number_format($purchase->volume_transferido, 0, ',', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>(C) Volume Devolvido</td>
                                    <td class="money"><?= number_format($purchase->volume_devolvido, 0, ',', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>(D) Volume em Trânsito</td>
                                    <td class="money"><?= number_format($purchase->volume_transito, 0, ',', '.') ?></td>
                                </tr>
                                <tr>
                                    <td>(E) Volume Vendido</td>
                                    <td class="money"><?= number_format($purchase->volume_vendido, 0, ',', '.') ?></td>
                                </tr>
                                <tr style="font-weight: bold">
                                    <td>Estoque Atual (Litros) (A + B - C - E - D)</td>
                                    <td class="money"><?= number_format($estoque, 0, ',', '.') ?></td>
                                </tr>
                                <tr style="font-weight: bold">
                                    <td>Valor Financeiro Estoque</td>
                                    <td class="money">R$ <?= number_format($estoque * ($purchase->preco_litro + $purchase->preco_litro_adicional), 2, ',', '.') ?></td>
                                </tr>
                            </table>
                            <?php if ($estoque > 0) { ?>
                                <button class="btn btn-lg btn-outline-dark" data-toggle="modal" data-target="#devolucao">Devolução</button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
<!--Devolucao de estoque-->
<div class="modal fade" id="devolucao" tabindex="-1" role="dialog" aria-labelledby="devolucaoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 50% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="devolucaoModalLabel">Devolução</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null, ['id' => 'form-devolucao']) ?>
                <?= $this->Form->control('purchase_id', ['type' => 'hidden', 'value' => $purchase->id]) ?>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <?= $this->Form->control('volume_devolver', ['type' => 'text', 'label' => 'Volume a Devolver', 'value' => $estoque]) ?>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <?= $this->Form->control('account_id', ['type' => 'select', 'options' => $accounts, 'label' => 'Conta Registro da Devolução']) ?>
                    </div>
                </div>
                <div class="text-danger" style="font-weight: bold">
                    Um lançamento no valor monetário desta devolução derá criado na selecionada conta acima.<br>
                    Os volumes e valores provisionados cujas contas a pagar não foram dadas baixa serão removidos.
                </div>
                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button id="devolver" type="button" class="btn btn-primary">Efetuar Devolução</button>
            </div>
        </div>
    </div>
</div>