<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function(e) {
            console.log($(this).val());
        });

        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };
        createDatePicker("data_compra", configRangePicker);

        $(".statement").click(function() {
            // $("#generate").attr('data-purchase-id', $(this).data("purchase-id"));
            $("#idPurchase").val($(this).data("purchase-id"));
            $.get("<?= $this->Url->build(['controller' => 'Purchases', 'action' => 'statement']) ?>/" + $(this).data("purchase-id"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=statement-area]").html(data);

                        $(".modal").modal('show');
                    }
                });
        });

    });

    function createDatePicker(name, configRangePicker) {
        $("input[name=" + name + "_range]").daterangepicker(configRangePicker, function(start, end, label) {

        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=" + name + "_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=" + name + "_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=" + name + "_from]").val("");
            $("input[name=" + name + "_to]").val("");
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Compras']
]);
echo $this->Breadcrumbs->render();
?>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                            <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                            <div class="row">
                                <div class="col-md-2 col-sm-12">
                                    <?= $this->Form->control('numero_contrato', ['type' => 'text', 'label' => 'Número do Contrato']) ?>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <?= $this->Form->control('plant_id', ['type' => 'select', 'options' => $plants, 'label' => 'Usina', 'empty' => true]) ?>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <?= $this->Form->control('data_compra_range', ['type' => 'text', 'label' => 'Data Compra']) ?>
                                    <?= $this->Form->control('data_compra_de', ['type' => 'hidden']) ?>
                                    <?= $this->Form->control('data_compra_ate', ['type' => 'hidden']) ?>
                                </div>
                                <div class="col-md-3 col-sm-12 mt-4">
                                    <?= $this->Form->control('in_stock', ['type' => 'checkbox', 'label' => 'Com estoque']) ?>
                                </div>
                            </div>
                            <div class="row mt-2 mb-2">
                                <div class="col">
                                    <?= $this->element('button_filter') ?>
                                    <?= $this->element('button_clear') ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover">
                    <tr>
                        <th style="text-align: center">Número Contrato</th>
                        <th>Usina</th>
                        <th class="money">Preço/Litro</th>
                        <th class="money">Estoque</th>
                        <th style="text-align: center">Data da Compra</th>
                        <th></th>
                        <th></th>
                    </tr>
                    <?php
                    if (sizeof($purchases) > 0) {
                        foreach ($purchases as $purchase) {
                            ?>
                            <tr>
                                <td style="text-align: center"><?= $this->Html->link($purchase->numero_contrato, ['controller' => 'Purchases', 'action' => 'edit', $purchase->id]) ?></td>
                                <td><?= $purchase->plant->razao_social ?></td>
                                <td class="money"><?= $this->Number->currency($purchase->preco_litro, null, ['places' => 4]) ?></td>
                                <td class="money"><?= number_format($purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_vendido, 0, ',', '.') ?></td>
                                <td style="text-align: center"><?= $purchase->data_compra ?></td>
                                <td>
                                    <?php
                                            if ($purchase->transito) {
                                                echo 'EM TRÂNSITO';
                                            }
                                            if ($purchase->transferido) {
                                                echo 'TRANSFERIDO';
                                            }
                                            ?>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-outline-primary statement" data-purchase-id="<?= $purchase->id ?>">Extrato</button>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {

        // var configRangePicker = {
        //     locale: {
        //         "format": "DD/MM/YYYY",
        //         "separator": " - ",
        //         "applyLabel": "Aplicar",
        //         "cancelLabel": "Limpar",
        //         "customRangeLabel": "Customizado",
        //         "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        //         "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        //     },
        //     autoUpdateInput: false,
        //     ranges: {
        //         'Hoje': [moment(), moment()],
        //         'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        //         'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
        //         'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
        //         'Este Mês': [moment().startOf('month'), moment().endOf('month')],
        //         'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        //     }
        // };

        // $("input[name=date_range]").daterangepicker(configRangePicker, function(start, end, label) {

        // }).on('apply.daterangepicker', function(ev, picker) {
        //     $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
        //     $("input[name=date_start]").val(picker.startDate.format("YYYY-MM-DD"));
        //     $("input[name=date_end]").val(picker.endDate.format("YYYY-MM-DD"));
        // }).on('cancel.daterangepicker', function(ev, picker) {
        //     $(this).val("");
        //     $("input[name=date_start]").val("");
        //     $("input[name=date_end]").val("");
        // });

        $("#generate").click(function() { //acessa controler expenses ação check para verificar se existe opção selecionada.
            window.open("<?= $this->Url->build(['action' => 'exportPdf', "TROCAR"]) ?>/exportPdf.pdf".replace("TROCAR", $("#idPurchase").val()),
                "_blank");
        });

    });
</script>
<input type="hidden" id="idPurchase">
<div class="modal fade" id="statement" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 60% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Extrato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="statement-area"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Fechar</button>
                <button button id="generate" type="button" class="btn btn-primary" data-purchase-id="">GERAR PDF</button>
            </div>
        </div>
    </div>
</div>