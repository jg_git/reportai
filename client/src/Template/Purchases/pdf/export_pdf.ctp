<style>
    @font-face {
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
        src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
    }

    body {
        font-family: Verdana;
        font-size: 10px;
        margin-left: 0.5cm;
        margin-right: 0.5cm;
    }
</style>
<?= $balance = NULL;
$balance = number_format($purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_vendido, 0, ',', '.');
?>
<div>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
    <h2>RELATORIO DO CONTRATO</h2>
    <h4>Número Contrato: <?= $purchase->numero_contrato ?></h4>

    </h5>
    <table class="table table-hover extra-slim-row">
        <tr>
            <th>Data</th>
            <th>Descrição</th>
            <th class="money">Volume</th>
        </tr>
        <?php

        if (sizeof($ledger) > 0) {
            foreach ($ledger as $entry) {
                ?>
                <tr>
                    <td><?= $entry['data'] ?></td>
                    <td><?= $entry['descricao'] ?></td>
                    <td class="money"><?= number_format($entry['volume'], 0, ',', '.') ?> <?= $entry['dc'] ?></td>
                </tr>
            <?php
                }
            } else {
                ?>
            <tr class="no-data-found">
                <td colspan="10">Nenhum registro encontrado</td>
            </tr>
        <?php } ?>
    </table>

    <div> TOTAL VOLUME:
        <span class="<?= $balance > 0 ? 'credit' : ($balance < 0 ? 'debit' : '') ?>">
            <?= $this->Number->precision($balance) ?> <?= $balance > 0 ? '+L' : ($balance < 0 ? '-L' : '') ?>
        </span>
    </div>
</div>