<tr id="edit-payment-<?= $anchor ?>" data-index="<?= $index ?>" data-anchor="<?= $anchor ?>">
    <td><?= $this->Form->control('purchase_payments.' . $index . '.volume_provisionado', ['type' => 'text', 'label' => false, 'data-index' => $index]) ?></td>
    <td style="text-align: center">
        <div id="parcela-<?= $index ?>" class="standout"><?= number_format(0, 2, ',', '.') ?></div>
        <?= $this->Form->control('purchase_payments.' . $index . '.valor_provisionado', ['type' => 'hidden']) ?>
    </td>
    <td><?= $this->Form->control('purchase_payments.' . $index . '.data_vencimento', ['type' => 'text', 'label' => false]) ?></td>
    <td><i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('<?= $anchor ?>')"></i></td>
</tr>