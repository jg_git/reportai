<script>
    $(document).ready(function() {
        $("select[name=broker_id]").change(function() {
            $.get("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'brokerPurchases']) ?>/" + $(this).val(),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=purchases]").html(data);
                    }
                });
        });
    });
</script>

<?php

switch ($tipo) {
    case 'CC':
        echo $this->Form->control('broker_id', ['type' => 'select', 'options' => $options, 'label' => 'Corretora', 'empty' => true, 'value' => $default]);
        break;
    case 'CT':
        echo $this->Form->control('broker_transfer_id', ['type' => 'select', 'options' => $options, 'label' => 'Corretora', 'empty' => true, 'value' => $default]);
        break;
    case 'SP':
        echo $this->Form->control('distributor_id', ['type' => 'select', 'options' => $options, 'label' => 'Distribuidora', 'empty' => true, 'value' => $default]);
        break;
    case 'PC':
        echo $this->Form->control('plant_id', ['type' => 'select', 'options' => $options, 'label' => 'Usina', 'empty' => true, 'value' => $default]);
        break;
    case 'FR':
        echo $this->Form->control('carrier_id', ['type' => 'select', 'options' => $options, 'label' => 'Transportadora', 'empty' => true, 'value' => $default]);
        break;
    case 'FT':
        echo $this->Form->control('carrier_transfer_id', ['type' => 'select', 'options' => $options, 'label' => 'Transportadora', 'empty' => true, 'value' => $default]);
        break;
    case 'CV':
        echo $this->Form->control('seller_id', ['type' => 'select', 'options' => $options, 'label' => 'Vendedor', 'empty' => true, 'value' => $default]);
        break;
    default:
        echo '';
}
