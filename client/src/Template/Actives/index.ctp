<script>
    $(document).ready(function () {
        $("button").click(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
        $("button").focus(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Ativos']
]);
echo $this->Breadcrumbs->render();
?>
<?php 
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('nome_ativo', ['label' => 'Nome']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('client_id', ['label' => 'Cliente', 'options' => $clients2, 'empty' => true]) ?>
                        </div>
                        
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('location_id', ['label' => 'Local', 'options' => $locations, 'empty' => true]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('brand_id', ['label' => 'Marca / Fabricante', 'options' => $brands, 'empty' => true]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('brand_model_id', ['label' => 'Modelo', 'options' => $brandmodels, 'empty' => true]) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('ativo', ['type' => 'select', 'options' =>['1' => 'Ativo', '0' => 'Inativo'], 'label' => 'Situação do Ativo']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('patrimonio', ['label' => 'Patrimônio/ Número de Série']) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('userid', ['type' => 'select', 'options' => $users2, 'empty' => 'SELECIONE UM USUÁRIO', 'label' => 'Usuário Responsável']) ?>
                            <?= $this->Form->control('user_id', ['type' => 'hidden', 'label' => false]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear_client') ?>
                        </div>

                    </div>

                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', []) ?> 
                    </div>
                </div>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('Actives.nome_ativo', 'Nome do Ativo') ?></th>
                        <th><?= $this->Paginator->sort('Actives.patrimonio', 'Patrimônio / Número de Série') ?></th>
                        <th><?= $this->Paginator->sort('Locations.nome_local', 'Local') ?></th>
                        <th><?= $this->Paginator->sort('Clients.razao_social', 'Cliente') ?></th>
                        <th><?= $this->Paginator->sort('Brands.nome_marca', 'Marca / Fabricante') ?></th>
                        <th><?= $this->Paginator->sort('brandModels.nome_modelo', 'Modelo') ?></th>
                        <th>Usuários Responsáveis</th>
                        <th>Ações</th>
                    </tr>
                    <?php

                    $id_useractives = array();
                    
                    foreach($user_actives as $useract){
                        $data= '['.implode(' ,', [ 'user_id' => $useract->user_id,
                                    'active_id' => $useract->active_id
                                ]).']';
                        array_push($id_useractives, $data);
                    }

                    $contagem=0;
                    if (sizeof($actives) > 0) {
                        foreach ($actives as $active) {
                            
                            $user_id = $this->request->session()->read('Auth.User.id');
                            $active_id = $active->id;
                            $micro_data = '['.implode(' ,', [ 'user_id' => $user_id,
                                    'active_id' => $active_id
                                ]).']';
                            if((in_array($micro_data, $id_useractives)) OR (($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id') AND $this->request->session()->read('Auth.User.prop_id') == $active->prop_id))){
                                $contagem=$contagem+1;
                    ?>
                            <tr>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->nome_ativo ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->patrimonio ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->location->nome_local ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->client->razao_social ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->brand->nome_marca ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->brand_model->nome_modelo ?></div></nav></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-user-circle"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?php 
                                                            foreach($user_actives as $useract_hov){
                                                                if($useract_hov->active_id == $active_id){
                                                                    $nome_proprio="";
                                                                    foreach($users as $user){
                                                                        if ($user->id == $useract_hov->user_id){
                                                                            $nome_proprio=$user->nome;
                                                                        }
                                                                    }
                                                                    echo '<a class="dropdown-item">';
                                                                        echo '<span>'. $nome_proprio.'</span>';
                                                                    echo '</a>';
                                                                }
                                                            }
                                                        ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>

                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Actives', 'action' => 'view', $active->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Actives', 'action' => 'edit', $active->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?php 
                                                            echo $this->element('a_delete', ['id' => $active->id]);
                                                        ?>
                                                        <?php if($active->ativo == "1" OR $active->ativo == 1){ ?>
                                                        <?= $this->element('a_deactivate', ['id' => $active->id.'i'])?>
                                                        <?php } ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                            <?php
                            }
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>