<script>
    
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Downloads', 'url' => ['action' => 'index?ativo=1']],
    ['title' => 'Downloads']
]);
echo $this->Breadcrumbs->render();
?>
<?php 
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
        
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('Actives.nome_ativo', 'Nome do Ativo') ?></th>
                        <th><?= $this->Paginator->sort('Actives.patrimonio', 'Patrimônio / Número de Série') ?></th>
                        <th><?= $this->Paginator->sort('Locations.nome_local', 'Local') ?></th>
                        <th><?= $this->Paginator->sort('Clients.razao_social', 'Cliente') ?></th>
                        <th><?= $this->Paginator->sort('Brands.nome_marca', 'Marca / Fabricante') ?></th>
                        <th><?= $this->Paginator->sort('brandModels.nome_modelo', 'Modelo') ?></th>
                        <!-- <th>Usuários Responsáveis</th> -->
                        <th>             </th>
                    </tr>
                    
                    <tr>
                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->nome_ativo ?></div></nav></td>
                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->patrimonio ?></div></nav></td>
                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->location->nome_local ?></div></nav></td>
                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->client->razao_social ?></div></nav></td>
                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->brand->nome_marca ?></div></nav></td>
                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $active->brand_model->nome_modelo ?></div></nav></td>
                        <td> 
                        <?= $this->element('button_arquive_download') ?>
                        </td>
                    </tr>

                </table>
            </div>
        </div>
            <?= $this->element('button_return_client') ?>
    </div>
</div>