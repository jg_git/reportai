<script>
    
    $(document).ready(function () {
        
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        $('#check-all').on('ifUnchecked', function (event) {
            $('.icheck-flat input').iCheck('uncheck');
        });

        $('#check-all').on('ifChecked', function (event) {
            $('.icheck-flat input').iCheck('check');
        });
        if(!$("#brand-id").val() && !$("#brandnome").val()){
            $("#brand-model-id").hide();
            $("#areadois").hide();
        }else{
            $("#brand-model-id").show();
            $("#areadois").show();
        }
        let input = document.getElementById('brand-id')
        let input2 = document.getElementById('brand-model-id')
        let great = document.getElementById('save')

        $("#brand-id").focusout(function () {
            if(!$("#brand-id").val() && !$("#brandnome").val()){
                $("#brand-model-id").hide();
                $("#areadois").hide();
            }else{
                $("#brand-model-id").show();
                $("#areadois").show();
            }
        });

        input.addEventListener('input', function(evt) {
            let selector = document.querySelector('option[value="'+this.value+'"]')
            if ( selector ) {
                great.addEventListener('focus', function(evt) {
                    if(!selector.getAttribute('data-value')){
                        var marca = $('#brand-id').val();
                        $('#nomebrand').val(marca);
                    } else {
                        input.value = selector.getAttribute('data-value');
                    }
                }, false)
            }
        }, false);

        

        input2.addEventListener('input', function(evt) {
            let selector2 = document.querySelector('option[value="'+this.value+'"]')
            if ( selector2 ) {
                great.addEventListener('focus', function(evt) {
                    if(!selector2.getAttribute('data-value')){
                        var modelo = $('#brand-model-id').val();
                        $('#nomebrandmodel').val(modelo);
                    } else {
                        input2.value = selector2.getAttribute('data-value');
                    }
                }, false)
            }
        }, false);

        $("#brand-id").focusout(function () {
            var brandnome = $("#brand-id").val();
            $("#nomebrand").val(brandnome);
            
        });

        $("#brand-model-id").focusout(function () {
            var modelbrandnome = $("#brand-model-id").val();
            $("#nomebrandmodel").val(modelbrandnome);
        });
        
        $("#save").focus(function(){
            var modelbrandnome = $("#brand-model-id").val();
            $("#nomebrandmodel").val(modelbrandnome);

            var brandnome = $("#brand-id").val();
            $("#nomebrand").val(brandnome);
        });

    });
    $('#check-all').on('ifUnchecked', function (event) {
            $('.icheck-flat input').iCheck('uncheck');
        });

        $('#check-all').on('ifChecked', function (event) {
            $('.icheck-flat input').iCheck('check');
        });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Ativos', 'url' => ['action' => 'index']],
    ['title' => 'Cadastro de Ativos']
]);
echo $this->Breadcrumbs->render();
?>
<?php 
?>
<?= $this->Flash->render() ?>
        <div class="card">
            <div class="card-body">
                <?=
                    $this->Form->create($active, [
                        'enctype' => 'multipart/form-data',
                        'id' => 'form-active',
                        'data-id' => $active->isNew() ? 'new' : $active->id,
                        'data-error' => sizeof($active->getErrors()) > 0 ? 'Y' : 'N'])
                ?>
                <div class="row">
                        <div class="col-md-3 col-sm-12">
                        <?php 
                            $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                            if(strpos($checagem, "view")==true){
                                echo $this->Form->control('nome_ativo', ['type' => 'text', 'label' => 'Nome do Ativo', 'disabled' => true]);
                            }else{
                                echo '<div style="margin-bottom: .5rem">';
                                echo '<label for="nome_ativo" style="display: inline; margin-bottom: .5rem">Nome do Ativo</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                                echo '</div>';
                                echo $this->Form->control('nome_ativo', ['type' => 'text', 'label' => false ]);
                            }
                        ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                        <?php 
                            
                            if(strpos($checagem, "view")==true){
                                    echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'label' => 'Cliente Proprietário', 'disabled' => true]);
                                }else{
                                    echo '<div style="margin-bottom: .5rem">';
                                    echo '<label for="client_id" style="display: inline; margin-bottom: .5rem">Cliente Proprietário</label>';
                                    echo '</div>';
                                    echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'empty' => 'SELECIONE UM CLIENTE', 'label' => false]);
                                }
                            ?>
                        </div>

                        <div class="col-md-3 col-sm-12">
                        <?php 
                            if(strpos($checagem, "view")==true){
                                    echo $this->Form->control('location_id', ['type' => 'select', 'options' => $locations, 'label' => 'Local', 'disabled' => true]);
                                }else{
                                    echo '<div style="margin-bottom: .5rem">';
                                    echo '<label for="location_id" style="display: inline; margin-bottom: .5rem">Local</label>';
                                    echo '</div>';
                                    echo $this->Form->control('location_id', ['type' => 'select', 'options' => $locations, 'empty' => 'SELECIONE UM LOCAL', 'label' => false]);
                                }
                            ?>
                        </div>
                        
                        
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?php
                            
                            if(strpos($checagem, "view")==true){
                                echo $this->Form->control('patrimonio', ['type' => 'text', 'label' => 'Patrimônio / Número de Série', 'disabled' => true]);
                            }else{
                                echo '<div style="margin-bottom: .5rem">';
                                echo '<label for="patrimonio" style="display: inline; margin-bottom: .5rem">Patrimônio / Número de Série</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                                echo '</div>';
                                echo $this->Form->control('patrimonio', ['type' => 'text', 'label' => false ]);
                            }
                        ?>
                    </div>

                    <div class="col-md-3 col-sm-12">
                        <?php 
                            if(strpos($checagem, "view")==true){
                                echo $this->Form->control('brand_id', ['type' => 'select', 'options' => $selbrands, 'label' => 'Marca / Fabricante', 'disabled' => true]);
                            }else{
                                echo '<div style="margin-bottom: .5rem">';
                                echo '<label for="brand_id" style="display: inline; margin-bottom: .5rem">Marca / Fabricante</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                                echo '</div>';
                                echo '<datalist id="valuesList1">';
                                $nome_marca="";
                                foreach($selbrands as $id => $nome){
                                    echo '<option data-value="'.$id.'" value="'.$nome.'">';
                                    if ($id==$active->brand_id){
                                        $nome_marca=$nome;
                                    }
                                }
                                echo '</datalist>';
                                echo $this->Form->control('brand_id', ['type' => 'text', 'value' => $nome_marca,'list' => 'valuesList1', 'label' => false]);
                                echo $this->Form->control('nomebrand', ['type' => 'hidden', 'label' => false]);
                            }
                        ?>
                    </div>

                    <div id ="areadois" class="col-md-3 col-sm-12">
                        <?php 
                            if(strpos($checagem, "view")==true){
                                echo $this->Form->control('brand_model_id', ['type' => 'select', 'options' => $selbrandmodels, 'label' => 'Modelo', 'disabled' => true]);
                            }else{
                                echo '<div style="margin-bottom: .5rem">';
                                echo '<label for="brand_model_id" style="display: inline; margin-bottom: .5rem">Modelo</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                                echo '</div>';
                                echo '<datalist id="valuesList2">';
                                $nome_modelo="";
                                foreach($selbrandmodels as $id_dois => $nome_dois){
                                    echo '<option data-value="'.$id_dois.'" value="'.$nome_dois.'">';
                                    if ($id_dois==$active->brand_model_id){
                                        $nome_modelo=$nome_dois;
                                    }
                                }
                                echo '</datalist>';
                                echo $this->Form->control('brand_model_id', ['type' => 'text', 'value' => $nome_modelo, 'list' => 'valuesList2', 'label' => false]);
                                echo $this->Form->control('nomebrandmodel', ['type' => 'hidden', 'label' => false]);
                            }
                        ?>
                    </div>
                    <div calss="col-md-3 col-sm-12" style="margin-left:15px">
                        <label>Arquivo</label>
                        <?= $this->Form->file('arquivo', ['class' => 'form-control','label' => false]) ?>
                    </div> 
                    <div class="col-md-3 col-sm-3" style="margin-top:30px">
                        <?= $this->element('button_download')?>
                    </div> 
                </div>            
                    
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?php
                            if(strpos($checagem, "view")==true){
                                echo $this->Form->control('info_add', ['type' => 'textarea', 'label' => 'Informaçoes Adicionais', 'disabled' => true]);
                            }else{
                                echo $this->Form->control('info_add', ['type' => 'textarea', 'label' => 'Informaçoes Adicionais']);
                            }
                        ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?php
                            $caminho = 'https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl='.$checagem.'&choe=UTF-8';
                            if(strpos($checagem, "new")==true){
                                
                            }else{
                                echo '<a download="qrCode.png" href="'.$caminho.'" title="ImageName">
                                    <img alt="ImageName" src="'.$caminho.'">
                                </a>';
                            }
                        ?>
                    </div>
                          
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-12">
                    <?php
                        if(strpos($checagem, "view")==true){
                            echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label>';
                            $usuariospresentes = array();
                            foreach($useract as $user_unico){
                                if ($active->id == $user_unico['active_id']){
                                    array_push($usuariospresentes, $user_unico['user_id']);
                                }
                            }
                            foreach($users as $id => $nome){
                                if(in_array($id, $usuariospresentes)){
                                    echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true, 'disabled' => true])."</div>";
                                }
                                else{
                                    echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'disabled' => true])."</div>";
                                }
                            }
                        ?>
                        <div style="margin-bottom: .5rem">
                        </div>
                        <?php } else {  ?>
                        <div style="margin-bottom: .5rem">
                            <?php
                                echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label>';
                                echo "<div>". $this->Form->control('check-all', ['type' => 'checkbox', 'value' => 'todos', 'label' => 'Selecionar Todos'])."</div>";
                                $usuariospresentes = array();
                                foreach($useract as $user_unico){
                                    if ($active->id == $user_unico['active_id']){
                                        array_push($usuariospresentes, $user_unico['user_id']);
                                    }
                                }
                                foreach($users as $id => $nome){
                                    if(in_array($id, $usuariospresentes)){
                                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true])."</div>";
                                    }
                                    else{
                                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome])."</div>";
                                    }
                                }
                            ?>
                    </div>
                            <?php } 
                            ?>
                </div>
            </div>
                         
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_return_client') ?>
    </div>
</div>