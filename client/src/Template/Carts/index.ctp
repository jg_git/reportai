<script>
    $(document).ready(function () {

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Veículos']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8 col-sm-12">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <?= $this->Form->control('marca_modelo', ['label' => 'Marca/Modelo']) ?>
                        </div>
                        <div class="col-md-8 col-sm-12 mt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th>Marca/Modelo</th>
                        <th>Placa</th>
                        <th>Bocas</th>
                    </tr>
                    <?php
                    if (sizeof($carts) > 0) {
                        foreach ($carts as $cart) {
                            ?>
                            <tr>
                                <td><?= $this->Html->link($cart->marca_modelo, ['action' => 'edit', $cart->id]) ?></td>
                                <td><?= $cart->placa ?></td>
                                <td><?= sizeof($cart->cart_orifices) ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    