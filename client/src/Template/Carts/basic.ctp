<script>
    $(document).ready(function() {

        $("input[data-date]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR"
        });

        $("input[name=quantidade_litros]").maskMoney({
            thousands: '.',
            decimal: ',',
            precision: 0
        }).maskMoney('mask', <?= $cart->quantidade_litros / 1000 ?>);

    });
</script>

<?= $this->Flash->render() ?>
<?=
    $this->Form->create($cart, [
        'id' => 'form-basic',
        'data-id' => $cart->isNew() ? 'new' : $cart->id,
        'data-error' => sizeof($cart->getErrors()) > 0 ? 'Y' : 'N'
    ])
?>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('marca_modelo', ['type' => 'text', 'label' => 'Marca/Modelo']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('placa', ['type' => 'text', 'label' => 'Placa']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('ano', ['type' => 'text', 'label' => 'Ano Fabricação/Ano Modelo']) ?>
    </div>

</div>

</div>

<div class="row">
    <div class="col-md-2 col-sm-12">
        <?= $this->Form->control('quantidade_litros', ['type' => 'text', 'label' => 'Quantidade de Litros']) ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <?=
            $this->Form->control('quantidade_bocas', [
                'type' => 'select',
                'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
                'label' => 'Quantidade de Bocas',
                'empty' => true
            ])
        ?>
    </div>
</div>
<?= $this->Form->end() ?>