<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        $("#valor").maskMoney({
            thousands: '.',
            decimal: ','
        }).maskMoney('mask', <?= $transfer->valor ?>);
        $("input[name=data_lancamento]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR"
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Transferências entre Contas', 'url' => ['action' => 'index']],
    ['title' => 'Transferência entre Contas']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <?=
                    $this->Form->create($transfer, [
                        'id' => 'form-transfer',
                        'data-id' => $transfer->isNew() ? 'new' : $transfer->id,
                        'data-error' => sizeof($transfer->getErrors()) > 0 ? 'Y' : 'N'
                    ])
                ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <?= $this->Form->control('conta_origem_id', ['type' => 'select', 'options' => $accounts, 'label' => 'Conta Origem', 'disabled' => !$transfer->isNew()]) ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <?= $this->Form->control('conta_destino_id', ['type' => 'select', 'options' => $accounts, 'label' => 'Conta Destino', 'disabled' => !$transfer->isNew()]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?php
                        if (!$transfer->isNew()) {
                            echo $this->Form->control('data_transferencia', ['type' => 'text', 'label' => 'Data da Trasferência', 'disabled' => !$transfer->isNew()]);
                        } else {
                            echo $this->Form->control('data_transferencia', ['type' => 'text', 'label' => 'Data da Trasferência', 'value' => date('d/m/Y'), 'disabled' => !$transfer->isNew()]);
                        }
                        ?>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <?= $this->Form->control('descricao', ['type' => 'text', 'label' => 'Descrição', 'disabled' => !$transfer->isNew()]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('payment_method_id', ['type' => 'select', 'options' => $payment_methods, 'label' => 'Forma de Pagamento', 'disabled' => !$transfer->isNew()]) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('valor', ['type' => 'text', 'label' => 'Valor', 'disabled' => !$transfer->isNew()]) ?>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <?= $this->Form->control('observacao', ['type' => 'textarea', 'label' => 'Observação', 'disabled' => !$transfer->isNew()]) ?>
                    </div>
                </div>
                <?php
                if ($transfer->isNew()) {
                    echo $this->element('button_save', ['id' => 'save']);
                }
                ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_delete', ['id' => $transfer->id]) ?>
                <?= $this->element('button_return') ?>

                <?= $this->element('button_new_transfer', []) ?>

                <button class="btn btn-lg btn-primary nohover pull-left" onClick="redirectReport()">GERAR PDF</button>
                <script>
                    function redirectReport() {
                        //     window.open("/" + $(this).data("client-id") + "/report.pdf")
                        window.print();
                    }
                    //window.open(routing.find(x => x.key === route).route + "/report.pdf"); 
                </script>
            </div>
        </div>
    </div>
</div>