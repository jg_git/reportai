<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {

        
        //  $("#p").hide(0);
        //    $("#paginator").hide(0)
        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        $("input[name=data_transferencia_range]").daterangepicker(configRangePicker, function(start, end, label) {

        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=data_transferencia_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=data_transferencia_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=data_transferencia_de]").val("");
            $("input[name=data_transferencia_ate]").val("");
        });

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Transferências entre Contas']
]);
echo $this->Breadcrumbs->render();
?>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                            <?= $this->Form->create(
                                'index',

                                [

                                    'type' => 'GET',
                                    'url' => [
                                        'controller' => 'Transfers',
                                        'action' => 'filter',
                                        //   'account_id' => $("input[name=account_id").val(),
                                        //  'ledger_entry_type_id' => $entries->ledger_entry_type,
                                    ]

                                ], ['valueSources' => 'query']) ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <?= $this->Form->control('conta_origem_id', ['label' => 'Conta Origem', 'options' => $accounts, 'empty' => true]) ?>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <?= $this->Form->control('conta_destino_id', ['label' => 'Conta Destino', 'options' => $accounts, 'empty' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12">
                                    <?= $this->Form->control('data_transferencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                                    <?= $this->Form->control('data_transferencia_de', ['type' => 'hidden']) ?>
                                    <?= $this->Form->control('data_transferencia_ate', ['type' => 'hidden']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <?= $this->element('button_filter') ?>
                                    <?= $this->element('button_clear') ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <?= $this->element('button_new', []) ?>
               
               
            </div>
        </div>
    </div>
</div>