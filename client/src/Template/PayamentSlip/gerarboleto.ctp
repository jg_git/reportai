<script>
    $(document).ready(function() {});
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Formas de Pagamento', 'url' => ['action' => 'index']],
    ['title' => 'Forma de Pagamento']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">

                <?= $this->element('button_return') ?>
            </div>
        </div>
    </div>
</div>