<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {

        $("#basic").load("<?= $this->Url->build(['action' => 'basic', $vehicle->isNew() ? 'new' : $vehicle->id]) ?>", function() {

            $("input[type=radio]").iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat',
                increaseArea: '20%'
            }).on('ifClicked', function(e) {
                if ($(this)[0]['name'] === 'tipo') {
                    switch ($(this).val()) {
                        case "T":
                        case "4":
                            $("div[data-truck-4eixo").show();
                            $("div[data-cavalo").hide();
                            break;
                        case "C":
                            $("div[data-truck-4eixo").hide();
                            $("div[data-cavalo").show();
                            break;
                    }
                }
            });

            $("input[type=checkbox]").iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat',
                increaseArea: '20%'
            }).on('ifChecked', function(e) {
                $("div[data-ipva").show();
            }).on('ifUnchecked', function(e) {
                $("div[data-ipva").hide();
            });

        });

        $("#orifice").on("click", ".fa-edit", function() {
            anchor = $(this).data("anchor");
            if ($("#edit-orifice-area-" + anchor + " form[id|=form-orifice]").length === 0) {
                $("#edit-orifice-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'orifice']) ?>/<?= $vehicle->isNew() ? 'new' : $vehicle->id ?>/" +
                    $(this).data("id") + "/" +
                    anchor,
                    function() {
                        $("#form-orifice-" + anchor + " input[name=quantidade_litros]").maskMoney({
                            thousands: '.',
                            decimal: ',',
                            precision: 0
                        });
                    });
            }
            $("#edit-orifice-" + anchor).show();
            $("#view-orifice-" + anchor).hide();
        });

        $("#orifice").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-orifice-area-" + anchor + " span[id=numero]").html($("#form-orifice-" + anchor + " input[id=numero]").val());
            $("#view-orifice-area-" + anchor + " span[id=quantidade-litros]").html(formatMoney($("#form-orifice-" + anchor + " input[id=quantidade-litros]").val(), 0));
            $("#edit-orifice-" + anchor).hide();
            $("#view-orifice-" + anchor).show();
        });

        //        $("#orifice-list").on('click', '.fa-plus-square', function () {
        //            anchor = anchorize();
        //            $("#no-orifice").remove();
        //            $("#orifice-add").before(
        //                    '<tr id="edit-orifice-' + anchor + '">' +
        //                    '<td>' +
        //                    '<div id="edit-orifice-area-' + anchor + '"></div>' +
        //                    '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'orifice\', \'' + anchor + '\', \'new\')"></i>' +
        //                    '<i class="fa fa-check-square pull-right mr-2" data-type="orifice" data-anchor="' + anchor + '"></i>' +
        //                    '</td>' +
        //                    '</tr>' +
        //                    '<tr id="view-orifice-' + anchor + '" style="display:none">' +
        //                    '<td>' +
        //                    '<span id="view-orifice-area-' + anchor + '" style="font-size: 14px">' +
        //                    'Número da Boca: <span class="mr-2" id="numero"></span>' +
        //                    'Quantidade de Litros: <span class="mr-2" id="quantidade-litros"></span>' +
        //                    '</span>' +
        //                    '<i class="fa fa-edit pull-right" data-type="orifice" data-anchor="' + anchor + '" data-id="new"></i>' +
        //                    '</td>' +
        //                    '</tr>'
        //                    );
        //            $("#edit-orifice-area-" + anchor).load(
        //                    "<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'orifice']) ?>/<?= $vehicle->isNew() ? 'new' : $vehicle->id ?>/" +
        //                    "new" + "/" +
        //                    anchor);
        //        });

        $("#cart-list").on('click', '.fa-plus-square', function() {
            anchor = anchorize();
            $("#no-cart").remove();
            $("#cart-add").before(
                '<tr id="edit-cart-' + anchor + '">' +
                '<td>' +
                '<div id="edit-cart-area-' + anchor + '"></div>' +
                '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'cart\', \'' + anchor + '\', \'new\')"></i>' +
                '</td>' +
                '</tr>' +
                '<tr id="view-cart-' + anchor + '" style="display:none">' +
                '<td>' +
                '<span id="view-cart-area-' + anchor + '" style="font-size: 14px">' +
                '</span>' +
                '<i class="fa fa-edit pull-right" data-type="cart" data-anchor="' + anchor + '" data-id="new"></i>' +
                '</td>' +
                '</tr>'
            );
            $("#edit-cart-area-" + anchor).load(
                "<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'cart']) ?>/<?= $vehicle->isNew() ? 'new' : $vehicle->id ?>/" +
                anchor);
        });

        $("#save").click(function() {
            HoldOn.open({
                theme: "sk-cube-grid"
            });
            var basic = $.Deferred();
            var orifice = $.Deferred();
            var cart = $.Deferred();
            vehicle_id = $("#form-basic").data("id");
            $.post("<?= $this->Url->build(['action' => 'basic']) ?>/" + vehicle_id,
                $("#form-basic").serialize(),
                function(data) {
                    $("#basic").html(data);

                    $("input[type=radio]").iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat',
                        increaseArea: '20%'
                    }).on('ifClicked', function(e) {
                        if ($(this)[0]['name'] === 'tipo') {
                            switch ($(this).val()) {
                                case "T":
                                case "4":
                                    $("div[data-truck-4eixo").show();
                                    $("div[data-cavalo").hide();
                                    break;
                                case "C":
                                    $("div[data-truck-4eixo").hide();
                                    $("div[data-cavalo").show();
                                    break;
                            }
                        }
                    });

                    $("input[type=checkbox]").iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat',
                        increaseArea: '20%'
                    }).on('ifChecked', function(e) {
                        $("div[data-ipva").show();
                    }).on('ifUnchecked', function(e) {
                        $("div[data-ipva").hide();
                    });


                    if ($("<div>" + data + "</div>").find("#form-basic").data("error") === "N") {
                        basic.resolve("OK");
                        vehicle_id = $("<div>" + data + "</div>").find("#form-basic").data("id");
                        if ($("input[name=tipo]:checked").val() !== "C") {
                            //======== Salvar bocas ======
                            var req_orifice = [];
                            $("form[id|=form-orifice]").each(function() {
                                anchor = $(this).data("anchor");
                                req_orifice.push($.post("<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'orifice']) ?>/" + vehicle_id + "/" + $(this).data("id") + "/" + anchor,
                                    $("#form-orifice-" + anchor).serialize()));
                            });
                            if (req_orifice.length > 0) {
                                $.when.apply(undefined, req_orifice).then(function() {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-orifice']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-orifice']").data("id");
                                        $("#edit-orifice-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('orifice', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-orifice-" + anchor).data("error") === "N") {} else {
                                            saved = false;
                                        }
                                    }
                                    orifice.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                orifice.resolve("OK");
                            }
                            if ($("#form-delete-orifice option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Plants', 'action' => 'orifice-delete']) ?>",
                                    $("#form-delete-orifice").serialize());
                            }
                            cart.resolve("OK");
                        } else {
                            //======== Salvar carretas ======
                            var req_cart = [];
                            $("form[id|=form-cart]").each(function() {
                                anchor = $(this).data("anchor");
                                req_cart.push($.post("<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'cart']) ?>/" + vehicle_id + "/" + anchor,
                                    $("#form-cart-" + anchor).serialize()));
                            });
                            if ($("#form-delete-cart option").length > 0) {
                                req_cart.push($.post("<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'cart-delete']) ?>",
                                    $("#form-delete-cart").serialize()));
                            }
                            if (req_cart.length > 0) {
                                $.when.apply(undefined, req_cart).then(function() {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|=form-cart]").data("anchor");
                                    }
                                    $("#cart-list").load("<?= $this->Url->build(['controller' => 'Vehicles', 'action' => 'cart-list']) ?>/" + vehicle_id);
                                    cart.resolve("OK");
                                });
                            } else {
                                cart.resolve("OK");
                            }
                            orifice.resolve("OK");
                        }
                    } else {
                        basic.resolve("NG");
                        cart.resolve("NG");
                        orifice.resolve("NG");
                    }
                });
            $.when(basic, cart, orifice).done(function(v1, v2, v3) {
                $("#save").attr("disabled", false);
                console.log("basic: " + v1 + " cart: " + v2 + " orifice: " + v3);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK") {
                    toast("Cadastro salvo com sucesso");
                }
            });

        });

        $("body").on("change", "select[name=quantidade_bocas]", function() {
            quantidade_bocas = parseInt($(this).val());
            bocas = $("tr[id|=view-orifice]").length;
            if (quantidade_bocas > bocas) {
                for (i = 1; i <= quantidade_bocas - bocas; i++) {
                    anchor = anchorize();
                    $("#no-orifice").remove();
                    $("#orifice").append(
                        '<tr id="edit-orifice-' + anchor + '">' +
                        '<td>' +
                        '<div id="edit-orifice-area-' + anchor + '"></div>' +
                        '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" data-orifice-id="new" onclick="remove(\'orifice\', \'' + anchor + '\', \'new\')"></i>' +
                        '<i class="fa fa-check-square pull-right mr-2" data-type="orifice" data-anchor="' + anchor + '"></i>' +
                        '</td>' +
                        '</tr>' +
                        '<tr id="view-orifice-' + anchor + '" style="display:none" data-anchor="' + anchor + '">' +
                        '<td>' +
                        '<span id="view-orifice-area-' + anchor + '" style="font-size: 14px">' +
                        'Número da Boca: <span class="mr-2" id="numero"></span>' +
                        'Quantidade de Litros: <span class="mr-2" id="quantidade-litros"></span>' +
                        '</span>' +
                        '<i class="fa fa-edit pull-right" data-type="orifice" data-anchor="' + anchor + '" data-id="new"></i>' +
                        '</td>' +
                        '</tr>'
                    );
                    $("#edit-orifice-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Carts', 'action' => 'orifice']) ?>/<?= $vehicle->isNew() ? 'new' : $vehicle->id ?>/" +
                        "new" + "/" +
                        anchor,
                        function() {
                            $("#form-orifice-" + anchor + " input[name=quantidade_litros]").maskMoney({
                                thousands: '.',
                                decimal: ',',
                                precision: 0
                            });
                        });
                }
            } else if (quantidade_bocas < bocas) {
                i = 1;
                $("tr[id|=view-orifice]").each(function() {
                    if (i++ > quantidade_bocas) {
                        anchor = $(this).data("anchor");
                        console.log(bocas + " " + quantidade_bocas + " " + anchor);
                        id = $("#trash-" + anchor).data("orifice-id");
                        if (id !== "new") {
                            $("#delete-orifice-list").append("<option value='" + id + "' selected></option>");
                        }
                        $("tr[id=edit-orifice-" + anchor + "]").remove();
                        $("tr[id=view-orifice-" + anchor + "]").remove();
                    }
                });
            }
        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Essa operação não poderá ser revertida',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                switch (type) {
                    case "orifice":
                        $("#edit-orifice-" + anchor).remove();
                        $("#view-orifice-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-orifice-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "cart":
                        $("#edit-cart-" + anchor).remove();
                        $("#view-cart-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-cart-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                }
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Veículos', 'url' => ['action' => 'index']],
    ['title' => 'Veículo']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div id="basic"></div>
        <div class="row">
            <div class="col-md-4 col-sm-12" data-truck-4eixo="yes" style="display:none">
                <h5 class="mt-3">Bocas</h5>
                <div id="orifice-list">
                    <table id="orifice" class="table table-hover">
                        <?php
                        if (isset($vehicle->vehicle_orifices) and sizeof($vehicle->vehicle_orifices) > 0) {
                            foreach ($vehicle->vehicle_orifices as $orifice) {
                                $anchor = rand();
                                ?>
                                <tr id="edit-orifice-<?= $anchor ?>" style="display: none">
                                    <td>
                                        <div id="edit-orifice-area-<?= $anchor ?>"></div>
                                        <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('orifice', '<?= $anchor ?>', <?= $orifice->id ?>)"></i>
                                        <i class="fa fa-check-square pull-right mr-2" data-type="orifice" data-anchor="<?= $anchor ?>"></i>
                                    </td>
                                </tr>
                                <tr id="view-orifice-<?= $anchor ?>">
                                    <td>
                                        <span id="view-orifice-area-<?= $anchor ?>" style="font-size: 14px">
                                            Número da Boca: <span class="mr-2" id="numero"><?= $orifice->numero ?></span>
                                            Quantidade de Litros: <span class="mr-2" id="quantidade-litros"><?= number_format($orifice->quantidade_litros, 0, ',', '.') ?></span>
                                        </span>
                                        <i class="fa fa-edit pull-right" data-type="orifice" data-anchor="<?= $anchor ?>" data-id="<?= $orifice->id ?>"></i>
                                    </td>
                                </tr>
                            <?php
                                }
                            } else {
                                ?>
                            <tr id="no-orifice">
                                <td class="no-data-found">Nenhuma Boca Cadastrada</td>
                            </tr>
                        <?php
                        }
                        ?>
                    </table>
                </div>
                <?= $this->Form->create(null, ['id' => 'form-delete-orifice']) ?>
                <?= $this->Form->control('delete-orifice-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-3 col-sm-12" data-cavalo="yes" style="display:none">
                <h5>Carretas</h5>
                <div id="cart-list">
                    <table id="cart" class="table table-hover">
                        <?php
                        if (isset($vehicle->carts) and sizeof($vehicle->carts) > 0) {
                            foreach ($vehicle->carts as $cart) {
                                $anchor = rand();
                                ?>
                                <tr id="view-cart-<?= $anchor ?>">
                                    <td>
                                        <span id="view-cart-area-<?= $anchor ?>" style="font-size: 14px">
                                            Placa: <?= $cart->placa ?>
                                        </span>
                                        <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('cart', '<?= $anchor ?>', <?= $cart->id ?>)"></i>
                                    </td>
                                </tr>
                            <?php
                                }
                            } else {
                                ?>
                            <tr id="no-cart">
                                <td class="no-data-found">Nenhuma Carreta Cadastrada</td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr id="cart-add">
                            <td>
                                <i class="fa fa-plus-square" data-type="cart"></i>
                            </td>
                        </tr>
                    </table>
                </div>
                <?= $this->Form->create(null, ['id' => 'form-delete-cart']) ?>
                <?= $this->Form->control('delete-cart-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->element('button_delete', ['id' => $vehicle->id]) ?>
                <?= $this->element('button_return') ?>

                <button class="btn btn-lg btn-primary nohover pull-left" onClick="redirectReport()">GERAR PDF</button>
                <script>
                    function redirectReport() {
                        //     window.open("/" + $(this).data("client-id") + "/report.pdf")
                        window.print();
                    }
                    //window.open(routing.find(x => x.key === route).route + "/report.pdf"); 
                </script>
            </div>
        </div>
    </div>
</div>

<?php
//dump($vehicle);
?>