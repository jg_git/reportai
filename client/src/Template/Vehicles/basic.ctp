<script>
    $(document).ready(function() {

        $("input[data-date]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });

        $("input[name=quantidade_litros]").maskMoney({
            thousands: '.',
            decimal: ',',
            precision: 0
        }).maskMoney('mask', <?= $vehicle->quantidade_litros / 1000 ?>);
        <?php
        switch ($vehicle->tipo) {
            case 'T':
            case '4':
                ?>
                $("div[data-truck-4eixo").show();
                $("div[data-cavalo").hide();
            <?php
                break;
            case 'C':
                ?>
                $("div[data-truck-4eixo").hide();
                $("div[data-cavalo").show();
                $("#bocas-area").hide();
            <?php
                break;
        }
        ?>
            $("#ipva1").maskMoney({
                thousands: '.',
                decimal: ','
            }).maskMoney('mask', <?= $vehicle->ipva1 ?>);
            $("#ipva2").maskMoney({
                thousands: '.',
                decimal: ','
            }).maskMoney('mask', <?= $vehicle->ipva2 ?>);
            $("#ipva3").maskMoney({
                thousands: '.',
                decimal: ','
            }).maskMoney('mask', <?= $vehicle->ipva3 ?>);

    });
</script>

<?= $this->Flash->render() ?>
<?=
    $this->Form->create($vehicle, [
        'id' => 'form-basic',
        'data-id' => $vehicle->isNew() ? 'new' : $vehicle->id,
        'data-error' => sizeof($vehicle->getErrors()) > 0 ? 'Y' : 'N'
    ])
?>
<div class="row">
    <div class="col-md-3 col-sm-12 mt-4 mb-3">
        <?= $this->Form->radio('tipo', $vehicle_types) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('marca_modelo', ['type' => 'text', 'label' => 'Marca/Modelo']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('placa', ['type' => 'text', 'label' => 'Placa']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('ano', ['type' => 'text', 'label' => 'Ano Fabricação/Ano Modelo']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12" data-truck-4eixo="yes" style="display:none">
        <?= $this->Form->control('marca_modelo_tanque', ['type' => 'text', 'label' => 'Marca/Modelo Tanque']) ?>
    </div>
    <div class="col-md-3 col-sm-12" data-truck-4eixo="yes" style="display:none">
        <?= $this->Form->control('ano_fabricacao_tanque', ['type' => 'text', 'label' => 'Ano de Fabricação Tanque']) ?>
    </div>
</div>



<div class="row">


    <div class="col-md-3 col-sm-12" data-cavalo="yes" style="display:none">
        <?= $this->Form->control('cronotacografo', ['type' => 'text', 'label' => 'Cronotacógrafo', 'data-date' => 'yes']) ?>
    </div>

</div>

</div>
<div class="row">
    <div class="col-md-2 col-sm-12" data-truck-4eixo="yes" style="display:none">
        <?= $this->Form->control('quantidade_litros', ['type' => 'text', 'label' => 'Quantidade de Litros']) ?>
    </div>
    <div id="bocas-area" class="col-md-2 col-sm-12">
        <?=
            $this->Form->control('quantidade_bocas', [
                'type' => 'select',
                'options' => [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10],
                'label' => 'Quantidade de Bocas',
                'empty' => true
            ])
        ?>
    </div>
</div>
<?= $this->Form->end() ?>