<script>
    $(document).ready(function() {

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Veículos']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8 col-sm-12">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <!-- <div class="row"> -->
                    <!-- <div class="col-md-4 col-sm-12">
                            <//?= $this->Form->control('marca_modelo', ['label' => 'Marca/Modelo']) ?> -->
                    <!-- </div> -->
                    <div class="col-md-4 col-sm-12">
                        <?= $this->Form->control('placa', ['label' => 'Placa']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 mt-2 mb-2">
                        <?= $this->element('button_filter') ?>
                        <?= $this->element('button_clear') ?>
                        <?= $this->element('button_new', []) ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>

            <table class="table table-hover table-responsive-sm">
                <tr>
                    <th>Placa</th>
                </tr>
                <?php
                if (sizeof($vehicles) > 0) {
                    foreach ($vehicles as $vehicle) {
                        ?>
                        <tr>
                            <!-- <td><//?= $this->Html->link($vehicle->marca_modelo, ['action' => 'edit', $vehicle->id]) ?></td>
                                <td><//?= $vehicle_types[$vehicle->tipo] ?></td> -->
                            <td><?= $this->Html->link($vehicle->placa, ['action' => 'edit', $vehicle->id]) ?></td>
                        </tr>
                    <?php
                        }
                    } else {
                        ?>
                    <tr>
                        <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                    </tr>
                <?php } ?>
            </table>
            <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
        </div>
    </div>
</div>
</div>