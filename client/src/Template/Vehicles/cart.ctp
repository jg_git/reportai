<?= $this->Flash->render() ?>
<?=
$this->Form->create(null, [
    'id' => 'form-cart-' . $anchor,
    'data-anchor' => $anchor])
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <?= $this->Form->control('cart_id', ['type' => 'select', 'options' => $carts, 'label' => false]) ?>
    </div>
</div>
<?= $this->Form->end() ?>
