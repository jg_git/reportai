<?= $this->Form->create(null, ['id' => 'form-contact-' . $anchor, 'data-anchor' => $anchor]) ?>
<div class="row">
    <div class="col">
        <?= $this->Form->control('contact_id', ['options' => $contacts, 'label' => false]) ?>
    </div>
</div>
<?= $this->Form->end() ?>

