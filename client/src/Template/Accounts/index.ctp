<script>
    $(document).ready(function () {
        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            $.get("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'active']) ?>/" + $(this).data("account-id") + "/Y");
        }).on('ifUnchecked', function (e) {
            $.get("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'active']) ?>/" + $(this).data("account-id") + "/N");
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Contas']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('titular', ['label' => 'Titular']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('bank_id', ['label' => 'Banco', 'options' => $banks, 'empty' => true]) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('distributor_id', ['label' => 'Distribuidora', 'options' => $distributors, 'empty' => true]) ?>
                        </div>
                        <div class="col-md-3 col-sm-12 mt-4">
                            <?= $this->Form->control('ativa', ['type' => 'checkbox', 'label' => 'Ativas']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9 col-sm-12">

                        </div>
                        <div class="col-md-3 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th style="text-align: center">Ativa</th>
                        <th><?= $this->Paginator->sort('titular', 'Titular') ?></th>
                        <th><?= $this->Paginator->sort('tipo', 'Tipo') ?></th>
                        <th>Dados Adicionais</th>
                    </tr>
                    <?php
                    if (sizeof($accounts) > 0) {
                        foreach ($accounts as $account) {
                            ?>
                            <tr>
                                <td style="text-align: center">
                                    <?= $this->Form->control('ativa', ['type' => 'checkbox', 'label' => false, 'data-account-id' => $account->id, 'checked' => $account->ativa]) ?>
                                </td>
                                <td><?= $this->Html->link($account->titular, ['action' => 'edit', $account->id]) ?></td>
                                <td><?= $account_record_types[$account->tipo] ?></td>
                                <td>
                                    <?php
                                    switch ($account->tipo) {
                                        case 'F':
                                            echo isset($account->bank) ? $account->bank->nome . ' - ' . $account->numero_agencia . ' - ' . $account->numero_conta : '';
                                            break;
                                        case 'I':
                                            echo isset($account->distributor) ? 'Distribuidora: ' . $account->distributor->razao_social : '';
                                            break;
                                    }
                                    ?>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    