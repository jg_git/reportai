<script>
    $(document).ready(function () {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
    });
</script>


<?= $this->Flash->render() ?>
<?=
$this->Form->create($account, [
    'id' => 'form-account-' . $anchor,
    'data-id' => $account->isNew() ? 'new' : $account->id,
    'data-error' => sizeof($account->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row">
    <div class="col-md-3 col-sm-12 mt-4">
        <?= $this->Form->radio('tipo_conta', $account_types, ['default' => 'C']) ?>
    </div>
    <div class="col-md-5 col-sm-12">
        <?= $this->Form->control('bank_id', ['type' => 'select', 'options' => $banks, 'label' => 'Banco']) ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <?= $this->Form->control('numero_agencia', ['type' => 'text', 'label' => 'Agência']) ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <?= $this->Form->control('numero_conta', ['type' => 'text', 'label' => 'Conta']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
