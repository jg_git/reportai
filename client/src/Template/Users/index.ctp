<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Controle de Usuários']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <?= $this->element('button_new', ['action' => 'add']) ?> <br><br><br>
                <table class="table table-hover table-responsive-sm">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('Users.ativo', ['label' => 'Ativo']) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Users.nome', ['label' => 'Nome']) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Users.email', ['label' => 'E-Mail']) ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Roles.nome', ['label' => 'Perfil']) ?></th>
                            <th scope="col">Funções</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user) : ?>
                            <tr>
                                <td><?= $this->element('display_check', ['check' => $user->ativo]) ?></td>
                                <td><?= $this->Html->link(h($user->nome), ['action' => 'edit', $user->id]) ?></td>
                                <td><?= h($user->email) ?></td>
                                <td><?= h($user->role->nome) ?></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Users', 'action' => 'edit', $user->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-asterisk']) . ' ' . $this->Html->tag('span', 'Redefinir Senha', ['class' => 'notification-text']), ['controller' => 'Users', 'action' => 'reset', $user->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                        <?php if($this->request->session()->read('Auth.User.role_id') == 1 AND (sizeof($admins) > 1) AND ($user->prop_id != $user->client_id)){
                                                                echo $this->element('a_delete', ['id' => $user->id]);
                                                            } 
                                                        ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>