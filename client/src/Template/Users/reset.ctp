<script>
    $(document).ready(function() {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Controle de Usuários', 'url' => ['action' => 'index']],
    ['title' => 'Edição de Usuário']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?= $this->Form->create($user) ?>
                <fieldset>

                </fieldset>
                <?= $this->element('button_sreset') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>