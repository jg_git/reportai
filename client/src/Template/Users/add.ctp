<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script>
    $(document).ready(function() {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        $("#telefoneum").inputmask({
            mask: ["(99) 9999-9999", "(99) 99999-9999", ],
            keepStatic: true
        });
            $("#telefonedois").inputmask({
            mask: ["(99) 9999-9999", "(99) 99999-9999", ],
            keepStatic: true
        });
    });

    $("#telefoneum").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });
    $("#telefonedois").inputmask({
        mask: ["(99) 9999-9999", "(99) 99999-9999", ],
        keepStatic: true
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Controle de Usuários', 'url' => ['action' => 'index']],
    ['title' => 'Novo Usuário']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?= $this->Form->create($user) ?>
                <fieldset>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <?= $this->Form->control('ativo', ['type' => 'checkbox']) ?>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <label>Perfil</label> <span style="color: red">*</span>
                            <?= $this->Form->control('role_id', ['label' => false]) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <label>Nome</label> <span style="color: red">*</span>
                            <?= $this->Form->control('nome', ['label' => false]) ?>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <label>E-Mail</label> <span style="color: red">*</span>
                            <?= $this->Form->control('email', ['label' => false]) ?>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                             <?= $this->Form->control('telefoneum', ['label' => 'Primeiro Telefone']) ?>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <?= $this->Form->control('telefonedois', ['label' => 'Segundo Telefone']) ?>
                        </div>
                    </div>
                </fieldset>
                <?= $this->element('button_save') ?>
                <?= $this->element('button_return') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>