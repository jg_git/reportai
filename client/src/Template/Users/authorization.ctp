<script>
    $(document).ready(function () {
        // $("input[value=Salvar]").click(function(){
        //     window.location.href = window.location.href;
        // });
        // $("input[data-role]").click(function () {
        //     $.get(" ?php $this->Url->build(['action' => 'authorization-switch']) ?>/" +
        //             $(this).data("role") + "/" +
        //             $(this).data("module") + "/" +
        //             ($(this).is(":checked") ? "Y" : "N"));
        // });

        $("#all-on").click(function () {
            $("[id^=check-]").each(function () {
                $(this).attr("checked", true);
            });
        });

        $("#all-off").click(function () {
            $("[id^=check-]").each(function () {
                $(this).attr("checked", false);
            });
        });

        $("input[name=super_user]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            $.get("<?= $this->Url->build(['controller' => 'Users', 'action' => 'super-user']) ?>/" + $(this).data("role-id") + "/Y");
        }).on('ifUnchecked', function (e) {
            $.get("<?= $this->Url->build(['controller' => 'Users', 'action' => 'super-user']) ?>/" + $(this).data("role-id") + "/N");
        });

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Controle de Acesso']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <button class="btn btn-lg btn-primary mb-2" type="button" data-toggle="collapse" data-target="#collapseAdd" aria-expanded="false" aria-controls="collapseAdd">
                    Novo
                </button>
                <div class="collapse" id="collapseAdd">
                    <div class="card card-body">
                        <h5 class="card-title">Novo Perfil</h5>
                        <?= $this->Form->create($newrole) ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <?php 
                                    echo '<div style="margin-bottom: .5rem">';
                                        echo '<label for="nome" style="display: inline; margin-bottom: .5rem">Nome do Perfil</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                                    echo '</div>';
                                ?>
                                <?= $this->Form->control('nome', ['label' => false]) ?>
                            </div>
                        </div>
                        <h5 class="mt-3 mb-3">Permissão de Módulos</h5>
                        <?php
                        $i = 1;
                        foreach ($modules as $module) {
                            ?>
                            <div class="mb-1" style="display: inline-block; width: 300px">
                                <label class="switch">
                                    <?= $this->Form->checkbox('check-' . $module->id, ['id' => 'check-' . $module->id]) ?>
                                    <span class="slider round"></span>
                                </label>
                                <?= $module->nome ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="mt-2">
                            <?= $this->element('button_save') ?>
                            <div id = 'all-on' class="btn btn-lg btn-outline-primary mb-2">Habilitar Todos </div>
                            <div id = 'all-off' class="btn btn-lg btn-outline-primary mb-2">Desabilitar Todos </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>

                <div id="accordion">
                    <?php foreach ($roles as $role) { ?>
                        <div class="card">
                            <div class="card-header" role="tab" id="heading-<?= $role->id ?>">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse-<?= $role->id ?>" aria-expanded="true" aria-controls="collapse-<?= $role->id ?>">
                                        <?= h($role->nome) ?>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse-<?= $role->id ?>" class="collapse" aria-labelledby="heading-<?= $role->id ?>" data-parent="#accordion">
                                <div class="card-body pb-5">
                                    <div class="row mb-3">
                                         <!-- <div class="col"> -->
                                            <!-- =
                                            $this->Form->control('super_user', [
                                                'type' => 'checkbox',
                                                'label' => 'Super Usuário',
                                                'checked' => $role->super_user,
                                                'data-role-id' => $role->id
                                            ])
                                            ?> -->
                                        <!-- </div> -->
                                    </div>
                                    <?= $this->Form->create($editrole,['action' => 'authorization-switch']) ?>
                                    <div class="row">
                                        <div class="col">
                                            <?php
                                            foreach ($modules as $module) {
                                                if($role->nome == "Administrador" AND $module->id == '3'){

                                                }else{
                                                $checked = false;
                                                foreach ($role->modules as $rm) {
                                                    if ($rm->id == $module->id) {
                                                        $checked = true;
                                                        break;
                                                    }
                                                }
                                                ?>
                                                <div class="mb-1" style="display: inline-block; width: 300px;">
                                                    <label class="switch">
                                                        <input type="checkbox" 
                                                               data-role="<?= $role->id ?>" 
                                                               data-module="<?= $module->id ?>" <?= $checked ? 'checked' : '' ?>>
                                                        <span class="slider round"></span>
                                                    </label>
                                                    <?= $module->nome ?>
                                                </div>
                                            <?php } } ?>
                                        </div>
                                    </div>
                                    <?= $this->element('button_save') ?>
                                    <?php if($role->nome != 'Administrador'){ ?>
                                        <?= $this->element('button_delete', ['id' => $role->id, 'action' => 'delete_role', 'title' => 'Excluir função ' . $role->nome . '?']) ?>
                                    <?php } ?>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
