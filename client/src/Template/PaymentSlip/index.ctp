<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        $("input[name=vencimento_range]").daterangepicker(configRangePicker, function(start, end, label) {}).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=data_vencimento_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=data_vencimento_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=data_vencimento_de]").val("");
            $("input[name=data_vencimento_ate]").val("");
        });

        $("input[name=venda_range]").daterangepicker(configRangePicker, function(start, end, label) {}).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=data_venda_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=data_venda_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=data_venda_de]").val("");
            $("input[name=data_venda_ate]").val("");
        });

        $("input[name=all]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function(e) {
            checklist = "";
            $("input[type=checkbox][name=flag]").each(function() {
                $(this).off("ifChecked");
                $(this).iCheck("check");
                checklist += $(this).data("receivable-id") + ",";
                $(this).on('ifChecked', function(e) {
                    $.getJSON("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/A/" + $(this).data("receivable-id"),
                        function(data, code) {
                            if (checkstatus(code)) {
                                $("div[id=itens").html(data.itens);
                                $("span[id=total").html(formatMoney(data.total, 2));
                            }
                        });
                });
            });
            $.getJSON("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/LA/" + checklist.replace(/,+$/, ''),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens").html(data.itens);
                        $("span[id=total").html(formatMoney(data.total, 2));
                    }
                });
        }).on('ifUnchecked', function(e) {
            checklist = "";
            $("input[type=checkbox][name=flag]").each(function() {
                $(this).off("ifUnchecked");
                $(this).iCheck("uncheck");
                checklist += $(this).data("receivable-id") + ",";
                $(this).on('ifUnchecked', function(e) {
                    $.getJSON("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/R/" + $(this).data("receivable-id"),
                        function(data, code) {
                            if (checkstatus(code)) {
                                $("div[id=itens").html(data.itens);
                                $("span[id=total").html(formatMoney(data.total, 2));
                            }
                        });
                });
            });
            $.getJSON("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/LR/" + checklist.replace(/,+$/, ''),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens").html(data.itens);
                        $("span[id=total").html(formatMoney(data.total, 2));
                    }
                });
        });

        <?php
        $found = false;
        foreach ($receivables as $receivable) {
            if (!in_array($receivable->id, $cart)) {
                $found = true;
            }
        }
        if (!$found) {
        ?>
            $("input[name=all]").iCheck("check");
        <?php
        }
        ?>

        $("input[name=flag]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function(e) {
            $.getJSON("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/A/" + $(this).data("receivable-id"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens").html(data.itens);
                        $("span[id=total").html(formatMoney(data.total, 2));
                    }
                });
        }).on('ifUnchecked', function(e) {
            $.getJSON("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/R/" + $(this).data("receivable-id"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens").html(data.itens);
                        $("span[id=total").html(formatMoney(data.total, 2));
                    }
                });
        });

        $("button[id=clear-cart]").click(function() {
            $.get("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cart']) ?>/C",
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens").html(data.itens);
                        $("span[id=total").html(formatMoney(data.total, 2));
                        $("input[name=flag]").iCheck("uncheck");
                    }
                });
        });

        $("i[data-batch-id]").click(function() {
            $.get("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'error']) ?>/" + $(this).data("batch-id"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("#error-area").html(data);
                    }
                });
        });

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Emissão de Boletos']
]);
echo $this->Breadcrumbs->render();
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('vencimento_range', ['type' => 'text', 'label' => 'Data de Vencimento']) ?>
                            <?= $this->Form->control('data_vencimento_de', ['type' => 'hidden']) ?>
                            <?= $this->Form->control('data_vencimento_ate', ['type' => 'hidden']) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('venda_range', ['type' => 'text', 'label' => 'Data da Venda']) ?>
                            <?= $this->Form->control('data_venda_de', ['type' => 'hidden']) ?>
                            <?= $this->Form->control('data_venda_ate', ['type' => 'hidden']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('razao_social', ['type' => 'text', 'label' => 'Razão Social']) ?>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('nota_fiscal', ['type' => 'text', 'label' => 'Nota Fiscal']) ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5 col-sm-12">
                <h4>Boletos Selecionados</h4>
                <table class="table table-hover extra-slim-row">
                    <tr>
                        <td class="standout" width="30%">Itens</td>
                        <td class="money standout">
                            <div id="itens"><?= sizeof($cart) - 1 ?></div>
                        </td>
                    </tr>
                    <tr>
                        <td class="standout">Total</td>
                        <td class="money standout">
                            R$ <span id="total"><?= number_format($total, 2, ',', '.') ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <button id="clear-cart" type="button" class="btn btn-primary">Limpar</button>
                        </td>
                    </tr>
                </table>
                <h4>Boletos Em Processamento</h4>
                <table class="table table-hover extra-slim-row">
                    <tr>
                        <th width="10%">Solicitados</th>
                        <th width="10%">Gerados</th>
                        <th width="10%">Erros</th>
                        <th width="10%">&nbsp;</th>
                        <th width="25%">Data Solicitação</th>
                        <th width="25%">Data Geração</th>
                        <th width="5%">&nbsp;</th>
                        <th width="5%">&nbsp;</th>
                    </tr>
                    <?php foreach ($batches as $batch) { ?>
                        <tr>
                            <td class="money"><?= $batch->num_requested ?></td>
                            <td class="money"><?= $batch->num_printed ?></td>
                            <td class="money"><?= $batch->num_registration_errors ?>
                            <td class="money">
                                <?php if ($batch->num_requested > 0) {
                                    echo round((($batch->num_printed + $batch->num_registration_errors + $batch->num_print_errors) / $batch->num_requested) * 100) . '%';
                                } else {
                                    echo '0%';
                                }
                                ?>
                            </td>
                            </td>
                            <td><?= $batch->date_requested ?></td>
                            <td><?= $batch->date_printed ?></td>
                            <td>
                                <?php if ($batch->num_requested == $batch->num_printed + $batch->num_registration_errors + $batch->num_print_errors) { ?>
                                    <?php if ($batch->pdf_filename) { ?>
                                        <a href="boletos/<?= $batch->pdf_filename ?>" target="_new"><i class="fa fa-2x fa-download" style="color:green"></i></a>
                                    <?php } ?>
                                <?php } else { ?>
                                    <i class="fa fa-2x fa-clock-o" style="color:lightgray"></i>
                                <?php } ?>
                            </td>
                            <td>
                                <?php if ($batch->num_registration_errors > 0) { ?>
                                    <i class="fa fa-2x fa-exclamation-triangle" data-toggle="modal" data-target="#error-modal" style="color:red" data-batch-id="<?= $batch->id ?>"></i>
                                <?php } ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <div class="col-md-7">
                <h3>
                    <center>EMISSÃO E REGISTRO DE BOLETOS</center>
                </h3>
                <div class="row mt-3 mb-3">
                    <div class="col">
                        <?=
                            $this->Form->postbutton(
                                'REGISTRAR E EMITIR BOLETOS SELECIONADOS',
                                ['action' => 'gerarBoletos'],
                                ['confirm' => 'Deseja gerar os boletos selecionados?', 'class' => 'btn btn-outline-primary statement']
                            )
                        ?>
                    </div>
                    <div class="col">
                        <div class="text-right">
                            <?=
                                $this->Form->postbutton(
                                    'EXCLUIR',
                                    ['action' => 'excluirBoletos'],
                                    ['confirm' => 'Deseja excluir os boletos selecionados?', 'class' => 'btn btn-outline-dark statement']
                                )
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <table class="table table-hover extra-slim-row">
                            <tr>
                                <th style="text-align: center">
                                    <?= $this->Form->control('all', ['type' => 'checkbox', 'label' => false]) ?>
                                </th>
                                <th>Tipo</th>
                                <th><?= $this->Paginator->sort('data_vencimento', 'Data Vencimento') ?></th>
                                <th><?= $this->Paginator->sort('valor', 'Valor') ?></th>
                                <th>Descrição</th>
                            </tr>
                            <?php
                            if (sizeof($receivables) > 0) {
                                foreach ($receivables as $receivable) {
                            ?>
                                    <tr>
                                        <td style="text-align: center">
                                            <?php
                                            echo $this->Form->control('flag', [
                                                'type' => 'checkbox',
                                                'label' => false,
                                                'data-receivable-id' => $receivable->id,
                                                'checked' => in_array($receivable->id, $cart)
                                            ]);
                                            ?>
                                        </td>
                                        <td><?= $receivable_types[$receivable->tipo] ?></td>
                                        <td><?= $receivable->data_vencimento ?></td>
                                        <td style=" text-align: right"><?= $this->Number->currency($receivable->valor) ?></td>
                                        <td>
                                            <?php
                                            switch ($receivable->tipo) {
                                                case 'VE':
                                                    $msg = 'Data Venda: ' . $receivable->sale_detail->sale->data_venda . '<br>' .
                                                        'Cliente: ' . $receivable->sale_detail->presale->client->razao_social . '<br>' .
                                                        'Distribuidora: ' . $receivable->sale_detail->sale->purchases[0]->distributor->razao_social . '<br>' .
                                                        'NF: ' . $receivable->sale_detail->nota_fiscal;
                                                    echo $msg;
                                                    break;
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                                </tr>
                            <?php
                            }
                            ?>
                        </table>
                        <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="error-modal" role="dialog">
    <div class="modal-dialog modal-lg" style="max-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-body">
                <div id="error-area"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>