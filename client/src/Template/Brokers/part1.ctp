<script>
    $(document).ready(function () {
        $('#tipo-pj input[type=radio]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });

        $("input[name=cnpj]").inputmask("99.999.999/9999-99");
        $("input[name=inscricao_estadual]").inputmask("999.999.999.999");
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($broker, [
    'id' => 'form-part1',
    'data-id' => $broker->isNew() ? 'new' : $broker->id,
    'data-error' => sizeof($broker->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Dados Básicos</h5>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('razao_social', ['type' => 'text', 'label' => 'Razão Social']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('nome_fantasia', ['type' => 'text', 'label' => 'Nome Fantasia']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cnpj', ['type' => 'text', 'label' => 'CNPJ']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('inscricao_estadual', ['type' => 'text', 'label' => 'Inscrição Estadual']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('inscricao_municipal', ['type' => 'text', 'label' => 'Inscrição Municipal']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
