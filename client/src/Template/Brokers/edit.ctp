<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {
        phone_types = [];
<?php foreach ($phone_types as $key => $type) { ?>
            phone_types["<?= $key ?>"] = "<?= $type ?>";
<?php } ?>

        $("#part1").load("<?= $this->Url->build(['action' => 'part1', $broker->isNew() ? 'new' : $broker->id]) ?>");
        $("#part2").load("<?= $this->Url->build(['action' => 'part2', $broker->isNew() ? 'new' : $broker->id]) ?>");

        $("#email").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-email-area-" + anchor + " form[id|=form-email]").length === 0) {
                $("#edit-email-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'email']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-email-" + anchor).show();
            $("#view-email-" + anchor).hide();
        });

        $("#phone").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-phone-area-" + anchor + " form[id|=form-phone]").length === 0) {
                $("#edit-phone-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'phone']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-phone-" + anchor).show();
            $("#view-phone-" + anchor).hide();
        });

        $("#account").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-account-area-" + anchor + " form[id|=form-account]").length === 0) {
                $("#edit-account-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'account']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-account-" + anchor).show();
            $("#view-account-" + anchor).hide();
        });

        $("#contact").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-contact-area-" + anchor + " form[id|=form-contact]").length === 0) {
                $("#edit-contact-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'contact']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-contact-" + anchor).show();
            $("#view-contact-" + anchor).hide();
        });

        $("#log").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-log-area-" + anchor + " form[id|=form-log]").length === 0) {
                $("#edit-log-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'clog']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-log-" + anchor).show();
            $("#view-log-" + anchor).hide();
        });

        $("#email").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-email-area-" + anchor).html($("#form-email-" + anchor + " input[id=email]").val());
            $("#edit-email-" + anchor).hide();
            $("#view-email-" + anchor).show();
        });

        $("#phone").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-phone-area-" + anchor + " span[id=type]").html(phone_types[$("#form-phone-" + anchor + " input[name=tipo]:checked").val()]);
            $("#view-phone-area-" + anchor + " span[id=ddd]").html("(" + $("#form-phone-" + anchor + " input[id=ddd]").val() + ")");
            $("#view-phone-area-" + anchor + " span[id=phone]").html($("#form-phone-" + anchor + " input[id=telefone]").val());
            $("#edit-phone-" + anchor).hide();
            $("#view-phone-" + anchor).show();
        });

        $("#account").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-account-area-" + anchor + " span[id=banco]").html($("#form-account-" + anchor + " select[name=bank_id] option:selected").text());
            $("#view-account-area-" + anchor + " span[id=agencia]").html($("#form-account-" + anchor + " input[name=numero_agencia]").val());
            $("#view-account-area-" + anchor + " span[id=conta]").html($("#form-account-" + anchor + " input[name=numero_conta]").val());
            $("#edit-account-" + anchor).hide();
            $("#view-account-" + anchor).show();
        });

        $("#contact").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-contact-area-" + anchor + " span[id=nome]").html($("#form-contact-" + anchor + " input[id=nome]").val());
            $("#view-contact-area-" + anchor + " span[id=cpf]").html($("#form-contact-" + anchor + " input[id=cpf]").val());
            $("#view-contact-area-" + anchor + " span[id=email]").html($("#form-contact-" + anchor + " input[id=email]").val());
            if ($("#form-contact-" + anchor + " input[id=telefone]").val() !== "") {
                $("#view-contact-area-" + anchor + " span[id=telefone]").html("Telefone: " + $("#form-contact-" + anchor + " input[id=telefone]").val());
            } else {
                $("#view-contact-area-" + anchor + " span[id=telefone]").html("");
            }
            $("#view-contact-area-" + anchor + " span[id=celular]").html($("#form-contact-" + anchor + " input[id=celular]").val());
            $("#edit-contact-" + anchor).hide();
            $("#view-contact-" + anchor).show();
        });

        $("#log").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-log-area-" + anchor + " span[id=descricao]").html($("#form-log-" + anchor + " textarea[name=descricao]").val());
            $("#view-log-area-" + anchor + " span[id=created]").html("");
            $("#view-log-area-" + anchor + " span[id=user]").html("");
            filename = $("#form-log-" + anchor + " input[name=file]").val().replace(/^.*[\\\/]/, '');
            if (filename !== "") {
                $("#view-log-area-" + anchor + " span[id=file]").html("Anexo: " + filename);
            } else {
                $("#view-log-area-" + anchor + " span[id=file]").html("");
            }
            $("#edit-log-" + anchor).hide();
            $("#view-log-" + anchor).show();
        });

        $(".fa-plus-square").click(function () {
            anchor = anchorize();
            switch ($(this).data("type")) {
                case "email":
                    $("#no-email").remove();
                    $("#email-add").before(
                            '<tr id="edit-email-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-email-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'email\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-email-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-email-area-' + anchor + '" style="font-size: 14px">' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="email" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-email-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'email']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
                case "phone":
                    $("#no-phone").remove();
                    $("#phone-add").before(
                            '<tr id="edit-phone-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-phone-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'phone\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-phone-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-phone-area-' + anchor + '" style="font-size: 14px">' +
                            '<span class="mr-2" id="ddd"></span>' +
                            '<span class="mr-2" id="phone"></span>' +
                            '<span class="mr-2" id="type"></span>' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="phone" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-phone-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'phone']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
                case "account":
                    $("#no-account").remove();
                    $("#account-add").before(
                            '<tr id="edit-account-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-account-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'account\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="account" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-account-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-account-area-' + anchor + '" style="font-size: 14px">' +
                            '<span class="mr-2" id="banco"></span>' +
                            'Agência: <span class="mr-2" id="agencia"></span>' +
                            'Conta: <span class="mr-2" id="conta"></span>' +
                            '<span class="mr-2" id="tipo-conta"></span>' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="account" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-account-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'account']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
                case "contact":
                    $("#no-contact").remove();
                    $("#contact-add").before(
                            '<tr id="edit-contact-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-contact-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'contact\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="contact" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-contact-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-contact-area-' + anchor + '" style="font-size: 14px">' +
                            'Nome: <b><span id="nome"></span></b><br>' +
                            'Email: <span id="email"></span><br>' +
                            'CPF: <span id="cpf"></span><br>' +
                            '<span id="telefone"></span> Celular: <span id="celular"></span><br>' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="contact" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-contact-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'contact']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
                case "log":
                    $("#no-log").remove();
                    $("#log-add").before(
                            '<tr id="edit-log-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-log-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'log\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="log" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-log-' + anchor + '" style="display: none">' +
                            '<td>' +
                            '<span id="view-log-area-' + anchor + '" style="font-size: 14px">' +
                            '<span id="descricao"></span><br>' +
                            '<span id="created"></span> <span id="user"></span><br>' +
                            '<span id="file"></span>' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="log" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-log-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'clog']) ?>/<?= $broker->isNew() ? 'new' : $broker->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
            }
        });

        $("#save").click(function () {
            HoldOn.open({theme: "sk-cube-grid"});
            var part1 = $.Deferred();
            var part2 = $.Deferred();
            var email = $.Deferred();
            var phone = $.Deferred();
            var account = $.Deferred();
            var contact = $.Deferred();
            var log = $.Deferred();
            broker_id = $("#form-part1").data("id");
            $.post("<?= $this->Url->build(['action' => 'part1']) ?>/" + broker_id,
                    $("#form-part1").serialize(),
                    function (data) {
                        $("#part1").html(data);
                        if ($("<div>" + data + "</div>").find("#form-part1").data("error") === "N") {
                            part1.resolve("OK");
                            broker_id = $("<div>" + data + "</div>").find("#form-part1").data("id");
                            $.post("<?= $this->Url->build(['action' => 'part2']) ?>/" + broker_id,
                                    $("#form-part2").serialize(),
                                    function (data) {
                                        $("#part2").html(data);
                                        if ($("<div>" + data + "</div>").find("#form-part2").data("error") === "N") {
                                            part2.resolve("OK");
                                        } else {
                                            part2.resolve("NG");
                                        }
                                    });
                            //======== Salvar emails ======
                            var req_email = [];
                            $("form[id|='form-email']").each(function () {
                                anchor = $(this).data("anchor");
                                req_email.push($.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'email']) ?>/" + broker_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-email-" + anchor).serialize()));
                            });
                            if (req_email.length > 0) {
                                $.when.apply(undefined, req_email).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-email']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-email']").data("id");
                                        $("#edit-email-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('email', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-email-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    email.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                email.resolve("OK");
                            }
                            if ($("#form-delete-email option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'email-delete']) ?>",
                                        $("#form-delete-email").serialize());
                            }
                            //======== Salvar telefones ======
                            var req_phone = [];
                            $("form[id|='form-phone']").each(function () {
                                anchor = $(this).data("anchor");
                                req_phone.push($.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'phone']) ?>/" + broker_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-phone-" + anchor).serialize()));
                            });
                            if (req_phone.length > 0) {
                                $.when.apply(undefined, req_phone).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-phone']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-phone']").data("id");
                                        $("#edit-phone-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('phone', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-phone-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    phone.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                phone.resolve("OK");
                            }
                            if ($("#form-delete-phone option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'phone-delete']) ?>",
                                        $("#form-delete-phone").serialize());
                            }
                            //======== Salvar contas ======
                            var req_account = [];
                            $("form[id|='form-account']").each(function () {
                                anchor = $(this).data("anchor");
                                req_account.push($.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'account']) ?>/" + broker_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-account-" + anchor).serialize()));
                            });
                            if (req_account.length > 0) {
                                $.when.apply(undefined, req_account).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-account']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-account']").data("id");
                                        $("#edit-account-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('account', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-account-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    account.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                account.resolve("OK");
                            }
                            if ($("#form-delete-account option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'account-delete']) ?>",
                                        $("#form-delete-account").serialize());
                            }
                            //======== Salvar contatos ======
                            var req_contact = [];
                            $("form[id|='form-contact']").each(function () {
                                anchor = $(this).data("anchor");
                                req_contact.push($.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'contact']) ?>/" + broker_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-contact-" + anchor).serialize()));
                            });
                            if (req_contact.length > 0) {
                                $.when.apply(undefined, req_contact).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-contact']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-contact']").data("id");
                                        $("#edit-contact-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('contact', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-contact-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    contact.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                contact.resolve("OK");
                            }
                            if ($("#form-delete-contact option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'contact-delete']) ?>",
                                        $("#form-delete-contact").serialize());
                            }
                            //======== Salvar historico ======
                            var req_log = [];
                            $("form[id|='form-log']").each(function () {
                                anchor = $(this).data("anchor");
                                req_log.push(
                                        $.ajax({
                                            url: "<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'clog']) ?>/" +
                                                    broker_id + "/" +
                                                    $(this).data("id") + "/" +
                                                    anchor,
                                            type: 'POST',
                                            data: new FormData($("#form-log-" + anchor)[0]),
                                            processData: false,
                                            contentType: false
                                        }));
                            });
                            if (req_log.length > 0) {
                                $.when.apply(undefined, req_log).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-log']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-log']").data("id");
                                        $("#edit-log-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('log', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-log-" + anchor).data("error") === "N") {
                                            $("#view-log-area-" + anchor + " span[id=created]").html($("<div>" + data[i] + "</div>").find("#created").html());
                                            $("#view-log-area-" + anchor + " span[id=user]").html($("<div>" + data[i] + "</div>").find("#user").html());
                                            $("#view-log-area-" + anchor + " span[id=file]").html("Anexo: " + $("<div>" + data[i] + "</div>").find("#attach").html());
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    log.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                log.resolve("OK");
                            }
                            if ($("#form-delete-log option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Brokers', 'action' => 'log-delete']) ?>",
                                        $("#form-delete-log").serialize());
                            }
                        } else {
                            part1.resolve("NG");
                            part2.resolve("NG");
                            email.resolve("NG");
                            phone.resolve("NG");
                            account.resolve("NG");
                            contact.resolve("NG");
                            log.resolve("NG");
                        }
                    });
            $.when(part1, part2, email, phone, account, contact, log).done(function (v1, v2, v3, v4, v5, v6, v7) {
                $("#save").attr("disabled", false);
                console.log("part1: " + v1 + " part2: " + v2 + " email: " + v3 + " phone: " + v4 + " account: " + v5 + " contact: " + v6 + " log: " + v7);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK" && v3 === "OK" && v4 === "OK" && v5 === "OK" && v6 === "OK" && v7 === "OK") {
                    toast("Cadastro salvo com sucesso");
                }
            });

        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Essa operação não poderá ser revertida',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                switch (type) {
                    case "email":
                        $("#edit-email-" + anchor).remove();
                        $("#view-email-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-email-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "phone":
                        $("#edit-phone-" + anchor).remove();
                        $("#view-phone-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-phone-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "account":
                        $("#edit-account-" + anchor).remove();
                        $("#view-account-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-account-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "contact":
                        $("#edit-contact-" + anchor).remove();
                        $("#view-contact-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-contact-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "log":
                        $("#edit-log-" + anchor).remove();
                        $("#view-log-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-log-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                }
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Corretoras', 'url' => ['action' => 'index']],
    ['title' => 'Corretora']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div id="part1"></div>
        <div class="row">
            <div class="col-md-3">
                <h5 class="mt-3">Emails</h5>
                <table id="email" class="table table-hover">
                    <?php
                    if (isset($broker->broker_emails) and sizeof($broker->broker_emails) > 0) {
                        foreach ($broker->broker_emails as $email) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-email-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-email-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('email', '<?= $anchor ?>', <?= $email->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-email-<?= $anchor ?>">
                                <td>
                                    <span id="view-email-area-<?= $anchor ?>" style="font-size: 14px">
                                        <?= $email->email ?>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="email" data-anchor="<?= $anchor ?>" data-id="<?= $email->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-email">
                            <td class="no-data-found">Nenhum Email Cadastrado</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="email-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="email"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-email']) ?>
                <?= $this->Form->control('delete-email-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-5">
                <h5 class="mt-3">Telefones</h5>
                <table id="phone" class="table table-hover">
                    <?php
                    if (isset($broker->broker_phones) and sizeof($broker->broker_phones) > 0) {
                        foreach ($broker->broker_phones as $phone) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-phone-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-phone-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('phone', '<?= $anchor ?>', <?= $phone->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-phone-<?= $anchor ?>">
                                <td>
                                    <span id="view-phone-area-<?= $anchor ?>" style="font-size: 14px">
                                        <span class="mr-2" id="ddd">(<?= $phone->ddd ?>)</span>
                                        <span class="mr-2" id="phone"><?= $phone->telefone ?></span>
                                        <span class="mr-2" id="type"><?= $phone_types[$phone->tipo] ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="phone" data-anchor="<?= $anchor ?>" data-id="<?= $phone->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-phone">
                            <td class="no-data-found">Nenhum Telefone Cadastrado</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="phone-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="phone"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-phone']) ?>
                <?= $this->Form->control('delete-phone-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div id="part2"></div>
        <div class="row">
            <div class="col-md-9">
                <h5 class="mt-3">Contas Bancárias</h5>
                <table id="account" class="table table-hover">
                    <?php
                    if (isset($broker->broker_bank_accounts) and sizeof($broker->broker_bank_accounts) > 0) {
                        foreach ($broker->broker_bank_accounts as $account) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-account-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-account-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('account', '<?= $anchor ?>', <?= $account->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="account" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-account-<?= $anchor ?>">
                                <td>
                                    <span id="view-account-area-<?= $anchor ?>" style="font-size: 14px">
                                        <span class="mr-2" id="banco"><?= $account->bank->codigo ?> - <?= $account->bank->nome ?></span>
                                        Agência: <span class="mr-2" id="agencia"><?= $account->numero_agencia ?></span>
                                        Conta: <span class="mr-2" id="conta"><?= $account->numero_conta ?></span>
                                        <span class="mr-2" id="tipo-conta"><?= $account_types[$account->tipo_conta] ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="account" data-anchor="<?= $anchor ?>" data-id="<?= $account->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-account">
                            <td class="no-data-found">Nenhuma Conta Cadastrada</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="account-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="account"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-account']) ?>
                <?= $this->Form->control('delete-account-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-9">
                <h5 class="mt-3">Contatos</h5>
                <table id="contact" class="table table-hover">
                    <?php
                    if (isset($broker->broker_contacts) and sizeof($broker->broker_contacts) > 0) {
                        foreach ($broker->broker_contacts as $contact) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-contact-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-contact-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('contact', '<?= $anchor ?>', <?= $contact->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="contact" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-contact-<?= $anchor ?>">
                                <td>
                                    <span id="view-contact-area-<?= $anchor ?>" style="font-size: 14px">
                                        Nome: <b><span id="nome"><?= $contact->nome ?></span></b><br>
                                        CPF: <span id="cpf"><?= $contact->cpf ?></span><br>
                                        Email: <span id="email"><?= $contact->email ?></span><br>
                                        <span id="telefone"><?= !empty($contact->telefone) ? 'Telefone: ' . $contact->telefone : '' ?></span>
                                        Celular: <span id="celular"><?= $contact->celular ?></span><br>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="contact" data-anchor="<?= $anchor ?>" data-id="<?= $contact->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-contact">
                            <td class="no-data-found">Nenhum Contato Cadastrado</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="contact-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="contact"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-contact']) ?>
                <?= $this->Form->control('delete-contact-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->element('button_delete', ['id' => $broker->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>
    </div>
</div>
