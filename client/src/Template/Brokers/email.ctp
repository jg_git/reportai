<?= $this->Flash->render() ?>
<?=
$this->Form->create($email, [
    'id' => 'form-email-' . $anchor,
    'data-id' => $email->isNew() ? 'new' : $email->id,
    'data-error' => sizeof($email->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row">
    <div class="col">
        <?= $this->Form->control('email', ['type' => 'text', 'label' => 'Email']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
