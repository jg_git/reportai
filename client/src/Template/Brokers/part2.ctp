<script>
    $(document).ready(function () {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });

        $("#form-part2 input[id=cep]").inputmask("99999-999");

        $("#form-part2 input[id=cep]").focusout(function () {
            if (!$("#form-part2 input[id=logradouro]").val()) {
                var cep_value = $(this).val();
                var cep_stripped = cep_value.replace(/[^A-Za-z0-9]/g, '');
                var error = false;
                $.getJSON("<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'cep']) ?>" + "/" + cep_stripped, function (data) {
                    $.each(data, function (key, val) {
                        switch (key) {
                            case "logradouro":
                                $("#form-part2 input[id=logradouro]").val(val);
                                break;
                            case "bairro":
                                $("#form-part2 input[id=bairro]").val(val);
                                break;
                            case "localidade":
                                $("#form-part2 input[id=cidade]").val(val);
                                break;
                            case "uf":
                                $("#form-part2 select[id=estado]").val(val);
                                break;
                            case "erro":
                                error = true;
                        }
                    });
                    if (!error) {
                        $("#form-part2 input[id=numero]").focus();
                    }
                });
            }
        });

        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: "<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'bank']) ?>/%QUERY",
                wildcard: '%QUERY'
            }
        });
        $('#bank').typeahead(null, {
            source: engine,
            display: "name",
            templates: {
                notFound: '<div style="margin-left:10px">Banco não encontrado</div>',
                suggestion: Handlebars.compile(
                        '<div class="typeahead-item">' +
                        '{{code}} - {{name}}<br>' +
                        '</div>')
            }
        }).on('typeahead:select', function (ev, suggestion) {
            $("#bank-id").val(suggestion.id);
            $("#code").val(suggestion.code);
        }).on('typeahead:render', function (ev, suggestion, flag, ds) {
            if (suggestion.length === 0) {
                $("#bank-id").val("");
            }
        });

    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($broker, [
    'id' => 'form-part2',
    'data-id' => $broker->isNew() ? 'new' : $broker->id,
    'data-error' => sizeof($broker->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Endereço</h5>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cep', ['type' => 'text', 'label' => 'CEP']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('logradouro', ['type' => 'text', 'label' => 'Logradouro']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('numero', ['type' => 'text', 'label' => 'Número']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('bairro', ['type' => 'text', 'label' => 'Bairro']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cidade', ['type' => 'text', 'label' => 'Cidade']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('estado', ['options' => $states, 'label' => 'Estado']) ?>
    </div>
    <div class="col-md-3 col-sm-12">

    </div>
</div>
<?= $this->Form->end() ?>
