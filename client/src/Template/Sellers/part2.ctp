<script>
    $(document).ready(function () {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });


        $("#cep").inputmask("99999-999");

        $("#cep").focusout(function () {
            if (!$("#logradouro").val()) {
                var cep_value = $(this).val();
                var cep_stripped = cep_value.replace(/[^A-Za-z0-9]/g, '');
                var error = false;
                $.getJSON("<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'cep']) ?>" + "/" + cep_stripped, function (data) {
                    $.each(data, function (key, val) {
                        switch (key) {
                            case "logradouro":
                                $("#logradouro").val(val);
                                break;
                            case "bairro":
                                $("#bairro").val(val);
                                break;
                            case "localidade":
                                $("#cidade").val(val);
                                break;
                            case "uf":
                                $("#estado").val(val);
                                break;
                            case "erro":
                                error = true;
                        }
                    });
                    if (!error) {
                        $("#numero").focus();
                    }
                });
            }
        });

    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($seller, [
    'id' => 'form-part2',
    'data-id' => $seller->isNew() ? 'new' : $seller->id,
    'data-error' => sizeof($seller->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Endereço</h5>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cep', ['type' => 'text', 'label' => 'CEP']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('logradouro', ['type' => 'text', 'label' => 'Logradouro']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('numero', ['type' => 'text', 'label' => 'Número']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('bairro', ['type' => 'text', 'label' => 'Bairro']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cidade', ['type' => 'text', 'label' => 'Cidade']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('estado', ['options' => $states, 'label' => 'Estado']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
