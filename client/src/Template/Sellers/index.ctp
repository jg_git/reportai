<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Vendedores']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('razao_social', ['label' => 'Razão Social/Nome']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('cnpj_cpf', ['label' => 'CNPJ/CPF']) ?>
                        </div>
                        <div class="col-md-6 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover">
                    <tr>
                        <th>Razão Social/Nome</th>
                        <th>CNPJ/CPF</th>
                    </tr>
                    <?php
                    if (sizeof($sellers) > 0) {
                        foreach ($sellers as $seller) {
                            ?>
                            <tr>
                                <td><?= $this->Html->link($seller->razao_social, ['action' => 'edit', $seller->id]) ?></td>
                                <td><?= $seller->cnpj_cpf ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    