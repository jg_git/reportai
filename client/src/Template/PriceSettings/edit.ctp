<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Configurações de Preços']
]);
echo $this->Breadcrumbs->render();
?>
<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <?= $this->Form->create($settings, ['id' => 'form-settings']) ?>
                <h5 class="mt-3 mb-3">Preços Pré Vendas</h5>
                <div class="row mb-2">
                    <div class="col-md-6 col-sm-12">

                    </div>
                    <div class="col-md-2 col-sm-12">
                        Valor
                    </div>
                    <div class="col-md-2 col-sm-12">
                        De Dias
                    </div>
                    <div class="col-md-2 col-sm-12">
                        Até Dias
                    </div>
                </div>
                <?php
                foreach ($settings as $index => $setting) {
                    if (substr($setting->tag, 0, 9) == 'acrescimo') {
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div style="font-weight: bold"><?= $setting->descricao ?></div>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <?= $this->Form->control('PriceSettings.' . $index . '.tag', ['type' => 'hidden']) ?>
                                <?= $this->Form->control('PriceSettings.' . $index . '.valor', ['type' => 'text', 'label' => false]) ?>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <?= $this->Form->control('PriceSettings.' . $index . '.dias_de', ['type' => 'text', 'label' => false]) ?>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <?= $this->Form->control('PriceSettings.' . $index . '.dias_ate', ['type' => 'text', 'label' => false]) ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <h5 class="mt-3 mb-3">Comissões</h5>
                <div class="row mb-2">
                    <div class="col-md-6 col-sm-12">

                    </div>
                    <div class="col-md-2 col-sm-12">
                        Valor
                    </div>
                </div>
                <?php
                foreach ($settings as $index => $setting) {
                    if (substr($setting->tag, 0, 8) == 'comissao') {
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div style="font-weight: bold"><?= $setting->descricao ?></div>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <?= $this->Form->control('PriceSettings.' . $index . '.tag', ['type' => 'hidden']) ?>
                                <?= $this->Form->control('PriceSettings.' . $index . '.valor', ['type' => 'text', 'label' => false]) ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <h5 class="mt-3 mb-3">Descontos</h5>
                <div class="row mb-2">
                    <div class="col-md-6 col-sm-12">

                    </div>
                    <div class="col-md-2 col-sm-12">
                        Valor
                    </div>
                </div>
                <?php
                foreach ($settings as $index => $setting) {
                    if (substr($setting->tag, 0, 8) == 'desconto') {
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div style="font-weight: bold"><?= $setting->descricao ?></div>
                            </div>
                            <div class="col-md-2 col-sm-12">
                                <?= $this->Form->control('PriceSettings.' . $index . '.tag', ['type' => 'hidden']) ?>
                                <?= $this->Form->control('PriceSettings.' . $index . '.valor', ['type' => 'text', 'label' => false]) ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <h5 class="mt-3 mb-3">Outras configurações</h5>
                <?php
                foreach ($settings as $index => $setting) {
                    if (substr($setting->tag, 0, 5) == 'conta') {
                        ?>
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div style="font-weight: bold"><?= $setting->descricao ?></div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <?= $this->Form->control('PriceSettings.' . $index . '.tag', ['type' => 'hidden']) ?>
                                <?= $this->Form->control('PriceSettings.' . $index . '.valor', [
                                    'type' => 'select', 
                                    'options' => $accounts, 
                                    'label' => false,
                                    'empty' => true]) ?>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
