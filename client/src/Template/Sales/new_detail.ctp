<?php $anchor = rand() ?>
<div class="row" data-detail-anchor="<?= $anchor ?>">
    <?= $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.id', ['type' => 'hidden']) ?>
    <div class="col-md-4 col-sm-12">
        <div id="client-<?= $sale_index ?>-<?= $detail_index ?>"  style="font-size: larger; font-weight: bold"></div>
        <?=
        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.presale_id', [
            'type' => 'hidden'
        ])
        ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <?=
        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.nota_fiscal', [
            'type' => 'text',
            'label' => false])
        ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <?=
        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.volume', [
            'type' => 'text',
            'label' => false,
            'data-sale-index' => $sale_index,
            'data-detail-index' => $detail_index,
            'data-presale-id' => $presale_id
        ])
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?=
        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.account_id', [
            'type' => 'select',
            'options' => $accounts,
            'label' => false,
            'default' => $default_account->valor])
        ?>
    </div>
    <div class="col-md-1 col-sm-12">
        <i class="fa fa-trash" 
           data-detail-anchor="<?= $anchor ?>" 
           data-sale-index="<?= $sale_index ?>"
           data-detail-index="<?= $detail_index ?>">
        </i>
    </div>
</div>
