<?= $this->Form->control('Sales.' . $sale_index . '.purchases.' . $purchase_index . '.id', ['type' => 'hidden', 'value' => $purchase->id]) ?>
<div class="row">
    <div class="col-md-5">
        <span class="standout"><?= $purchase->numero_contrato ?></span>
        <br>
        <span class="mr-3">Usina: <?= $purchase->plant->nome_fantasia ?></span><br>
        <span>Distribuidora: <?= $purchase->distributor->nome_fantasia ?></span>
    </div>
    <div class="col-md-2 col-sm-12">
        <label>Volume</label>
        <div id="purchase-volume-<?= $sale_index ?>-<?= $purchase_index ?>" class="standout">0</div>
        <?= $this->Form->control('Sales.' . $sale_index . '.purchases.' . $purchase_index . '._joinData.volume', ['type' => 'hidden', 'value' => 0]) ?>
    </div>
    <div class="col-md-2 col-sm-12">
        <label>Estoque</label>
        <div id="estoque-<?= $sale_index ?>-<?= $purchase_index ?>" class="standout">
            <?= number_format($purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito - $purchase->volume_vendido, 0, ',', '.') ?>
        </div>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('Sales.' . $sale_index . '.purchases.' . $purchase_index . '._joinData.spread', ['type' => 'text', 'label' => 'Spread (R$)', 'value' => 0]) ?>
    </div>
</div>