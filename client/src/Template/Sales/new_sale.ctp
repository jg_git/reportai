<?php $sale_anchor = rand() ?>
<div class="row" data-sale-anchor="<?= $sale_anchor ?>">
    <div class="col">
        <div class="border border-dark bg-primary p-3 mb-2">
            <?= $this->Form->control('Sales.' . $sale_index . '.data_venda', ['type' => 'hidden']) ?>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="row">
                        <div class="col">
                            <?=
                            $this->Form->control('Sales.' . $sale_index . '.carrier_id', [
                                'type' => 'select',
                                'options' => $carriers,
                                'label' => 'Transportadora',
                                'empty' => true,
                                'data-sale-index' => $sale_index])
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div id="driver-<?= $sale_index ?>" class="col">
                            <?=
                            $this->Form->control('Sales.' . $sale_index . '.driver_id', [
                                'type' => 'select',
                                'options' => [],
                                'label' => 'Motorista',
                                'empty' => true,
                                'data-sale-index' => $sale_index])
                            ?>
                        </div>
                        <div id="driver-spinner-<?= $sale_index ?>" class="col" style="display: none">
                            <?= $this->Html->image('spinner.gif') ?>
                        </div>
                    </div>
                    <div class="row">
                        <div id="vehicle-<?= $sale_index ?>" class="col">
                            <?=
                            $this->Form->control('Sales.' . $sale_index . '.vehicle_id', [
                                'type' => 'select',
                                'options' => [],
                                'label' => 'Veículo',
                                'empty' => true,
                                'data-sale-index' => $sale_index])
                            ?>
                        </div>
                        <div id="vehicle-spinner-<?= $sale_index ?>" class="col" style="display: none">
                            <?= $this->Html->image('spinner.gif') ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div class="row mb-2">
                        <div class="col">
                            <button type="button" 
                                    class="btn btn-primary mr-2" 
                                    data-toggle="modal" 
                                    data-target="#purchases" 
                                    onclick="$('#sale-index').val(<?= $sale_index ?>)">Estoque</button>
                            <button  id="button-presales-<?= $sale_index ?>"
                                     type="button" 
                                     class="btn btn-primary" 
                                     data-toggle="modal" 
                                     data-target="#presales"
                                     onclick="$('#sale-index').val(<?= $sale_index ?>)" disabled>Pré Vendas</button>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col">
                                    <label>Volume Veículo</label>
                                    <div id="capacidade-<?= $sale_index ?>" class="standout">0</div> 
                                </div>
                                <div class="col">
                                    <label>Volume Total</label>
                                    <div id="volume-total-<?= $sale_index ?>" class="standout">0</div>
                                    <?= $this->Form->control('Sales.' . $sale_index . '.volume_total', ['type' => 'hidden']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 mt-2">
                                    <?= $this->Form->control('Sales.' . $sale_index . '.frete', ['type' => 'text', 'label' => 'Frete (R$)', 'value' => 0]) ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <label>Contratos</label>
                            <div id="purchases-<?= $sale_index ?>">
                                <?= $this->Form->control('purchase_index_' . $sale_index, ['type' => 'hidden', 'value' => 0]) ?>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-12">
                            <i class="fa fa-trash" data-sale-anchor="<?= $sale_anchor ?>" data-sale-index="<?= $sale_index ?>"></i>
                        </div>
                    </div>
                    <hr>
                    <div class="row mb-1">
                        <div class="col-md-4 col-sm-12">Cliente</div>
                        <div class="col-md-2 col-sm-12">Nota Fiscal</div>
                        <div class="col-md-2 col-sm-12">Volume Vendido</div>
                        <div class="col-md-2 col-sm-12">Conta</div>
                    </div>
                    <div id="details-<?= $sale_index ?>">
                        <?= $this->Form->control('detail_index_' . $sale_index, ['type' => 'hidden', 'value' => 0]) ?>
                        <div id="no-data-<?= $sale_index ?>" class="p-3 no-data-found">
                            Nenhum detalhe encontrado
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>