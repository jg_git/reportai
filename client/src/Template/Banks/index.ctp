<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Bancos']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <?= $this->Form->control('nome', ['label' => 'Nome']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12 mt-4 mb-2">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th>Número</th>
                        <th>Nome</th>
                    </tr>
                    <?php
                    if (sizeof($banks) > 0) {
                        foreach ($banks as $bank) {
                            ?>
                            <tr>
                                <td><?= $this->Html->link($bank->codigo, ['action' => 'edit', $bank->id]) ?></td>
                                <td><?= $bank->nome ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    