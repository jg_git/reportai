<script>
     $(document).ready(function () {
        
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        $('#check-all').on('ifUnchecked', function (event) {
            $('.icheck-flat input').iCheck('uncheck');
        });

        $('#check-all').on('ifChecked', function (event) {
            $('.icheck-flat input').iCheck('check');
        });

    });
    $('#check-all').on('ifUnchecked', function (event) {
            $('.icheck-flat input').iCheck('uncheck');
        });

        $('#check-all').on('ifChecked', function (event) {
            $('.icheck-flat input').iCheck('check');
        });
     
    $("input[name=inicio]").datepicker({
        format: "dd/mm/yyyy hh/mm",
        todayBtn: true,
        language: "pt-BR",
        orientation: "bottom"
    });    
    
</script>
<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Serviços', 'url' => ['action' => 'index?ativo=1']],
    ['title' => 'Visualização de Serviços']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
    <?=
        $this->Form->create($job, [
            'enctype' => 'multipart/form-data',
            'id' => 'form-active',
            'data-id' => $job->isNew() ? 'new' : $job->id,
            'data-error' => sizeof($job->getErrors()) > 0 ? 'Y' : 'N'])
    ?>
        <div class="row">
                <div class="col-md-6 col-sm-12">
                    <label>Nome do serviço</label> <span style="color: red">*</span>
                    <?= $this->Form->control('nome_servico', ['type'=>'text','label' => false, 'disabled' => true]) ?>
                </div>
            <div class="col-md-6 col-sm-12">  
            <?php
                $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";             
                if(strpos($checagem, "view")==true){
                        echo $this->Form->control('formulario_id', ['type' => 'select', 'options' => $polls, 'label' => 'Formulário', 'disabled' => true]);
                    }else{
                        echo '<div style="margin-bottom: .5rem">';
                        echo '<label for="formulario_id" style="display: inline; margin-bottom: .5rem">Formulário</label>';
                        echo '</div>';
                        echo $this->Form->control('formulario_id', ['type' => 'select', 'options' => $polls, 'empty' => 'SELECIONE UM FORMULÁRIO', 'label' => false]);
                    }
                ?>         
            </div>            
        </div>
        <div class="row">
                <div class="col-md-12 col-sm-12">
                    <label>Instruções para execução</label> <span style="color: red">*</span>
                    <?= $this->Form->control('instrucoes', ['label' => false, 'type'=> 'textarea', 'disabled' => true]) ?>
                </div>
        </div>
        <div class="row">
        <div class="col-md-4 col-sm-12">
                    <label>Status</label> <span style="color: red">*</span>
                    <?= $this->Form->control('status', ['label' => false, 'disabled' => true]) ?>
            </div>
            <div class="col-md-4 col-sm-12"> 
                <label>Data e Hora para Início</label><span style="color: red">*</span>
                <?php
                    if(strpos($checagem, "view")==true){
                        echo $this->Form->control('inicio' , ['type' => 'text', 'label' => false, 'disabled' => true]);
                    }else{
                        if (!$job->isNew()) {
                            echo $this->Form->control('inicio', ['type' => 'text', 'label' => false]);
                        } else {
                            echo $this->Form->control('inicio', ['type' => 'text', 'label' => false]);
                        }
                    }
                ?> 
            </div>
            <div class="col-md-4 col-sm-12">
            <label>Data e Hora para Fim</label><span style="color: red">*</span>
                <?php
                    if(strpos($checagem, "view")==true){
                        echo $this->Form->control('final' , ['type' => 'text', 'label' => false, 'disabled' => true]);
                    }else{
                        if (!$job->isNew()) {
                            echo $this->Form->control('final', ['type' => 'text', 'label' => false]);
                        } else {
                            echo $this->Form->control('final', ['type' => 'text', 'label' => false]);
                        }
                    }
                ?> 
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-6 col-sm-12">  
            <?php
                $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";             
                if(strpos($checagem, "view")==true){
                        echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'label' => 'Cliente', 'disabled' => true]);
                    }else{
                        echo '<div style="margin-bottom: .5rem">';
                        echo '<label for="client_id" style="display: inline; margin-bottom: .5rem">Cliente</label>';
                        echo '</div>';
                        echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'empty' => 'SELECIONE UM CLIENTE', 'label' => false]);
                    }
                ?>         
            </div>
            <div class="col-md-6 col-sm-12">  
            <?php
                $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";             
                if(strpos($checagem, "view")==true){
                        echo $this->Form->control('local_id', ['type' => 'select', 'options' => $locations, 'label' => 'Local', 'disabled' => true]);
                    }else{
                        echo '<div style="margin-bottom: .5rem">';
                        echo '<label for="local_id" style="display: inline; margin-bottom: .5rem">Local</label>';
                        echo '</div>';
                        echo $this->Form->control('local_id', ['type' => 'select', 'options' => $locations, 'empty' => 'SELECIONE UM LOCAL', 'label' => false]);
                    }
                ?>         
            </div>            
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                    <label>Relatório</label>
                    <br>
                    <a href="http://localhost/vita3modelo/client/polls/edit/5">Nenhum Relatório preenchido</a>
                    
            </div>
        </div>
        <br>
        <div class="row">
                    <div class="col-md-3 col-sm-12">
                    <?php
                        if(strpos($checagem, "view")==true){
                            echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label>';
                            $usuariospresentes = array();
                            foreach($userjob as $user_unico){
                                if ($job->id == $user_unico['job_id']){
                                    array_push($usuariospresentes, $user_unico['user_id']);
                                }
                            }
                            foreach($users as $id => $nome){
                                if(in_array($id, $usuariospresentes)){
                                    echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true, 'disabled' => true])."</div>";
                                }
                                else{
                                    echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'disabled' => true])."</div>";
                                }
                            }
                        ?>
                        <div style="margin-bottom: .5rem">
                        </div>
                        <?php } else {  ?>
                        <div style="margin-bottom: .5rem">
                            <?php
                                echo '<label for="users" style="margin-bottom: .5rem">Usuários Responsáveis</label>';
                                echo "<div>". $this->Form->control('check-all', ['type' => 'checkbox', 'value' => 'todos', 'label' => 'Selecionar Todos'])."</div>";
                                $usuariospresentes = array();
                                foreach($userjob as $user_unico){
                                    if ($job->id == $user_unico['job_id']){
                                        array_push($usuariospresentes, $user_unico['user_id']);
                                    }
                                }
                                foreach($users as $id => $nome){
                                    if(in_array($id, $usuariospresentes)){
                                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome, 'checked' => true])."</div>";
                                    }
                                    else{
                                        echo "<div>". $this->Form->control($id, ['type' => 'checkbox', 'value' => $id, 'label' => $nome])."</div>";
                                    }
                                }
                            ?>
                    </div>
                            <?php } 
                            ?>
                </div> 
                
        </div>
        <div class="row">
                <div class="col-md-12 col-sm-12">
                    <?= $this->Form->end() ?>
                    <?= $this->element('button_return_client') ?>
        </div>          
    </div>
</div>