<!-- Custom barra de andamentos -->
<style>
    .nav-tabs>li>a{
    font-weight: bold;
}
</style>
<script>
    $(document).ready(function () {
        $("button").click(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
        $("button").focus(function(){
        var valorid = $("#userid").val();
            if(valorid == ""){
                $('#user-id').val("");
                $('#user_id').val("");
            }else{
                var item2 = $('#userid').val();
                item2 = ', '+item2+' ,';
                $('#user-id').val(item2);
                $('#user_id').val(item2);
            }
        });
    });


    
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Serviços']
]);
echo $this->Breadcrumbs->render();
?>
<?php 
?>
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('nome_servico', ['label' => 'Nome Serviço']) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('client_id', ['label' => 'Responsável', 'options' => $clients2, 'empty' => true]) ?>
                        </div>
                        
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('fomulario_id', ['label' => 'Formulário', 'options' => $polls, 'empty' => true]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('local_id', ['label' => 'Local', 'options' => $locations, 'empty' => true]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('inicio', ['label' => 'Data e Hora para Início']) ?>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-12">
                            <?= $this->Form->control('id', ['type' => 'select', 'options' => $users2, 'empty' => 'SELECIONE UM USUÁRIO', 'label' => 'Usuário Responsável']) ?>
                            <?= $this->Form->control('user_id', ['type' => 'hidden', 'label' => false]) ?>
                        </div>

                        <div class="col-md-2 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear_client') ?>
                        </div>

                    </div>

                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', ['action' => 'edit']) ?> 
                    </div>
                </div>
                <section class="content">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <!-- Nav Tabs -->
                                <ul class="nav nav-tabs justify-content-center" roles="tablist">
                                    <li role="presentation" class="active"><a class="nav-link active" href="#realizar" aria-controls="realizar" role="tab" data-toggle="tab">Planejar</a></li>
                                    <li role="presentation" ><a class="nav-link" href="#andamento" aria-controls="andamento" role="tab" data-toggle="tab">Acompanhar </a></li>
                                    <li role="presentation" ><a class="nav-link" href="#finalizado" aria-controls="finalizado" role="tab" data-toggle="tab">Finalizado</a></li>
                                    <li role="presentation" ><a class="nav-link" href="#revisao" aria-controls="revisao" role="tab" data-toggle="tab">Revisão</a></li>
                                </ul> 

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="realizar">
                                        <table class="table table-hover table-responsive-sm">
                                            <tr>
                                                <th><?= $this->Paginator->sort('Jobs.nome_servico', 'Nome do Serviço') ?></th>
                                                <th><?= $this->Paginator->sort('Clients.razao_social', 'Cliente') ?></th>
                                                <th><?= $this->Paginator->sort('Polls.nome_formulario', 'Formulário') ?></th>
                                                <th><?= $this->Paginator->sort('Locations.nome_local', 'Local') ?></th>
                                                <th><?= $this->Paginator->sort('Jobs.inicio', 'Data e Hora para Início') ?></th>
                                                
                                                <th>Usuários Responsáveis</th>
                                                <th>Ações</th>
                                            </tr>
                                            <?php

                                            $id_userjobs = array();
                                            
                                            foreach($user_jobs as $userjob){
                                                $data= '['.implode(' ,', [ 'user_id' => $userjob->user_id,
                                                            'job_id' => $userjob->job_id
                                                        ]).']';
                                                array_push($id_userjobs, $data);
                                            }

                                            $contagem=0;
                                            if (sizeof($jobs) > 0) {
                                                foreach ($jobs as $job) {
                                                    
                                                    $user_id = $this->request->session()->read('Auth.User.id');
                                                    $job_id = $job->id;
                                                    $micro_data = '['.implode(' ,', [ 'user_id' => $user_id,
                                                            'job_id' => $job_id
                                                        ]).']';
                                                    if($job->status == 1){
                                                        if((in_array($micro_data, $id_userjobs)) OR (($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id') AND $this->request->session()->read('Auth.User.prop_id') == $job->prop_id))){
                                                            $contagem=$contagem+1;
                                                    }
                                            ?>

                                                    <tr>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->nome_servico ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->client->razao_social ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->poll->nome_formulario ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->location->nome_local ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->inicio ?></div></nav></td>
                                                        
                                                        <td style="padding-bottom: 0; padding-left: 0">
                                                            <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                                                <div class="d-flex align-items-center justify-content-center h-100">
                                                                    <ul class="navbar-nav ml-auto">
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                                                <i class="fa fa-user-circle"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                                <?php 
                                                                                    foreach($user_jobs as $userjob_hov){
                                                                                        if($userjob_hov->job_id == $job_id){
                                                                                            $nome_proprio="";
                                                                                            foreach($users as $user){
                                                                                                if ($user->id == $userjob_hov->user_id){
                                                                                                    $nome_proprio=$user->nome;
                                                                                                }
                                                                                            }
                                                                                            echo '<a class="dropdown-item">';
                                                                                                echo '<span>'. $nome_proprio.'</span>';
                                                                                            echo '</a>';
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </div>
                                                                        </li>
                                                                    </ul> 
                                                                </div>
                                                            </nav>         
                                                        </td>

                                                        <td style="padding-bottom: 0; padding-left: 0">
                                                            <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                                                <div class="d-flex align-items-center justify-content-center h-100">
                                                                    <ul class="navbar-nav ml-auto">
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                                                <i class="fa fa-cog"></i>
                                                                                <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Jobs', 'action' => 'view', $job->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Jobs', 'action' => 'edit', $job->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                                <?php 
                                                                                    echo $this->element('a_delete', ['id' => $job->id]);
                                                                                ?>
                                                                                <?php if($job->ativo == "1" OR $job->ativo == 1){ ?>
                                                                                <?= $this->element('a_deactivate', ['id' => $job->id.'i'])?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </li>
                                                                    </ul> 
                                                                </div>
                                                            </nav>         
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
                                    </div>  
                                    <div role="tabpanel" class="tab-pane" id="andamento">
                                    <table class="table table-hover table-responsive-sm">
                                            <tr>
                                                <th><?= $this->Paginator->sort('Jobs.nome_servico', 'Nome do Serviço') ?></th>
                                                <th><?= $this->Paginator->sort('Clients.razao_social', 'Cliente') ?></th>
                                                <th><?= $this->Paginator->sort('Polls.nome_formulario', 'Formulário') ?></th>
                                                <th><?= $this->Paginator->sort('Locations.nome_local', 'Local') ?></th>
                                                <th><?= $this->Paginator->sort('Jobs.inicio', 'Data e Hora para Início') ?></th>
                                                <th>Usuários Responsáveis</th>
                                                <th>Ações</th>
                                            </tr>
                                            <?php

                                            $id_userjobs = array();
                                            
                                            foreach($user_jobs as $userjob){
                                                $data= '['.implode(' ,', [ 'user_id' => $userjob->user_id,
                                                            'job_id' => $userjob->job_id
                                                        ]).']';
                                                array_push($id_userjobs, $data);
                                            }

                                            $contagem=0;
                                            if (sizeof($jobs) > 0) {
                                                foreach ($jobs as $job) {
                                                    
                                                    $user_id = $this->request->session()->read('Auth.User.id');
                                                    $job_id = $job->id;
                                                    $micro_data = '['.implode(' ,', [ 'user_id' => $user_id,
                                                            'job_id' => $job_id
                                                        ]).']';
                                                        if($job->status == 2){
                                                            if((in_array($micro_data, $id_userjobs)) OR (($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id') AND $this->request->session()->read('Auth.User.prop_id') == $job->prop_id))){
                                                                $contagem=$contagem+1;
                                                        }
                                            ?>
                                                    <tr>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->nome_servico ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->client->razao_social ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->poll->nome_formulario ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->location->nome_local ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->inicio ?></div></nav></td>
                                                        <td style="padding-bottom: 0; padding-left: 0">
                                                            <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                                                <div class="d-flex align-items-center justify-content-center h-100">
                                                                    <ul class="navbar-nav ml-auto">
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                                                <i class="fa fa-user-circle"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                                <?php 
                                                                                    foreach($user_jobs as $userjob_hov){
                                                                                        if($userjob_hov->job_id == $job_id){
                                                                                            $nome_proprio="";
                                                                                            foreach($users as $user){
                                                                                                if ($user->id == $userjob_hov->user_id){
                                                                                                    $nome_proprio=$user->nome;
                                                                                                }
                                                                                            }
                                                                                            echo '<a class="dropdown-item">';
                                                                                                echo '<span>'. $nome_proprio.'</span>';
                                                                                            echo '</a>';
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </div>
                                                                        </li>
                                                                    </ul> 
                                                                </div>
                                                            </nav>         
                                                        </td>

                                                        <td style="padding-bottom: 0; padding-left: 0">
                                                            <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                                                <div class="d-flex align-items-center justify-content-center h-100">
                                                                    <ul class="navbar-nav ml-auto">
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                                                <i class="fa fa-cog"></i>
                                                                                <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Jobs', 'action' => 'view', $job->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Jobs', 'action' => 'edit', $job->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                                <?php 
                                                                                    echo $this->element('a_delete', ['id' => $job->id]);
                                                                                ?>
                                                                                <?php if($job->ativo == "1" OR $job->ativo == 1){ ?>
                                                                                <?= $this->element('a_deactivate', ['id' => $job->id.'i'])?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </li>
                                                                    </ul> 
                                                                </div>
                                                            </nav>         
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="finalizado">
                                    <table class="table table-hover table-responsive-sm">
                                            <tr>
                                                <th><?= $this->Paginator->sort('Jobs.nome_servico', 'Nome do Serviço') ?></th>
                                                <th><?= $this->Paginator->sort('Clients.razao_social', 'Cliente') ?></th>
                                                <th><?= $this->Paginator->sort('Polls.nome_formulario', 'Formulário') ?></th>
                                                <th><?= $this->Paginator->sort('Locations.nome_local', 'Local') ?></th>
                                                <th><?= $this->Paginator->sort('Jobs.inicio', 'Data e Hora para Início') ?></th>
                                                <th>Usuários Responsáveis</th>
                                                <th>Ações</th>
                                            </tr>
                                            <?php

                                            $id_userjobs = array();
                                            
                                            foreach($user_jobs as $userjob){
                                                $data= '['.implode(' ,', [ 'user_id' => $userjob->user_id,
                                                            'job_id' => $userjob->job_id
                                                        ]).']';
                                                array_push($id_userjobs, $data);
                                            }

                                            $contagem=0;
                                            if (sizeof($jobs) > 0) {
                                                foreach ($jobs as $job) {
                                                    
                                                    $user_id = $this->request->session()->read('Auth.User.id');
                                                    $job_id = $job->id;
                                                    $micro_data = '['.implode(' ,', [ 'user_id' => $user_id,
                                                            'job_id' => $job_id
                                                        ]).']';
                                                        if($job->status == 3){
                                                            if((in_array($micro_data, $id_userjobs)) OR (($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id') AND $this->request->session()->read('Auth.User.prop_id') == $job->prop_id))){
                                                                $contagem=$contagem+1;
                                                        }
                                            ?>
                                                    <tr>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->nome_servico ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->client->razao_social ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->poll->nome_formulario ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->location->nome_local ?></div></nav></td>
                                                        <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $job->inicio ?></div></nav></td>
                                                        <td style="padding-bottom: 0; padding-left: 0">
                                                            <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                                                <div class="d-flex align-items-center justify-content-center h-100">
                                                                    <ul class="navbar-nav ml-auto">
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                                                <i class="fa fa-user-circle"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                                <?php 
                                                                                    foreach($user_jobs as $userjob_hov){
                                                                                        if($userjob_hov->job_id == $job_id){
                                                                                            $nome_proprio="";
                                                                                            foreach($users as $user){
                                                                                                if ($user->id == $userjob_hov->user_id){
                                                                                                    $nome_proprio=$user->nome;
                                                                                                }
                                                                                            }
                                                                                            echo '<a class="dropdown-item">';
                                                                                                echo '<span>'. $nome_proprio.'</span>';
                                                                                            echo '</a>';
                                                                                        }
                                                                                    }
                                                                                ?>
                                                                            </div>
                                                                        </li>
                                                                    </ul> 
                                                                </div>
                                                            </nav>         
                                                        </td>

                                                        <td style="padding-bottom: 0; padding-left: 0">
                                                            <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                                                <div class="d-flex align-items-center justify-content-center h-100">
                                                                    <ul class="navbar-nav ml-auto">
                                                                        <li class="nav-item dropdown">
                                                                            <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                                                <i class="fa fa-cog"></i>
                                                                                <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                                            </a>
                                                                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Jobs', 'action' => 'view', $job->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Jobs', 'action' => 'edit', $job->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                                <?php 
                                                                                    echo $this->element('a_delete', ['id' => $job->id]);
                                                                                ?>
                                                                                <?php if($job->ativo == "1" OR $job->ativo == 1){ ?>
                                                                                <?= $this->element('a_deactivate', ['id' => $job->id.'i'])?>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </li>
                                                                    </ul> 
                                                                </div>
                                                            </nav>         
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    }
                                                }
                                            } else {
                                                ?>
                                                <tr>
                                                    <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                        <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="revisao"> 
                                        <!-- <p>Será implantado no futuro</p> -->
                                    </div>
                            </div>  
                        </div>
                    </div>
                </section>
           </div> 
        </div>
    </div>
</div>