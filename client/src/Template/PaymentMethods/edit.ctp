<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Formas de Pagamento', 'url' => ['action' => 'index']],
    ['title' => 'Forma de Pagamento']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <?=
                $this->Form->create($method, [
                    'id' => 'form-method',
                    'data-id' => $method->isNew() ? 'new' : $method->id,
                    'data-error' => sizeof($method->getErrors()) > 0 ? 'Y' : 'N'])
                ?>
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <?= $this->Form->control('descricao') ?>
                    </div>
                </div>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_delete', ['id' => $method->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>    
    </div>
</div>
