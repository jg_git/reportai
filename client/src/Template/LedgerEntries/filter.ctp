<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {


        // $("#p").hide(0);
        //   $("#paginator").hide(0);
        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        $("input[name=data_range]").daterangepicker(configRangePicker, function(start, end, label) {

        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=date_start]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=date_end]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=date_start]").val("");
            $("input[name=date_end").val("");
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Lançamentos']
]);
echo $this->Breadcrumbs->render();
?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                            <?= $this->Form->create(
                                'Filter',
                                ['type' => 'GET'],
                                ['url' => [
                                    'controller' => 'LedgerEntries',
                                    'action' => 'filter'
                                ]],
                                ['valueSources' => 'query']

                            ); ?>
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <?= $this->Form->control('account_id', ['label' => 'Conta', 'options' => $accounts, 'empty' => true]) ?>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <?= $this->Form->control('ledger_entry_type_id', ['type' => 'select', 'options' => $entry_types, 'label' => 'Tipo de Lançamento', 'empty' => true]) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 col-sm-12">
                                    <?= $this->Form->control('data_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                                    <?= $this->Form->control('date_start', ['type' => 'hidden']) ?>
                                    <?= $this->Form->control('date_end', ['type' => 'hidden']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <?= $this->element('button_filter') ?>
                                    <?= $this->element('button_clear_p') ?>
                                    <button class="btn btn-lg btn-primary nohover pull-left" onClick="redirectReport()">GERAR PDF</button>
                                    <script>
                                        function redirectReport() {
                                            //     window.open("/" + $(this).data("client-id") + "/report.pdf")
                                            window.print();
                                        }
                                        //window.open(routing.find(x => x.key === route).route + "/report.pdf"); 
                                    </script>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover slim-row">
                    <tr>
                        <th width="35%">Conta</th>
                        <th width="35%">Tipo de Lançamento</th>
                        <th width="15%">Data</th>
                        <th width="15%">Valor</th>
                    </tr>
                    <?php
                    if (sizeof($entries) > 0) {
                        foreach ($entries as $entry) {
                            $ac = $entry->account;
                            $type = $entry->ledger_entry_type;
                            ?>
                            <tr>
                                <td>
                                    <?php
                                            switch ($ac->tipo) {
                                                case 'I':
                                                    $msg = 'Interna ' . $ac->distributor->razao_social;
                                                    break;
                                                case 'F':
                                                    $msg = 'Financeira ' . $ac->bank->nome . ' - ' . $ac->titular . ' - ' . $ac->numero_agencia . ' - ' . $ac->numero_conta;
                                                    break;
                                                case 'C':
                                                    $msg = 'Caixa ' . $ac->titular;
                                                    break;
                                            }
                                            echo $this->Html->link($msg, ['controller' => 'LedgerEntries', 'action' => 'edit', $entry->id]);
                                            ?>
                                </td>
                                <td><?= $type->descricao ?></td>
                                <td><?= $entry->data_lancamento ?></td>
                                <td class="money">
                                    <div class="<?= $type->dc == 'C' ? 'credit' : 'debit' ?>"><?= $this->Number->currency($entry->valor) ?> <?= $type->dc ?></div>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <div>
                    <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
                </div>
            </div>
        </div>
    </div>
</div>