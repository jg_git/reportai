<script>
    $(document).ready(function () {
        
    });
</script>

<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - SERÁ MODIFICADA FUTURAMENTE
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Dados Contratos']
]);
echo $this->Breadcrumbs->render();
?>


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>

                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('cnpj_cpf', ['type' => 'text', 'label' => 'CPF/CNPJ']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('razao_social', ['label' => 'Nome do Cliente']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', []) ?>
                        </div>
                </div>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('cpf_cnpj', 'CPF/CNPJ') ?></th>
                        <th><?= $this->Paginator->sort('razao_social', 'Nome do Cliente') ?></th>
                        <th><?= $this->Paginator->sort('valor', 'Valor do Contrato') ?></th>
                        <th><?= $this->Paginator->sort('prazo', 'Prazo do Contrato (meses)') ?></th>
                        <th><?= $this->Paginator->sort('actions', 'Ações') ?></th>
                    </tr>
                    <?php
                    $contador=0;
                    if (sizeof($deals) > 0) {
                        if(!isset($_GET['cnpj_cpf'])){
                        foreach ($deals as $deal) {
                            
                            $converte_d1="".$deal->data_vigencia_termino;
                            $converte_d2="".$deal->data_vigencia_inicio;
                            
                            $converte_d1=substr($converte_d1, 6,4)."-".substr($converte_d1, 3,2)."-".substr($converte_d1, 0,2)." 00:00:00";
                            $converte_d2=substr($converte_d2, 6,4)."-".substr($converte_d2, 3,2)."-".substr($converte_d2, 0,2)." 00:00:00";
                            $data_vigencia_ate = new DateTime($converte_d1);
                            $data_vigencia_de = new DateTime($converte_d2);
                            $intervalo = $data_vigencia_de->diff($data_vigencia_ate);
                            $prazo = $intervalo->m;
                            $cnpjformatar=$deal->client->cnpj_cpf;
                            $newa="";
                            if (strlen($cnpjformatar)==14){
                                $newa= substr($cnpjformatar, 0, 2).'.'.substr($cnpjformatar,2,3).'.'.substr($cnpjformatar,5,3).'/'.substr($cnpjformatar,8,4).'-'.substr($cnpjformatar,12,2);
                            }else if(strlen($cnpjformatar)==11){
                                $newa= substr($cnpjformatar,0,3).'.'.substr($cnpjformatar,3,3).'.'.substr($cnpjformatar,6,3).'-'.substr($cnpjformatar,9,2);
                            }else{
                                $newa=$cnpjformatar;
                            }
                            if ($deal->client->ativo!='0'){
                                $contador=$contador+1;
                            ?>
                            <tr>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $newa ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $deal->client->razao_social ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $this->Number->currency($deal->valor)?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $prazo?></div></nav></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Deals', 'action' => 'view', $deal->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Deals', 'action' => 'edit', $deal->id], ['class' => 'dropdown-item', 'escape' => false]) ?>

                                                            <?php 
                                                                if(implode(', ', $deal->deal_payments) == ''){
                                                                    echo $this->element('a_delete', ['id' => $deal->id]);
                                                                }
                                                            ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                            <?php
                            }
                        }
                    }else{
                        $cnpjGet=str_replace(array(".","-","/","%2F"), "", $_GET["cnpj_cpf"]);
                        foreach ($deals as $deal) {
                            
                            $converte_d1="".$deal->data_vigencia_termino;
                            $converte_d2="".$deal->data_vigencia_inicio;
                            
                            $converte_d1=substr($converte_d1, 6,4)."-".substr($converte_d1, 3,2)."-".substr($converte_d1, 0,2)." 00:00:00";
                            $converte_d2=substr($converte_d2, 6,4)."-".substr($converte_d2, 3,2)."-".substr($converte_d2, 0,2)." 00:00:00";
                            $data_vigencia_ate = new DateTime($converte_d1);
                            $data_vigencia_de = new DateTime($converte_d2);
                            $intervalo = $data_vigencia_de->diff($data_vigencia_ate);
                            $prazo = $intervalo->m;
                            $cnpjformatar=$deal->client->cnpj_cpf;
                            $newa="";
                            if (strlen($cnpjformatar)==14){
                                $newa= substr($cnpjformatar, 0, 2).'.'.substr($cnpjformatar,2,3).'.'.substr($cnpjformatar,5,3).'/'.substr($cnpjformatar,8,4).'-'.substr($cnpjformatar,12,2);
                            }else if(strlen($cnpjformatar)==11){
                                $newa= substr($cnpjformatar,0,3).'.'.substr($cnpjformatar,3,3).'.'.substr($cnpjformatar,6,3).'-'.substr($cnpjformatar,9,2);
                            }else{
                                $newa=$cnpjformatar;
                            }
                            if ($deal->client->ativo!='0'){
                                
                                if(strpos($deal->client->cnpj_cpf, $cnpjGet)!== false){
                                    $contador=$contador+1;
                                
                                ?>
                                <tr>
                                    <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $newa ?></div></nav></td>
                                    <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $deal->client->razao_social ?></div></nav></td>
                                    <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $this->Number->currency($deal->valor)?></div></nav></td>
                                    <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $prazo?></div></nav></td>
                                    <td style="padding-bottom: 0; padding-left: 0">
                                        <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                            <div class="d-flex align-items-center justify-content-center h-100">
                                                <ul class="navbar-nav ml-auto">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                            <i class="fa fa-cog"></i>
                                                            <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Deals', 'action' => 'view', $deal->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                                <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Deals', 'action' => 'edit', $deal->id], ['class' => 'dropdown-item', 'escape' => false]) ?>

                                                                <?php 
                                                                    if(implode(', ', $deal->deal_payments) == ''){
                                                                        echo $this->element('a_delete', ['id' => $deal->id]);
                                                                    }
                                                                ?>
                                                        </div>
                                                    </li>
                                                </ul> 
                                            </div>
                                        </nav>         
                                    </td>
                                </tr>
                                <?php
                                }
                            }
                        }
                    }
                    } if($contador==0) {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    