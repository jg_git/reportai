<script>
    $(document).ready(function () {
        
        $("input[name=dt_pagamento]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
        $("input[name=valor_pagamento]").maskMoney({thousands: '.', decimal: ','}).maskMoney('mask', <?= $payment->valor_pagamento ?>);
        
        $("input[name=dt_emissao]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });

    });   
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($payment, [
    'id' => 'form-payment-' . $anchor,
    'data-id' => $payment->isNew() ? 'new' : $payment->id,
    'data-error' => sizeof($payment->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
    
    
?>
<div class="row">
    <div class="col">
        <?= $this->Form->control('nota_fiscal', ['type' => 'text', 'label' => 'Nº Nota Fiscal', 'disabled'=> !$payment->isNew()]) ?>
    </div>
    <div class="col">
        <?= $this->Form->control('valor_pagamento', ['type' => 'text', 'label' => 'Valor']) ?>
    </div>
    <div class="col">
    <?php
        if (!$payment->isNew()) {
            echo $this->Form->control('dt_emissao', ['type' => 'text', 'label' => 'Emissão']);
        } else {
            echo $this->Form->control('dt_emissao', ['type' => 'text', 'label' => 'Emissão', 'value' => date('d/m/Y')]);
        }
    ?>    
    </div>
    <div class="col">
    <?php
        if (!$payment->isNew()) {
            echo $this->Form->control('dt_pagamento', ['type' => 'text', 'label' => 'Pagamento']);
        } else {
            echo $this->Form->control('dt_pagamento', ['type' => 'text', 'label' => 'Pagamento', 'value' => date('d/m/Y')]);
        }
    ?>    
    </div>
</div>
<?= $this->Form->end() ?>
