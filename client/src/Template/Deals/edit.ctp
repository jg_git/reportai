<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>
<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>
<script>

    $(document).ready(function() {
        
        $("#part1").load("<?= $this->Url->build(['action' => 'part1', $deal->isNew() ? 'new' : $deal->id]) ?>");

        $("#payment").on("click", ".fa-edit", function() {
            anchor = $(this).data("anchor");
            if ($("#edit-payment-area-" + anchor + " form[id|=form-payment]").length === 0) {
                $("#edit-payment-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Deals', 'action' => 'payment']) ?>/<?= $deal->isNew() ? 'new' : $deal->id ?>/" +
                    $(this).data("id") + "/" +
                    anchor);
            }
            $("#edit-payment-" + anchor).show();
            $("#view-payment-" + anchor).hide();


        });
        $("#payment").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-payment-area-" + anchor + " span[id=nota_fiscal]").html(+ $("#form-payment-" + anchor + " input[id=nota_fiscal]").val());
            $("#view-payment-area-" + anchor + " span[id=valor_pagamento]").html($("#form-payment-" + anchor + " input[id=valor_pagamento]").val());
            $("#view-payment-area-" + anchor + " span[id=dt_emissao]").html($("#form-payment-" + anchor + " input[id=dt_emissao]").val());
            $("#view-payment-area-" + anchor + " span[id=dt_pagamento]").html($("#form-payment-" + anchor + " input[id=dt_pagamento]").val());
            //$("#view-payment-area-" + anchor + " span[id=dt_pagamento]").html($("#form-payment-" + anchor + " input[id=dt_pagamento]").val());
            $("#edit-payment-" + anchor).hide();
            $("#view-payment-" + anchor).show();
        });

        $(".fa-plus-square").click(function() {
            anchor = anchorize();
            $("#no-payment").remove();
            $("#payment-add").before(
                '<tr id="edit-payment-' + anchor + '">' +
                '<td>' +
                '<div id="edit-payment-area-' + anchor + '"></div>' +
                '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'payment\', \'' + anchor + '\', \'new\')"></i>' +
                //'<i class="fa fa-check-square pull-right mr-2" data-type="payment" data-anchor="' + anchor + '"></i>' +
                '</td>' +
                '</tr>' +
                '<tr id="view-payment-' + anchor + '" style="display:none">' +
                '<td>' +
                '<span id="view-payment-area-' + anchor + '" style="font-size: 14px">' +
                'Nº Nota Fiscal: <span class="mr-2" id="nota_fiscal"></span>' +
                'Valor: <span class="mr-2" id="valor_pagamento"></span>' +
                'Emissão: <span class="mr-2" id="dt_emissao"></span>' +
                'Pagamento: <span class="mr-2" id="dt_pagamento"></span>' +
                '</span>' +
                '<i class="fa fa-edit pull-right" data-type="payment" data-anchor="' + anchor + '" data-id="new"></i>' +
                '</td>' +
                '</tr>'
                );

            $("#edit-payment-area-" + anchor).load(
                "<?= $this->Url->build(['controller' => 'Deals', 'action' => 'payment']) ?>/<?= $deal->isNew() ? 'new' : $deal->id ?>/" +
                "new" + "/" + anchor);
                
        });

        $("#save").click(function() {
            HoldOn.open({
                theme: "sk-cube-grid"
            });
            var part1 = $.Deferred();
            var payment = $.Deferred();
            deal_id = $("#form-part1").data("id");
            $.post("<?= $this->Url->build(['action' => 'part1']) ?>/" + deal_id,
                $("#form-part1").serialize(),
                function(data) {
                    $("#part1").html(data);
                    if ($("<div>" + data + "</div>").find("#form-part1").data("error") === "N") {
                        part1.resolve("OK");
                        deal_id = $("<div>" + data + "</div>").find("#form-part1").data("id");
                        //======== Salvar pagamento de Contratos ======
                        var req_payment = [];
                        $("form[id|='form-payment']").each(function() {
                            anchor = $(this).data("anchor");
                            req_payment.push($.post("<?= $this->Url->build(['controller' => 'Deals', 'action' => 'payment']) ?>/" + deal_id + "/" + $(this).data("id") + "/" + anchor,
                                $("#form-payment-" + anchor).serialize()));
                        });
                        if (req_payment.length > 0) {
                            $.when.apply(undefined, req_payment).then(function() {
                                var objects = arguments;
                                var saved = true;
                                var data = [];
                                if (Array.isArray(objects[0])) {
                                    for (i = 0; i < objects.length; i++) {
                                        data.push(objects[i][0]);
                                    }
                                } else {
                                    data.push(objects[0]);
                                }
                                for (i = 0; i < data.length; i++) {
                                    anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-payment']").data("anchor");
                                    id = $("<div>" + data[i] + "</div>").find("form[id|='form-payment']").data("id");
                                    $("#edit-payment-area-" + anchor).html(data[i]);
                                    $("#trash-" + anchor).attr("onclick", "remove('payment', '" + anchor + "', " + id + ")");
                                    if ($("<div>" + data[i] + "</div>").find("#form-payment-" + anchor).data("error") === "N") {} else {
                                        saved = false;
                                    }
                                }
                                payment.resolve(saved ? "OK" : "NG");
                            });
                        } else {
                            payment.resolve("OK");
                        }
                        if ($("#form-delete-payment option").length > 0) {
                            $.post("<?= $this->Url->build(['controller' => 'Deals', 'action' => 'payment-delete']) ?>",
                                $("#form-delete-payment").serialize());
                        }
                    } else {
                        part1.resolve("NG");
                        payment.resolve("NG");
                    }
                });
            $.when(part1, payment).done(function(v1, v2) {
                $("#save").attr("disabled", false);
                console.log("part1: " + v1 + " payment: " + v2);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK") {
                    toast("Cadastro salvo com sucesso");
                    var url = window.location.href;
                    if (url.includes('new')===true){
                        window.history.back() 
                    }else{
                        location.reload(true);
                    }
                }
            });
        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Este registro só será removido após o salvamento da página, tem certeza que deseja adicioná-lo a lista de exclusão?',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                var valor_alterar = $("#form-payment-" + anchor).html();
                var position = valor_alterar.search('valor-pagamento" value="');
                var subStr = valor_alterar.substring(position+24, position+50);
                var position2 = subStr.search('"');
                var subbestStr = subStr.substring(0, position2);

                alert(subbestStr);
                $("#edit-payment-" + anchor).remove();
                $("#view-payment-" + anchor).remove();
                    if (id !== "new") {
                        $("#delete-payment-list").append("<option value='" + id + "' selected></option>");
                    }
                    var valor_pago=0;
                    var n_parcelas=0;

                    
                    //valor_pago = valor_pago + valor;
                    n_parcelas=n_parcelas+1;
                    
                    
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Dados Contratos', 'url' => ['action' => 'index']],
    ['title' => 'Cadastro de Contratos']
]);
echo $this->Breadcrumbs->render();
?>
    <div class="card">
        <div class="card-body">
            <div id="part1">
                
            </div>
            <?php
                $somatoria = 0;
                $contagem = 0;
            ?>
            <div class="row">
                <div class="col-md-12">
                    <p class="mt-3">Parcelas</p>
                    <table id="payment" class="table table-hover">
                        <?php
                        if (isset($deal->deal_payments) and sizeof($deal->deal_payments) > 0) {
                            foreach ($deal->deal_payments as $payment) {
                                $anchor = rand();
                                ?>
                                <tr id="edit-payment-<?= $anchor ?>" style="display: none">
                                    <td>
                                        <div id="edit-payment-area-<?= $anchor ?>"></div>
                                        <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('payment', '<?= $anchor ?>', <?= $payment->id ?>)"></i>
                                    </td>
                                </tr>
                                <tr id="view-payment-<?= $anchor ?>">
                                    <td>
                                        <span id="view-payment-area-<?= $anchor ?>" style="font-size: 14px">
                                            <?php
                                                $contagem = $contagem+1;
                                                if($payment->dt_pagamento != ''){
                                                    $somatoria = $somatoria+$payment->valor_pagamento;

                                                }
                                            ?>
                                            <?php 
                                                $strpagamento = $payment->valor_pagamento;
                                                $formatter = new NumberFormatter('pt_BR',  NumberFormatter::CURRENCY);

                                            ?>
                                            <span class="mr-2" id="nota_fiscal"><b>Nº Nota Fiscal: </b><?= $payment->nota_fiscal ?></span>
                                            <span class="mr-2" id="valor_pagamento"><b>Valor: <?=$formatter->formatCurrency($strpagamento, 'BRL') ?></b></span>
                                            <span class="mr-2" id="dt_emissao"><b>Emissão: </b><?= $payment->dt_emissao ?></span>
                                            <span class="mr-2" id="dt_pagamento"><b>Pagamento: </b><?= $payment->dt_pagamento ?></span>
                                        </span>
                                        <i class="fa fa-edit pull-right" data-type="payment" data-anchor="<?= $anchor ?>" data-id="<?= $payment->id ?>"></i>
                                    </td>
                                </tr>
                            <?php
                                }
                            } else {
                                ?>
                            <tr id="no-payment">
                                <td class="no-data-found">Nenhuma Parcela Cadastrada</td>
                            </tr>
                        <?php
                        }
                        ?>
                        <tr id="payment-add">
                            <td>
                                <i class="fa fa-plus-square" data-type="payment"></i>
                            </td>
                        </tr>
                    </table>
                    <?= $this->Form->create(null, ['id' => 'form-delete-payment']) ?>
                    <?= $this->Form->control('delete-payment-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                    <?= $this->Form->end() ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <b>Valor Total Recebido: </b>
                        <?php echo "R$".number_format($somatoria, 2, ',', '');?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <b>Quantidade de Parcelas: </b>
                        <?php echo $contagem;?>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col">
                    <?= $this->element('button_save', ['id' => 'save']) ?>
                    <?= $this->element('button_return') ?>
                </div>
            </div>
        </div>
    </div>