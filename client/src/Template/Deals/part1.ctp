<script>
    $(document).ready(function () {
        
        $("input[name=data_vigencia_inicio]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
        $("#valor").maskMoney({thousands: '.', decimal: ','}).maskMoney('mask', <?= $deal->valor ?>);
        $("input[name=data_vigencia_termino]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });

        $("input[name=data_firmacao_contrato]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
    });   
</script>

<?= $this->Flash->render() //PÁGINA CRIADA POR MATHEUS MIELLY?>
<?=
$this->Form->create($deal, [
    'id' => 'form-part1',
    'data-id' => $deal->isNew() ? 'new' : $deal->id,
    'data-error' => sizeof($deal->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Contrato</h5>
<div class="row mb-3 mt-3">
</div>
<div class="row">
    <div class="col-md-4 col-sm-12">
        <?php
            $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'label' => 'Nome Cliente', 'disabled' => true]);
            }else{
                echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'empty' => 'SELECIONE UM CLIENTE', 'label' => 'Nome Cliente', 'disabled' => !$deal->isNew()]);
            }
        ?>
    </div>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('data_firmacao_contrato', ['type' => 'text', 'label' => 'Contrato Firmado em', 'disabled' => true]);
            }else{
                if (!$deal->isNew()) {
                    echo $this->Form->control('data_firmacao_contrato', ['type' => 'text', 'label' => 'Contrato Firmado em']);
                } else {
                    echo $this->Form->control('data_firmacao_contrato', ['type' => 'text', 'label' => 'Contrato Firmado em']);
                }
            }
        ?>    
    </div>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('valor', ['type' => 'text', 'label' => 'Valor Total do Contrato', 'disabled' => true ]);
            }else{
                echo $this->Form->control('valor', ['type' => 'text', 'label' => 'Valor Total do Contrato']);
            }
        ?>
    </div>
</div>
<div class="row">
        <?php
            if(strpos($checagem, "view")==true){
                echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_inicio', ['type' => 'text', 'label' => 'Vigência do Contrato -  De', 'disabled' => true]).'</div>';    
                echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_termino', ['type' => 'text', 'label' => 'Até', 'disabled' => true]).'</div>';
            }
            else{
                if (!$deal->isNew()) {
                    echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_inicio', ['type' => 'text', 'label' => 'Vigência do Contrato - De']).'</div>';
                    echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_termino', ['type' => 'text', 'label' => 'Até']).'</div>';
                } else {
                    echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_inicio', ['type' => 'text', 'label' => 'Vigência do Contrato - De']).'</div>';
                    echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_termino', ['type' => 'text', 'label' => 'Até']).'</div>';
                }
            }
        ?>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){ 
                echo$this->Form->control('n_max_usuario', ['type' => 'number', 'label' => 'Limite Máximo de Usuários do Cliente', 'disabled' => true]);
            }else{
                echo$this->Form->control('n_max_usuario', ['type' => 'number', 'label' => 'Limite Máximo de Usuários do Cliente']);
            }
        ?>
    </div>
</div>
<?= $this->Form->end() ?>
