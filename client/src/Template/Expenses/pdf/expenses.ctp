<style>
    @font-face {
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
        src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
    }

    body {
        font-family: Verdana;
        font-size: 10px;
        margin-left: 0.5cm;
        margin-right: 0.5cm;
    }
</style>

<div>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
    <?php if (!is_null($balance)) { ?>
        <div>
            DESCRIÇÃO: OUTRAS DESPESAS<br>

        </div>
        <div>DATA DE INÍCIO: <?= $date_start ?></div>
        <div>DATA FINAL: <?= $date_end ?></div>

        <br>
        <table class="table table-hover report-row">
            <tr>
                <th width="20%">DATA</th>
                <th width="45%">NOME</th>
                <th width="35%">VALOR R$</th>

            </tr>
            <?php
                if (isset($entries) and sizeof($entries) > 0) {
                    foreach ($entries as $entry) {
                        if ($entry->ledger_entry_type->dc == 'D') {
                            $balance -= $entry->valor;
                        } else {
                            $balance += $entry->valor;
                        }
                        ?>
                    <tr>
                        <td><?= $entry->data_lancamento ?></td>
                        <td><?= $entry->descricao ?></td>
                        <!--<td><//?= $entry->ledger_entry_type->descricao ?></td>-->
                        <!--  <td><//?= $entry->ledger_entry_type->dc ?></td> -->
                        <td style="text-align:right"><?= number_format($entry->valor, 2, ',', '.') ?></td>

                    </tr>
                <?php
                        }
                    } else {
                        ?>
                <tr class="no-data-found">
                    <td colspan="5">Nenhum registro encontrado</td>
                </tr>
            <?php } ?>
        </table>
        <div>VALOR TOTAL DESPESAS: <?= $date_end ?>:
            <span class="<?= $balance > 0 ? 'credit' : ($balance < 0 ? 'debit' : '') ?>">
                <?= $this->Number->currency(abs($balance)) ?> <?= $balance > 0 ? 'C' : ($balance < 0 ? 'D' : '') ?>
            </span>
        </div>
    <?php } ?>
</div>