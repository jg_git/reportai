<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {

        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        $("input[name=date_range]").daterangepicker(configRangePicker, function (start, end, label) {
            
        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=date_start]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=date_end]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val("");
            $("input[name=date_start]").val("");
            $("input[name=date_end]").val("");
        });

        $("#generate").click(function () {//acessa controler expenses ação check para verificar se existe opção selecionada.
                 window.open("<?= $this->Url->build(['controller' => 'Expenses', 'action' => 'expenses']) ?>/expense.pdf" +
                                        "?account_id=" + $("select[name=account_id] option:selected").val() +
                                        "&date_start=" + $("input[name=date_start]").val() +
                                        "&date_end=" + $("input[name=date_end]").val(),
                                        "_blank");

          
        });

    });
</script>

<section class="p-3">
    <?php
    $this->Breadcrumbs->add([
        ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
        ['title' => 'Outras Despesas']
    ]);
    echo $this->Breadcrumbs->render();
    ?>
    <div class="card">
        <div class="card-body">
            <div class="row justify-content-center">
                <div class="col-md-8 col-sm-12">
                    <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                        <?= $this->Form->create(null, ['id' => 'form-statement', 'valueSources' => 'query']) ?>
                        <div class="row">
                            <!--<div class="col-md-9 col-sm-12">-->
                                <!--<//?= $this->Form->control('account_id', ['label' => 'Conta', 'options' => $accounts]) ?>-->
                            <!--</div>-->
                            <div class="col-md-5 col-sm-12">
                                <?= $this->Form->control('date_range', ['type' => 'text', 'label' => 'Período']) ?>
                                <?= $this->Form->control('date_start', ['type' => 'hidden']) ?>
                                <?= $this->Form->control('date_end', ['type' => 'hidden']) ?>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                        <div class="row mt-2 mb-2">
                            <div class="col">
                                <button id="generate" class="btn btn-lg btn-dark pull-right">Imprimir</button>
                                <?= $this->element('button_clear') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>