<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class BrokersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['razao_social', 'nome_fantasia'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Brokers->find('search', ['search' => $data]);
        $brokers = $this->paginate($query);
        $this->set(compact('brokers'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $broker = $this->Brokers->get($id, ['contain' => [
                    'BrokerEmails',
                    'BrokerPhones',
                    'BrokerContacts',
                    'BrokerBankAccounts',
                    'BrokerBankAccounts.Banks'
             ]]);
        } else {
            $broker = $this->Brokers->newEntity();
        }
        $this->set(compact('broker'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $broker = $this->Brokers->get($id, ['contain' => []]);
        } else {
            $broker = $this->Brokers->newEntity();
            $broker->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }
            $broker = $this->Brokers->patchEntity($broker, $data, ['validate' => 'part1']);
            if ($this->Brokers->save($broker)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('broker'));
    }

    public function part2($id) {
        if ($id != 'new') {
            $broker = $this->Brokers->get($id, ['contain' => ['Banks']]);
        } else {
            $broker = $this->Brokers->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $broker = $this->Brokers->patchEntity($broker, $data, ['validate' => 'part2']);
            if ($this->Brokers->save($broker)) {
                $broker = $this->Brokers->get($broker->id, ['contain' => ['Banks']]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('broker'));
    }

    public function email($broker_id, $id, $anchor) {
        $table = TableRegistry::get('BrokerEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->broker_id = $broker_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($broker_id, $id, $anchor) {
        $table = TableRegistry::get('BrokerPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->broker_id = $broker_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    public function account($broker_id, $id, $anchor) {
        $table = TableRegistry::get('BrokerBankAccounts');
        if ($id != 'new') {
            $account = $table->get($id);
        } else {
            $account = $table->newEntity();
            $account->broker_id = $broker_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $table->patchEntity($account, $data);
            if ($table->save($account)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $banks = TableRegistry::get('Banks')->find('list')->orderAsc('nome');
        $this->set(compact('account', 'banks', 'anchor'));
    }

    public function contact($broker_id, $id, $anchor) {
        $table = TableRegistry::get('BrokerContacts');
        if ($id != 'new') {
            $contact = $table->get($id);
        } else {
            $contact = $table->newEntity();
            $contact->broker_id = $broker_id;
            $contact->tipo = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $contact = $table->patchEntity($contact, $data);
            if ($table->save($contact)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('contact', 'anchor'));
    }

    public function emailDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('BrokerEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('BrokerPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function accountDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-account-list'] as $id) {
            $table = TableRegistry::get('BrokerBankAccounts');
            $account = $table->get($id);
            $table->delete($account);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function contactDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-contact-list'] as $id) {
            $table = TableRegistry::get('BrokerContacts');
            $contact = $table->get($id);
            $table->delete($contact);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $broker = $this->Brokers->get($id);
        if ($this->Brokers->delete($broker)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
