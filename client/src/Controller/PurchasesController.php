<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class PurchasesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
                'in_stock' => '0',
            ]
        ]);
    }

    public function index()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $query = $this->Purchases->find('search', ['search' => $data])->contain([
            'Plants',
        ])->orderAsc('data_compra');
        // if (array_key_exists('in_stock', $data)) {
        $query->where(['volume_comprado + volume_transferido - volume_transito - volume_devolvido - volume_vendido > 0']);
        //    }
        $purchases = $this->paginate($query);
        $plants = TableRegistry::get('Plants')->find('list')->orderAsc('razao_social');
        $this->set(compact('purchases', 'plants'));
    }

    public function edit($id)
    {
        if ($id != 'new') {
            $purchase = $this->Purchases->find()
                ->where(['id' => $id])
                ->contain([
                    'PurchasePayments',
                    'PurchasePayments.Payables'
                ])->first();
        } else {
            $purchase = $this->Purchases->newEntity();
            $purchase->forma_pagamento = 'P';
            $purchase->corretada = 0;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('purchase_payments', $data)) {
                $data['purchase_payments'] = [];
            }
            $purchase = $this->Purchases->patchEntity($purchase, $data, ['associated' => ['PurchasePayments']]);
            // remove contas a pagar de pagamentos removidos
            if (gettype($data['delete-payment-list']) == 'array') {
                foreach ($data['delete-payment-list'] as $payment_id) {
                    $payable = TableRegistry::get('Payables')->find()->where(['purchase_payment_id' => $payment_id])->first();
                    if (!is_null($payable)) {
                        TableRegistry::get('Payables')->delete($payable);
                    }
                }
            }
            if ($this->Purchases->save($purchase)) {
                if (!is_null($purchase->purchase_payments)) {
                    foreach ($purchase->purchase_payments as $payment) {
                        $payable = TableRegistry::get('Payables')->find()
                            ->where(['tipo' => 'PC', 'purchase_payment_id' => $payment->id])->first();
                        if (is_null($payable)) {
                            $payable = TableRegistry::get('Payables')->newEntity();
                            $payable->tipo = 'PC';
                            $payable->data_vencimento = $payment->data_vencimento;
                            $payable->valor = $payment->valor_provisionado;
                            $payable->purchase_payment_id = $payment->id;
                            TableRegistry::get('Payables')->save($payable);
                        } else {
                            if (($payable->valor != $payment->valor_provisionado) || ($payable->data_vencimento != $payment->data_vencimento)) {
                                $payable->data_vencimento = $payment->data_vencimento;
                                $payable->valor = $payment->valor_provisionado;
                                TableRegistry::get('Payables')->save($payable);
                            }
                        }
                    }
                }
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $purchase->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $plants = TableRegistry::get('Plants')->find('list')->orderAsc('razao_social');
        $distributors = TableRegistry::get('Distributors')->find('list')->orderAsc('razao_social');
        $brokers = TableRegistry::get('Brokers')->find('list')->orderAsc('razao_social');
        $accounts = $this->getAccounts();
        $this->set(compact('purchase', 'plants', 'distributors', 'brokers', 'accounts'));
    }

    public function devolucao()
    {
        $data = $this->request->getData();

        $purchase = $this->Purchases->get($data['purchase_id'], ['contain' => [
            'PurchasePayments',
            'PurchasePayments.Payables'
        ]]);

        if (!is_null($purchase->purchase_payments)) {
            foreach ($purchase->purchase_payments as $payment) {
                if (!$payment->payable->baixa) {
                    $payable = $payment->payable;
                    TableRegistry::get('Payables')->delete($payable);
                    TableRegistry::get('PurchasePayments')->delete($payment);
                }
            }
        }

        $devolution = TableRegistry::get('PurchaseDevolutions')->newEntity();
        $devolution->purchase_id = $purchase->id;
        $devolution->volume = str_replace(',', '.', str_replace('.', '', $data['volume_devolver']));
        TableRegistry::get('PurchaseDevolutions')->save($devolution);

        $volume = 0;
        $devolutions = TableRegistry::get('PurchaseDevolutions')->find()->where(['purchase_id' => $purchase->id]);
        foreach ($devolutions as $devolution) {
            $volume += $devolution->volume;
        }
        $purchase->volume_devolvido = $volume;
        $this->Purchases->save($purchase);

        $entry = TableRegistry::get('LedgerEntries')->newEntity();
        $entry->account_id = $data['account_id'];
        $entry->ledger_entry_type_id = 23;
        $entry->payment_method_id = 1;
        $entry->data_lancamento = date('Y-m-d');
        $entry->descricao = 'Devolução Ctt ' . $purchase->numero_contrato . ' - ' . number_format($devolution->volume, 0, ',', '.') . ' l a ' . ($purchase->preco_litro + $purchase->preco_litro_adicional);
        $entry->valor = $devolution->volume * ($purchase->preco_litro + $purchase->preco_litro_adicional);
        TableRegistry::get('LedgerEntries')->save($entry);

        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function payment($index, $anchor)
    {
        $this->set(compact('index', 'anchor'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $purchase = $this->Purchases->get($id, ['contain' => [
            'PurchasePayments',
            'PurchasePayments.Payables',
        ]]);

        $found = false;
        foreach ($purchase->purchase_payments as $payment) {
            if ($payment->payable->baixa) {
                $found = true;
            }
        }
        if ($found) {
            $this->Flash->error('Compra não pode ser removida por ter pagamentos já dados baixa');
        }

        if (!$found) {
            foreach ($purchase->purchase_payments as $payment) {
                $payable = $payment->payable;
                TableRegistry::get('Payables')->delete($payable);
            }
            if ($this->Purchases->delete($purchase)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
            return $this->redirect(['action' => 'index']);
        }
        return $this->redirect(['action' => 'edit', $id]);
    }

    public function exportPdf($purchase_id)
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $ledger = [];
        $purchase = TableRegistry::get('Purchases')->get($purchase_id, ['contain' => ['Distributors', 'Plants']]);
        $ledger[] = [
            'data' => $purchase->data_compra,
            'descricao' => '[COMPRA] Distribuidora: ' . $purchase->distributor->razao_social . ' Usina: ' . $purchase->plant->razao_social,
            'dc' => 'C',
            'volume' => $purchase->volume_comprado
        ];
        $sales_purchases = TableRegistry::get('SalesPurchases')->find()
            ->where(['purchase_id' => $purchase_id])
            ->contain([
                'Sales',
                'Sales.Carriers',
                'Sales.Drivers',
                'Sales.Vehicles',
                'Purchases',
                'SalesPurchasesConciliations'
            ])
            ->orderAsc('Sales.data_venda');
        foreach ($sales_purchases as $key => $sp) {
            $sale = $sp->sale;
            $ledger[] = [
                'data' => $sale->data_venda,
                'descricao' => '[VENDA] Transportadora: ' . $sale->carrier->razao_social .
                    ' / Motorista: ' . $sale->driver->nome .
                    ' / Veículo: ' . $sale->vehicle->placa,
                'dc' => 'D',
                'volume' => $sale->conciliado ? $sp->volume_conciliado : $sp->volume,
            ];
        }
        $sales_devolutions = TableRegistry::get('PurchaseDevolutions')->find()->where(['purchase_id' => $purchase_id])->contain([
            'Purchases',
            'Purchases.Plants'
        ])->toList();
        foreach ($sales_devolutions as $devolution) {
            //  foreach ($ledger as $key => $entry) {

            if ($devolution->volume > 0) {
                $ledger[] = [
                    'data' => date('d/m/Y', strtotime(str_replace('/', '-', $devolution->created))),
                    'descricao' => '[DEVOLUÇÃO] efetuada: ' . $purchase->plant->razao_social,
                    'dc' => 'D',
                    'volume' => $devolution->volume,
                ];
                //   $ledger = array_slice($ledger, 0, $key - 1, true) +
                // $new_entry +
                //     array_slice($ledger, $key - 1, count($ledger) - 1, true);
                // break;
            }
        }

        $purchases = TableRegistry::get('Purchases')->get($purchase_id, ['contain' => ['Distributors', 'Plants']]);
        $purchase_transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['OR' => [
            'destination_purchase_id' => $purchase_id,
            'purchase_id' => $purchase_id
        ]])->contain([
            'Purchases',
            'Plants',
            'Purchases.Plants',
        ])->toList();
        foreach ($purchase_transfers as $transfer) {
            // $transfer_date = date('Y-m-d', strtotime(str_replace('/', '-', $transfer->created)));

            //    $this->log($entry, 'debug');
            // $entry_date = date('Y-m-d', strtotime(str_replace('/', '-', $entry['data'])));
            //    $transfer = $entry->$transfer;
            //   if ($ledger['data'] > $transfer->created) {
            if ($transfer->volume > 0) {
                //  dump($new_entry);
                $ledger[] = [
                    'data' => date('d/m/Y', strtotime(str_replace('/', '-', $transfer->created))),
                    //  'data' =>  $transfer->created,
                    //
                    'descricao' => '[TRANSFERÊNCIA] recebida: ', //. $purchase->plant->razao_social,
                    'dc' => 'C',
                    'volume' => $transfer->volume
                ];
            } else {
                //dump($transfer);
                $ledger[] = [
                    'data' => date('d/m/Y', strtotime(str_replace('/', '-', $transfer->created))),
                    //'data' =>  $transfer->created,
                    //plant id must be 27
                    //purchase transfers -> destionation purchase -> purchases -> plant id

                    'descricao' => '[TRANSFERÊNCIA] enviada para Usina: ' . $transfer->plant->razao_social  . ' - ' . 'Contrato' .  $transfer->purchase->numero_contrato,
                    'dc' => 'D',
                    'volume' => $transfer->volume
                ];
            }


            usort($ledger, [$this, 'cmp']);
            // dump($ledger);
            // $ledger = array_slice($ledger, 0, $key - 1, true) +
            //     $new_entry +
            //     array_slice($ledger, $key - 1, count($ledger) - 1, true);
            // break;

        }

        $this->set(compact('purchase', 'sales_purchases', 'ledger'));
    }
    public function statement($purchase_id)
    {
        $ledger = [];
        $purchase = TableRegistry::get('Purchases')->get($purchase_id, ['contain' => ['Distributors', 'Plants']]);
        $ledger[] = [
            'data' => $purchase->data_compra,
            'descricao' => '[COMPRA] Distribuidora: ' . $purchase->distributor->razao_social . ' Usina: ' . $purchase->plant->razao_social,
            'dc' => 'C',
            'volume' => $purchase->volume_comprado
        ];
        $sales_purchases = TableRegistry::get('SalesPurchases')->find()
            ->where(['purchase_id' => $purchase_id])
            ->contain([
                'Sales',
                'Sales.Carriers',
                'Sales.Drivers',
                'Sales.Vehicles',
                'Purchases',
                'SalesPurchasesConciliations'
            ])
            ->orderAsc('Sales.data_venda');
        foreach ($sales_purchases as $key => $sp) {
            $sale = $sp->sale;
            $ledger[] = [
                'data' => $sale->data_venda,
                'descricao' => '[VENDA] Transportadora: ' . $sale->carrier->razao_social .
                    ' / Motorista: ' . $sale->driver->nome .
                    ' / Veículo: ' . $sale->vehicle->placa,
                'dc' => 'D',
                'volume' => $sale->conciliado ? $sp->volume_conciliado : $sp->volume,
            ];
        }
        $sales_devolutions = TableRegistry::get('PurchaseDevolutions')->find()->where(['purchase_id' => $purchase_id])->contain([
            'Purchases',
            'Purchases.Plants'
        ])->toList();
        foreach ($sales_devolutions as $devolution) {
            //  foreach ($ledger as $key => $entry) {

            if ($devolution->volume > 0) {
                $ledger[] = [
                    'data' => date('d/m/Y', strtotime(str_replace('/', '-', $devolution->created))),
                    'descricao' => '[DEVOLUÇÃO] efetuada: ' . $purchase->plant->razao_social,
                    'dc' => 'D',
                    'volume' => $devolution->volume,
                ];
                //   $ledger = array_slice($ledger, 0, $key - 1, true) +
                // $new_entry +
                //     array_slice($ledger, $key - 1, count($ledger) - 1, true);
                // break;
            }
        }

        $purchases = TableRegistry::get('Purchases')->get($purchase_id, ['contain' => ['Distributors', 'Plants']]);
        $purchase_transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['OR' => [
            'destination_purchase_id' => $purchase_id,
            'purchase_id' => $purchase_id
        ]])->contain([
            'Purchases',
            'Plants',
            'Purchases.Plants',
        ])->toList();
        foreach ($purchase_transfers as $transfer) {
            // $transfer_date = date('Y-m-d', strtotime(str_replace('/', '-', $transfer->created)));

            //    $this->log($entry, 'debug');
            // $entry_date = date('Y-m-d', strtotime(str_replace('/', '-', $entry['data'])));
            //    $transfer = $entry->$transfer;
            //   if ($ledger['data'] > $transfer->created) {
            if ($transfer->volume > 0) {
                //  dump($new_entry);
                $ledger[] = [
                    'data' => date('d/m/Y', strtotime(str_replace('/', '-', $transfer->created))),
                    //  'data' =>  $transfer->created,
                    //
                    'descricao' => '[TRANSFERÊNCIA] recebida: ', //. $purchase->plant->razao_social,
                    'dc' => 'C',
                    'volume' => $transfer->volume
                ];
            } else {
                //dump($transfer);
                $ledger[] = [
                    'data' => date('d/m/Y', strtotime(str_replace('/', '-', $transfer->created))),
                    //'data' =>  $transfer->created,
                    //plant id must be 27
                    //purchase transfers -> destionation purchase -> purchases -> plant id

                    'descricao' => '[TRANSFERÊNCIA] enviada para Usina: ' . $transfer->plant->razao_social  . ' - ' . 'Contrato: ' .  $transfer->purchase->numero_contrato,
                    'dc' => 'D',
                    'volume' => $transfer->volume
                ];
            }


            usort($ledger, [$this, 'cmp']);
            // dump($ledger);
            // $ledger = array_slice($ledger, 0, $key - 1, true) +
            //     $new_entry +
            //     array_slice($ledger, $key - 1, count($ledger) - 1, true);
            // break;

        }

        $this->set(compact('purchase', 'sales_purchases', 'ledger'));
    }

    function cmp($a, $b)
    {
        return $a['data'] > $b['data'];
    }
}
