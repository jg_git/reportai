<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class TransfersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

      public function filter()
    {
             $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['date_start', 'date_end'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
            }
        }
        $query = $this->Transfers->find('search', ['search' => $data])->contain([
            'SourceAccounts',
            'SourceAccounts.Distributors',
            'SourceAccounts.Banks',
            'DestinationAccounts',
            'DestinationAccounts.Distributors',
            'DestinationAccounts.Banks'
        ]);
        $transfers = $this->paginate($query);
        $accounts = $this->getAccounts();
        $this->set(compact('transfers', 'accounts'));
    }
    
    public function index()
    {
      $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['date_start', 'date_end'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
            }
        }
        $query = $this->Transfers->find('search', ['search' => $data])->contain([
            'SourceAccounts',
            'SourceAccounts.Distributors',
            'SourceAccounts.Banks',
            'DestinationAccounts',
            'DestinationAccounts.Distributors',
            'DestinationAccounts.Banks'
        ]);
        $transfers = $this->paginate($query);
        $accounts = $this->getAccounts();
        $this->set(compact('transfers', 'accounts'));
    }

    public function edit($id)
    {
        if ($id != 'new') {
            $transfer = $this->Transfers->get($id, ['contain' => []]);
            $accounts = $this->getAllAccounts();
        } else {
            $transfer = $this->Transfers->newEntity();
            $transfer->dc = 'D';
            $accounts = $this->getAccounts();
            // 
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['valor'] = str_replace(',', '.', str_replace('.', '', $data['valor']));
            $transfer = $this->Transfers->patchEntity($transfer, $data);
            if ($this->Transfers->save($transfer)) {

                $entry = TableRegistry::get('LedgerEntries')->newEntity();
                $entry->account_id = $transfer->conta_origem_id;
                $entry->ledger_entry_type_id = 1;
                $entry->data_lancamento = $transfer->data_transferencia;
                $entry->payment_method_id = $transfer->payment_method_id;
                $entry->descricao = $transfer->descricao;
                $entry->valor = $transfer->valor;
                TableRegistry::get('LedgerEntries')->save($entry);
                $transfer->source_ledger_entry_id = $entry->id;

                $entry = TableRegistry::get('LedgerEntries')->newEntity();
                $entry->account_id = $transfer->conta_destino_id;
                $entry->ledger_entry_type_id = 2;
                $entry->data_lancamento = $transfer->data_transferencia;
                $entry->payment_method_id = $transfer->payment_method_id;
                $entry->descricao = $transfer->descricao;
                $entry->valor = $transfer->valor;
                TableRegistry::get('LedgerEntries')->save($entry);
                $transfer->destination_ledger_entry_id = $entry->id;

                $this->Transfers->save($transfer);

                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $transfer->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }

        $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('transfer', 'accounts', 'payment_methods'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transfer = $this->Transfers->get($id);
        $source = $transfer->source_ledger_entry_id;
        $destination = $transfer->destination_ledger_entry_id;
        if ($this->Transfers->delete($transfer)) {
            $entry = TableRegistry::get('LedgerEntries')->get($source);
            TableRegistry::get('LedgerEntries')->delete($entry);
            $entry = TableRegistry::get('LedgerEntries')->get($destination);
            TableRegistry::get('LedgerEntries')->delete($entry);
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }
}
