<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class PlantsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['razao_social', 'nome_fantasia'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Plants->find('search', ['search' => $data]);
        $plants = $this->paginate($query);
        $this->set(compact('plants'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $plant = $this->Plants->get($id, ['contain' => [
                    'PlantEmails',
                    'PlantPhones',
                    'PlantContacts',
                    'PlantBankAccounts',
                    'PlantBankAccounts.Banks'
            ]]);
        } else {
            $plant = $this->Plants->newEntity();
        }
        $this->set(compact('plant'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $plant = $this->Plants->get($id, ['contain' => []]);
        } else {
            $plant = $this->Plants->newEntity();
            $plant->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }
            $plant = $this->Plants->patchEntity($plant, $data, ['validate' => 'part1']);
            if ($this->Plants->save($plant)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('plant'));
    }

    public function part2($id) {
        if ($id != 'new') {
            $plant = $this->Plants->get($id);
        } else {
            $plant = $this->Plants->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $plant = $this->Plants->patchEntity($plant, $data, ['validate' => 'part2']);
            if ($this->Plants->save($plant)) {
                $plant = $this->Plants->get($plant->id);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('plant'));
    }

    public function email($plant_id, $id, $anchor) {
        $table = TableRegistry::get('PlantEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->plant_id = $plant_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($plant_id, $id, $anchor) {
        $table = TableRegistry::get('PlantPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->plant_id = $plant_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    public function account($plant_id, $id, $anchor) {
        $table = TableRegistry::get('PlantBankAccounts');
        if ($id != 'new') {
            $account = $table->get($id);
        } else {
            $account = $table->newEntity();
            $account->plant_id = $plant_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $table->patchEntity($account, $data);
            if ($table->save($account)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $bank_list = TableRegistry::get('Banks')->find()->orderAsc('nome');
        $banks = [];
        foreach ($bank_list as $bank) {
            $banks[$bank->id] = $bank->codigo . ' - ' . $bank->nome;
        }
        $this->set(compact('account', 'banks', 'anchor'));
    }

    public function contact($plant_id, $id, $anchor) {
        $table = TableRegistry::get('PlantContacts');
        if ($id != 'new') {
            $contact = $table->get($id);
        } else {
            $contact = $table->newEntity();
            $contact->plant_id = $plant_id;
            $contact->tipo = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $contact = $table->patchEntity($contact, $data);
            if ($table->save($contact)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('contact', 'anchor'));
    }

    public function emailDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('PlantEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('PlantPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function accountDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-account-list'] as $id) {
            $table = TableRegistry::get('PlantBankAccounts');
            $account = $table->get($id);
            $table->delete($account);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function contactDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-contact-list'] as $id) {
            $table = TableRegistry::get('PlantContacts');
            $contact = $table->get($id);
            $table->delete($contact);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $plant = $this->Plants->get($id);
        if ($this->Plants->delete($plant)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
