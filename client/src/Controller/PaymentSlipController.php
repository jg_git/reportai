<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Client\FormData;
use Cake\I18n\Date;
use Cake\ORM\Table;

class PaymentSlipController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
        ]);
    }

    public function isAuthorized($user)
    {
        return true;
    }

    public function index()
    {
        $session = $this->request->getSession();
        if (strpos($this->request->env('HTTP_REFERER'), 'payment-slip') === false) {
            $session->delete('cartps');
        }
        $cart = explode(',', $session->read('cartps'));
        $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        $total = 0;
        foreach ($cartdb as $item) {
            $total += $item->valor;
        }

        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = [];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
            }
        }

        $receivables = TableRegistry::get('Receivables')->find('search', ['search' => $data])->where([
            'Receivables.boleto_gerado' => 0,
            'Receivables.baixa' => 0,
            // 'Receivables.data_vencimento >=' => new Date()
        ])->contain([
            'SaleDetails',
            'SaleDetails.Accounts',
            'SaleDetails.Sales',
            'SaleDetails.Sales.Purchases',
            'SaleDetails.Sales.Purchases.Brokers',
            'SaleDetails.Sales.Purchases.Plants',
            'SaleDetails.Sales.Purchases.Distributors',
            'SaleDetails.Presales',
            'SaleDetails.Presales.Clients',
            'SaleDetails.Presales.Clients.Sellers'
        ])->matching('SaleDetails.Accounts', function ($q) {
            return $q->where(['titular LIKE' => '%Boleto%']);
        })->orderAsc('Receivables.data_vencimento');

        if (array_key_exists('razao_social', $data)) {
            $receivables->matching('SaleDetails.Presales.Clients', function ($q) use ($data) {
                return $q->where(['Clients.razao_social LIKE' => '%' . $data['razao_social'] . '%']);
            });
        }

        if (array_key_exists('nota_fiscal', $data)) {
            $receivables->matching('SaleDetails', function ($q) use ($data) {
                return $q->where(['SaleDetails.nota_fiscal' => $data['nota_fiscal']]);
            });
        }

        if (array_key_exists('data_venda_de', $data)) {
            $receivables->matching('SaleDetails.Sales', function ($q) use ($data) {
                return $q->where([
                    'Sales.data_venda >=' => $data['data_venda_de'],
                    'Sales.data_venda <=' => $data['data_venda_ate']
                ]);
            });
        }

        // echo $receivables;

        $batches = TableRegistry::get('PayamentSlipBatches')->find()
            ->where([
                'date_requested >=' => date('Y-m-d 00:00:00'),
                'date_requested <=' => date('Y-m-d 23:59:59')
            ])
            ->orderDesc('date_requested');

        $accounts = $this->getAccounts();
        $receivables = $this->paginate($receivables);
        $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('receivables', 'accounts', 'payment_methods', 'cart', 'total', 'batches'));
    }

    public function error($batch_id)
    {
        $registration_slips = TableRegistry::get('PayamentSlips')->find()
            ->where([
                'payament_slip_batch_id' => $batch_id,
                'is_registration_error' => 1
            ])
            ->contain([
                'Receivables',
                'Receivables.SaleDetails',
                'Receivables.SaleDetails.Accounts',
                'Receivables.SaleDetails.Sales',
                'Receivables.SaleDetails.Sales.Purchases',
                'Receivables.SaleDetails.Sales.Purchases.Brokers',
                'Receivables.SaleDetails.Sales.Purchases.Plants',
                'Receivables.SaleDetails.Presales',
                'Receivables.SaleDetails.Presales.Clients',
                'Receivables.SaleDetails.Presales.Clients.Sellers'
            ])
            ->toList();

        $pdf_slips = TableRegistry::get('PayamentSlips')->find()
            ->where([
                'payament_slip_batch_id' => $batch_id,
                'is_print_error' => 1
            ])
            ->contain([
                'Receivables',
                'Receivables.SaleDetails',
                'Receivables.SaleDetails.Accounts',
                'Receivables.SaleDetails.Sales',
                'Receivables.SaleDetails.Sales.Purchases',
                'Receivables.SaleDetails.Sales.Purchases.Brokers',
                'Receivables.SaleDetails.Sales.Purchases.Plants',
                'Receivables.SaleDetails.Presales',
                'Receivables.SaleDetails.Presales.Clients',
                'Receivables.SaleDetails.Presales.Clients.Sellers'
            ])
            ->toList();

        $this->set(compact('registration_slips', 'pdf_slips'));
    }

    public function cancel()
    {
        $data = $this->request->getData();
        $slip = TableRegistry::get('PayamentSlips')->get($data['slip_id']);
        $slip->is_canceled = 1;
        TableRegistry::get('PayamentSlips')->save($slip);

        $receivable = TableRegistry::get('Receivables')->get($slip->receivable_id);
        $receivable->boleto_gerado = 0;
        TableRegistry::get('Receivables')->save($receivable);

        $this->redirect(['action' => 'error', $slip->payament_slip_batch_id]);
    }

    public function excluirBoletos()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $session = $this->request->getSession();
            $cart = explode(',', $session->read('cartps'));

            if (sizeof($cart) == 0) {
                $this->Flash->error('Nenhuma conta a receber selecionada.');
                $this->redirect(['controller' => 'PaymentSlip', 'action' => 'index']);
            }

            $receivables = TableRegistry::get('Receivables')->find('all')
                ->where(['Receivables.id IN' => $cart])
                ->contain([
                    'SaleDetails',
                    'SaleDetails.Presales',
                    'SaleDetails.Sales.Purchases.Distributors',
                    'SaleDetails.Presales.Clients',
                    'SaleDetails.Accounts'
                ]);

            foreach ($receivables as $receivable) {
                $receivable->boleto_gerado = 1;
                TableRegistry::get('Receivables')->save($receivable);
            }
        }

        $session->delete('cartps');
        $this->Flash->success('Boletos excluídos.');
        $this->redirect(['controller' => 'PaymentSlip', 'action' => 'index']);
    }

    public function gerarBoletos()
    {
        $this->loadModel('PayamentSlips');

        if ($this->request->is(['patch', 'post', 'put'])) {
            $session = $this->request->getSession();
            $cart = explode(',', $session->read('cartps'));

            if (sizeof($cart) == 0) {
                $this->Flash->error('Nenhuma conta a receber selecionada.');
                $this->redirect(['controller' => 'PaymentSlip', 'action' => 'index']);
            }

            $receivables = TableRegistry::get('Receivables')->find('all')
                ->where(['Receivables.id IN' => $cart])
                ->contain([
                    'SaleDetails',
                    'SaleDetails.Presales',
                    'SaleDetails.Sales.Purchases.Distributors',
                    'SaleDetails.Presales.Clients',
                    'SaleDetails.Accounts'
                ]);

            $batch = TableRegistry::get('PayamentSlipBatches')->newEntity();
            $batch->date_requested = date('Y-m-d H:i:s');
            TableRegistry::get('PayamentSlipBatches')->save($batch);

            $count = 0;
            foreach ($receivables as $receivable) {
                $count++;

                $account = $receivable->sale_detail->account;
                $account_tokens = explode('-', $account->numero_conta);

                $slip = $this->PayamentSlips->newEntity();
                $slip->payament_slip_batch_id = $batch->id;
                $slip->receivable_id = $receivable->id;
                $slip->account_id = $account->id;
                $slip->nota_fiscal = $receivable->sale_detail->nota_fiscal;
                $slip->nosso_numero = $this->formata_numero($receivable->id, 8, 0);
                $slip->digito_verificador_nosso_numero = $this->modulo_10($this->formata_numero($account->numero_agencia, 4, 0) .
                    $this->formata_numero($account_tokens[0], 5, 0) .
                    $account->carteira .
                    $this->formata_numero($receivable->id, 8, 0));
                $slip->data_emissao = date('Y-m-d H:i:s');
                $slip->data_vencimento = $receivable->data_vencimento;
                $slip->valor_cobrado = str_pad(str_replace('.', '', number_format($receivable->valor, 2, '.', '')), 17, '0', STR_PAD_LEFT);

                $distributor = $receivable->sale_detail->sale->purchases[0]->distributor;

                $slip->nome_sacador_avalista = $distributor->razao_social;
                $slip->cpf_cnpj_sacador_avalista = str_pad(str_replace(['/', '-', '.'], '', $distributor->cnpj), 14, '0', STR_PAD_LEFT);
                $slip->logradouro_sacador_avalista = $distributor->logradouro;
                $slip->bairro_sacador_avalista = $distributor->bairro;
                $slip->cidade_sacador_avalista = $distributor->cidade;
                $slip->uf_sacador_avalista = $distributor->estado;
                $slip->cep_sacador_avalista = str_replace('-', '',  $distributor->cep);

                $slip->nome_pagador = $receivable->sale_detail->presale->client->razao_social;
                $slip->cpf_cnpj_pagador = str_pad(str_replace(['/', '-', '.'], '', $receivable->sale_detail->presale->client->cnpj), 14, '0', STR_PAD_LEFT);
                $slip->logradouro_pagador = $receivable->sale_detail->presale->client->logradouro;
                $slip->bairro_pagador = $receivable->sale_detail->presale->client->bairro;
                $slip->cidade_pagador = $receivable->sale_detail->presale->client->cidade;
                $slip->uf_pagador = $receivable->sale_detail->presale->client->estado;
                $slip->cep_pagador = str_replace('-', '',  $receivable->sale_detail->presale->client->cep);

                $this->PayamentSlips->save($slip);

                $receivablesTable = TableRegistry::get('Receivables');
                $receivable = $receivablesTable->get($receivable->id);
                $receivable->boleto_gerado = 1;
                $receivablesTable->save($receivable);
            }

            $batch->num_requested = $count;
            TableRegistry::get('PayamentSlipBatches')->save($batch);

            $session->delete('cartps');
            $this->Flash->success('Os boletos estão sendo gerados.');
            $this->redirect(['controller' => 'PaymentSlip', 'action' => 'index']);
        }
    }

    public function cart($do, $id = null)
    {
        $session = $this->request->getSession();
        switch ($do) {
            case 'A':
                $cart = explode(',', $session->read('cartps'));
                if (!in_array($id, $cart)) {
                    $cart[] = $id;
                }
                $session->write('cartps', implode(',', $cart));
                break;
            case 'R':
                $cart = explode(',', $session->read('cartps'));
                if (($index = array_search($id, $cart)) !== false) {
                    unset($cart[$index]);
                    $session->write('cartps', implode(',', $cart));
                }
                break;
            case 'C':
                $session->delete('cartps');
                break;
            case 'LA':
                $ids = explode(',', $id);
                $cart = explode(',', $session->read('cartps'));
                foreach ($ids as $id) {
                    if (!in_array($id, $cart)) {
                        $cart[] = $id;
                    }
                }
                $session->write('cartps', implode(',', $cart));
                break;
            case 'LR':
                $ids = explode(',', $id);
                $cart = explode(',', $session->read('cartps'));
                foreach ($ids as $id) {
                    if (($index = array_search($id, $cart)) !== false) {
                        unset($cart[$index]);
                    }
                }
                $session->write('cartps', implode(',', $cart));
                break;
        }

        $cart = explode(',', $session->read('cartps'));
        $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        $total = 0;
        foreach ($cartdb as $item) {
            $total += $item->valor;
        }

        $json['status'] = 'OK';
        $json['itens'] = sizeof($cart) - 1;
        $json['total'] = $total;
        $json['cart'] = $session->read('cartps');
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    private function modulo_10($num)
    {
        $numtotal10 = 0;
        $fator = 2;
        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // várias linhas removidas, vide função original
        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    function formata_numero($numero, $loop, $insert, $tipo = "geral")
    {
        if ($tipo == "geral") {
            $numero = str_replace(",", "", $numero);
            while (strlen($numero) < $loop) {
                $numero = $insert . $numero;
            }
        }
        if ($tipo == "valor") {
            $numero = str_replace(",", "", $numero);
            while (strlen($numero) < $loop) {
                $numero = $insert . $numero;
            }
        }
        if ($tipo == "convenio") {
            while (strlen($numero) < $loop) {
                $numero = $numero . $insert;
            }
        }
        return $numero;
    }
}
