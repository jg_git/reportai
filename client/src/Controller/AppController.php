<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

class AppController extends Controller
{

    public $ledger_entry_types = [
        'D' => 'Débito',
        'C' => 'Crédito'
    ];
    public $paginate = [
        'limit' => 20
    ];
    public $payable_types = [
        'CC' => 'Comissão de Corretagem',
        'CT' => 'Comissão de Corretagem de Transferência',
        'SP' => 'Spread',
        'FR' => 'Frete de Venda',
        'FT' => 'Frete de Transferência',
        'CV' => 'Comissão de Venda',
        'PC' => 'Parcela de Compra'
    ];
    public $receivable_types = [
        'VE' => 'Vendas'
    ];

    public function isAuthorized($user)
    {
        $whitelist = ['Users' => 'password'];
        $controller = $this->request->getParam('controller');
        $action = $this->request->getParam('action');
        $authorized = false;
        foreach ($whitelist as $c => $a) {
            if ($c == $controller and $a == $action) {
                $authorized = true;
            }
        }
        if (!$authorized) {
            $session = $this->request->getSession();
            if ($session->check('Security.role')) {
                $role = $session->read('Security.role');
            } else {
                $role = TableRegistry::get('Roles')
                    ->find()
                    ->where(['id' => $user['role_id']])
                    ->contain(['Modules' => ['sort' => ['Modules.sequence' => 'ASC']], 'Modules.ModuleActions'])
                    ->first();
                $session->write('Security.role', $role);
            }
            $resource = $controller . ':' . $action;
            foreach ($role->modules as $module) {
                if ($module->menu_link == $resource) {
                    $authorized = true;
                    break;
                }
                foreach ($module->module_actions as $action) {
                    if ($action->action == $resource) {
                        $authorized = true;
                        break 2;
                    }
                }
            }
        }
        return $authorized;
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Csrf');
        $this->loadComponent('Cookie');
        $this->loadComponent('Auth', [
            'authError' => false,
            'authenticate' => ['Form' => ['fields' => ['username' => 'email', 'password' => 'password']]],
            'authorize' => ['Controller'],
            'loginAction' => ['controller' => 'Users', 'action' => 'login'],
            'unauthorizedRedirect' => false
        ]);
        //$this->Auth->allow(['display', 'view', 'index']);
        //$this->loadComponent('Security');
    }

    public function beforeFilter(Event $event)
    {
        $vstates = [
            'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA',
            'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN',
            'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'
        ];
        $states = array_combine($vstates, $vstates);
        $entity_types = [
            'J' => 'Pessoa Jurídica',
            'F' => 'Pessoa Física'
        ];
        $account_types = [
            'C' => 'Conta Corrente',
            'P' => 'Poupança'
        ];
        $phone_types = [
            'F' => 'Fixo',
            'C' => 'Celular'
        ];
        $contact_types = [
            'D' => 'Dono',
            'G' => 'Gerente'
        ];
        $vehicle_types = [
            'T' => 'Truck',
            '4' => '4o Eixo',
            'C' => 'Cavalo'
        ];
        $account_record_types = [
            'I' => 'Interna',
            'F' => 'Financeira',
            'C' => 'Caixa'
        ];
        $payment_types = [
            'V' => 'A Vista',
            'P' => 'A Prazo'
        ];

        $ledger_entry_types = $this->ledger_entry_types;
        $payable_types = $this->payable_types;
        $receivable_types = $this->receivable_types;
        $user = $this->request->session()->read('Auth.User');
        $sets = [
            'user',
            'states',
            'entity_types',
            'account_types',
            'phone_types',
            'contact_types',
            'vehicle_types',
            'account_record_types',
            'ledger_entry_types',
            'payment_types',
            'payable_types',
            'receivable_types'
        ];
        $this->set(compact($sets));
    }

    public function checkErrors($errors)
    {
        if (sizeof($errors) > 0) {
            $this->log($errors, 'debug');
            $required = false;
            $length = false;
            foreach ($errors as $field => $error) {
                foreach ($error as $key => $msg) {
                    switch ($key) {
                        case '_empty':
                            $required = true;
                            break;
                        case 'maxLength':
                        case 'minLength':
                            $length = true;
                            break;
                    }
                }
            }
            if ($required and $length) {
                $this->Flash->error('Campos com erro na entrada de dados e não preenchidos', ['params' => ['title' => 'Dados Incorretos!']]);
            } else if ($required and !$length) {
                $this->Flash->error('Campos obrigatórios não preenchidos', ['params' => ['title' => 'Dados Incompletos!']]);
            } else if (!$required and $length) {
                $this->Flash->error('Campos com erro na entrada de dados', ['params' => ['title' => 'Dados Incorretos!']]);
            } else {
                $this->Flash->error('Campos com erro na entrada de dados', ['params' => ['title' => 'Dados Incorretos!']]);
            }
            return false;
        } else {
            return true;
        }
    }

    public function getAccounts()
    {
        $accounts = [];
        $acs = TableRegistry::get('Accounts')->find()
            ->leftJoinWith('Distributors')
            ->leftJoinWith('Banks')
            ->where(['ativa = 1'])
            ->orderAsc('Banks.nome')
            ->orderAsc('titular')
            ->orderAsc('Distributors.razao_social')
            ->contain(['Distributors', 'Banks'])
            ->toList();
        foreach ($acs as $ac) {
            if ($ac->tipo == 'F') {
                $accounts[$ac->id] = $ac->bank->nome . ' - ' . $ac->titular . ' - ' . $ac->numero_agencia . ' - ' . $ac->numero_conta;
            }
        }
        foreach ($acs as $ac) {
            if ($ac->tipo == 'C') {
                $accounts[$ac->id] = $ac->titular;
            }
        }
        foreach ($acs as $ac) {
            if ($ac->tipo == 'I') {
                $accounts[$ac->id] = $ac->distributor->razao_social . ' - ' . $ac->titular;
            }
        }
        return $accounts;
    }
    public function getAllAccounts()
    {
        $accounts = [];
        $acs = TableRegistry::get('Accounts')->find()
            ->leftJoinWith('Distributors')
            ->leftJoinWith('Banks')

            ->orderAsc('Banks.nome')
            ->orderAsc('titular')
            ->orderAsc('Distributors.razao_social')
            ->contain(['Distributors', 'Banks'])
            ->toList();
        foreach ($acs as $ac) {
            if ($ac->tipo == 'F') {
                $accounts[$ac->id] = $ac->bank->nome . ' - ' . $ac->titular . ' - ' . $ac->numero_agencia . ' - ' . $ac->numero_conta;
            }
        }
        foreach ($acs as $ac) {
            if ($ac->tipo == 'C') {
                $accounts[$ac->id] = $ac->titular;
            }
        }
        foreach ($acs as $ac) {
            if ($ac->tipo == 'I') {
                $accounts[$ac->id] = $ac->distributor->razao_social . ' - ' . $ac->titular;
            }
        }
        return $accounts;
    }
}
