<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class PollsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {
        $this->paginate = [
            'sortWhitelist' => [
                'Polls.nome_formulario',
                'Polls.tipo_formulario',
                'Locations.nome_local'
            ],
        ];
        $userPolls = TableRegistry::get('UserPolls');
        $userpolls = $userPolls->query();
        $Users = TableRegistry::get('Users');
        $users = $Users->query();

        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('nome_local')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('razao_social')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $users2 = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['Polls.nome_formulario', 'Polls.tipo_formulario', 'Locations.nome_local'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        if($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id')){
            $query = $this->Polls->find('search', ['search' => $data])->contain(['Locations'])->where('Polls.prop_id = '.$this->request->session()->read('Auth.User.prop_id').' and Polls.ativo = 1');
        }else{
            $query = $this->Polls->find('search', ['search' => $data])->contain(['Locations'])->where('Polls.prop_id = '.$this->request->session()->read('Auth.User.prop_id').' and Polls.ativo = 1 '.' AND responsaveis LIKE "%, '.$this->request->session()->read('Auth.User.id').' ,%"');
        }
        $polls = $this->paginate($query);
        $this->set(compact('users', 'users2', 'userpolls', 'locations', 'clients', 'polls'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $poll = $this->Polls->get($id, ['contain' => [
                    'Locations',
                    'PollSections'
            ]]);
        } else {
            $poll = $this->Polls->newEntity();
        }
        $this->set(compact('poll'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $poll = $this->Polls->get($id, ['contain' => ['Locations']]);
        } else {
            $poll = $this->Polls->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('id', $data)) {
                $data['id'] = '';
            }
            if($id!='new'){
                $data['id']=$id;
            }

            $id_user_add= array();
            $id_user_rem= array();

            $checagem = false;
            foreach($data as $key => $dadinho){
                //array_push($id_user_add, $key);
                if ($checagem == true){
                    if(($dadinho != 0 or $dadinho != '0') AND ($dadinho !== 'id')){
                       array_push($id_user_add, $key);
                    }else{
                        array_push($id_user_rem, $key);
                    }

                }
                if($key == "tipo_formulario"){
                    $checagem = true;
                }
            }

            $poll = $this->Polls->patchEntity($poll, $data, ['validate' => 'part1']);
            $poll->prop_id = $this->request->session()->read('Auth.User.prop_id');
            $poll->ativo = 1;
            $conn = ConnectionManager::get('default');
            if ($this->Polls->save($poll)) {

                $prop_id=$this->request->session()->read('Auth.User.prop_id');
                $poll_id = $poll->id;
                $userPolls = TableRegistry::get('UserPolls');
                $query = $userPolls->query();
                $insercoes = array();
                $remocoes = array();

                $insert_string = " , ";
                //adicionando registros no user_polls
                foreach($id_user_add as $adicao){
                    if($adicao != "id"){
                        $query_search_add = $conn->execute('SELECT * FROM user_polls');
                        $result_sel_add = $query_search_add ->fetchAll('assoc');
                        $contagem = 0;
                        foreach($result_sel_add as $checagem){
                            if($checagem['poll_id']==$poll_id AND $checagem['user_id'] == $adicao){
                                $contagem = $contagem+1;
                            }
                        }
                        $insert_string = $insert_string." , ".$adicao." , ";
                        if ($contagem==0){
                            if(is_numeric($adicao)){
                                $insercao=[
                                    'user_id' => $adicao,
                                    'poll_id' => $poll_id
                                ];
                                array_push($insercoes, $insercao);
                            }
                        }
                    }else{

                    }
                }

                $user_poll_add = TableRegistry::getTableLocator()->get('userPolls');
                $entities = $user_poll_add->newEntities($insercoes);
                $result = $user_poll_add->saveMany($entities);
                
                //removendo registros no user_polls
                foreach($id_user_rem as $removendo){
                    $query_search_rem = $conn->execute('SELECT * FROM user_polls');
                    $result_sel_rem = $query_search_rem ->fetchAll('assoc');

                    foreach($result_sel_rem as $checagem){
                        if($checagem['poll_id']==$poll_id AND $checagem['user_id'] == $removendo){
                            $entity = $userPolls->get($checagem['id']);
                            if($userPolls->delete($entity)){
                            }
                        }
                    }
                }
                $poll->responsaveis = $insert_string;
                $this->Polls->save($poll);
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $conn2 = ConnectionManager::get('default');
        $user_poll = $conn2->execute('SELECT * FROM user_polls');
        $userpoll = $user_poll ->fetchAll('assoc');
        $validacao = $this->request->session()->read('Auth.User.prop_id');
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$validacao);
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $this->set(compact('poll', 'userpoll', 'users', 'locations')); 
    }

    public function pollsection($poll_id, $id, $anchor) {
        $table = TableRegistry::get('PollSections');
        if ($id != 'new') {
            $pollsection = $table->get($id);
        } else {
            $pollsection = $table->newEntity();
            $pollsection->poll_id = $poll_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $pollsection = $table->patchEntity($pollsection, $data);
        
            if ($table->save($pollsection)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('pollsection', 'anchor'));
    }

    public function pollsectionDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-pollsection-list'] as $id) {
            $table = TableRegistry::get('PollSections');
            $pollsection = $table->get($id);
            $table->delete($pollsection);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }
    
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if(strpos($id, 'i') == true){
            $id = str_replace('i','', $id);
            $this->request->allowMethod(['post', 'deactivate']);
            $pollsTable = TableRegistry::getTableLocator()->get('Polls');
            $poll = $pollsTable->get($id);
            $poll->ativo='0';
            $pollsTable->save($poll);
            $this->Flash->success('Cadastro desativado com sucesso');
        }else if (strpos($id, 'c')==true){

            $id = str_replace('c', '', $id);
            
            $pollsTable = TableRegistry::getTableLocator()->get('Polls');
            $poll = $pollsTable->get($id);
            unset($poll->id);
            unset($poll->created);
            $conn = ConnectionManager::get('default');
            $query_search = $conn->execute('SELECT * FROM polls');
            $result_sel = $query_search ->fetchAll('assoc');
            $contagem = 0;
            foreach($result_sel as $checagem){
                if(strpos($checagem['nome_formulario'], ' (C')){
                    $z = strpos($checagem['nome_formulario'], ' (C');
                    $checagem['nome_formulario'] = substr($checagem['nome_formulario'], 0, ($z+1));
                    $poll->nome_formulario = substr(($poll->nome_formulario), 0, ($z+1));
                }
                if(strpos($checagem['nome_formulario'], $poll->nome_formulario)!== false){
                    if($checagem['prop_id'] == $this->request->session()->read('Auth.User.prop_id')){
                        $contagem = $contagem + 1;
                    }
                }
            }
            
            $poll->nome_formulario = $poll->nome_formulario. " (CÓPIA ".$contagem.")";
            $pollsInsertTable = TableRegistry::getTableLocator()->get('Polls');
            $pollsInsert = $pollsInsertTable->newEntity();
            $dados = $pollsInsertTable->patchEntity($pollsInsert, $poll->toArray());
            $pollsTable->save($dados);

            $pollsectionsTable = TableRegistry::getTableLocator()->get('PollSections');
            $pollsections = TableRegistry::get('PollSections')->find()->where('poll_id = '.$id);
            foreach($pollsections as $pollsect){
                unset($pollsect->id);
                $NEntity_pollsect = $pollsectionsTable->newEntity();
                $pollsect->poll_id = $dados->id;
                $pollsect = $pollsectionsTable->patchEntity($NEntity_pollsect, $pollsect->toArray());
                $pollsectionsTable->save($pollsect);
            }
            
            $userpollsTable = TableRegistry::getTableLocator()->get('UserPolls');
            $userpolls = TableRegistry::get('UserPolls')->find()->where('poll_id = '.$id);
            foreach($userpolls as $userpoll){
                unset($userpoll->id);
                $NEntity_userpoll = $userpollsTable->newEntity();
                $userpoll->poll_id = $dados->id;
                $userpoll = $userpollsTable->patchEntity($NEntity_userpoll, $userpoll->toArray());
                $userpollsTable->save($userpoll);
            }

            $this->Flash->success('Cadastro clonado com sucesso');

        }else{
            $poll = $this->Polls->get($id, ['contain' => []]);
            if ($this->Polls->delete($poll)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        }
        return $this->redirect($this->referer());
    }

}
