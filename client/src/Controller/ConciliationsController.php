<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ConciliationsController extends AppController {

    public function edit($data_venda = null) {
        $data_venda = is_null($data_venda) ? date('Y-m-d') : $data_venda;
        
        $sales = TableRegistry::get('Sales')->find()->where(['data_venda' => $data_venda])
                ->contain([
                    'Purchases',
                    'Purchases.Plants',
                    'Purchases.Distributors',
                    'SalesPurchases.SalesPurchasesConciliations',
                    'Carriers',
                    'Vehicles',
                    'Drivers'
                ])
                ->orderAsc('Sales.created')
                ->toList();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            // Remove as conciliações deletadas
            $purchase_list = [];
            foreach ($sales as $sale) {
                foreach ($sale->sales_purchases as $purchase) {
                    if(!in_array($purchase->purchase_id, $purchase_list)) {
                        $purchase_list[] = $purchase->purchase_id;
                    }
                    foreach ($purchase->sales_purchases_conciliations as $conciliation) {
                        $found = false;
                        foreach ($data['Sales'] as $dsale) {
                            foreach ($dsale['sales_purchases'] as $dpurchase) {
                                if (array_key_exists('sales_purchases_conciliations', $dpurchase)) {
                                    foreach ($dpurchase['sales_purchases_conciliations'] as $dconciliation) {
                                        if (array_key_exists('id', $dconciliation)) {
                                            if ($conciliation->id == $dconciliation['id']) {
                                                $found = true;
                                            }
                                        } else {
                                            $found = true;
                                        }
                                    }
                                }
                            }
                        }
                        if (!$found) {
                            $conciliation = TableRegistry::get('SalesPurchasesConciliations')->get($conciliation->id);
                            TableRegistry::get('SalesPurchasesConciliations')->delete($conciliation);
                        }
                    }
                }
            }

            $sales = TableRegistry::get('Sales')->patchEntities($sales, $data['Sales'], ['associated' => 'SalesPurchases.SalesPurchasesConciliations']);
            if (TableRegistry::get('Sales')->saveMany($sales)) {
                $sales = TableRegistry::get('Sales')->find()->where(['data_venda' => $data_venda])
                        ->contain([
                            'Purchases',
                            'Purchases.Brokers',
                            'SalesPurchases',
                            'SalesPurchases.Purchases',
                        ])
                        ->toList();
                // atualiza volumes conciliados em vendas
                foreach ($sales as $sale) {
                    if ($sale->conciliado) {
                        $volume = 0;
                        foreach ($sale->sales_purchases as $sp) {
                            $volume += $sp->volume_conciliado;
                        }
                        $sale->volume_conciliado = $volume;
                        TableRegistry::get('Sales')->save($sale);
                    }
                }
                // calcula comissoes de corretoras
                foreach ($sales as $sale) {
                    if ($sale->conciliado) {
                        foreach ($sale->purchases as $purchase_index => $purchase) {
                            if ($sale->sales_purchases[$purchase_index]->purchase->corretada) {
                                $payable = TableRegistry::get('Payables')->find()->where([
                                            'tipo' => 'CC',
                                            'sales_purchases_id' => $purchase->_joinData->id
                                        ])->first();
                                if (is_null($payable)) {
                                    $payable = TableRegistry::get('Payables')->newEntity();
                                    $payable->tipo = 'CC';
                                    $payable->data_vencimento = date('Y-m-t');
                                    $payable->valor = round($purchase->_joinData->volume_conciliado * 0.003, 2);
                                    $payable->sales_purchases_id = $purchase->_joinData->id;
                                    TableRegistry::get('Payables')->save($payable);
                                } else {
                                    if (!$payable->baixa) {
                                        $payable->valor = round($purchase->_joinData->volume_conciliado * 0.003, 2);
                                        TableRegistry::get('Payables')->save($payable);
                                    } else {
//                                        $this->Flash->error('Comissão de corretora já dada baixa para essa venda, não foi gerada/atualizada conta a pagar');
                                    }
                                }
                            }
                        }
                    }
                }
                // ajusta estoques
                foreach ($purchase_list as $purchase_id) {
                    $purchase = TableRegistry::get('Purchases')->get($purchase_id);
                    $sps = TableRegistry::get('SalesPurchases')->find()->where(['SalesPurchases.purchase_id' => $purchase->id])->contain('Sales');
                    $total = 0;
                    foreach ($sps as $sp) {
                        $total += $sp->sale->conciliado ? $sp->volume_conciliado : $sp->volume;
                    }
                    $purchase->volume_vendido = $total;
                    TableRegistry::get('Purchases')->save($purchase);
                }

                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['controller' => 'Conciliations', 'action' => 'edit', $data_venda]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $data_venda = date('d/m/Y', strtotime($data_venda));
        $today = date('d/m/Y');
        $this->set(compact('sales', 'data_venda', 'today'));
    }

    public function newConciliation($sale_index, $purchase_index, $conciliation_index) {
        $this->set(compact('sale_index', 'purchase_index', 'conciliation_index'));
    }

}
