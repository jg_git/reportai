<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class PriceSettingsController extends AppController {

    public function edit() {
        $settings = $this->PriceSettings->find();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $settings = $this->PriceSettings->patchEntities($settings, $data['PriceSettings']);
            if ($this->PriceSettings->saveMany($settings)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['controller' => 'PriceSettings', 'action' => 'edit']);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $accounts = $this->getAccounts();
        $this->set(compact('settings', 'accounts'));
    }

}
