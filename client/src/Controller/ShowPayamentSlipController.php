<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Client\FormData;
use Cake\I18n\Date;

class ShowPayamentSlipController extends AppController
{

    // public function isAuthorized($user)
    // {
    //     return true;
    // }
    //  $response = $http->post('https://oauth.itau.com.br/identify/connect/token', [
    //     'grant_type' => 'client_credentials',
    //     'client_id' => '-vpAwJj2xv4e0',
    //     'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'
    // ]);

    public function index()
    {
        //   'body' => ['grant_type' => 'client_credentials', 'client_id' => 'vpAwJj2xv4e0', 'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'],

        $http = new Client();
        $response = $http->post('https://oauth.itau.com.br/identify/connect/token', [
            'body' => ['scope' => 'readonly', 'grant_type' => 'client_credentials', 'client_id' => 'vpAwJj2xv4e0', 'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'],
            'headers' => ['Content-Type' => 'application/x-www-form-urlencoded', 'Authorization' => 'Basic headerValue']
        ]);
        $json = $response->json;

        // $session = $this->request->getSession();
        // if (strpos($this->request->env('HTTP_REFERER'), 'receivables') === false) {
        //     $session->delete('cartr');
        // }
        // $cart = explode(',', $session->read('cartr'));
        // $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        // $total = 0;
        // foreach ($cartdb as $item) {
        //     $total += $item->valor;
        // }
        // $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        // $fields = [];
        // foreach ($fields as $field) {
        //     if (array_key_exists($field, $data)) {
        //         $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
        //     }
        // }
        // if (array_key_exists('pendentes', $data)) {
        //     $data['baixa'] = 0;
        // }
        // $query = $this->Receivables->find('search', ['search' => $data])->contain([
        //     'SaleDetails',
        //     'SaleDetails.Sales',
        //     'SaleDetails.Sales.Purchases',
        //     'SaleDetails.Sales.Purchases.Brokers',
        //     'SaleDetails.Sales.Purchases.Plants',
        //     'SaleDetails.Presales',
        //     'SaleDetails.Presales.Clients',
        //     'SaleDetails.Presales.Clients.Sellers'
        // ])->orderAsc('Receivables.data_vencimento');
        // $receivables = $this->paginate($query);
        //->where(['nota_fiscal !=' => '']),

        $payamentSlips = TableRegistry::get('PayamentSlip')
            ->find('all')->orderAsc('PayamentSlip.data_vencimento');

        // debug($receivables);
        //    ])->matching('SaleDetails', function ($q) {
        //     return $q->where(['SaleDetails.nota_fiscal !=' => '']);
        // })->orderAsc('Receivables.data_vencimento');

        $accounts = $this->getAccounts();
        $payamentSlips = $this->paginate($payamentSlips);
        // $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('payamentSlips'));

        // $http = new Client([
        //     'headers' => ['Authorization' => 'Bearer ' . $accessToken]
        // ]);
        // $response = $http->get('https://example.com/api/profile/1');
        //

        $session = $this->request->session();
        $session->write('boletosel', $this->request->getData('checked'));
    }

    public function gerarPdf($ide)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $this->loadModel('PayamentSlip');
        // $id = $this->request->params['pass'][0];
        // $id = $ide;
        // $payamentSlips = $this->PayamentSlip->find('all',array('conditions' => array('PayamentSlip.id IN'=> $ide)));
        // $payamentSlips = TableRegistry::get('PayamentSlip')->find('all', ['conditions' => array("PayamentSlip.id" => '0')]);

        $payamentSlips = TableRegistry::get('PayamentSlip')->find('all')->where(['id IN' => $ide]);

        // $payamentSlips = TableRegistry::get('PayamentSlip')->find('all')->where(['id' => '88']);
        // $this->viewBuilder()->setClassName('CakePdf.Pdf');
        // $this->viewBuilder()->options([
        //     'pdfConfig' => [
        //         'orientation' => 'portrait',
        //         'filename' => 'certificate_'
        //     ],
        // ]);

        $this->set('payamentSlips', $payamentSlips);

        // $this->set('checked',null);
        //  if ($this->request->is('post')){
        //  $texto =  $this->request->getData();
        //  }
        //$this->setAction('index');
    }

    public function gerarPdfSelecionados()
    {
        if ($this->request->is('post')) {
            $boletosel = [];
            $data = [];
            // $session = $this->request->session();
            // $data =  $this->request->getData('flag');

            foreach ($this->request->getData('flag') as $key => $datum) {
                if ($datum == 1) {
                    array_push($data, $key);
                    // $this->gerarPdf($key);
                    // $this->redirect(array('controller' => 'ShowPayamentSlip', 'action' => 'gerarPdf/'.$key,));
                }
            }
            //    $this->setAction('gerarPdf',$data);

            // $checked = $this->request->getData('inpute.checked');
            // if ($data) {
            //     $this->Flash->Error('Por favor selecione ao menos um boleto ');
            //     $this->setAction('index');
            // } else {
            //     $this->Flash->Error('Não existe ');
            // }

            $this->set('data', $data);
        }
    }

    // public function settle()
    // {
    //     $data = $this->request->getData();
    //     $session = $this->request->getSession();
    //     $cart = explode(',', $session->read('cartr'));
    //     foreach ($cart as $receivable_id) {
    //         if ($receivable_id != '' and !is_null($receivable_id)) {
    //             $receivable = TableRegistry::get('Receivables')->get($receivable_id);
    //             $entry = TableRegistry::get('LedgerEntries')->newEntity();
    //             $entry->account_id = $data['account_id'];
    //             $entry->data_lancamento = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
    //             $entry->payment_method_id = $data['payment_method_id'];
    //             switch ($receivable->tipo) {
    //                 case 'VE':
    //                     $entry->ledger_entry_type_id = 21;
    //                     $entry->receivable_id = $receivable->id;
    //                     break;
    //             }
    //             $entry->valor = $receivable->valor;
    //             TableRegistry::get('LedgerEntries')->save($entry);
    //             $receivable->data_baixa = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
    //             $receivable->baixa = 1;
    //             $this->Receivables->save($receivable);
    //         }
    //     }
    //     $session->delete('cartr');
    //     $json['status'] = 'OK';
    //     $this->set(['json' => $json, '_serialize' => 'json']);
    //     $this->RequestHandler->renderAs($this, 'json');
    // }

    public function gerarPdfhtml()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $this->loadModel('PayamentSlip');
        $id = $this->request->params['pass'][0];
        // $payamentslips = $this->PayamentSlip->find($id);
        $payamentSlips = TableRegistry::get('PayamentSlip')->find('all')->where(['id' => $id]);

        // $this->viewBuilder()->setClassName('CakePdf.Pdf');
        // $this->viewBuilder()->options([
        //     'pdfConfig' => [
        //         'orientation' => 'portrait',
        //         'filename' => 'certificate_'
        //     ],
        // ]);

        $this->set('payamentSlips', $payamentSlips);
    }

    public function test()
    {
    }
}
