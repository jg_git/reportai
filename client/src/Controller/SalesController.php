<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use DateTime;

class SalesController extends AppController {

    public function edit($data_venda = null) {
        $data_venda = is_null($data_venda) ? date('Y-m-d') : $data_venda;
        $sales = $this->Sales->find()->where(['data_venda' => $data_venda])
                ->contain([
                    'Carriers',
                    'Drivers',
                    'Vehicles',
                    'Vehicles.Carts',
                    'Purchases',
                    'Purchases.Plants',
                    'Purchases.Distributors',
                    'SaleDetails',
                    'SaleDetails.Receivables',
                    'SaleDetails.Presales',
                    'SaleDetails.Presales.Clients',
                    'SaleDetails.Presales.Child'
                ])
                ->orderAsc('Sales.created')
                ->toList();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
//            $this->log("Salvando Venda", 'debug');
//            $this->log($data, 'debug');
            $saved = true;
            if (array_key_exists('Sales', $data)) {
                foreach (array_keys($data['Sales']) as $i) {
                    if (!array_key_exists('sale_details', $data['Sales'][$i])) {
                        $data['Sales'][$i]['sale_details'] = [];
                    }
                }
                if (gettype($data['delete-detail-list']) == 'array') {
                    foreach ($data['delete-detail-list'] as $detail_id) {
                        $detail = TableRegistry::get('SaleDetails')->get($detail_id);
                        $presale = TableRegistry::get('Presales')->get($detail->presale_id);
                        $presale->volume_vendido -= $detail->volume;
                        TableRegistry::get('Presales')->save($presale);

                        $receivables = TableRegistry::get('Receivables')->find()->where(['sale_detail_id' => $detail_id]);
                        foreach ($receivables as $receivable) {
                            TableRegistry::get('Receivables')->delete($receivable);
                        }
                        $payables = TableRegistry::get('Payables')->find()->where(['sale_detail_id' => $detail_id]);
                        foreach ($payables as $payable) {
                            TableRegistry::get('Payables')->delete($payable);
                        }
                    }
                }
                $sales = $this->Sales->patchEntities($sales, $data['Sales']);
                foreach ($sales as $sale) {
                    foreach ($sale->purchases as $purchase) {
                        if (is_null($purchase->_joinData->spread)) {
                            $purchase->_joinData->spread = 0;
                        }
                    }
                }
                $saved = $this->Sales->saveMany($sales);
            }
            if ($saved) {
                // ajusta volume vendido referente a pre-vendas
                foreach ($sales as $sale) {
                    foreach ($sale->sale_details as $detail) {
                        $presale = TableRegistry::get('Presales')->get($detail->presale_id);
                        $sds = TableRegistry::get('SaleDetails')->find()->where(['presale_id' => $presale->id]);
                        $v = 0;
                        foreach ($sds as $sd) {
                            $v += $sd->volume;
                        }
                        $presale->volume_vendido = $v;
                        TableRegistry::get('Presales')->save($presale);
                    }
                }
                $sales = $this->Sales->find()->where(['data_venda' => $data_venda])->contain([
                            'Carriers',
                            'Drivers',
                            'Vehicles',
                            'Purchases',
                            'Purchases.Plants',
                            'Purchases.Distributors',
                            'SaleDetails',
                            'SaleDetails.Presales',
                            'SaleDetails.Presales.Clients',
                            'SaleDetails.Presales.Child',
                        ])->toList();
                // salva frete e spread no contas a receber
                $payables = TableRegistry::get('Payables');
                foreach ($sales as $sale) {
                    foreach ($sale->purchases as $purchase) {
                        $payable = $payables->find()
                                ->where([
                                    'sales_purchases_id' => $purchase->_joinData->id,
                                    'tipo' => 'SP'
                                ])
                                ->first();
                        if (is_null($payable)) {
                            $payable = $payables->newEntity();
                            $payable->tipo = 'SP';
                            $payable->data_vencimento = date('Y-m-t');
                            $payable->valor = $purchase->_joinData->spread;
                            $payable->sales_purchases_id = $purchase->_joinData->id;
                            $payables->save($payable);
                        } else {
                            if (!$payable->baixa) {
                                $payable->valor = $purchase->_joinData->spread;
                                $payables->save($payable);
                            } else {
//                                $this->Flash->error('Spread já dado baixa para ' . $sale->purchase->distributor->razao_social . ', não foi gerada/atualizada conta a pagar');
                            }
                        }
                    }
                    $payable = $payables->find()->where(['sale_id' => $sale->id])->andWhere(['tipo' => 'FR'])->first();
                    if (is_null($payable)) {
                        $payable = $payables->newEntity();
                        $payable->tipo = 'FR';
                        $payable->data_vencimento = date('Y-m-t');
                        $payable->valor = $sale->frete;
                        $payable->sale_id = $sale->id;
                        $payables->save($payable);
                    } else {
                        if (!$payable->baixa) {
                            $payable->valor = $sale->frete;
                            $payables->save($payable);
                        } else {
//                            $this->Flash->error('Frete já dado baixa para transportadora ' . $sale->carrier->razao_social . ', não foi gerada/atualizada conta a pagar');
                        }
                    }
                }
                foreach ($sales as $sale) {
                    foreach ($sale->sale_details as $detail) {
                        $presale = $detail->presale;
                        // ajusta volume vendido referente a pre-vendas
                        $sds = TableRegistry::get('SaleDetails')->find()->where(['presale_id' => $presale->id]);
                        $v = 0;
                        foreach ($sds as $sd) {
                            $v += $sd->volume;
                        }
                        $presale->volume_vendido = $v;
                        TableRegistry::get('Presales')->save($presale);
                        // salva venda no contas a receber
                        $parcelas = 0;
                        for ($i = 1; $i <= 4; $i++) {
                            if (!is_null($presale->get('data_pagamento_' . $i))) {
                                $parcelas++;
                            }
                        }
                        $receivables = TableRegistry::get('Receivables')->find()->where(['sale_detail_id' => $detail->id, 'tipo' => 'VE']);
                        for ($i = 1; $i <= 4; $i++) {
                            if (!is_null($presale->get('data_pagamento_' . $i))) {
                                $found = false;
                                foreach ($receivables as $receivable) {
                                    if ($receivable->data_vencimento == $presale->get('data_pagamento_' . $i)) {
                                        $found = true;
                                        if (!$receivable->baixa) {
                                            $total = $detail->volume * $presale->preco_venda;
                                            $parcela = round($total / $parcelas, 2);
                                            $receivable->valor = ($i < $parcelas) ? $parcela : $total - ($parcela * ($parcelas - 1));
                                            TableRegistry::get('Receivables')->save($receivable);
                                        } else {
                                            break 2;
                                        }
                                    }
                                }
                                if (!$found) {
                                    $receivable = TableRegistry::get('Receivables')->newEntity();
                                    $receivable->tipo = 'VE';
                                    $receivable->data_vencimento = $presale->get('data_pagamento_' . $i);
                                    $total = $detail->volume * $presale->preco_venda;
                                    $parcela = round($total / $parcelas, 2);
                                    $receivable->valor = ($i < $parcelas) ? $parcela : $total - ($parcela * ($parcelas - 1));
                                    $receivable->sale_detail_id = $detail->id;
                                    TableRegistry::get('Receivables')->save($receivable);
                                }
                            }
                        }
                    }
                }
                // remove vendas
                if (gettype($data['delete-sale-list']) == 'array') {
                    $this->log("delete sale list", 'debug');
                    $this->log($data['delete-sale-list'], 'debug');
                    foreach ($data['delete-sale-list'] as $sale_id) {
                        $sale = TableRegistry::get('Sales')->get($sale_id, ['contain' => [
                                'SaleDetails',
                                'SalesPurchases'
                        ]]);
                        $found = false;
                        if ($sale->conciliado) {
                            $found = true;
                        }
                        $fretes_spreads = TableRegistry::get('Payables')->find()->where(['sale_id' => $sale->id]);
                        foreach ($fretes_spreads as $fs) {
                            if ($fs->baixa) {
                                $found = true;
                            }
                        }
                        $details = [];
                        foreach ($sale->sale_details as $detail) {
                            $details[] = $detail->id;
                        }
                        $receivables = TableRegistry::get('Receivables')->find()->where(['sale_detail_id IN' => $details]);
                        foreach ($receivables as $receivable) {
                            if ($receivable->baixa) {
                                $found = true;
                            }
                        }
                        $payables = TableRegistry::get('Payables')->find()->where(['sale_detail_id IN' => $details]);
                        foreach ($payables as $payable) {
                            if ($payable->baixa) {
                                $found = true;
                            }
                        }
                        $sps = [];
                        foreach ($sale->sales_purchases as $sp) {
                            $sps[] = $sp->id;
                        }
                        if (sizeof($sps) > 0) {
                            $payables_sps = TableRegistry::get('Payables')->find()->where(['sales_purchases_id IN' => $sps]);
                            foreach ($payables_sps as $payable) {
                                if ($payable->baixa) {
                                    $found = true;
                                }
                            }
                        }
                        // nenhuma dependencia encontrada, remove a venda
                        if (!$found) {
                            // ajusta prevendas devido a essa remocao
                            foreach ($sale->sale_details as $detail) {
                                $detail = TableRegistry::get('SaleDetails')->get($detail->id);
                                $presale = TableRegistry::get('Presales')->get($detail->presale_id);
                                $presale->volume_vendido -= $detail->volume;
                                TableRegistry::get('Presales')->save($presale);
                            }
                            foreach ($fretes_spreads as $fs) {
                                TableRegistry::get('Payables')->delete($fs);
                            }
                            foreach ($receivables as $receivable) {

                                $slips = TableRegistry::get('PayamentSlips')->find()->where(['receivable_id' => $receivable->id]);
                                foreach ($slips as $slip) {
                                    TableRegistry::get('PayamentSlips')->delete($slip);
                                }

                                TableRegistry::get('Receivables')->delete($receivable);
                            }
                            foreach ($payables as $payable) {
                                TableRegistry::get('Payables')->delete($payable);
                            }
                            if (sizeof($sps) > 0) {
                                foreach ($payables_sps as $payable) {
                                    TableRegistry::get('Payables')->delete($payable);
                                }
                                $sales_purchases_conciliations = TableRegistry::get('SalesPurchasesConciliations')->find()->where(['sales_purchases_id IN' => $sps]);
                                foreach ($sales_purchases_conciliations as $spc) {
                                    TableRegistry::get('SalesPurchasesConciliations')->delete($spc);
                                }
                                $sale_purchases = TableRegistry::get('SalesPurchases')->find()->where(['id IN' => $sps]);
                                foreach ($sale_purchases as $sale_purchase) {
                                    $purchase = TableRegistry::get('Purchases')->get($sale_purchase->purchase_id);
                                    $purchase->volume_vendido -= $sale_purchase->volume;
                                    TableRegistry::get('Purchases')->save($purchase);
                                    TableRegistry::get('SalesPurchases')->delete($sale_purchase);
                                }
                            }
                            TableRegistry::get('Sales')->delete($sale);
                        } else {
                            $this->Flash->error('Já existem contas a pagar/receber associadas a essa venda que foram quitadas e/ou venda já conciliada', [
                                'params' => ['title' => 'Venda não pode ser removida']
                            ]);
                        }
                    }
                }
                $sales = $this->Sales->find()->where(['data_venda' => $data_venda])->contain([
                    'Purchases',
                    'SaleDetails',
                    'SaleDetails.Presales',
                    'SaleDetails.Presales.Clients',
                    'SaleDetails.Presales.Clients.Sellers',
                ]);
                $price_settings = TableRegistry::get('PriceSettings')->find();
                foreach ($sales as $sale) {
                    foreach ($sale->sale_details as $detail) {
                        // ==== Comissao de vendedores ====
                        $pd = TableRegistry::get('SaleDetails')->get($detail->id);
                        $presale = $detail->presale;
                        if (!is_null($presale->client->seller)) {
                            $max_payment_date = '';
                            for ($i = 1; $i <= 4; $i++) {
                                $d = date('Y-m-d', strtotime(str_replace('/', '-', $presale->get('data_pagamento_' . $i))));
                                if ($d > $max_payment_date) {
                                    $max_payment_date = $d;
                                }
                            }
                            $date1 = new DateTime(date('Y-m-d', strtotime(str_replace('/', '-', $presale->data_pedido))));
                            $date2 = new DateTime($max_payment_date);
                            $interval = $date1->diff($date2)->days;
                            foreach ($price_settings as $setting) {
                                if (substr($setting->tag, 0, 9) == 'acrescimo') {
                                    if (($interval >= $setting->dias_de) and ( $interval <= $setting->dias_ate)) {
                                        $market_price = $presale->preco_mercado + $setting->valor;
                                        if (round($presale->preco_venda, 4) < round($market_price, 4)) {
                                            foreach ($price_settings as $setting) {
                                                if ($setting->tag == 'comissao_abaixo_mercado') {
                                                    $pd->comissao = $detail->volume * $setting->valor;
                                                    break;
                                                }
                                            }
                                        } else if (round($presale->preco_venda, 4) == round($market_price, 4)) {
                                            foreach ($price_settings as $setting) {
                                                if ($setting->tag == 'comissao_preco_justo') {
                                                    $pd->comissao = $detail->volume * $setting->valor;
                                                    break;
                                                }
                                            }
                                        } else {
                                            $pd->comissao = $detail->volume * ($presale->preco_venda - $market_price);
                                        }
                                        TableRegistry::get('SaleDetails')->save($pd);
                                        break;
                                    }
                                }
                            }
                            $payables = TableRegistry::get('Payables');
                            $payable = $payables->find()->where([
                                        'tipo' => 'CV',
                                        'sale_detail_id' => $pd->id
                                    ])->first();
                            if (is_null($payable)) {
                                $payable = $payables->newEntity();
                                $payable->tipo = 'CV';
                                $payable->data_vencimento = date('Y-m-t');
                                $payable->valor = $pd->comissao;
                                $payable->sale_detail_id = $pd->id;
                                $payables->save($payable);
                            } else {
                                if (!$payable->baixa) {
                                    $payable->valor = $pd->comissao;
                                    $payables->save($payable);
                                } else {
//                                    $this->Flash->error('Comissão de vendedor já dada baixa para uma venda, não foi gerada/atualizada conta a pagar');
                                }
                            }
                        }
                    }
                }
                $this->Flash->success('Cadastro salvo com sucesso');
                // ajusta estoques
                $purchases = TableRegistry::get('Purchases')->find()->where(['volume_comprado + volume_transferido - volume_transito - volume_devolvido - volume_vendido > 0']);
                foreach ($purchases as $purchase) {
                    $sps = TableRegistry::get('SalesPurchases')->find()->where(['SalesPurchases.purchase_id' => $purchase->id])->contain('Sales');
                    $total = 0;
                    foreach ($sps as $sp) {
                        $total += $sp->sale->conciliado ? $sp->volume_conciliado : $sp->volume;
                    }
                    $purchase->volume_vendido = $total;
                }
                TableRegistry::get('Purchases')->saveMany($purchases);
                $this->redirect(['controller' => 'Sales', 'action' => 'edit', $data_venda]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        // contratos com estoque disponivel
        $purchases = TableRegistry::get('Purchases')->find()
                ->where(['volume_comprado + volume_transferido - volume_devolvido - volume_transito - volume_vendido > 0'])
                ->contain(['Plants', 'Distributors'])
                ->orderAsc('Distributors.nome_fantasia')
                ->orderAsc('Plants.nome_fantasia')
                ->orderAsc('Purchases.created')
                ->toList();
        // contratos sem estoque mas que fazem parte de vendas
        $purchase_list = [];
        foreach ($sales as $s) {
            foreach ($s->purchases as $ps) {
                $found = false;
                foreach ($purchases as $pp) {
                    if ($pp->id == $ps->id) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $found = false;
                    foreach ($purchase_list as $p) {
                        if ($ps->id == $p) {
                            $found = true;
                        }
                    }
                    if (!$found) {
                        $purchase_list[] = $ps->id;
                    }
                }
            }
        }
        if (sizeof($purchase_list) > 0) {
            $purchases_in_use = TableRegistry::get('Purchases')->find()
                    ->where(['Purchases.id IN' => $purchase_list])
                    ->contain(['Plants', 'Distributors'])
                    ->toList();
        } else {
            $purchases_in_use = null;
        }
        // pre vendas ainda não preenchidas
        $presales = TableRegistry::get('Presales')->find()
                ->where([
                    'volume_pedido != volume_vendido',
                    'cancelado = 0'
                ])
                ->contain(['Clients'])
                ->orderAsc('data_pedido')
                ->toList();
        $carriers = TableRegistry::get('Carriers')->find()
                ->contain([
                    'Drivers',
                    'Vehicles',
                    'Vehicles.VehicleOrifices',
                    'Vehicles.Carts'
                ])
                ->orderAsc('razao_social')
                ->toList();
        $distributors = TableRegistry::get('Distributors')->find();
        $price_settings = TableRegistry::get('PriceSettings')->find()->toList();
        $accounts = $this->getAccounts();
        $data_venda = date('d/m/Y', strtotime($data_venda));
        $this->set(compact('sales', 'data_venda', 'carriers', 'purchases', 'purchases_in_use', 'presales', 'distributors', 'price_settings', 'accounts'));
    }

    public function driver($carrier_id, $sale_index) {
        $drivers = TableRegistry::get('Drivers')->find('list')->matching('Carriers', function($query) use ($carrier_id) {
            return $query->where(['carrier_id' => $carrier_id]);
        });
        $this->set(compact('drivers', 'sale_index'));
    }

    public function vehicle($carrier_id, $sale_index) {
        $vehicles = TableRegistry::get('Vehicles')->find()->matching('Carriers', function($query) use ($carrier_id) {
                    return $query->where(['carrier_id' => $carrier_id]);
                })->contain(['VehicleOrifices']);
        $this->set(compact('vehicles', 'sale_index'));
    }

    public function newSale($sale_index) {
        $carriers = TableRegistry::get('Carriers')->find('list')->orderAsc('razao_social');
        $this->set(compact('carriers', 'sale_index'));
    }

    public function newDetail($sale_index, $detail_index, $presale_id) {
        $accounts = $this->getAccounts();
        $default_account = TableRegistry::get('PriceSettings')->find()->where(['tag' => 'conta_padrao'])->first();
        $this->set(compact('sale_index', 'detail_index', 'presale_id', 'accounts', 'default_account'));
    }

    public function newPurchase($sale_index, $purchase_index, $purchase_id) {
        $purchase = TableRegistry::get('Purchases')->get($purchase_id, ['contain' => ['Plants', 'Distributors']]);
        $this->set(compact('sale_index', 'purchase_index', 'purchase'));
    }

    public function presale($id) {
        $presale = TableRegistry::get('Presales')->get($id, ['contain' => ['Clients']]);
        $json['id'] = $presale->id;
        $json['client_id'] = $presale->client_id;
        $json['nome_fantasia'] = $presale->client->nome_fantasia;
        $json['volume_pedido'] = $presale->volume_pedido;
        $json['volume_vendido'] = $presale->volume_vendido;
        $json['data_pedido'] = date('Y-m-d', strtotime(str_replace('/', '-', $presale->data_pedido)));
        $json['preco_venda'] = $presale->preco_venda;
        $json['preco_mercado'] = $presale->preco_mercado;
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function printout($data_venda, $sale_list) {
        $sales = $this->Sales->find()->where(['data_venda' => $data_venda])
                ->contain([
                    'Carriers',
                    'Drivers',
                    'Vehicles',
                    'Vehicles.Carts',
                    'SalesPurchases',
                    'SalesPurchases.Purchases.Plants',
                    'SalesPurchases.Purchases.Distributors',
                    'SaleDetails',
                    'SaleDetails.Receivables',
                    'SaleDetails.Presales',
                    'SaleDetails.Presales.Clients',
                    'SaleDetails.Presales.Child'
                ])
                ->orderAsc('Sales.created')
                ->toList();
        $data = date('d/m/Y', strtotime($data_venda));
        $this->set(compact('data', 'sales', 'sale_list'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $preSale = $this->Sales->get($id);
        if ($this->Sales->delete($preSale)) {
            $this->Flash->success(__('The pre sale has been deleted.'));
        } else {
            $this->Flash->error(__('The pre sale could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
