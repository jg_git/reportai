<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class VehiclesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['marca_modelo'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Vehicles->find('search', ['search' => $data])->contain(['VehicleOrifices']);
        $vehicles = $this->paginate($query);
        $this->set(compact('vehicles'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $vehicle = $this->Vehicles->get($id, ['contain' => ['VehicleOrifices', 'Carts']]);
        } else {
            $vehicle = $this->Vehicles->newEntity();
        }
        $this->set(compact('vehicle'));
    }

    public function basic($id) {
        if ($id != 'new') {
            $vehicle = $this->Vehicles->get($id, []);
        } else {
            $vehicle = $this->Vehicles->newEntity();
            $vehicle->tipo = 'T';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $vehicle = $this->Vehicles->patchEntity($vehicle, $data);
            if ($this->Vehicles->save($vehicle)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('vehicle'));
    }

    public function orifice($vehicle_id, $id, $anchor) {
        $table = TableRegistry::get('VehicleOrifices');
        if ($id != 'new') {
            $orifice = $table->get($id);
        } else {
            $orifice = $table->newEntity();
            $orifice->vehicle_id = $vehicle_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $orifice = $table->patchEntity($orifice, $data);
            if ($table->save($orifice)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('orifice', 'anchor'));
    }

    public function orificeDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-orifice-list']) == 'array') {
            foreach ($data['delete-orifice-list'] as $id) {
                $table = TableRegistry::get('VehicleEmails');
                $orifice = $table->get($id);
                $table->delete($orifice);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function cart($vehicle_id, $anchor) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $cart = TableRegistry::get('Carts')->get($data['cart_id']);
            $cart->vehicle_id = $vehicle_id;
            TableRegistry::get('Carts')->save($cart);
        }
        $carts = TableRegistry::get('Carts')->find('list')->where(['vehicle_id IS' => null]);
        $this->set(compact('carts', 'anchor'));
    }

    public function cartList($vehicle_id) {
        $carts = TableRegistry::get('Carts')->find()->where(['vehicle_id' => $vehicle_id])->toList();
        $this->set(compact('carts'));
    }

    public function cartDelete() {
        $this->request->allowMethod(['post']);
        $data = $this->request->getData();
        foreach ($data['delete-cart-list'] as $id) {
            $table = TableRegistry::get('Carts');
            $cart = $table->get($id);
            $cart->vehicle_id = null;
            $table->save($cart);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $table = TableRegistry::get('Carts');
        $carts = $table->find()->where(['vehicle_id' => $id]);
        foreach ($carts as $cart) {
            $cart->vehicle_id = null;
            $table->save($cart);
        }
        $vehicle = $this->Vehicles->get($id);
        if ($this->Vehicles->delete($vehicle)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
