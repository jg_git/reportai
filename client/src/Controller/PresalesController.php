<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class PresalesController extends AppController {

    public function edit($data_pedido = null) {
        $data_pedido = is_null($data_pedido) ? date('Y-m-d') : $data_pedido;
        $presales = $this->Presales->find()->where(['data_pedido' => $data_pedido])->contain(['Clients'])->toList();
        $previous_data = null;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (array_key_exists('Presales', $data)) {
                $presales = $this->Presales->patchEntities($presales, $data['Presales']);
                if ($this->Presales->saveMany($presales)) {
                    if (gettype($data['delete-presale-list']) == 'array') {
                        foreach ($data['delete-presale-list'] as $presale_id) {
                            if (TableRegistry::get('SaleDetails')->find()->where(['presale_id' => $presale_id])->count() == 0) {
                                $presale = $this->Presales->get($presale_id);
                                $this->Presales->delete($presale);
                            }
                        }
                    }
                    if (gettype($data['cancel-presale-list']) == 'array') {
                        foreach ($data['cancel-presale-list'] as $presale_id) {
                            $p = $this->Presales->get($presale_id);
                            $p->cancelado = 1;
                            $this->Presales->save($p);
                        }
                    }
                    $this->Flash->success('Cadastro salvo com sucesso');
                    $this->redirect(['action' => 'edit', $data_pedido]);
                } else {
                    $previous_data = $data;
                    $this->Flash->error('Erro no salvamento do cadastro');
                }
            }
        }
        $p = TableRegistry::get('Prices')->find()->orderDesc('created')->first();
        $preco = $p->preco;
        $price_settings = TableRegistry::get('PriceSettings')->find()->toList();
        $sales = TableRegistry::get('Sales')->find()->where(['data_venda' => $data_pedido])->contain(['SaleDetails'])->toList();
        $data_pedido = date('d/m/Y', strtotime($data_pedido));
        $this->set(compact('presales', 'sales', 'data_pedido', 'preco', 'price_settings', 'previous_data'));
    }

    public function newSale($index) {
        $this->set(compact('index'));
    }

    public function changeDate() {
        $this->request->allowMethod(['post']);
        $data = $this->request->getData();
        $presale = $this->Presales->get($data['presale_id']);
        $presale->data_pedido = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_pedido'])));
        $this->Presales->save($presale);
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function updatePrice() {
        $data = $this->request->getData();
        $price = TableRegistry::get('Prices')->newEntity();
        $price->preco = str_replace(',', '.', str_replace('.', '', $data['price_now']));
        TableRegistry::get('Prices')->save($price);
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $preSale = $this->Presales->get($id);
        if ($this->Presales->delete($preSale)) {
            $this->Flash->success(__('The pre sale has been deleted.'));
        } else {
            $this->Flash->error(__('The pre sale could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }

}
