<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class CarriersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['razao_social', 'nome_fantasia'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Carriers->find('search', ['search' => $data]);
        $carriers = $this->paginate($query);
        $this->set(compact('carriers'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $carrier = $this->Carriers->get($id, ['contain' => [
                    'CarrierEmails',
                    'CarrierPhones',
                    'CarrierContacts',
                    'Drivers',
                    'Vehicles',
                    'Vehicles.VehicleOrifices',
                    'CarrierBankAccounts',
                    'CarrierBankAccounts.Banks'
            ]]);
        } else {
            $carrier = $this->Carriers->newEntity();
        }
        $drivers = TableRegistry::get('Drivers')->find();
        $vehicles = TableRegistry::get('Vehicles')->find()->contain(['VehicleOrifices']);
        $this->set(compact('carrier', 'drivers', 'vehicles'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $carrier = $this->Carriers->get($id, ['contain' => []]);
        } else {
            $carrier = $this->Carriers->newEntity();
            $carrier->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }
            $carrier = $this->Carriers->patchEntity($carrier, $data, ['validate' => 'part1']);
            if ($this->Carriers->save($carrier)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('carrier'));
    }

    public function part2($id) {
        if ($id != 'new') {
            $carrier = $this->Carriers->get($id, ['contain' => []]);
        } else {
            $carrier = $this->Carriers->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $carrier = $this->Carriers->patchEntity($carrier, $data, ['validate' => 'part2']);
            if ($this->Carriers->save($carrier)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('carrier'));
    }

    public function pivot() {
        $data = $this->request->getData();
        $carrier = $this->Carriers->get($data['carrier_id'], ['contain' => ['Drivers', 'Vehicles']]);
        foreach ($carrier->drivers as $driver) {
            $found = false;
            if (array_key_exists('drivers', $data)) {
                foreach ($data['drivers'] as $dd) {
                    if ($driver->id == $dd) {
                        $found = true;
                        break;
                    }
                }
            }
            if (!$found) {
                $this->Carriers->Drivers->unlink($carrier, [$driver]);
            }
        }
        if (array_key_exists('drivers', $data)) {
            foreach ($data['drivers'] as $dd) {
                $found = false;
                foreach ($carrier->drivers as $driver) {
                    if ($driver->id == $dd) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $driver = TableRegistry::get('Drivers')->get($dd);
                    $this->Carriers->Drivers->link($carrier, [$driver]);
                }
            }
        }
        foreach ($carrier->vehicles as $vehicle) {
            $found = false;
            if (array_key_exists('vehicles', $data)) {
                foreach ($data['vehicles'] as $dv) {
                    if ($vehicle->id == $dv) {
                        $found = true;
                        break;
                    }
                }
            }
            if (!$found) {
                $this->Carriers->Vehicles->unlink($carrier, [$vehicle]);
            }
        }
        if (array_key_exists('vehicles', $data)) {
            foreach ($data['vehicles'] as $dv) {
                $found = false;
                foreach ($carrier->vehicles as $vehicle) {
                    if ($vehicle->id == $dv) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $vehicle = TableRegistry::get('Vehicles')->get($dv);
                    $this->Carriers->Vehicles->link($carrier, [$vehicle]);
                }
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function email($carrier_id, $id, $anchor) {
        $table = TableRegistry::get('CarrierEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->carrier_id = $carrier_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($carrier_id, $id, $anchor) {
        $table = TableRegistry::get('CarrierPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->carrier_id = $carrier_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    public function account($carrier_id, $id, $anchor) {
        $table = TableRegistry::get('CarrierBankAccounts');
        if ($id != 'new') {
            $account = $table->get($id);
        } else {
            $account = $table->newEntity();
            $account->carrier_id = $carrier_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $table->patchEntity($account, $data);
            if ($table->save($account)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $bank_list = TableRegistry::get('Banks')->find()->orderAsc('nome');
        $banks = [];
        foreach ($bank_list as $bank) {
            $banks[$bank->id] = $bank->codigo . ' - ' . $bank->nome;
        }
        $this->set(compact('account', 'banks', 'anchor'));
    }

    public function contact($carrier_id, $id, $anchor) {
        $table = TableRegistry::get('CarrierContacts');
        if ($id != 'new') {
            $contact = $table->get($id);
        } else {
            $contact = $table->newEntity();
            $contact->carrier_id = $carrier_id;
            $contact->tipo = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $contact = $table->patchEntity($contact, $data);
            if ($table->save($contact)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('contact', 'anchor'));
    }

    public function emailDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('CarrierEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('CarrierPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function accountDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-account-list'] as $id) {
            $table = TableRegistry::get('CarrierBankAccounts');
            $account = $table->get($id);
            $table->delete($account);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function contactDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-contact-list'] as $id) {
            $table = TableRegistry::get('CarrierContacts');
            $contact = $table->get($id);
            $table->delete($contact);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $carrier = $this->Carriers->get($id);
        if ($this->Carriers->delete($carrier)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
