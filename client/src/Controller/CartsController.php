<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class CartsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['identificacao'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Carts->find('search', ['search' => $data])->contain(['CartOrifices']);
        $carts = $this->paginate($query);
        $this->set(compact('carts'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $cart = $this->Carts->get($id, ['contain' => ['CartOrifices']]);
        } else {
            $cart = $this->Carts->newEntity();
        }
        $this->set(compact('cart'));
    }

    public function basic($id) {
        if ($id != 'new') {
            $cart = $this->Carts->get($id);
        } else {
            $cart = $this->Carts->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $cart = $this->Carts->patchEntity($cart, $data);
            if ($this->Carts->save($cart)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('cart'));
    }

    public function orifice($cart_id, $id, $anchor) {
        $table = TableRegistry::get('CartOrifices');
        if ($id != 'new') {
            $orifice = $table->get($id);
        } else {
            $orifice = $table->newEntity();
            $orifice->cart_id = $cart_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $orifice = $table->patchEntity($orifice, $data);
            if ($table->save($orifice)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('orifice', 'anchor'));
    }

    public function orificeDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-orifice-list']) == 'array') {
            foreach ($data['delete-orifice-list'] as $id) {
                $table = TableRegistry::get('CartEmails');
                $orifice = $table->get($id);
                $table->delete($orifice);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $cart = $this->Carts->get($id);
        if ($this->Carts->delete($cart)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
