<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class AccountsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
                'ativa' => '0'
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['titular'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Accounts->find('search', ['search' => $data])->contain(['Distributors', 'Banks']);
        $accounts = $this->paginate($query);
        $distributors = TableRegistry::get('Distributors')->find('list')->orderAsc('razao_social');
        $banks = TableRegistry::get('Banks')->find('list')->orderAsc('nome');
        $this->set(compact('accounts', 'distributors', 'banks'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $account = $this->Accounts->get($id, ['contain' => ['DistributorContacts']]);
        } else {
            $account = $this->Accounts->newEntity();
        }
        $this->set(compact('account'));
    }

    public function basic($id) {
        $this->viewBuilder()->setLayout('simple');
        if ($id != 'new') {
            $account = $this->Accounts->get($id);
        } else {
            $account = $this->Accounts->newEntity();
            $account->tipo = 'I';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $this->Accounts->patchEntity($account, $data);
            if ($data['tipo'] == 'I') {
                $account->bank_id = null;
            } else {
                $account->distributor_id = null;
            }
            if ($this->Accounts->save($account)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $distributors = TableRegistry::get('Distributors')->find('list')->orderAsc('razao_social');
        $banks = TableRegistry::get('Banks')->find('list')->orderAsc('nome');
        $this->set(compact('account', 'distributors', 'banks'));
    }

    public function active($id, $active) {
        $account = TableRegistry::get('Accounts')->get($id);
        $account->ativa = ($active == 'Y') ? 1 : 0;
        TableRegistry::get('Accounts')->save($account);
        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function contact($account_id, $anchor) {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $this->Accounts->get($account_id);
            $contact = TableRegistry::get('DistributorContacts')->get($data['contact_id']);
            $this->Accounts->DistributorContacts->link($account, [$contact]);
        }
        $contacts = TableRegistry::get('DistributorContacts')->find('list')->orderAsc('nome');
        $this->set(compact('contacts', 'anchor'));
    }

    public function contactList($account_id) {
        $this->viewBuilder()->setLayout('simple');
        $account = $this->Accounts->get($account_id, ['contain' => ['DistributorContacts']]);
        $this->set(compact('account'));
    }

    public function contactDelete($account_id) {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        $account = $this->Accounts->get($account_id);
        if (gettype($data['delete-contact-list']) == 'array') {
            $table = TableRegistry::get('DistributorContacts');
            foreach ($data['delete-contact-list'] as $id) {
                $contact = $table->get($id);
                $this->Accounts->DistributorContacts->unlink($account, [$contact]);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $account = $this->Accounts->get($id);
        if ($this->Accounts->delete($account)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
