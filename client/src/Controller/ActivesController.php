<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class ActivesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {

        $this->paginate = [
            'sortWhitelist' => [
                'Actives.nome_ativo',
                'Actives.patrimonio',
                'Locations.nome_local',
                'Clients.razao_social',
                'Brands.nome_marca',
                'brandModels.nome_modelo',
            ],
        ];

        $userActives = TableRegistry::get('UserActives');
        $user_actives = $userActives->query();
        $Users = TableRegistry::get('Users');
        $users = $Users->query();
        $users2 = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $clientsQ = TableRegistry::get('Clients');
        $query2 = $clientsQ->query();
        $clients2 = TableRegistry::get('Clients')->find('list')->orderAsc('razao_social')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('nome_local')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $brands = TableRegistry::get('Brands')->find('list')->orderAsc('nome_marca')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $brandmodels = TableRegistry::get('brandModels')->find('list')->orderAsc('nome_modelo')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));

        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['nome_ativo', 'patrimonio', 'client_id', 'location_id','brand_id', 'brand_model_id'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        if($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id')){
            $query = $this->Actives->find('search', ['search' => $data])->contain([
                'Brands',
                'Clients',
                'Locations',
                'brandModels',
            ])->where('Actives.prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        } else {
            $query = $this->Actives->find('search', ['search' => $data])->contain([
                'Brands',
                'Clients',
                'Locations',
                'brandModels',
            ])->where('Actives.prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND responsaveis LIKE "%, '.$this->request->session()->read('Auth.User.id').' ,%"');
        }
        $actives = $this->paginate($query);
        $this->set(compact('user_actives','actives', 'clients', 'clients2', 'users2', 'locations', 'brands', 'brandmodels', 'users'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $active = $this->Actives->get($id);
        } else {
            $active = $this->Actives->newEntity();
        }

        if ($this->request->is(['patch', 'post', 'put'])) {

            
            $data = $this->request->getData();

            $id_user_add= array();
            $id_user_rem= array();
            $checagem = false;
            foreach($data as $key => $dadinho){

                
                if ($checagem ==true){
                    if($dadinho != 0 or $dadinho != '0'){
                        array_push($id_user_add, $key);
                    }else{
                        array_push($id_user_rem, $key);
                    }                
                }
                if ($key=="info_add"){
                    $checagem = true;
                }

            }
            $conn = ConnectionManager::get('default');
            $active = $this->Actives->patchEntity($active, $data);
            $active->prop_id = $this->request->session()->read('Auth.User.prop_id');
        
            //FUNÇÃO RESPONSÁVEL POR PRIMERIO CHECAR SE EXISTE UM REGISTRO IGUAL NA TABELA BRANDS E EM SEGUIDA CRIA UM NOVO REGISTRO DE MARCA

            if(!is_numeric($active->brand_id)){
                $brand_id = str_replace(' ', '%20', $active->nomebrand);
                $brand_model_id = str_replace(' ', '%20', $active->nomebrandmodel);
    
                $brandTables = TableRegistry::get('Brands')->find('list')->orderAsc('nome_marca')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND nome_marca LIKE "'.$active->nomebrand.'"');
                
                $contagem_brand=0;
                foreach ($brandTables as $id => $nome){
                    $contagem_brand=$contagem_brand+1;
                    $active->brand_id = $id;
                }
                if($contagem_brand == null){
                    $query_brands = 'apiBrandname='.$brand_id.'&apiBrandModelname='.$brand_model_id.'&apiProp_id='.$this->request->session()->read('Auth.User.prop_id');
                    $response = file_get_contents('http://projetovistoria.com.br/webservices/APIcadastrarBrands.php?'.$query_brands);
                    $brand_id_reg = str_replace(['{"brand_id":', '}'],'', $response);
                    $active->brand_id = $brand_id_reg;
                    $modelTables = TableRegistry::get('BrandModels')->find('list')->orderAsc('nome_modelo')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND nome_modelo LIKE "'.$active->nomebrandmodel.'"');
                    $contagem_model = 0;
                    foreach($modelTables as $id2 => $nome2){

                        $contagem_model = $contagem_model+1;
                        $active->brand_model_id = $id2;
                    }
                    if($contagem_model == null){
                        $query_brand_models = 'apiBrand_id='.$active->brand_id.'&apiProp_id='.$active->prop_id.'&apiBrandModelname='.$brand_model_id;
                        $response2 = file_get_contents('http://projetovistoria.com.br/webservices/APIcadastrarBrandModels.php?'.$query_brand_models);
                        
                        $brand_model_id_reg = str_replace(['{"brand_model_id":', '}'],'', $response2);
                        $active->brand_model_id = $brand_model_id_reg;
                    }
                    
                }
                
            }

            if(!is_numeric($active->brand_model_id)){
                $brand_model_id = str_replace(' ', '%20', $active->nomebrandmodel);
    
                $modelTables = TableRegistry::get('brandModels')->find('list')->orderAsc('nome_modelo')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND nome_modelo LIKE "'.$active->nomebrandmodel.'"');
                $contagem_model = 0;
                foreach($modelTables as $id3 => $nome3){
                    $contagem_model = $contagem_model+1;
                    $active->brand_model_id = $id3;
                }
                if($contagem_model == 0){
                    $query_brand_models = 'apiBrand_id='.$active->brand_id.'&apiProp_id='.$active->prop_id.'&apiBrandModelname='.$brand_model_id;
                    $response3 = file_get_contents('http://projetovistoria.com.br/webservices/APIcadastrarBrandModels.php?'.$query_brand_models);
                    $brand_model_id_reg2 = str_replace(['{"brand_model_id":', '}'],'', $response3);
                    $active->brand_model_id = $brand_model_id_reg2;
                }
            }
                $active->ativo='1';
            if ($this->Actives->save($active)) {
                $insert_string = " , ";
                
                $prop_id=$this->request->session()->read('Auth.User.prop_id');
                $active_id = $active->id;
                $insercoes = array();
                $remocoes = array();
                $userActives = TableRegistry::get('userActives');
                $query = $userActives->query();
                //adicionando registros no user_actives
                foreach($id_user_add as $adicao){
                    $query_search_add = $conn->execute('SELECT * FROM user_actives');
                    $result_sel_add = $query_search_add ->fetchAll('assoc');
                    $contagem = 0;
                    
                    foreach($result_sel_add as $checagem){
                        if($checagem['active_id']==$active_id AND $checagem['user_id'] == $adicao){
                            $contagem = $contagem+1;
                        }
                    }
                    
                    $insert_string = $insert_string." , ".$adicao." , ";
                    if ($contagem==0){
                        if(is_numeric($adicao)){
                        $insercao=[
                            'active_id' => $active_id,
                            'user_id' => $adicao
                        ];
                        array_push($insercoes, $insercao);
                        }
                    }
                    
                }

                $user_act_add = TableRegistry::getTableLocator()->get('UserActives');
                $entities = $user_act_add->newEntities($insercoes);
                $result = $user_act_add->saveMany($entities);
                
                //removendo registros no user_actives
                foreach($id_user_rem as $removendo){
                    $query_search_rem = $conn->execute('SELECT * FROM user_actives');
                    $result_sel_rem = $query_search_rem ->fetchAll('assoc');

                    foreach($result_sel_rem as $checagem){
                        if($checagem['active_id']==$active_id AND $checagem['user_id'] == $removendo){
                            $entity = $userActives->get($checagem['id']);
                            if($userActives->delete($entity)){
                            }
                        }
                    }
                    
                }
                /////// uploads //////
                $nomeArq = $this->request->getData()['arquivo']['name'];
                $arqTemp = $this->request->getData()['arquivo']['tmp_name'];
                $caminho= WWW_ROOT."files". DS ."props". DS .$this->request->session()->read('Auth.User.prop_id'). DS ."brands". DS .$active->brand_id. DS ."brandModel". DS .$active->brand_model_id;
                $destino= "files". DS ."props". DS .$this->request->session()->read('Auth.User.prop_id'). DS ."brands". DS .$active->brand_id. DS ."brandModel". DS .$active->brand_model_id. DS .$nomeArq;
    
                if(!file_exists($caminho)){
                   mkdir($caminho, 0777, true);
                }
                if(move_uploaded_file($arqTemp, WWW_ROOT.$destino)){
                }else{
                }

                ////// fim-uploads /////
                $this->Flash->success('Cadastro salvo com sucesso');
                $active->responsaveis = $insert_string;
                $this->Actives->save($active);
                $this->redirect(['action' => 'edit', $active->id]);
            } else {
                $this->Flash->error('Dados obrigatórios não preenchidos');
            }
        }
        
        $conn2 = ConnectionManager::get('default');
        $user_act = $conn2->execute('SELECT * FROM user_actives');
        $useract = $user_act ->fetchAll('assoc');

        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $selbrands = TableRegistry::get('Brands')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $selbrandmodels = TableRegistry::get('BrandModels')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $this->set(compact('active', 'clients', 'useract', 'locations', 'selbrands', 'selbrandmodels', 'users'));
    }
    
    public function download($id){
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['nome_ativo', 'patrimonio', 'nome_local', 'razao_social', 'nome_marca', 'nome_modelo'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Actives->find('search', ['search' => $data])->contain([]);
        $clients = $this->paginate($query);
        $this->set(compact('active'));

    }

    public function downloadFile ($id){
        $file = $this->Files->get($id);
        $path = WWW_ROOT.$file->path;
        $this->response->body(function() use($path){
            return file_get_contents($path);
        });
        return $this->response->whithDownload($file->name);
    }



    public function view($id) {
        if ($id != 'new') {
            $active = $this->Actives->get($id);
        } else {
            $active = $this->Actives->newEntity();
        }
        
        $conn2 = ConnectionManager::get('default');
        $user_act = $conn2->execute('SELECT * FROM user_actives');
        $useract = $user_act ->fetchAll('assoc');

        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $selbrands = TableRegistry::get('Brands')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $selbrandmodels = TableRegistry::get('BrandModels')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $this->set(compact('active', 'clients', 'useract', 'locations', 'selbrands', 'selbrandmodels', 'users'));
    }


    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if(strpos($id, 'i') == false){
            $active = $this->Actives->get($id, ['contain' => []]);
            if ($this->Actives->delete($active)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        }else{
            $id = str_replace('i','', $id);
            $this->request->allowMethod(['post', 'deactivate']);
            $activesTable = TableRegistry::getTableLocator()->get('Actives');
            $active = $activesTable->get($id);
            $active->ativo='0';
            $activesTable->save($active);
        }
        return $this->redirect($this->referer());
    }

}
