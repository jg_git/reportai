<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class LocationsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {
        $userLocations = TableRegistry::get('userLocations');
        $user_locations = $userLocations->query();
        $Users = TableRegistry::get('Users');
        $users = $Users->query();
        $users2 = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $clientsQ = TableRegistry::get('Clients');
        $query2 = $clientsQ->query();
        $clients = $this->paginate($query2);
        $clients2 = TableRegistry::get('Clients')->find('list')->orderAsc('razao_social')->where('Clients.prop_id = '.$this->request->session()->read('Auth.User.prop_id'));

        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['nome_local', 'logradouro', 'numero', 'cidade', 'estado'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }

        if($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id')){
            $query = $this->Locations->find('search', ['search' => $data])->contain(['Actives', 'Polls'])->where('Locations.prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        }else{
            $query = $this->Locations->find('search', ['search' => $data])->contain(['Actives', 'Polls'])->where('Locations.prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND responsaveis LIKE "%, '.$this->request->session()->read('Auth.User.id').' ,%"');
        }
        $locations = $this->paginate($query);
        $this->set(compact('user_locations', 'users', 'clients', 'clients2', 'users2', 'locations'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $location = $this->Locations->get($id);
        } else {
            $location = $this->Locations->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            $id_user_add= array();
            $id_user_rem= array();
            $checagem = false;
            foreach($data as $key => $dadinho){
                if ($checagem ==true){
                    if($dadinho != 0 or $dadinho != '0'){
                        array_push($id_user_add, $key);
                    }else{
                        array_push($id_user_rem, $key);
                    }         
                }
                if ($key=="nome_contato"){
                    $checagem = true;
                }
            }

            $conn = ConnectionManager::get('default');
            $location = $this->Locations->patchEntity($location, $data);
            $location->prop_id = $this->request->session()->read('Auth.User.prop_id');
            $location->ativo = 1;
            $location->endereco_full = $location->logradouro." ".$location->cidade." ".$location->estado;
            if ($this->Locations->save($location)) {

                
                $prop_id=$this->request->session()->read('Auth.User.prop_id');
                $location_id = $location->id;
                $insercoes = array();
                $remocoes = array();
                $userLocations = TableRegistry::get('UserLocations');
                $query = $userLocations->query();
                $insert_string = " , ";
                //adicionando registros no user_locations
                foreach($id_user_add as $adicao){
                    $query_search_add = $conn->execute('SELECT * FROM user_locations');
                    $result_sel_add = $query_search_add ->fetchAll('assoc');
                    $contagem = 0;
                    
                    foreach($result_sel_add as $checagem){
                        if($checagem['location_id']==$location_id AND $checagem['user_id'] == $adicao){
                            $contagem = $contagem+1;
                        }
                    }

                    $insert_string = $insert_string." , ".$adicao." , ";
                    if ($contagem==0){
                        if(is_numeric($adicao)){
                            $insercao=[
                                'location_id' => $location_id,
                                'user_id' => $adicao,
                                'prop_id' => $prop_id
                            ];
                            array_push($insercoes, $insercao);
                        }
                    }
                }

                $user_loc_add = TableRegistry::getTableLocator()->get('UserLocations');
                $entities = $user_loc_add->newEntities($insercoes);
                $result = $user_loc_add->saveMany($entities);
                
                //removendo registros no user_locations
                foreach($id_user_rem as $removendo){
                    $query_search_rem = $conn->execute('SELECT * FROM user_locations');
                    $result_sel_rem = $query_search_rem ->fetchAll('assoc');

                    foreach($result_sel_rem as $checagem){
                        if($checagem['location_id']==$location_id AND $checagem['user_id'] == $removendo){
                            $entity = $userLocations->get($checagem['id']);
                            if($userLocations->delete($entity)){
                            }
                        }
                    }
                    
                }

                $this->Flash->success('Cadastro salvo com sucesso');
                $location->responsaveis = $insert_string;
                $this->Locations->save($location);
                $this->redirect(['action' => 'edit', $location->id]);
            } else {
                $this->Flash->error('Dados obrigatórios não preenchidos');
            }
        }

        $conn2 = ConnectionManager::get('default');
        $user_loc = $conn2->execute('SELECT * FROM user_locations');
        $userloc = $user_loc ->fetchAll('assoc');

        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $this->set(compact('location', 'clients', 'userloc', 'users'));
    }

    public function view($id) {
        if ($id != 'new') {
            $location = $this->Locations->get($id);
        } else {
            $location = $this->Locations->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            $id_user_add= array();
            $id_user_rem= array();
            $checagem = false;
            foreach($data as $key => $dadinho){

                if ($key=="nome_contato"){
                    $checagem = true;
                }

                if ($checagem ==true){
                    if($dadinho != 0 or $dadinho != '0'){
                        array_push($id_user_add, $key);
                    }else{
                        array_push($id_user_rem, $key);
                    }                
                }
            }

            $conn = ConnectionManager::get('default');
            $location = $this->Locations->patchEntity($location, $data);
            $location->prop_id = $this->request->session()->read('Auth.User.prop_id');
        }

        $conn2 = ConnectionManager::get('default');
        $user_loc = $conn2->execute('SELECT * FROM user_locations');
        $userloc = $user_loc ->fetchAll('assoc');

        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $this->set(compact('location', 'clients', 'userloc', 'users'));
    }

    
    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if(strpos($id, 'i') == false){
            $location = $this->Locations->get($id, ['contain' => []]);
            if ($this->Locations->delete($location)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        }else{
            $id = str_replace('i','', $id);
            $this->request->allowMethod(['post', 'deactivate']);
            $locationsTable = TableRegistry::getTableLocator()->get('Locations');
            $location = $locationsTable->get($id);
            $location->ativo='0';
            $locationsTable->save($location);
        }
        return $this->redirect($this->referer());
    }

}
