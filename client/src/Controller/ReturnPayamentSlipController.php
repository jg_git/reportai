<?php

namespace CnabPHP\samples;

// include("..\..\autoloader.php");

use \CnabPHP\Retorno;

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Client\FormData;
use Cake\I18n\Date;





class ReturnPayamentSlipController extends AppController
{

    // public function isAuthorized($user)
    // {
    //     return true;
    // }
    //  $response = $http->post('https://oauth.itau.com.br/identify/connect/token', [
    //     'grant_type' => 'client_credentials',
    //     'client_id' => '-vpAwJj2xv4e0',
    //     'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'

    // ]);
    public function index()
    {
        $this->loadModel('ReturnPayamentSlips');

        $uploadData = '';
        if ($this->request->is('post')) {

            // if (!empty($this->request->data['file']['name'])) {
            $fileContent = file_get_contents($this->request->data['file']);


            $arquivo = new \CnabPHP\Retorno($fileContent);


            $registros = $arquivo->getRegistros();
            foreach ($registros as $registro) {
                if ($registro->R3U->codigo_movimento == 6) {
                    $nossoNumero = $registro->nosso_numero;
                    $dataPagamento = $registro->R3U->data_ocorrencia;
                }
            }

            // $fileName = $this->request->data['file']['name'];
            // $uploadPath = 'uploads/files';
            // $uploadFile = $uploadPath . $fileName;


            // }
            var_dump($registros);
            die();
        }
        //   'body' => ['grant_type' => 'client_credentials', 'client_id' => 'vpAwJj2xv4e0', 'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'],

        // dump($response);


        // $session = $this->request->getSession();
        // if (strpos($this->request->env('HTTP_REFERER'), 'receivables') === false) {
        //     $session->delete('cartr');
        // }
        // $cart = explode(',', $session->read('cartr'));
        // $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        // $total = 0;
        // foreach ($cartdb as $item) {
        //     $total += $item->valor;
        // }
        // $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        // $fields = [];
        // foreach ($fields as $field) {
        //     if (array_key_exists($field, $data)) {
        //         $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
        //     }
        // }
        // if (array_key_exists('pendentes', $data)) {
        //     $data['baixa'] = 0;
        // }
        // $query = $this->Receivables->find('search', ['search' => $data])->contain([
        //     'SaleDetails',
        //     'SaleDetails.Sales',
        //     'SaleDetails.Sales.Purchases',
        //     'SaleDetails.Sales.Purchases.Brokers',
        //     'SaleDetails.Sales.Purchases.Plants',
        //     'SaleDetails.Presales',
        //     'SaleDetails.Presales.Clients',
        //     'SaleDetails.Presales.Clients.Sellers'
        // ])->orderAsc('Receivables.data_vencimento');
        // $receivables = $this->paginate($query);
        //->where(['nota_fiscal !=' => '']),
        $returnpayamentSlips = TableRegistry::get('PayamentSlips')
            ->find('all');


        // debug($receivables);
        //    ])->matching('SaleDetails', function ($q) {
        //     return $q->where(['SaleDetails.nota_fiscal !=' => '']);
        // })->orderAsc('Receivables.data_vencimento');



        // $accounts = $this->getAccounts();
        $returnpayamentSlips = $this->paginate($returnpayamentSlips);


        // $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('returnpayamentSlips'));

        // $http = new Client([
        //     'headers' => ['Authorization' => 'Bearer ' . $accessToken]
        // ]);
        // $response = $http->get('https://example.com/api/profile/1');
        //

    }
    public function gerarPdf()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $this->loadModel('ReturnPayamentSlip');
        $id = $this->request->params['pass'][0];
        // $payamentslips = $this->PayamentSlip->find($id);
        $payamentSlips = TableRegistry::get('PayamentSlip')->find('all')->where(['id' => $id]);

        // $this->viewBuilder()->setClassName('CakePdf.Pdf');
        // $this->viewBuilder()->options([
        //     'pdfConfig' => [
        //         'orientation' => 'portrait',
        //         'filename' => 'certificate_'
        //     ],

        // ]);
        $this->set('payamentSlips', $payamentSlips);
    }

    public function gerarPdfSelecionados()
    {
        $data = $this->request->getData('checked');
        // $checked = $this->request->getData('inpute.checked');
        if ($data) {
            $this->Flash->Error('Por favor selecione ao menos um boleto ');
            return $this->redirect(
                array('controller' => 'ShowPayamentSlip', 'action' =>
                'index')
            );
        } else {
            $this->Flash->Error('Não existe ');
            // dump($data);
        }
    }
    // public function settle()
    // {
    //     $data = $this->request->getData();
    //     $session = $this->request->getSession();
    //     $cart = explode(',', $session->read('cartr'));
    //     foreach ($cart as $receivable_id) {
    //         if ($receivable_id != '' and !is_null($receivable_id)) {
    //             $receivable = TableRegistry::get('Receivables')->get($receivable_id);

    //             $entry = TableRegistry::get('LedgerEntries')->newEntity();
    //             $entry->account_id = $data['account_id'];
    //             $entry->data_lancamento = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
    //             $entry->payment_method_id = $data['payment_method_id'];
    //             switch ($receivable->tipo) {
    //                 case 'VE':
    //                     $entry->ledger_entry_type_id = 21;
    //                     $entry->receivable_id = $receivable->id;
    //                     break;
    //             }
    //             $entry->valor = $receivable->valor;
    //             TableRegistry::get('LedgerEntries')->save($entry);

    //             $receivable->data_baixa = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
    //             $receivable->baixa = 1;
    //             $this->Receivables->save($receivable);
    //         }
    //     }
    //     $session->delete('cartr');
    //     $json['status'] = 'OK';
    //     $this->set(['json' => $json, '_serialize' => 'json']);
    //     $this->RequestHandler->renderAs($this, 'json');
    // }


    public function gerarPdfhtml()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $this->loadModel('PayamentSlip');
        $id = $this->request->params['pass'][0];
        // $payamentslips = $this->PayamentSlip->find($id);
        $payamentSlips = TableRegistry::get('PayamentSlip')->find('all')->where(['id' => $id]);

        // $this->viewBuilder()->setClassName('CakePdf.Pdf');
        // $this->viewBuilder()->options([
        //     'pdfConfig' => [
        //         'orientation' => 'portrait',
        //         'filename' => 'certificate_'
        //     ],

        // ]);
        $this->set('payamentSlips', $payamentSlips);
    }



    public function test()
    { }
}
