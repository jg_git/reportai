<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class JobsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {

        $this->paginate = [
            'sortWhitelist' => [
                'Jobs.nome_servico',
                'Jobs.inicio',
                'Locations.nome_local',
                'Clients.razao_social',
                'Polls.nome_fomulario',
                'Jobs.responsaveis',
                'Jobs.status',
                'Jobs.final',

            ],
        ];

        $userJobs = TableRegistry::get('UserJobs');
        $user_jobs = $userJobs->query();
        $Users = TableRegistry::get('Users');
        $users = $Users->query();
        $users2 = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $clientsQ = TableRegistry::get('Clients');
        $query2 = $clientsQ->query();
        $clients2 = TableRegistry::get('Clients')->find('list')->orderAsc('razao_social')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('nome_local')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $polls = TableRegistry::get('Polls')->find('list')->orderAsc('nome_formulario')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
       
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['nome_servico', 'inicio', 'client_id', 'location_id', 'formulario_id', 'status', 'final', 'responsaveis'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        if($this->request->session()->read('Auth.User.prop_id') == $this->request->session()->read('Auth.User.client_id')){
            $query = $this->Jobs->find('search', ['search' => $data])->contain([
                'Polls',
                'Clients',
                'Locations',
            ])->where('Jobs.prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        } else {
            $query = $this->Jobs->find('search', ['search' => $data])->contain([
                'Polls',
                'Clients',
                'Locations',
            ])->where('Jobs.prop_id = '.$this->request->session()->read('Auth.User.prop_id').' AND responsaveis LIKE "%, '.$this->request->session()->read('Auth.User.id').' ,%"');
        }
        $jobs = $this->paginate($query);
        $this->set(compact('user_jobs','jobs', 'clients', 'clients2', 'users2', 'locations', 'polls', 'users'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $job = $this->Jobs->get($id);
        } else {
            $job = $this->Jobs->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('id', $data)) {
                $data['id'] = '';
            }
            if($id!='new'){
                $data['id']=$id;
            }

            $id_user_add= array();
            $id_user_rem= array();

            $checagem = false;
            foreach($data as $key => $dadinho){
                //array_push($id_user_add, $key);
                if ($checagem == true){
                    if(($dadinho != 0 or $dadinho != '0') AND ($dadinho !== 'id')){
                       array_push($id_user_add, $key);
                    }else{
                        array_push($id_user_rem, $key);
                    }

                }
                if($key == "ativo"){
                    $checagem = true;
                }
            }

            $job = $this->Jobs->patchEntity($job, $data);
            $job->prop_id = $this->request->session()->read('Auth.User.prop_id');
            $job->ativo = 1;
            $conn = ConnectionManager::get('default');
            if ($this->Jobs->save($job)) {

                $prop_id=$this->request->session()->read('Auth.User.prop_id');
                $job_id = $job->id;
                $userJobs = TableRegistry::get('UserJobs');
                $query = $userJobs->query();
                $insercoes = array();
                $remocoes = array();

                $insert_string = " , ";
                //adicionando registros no user_jobs
                foreach($id_user_add as $adicao){
                    if($adicao != "id"){
                        $query_search_add = $conn->execute('SELECT * FROM user_jobs');
                        $result_sel_add = $query_search_add ->fetchAll('assoc');
                        $contagem = 0;
                        foreach($result_sel_add as $checagem){
                            if($checagem['job_id']==$job_id AND $checagem['user_id'] == $adicao){
                                $contagem = $contagem+1;
                            }
                        }
                        $insert_string = $insert_string." , ".$adicao." , ";
                        if ($contagem==0){
                            if(is_numeric($adicao)){
                                $insercao=[
                                    'user_id' => $adicao,
                                    'job_id' => $job_id
                                ];
                                array_push($insercoes, $insercao);
                            }
                        }
                    }else{

                    }
                }

                $user_job_add = TableRegistry::getTableLocator()->get('userJobs');
                $entities = $user_job_add->newEntities($insercoes);
                $result = $user_job_add->saveMany($entities);
                
                //removendo registros no user_jobs
                foreach($id_user_rem as $removendo){
                    $query_search_rem = $conn->execute('SELECT * FROM user_jobs');
                    $result_sel_rem = $query_search_rem ->fetchAll('assoc');

                    foreach($result_sel_rem as $checagem){
                        if($checagem['job_id']==$job_id AND $checagem['user_id'] == $removendo){
                            $entity = $userJobs->get($checagem['id']);
                            if($userJobs->delete($entity)){
                            }
                        }
                    }
                }
                $job->responsaveis = $insert_string;
                $this->Jobs->save($job);
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $conn2 = ConnectionManager::get('default');
        $user_job = $conn2->execute('SELECT * FROM user_jobs');
        $userjob = $user_job ->fetchAll('assoc');
        $validacao = $this->request->session()->read('Auth.User.prop_id');
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->where('prop_id = '.$validacao);
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $polls = TableRegistry::get('Polls')->find('list')->orderAsc('id')->where('prop_id = '.$this->request->session()->read('Auth.User.prop_id'));
        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));

        $this->set(compact('job', 'userjob','users','user_jobs','jobs', 'clients','locations', 'polls')); 
                 
} 
    




    public function view($id) {
        if ($id != 'new') {
            $job = $this->Jobs->get($id);
        } else {
            $job = $this->Jobs->newEntity();
        }

        $conn2 = ConnectionManager::get('default');
        $user_job = $conn2->execute('SELECT * FROM user_jobs');
        $userjob = $user_job ->fetchAll('assoc');

        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $locations = TableRegistry::get('Locations')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $polls = TableRegistry::get('Polls')->find('list')->orderAsc('id')->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        $users = TableRegistry::get('Users')->find('list')->orderAsc('id')->contain(['UserClients'])->where('prop_id='.$this->request->session()->read('Auth.User.prop_id'));
        
        $this->set(compact('job','userjob','users','user_jobs','jobs', 'clients','locations', 'polls','useract'));
    }


    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        if(strpos($id, 'i') == false){
            $job = $this->Jobs->get($id, ['contain' => []]);
            if ($this->Jobs->delete($job)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        }else{
            $id = str_replace('i','', $id);
            $this->request->allowMethod(['post', 'deactivate']);
            $jobsTable = TableRegistry::getTableLocator()->get('Jobs');
            $job = $jobssTable->get($id);
            $job->ativo='0';
            $jobsTable->save($job);
        }
        return $this->redirect($this->referer());
    }

}
