<?php

namespace App\Controller;

use Cake\Http\Client;
use Cake\ORM\TableRegistry;

class AjaxController extends AppController {

    public function isAuthorized($user) {
        return true;
    }

    public function initialize() {
        parent::initialize();
    }

    public function cep($cep) {
        $http = new Client();
        $response = $http->get('https://viacep.com.br/ws/' . $cep . '/json/');
        $this->set('response', $response->body());
    }

    public function client($name = null) {
        $clients = TableRegistry::get('Clients')->find('all', [
                    'conditions' => ['Clients.nome_fantasia LIKE' => '%' . $name . '%'],
                    'limit' => 10,
                ])->distinct('Clients.nome_fantasia')->all();
        $json = [];
        foreach ($clients as $client) {
            $row['id'] = $client->id;
            $row['name'] = $client->nome_fantasia;
            $json[] = $row;
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function seller($name = null) {
        $sellers = TableRegistry::get('Sellers')->find('all', [
                    'conditions' => ['Sellers.razao_social LIKE' => '%' . $name . '%'],
                    'limit' => 10,
                ])->distinct('Sellers.razao_social')->all();
        $json = [];
        foreach ($sellers as $seller) {
            $row['id'] = $seller->id;
            $row['name'] = $seller->razao_social;
            $json[] = $row;
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function carrier($name = null) {
        $carriers = TableRegistry::get('Carriers')->find('all', [
                    'conditions' => ['Carriers.razao_social LIKE' => '%' . $name . '%'],
                    'limit' => 10,
                ])->distinct('Carriers.razao_social')->all();
        $json = [];
        foreach ($carriers as $carrier) {
            $row['id'] = $carrier->id;
            $row['name'] = $carrier->razao_social;
            $json[] = $row;
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function driver($name = null) {
        $drivers = TableRegistry::get('Drivers')->find('all', [
                    'conditions' => ['Drivers.nome LIKE' => '%' . $name . '%'],
                    'limit' => 10,
                ])->distinct('Drivers.nome')->all();
        $json = [];
        foreach ($drivers as $driver) {
            $row['id'] = $driver->id;
            $row['name'] = $driver->nome;
            $json[] = $row;
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function bank($name = null) {
        $banks = TableRegistry::get('Banks')->find('all', [
                    'conditions' => ['Banks.nome LIKE' => '%' . $name . '%'],
                    'limit' => 10,
                ])->distinct('Banks.nome')->all();
        $json = [];
        foreach ($banks as $bank) {
            $row['id'] = $bank->id;
            $row['code'] = $bank->codigo;
            $row['name'] = $bank->nome;
            $json[] = $row;
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function city($name) {
        $clients = TableRegistry::get('Clients')->find('all', [
                    'conditions' => ['Clients.cidade LIKE' => '%' . $name . '%'],
                    'limit' => 10,
                ])->distinct('Clients.cidade')->all();
        $json = [];
        foreach ($clients as $client) {
            $row['name'] = $client->cidade;
            $json[] = $row;
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

}
