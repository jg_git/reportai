<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - SERÁ MODIFICADA FUTURAMENTE
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use \Datetime;

class DealsController extends AppController
{
	public function initialize() {
		parent::initialize();
		$this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
        
    }

	public function index()
	{
		$data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        
        $query = $this->Deals->find('search', ['search' => $data])->contain(['Clients', 'DealPayments']);
        $deals = $this->paginate($query);
        $this->set(compact('deals'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $deal = $this->Deals->get($id, ['contain' => [
                    'Clients',
                    'DealPayments'
            ]]);
        } else {
            $deal = $this->Deals->newEntity();
        }
        $this->set(compact('deal'));
    }

    public function view($id) {
        if ($id != 'new') {
            $deal = $this->Deals->get($id, ['contain' => [
                    'Clients',
                    'DealPayments'
            ]]);
        } else {
            $deal = $this->Deals->newEntity();
        }
        $this->set(compact('deal'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $deal = $this->Deals->get($id, ['contain' => ['Clients']]);
        } else {
            $deal = $this->Deals->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('id', $data)) {
                $data['id'] = '';
            }
            $strvalor = $data['valor'];
            $alt1 = str_replace(".", "",$strvalor);
            $alt2 = str_replace(",", ".",$alt1);
            $data['valor']=$alt2;
            if($id!='new'){
                $data['id']=$id;
            }
            $deal = $this->Deals->patchEntity($deal, $data, ['validate' => 'part1']);

            $converte_d1="".$deal->data_vigencia_inicio;
            $converte_d2="".$deal->data_vigencia_termino;

            $converte_d1=substr($converte_d1,6,4).'-'.substr($converte_d1,3,2).'-'.substr($converte_d1,0,2);
            $converte_d2=substr($converte_d2,6,4).'-'.substr($converte_d2,3,2).'-'.substr($converte_d2,0,2);

            $bdate = ''.$converte_d1;
            $edate = ''.$converte_d2;

            $d1 = new DateTime($edate);
            $d2 = new DateTime($bdate);
            $intervalo = $d1->diff( $d2 );
            $data_soma = 0;
            $switcher = $d1->format('m')+0;
            switch ($switcher){
                case 1:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }
                    break;
                case 2:
                    if($intervalo->d >= 28){
                        $data_soma = 1;
                    }
                case 3:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }
                    break;
                case 4:
                    if($intervalo->d >= 29){
                        $data_soma = 1;
                    }    
                    break;
                case 5:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }    
                    break;
                case 6:
                    if($intervalo->d >= 29){
                        $data_soma = 1;
                    }    
                    break;
                case 7:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }    
                    break;
                case 8:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }    
                    break;
                case 9:
                    if($intervalo->d >= 29){
                        $data_soma = 1;
                    }    
                    break;
                case 10:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }    
                    break;
                case 11:
                    if($intervalo->d >= 29){
                        $data_soma = 1;
                    }    
                    break;
                case 12:
                    if($intervalo->d >= 30){
                        $data_soma = 1;
                    }    
                    break;       
            }
            $data_insercao = $intervalo->m + ($intervalo->y * 12);
            $data_insercao = $data_insercao + $data_soma;

            $deal->prazo = $data_insercao;
            if ($this->Deals->save($deal)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $clients = TableRegistry::get('Clients')->find('list')->orderAsc('id')->where('ativo = 1');
        $this->set(compact('deal', 'clients')); 
    }

    public function payment($deal_id, $id, $anchor) {
        $table = TableRegistry::get('DealPayments');
        if ($id != 'new') {
            $payment = $table->get($id);
        } else {
            $payment = $table->newEntity();
            $payment->deal_id = $deal_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $strpagamento = $data['valor_pagamento'];
            $alt1 = str_replace(".", "",$strpagamento);
            $alt2 = str_replace(",", ".",$alt1);
            $data['valor_pagamento']=$alt2;
            $payment = $table->patchEntity($payment, $data);
            if ($table->save($payment)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro'.implode(',', $data)."CHAVE = ". $key = array_search('', $data));
            }
        }
        $this->set(compact('payment', 'anchor'));
    }

    public function paymentDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-payment-list'] as $id) {
            $table = TableRegistry::get('DealPayments');
            $payment = $table->get($id);
            $table->delete($payment);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $deal = $this->Deals->get($id);
        if ($this->Deals->delete($deal)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

    public function deleteIndex($id = null){
        $this->request->allowMethod(['post', 'delete']);
        $deal = $this->Deals->get($id);
        $this->Deals->delete($deal);
    }
    
}