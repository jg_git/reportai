function anchorize() {
    return Math.random().toString(36).substr(2, 9);
}

function toast(message) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    toastr["success"](message);
}

function formatMoney(n, c, d, t) {
    var c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d === undefined ? "," : d,
            t = t === undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
            j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

function checkstatus(code) {
    if (code !== "success") {
        swal({
            title: "Erro de comunicação com o servidor",
            text: "Código de erro: " + code,
            confirmButtonColor: '#079dff',
            confirmButtonText: 'OK',
            confirmButtonClass: 'btn btn-lg btn-warning',
            buttonsStyling: false
        });
        return false;
    } else {
        return true;
    }
}

function convertDateYMD(input_date) {
    ds = input_date.split("/");
    d = new Date(ds[2], ds[1] - 1, ds[0]);
    data_convertida = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
    return data_convertida;
}

function convertDateDMY(input_date) {
    ds = input_date.split("-");
    d = new Date(ds[0], ds[1] - 1, ds[2]);
    data_convertida = ('0' + d.getDate()).slice(-2) + '/' + ('0' + (d.getMonth() + 1)).slice(-2) + '/' + d.getFullYear();
    return data_convertida;
}