<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ModuleActionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ModuleActionsTable Test Case
 */
class ModuleActionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ModuleActionsTable
     */
    public $ModuleActions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.module_actions',
        'app.modules',
        'app.roles',
        'app.users',
        'app.roles_modules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ModuleActions') ? [] : ['className' => ModuleActionsTable::class];
        $this->ModuleActions = TableRegistry::get('ModuleActions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ModuleActions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
