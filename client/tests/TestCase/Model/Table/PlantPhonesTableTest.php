<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlantPhonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlantPhonesTable Test Case
 */
class PlantPhonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlantPhonesTable
     */
    public $PlantPhones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.plant_phones',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PlantPhones') ? [] : ['className' => PlantPhonesTable::class];
        $this->PlantPhones = TableRegistry::get('PlantPhones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PlantPhones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
