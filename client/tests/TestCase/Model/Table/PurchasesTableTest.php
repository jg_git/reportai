<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PurchasesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PurchasesTable Test Case
 */
class PurchasesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PurchasesTable
     */
    public $Purchases;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.purchases',
        'app.plants',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.purchase_commisions',
        'app.purchase_payments'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Purchases') ? [] : ['className' => PurchasesTable::class];
        $this->Purchases = TableRegistry::get('Purchases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Purchases);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
