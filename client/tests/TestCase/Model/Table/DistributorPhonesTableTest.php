<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributorPhonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributorPhonesTable Test Case
 */
class DistributorPhonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributorPhonesTable
     */
    public $DistributorPhones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.distributor_phones',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DistributorPhones') ? [] : ['className' => DistributorPhonesTable::class];
        $this->DistributorPhones = TableRegistry::get('DistributorPhones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributorPhones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
