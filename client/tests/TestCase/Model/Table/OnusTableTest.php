<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OnusTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OnusTable Test Case
 */
class OnusTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OnusTable
     */
    public $Onus;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.onus',
        'app.emergency_cares',
        'app.leak_phrases',
        'app.emergency_cares_leak_phrases',
        'app.leak_phrases_onus',
        'app.emergency_cares_onus'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Onus') ? [] : ['className' => OnusTable::class];
        $this->Onus = TableRegistry::get('Onus', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Onus);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
