<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BrokerEmailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BrokerEmailsTable Test Case
 */
class BrokerEmailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BrokerEmailsTable
     */
    public $BrokerEmails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.broker_emails',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BrokerEmails') ? [] : ['className' => BrokerEmailsTable::class];
        $this->BrokerEmails = TableRegistry::get('BrokerEmails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BrokerEmails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
