<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributorEmailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributorEmailsTable Test Case
 */
class DistributorEmailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributorEmailsTable
     */
    public $DistributorEmails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.distributor_emails',
        'app.distributors',
        'app.plant_contacts',
        'app.plants',
        'app.plant_emails',
        'app.plant_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DistributorEmails') ? [] : ['className' => DistributorEmailsTable::class];
        $this->DistributorEmails = TableRegistry::get('DistributorEmails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributorEmails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
