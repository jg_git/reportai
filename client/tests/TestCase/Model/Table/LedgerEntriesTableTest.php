<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LedgerEntriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LedgerEntriesTable Test Case
 */
class LedgerEntriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LedgerEntriesTable
     */
    public $LedgerEntries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ledger_entries',
        'app.accounts',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.accounts_distributor_contacts',
        'app.ledger_entry_types',
        'app.payment_methods'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LedgerEntries') ? [] : ['className' => LedgerEntriesTable::class];
        $this->LedgerEntries = TableRegistry::get('LedgerEntries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LedgerEntries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
