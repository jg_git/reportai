<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PurchaseTransfersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PurchaseTransfersTable Test Case
 */
class PurchaseTransfersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PurchaseTransfersTable
     */
    public $PurchaseTransfers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.purchase_transfers',
        'app.purchases',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.plant_bank_accounts',
        'app.banks',
        'app.transit_plants',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.distributor_bank_accounts',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.broker_bank_accounts',
        'app.purchase_payments',
        'app.payables',
        'app.sales',
        'app.carriers',
        'app.carrier_contacts',
        'app.carrier_emails',
        'app.carrier_phones',
        'app.drivers',
        'app.carriers_drivers',
        'app.vehicles',
        'app.carriers_vehicles',
        'app.vehicle_orifices',
        'app.carts',
        'app.cart_orifices',
        'app.carrier_bank_accounts',
        'app.sale_details',
        'app.presales',
        'app.clients',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.seller_bank_accounts',
        'app.client_emails',
        'app.client_phones',
        'app.client_contacts',
        'app.client_logs',
        'app.users',
        'app.roles',
        'app.modules',
        'app.roles_modules',
        'app.module_actions',
        'app.receivables',
        'app.sales_purchases',
        'app.sales_purchases_conciliations',
        'app.transfer_to_purchases'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PurchaseTransfers') ? [] : ['className' => PurchaseTransfersTable::class];
        $this->PurchaseTransfers = TableRegistry::get('PurchaseTransfers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PurchaseTransfers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
