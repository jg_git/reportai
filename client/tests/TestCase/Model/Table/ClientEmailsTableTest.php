<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientEmailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientEmailsTable Test Case
 */
class ClientEmailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientEmailsTable
     */
    public $ClientEmails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.client_emails',
        'app.clients',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.client_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClientEmails') ? [] : ['className' => ClientEmailsTable::class];
        $this->ClientEmails = TableRegistry::get('ClientEmails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClientEmails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
