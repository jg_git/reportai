<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BrokerBankAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BrokerBankAccountsTable Test Case
 */
class BrokerBankAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BrokerBankAccountsTable
     */
    public $BrokerBankAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.broker_bank_accounts',
        'app.brokers',
        'app.banks',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.plant_bank_accounts',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.seller_bank_accounts',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BrokerBankAccounts') ? [] : ['className' => BrokerBankAccountsTable::class];
        $this->BrokerBankAccounts = TableRegistry::get('BrokerBankAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BrokerBankAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
