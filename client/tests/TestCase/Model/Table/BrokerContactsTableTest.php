<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BrokerContactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BrokerContactsTable Test Case
 */
class BrokerContactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BrokerContactsTable
     */
    public $BrokerContacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.broker_contacts',
        'app.brokers',
        'app.broker_emails',
        'app.broker_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BrokerContacts') ? [] : ['className' => BrokerContactsTable::class];
        $this->BrokerContacts = TableRegistry::get('BrokerContacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BrokerContacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
