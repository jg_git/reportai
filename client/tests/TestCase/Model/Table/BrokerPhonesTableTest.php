<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BrokerPhonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BrokerPhonesTable Test Case
 */
class BrokerPhonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BrokerPhonesTable
     */
    public $BrokerPhones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.broker_phones',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('BrokerPhones') ? [] : ['className' => BrokerPhonesTable::class];
        $this->BrokerPhones = TableRegistry::get('BrokerPhones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BrokerPhones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
