<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\InitialBalancesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\InitialBalancesTable Test Case
 */
class InitialBalancesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\InitialBalancesTable
     */
    public $InitialBalances;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.initial_balances',
        'app.accounts',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.accounts_distributor_contacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('InitialBalances') ? [] : ['className' => InitialBalancesTable::class];
        $this->InitialBalances = TableRegistry::get('InitialBalances', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->InitialBalances);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
