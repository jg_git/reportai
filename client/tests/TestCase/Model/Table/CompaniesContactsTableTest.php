<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CompaniesContactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CompaniesContactsTable Test Case
 */
class CompaniesContactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CompaniesContactsTable
     */
    public $CompaniesContacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.companies_contacts',
        'app.companies',
        'app.company_categories',
        'app.contacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CompaniesContacts') ? [] : ['className' => CompaniesContactsTable::class];
        $this->CompaniesContacts = TableRegistry::get('CompaniesContacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CompaniesContacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
