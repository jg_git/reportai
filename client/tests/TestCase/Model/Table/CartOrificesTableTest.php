<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CartOrificesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CartOrificesTable Test Case
 */
class CartOrificesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CartOrificesTable
     */
    public $CartOrifices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cart_orifices',
        'app.carts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CartOrifices') ? [] : ['className' => CartOrificesTable::class];
        $this->CartOrifices = TableRegistry::get('CartOrifices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CartOrifices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
