<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarrierBankAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarrierBankAccountsTable Test Case
 */
class CarrierBankAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CarrierBankAccountsTable
     */
    public $CarrierBankAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.carrier_bank_accounts',
        'app.carriers',
        'app.carrier_contacts',
        'app.carrier_emails',
        'app.carrier_phones',
        'app.drivers',
        'app.carriers_drivers',
        'app.vehicles',
        'app.vehicle_orifices',
        'app.carts',
        'app.cart_orifices',
        'app.carriers_vehicles',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.broker_bank_accounts',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.plant_bank_accounts',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.seller_bank_accounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CarrierBankAccounts') ? [] : ['className' => CarrierBankAccountsTable::class];
        $this->CarrierBankAccounts = TableRegistry::get('CarrierBankAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CarrierBankAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
