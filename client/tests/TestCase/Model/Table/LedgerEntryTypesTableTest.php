<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LedgerEntryTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LedgerEntryTypesTable Test Case
 */
class LedgerEntryTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\LedgerEntryTypesTable
     */
    public $LedgerEntryTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ledger_entry_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('LedgerEntryTypes') ? [] : ['className' => LedgerEntryTypesTable::class];
        $this->LedgerEntryTypes = TableRegistry::get('LedgerEntryTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->LedgerEntryTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
