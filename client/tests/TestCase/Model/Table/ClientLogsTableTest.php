<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientLogsTable Test Case
 */
class ClientLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientLogsTable
     */
    public $ClientLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.client_logs',
        'app.clients',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.client_emails',
        'app.client_phones',
        'app.client_contacts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClientLogs') ? [] : ['className' => ClientLogsTable::class];
        $this->ClientLogs = TableRegistry::get('ClientLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClientLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
