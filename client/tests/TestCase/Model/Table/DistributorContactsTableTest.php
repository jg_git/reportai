<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributorContactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributorContactsTable Test Case
 */
class DistributorContactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributorContactsTable
     */
    public $DistributorContacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.distributor_contacts',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DistributorContacts') ? [] : ['className' => DistributorContactsTable::class];
        $this->DistributorContacts = TableRegistry::get('DistributorContacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributorContacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
