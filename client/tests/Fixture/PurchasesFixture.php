<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PurchasesFixture
 *
 */
class PurchasesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'nro_contrato' => ['type' => 'string', 'length' => 20, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'data_compra' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'plant_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'distributor_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'corretada' => ['type' => 'tinyinteger', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'broker_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'volume_comprado' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'vendido' => ['type' => 'tinyinteger', 'length' => 4, 'unsigned' => false, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
        'volume_restante' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'preco_litro' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'forma_pagamento' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => 'V=A VISTA P=A PRAZO', 'precision' => null, 'fixed' => null],
        'comissao' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'FK_purchases_plants' => ['type' => 'index', 'columns' => ['plant_id'], 'length' => []],
            'FK_purchases_distributors' => ['type' => 'index', 'columns' => ['distributor_id'], 'length' => []],
            'FK_purchases_brokers' => ['type' => 'index', 'columns' => ['broker_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_purchases_brokers' => ['type' => 'foreign', 'columns' => ['broker_id'], 'references' => ['brokers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_purchases_distributors' => ['type' => 'foreign', 'columns' => ['distributor_id'], 'references' => ['distributors', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_purchases_plants' => ['type' => 'foreign', 'columns' => ['plant_id'], 'references' => ['plants', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'nro_contrato' => 'Lorem ipsum dolor ',
            'data_compra' => '2018-10-29',
            'plant_id' => 1,
            'distributor_id' => 1,
            'corretada' => 1,
            'broker_id' => 1,
            'volume_comprado' => 1,
            'vendido' => 1,
            'volume_restante' => 1,
            'preco_litro' => 1,
            'forma_pagamento' => 'Lorem ipsum dolor sit amet',
            'comissao' => 1,
            'created' => '2018-10-29 07:30:38',
            'modified' => '2018-10-29 07:30:38'
        ],
    ];
}
