<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlantContactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlantContactsTable Test Case
 */
class PlantContactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlantContactsTable
     */
    public $PlantContacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.plant_contacts',
        'app.plants',
        'app.plant_emails',
        'app.plant_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PlantContacts') ? [] : ['className' => PlantContactsTable::class];
        $this->PlantContacts = TableRegistry::get('PlantContacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PlantContacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
