<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PlantEmailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PlantEmailsTable Test Case
 */
class PlantEmailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PlantEmailsTable
     */
    public $PlantEmails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.plant_emails',
        'app.plants',
        'app.plant_contacts',
        'app.plant_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PlantEmails') ? [] : ['className' => PlantEmailsTable::class];
        $this->PlantEmails = TableRegistry::get('PlantEmails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PlantEmails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
