<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DistributorBankAccountsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DistributorBankAccountsTable Test Case
 */
class DistributorBankAccountsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DistributorBankAccountsTable
     */
    public $DistributorBankAccounts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.distributor_bank_accounts',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DistributorBankAccounts') ? [] : ['className' => DistributorBankAccountsTable::class];
        $this->DistributorBankAccounts = TableRegistry::get('DistributorBankAccounts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DistributorBankAccounts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
