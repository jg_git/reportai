<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OnuEntriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OnuEntriesTable Test Case
 */
class OnuEntriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OnuEntriesTable
     */
    public $OnuEntries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.onu_entries',
        'app.risk_classes',
        'app.pictograms',
        'app.risk_classes_pictograms',
        'app.pictogram_groups',
        'app.risk_numbers',
        'app.onus_onus_entries',
        'app.special_provisions',
        'app.onu_entries_special_provisions'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OnuEntries') ? [] : ['className' => OnuEntriesTable::class];
        $this->OnuEntries = TableRegistry::get('OnuEntries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OnuEntries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
