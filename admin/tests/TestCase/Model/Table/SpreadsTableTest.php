<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SpreadsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SpreadsTable Test Case
 */
class SpreadsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SpreadsTable
     */
    public $Spreads;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.spreads',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.distributor_bank_accounts',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.broker_bank_accounts',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.plant_bank_accounts',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.seller_bank_accounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Spreads') ? [] : ['className' => SpreadsTable::class];
        $this->Spreads = TableRegistry::get('Spreads', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Spreads);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
