<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ClientPhonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ClientPhonesTable Test Case
 */
class ClientPhonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ClientPhonesTable
     */
    public $ClientPhones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.client_phones',
        'app.clients',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.client_emails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ClientPhones') ? [] : ['className' => ClientPhonesTable::class];
        $this->ClientPhones = TableRegistry::get('ClientPhones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ClientPhones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
