<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarrierContactsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarrierContactsTable Test Case
 */
class CarrierContactsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CarrierContactsTable
     */
    public $CarrierContacts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.carrier_contacts',
        'app.carriers',
        'app.carrier_emails',
        'app.carrier_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CarrierContacts') ? [] : ['className' => CarrierContactsTable::class];
        $this->CarrierContacts = TableRegistry::get('CarrierContacts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CarrierContacts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
