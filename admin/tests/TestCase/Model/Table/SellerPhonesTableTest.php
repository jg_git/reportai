<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SellerPhonesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SellerPhonesTable Test Case
 */
class SellerPhonesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SellerPhonesTable
     */
    public $SellerPhones;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.seller_phones',
        'app.sellers',
        'app.seller_emails'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SellerPhones') ? [] : ['className' => SellerPhonesTable::class];
        $this->SellerPhones = TableRegistry::get('SellerPhones', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SellerPhones);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
