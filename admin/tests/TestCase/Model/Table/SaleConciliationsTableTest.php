<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaleConciliationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaleConciliationsTable Test Case
 */
class SaleConciliationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\SaleConciliationsTable
     */
    public $SaleConciliations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.sale_conciliations',
        'app.sales',
        'app.carriers',
        'app.carrier_contacts',
        'app.carrier_emails',
        'app.carrier_phones',
        'app.drivers',
        'app.carriers_drivers',
        'app.vehicles',
        'app.carriers_vehicles',
        'app.vehicle_orifices',
        'app.carts',
        'app.cart_orifices',
        'app.carrier_bank_accounts',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.broker_bank_accounts',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.plant_bank_accounts',
        'app.sellers',
        'app.seller_emails',
        'app.seller_phones',
        'app.seller_bank_accounts',
        'app.purchases',
        'app.distributors',
        'app.distributor_emails',
        'app.distributor_contacts',
        'app.distributor_phones',
        'app.distributor_bank_accounts',
        'app.purchase_payments',
        'app.sale_details',
        'app.presales',
        'app.clients',
        'app.client_emails',
        'app.client_phones',
        'app.client_contacts',
        'app.client_logs',
        'app.users',
        'app.roles',
        'app.modules',
        'app.roles_modules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('SaleConciliations') ? [] : ['className' => SaleConciliationsTable::class];
        $this->SaleConciliations = TableRegistry::get('SaleConciliations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SaleConciliations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
