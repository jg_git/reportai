<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CarrierEmailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CarrierEmailsTable Test Case
 */
class CarrierEmailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CarrierEmailsTable
     */
    public $CarrierEmails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.carrier_emails',
        'app.carriers',
        'app.carrier_contacts',
        'app.carrier_phones'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CarrierEmails') ? [] : ['className' => CarrierEmailsTable::class];
        $this->CarrierEmails = TableRegistry::get('CarrierEmails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CarrierEmails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
