<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ExpeditorsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ExpeditorsController Test Case
 */
class ExpeditorsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.expeditors',
        'app.companies',
        'app.company_categories',
        'app.contacts',
        'app.contact_areas',
        'app.companies_contacts',
        'app.companies_expeditors',
        'app.manufacturers',
        'app.companies_manufacturers'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
