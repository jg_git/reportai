<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PresalesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\PresalesController Test Case
 */
class PresalesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.presales',
        'app.clients',
        'app.sellers',
        'app.banks',
        'app.brokers',
        'app.broker_contacts',
        'app.broker_emails',
        'app.broker_phones',
        'app.broker_bank_accounts',
        'app.plants',
        'app.plant_contacts',
        'app.plant_emails',
        'app.plant_phones',
        'app.plant_bank_accounts',
        'app.seller_emails',
        'app.seller_phones',
        'app.seller_bank_accounts',
        'app.client_emails',
        'app.client_phones',
        'app.client_contacts',
        'app.client_logs',
        'app.users',
        'app.roles',
        'app.modules',
        'app.roles_modules'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
