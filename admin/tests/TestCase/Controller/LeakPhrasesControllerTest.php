<?php
namespace App\Test\TestCase\Controller;

use App\Controller\LeakPhrasesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\LeakPhrasesController Test Case
 */
class LeakPhrasesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.leak_phrases',
        'app.emergency_cares',
        'app.emergency_cares_leak_phrases',
        'app.onus',
        'app.emergency_cares_onus',
        'app.leak_phrases_onus'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
