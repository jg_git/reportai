<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PurchaseTransfersFixture
 *
 */
class PurchaseTransfersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'purchase_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'volume' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'frete' => ['type' => 'float', 'length' => null, 'precision' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        'carrier_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'plant_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'destination_purchase_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'FK_purchase_transfers_purchases' => ['type' => 'index', 'columns' => ['purchase_id'], 'length' => []],
            'FK_purchase_transfers_carriers' => ['type' => 'index', 'columns' => ['carrier_id'], 'length' => []],
            'FK_purchase_transfers_plants' => ['type' => 'index', 'columns' => ['plant_id'], 'length' => []],
            'FK_purchase_transfers_purchases_2' => ['type' => 'index', 'columns' => ['destination_purchase_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_purchase_transfers_carriers' => ['type' => 'foreign', 'columns' => ['carrier_id'], 'references' => ['carriers', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_purchase_transfers_plants' => ['type' => 'foreign', 'columns' => ['plant_id'], 'references' => ['plants', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_purchase_transfers_purchases' => ['type' => 'foreign', 'columns' => ['purchase_id'], 'references' => ['purchases', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
            'FK_purchase_transfers_purchases_2' => ['type' => 'foreign', 'columns' => ['destination_purchase_id'], 'references' => ['purchases', 'id'], 'update' => 'restrict', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'purchase_id' => 1,
            'volume' => 1,
            'frete' => 1,
            'carrier_id' => 1,
            'plant_id' => 1,
            'destination_purchase_id' => 1,
            'created' => '2019-03-16 13:41:28'
        ],
    ];
}
