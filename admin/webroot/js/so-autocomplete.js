function autocomplete(fieldname, options) {
    
    var currentFocus;
    
    field = document.getElementById(fieldname);

    field.addEventListener("input", function (e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) {
            return false;
        }
        currentFocus = -1;
        a = document.createElement("div");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        found = false;
        for (i = 0; i < options.length; i++) {
            if (options[i].name.substr(0, val.length).toUpperCase() === val.toUpperCase()) {
                b = document.createElement("div");
                b.innerHTML = "<strong>" + options[i].name.substr(0, val.length) + "</strong>";
                b.innerHTML += options[i].name.substr(val.length);
                b.innerHTML += "<input type='hidden' value='" + options[i].name + "' data-id='" + options[i].id + "'>";
                b.addEventListener("click", function (e) {
                    field.classList.remove("autocomplete-new");
                    field.classList.add("autocomplete-normal");
                    field.value = this.getElementsByTagName("input")[0].value;
                    field.setAttribute("data-id", this.getElementsByTagName("input")[0].dataset.id);
                    closeAllLists();
                });
                a.appendChild(b);
                found = true;
            }
        }
        if (!found) {
            b = document.createElement("div");
            b.innerHTML += "<i class='fa fa-plus-square'></i>  ";
            b.innerHTML += val;
            b.innerHTML += "  (Criar Novo)";
            b.addEventListener("click", function (e) {
                field.classList.remove("autocomplete-normal");
                field.classList.add("autocomplete-new");
                field.setAttribute("data-id", "new");
                closeAllLists();
            });
            a.appendChild(b);
        }
    });

    field.addEventListener("keydown", function (e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) {
            x = x.getElementsByTagName("div");
        }
        switch (e.keyCode) {
            case 40: // Down
                currentFocus++;
                addActive(x);
                break;
            case 38: // Up
                currentFocus--;
                addActive(x);
                break;
            case 13: // Enter
                e.preventDefault();
                if (currentFocus > -1) {
                    if (x) {
                        x[currentFocus].click();
                    }
                }
                break;
            case 27: // Escape
                closeAllLists();
                break;
        }
    });

    field.addEventListener("click", function (e) {
        var a, b, i;
        closeAllLists();
        a = document.createElement("div");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        for (i = 0; i < options.length && i < 10; i++) {
            b = document.createElement("div");
            b.innerHTML += options[i].name;
            b.innerHTML += "<input type='hidden' value='" + options[i].name + "' data-id='" + options[i].id + "'>";
            b.addEventListener("click", function (e) {
                field.classList.remove("autocomplete-new");
                field.classList.add("autocomplete-normal");
                field.value = this.getElementsByTagName("input")[0].value;
                field.setAttribute("data-id", this.getElementsByTagName("input")[0].dataset.id);
                closeAllLists();
            });
            a.appendChild(b);
        }
    });

    function addActive(x) {
        if (!x) {
            return false;
        }
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
        if (currentFocus >= x.length) {
            currentFocus = 0;
        }
        if (currentFocus < 0) {
            currentFocus = (x.length - 1);
        }
        x[currentFocus].classList.add("autocomplete-active");
    }

    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt !== x[i] && elmnt !== field) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}
