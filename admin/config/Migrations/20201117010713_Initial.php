<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {

        $this->table('accounts')
            ->addColumn('tipo', 'string', [
                'comment' => 'I=INTERNA F=FINANCEIRA',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('ativa', 'tinyinteger', [
                'default' => '1',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('distributor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('titular', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero_agencia', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('numero_conta', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('nome_gerente', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('telefone_gerente', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('email_gerente', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('carteira', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('client_id', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('client_secret', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('itau_chave', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->addIndex(
                [
                    'distributor_id',
                ]
            )
            ->create();

        $this->table('accounts_distributor_contacts', ['id' => false, 'primary_key' => ['account_id', 'distributor_contact_id']])
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('distributor_contact_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'account_id',
                ]
            )
            ->addIndex(
                [
                    'distributor_contact_id',
                ]
            )
            ->create();

        $this->table('banks')
            ->addColumn('codigo', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 300,
                'null' => false,
            ])
            ->create();

        $this->table('broker_bank_accounts')
            ->addColumn('broker_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero_agencia', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('numero_conta', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('tipo_conta', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->addIndex(
                [
                    'broker_id',
                ]
            )
            ->create();

        $this->table('broker_contacts')
            ->addColumn('broker_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'broker_id',
                ]
            )
            ->create();

        $this->table('broker_emails')
            ->addColumn('broker_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'broker_id',
                ]
            )
            ->create();

        $this->table('broker_phones')
            ->addColumn('broker_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'F=FIXO C=CELULAR',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('ddd', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'broker_id',
                ]
            )
            ->create();

        $this->table('brokers')
            ->addColumn('razao_social', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nome_fantasia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('inscricao_estadual', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('inscricao_municipal', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('agencia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('conta', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => true,
            ])
            ->addColumn('tipo_conta', 'string', [
                'comment' => 'C=CORRENTE P=POUPANÇA',
                'default' => null,
                'limit' => 1,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->create();

        $this->table('carrier_bank_accounts')
            ->addColumn('carrier_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero_agencia', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('numero_conta', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('tipo_conta', 'string', [
                'comment' => 'C=CORRENTE P=POUPANÇA',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->addIndex(
                [
                    'carrier_id',
                ]
            )
            ->create();

        $this->table('carrier_contacts')
            ->addColumn('carrier_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'carrier_id',
                ]
            )
            ->create();

        $this->table('carrier_emails')
            ->addColumn('carrier_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'carrier_id',
                ]
            )
            ->create();

        $this->table('carrier_phones')
            ->addColumn('carrier_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'F=FIXO C=CELULAR',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('ddd', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'carrier_id',
                ]
            )
            ->create();

        $this->table('carriers')
            ->addColumn('razao_social', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nome_fantasia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('inscricao_estadual', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('inscricao_municipal', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('antt', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('antt_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ibama_cert_regularidade_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ibama_autorizacao_ambiental_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('plano_atend_emergencial_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('frete', 'float', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('cart_orifices')
            ->addColumn('cart_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('quantidade_litros', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'cart_id',
                ]
            )
            ->create();

        $this->table('carts')
            ->addColumn('vehicle_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('marca_modelo', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('placa', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('ano', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('licenciamento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('quantidade_litros', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('quantidade_bocas', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('civ', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('cipp', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('calibragem', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('letpp', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('seguro_seguradora', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('seguro_corretora', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('seguro_apolice', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('seguro_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'vehicle_id',
                ]
            )
            ->create();

        $this->table('client_contacts')
            ->addColumn('client_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'D=DONO G=GERENTE',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular1', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular2', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular3', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('client_emails')
            ->addColumn('client_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'client_id',
                ]
            )
            ->create();

        $this->table('client_logs')
            ->addColumn('client_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('user_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('descricao', 'text', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('display', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('file', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'client_id',
                ]
            )
            ->addIndex(
                [
                    'user_id',
                ]
            )
            ->create();

        $this->table('client_phones')
            ->addColumn('client_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'F=FIXO C=CELULAR',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('ddd', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'client_id',
                ]
            )
            ->create();

        $this->table('clients')
            ->addColumn('razao_social', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nome_fantasia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('inscricao_estadual', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('inscricao_municipal', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('status_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('email_adm', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => false,
            ])
            ->addColumn('url_dashboard', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('latitude', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => true,
            ])
            ->addColumn('longitude', 'string', [
                'default' => null,
                'limit' => 256,
                'null' => true,
            ])
            ->addIndex(
                [
                    'status_id',
                ]
            )
            ->create();

        $this->table('distributor_bank_accounts')
            ->addColumn('distributor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero_agencia', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('numero_conta', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('tipo_conta', 'string', [
                'comment' => 'C=CORRENTE P=POUPANÇA',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->addIndex(
                [
                    'distributor_id',
                ]
            )
            ->create();

        $this->table('distributor_contacts')
            ->addColumn('distributor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('funcionalidade', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'distributor_id',
                ]
            )
            ->create();

        $this->table('distributor_emails')
            ->addColumn('distributor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'distributor_id',
                ]
            )
            ->create();

        $this->table('distributor_phones')
            ->addColumn('distributor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'F=FIXO C=CELULAR',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('ddd', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'distributor_id',
                ]
            )
            ->create();

        $this->table('distributors')
            ->addColumn('razao_social', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nome_fantasia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('inscricao_estadual', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('inscricao_municipal', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('spread', 'float', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('drivers')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cnh', 'string', [
                'default' => null,
                'limit' => 30,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('rg', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('data_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('mopp_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('nr20_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('nr35_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('cadastro_perfil_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('celular1', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('celular2', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('celular3', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('initial_balances')
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('dc', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_saldo', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('observacao', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'account_id',
                ]
            )
            ->create();

        $this->table('ledger_entries')
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('ledger_entry_type_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('data_lancamento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 500,
                'null' => false,
            ])
            ->addColumn('numero_documento', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('payable_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('receivable_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('payment_method_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('observacao', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'account_id',
                ]
            )
            ->addIndex(
                [
                    'ledger_entry_type_id',
                ]
            )
            ->addIndex(
                [
                    'payable_id',
                ]
            )
            ->addIndex(
                [
                    'payment_method_id',
                ]
            )
            ->addIndex(
                [
                    'receivable_id',
                ]
            )
            ->create();

        $this->table('ledger_entry_types')
            ->addColumn('editable', 'tinyinteger', [
                'default' => '1',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('dc', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('menus')
            ->addColumn('sequence', 'tinyinteger', [
                'default' => null,
                'limit' => 4,
                'null' => true,
            ])
            ->addColumn('parent', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->create();

        $this->table('module_actions')
            ->addColumn('module_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('action', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addIndex(
                [
                    'module_id',
                ]
            )
            ->create();

        $this->table('modules')
            ->addColumn('sequence', 'tinyinteger', [
                'default' => null,
                'limit' => 4,
                'null' => true,
            ])
            ->addColumn('menu_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('menu_display', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('menu_link', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('parameters', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->create();

        $this->table('payables')
            ->addColumn('tipo', 'string', [
                'comment' => 'CC=COMISSAO CORRETORA CT= COM CORRETORA TRANSFERENCIA SP=SPREAD FR=FRETE FT=FRETE TRANSFERENCIA CV=COMISSAO VENDEDOR PC=PAGAMENTO COMPRA',
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('data_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_baixa', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('baixa', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('sale_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('sale_detail_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('purchase_payment_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('sales_purchases_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('purchase_transfer_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'purchase_payment_id',
                ]
            )
            ->addIndex(
                [
                    'purchase_transfer_id',
                ]
            )
            ->addIndex(
                [
                    'sale_detail_id',
                ]
            )
            ->addIndex(
                [
                    'sale_id',
                ]
            )
            ->addIndex(
                [
                    'sales_purchases_id',
                ]
            )
            ->create();

        $this->table('payament_slip_batches')
            ->addColumn('is_downloaded', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('num_requested', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('num_printed', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('num_registration_errors', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('num_print_errors', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('pdf_filename', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('date_requested', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('date_printed', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('payament_slips')
            ->addColumn('payament_slip_batch_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('receivable_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('nota_fiscal', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('is_registered', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('is_printed', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('is_registration_error', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('is_print_error', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('is_canceled', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('pago', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('nosso_numero', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('digito_verificador_nosso_numero', 'string', [
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('data_emissao', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('valor_cobrado', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('cpf_cnpj_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('nome_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('logradouro_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('bairro_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('cidade_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('cep_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('uf_sacador_avalista', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('cpf_cnpj_pagador', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('nome_pagador', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('logradouro_pagador', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('bairro_pagador', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('cidade_pagador', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('uf_pagador', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('cep_pagador', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('codigo_barras', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('numero_linha_digitavel', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('status_code', 'smallinteger', [
                'default' => null,
                'limit' => 6,
                'null' => true,
            ])
            ->addColumn('error_code', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('error_message', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('request', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('response', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'account_id',
                ]
            )
            ->addIndex(
                [
                    'payament_slip_batch_id',
                ]
            )
            ->addIndex(
                [
                    'receivable_id',
                ]
            )
            ->create();

        $this->table('payment_methods')
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('plant_bank_accounts')
            ->addColumn('plant_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero_agencia', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('numero_conta', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('tipo_conta', 'string', [
                'comment' => 'C=CORRENTE P=POUPANÇA',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->addIndex(
                [
                    'plant_id',
                ]
            )
            ->create();

        $this->table('plant_contacts')
            ->addColumn('plant_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('celular', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'plant_id',
                ]
            )
            ->create();

        $this->table('plant_emails')
            ->addColumn('plant_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'plant_id',
                ]
            )
            ->create();

        $this->table('plant_phones')
            ->addColumn('plant_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'F=FIXO C=CELULAR',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('ddd', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'plant_id',
                ]
            )
            ->create();

        $this->table('plants')
            ->addColumn('razao_social', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nome_fantasia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('cnpj', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('inscricao_estadual', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('inscricao_municipal', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => true,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('presales')
            ->addColumn('data_pedido', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('client_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_pedido', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_vendido', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('preco_venda', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('preco_mercado', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_pagamento_1', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_pagamento_2', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_pagamento_3', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_pagamento_4', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('retira', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('cancelado', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('parent_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('price_settings', ['id' => false, 'primary_key' => ['tag']])
            ->addColumn('tag', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('dias_de', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('dias_ate', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('prices')
            ->addColumn('preco', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('purchase_devolutions')
            ->addColumn('purchase_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'purchase_id',
                ]
            )
            ->create();

        $this->table('purchase_payments')
            ->addColumn('purchase_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_provisionado', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('valor_provisionado', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('data_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'purchase_id',
                ]
            )
            ->create();

        $this->table('purchase_transfers')
            ->addColumn('purchase_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('wrapping', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('transito', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('volume', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('frete', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('carrier_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('plant_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('destination_purchase_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('preco_litro_adicional', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'carrier_id',
                ]
            )
            ->addIndex(
                [
                    'destination_purchase_id',
                ]
            )
            ->addIndex(
                [
                    'plant_id',
                ]
            )
            ->addIndex(
                [
                    'purchase_id',
                ]
            )
            ->create();

        $this->table('purchases')
            ->addColumn('numero_contrato', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('data_compra', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('plant_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('distributor_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('corretada', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('broker_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('volume_comprado', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_transferido', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_devolvido', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_transito', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_vendido', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('preco_litro', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('preco_litro_adicional', 'float', [
                'default' => '0',
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'broker_id',
                ]
            )
            ->addIndex(
                [
                    'distributor_id',
                ]
            )
            ->addIndex(
                [
                    'plant_id',
                ]
            )
            ->create();

        $this->table('receivables')
            ->addColumn('tipo', 'string', [
                'comment' => 'VE=VENDA',
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('data_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('data_baixa', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('baixa', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('sale_detail_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('boleto_gerado', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => true,
            ])
            ->addIndex(
                [
                    'sale_detail_id',
                ]
            )
            ->create();

        $this->table('roles')
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('super_user', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->create();

        $this->table('sale_details')
            ->addColumn('sale_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('presale_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nota_fiscal', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('volume', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('account_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('comissao', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('sales')
            ->addColumn('data_venda', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('carrier_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('driver_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('vehicle_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('spread', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('frete', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('volume_total', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_conciliado', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('conciliado', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('data_conciliado', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'carrier_id',
                ]
            )
            ->addIndex(
                [
                    'driver_id',
                ]
            )
            ->addIndex(
                [
                    'vehicle_id',
                ]
            )
            ->create();

        $this->table('sales_purchases')
            ->addColumn('sale_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('purchase_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('volume_conciliado', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('spread', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'purchase_id',
                ]
            )
            ->addIndex(
                [
                    'sale_id',
                ]
            )
            ->create();

        $this->table('sales_purchases_conciliations')
            ->addColumn('sales_purchases_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('nota_fiscal', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => true,
            ])
            ->addColumn('volume_conciliado', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'sales_purchases_id',
                ]
            )
            ->create();

        $this->table('seller_bank_accounts')
            ->addColumn('seller_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('bank_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero_agencia', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('numero_conta', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('tipo_conta', 'string', [
                'comment' => 'C=CORRENTE P=POUPANÇA',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'bank_id',
                ]
            )
            ->addIndex(
                [
                    'seller_id',
                ]
            )
            ->create();

        $this->table('seller_emails')
            ->addColumn('seller_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'seller_id',
                ]
            )
            ->create();

        $this->table('seller_phones')
            ->addColumn('seller_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('ddd', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('telefone', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('tipo', 'string', [
                'comment' => 'F=FIXO C=CELULAR',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'seller_id',
                ]
            )
            ->create();

        $this->table('sellers')
            ->addColumn('tipo', 'string', [
                'comment' => 'J=JURIDICA F=FISICA',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('razao_social', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('nome_fantasia', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => true,
            ])
            ->addColumn('cnpj_cpf', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('inscricao_estadual', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('inscricao_municipal', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('cep', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('logradouro', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('numero', 'string', [
                'default' => null,
                'limit' => 15,
                'null' => false,
            ])
            ->addColumn('complemento', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('bairro', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('cidade', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('estado', 'string', [
                'default' => null,
                'limit' => 2,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('transfers')
            ->addColumn('data_transferencia', 'date', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('conta_origem_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('conta_destino_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('descricao', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('payment_method_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('valor', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('observacao', 'text', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('source_ledger_entry_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('destination_ledger_entry_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'conta_destino_id',
                ]
            )
            ->addIndex(
                [
                    'conta_origem_id',
                ]
            )
            ->addIndex(
                [
                    'destination_ledger_entry_id',
                ]
            )
            ->addIndex(
                [
                    'payment_method_id',
                ]
            )
            ->addIndex(
                [
                    'source_ledger_entry_id',
                ]
            )
            ->create();

        $this->table('users')
            ->addColumn('ativo', 'tinyinteger', [
                'default' => '1',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('nome', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('role_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('hash', 'string', [
                'default' => null,
                'limit' => 40,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'role_id',
                ]
            )
            ->create();

        $this->table('vehicle_orifices')
            ->addColumn('vehicle_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('numero', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('quantidade_litros', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'vehicle_id',
                ]
            )
            ->create();

        $this->table('vehicles')
            ->addColumn('tipo', 'string', [
                'comment' => 'T=Truck 4=4o Eixo C=Cavalo',
                'default' => null,
                'limit' => 1,
                'null' => false,
            ])
            ->addColumn('marca_modelo', 'string', [
                'default' => null,
                'limit' => 100,
                'null' => false,
            ])
            ->addColumn('ano', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('placa', 'string', [
                'default' => null,
                'limit' => 10,
                'null' => false,
            ])
            ->addColumn('quantidade_litros', 'float', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('quantidade_bocas', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('cronotacografo', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('licenciamento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ipva_parcelado', 'tinyinteger', [
                'default' => '0',
                'limit' => 4,
                'null' => false,
            ])
            ->addColumn('ipva1', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ipva1_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ipva2', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ipva2_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ipva3', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('ipva3_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('marca_modelo_tanque', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => true,
            ])
            ->addColumn('ano_fabricacao_tanque', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => true,
            ])
            ->addColumn('civ', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('cipp', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('calibragem', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('letpp', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('seguro_seguradora', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('seguro_corretora', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('seguro_apolice', 'string', [
                'default' => null,
                'limit' => 200,
                'null' => false,
            ])
            ->addColumn('seguro_vencimento', 'date', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();

        $this->table('accounts')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'distributor_id',
                'distributors',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('accounts_distributor_contacts')
            ->addForeignKey(
                'account_id',
                'accounts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'distributor_contact_id',
                'distributor_contacts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('broker_bank_accounts')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'broker_id',
                'brokers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('broker_contacts')
            ->addForeignKey(
                'broker_id',
                'brokers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('broker_emails')
            ->addForeignKey(
                'broker_id',
                'brokers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('broker_phones')
            ->addForeignKey(
                'broker_id',
                'brokers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('brokers')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('carrier_bank_accounts')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'carrier_id',
                'carriers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('carrier_contacts')
            ->addForeignKey(
                'carrier_id',
                'carriers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('carrier_emails')
            ->addForeignKey(
                'carrier_id',
                'carriers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('carrier_phones')
            ->addForeignKey(
                'carrier_id',
                'carriers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('cart_orifices')
            ->addForeignKey(
                'cart_id',
                'carts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('carts')
            ->addForeignKey(
                'vehicle_id',
                'vehicles',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('client_emails')
            ->addForeignKey(
                'client_id',
                'clients',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('client_logs')
            ->addForeignKey(
                'client_id',
                'clients',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'user_id',
                'users',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('client_phones')
            ->addForeignKey(
                'client_id',
                'clients',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('clients')
            ->addForeignKey(
                'status_id',
                'status_clients',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('distributor_bank_accounts')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'distributor_id',
                'distributors',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('distributor_contacts')
            ->addForeignKey(
                'distributor_id',
                'distributors',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('distributor_emails')
            ->addForeignKey(
                'distributor_id',
                'distributors',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('distributor_phones')
            ->addForeignKey(
                'distributor_id',
                'distributors',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('initial_balances')
            ->addForeignKey(
                'account_id',
                'accounts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('ledger_entries')
            ->addForeignKey(
                'account_id',
                'accounts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'ledger_entry_type_id',
                'ledger_entry_types',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'payable_id',
                'payables',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'payment_method_id',
                'payment_methods',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'receivable_id',
                'receivables',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('module_actions')
            ->addForeignKey(
                'module_id',
                'modules',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('payables')
            ->addForeignKey(
                'purchase_payment_id',
                'purchase_payments',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'purchase_transfer_id',
                'purchase_transfers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'sale_detail_id',
                'sale_details',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'sale_id',
                'sales',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'sales_purchases_id',
                'sales_purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('payament_slips')
            ->addForeignKey(
                'account_id',
                'accounts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'payament_slip_batch_id',
                'payament_slip_batches',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'receivable_id',
                'receivables',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('plant_bank_accounts')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'plant_id',
                'plants',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('plant_emails')
            ->addForeignKey(
                'plant_id',
                'plants',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('plant_phones')
            ->addForeignKey(
                'plant_id',
                'plants',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('purchase_devolutions')
            ->addForeignKey(
                'purchase_id',
                'purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('purchase_payments')
            ->addForeignKey(
                'purchase_id',
                'purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('purchase_transfers')
            ->addForeignKey(
                'carrier_id',
                'carriers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'destination_purchase_id',
                'purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'plant_id',
                'plants',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'purchase_id',
                'purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('purchases')
            ->addForeignKey(
                'broker_id',
                'brokers',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'distributor_id',
                'distributors',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'plant_id',
                'plants',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('receivables')
            ->addForeignKey(
                'sale_detail_id',
                'sale_details',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('sales')
            ->addForeignKey(
                'carrier_id',
                'carriers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'driver_id',
                'drivers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'vehicle_id',
                'vehicles',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('sales_purchases')
            ->addForeignKey(
                'purchase_id',
                'purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'sale_id',
                'sales',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('sales_purchases_conciliations')
            ->addForeignKey(
                'sales_purchases_id',
                'sales_purchases',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('seller_bank_accounts')
            ->addForeignKey(
                'bank_id',
                'banks',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'seller_id',
                'sellers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('seller_emails')
            ->addForeignKey(
                'seller_id',
                'sellers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('seller_phones')
            ->addForeignKey(
                'seller_id',
                'sellers',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('transfers')
            ->addForeignKey(
                'conta_destino_id',
                'accounts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'conta_origem_id',
                'accounts',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'destination_ledger_entry_id',
                'ledger_entries',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'payment_method_id',
                'payment_methods',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->addForeignKey(
                'source_ledger_entry_id',
                'ledger_entries',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('users')
            ->addForeignKey(
                'role_id',
                'roles',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();

        $this->table('vehicle_orifices')
            ->addForeignKey(
                'vehicle_id',
                'vehicles',
                'id',
                [
                    'update' => 'RESTRICT',
                    'delete' => 'RESTRICT'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('accounts')
            ->dropForeignKey(
                'bank_id'
            )
            ->dropForeignKey(
                'distributor_id'
            );

        $this->table('accounts_distributor_contacts')
            ->dropForeignKey(
                'account_id'
            )
            ->dropForeignKey(
                'distributor_contact_id'
            );

        $this->table('broker_bank_accounts')
            ->dropForeignKey(
                'bank_id'
            )
            ->dropForeignKey(
                'broker_id'
            );

        $this->table('broker_contacts')
            ->dropForeignKey(
                'broker_id'
            );

        $this->table('broker_emails')
            ->dropForeignKey(
                'broker_id'
            );

        $this->table('broker_phones')
            ->dropForeignKey(
                'broker_id'
            );

        $this->table('brokers')
            ->dropForeignKey(
                'bank_id'
            );

        $this->table('carrier_bank_accounts')
            ->dropForeignKey(
                'bank_id'
            )
            ->dropForeignKey(
                'carrier_id'
            );

        $this->table('carrier_contacts')
            ->dropForeignKey(
                'carrier_id'
            );

        $this->table('carrier_emails')
            ->dropForeignKey(
                'carrier_id'
            );

        $this->table('carrier_phones')
            ->dropForeignKey(
                'carrier_id'
            );

        $this->table('cart_orifices')
            ->dropForeignKey(
                'cart_id'
            );

        $this->table('carts')
            ->dropForeignKey(
                'vehicle_id'
            );

        $this->table('client_emails')
            ->dropForeignKey(
                'client_id'
            );

        $this->table('client_logs')
            ->dropForeignKey(
                'client_id'
            )
            ->dropForeignKey(
                'user_id'
            );

        $this->table('client_phones')
            ->dropForeignKey(
                'client_id'
            );

        $this->table('clients')
            ->dropForeignKey(
                'status_id'
            );

        $this->table('distributor_bank_accounts')
            ->dropForeignKey(
                'bank_id'
            )
            ->dropForeignKey(
                'distributor_id'
            );

        $this->table('distributor_contacts')
            ->dropForeignKey(
                'distributor_id'
            );

        $this->table('distributor_emails')
            ->dropForeignKey(
                'distributor_id'
            );

        $this->table('distributor_phones')
            ->dropForeignKey(
                'distributor_id'
            );

        $this->table('initial_balances')
            ->dropForeignKey(
                'account_id'
            );

        $this->table('ledger_entries')
            ->dropForeignKey(
                'account_id'
            )
            ->dropForeignKey(
                'ledger_entry_type_id'
            )
            ->dropForeignKey(
                'payable_id'
            )
            ->dropForeignKey(
                'payment_method_id'
            )
            ->dropForeignKey(
                'receivable_id'
            );

        $this->table('module_actions')
            ->dropForeignKey(
                'module_id'
            );

        $this->table('payables')
            ->dropForeignKey(
                'purchase_payment_id'
            )
            ->dropForeignKey(
                'purchase_transfer_id'
            )
            ->dropForeignKey(
                'sale_detail_id'
            )
            ->dropForeignKey(
                'sale_id'
            )
            ->dropForeignKey(
                'sales_purchases_id'
            );

        $this->table('payament_slips')
            ->dropForeignKey(
                'account_id'
            )
            ->dropForeignKey(
                'payament_slip_batch_id'
            )
            ->dropForeignKey(
                'receivable_id'
            );

        $this->table('plant_bank_accounts')
            ->dropForeignKey(
                'bank_id'
            )
            ->dropForeignKey(
                'plant_id'
            );

        $this->table('plant_emails')
            ->dropForeignKey(
                'plant_id'
            );

        $this->table('plant_phones')
            ->dropForeignKey(
                'plant_id'
            );

        $this->table('purchase_devolutions')
            ->dropForeignKey(
                'purchase_id'
            );

        $this->table('purchase_payments')
            ->dropForeignKey(
                'purchase_id'
            );

        $this->table('purchase_transfers')
            ->dropForeignKey(
                'carrier_id'
            )
            ->dropForeignKey(
                'destination_purchase_id'
            )
            ->dropForeignKey(
                'plant_id'
            )
            ->dropForeignKey(
                'purchase_id'
            );

        $this->table('purchases')
            ->dropForeignKey(
                'broker_id'
            )
            ->dropForeignKey(
                'distributor_id'
            )
            ->dropForeignKey(
                'plant_id'
            );

        $this->table('receivables')
            ->dropForeignKey(
                'sale_detail_id'
            );

        $this->table('sales')
            ->dropForeignKey(
                'carrier_id'
            )
            ->dropForeignKey(
                'driver_id'
            )
            ->dropForeignKey(
                'vehicle_id'
            );

        $this->table('sales_purchases')
            ->dropForeignKey(
                'purchase_id'
            )
            ->dropForeignKey(
                'sale_id'
            );

        $this->table('sales_purchases_conciliations')
            ->dropForeignKey(
                'sales_purchases_id'
            );

        $this->table('seller_bank_accounts')
            ->dropForeignKey(
                'bank_id'
            )
            ->dropForeignKey(
                'seller_id'
            );

        $this->table('seller_emails')
            ->dropForeignKey(
                'seller_id'
            );

        $this->table('seller_phones')
            ->dropForeignKey(
                'seller_id'
            );

        $this->table('transfers')
            ->dropForeignKey(
                'conta_destino_id'
            )
            ->dropForeignKey(
                'conta_origem_id'
            )
            ->dropForeignKey(
                'destination_ledger_entry_id'
            )
            ->dropForeignKey(
                'payment_method_id'
            )
            ->dropForeignKey(
                'source_ledger_entry_id'
            );

        $this->table('users')
            ->dropForeignKey(
                'role_id'
            );

        $this->table('vehicle_orifices')
            ->dropForeignKey(
                'vehicle_id'
            );

        $this->dropTable('accounts');
        $this->dropTable('accounts_distributor_contacts');
        $this->dropTable('banks');
        $this->dropTable('broker_bank_accounts');
        $this->dropTable('broker_contacts');
        $this->dropTable('broker_emails');
        $this->dropTable('broker_phones');
        $this->dropTable('brokers');
        $this->dropTable('carrier_bank_accounts');
        $this->dropTable('carrier_contacts');
        $this->dropTable('carrier_emails');
        $this->dropTable('carrier_phones');
        $this->dropTable('carriers');
        $this->dropTable('cart_orifices');
        $this->dropTable('carts');
        $this->dropTable('client_contacts');
        $this->dropTable('client_emails');
        $this->dropTable('client_logs');
        $this->dropTable('client_phones');
        $this->dropTable('clients');
        $this->dropTable('distributor_bank_accounts');
        $this->dropTable('distributor_contacts');
        $this->dropTable('distributor_emails');
        $this->dropTable('distributor_phones');
        $this->dropTable('distributors');
        $this->dropTable('drivers');
        $this->dropTable('initial_balances');
        $this->dropTable('ledger_entries');
        $this->dropTable('ledger_entry_types');
        $this->dropTable('menus');
        $this->dropTable('module_actions');
        $this->dropTable('modules');
        $this->dropTable('payables');
        $this->dropTable('payament_slip_batches');
        $this->dropTable('payament_slips');
        $this->dropTable('payment_methods');
        $this->dropTable('plant_bank_accounts');
        $this->dropTable('plant_contacts');
        $this->dropTable('plant_emails');
        $this->dropTable('plant_phones');
        $this->dropTable('plants');
        $this->dropTable('presales');
        $this->dropTable('price_settings');
        $this->dropTable('prices');
        $this->dropTable('purchase_devolutions');
        $this->dropTable('purchase_payments');
        $this->dropTable('purchase_transfers');
        $this->dropTable('purchases');
        $this->dropTable('receivables');
        $this->dropTable('roles');
        $this->dropTable('sale_details');
        $this->dropTable('sales');
        $this->dropTable('sales_purchases');
        $this->dropTable('sales_purchases_conciliations');
        $this->dropTable('seller_bank_accounts');
        $this->dropTable('seller_emails');
        $this->dropTable('seller_phones');
        $this->dropTable('sellers');
        $this->dropTable('transfers');
        $this->dropTable('users');
        $this->dropTable('vehicle_orifices');
        $this->dropTable('vehicles');
    }
}
