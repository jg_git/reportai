<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Purchase Entity
 *
 * @property int $id
 * @property string $nro_contrato
 * @property \Cake\I18n\Date $data_compra
 * @property int $plant_id
 * @property int $distributor_id
 * @property int $corretada
 * @property int $broker_id
 * @property int $volume_comprado
 * @property int $vendido
 * @property int $volume_restante
 * @property float $preco_litro
 * @property string $forma_pagamento
 * @property float $comissao
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Plant $plant
 * @property \App\Model\Entity\Distributor $distributor
 * @property \App\Model\Entity\Broker $broker
 * @property \App\Model\Entity\PurchaseCommision[] $purchase_commisions
 * @property \App\Model\Entity\PurchasePayment[] $purchase_payments
 */
class Purchase extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
//        'numero_contrato' => true,
//        'data_compra' => true,
//        'plant_id' => true,
//        'distributor_id' => true,
//        'corretada' => true,
//        'broker_id' => true,
//        'volume_comprado' => true,
//        'vendido' => true,
//        'volume_restante' => true,
//        'preco_litro' => true,
//        'forma_pagamento' => true,
//        'comissao' => true,
//        'created' => true,
//        'modified' => true,
//        'plant' => true,
//        'distributor' => true,
//        'broker' => true,
//        'purchase_commisions' => true,
//        'purchase_payments' => true
    ];
}
