<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * VehicleOrifice Entity
 *
 * @property int $id
 * @property int $vehicle_id
 * @property int $numero
 * @property int $quantidade_litros
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Vehicle $vehicle
 */
class VehicleOrifice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'vehicle_id' => true,
        'numero' => true,
        'quantidade_litros' => true,
        'created' => true,
        'modified' => true,
        'vehicle' => true
    ];
}
