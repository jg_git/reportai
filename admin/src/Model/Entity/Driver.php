<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Driver Entity
 *
 * @property int $id
 * @property string $nome
 * @property string $cnh
 * @property \Cake\I18n\Date $data_vencimento
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 */
class Driver extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nome' => true,
        'cnh' => true,
        'cpf' => true,
        'rg' => true,
        'data_vencimento' => true,
        'mopp_vencimento' => true,
        'nr20_vencimento' => true,
        'nr35_vencimento' => true,
        'cadastro_perfil_vencimento' => true,
        'celular1' => true,
        'celular2' => true,
        'celular3' => true,
        'created' => true,
        'modified' => true
    ];
}
