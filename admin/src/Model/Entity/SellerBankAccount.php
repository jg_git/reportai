<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * SellerBankAccount Entity
 *
 * @property int $id
 * @property int $seller_id
 * @property int $bank_id
 * @property string $numero_agencia
 * @property string $numero_conta
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Seller $seller
 * @property \App\Model\Entity\Bank $bank
 */
class SellerBankAccount extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
//        'seller_id' => true,
//        'bank_id' => true,
//        'numero_agencia' => true,
//        'numero_conta' => true,
//        'created' => true,
//        'modified' => true,
//        'seller' => true,
//        'bank' => true
    ];
}
