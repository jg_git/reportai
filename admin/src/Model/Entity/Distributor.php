<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Distributor Entity
 *
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $cnpj
 * @property string $ie
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\DistributorEmail[] $distributor_emails
 * @property \App\Model\Entity\PlantContact[] $plant_contacts
 * @property \App\Model\Entity\PlantPhone[] $plant_phones
 */
class Distributor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
//        'razao_social' => true,
//        'nome_fantasia' => true,
//        'cnpj' => true,
//        'inscricao_estadual' => true,
//        'inscricao_municipal' => true,
//        'cep' => true,
//        'logradouro' => true,
//        'numero' => true,
//        'complemento' => true,
//        'bairro' => true,
//        'cidade' => true,
//        'estado' => true,
//        'created' => true,
//        'modified' => true,
//        'distributor_emails' => true,
//        'plant_contacts' => true,
//        'plant_phones' => true
    ];
}
