<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class LedgerEntriesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('ledger_entries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->value('account_id')
            ->value('ledger_entry_type_id')
            ->compare('date_start', [
                'operator' => '>=',
                'field' => 'data_lancamento'
            ])
            ->compare('date_end', [
                'operator' => '<=',
                'field' => 'data_lancamento'
            ]);

        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('LedgerEntryTypes', [
            'foreignKey' => 'ledger_entry_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PaymentMethods', [
            'foreignKey' => 'payment_method_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Payables', [
            'foreignKey' => 'payable_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Receivables', [
            'foreignKey' => 'receivable_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasOne('SourceTransfers', [
            'className' => 'Transfers',
            'foreignKey' => 'source_ledger_entry_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasOne('DestinationTransfers', [
            'className' => 'Transfers',
            'foreignKey' => 'destination_ledger_entry_id',
            'joinType' => 'LEFT'
        ]);
        // $this->belongsTo('Transfers.SourceAccounts', [
        // 'className' => 'Accounts',
        // 'foreignKey' => 'conta_origem_id',
        // 'joinType' => 'INNER'
        //]);


        // $this->belongsTo('Transfers', [
        //     'foreignKey' => 'account_id',
        //     'joinType' => 'LEFT'
        // ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('data_lancamento', 'dmy')
            ->requirePresence('data_lancamento', 'create')
            ->notEmpty('data_lancamento');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 100)
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        $validator
            ->scalar('numero_documento')
            ->maxLength('numero_documento', 20)
            ->requirePresence('numero_documento', 'create')
            ->allowEmpty('numero_documento');

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->scalar('observacao')
            ->allowEmpty('observacao');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['account_id'], 'Accounts'));
        $rules->add($rules->existsIn(['ledger_entry_type_id'], 'LedgerEntryTypes'));
        $rules->add($rules->existsIn(['payment_method_id'], 'PaymentMethods'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        $data['valor'] = str_replace(',', '.', str_replace('.', '', $data['valor']));
    }
}
