<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - SERÁ MODIFICADA FUTURAMENTE
namespace App\Model\Table;use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 * DealPayments Model
 *
 * @property \App\Model\Table\DealsTable|\Cake\ORM\Association\BelongsTo $Deals
 *
 * @method \App\Model\Entity\DealPayment get($primaryKey, $options = [])
 * @method \App\Model\Entity\DealPayment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DealPayment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DealPayment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DealPayment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DealPayment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DealPayment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DealPaymentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->setTable('deal_payments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('Deals', [
            'foreignKey' => 'deal_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {   
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        $validator
            ->date('d/m/y', 'dt_pagamento')
            ->allowEmpty('dt_pagamento');
            //NO VALIDATOR DE DATAS PRECISAMOS COLOCAR A MÁSCARA DE DATA QUE ESTAMOS UTILIZANDO
        $validator
            ->date('d/m/y', 'dt_emissao')
            ->requirePresence('dt_emissao', 'create')
            ->notEmpty('dt_emissao');
        $validator
            ->numeric('valor_pagamento')
            ->requirePresence('valor_pagamento', 'create')
            ->notEmpty('valor_pagamento');
        $validator
            ->scalar('nota_fiscal')
            ->maxLength('nota_fiscal', 50)
            ->requirePresence('nota_fiscal', 'create')
            ->notEmpty('nota_fiscal');
        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['deal_id'], 'Deals'));
        return $rules;
    }}
