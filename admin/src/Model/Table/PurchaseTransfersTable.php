<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PurchaseTransfersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('purchase_transfers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Purchases', [
            'foreignKey' => 'purchase_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Carriers', [
            'foreignKey' => 'carrier_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Plants', [
            'foreignKey' => 'plant_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('DestinationPurchases', [
            'className' => 'Purchases',
            'foreignKey' => 'destination_purchase_id',
            'joinType' => 'LEFT'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('volume')
            ->requirePresence('volume', 'create')
            ->notEmpty('volume');

        $validator
            ->numeric('frete')
            ->requirePresence('frete', 'create')
            ->notEmpty('frete');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['purchase_id'], 'Purchases'));
        $rules->add($rules->existsIn(['carrier_id'], 'Carriers'));
        $rules->add($rules->existsIn(['plant_id'], 'Plants'));
        $rules->add($rules->existsIn(['destination_purchase_id'], 'Purchases'));

        return $rules;
    }
}
