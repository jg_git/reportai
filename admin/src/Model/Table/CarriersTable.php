<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Carriers Model
 *
 * @property \App\Model\Table\CarrierContactsTable|\Cake\ORM\Association\HasMany $CarrierContacts
 * @property \App\Model\Table\CarrierEmailsTable|\Cake\ORM\Association\HasMany $CarrierEmails
 * @property \App\Model\Table\CarrierPhonesTable|\Cake\ORM\Association\HasMany $CarrierPhones
 *
 * @method \App\Model\Entity\Carrier get($primaryKey, $options = [])
 * @method \App\Model\Entity\Carrier newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Carrier[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Carrier|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Carrier patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Carrier[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Carrier findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CarriersTable extends Table
{

        /**
         * Initialize method
         *
         * @param array $config The configuration for the Table.
         * @return void
         */
        public function initialize(array $config)
        {
                parent::initialize($config);

                $this->setTable('carriers');
                $this->setDisplayField('razao_social');
                $this->setPrimaryKey('id');

                $this->addBehavior('Timestamp');

                $this->addBehavior('Search.Search');
                $this->searchManager()
                        ->like('razao_social', [
                                'multiValue' => true,
                                'before' => true,
                                'after' => true,
                                'field' => ['razao_social']
                        ])
                        ->like('nome_fantasia', [
                                'multiValue' => true,
                                'before' => true,
                                'after' => true,
                                'field' => ['nome_fantasia']
                        ])
                        ->like('cnpj', [
                                'multiValue' => false,
                                'before' => true,
                                'after' => true,
                                'field' => ['cnpj']
                        ]);

                $this->hasMany('CarrierContacts', [
                        'foreignKey' => 'carrier_id',
                        'dependent' => true
                ]);
                $this->hasMany('CarrierEmails', [
                        'foreignKey' => 'carrier_id',
                        'dependent' => true
                ]);
                $this->hasMany('CarrierPhones', [
                        'foreignKey' => 'carrier_id',
                        'dependent' => true
                ]);
                $this->belongsToMany('Drivers', [
                        'foreignKey' => 'carrier_id',
                        'targetForeignKey' => 'driver_id',
                        'joinTable' => 'carriers_drivers'
                ]);
                $this->belongsToMany('Vehicles', [
                        'foreignKey' => 'carrier_id',
                        'targetForeignKey' => 'vehicle_id',
                        'joinTable' => 'carriers_vehicles'
                ]);
                $this->hasMany('CarrierBankAccounts', [
                        'foreignKey' => 'carrier_id',
                        'dependent' => true
                ]);
        }

        public function validationPart1(Validator $validator)
        {
                $validator
                        ->integer('id')
                        ->allowEmpty('id', 'create');

                $validator
                        ->scalar('razao_social')
                        ->maxLength('razao_social', 100)
                        ->requirePresence('razao_social', 'create')
                        ->notEmpty('razao_social');

                $validator
                        ->scalar('nome_fantasia')
                        ->maxLength('nome_fantasia', 100)
                        ->requirePresence('nome_fantasia', 'create')
                        ->notEmpty('nome_fantasia');

                $validator
                        ->scalar('cnpj')
                        ->maxLength('cnpj', 20)
                        ->requirePresence('cnpj', 'create')
                        ->notEmpty('cnpj');

                $validator
                        ->scalar('inscricao_estadual')
                        ->maxLength('inscricao_estadual', 15)
                        ->allowEmpty('inscricao_estadual');

                $validator
                        ->scalar('inscricao_municipal')
                        ->maxLength('inscricao_municipal', 15)
                        ->allowEmpty('inscricao_municipal');

                return $validator;
        }

        public function validationPart2(Validator $validator)
        {
                $validator
                        ->scalar('cep')
                        ->maxLength('cep', 10)
                        ->requirePresence('cep', 'create')
                        ->notEmpty('cep');

                $validator
                        ->scalar('logradouro')
                        ->maxLength('logradouro', 200)
                        ->requirePresence('logradouro', 'create')
                        ->notEmpty('logradouro');

                $validator
                        ->scalar('complemento')
                        ->maxLength('complemento', 100)
                        ->requirePresence('complemento', 'create')
                        ->allowEmpty('complemento');

                $validator
                        ->scalar('numero')
                        ->maxLength('numero', 15)
                        ->requirePresence('numero', 'create')
                        ->notEmpty('numero');

                $validator
                        ->scalar('bairro')
                        ->maxLength('bairro', 50)
                        ->requirePresence('bairro', 'create')
                        ->notEmpty('bairro');

                $validator
                        ->scalar('cidade')
                        ->maxLength('cidade', 50)
                        ->requirePresence('cidade', 'create')
                        ->notEmpty('cidade');

                $validator
                        ->scalar('estado')
                        ->maxLength('estado', 2)
                        ->requirePresence('estado', 'create')
                        ->notEmpty('estado');


                return $validator;
        }
}
