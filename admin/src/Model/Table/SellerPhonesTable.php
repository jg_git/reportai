<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SellerPhones Model
 *
 * @property \App\Model\Table\SellersTable|\Cake\ORM\Association\BelongsTo $Sellers
 *
 * @method \App\Model\Entity\SellerPhone get($primaryKey, $options = [])
 * @method \App\Model\Entity\SellerPhone newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SellerPhone[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SellerPhone|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SellerPhone patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SellerPhone[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SellerPhone findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SellerPhonesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('seller_phones');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Sellers', [
            'foreignKey' => 'seller_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('ddd')
                ->maxLength('ddd', 2)
                ->requirePresence('ddd', 'create')
                ->notEmpty('ddd');

        $validator
                ->scalar('telefone')
                ->maxLength('telefone', 20)
                ->requirePresence('telefone', 'create')
                ->notEmpty('telefone');

        $validator
                ->scalar('tipo')
                ->maxLength('tipo', 1)
                ->requirePresence('tipo', 'create')
                ->notEmpty('tipo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['seller_id'], 'Sellers'));

        return $rules;
    }

}
