<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Log\Log;
use ArrayObject;
use Cake\Event\Event;

class PresalesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('presales');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasOne('Child', [
            'className' => 'Presales',
            'foreignKey' => 'parent_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Parent', [
            'className' => 'Presales',
            'foreignKey' => 'parent_id',
            'joinType' => 'LEFT'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->integer('client_id')
                ->notEmpty('client_id', 'create');

        $validator
                ->date('data_pedido', 'dmy')
                ->requirePresence('data_pedido', 'create')
                ->notEmpty('data_pedido');

        $validator
                ->integer('volume_pedido')
                ->requirePresence('volume_pedido', 'create')
                ->notEmpty('volume_pedido', 'Obrigatório');

        $validator
                ->numeric('preco_venda')
                ->requirePresence('preco_venda', 'create')
                ->notEmpty('preco_venda', 'Obrigatório');

        $validator
                ->numeric('preco_mercado')
                ->requirePresence('preco_mercado', 'create')
                ->notEmpty('preco_mercado', 'Obrigatório');

        $validator
                ->date('data_pagamento_1', 'dmy')
                ->requirePresence('data_pagamento_1', 'create')
                ->notEmpty('data_pagamento_1', 'Obrigatório');

        $validator
                ->date('data_pagamento_2', 'dmy')
                ->allowEmpty('data_pagamento_2');

        $validator
                ->date('data_pagamento_3', 'dmy')
                ->allowEmpty('data_pagamento_3');

        $validator
                ->date('data_pagamento_4', 'dmy')
                ->allowEmpty('data_pagamento_4');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['client_id'], 'Clients'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        $data['volume_pedido'] = str_replace(',', '.', str_replace('.', '', $data['volume_pedido']));
        $data['preco_venda'] = str_replace(',', '.', str_replace('.', '', $data['preco_venda']));
        $data['preco_mercado'] = str_replace(',', '.', str_replace('.', '', $data['preco_mercado']));
    }

}
