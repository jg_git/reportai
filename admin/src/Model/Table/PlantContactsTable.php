<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PlantContacts Model
 *
 * @property \App\Model\Table\PlantsTable|\Cake\ORM\Association\BelongsTo $Plants
 *
 * @method \App\Model\Entity\PlantContact get($primaryKey, $options = [])
 * @method \App\Model\Entity\PlantContact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PlantContact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PlantContact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PlantContact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PlantContact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PlantContact findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PlantContactsTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('plant_contacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Plants', [
            'foreignKey' => 'plant_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('nome')
                ->maxLength('nome', 100)
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->scalar('cpf')
                ->maxLength('cpf', 20)
                ->requirePresence('cpf', 'create')
                ->notEmpty('cpf');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->notEmpty('email');

        $validator
                ->scalar('telefone')
                ->maxLength('telefone', 20)
                ->requirePresence('telefone', 'create')
                ->allowEmpty('telefone');

        $validator
                ->scalar('celular')
                ->maxLength('celular', 20)
                ->requirePresence('celular', 'create')
                ->notEmpty('celular');

        $validator
                ->scalar('cep')
                ->maxLength('cep', 10)
                ->requirePresence('cep', 'create')
                ->allowEmpty('cep');

        $validator
                ->scalar('logradouro')
                ->maxLength('logradouro', 200)
                ->requirePresence('logradouro', 'create')
                ->allowEmpty('logradouro');

        $validator
                ->scalar('numero')
                ->maxLength('numero', 15)
                ->requirePresence('numero', 'create')
                ->allowEmpty('numero');

        $validator
                ->scalar('complemento')
                ->maxLength('complemento', 100)
                ->requirePresence('complemento', 'create')
                ->allowEmpty('complemento');

        $validator
                ->scalar('bairro')
                ->maxLength('bairro', 50)
                ->requirePresence('bairro', 'create')
                ->allowEmpty('bairro');

        $validator
                ->scalar('cidade')
                ->maxLength('cidade', 50)
                ->requirePresence('cidade', 'create')
                ->allowEmpty('cidade');

        $validator
                ->scalar('estado')
                ->maxLength('estado', 2)
                ->requirePresence('estado', 'create')
                ->allowEmpty('estado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['plant_id'], 'Plants'));

        return $rules;
    }

}
