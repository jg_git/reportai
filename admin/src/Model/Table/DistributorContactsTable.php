<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class DistributorContactsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('distributor_contacts');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Distributors', [
            'foreignKey' => 'plant_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('nome')
                ->maxLength('nome', 100)
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->scalar('cpf')
                ->maxLength('cpf', 20)
                ->requirePresence('cpf', 'create')
                ->allowEmpty('cpf');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->allowEmpty('email');

        $validator
                ->scalar('funcionalidade')
                ->maxLength('funcionalidade', 200)
                ->requirePresence('funcionalidade', 'create')
                ->allowEmpty('funcionalidade');

        $validator
                ->scalar('telefone')
                ->maxLength('telefone', 20)
                ->requirePresence('telefone', 'create')
                ->allowEmpty('telefone');

        $validator
                ->scalar('celular')
                ->maxLength('celular', 20)
                ->requirePresence('celular', 'create')
                ->allowEmpty('celular');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['plant_id'], 'Distributors'));

        return $rules;
    }

}
