<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - PODERÁ SER MODIFICADA FUTURAMENTE
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 * Deals Model
 *
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 * @property \App\Model\Table\DealPaymentsTable|\Cake\ORM\Association\HasMany $DealPayments
 *
 * @method \App\Model\Entity\Deal get($primaryKey, $options = [])
 * @method \App\Model\Entity\Deal newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Deal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Deal|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Deal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Deal[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Deal findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DealsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);        
        $this->setTable('deals');        
        $this->setDisplayField('id');        
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->searchManager()

        ->like('cnpjcpf', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['Clients.cnpj_cpf']
        ]) 
        ->like('razao_social', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['Clients.razao_social']
        ]) 
        ->value('valor', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['valor']
        ]);  
        $this->belongsTo('Clients', [
            'foreignKey' => 'client_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('DealPayments', [
            'foreignKey' => 'deal_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationPart1(Validator $validator)
    {        
        $validator->integer('id')
            ->allowEmpty('id', 'create');  

        $validator->integer('client_id')
            ->allowEmpty('client_id', 'create');  

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');        
        
        $validator->date('d/m/y','data_vigencia_inicio')
            ->requirePresence('data_vigencia_inicio', 'create')
            ->notEmpty('data_vigencia_inicio');        
        
        $validator->date('d/m/y', 'data_vigencia_termino')
            ->requirePresence('data_vigencia_termino', 'create')
            ->notEmpty('data_vigencia_termino');        
        
        $validator->date('d/m/y', 'data_firmacao_contrato')
            ->requirePresence('data_firmacao_contrato', 'create')
            ->notEmpty('data_firmacao_contrato');

        $validator->integer('n_max_usuario')
            ->requirePresence('n_max_usuario', 'create')
            ->notEmpty('n_max_usuario');
  
        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['client_id'], 'Clients'));
        return $rules;
    }
}
