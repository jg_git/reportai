<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class PayamentSlipsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payament_slips');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->compare('data_vencimento_de', [
                'operator' => '>=',
                'field' => 'data_vencimento'
            ])
            ->compare('data_vencimento_ate', [
                'operator' => '<=',
                'field' => 'data_vencimento'
            ]);
            // ->add('q', 'Search.Callback', [
            //     'callback' => function ($query, $args, $filter) {
            //         return $query->matching('SaleDetails', function ($query) use ($args) {
            //             return $query->matching('Presales', function ($query) use ($args) {
            //                 return $query->matching('Clients', function ($query) use ($args) {
            //                     return $query->where(['Clients.razao_social LIKE' => '%' . $args['q'] . '%']);
            //                 });
            //             });
            //         });
            //     }
            // ]);

        $this->belongsTo('PayamentSlipBatches', [
            'foreignKey' => 'payament_slip_batch_id'
        ]);
        $this->belongsTo('Receivables', [
            'foreignKey' => 'receivable_id'
        ]);
        $this->belongsTo('Accounts', [
            'foreignKey' => 'account_id'
        ]);
    }
}
