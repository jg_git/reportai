<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - PODERÁ SER MODIFICADA FUTURAMENTE
namespace App\Model\Table;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;/**
 * Locations Model
 *
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 *
 * @method \App\Model\Entity\Location get($primaryKey, $options = [])
 * @method \App\Model\Entity\Location newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Location[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Location|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Location patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Location[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Location findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PollsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);        
        $this->setTable('polls');        
        $this->setDisplayField('nome_formulario');        
        $this->setPrimaryKey('id');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');
        $this->searchManager()
        ->like('nome_local', [
            'multiValue' => true,
            'before' => true,
            'after' => true,
            'field' => ['nome_local']
        ]);

        $this->belongsTo('Locations', [
            'foreignKey' => 'location_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('PollUsers', [
            'foreignKey' => 'poll_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);

        $this->hasMany('PollSections', [
            'foreignKey' => 'poll_id',
            'dependent' => true,
            'cascadeCallbacks' => true
        ]);
        

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationPart1(Validator $validator)
    {        
        $validator->integer('id')
            ->allowEmpty('id', 'create');  

        $validator
            ->scalar('nome_formulario')
            ->maxLength('nome_formulario', 150)
            ->requirePresence('nome_formulario', 'create')
            ->notEmpty('nome_formulario');
            
        $validator
            ->numeric('tipo_formulario')
            ->requirePresence('tipo_formulario', 'create')
            ->notEmpty('tipo_formulario');
  
        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {        
        $rules->add($rules->existsIn(['location_id'], 'Locations'));
        return $rules;
    }
}
