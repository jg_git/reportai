<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use ArrayObject;
use Cake\Event\Event;

class SalesPurchasesTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('sales_purchases');
        $this->setDisplayField('sale_id');
//        $this->setPrimaryKey(['sale_id', 'purchase_id']);
        $this->setPrimaryKey(['id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Purchases', [
            'foreignKey' => 'purchase_id',
            'joinType' => 'LEFT'
        ]);
        $this->hasOne('Payables', [
            'foreignKey' => 'sales_purchases_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('SalesPurchasesConciliations', [
            'foreignKey' => 'sales_purchases_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('volume')
//                ->requirePresence('volume', 'create')
                ->notEmpty('volume');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->existsIn(['sale_id'], 'Sales'));
        $rules->add($rules->existsIn(['purchase_id'], 'Purchases'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options) {
        if (array_key_exists('volume', $data)) {
            $data['volume'] = str_replace(',', '.', str_replace('.', '', $data['volume']));
        }
    }

}
