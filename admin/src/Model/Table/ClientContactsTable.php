<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ClientContactsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('client_contacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('tipo')
                ->maxLength('tipo', 1)
                ->requirePresence('tipo', 'create')
                ->notEmpty('tipo');

        $validator
                ->scalar('nome')
                ->maxLength('nome', 100)
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->scalar('cpf')
                ->maxLength('cpf', 20)
                ->requirePresence('cpf', 'create')
                ->allowEmpty('cpf');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->allowEmpty('email');

        $validator
                ->scalar('telefone')
                ->maxLength('telefone', 20)
                ->requirePresence('telefone', 'create')
                ->allowEmpty('telefone');

        $validator
                ->scalar('celular1')
                ->maxLength('celular1', 20)
                ->requirePresence('celular1', 'create')
                ->allowEmpty('celular1');

        $validator
                ->scalar('celular2')
                ->maxLength('celular2', 20)
                ->requirePresence('celular2', 'create')
                ->allowEmpty('celular2');

        $validator
                ->scalar('celular3')
                ->maxLength('celular3', 20)
                ->requirePresence('celular3', 'create')
                ->allowEmpty('celular3');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }

}
