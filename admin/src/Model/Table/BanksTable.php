<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class BanksTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('banks');
        $this->setDisplayField('nome');
        $this->setPrimaryKey('id');

        $this->addBehavior('Search.Search');
        $this->searchManager()
                ->like('nome', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['nome']]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('codigo')
                ->maxLength('codigo', 10)
                ->requirePresence('codigo', 'create')
                ->notEmpty('codigo');

        $validator
                ->scalar('nome')
                ->maxLength('nome', 300)
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        return $validator;
    }

}
