<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class AccountsTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('accounts');
        $this->setDisplayField('titular');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->value('ativa')
            ->like('titular', [
                'multiValue' => true,
                'before' => true,
                'after' => true,
                'field' => ['titular']
            ])
            ->value('bank_id')
            ->value('distributor_id');

        $this->belongsTo('Distributors', [
            'foreignKey' => 'distributor_id'
        ]);
        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id'
        ]);
        $this->belongsToMany('DistributorContacts', [
            'foreignKey' => 'account_id',
            'targetForeignKey' => 'distributor_contact_id',
            'joinTable' => 'accounts_distributor_contacts',
            'dependent' => true
        ]);
        $this->hasMany('SaleDetails', [
            'foreignKey' => 'account_id'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 1)
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->scalar('titular')
            ->maxLength('titular', 200)
            ->requirePresence('titular', 'create')
            ->notEmpty('titular');

        $validator
            ->scalar('numero_agencia')
            ->maxLength('numero_agencia', 20)
            ->requirePresence('numero_agencia', 'create')
            ->allowEmpty('numero_agencia');

        $validator
            ->scalar('numero_conta')
            ->maxLength('numero_conta', 20)
            ->requirePresence('numero_conta', 'create')
            ->allowEmpty('numero_conta');

        $validator
            ->scalar('nome_gerente')
            ->maxLength('nome_gerente', 200)
            ->requirePresence('nome_gerente', 'create')
            ->allowEmpty('nome_gerente');

        $validator
            ->scalar('telefone_gerente')
            ->maxLength('telefone_gerente', 20)
            ->requirePresence('telefone_gerente', 'create')
            ->allowEmpty('telefone_gerente');

        $validator
            ->scalar('email_gerente')
            ->maxLength('email_gerente', 200)
            ->requirePresence('email_gerente', 'create')
            ->allowEmpty('email_gerente');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['distributor_id'], 'Distributors'));
        $rules->add($rules->existsIn(['bank_id'], 'Banks'));

        return $rules;
    }
}
