<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * LedgerEntryTypes Model
 *
 * @method \App\Model\Entity\LedgerEntryType get($primaryKey, $options = [])
 * @method \App\Model\Entity\LedgerEntryType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\LedgerEntryType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\LedgerEntryType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\LedgerEntryType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\LedgerEntryType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\LedgerEntryType findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class LedgerEntryTypesTable extends Table {

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('ledger_entry_types');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
                ->like('descricao', [
                    'multiValue' => true,
                    'before' => true,
                    'after' => true,
                    'field' => ['descricao']]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('dc')
                ->maxLength('dc', 1)
                ->requirePresence('dc', 'create')
                ->notEmpty('dc');

        $validator
                ->scalar('descricao')
                ->maxLength('descricao', 200)
                ->requirePresence('descricao', 'create')
                ->notEmpty('descricao');

        return $validator;
    }

}
