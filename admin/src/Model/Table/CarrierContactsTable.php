<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class CarrierContactsTable extends Table {

    public function initialize(array $config) {
        parent::initialize($config);

        $this->setTable('carrier_contacts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Carriers', [
            'foreignKey' => 'carrier_id',
            'joinType' => 'INNER'
        ]);
    }

    public function validationDefault(Validator $validator) {
        $validator
                ->integer('id')
                ->allowEmpty('id', 'create');

        $validator
                ->scalar('nome')
                ->maxLength('nome', 100)
                ->requirePresence('nome', 'create')
                ->notEmpty('nome');

        $validator
                ->scalar('cpf')
                ->maxLength('cpf', 20)
                ->requirePresence('cpf', 'create')
                ->allowEmpty('cpf');

        $validator
                ->email('email')
                ->requirePresence('email', 'create')
                ->allowEmpty('email');

        $validator
                ->scalar('telefone')
                ->maxLength('telefone', 20)
                ->requirePresence('telefone', 'create')
                ->allowEmpty('telefone');

        $validator
                ->scalar('celular')
                ->maxLength('celular', 20)
                ->requirePresence('celular', 'create')
                ->allowEmpty('celular');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['carrier_id'], 'Carriers'));

        return $rules;
    }

}
