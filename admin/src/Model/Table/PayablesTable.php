<?php

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class PayablesTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('payables');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->addBehavior('Search.Search');
        $this->searchManager()
            ->value('baixa')
            ->value('tipo')
            ->compare('data_vencimento_de', [
                'operator' => '>=',
                'field' => 'data_vencimento'
            ])
            ->compare('data_vencimento_ate', [
                'operator' => '<=',
                'field' => 'data_vencimento'
            ])
            ->compare('data_referencia_de', [
                'operator' => '>=',
                'field' => 'created'
            ])
            ->compare('data_referencia_ate', [
                'operator' => '<=',
                'field' => 'created'
            ])
            ->add('seller_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('SaleDetails', function ($query) use ($args) {
                        return $query->matching('Presales', function ($query) use ($args) {
                            return $query->matching('Clients', function ($query) use ($args) {
                                return $query->where(['Clients.seller_id' => $args['seller_id']]);
                            });
                        });
                    });
                }
            ])
            ->add('carrier_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('Sales', function ($query) use ($args) {
                        return $query->where(['Sales.carrier_id' => $args['carrier_id']]);
                    });
                }
            ])
            ->add('carrier_transfer_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('PurchaseTransfers', function ($query) use ($args) {
                        return $query->where(['PurchaseTransfers.carrier_id' => $args['carrier_transfer_id']]);
                    });
                }
            ])
            ->add('broker_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('SalesPurchases', function ($query) use ($args) {
                        return $query->matching('Purchases', function ($query) use ($args) {
                            return $query->where(['Purchases.broker_id' => $args['broker_id']]);
                        });
                    });
                }
            ])
            ->add('broker_transfer_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('PurchaseTransfers', function ($query) use ($args) {
                        return $query->matching('Purchases', function ($query) use ($args) {
                            return $query->where(['Purchases.broker_id' => $args['broker_transfer_id']]);
                        });
                    });
                }
            ])
            ->add('distributor_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('SalesPurchases', function ($query) use ($args) {
                        return $query->matching('Purchases', function ($query) use ($args) {
                            return $query->where(['Purchases.distributor_id' => $args['distributor_id']]);
                        });
                    });
                }
            ])
            ->add('plant_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('PurchasePayments', function ($query) use ($args) {
                        return $query->matching('Purchases', function ($query) use ($args) {
                            return $query->where(['Purchases.plant_id' => $args['plant_id']]);
                        });
                    });
                }
            ])
            ->add('broker_purchase_id', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query->matching('SalesPurchases', function ($query) use ($args) {
                        return $query->where(['SalesPurchases.purchase_id' => $args['broker_purchase_id']]);
                    });
                }
            ]);

        $this->belongsTo('Sales', [
            'foreignKey' => 'sale_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SaleDetails', [
            'foreignKey' => 'sale_detail_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('PurchasePayments', [
            'foreignKey' => 'purchase_payment_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('SalesPurchases', [
            'foreignKey' => 'sales_purchases_id',
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('PurchaseTransfers', [
            'foreignKey' => 'purchase_transfer_id',
            'joinType' => 'LEFT'
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('tipo')
            ->maxLength('tipo', 2)
            ->requirePresence('tipo', 'create')
            ->notEmpty('tipo');

        $validator
            ->date('data_vencimento')
            ->allowEmpty('data_vencimento');

        $validator
            ->date('data_baixa')
            ->allowEmpty('data_baixa');

        $validator
            ->requirePresence('baixa', 'create')
            ->notEmpty('baixa');

        $validator
            ->numeric('valor')
            ->requirePresence('valor', 'create')
            ->notEmpty('valor');

        $validator
            ->scalar('descricao')
            ->maxLength('descricao', 200)
            ->requirePresence('descricao', 'create')
            ->notEmpty('descricao');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['purchase_payment_id'], 'PurchasePayments'));
        $rules->add($rules->existsIn(['sale_id'], 'Sales'));
        $rules->add($rules->existsIn(['sale_detail_id'], 'SaleDetails'));

        return $rules;
    }
}
