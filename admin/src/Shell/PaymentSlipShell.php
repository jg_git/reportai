<?php

namespace App\Shell;

use Cake\View\View;
use Cake\Http\Client;
use Cake\Console\Shell;
use Cake\ORM\TableRegistry;

class PaymentSlipShell extends Shell
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('PayamentSlips');
    }

    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        return $parser;
    }

    public function main()
    {
        $base_url = 'http://rbl.cbsistema.com.br/imagens';

        set_time_limit(8 * 60);
        $current_account = null;
        $access_tokens = [];

        $start = time();
        $loop = true;

        while ($loop) {
            $count = $this->PayamentSlips->find()->where([
                'is_registration_error' => 0,
                'is_print_error' => 0,
                'is_registered' => 0
            ])->count();

            if ($count == 0) {
                $loop = false;
                break;
            }

            $slips = $this->PayamentSlips->find()->where([
                'is_registration_error' => 0,
                'is_print_error' => 0,
                'is_registered' => 0
            ])
                ->contain([
                    'Accounts'
                ])
                ->limit(50);

            foreach ($slips as $slip) {
                if (time() - $start > 110) {
                    $loop = false;
                    break 2;
                }

                echo $slip->id . "\n";

                $account = $slip->account;

                if (!$account->client_id) {
                    $slip->is_registration_error = 1;
                    $slip->error_message = 'Conta não compatível para emissão de boletos.';
                    $slip->save();
                    continue;
                }

                $account_tokens = explode('-', $account->numero_conta);

                if ($current_account != $slip->account_id) {

                    $current_account = $slip->account_id;

                    if (!array_key_exists($slip->account_id, $access_tokens)) {
                        $http = new Client();
                        $response = $http->post(
                            'https://oauth.itau.com.br/identity/connect/token',
                            [
                                'scope' => 'readonly',
                                'grant_type' => 'client_credentials',
                                'client_id' => $account->client_id,
                                'client_secret' => $account->client_secret
                            ],
                            [
                                'headers' => [
                                    'Content-Type' => 'application/x-www-form-urlencoded'
                                ]
                            ]
                        );
                        $json = $response->json;
                        $access_tokens[$slip->account_id] = $json['access_token'];
                    }
                }

                if (!$slip->is_registered) {
                    $batch = TableRegistry::get('PayamentSlipBatches')->get($slip->payament_slip_batch_id);

                    // Register slip
                    $json = [
                        'tipo_ambiente' => 2, // 1 = Testes 2 = Produção
                        'tipo_registro' => 1,
                        'tipo_cobranca' => 1,
                        'tipo_produto' => '00006',
                        'subproduto' => '00008',
                        'beneficiario' => [
                            'cpf_cnpj_beneficiario' => $account->cnpj,
                            'agencia_beneficiario' => $account->numero_agencia,
                            'conta_beneficiario' => sprintf('%07s', $account_tokens[0]),
                            'digito_verificador_conta_beneficiario' => $account_tokens[1]
                        ],
                        'identificador_titulo_empresa' => '171294                   ',
                        'titulo_aceite' => 'S',
                        'pagador' => [
                            'cpf_cnpj_pagador' => $slip->cpf_cnpj_pagador,
                            'nome_pagador' => mb_strimwidth($slip->nome_pagador, 0, 30, ""),
                            'logradouro_pagador' => $slip->logradouro_pagador,
                            'cidade_pagador' => $slip->cidade_pagador,
                            'uf_pagador' => $slip->uf_pagador,
                            'cep_pagador' => $slip->cep_pagador
                        ],
                        'sacador_avalista' => [
                            'cpf_cnpj_sacador_avalista' => $slip->cpf_cnpj_sacador_avalista,
                            'nome_sacador_avalista' => mb_strimwidth($slip->nome_sacador_avalista, 0, 30, ""),
                            'logradouro_sacador_avalista' => $slip->logradouro_sacador_avalista,
                            'bairro_sacador_avalista' => $slip->bairro_sacador_avalista,
                            'cidade_sacador_avalista' => $slip->cidade_sacador_avalista,
                            'uf_sacador_avalista' => $slip->uf_sacador_avalista,
                            'cep_sacador_avalista' => $slip->cep_sacador_avalista
                        ],
                        'tipo_carteira_titulo' => $account->carteira,
                        'moeda' => [
                            'codigo_moeda_cnab' => '09'
                        ],
                        'nosso_numero' => $slip->nosso_numero,
                        'digito_verificador_nosso_numero' => (string) $slip->digito_verificador_nosso_numero,
                        'data_vencimento' => date('Y-m-d', strtotime(str_replace('/', '-', $slip->data_vencimento))),
                        'valor_cobrado' => $slip->valor_cobrado,
                        'seu_numero' => $slip->nota_fiscal,
                        'especie' => '99',
                        'data_emissao' => date('Y-m-d', strtotime(str_replace('/', '-', $slip->data_emissao))),
                        'tipo_pagamento' => 3,
                        'indicador_pagamento_parcial' => false,
                        'juros' => [
                            'tipo_juros' => 2,
                            'percentual_juros' => '000000015000',
                        ],
                        'multa' => [
                            'tipo_multa' => 2,
                            'percentual_multa' => '000000100000',
                        ],
                        'grupo_desconto' => [
                            [
                                'tipo_desconto' => 0
                            ]
                        ],
                        'recebimento_divergente' => [
                            'tipo_autorizacao_recebimento' => '3'
                        ]
                    ];

                    $slip->request = json_encode($json);
                    $this->PayamentSlips->save($slip);

                    $http = new Client();
                    $response = $http->post(
                        'https://gerador-boletos.itau.com.br/router-gateway-app/public/codigo_barras/registro',
                        json_encode($json),
                        [
                            'headers' => [
                                'Accept' => 'application/vnd.itau',
                                'access_token' => $access_tokens[$slip->account_id],
                                'itau-chave' => $account->itau_chave,
                                'identificador' => $account->cnpj,
                                'Content-Type' => 'application/json'
                            ]
                        ]
                    );

                    $status_code = $response->getStatusCode();
                    $json = $response->json;
                    $slip->status_code = $status_code;
                    $slip->response = $response->body;

                    if ($status_code == 200) {
                        $slip->is_registered = 1;
                        $slip->codigo_barras = $json['codigo_barras'];
                        $slip->numero_linha_digitavel = $json['numero_linha_digitavel'];
                    } else {
                        $slip->is_registration_error = 1;
                        if (array_key_exists('codigo', $json)) {
                            $slip->error_code = $json['codigo'];
                            $slip->error_message = $json['mensagem'];
                        }
                    }
                    $this->PayamentSlips->save($slip);

                    if ($status_code == 200) {
                        // Print slip
                        $taxa_boleto = 0;

                        $dadosboleto["nosso_numero"] = $slip->nosso_numero . $slip->nosso_numero_digito_verificador;
                        $dadosboleto["numero_documento"] = $slip->nota_fiscal;
                        $dadosboleto["data_vencimento"] = date("d/m/Y", strtotime(str_replace("/", "-", $slip->data_vencimento)));
                        // $dadosboleto["data_documento"] =  date("d/m/Y", str_replace("/", "-", strtotime($slip->data_emissao)));
                        $dadosboleto["data_documento"] =  date("d/m/Y");
                        $dadosboleto["data_processamento"] = date("d/m/Y");
                        $dadosboleto["valor_boleto"] = number_format(intval($slip->valor_cobrado) / 100 + $taxa_boleto, 2, ',', '');

                        $cnpj_sacado = $slip->cpf_cnpj_pagador;
                        $dadosboleto["sacado"] = $slip->nome_pagador . ' ' . substr($cnpj_sacado,  0, 8) . '/' . substr($cnpj_sacado, 8, 4) . '-' . substr($cnpj_sacado, 12, 2);
                        $dadosboleto["endereco1"] = $slip->logradouro_pagador;
                        $dadosboleto["endereco2"] = $slip->cidade_pagador . ' - ' . $slip->uf_pagador . ' - ' . $slip->cep_pagador;

                        $dadosboleto["demonstrativo1"] = "Pagamento referente a Nota Fiscal " . $slip->nota_fiscal;
                        $dadosboleto["demonstrativo2"] = "PREZADO CLIENTE EFETUAR O PAGAMENTO VIA BOLETO<br>" .
                            "NÃO UTILIZAR DOC, TED, TRANSFERÊNCIA OU DEPÓSITO BANCÁRIO COMO FORMAS DE PAGAMENTO<br>" .
                            "NOSSO SISTEMA NÃO IDENTIFICA ESTES PAGAMENTOS";
                        $dadosboleto["demonstrativo3"] = "";

                        $dadosboleto["instrucoes1"] = "- Após vencimento cobrar multa de 1%<br>- Após vencimento cobrar juros de 4,5% ao mês";
                        $dadosboleto["instrucoes2"] = "- Receber até 10 dias após o vencimento";
                        $dadosboleto["instrucoes3"] = "- AS INFORMAÇÕES DESTE BOLETO SÃO DE EXCLUSIVA RESPONSABILIDADE DO BENEFICIÁRIO<br>" .
                            "PREZADO CLIENTE EFETUAR O PAGAMENTO VIA BOLETO. NÃO UTILIZAR DOC, TED, TRANSFERÊNCIA OU<br>" .
                            "DEPÓSITO BANCÁRIO COMO FORMAS DE PAGAMENTO, NOSSO SISTEMA NÃO IDENTIFICA ESTES PAGAMENTOS";
                        $dadosboleto["instrucoes4"] = "Em caso de dúvidas entre em contato conosco: adm@costaebarros.com.br";

                        $dadosboleto["quantidade"] = "";
                        $dadosboleto["valor_unitario"] = "";
                        $dadosboleto["aceite"] = "";
                        $dadosboleto["especie"] = "R$";
                        $dadosboleto["especie_doc"] = "";

                        $dadosboleto["agencia"] = $account->numero_agencia;
                        $dadosboleto["conta"] = $account_tokens[0];
                        $dadosboleto["conta_dv"] = $account_tokens[1];
                        $dadosboleto["carteira"] = $account->carteira;

                        $dadosboleto["identificacao"] = "BoletoRBL - CB Sistema de Boletos";
                        $dadosboleto["cpf_cnpj"] = " 34530660/0001-04";
                        $dadosboleto["endereco"] = "Dr Leonce Pinheiro, 270";
                        $dadosboleto["cidade_uf"] = "Itapetininga / SP";
                        $dadosboleto["cedente"] = "GLOBAL CB R C COBRANCA EIRELI";

                        $codigobanco = "341";
                        $codigo_banco_com_dv = $this->geraCodigoBanco($codigobanco);
                        $nummoeda = "9";
                        $fator_vencimento = $this->fator_vencimento($dadosboleto["data_vencimento"]);
                        $valor = $this->formata_numero($dadosboleto["valor_boleto"], 10, 0, "valor");
                        $agencia = $this->formata_numero($dadosboleto["agencia"], 4, 0);
                        $conta = $this->formata_numero($dadosboleto["conta"], 5, 0);
                        $conta_dv = $this->formata_numero($dadosboleto["conta_dv"], 1, 0);
                        $carteira = $dadosboleto["carteira"];
                        $nnum = $this->formata_numero($dadosboleto["nosso_numero"], 8, 0);
                        $codigo_barras = $codigobanco . $nummoeda . $fator_vencimento . $valor . $carteira . $nnum . $this->modulo_10($agencia . $conta . $carteira . $nnum) . $agencia . $conta . $this->modulo_10($agencia . $conta) . '000';
                        $dv = $this->digitoVerificador_barra($codigo_barras);
                        $linha = substr($codigo_barras, 0, 4) . $dv . substr($codigo_barras, 4, 43);
                        $nossonumero = $carteira . '/' . $nnum . '-' . $this->modulo_10($agencia . $conta . $carteira . $nnum);
                        $agencia_codigo = $agencia . "/" . $conta . "-" . $this->modulo_10($agencia . $conta);

                        $dadosboleto["codigo_barras"] = $linha;
                        $dadosboleto["linha_digitavel"] = $this->monta_linha_digitavel($linha);
                        $dadosboleto["agencia_codigo"] = $agencia_codigo;
                        $dadosboleto["nosso_numero"] = $nossonumero;
                        $dadosboleto["codigo_banco_com_dv"] = $codigo_banco_com_dv;

                        $dadosboleto["nome_sacador_avalista"] = $slip->nome_sacador_avalista;
                        $cnpj = $slip->cpf_cnpj_sacador_avalista;
                        $dadosboleto["cnpj_sacador_avalista"] = substr($cnpj,  0, 8) . '/' . substr($cnpj, 8, 4) . '-' . substr($cnpj, 12, 2);

                        $barcode = $this->fbarcode($dadosboleto["codigo_barras"], $base_url);

                        $view = new View();
                        $content = $view->element('layout_sicredi', compact('dadosboleto', 'barcode', 'base_url'));

                        require_once(dirname(__FILE__) . '/html2pdf/html2pdf.class.php');

                        try {
                            $html2pdf = new \HTML2PDF('P', 'A4', 'pt', array(0, 0, 0, 0));
                            $html2pdf->pdf->SetDisplayMode('real');
                            $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
                            // $html2pdf->Output($slip->nome_pagador . '_' . $slip->data_emissao . '_boleto.pdf', 'D');
                            $html2pdf->Output(TMP . 'boletos' . DS . $slip->id . '.pdf', 'F');
                            $slip->is_printed = 1;
                        } catch (\HTML2PDF_exception $e) {
                            echo $e->getMessage();
                            $slip->is_print_error = 1;
                        }
                        $this->PayamentSlips->save($slip);
                    }
                }

                $success_count = $this->PayamentSlips->find()->where([
                    'payament_slip_batch_id' => $slip->payament_slip_batch_id,
                    'is_printed' => 1
                ])->count();

                $batch->num_printed = $success_count;

                $registration_error_count = $this->PayamentSlips->find()->where([
                    'payament_slip_batch_id' => $slip->payament_slip_batch_id,
                    'is_registration_error' => 1
                ])->count();

                $batch->num_registration_errors = $registration_error_count;

                $print_error_count = $this->PayamentSlips->find()->where([
                    'payament_slip_batch_id' => $slip->payament_slip_batch_id,
                    'is_print_error' => 1
                ])->count();

                $batch->num_print_errors = $print_error_count;

                TableRegistry::get('PayamentSlipBatches')->save($batch);

                if ($batch->num_requested == $success_count + $registration_error_count + $print_error_count) {
                    $ss = $this->PayamentSlips->find()->where([
                        'payament_slip_batch_id' => $slip->payament_slip_batch_id,
                        'is_printed' => 1
                    ]);

                    $filename = 'boletos_' . date('Ymd_His') . '.pdf';
                    $cmd = 'gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=' . WWW_ROOT . DS . 'boletos' . DS . $filename;
                    foreach ($ss as $s) {
                        $cmd .= ' ' . TMP . DS . 'boletos' . DS . $s->id . ".pdf";
                    }

                    shell_exec($cmd);

                    foreach ($ss as $s) {
                        unlink(TMP . DS . 'boletos' . DS . $s->id . '.pdf');
                    }

                    $batch->date_printed = date('Y-m-d H:i:s');
                    $batch->pdf_filename = $filename;
                    TableRegistry::get('PayamentSlipBatches')->save($batch);
                }
            }
        }
    }

    function digitoVerificador_barra($numero)
    {
        $resto2 = $this->modulo_11($numero, 9, 1);
        $digito = 11 - $resto2;
        if ($digito == 0 || $digito == 1 || $digito == 10  || $digito == 11) {
            $dv = 1;
        } else {
            $dv = $digito;
        }
        return $dv;
    }

    function formata_numero($numero, $loop, $insert, $tipo = "geral")
    {
        if ($tipo == "geral") {
            $numero = str_replace(",", "", $numero);
            while (strlen($numero) < $loop) {
                $numero = $insert . $numero;
            }
        }
        if ($tipo == "valor") {
            $numero = str_replace(",", "", $numero);
            while (strlen($numero) < $loop) {
                $numero = $insert . $numero;
            }
        }
        if ($tipo == "convenio") {
            while (strlen($numero) < $loop) {
                $numero = $numero . $insert;
            }
        }
        return $numero;
    }

    function fbarcode($valor, $base_url)
    {
        $fino = 1;
        $largo = 3;
        $altura = 50;
        $barcodes[0] = "00110";
        $barcodes[1] = "10001";
        $barcodes[2] = "01001";
        $barcodes[3] = "11000";
        $barcodes[4] = "00101";
        $barcodes[5] = "10100";
        $barcodes[6] = "01100";
        $barcodes[7] = "00011";
        $barcodes[8] = "10010";
        $barcodes[9] = "01010";
        for ($f1 = 9; $f1 >= 0; $f1--) {
            for ($f2 = 9; $f2 >= 0; $f2--) {
                $f = ($f1 * 10) + $f2;
                $texto = "";
                for ($i = 1; $i < 6; $i++) {
                    $texto .=  substr($barcodes[$f1], ($i - 1), 1) . substr($barcodes[$f2], ($i - 1), 1);
                }
                $barcodes[$f] = $texto;
            }
        }

        $code = "<img src='$base_url/p.png' width=$fino height=$altura border=0>" .
            "<img src='$base_url/b.png' width=$fino height=$altura border=0>" .
            "<img src='$base_url/p.png' width=$fino height=$altura border=0>" .
            "<img src='$base_url/b.png' width=$fino height=$altura border=0>" .
            "<img ";

        $texto = $valor;
        if ((strlen($texto) % 2) <> 0) {
            $texto = "0" . $texto;
        }
        while (strlen($texto) > 0) {
            $i = round($this->esquerda($texto, 2));
            $texto = $this->direita($texto, strlen($texto) - 2);
            $f = $barcodes[$i];
            for ($i = 1; $i < 11; $i += 2) {
                if (substr($f, ($i - 1), 1) == "0") {
                    $f1 = $fino;
                } else {
                    $f1 = $largo;
                }
                $code .= "src='$base_url/p.png' width=$f1 height=$altura border=0><img ";
                if (substr($f, $i, 1) == "0") {
                    $f2 = $fino;
                } else {
                    $f2 = $largo;
                }
                $code .= "src='$base_url/b.png' width=$f2 height=$altura border=0><img ";
            }
        }
        $code .= "src='$base_url/p.png' width=$largo height=$altura border=0>" .
            "<img src='$base_url/b.png' width=$fino height=$altura border=0>" .
            "<img src='$base_url/p.png' width=1 height=$altura border=0>";

        return $code;
    }

    function esquerda($entra, $comp)
    {
        return substr($entra, 0, $comp);
    }

    function direita($entra, $comp)
    {
        return substr($entra, strlen($entra) - $comp, $comp);
    }

    function fator_vencimento($data)
    {
        $data = explode("/", $data);
        $ano = $data[2];
        $mes = $data[1];
        $dia = $data[0];
        return (abs(($this->_dateToDays("1997", "10", "07")) - ($this->_dateToDays($ano, $mes, $dia))));
    }

    function _dateToDays($year, $month, $day)
    {
        $century = substr($year, 0, 2);
        $year = substr($year, 2, 2);
        if ($month > 2) {
            $month -= 3;
        } else {
            $month += 9;
            if ($year) {
                $year--;
            } else {
                $year = 99;
                $century--;
            }
        }
        return (floor((146097 * $century)    /  4) +
            floor((1461 * $year)        /  4) +
            floor((153 * $month +  2) /  5) +
            $day +  1721119);
    }

    function modulo_10($num)
    {
        $numtotal10 = 0;
        $fator = 2;
        // Separacao dos numeros
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo (falor 10)
            // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
            $temp = $numeros[$i] * $fator;
            $temp0 = 0;
            foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                $temp0 += $v;
            }
            $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
            // monta sequencia para soma dos digitos no (modulo 10)
            $numtotal10 += $parcial10[$i];
            if ($fator == 2) {
                $fator = 1;
            } else {
                $fator = 2; // intercala fator de multiplicacao (modulo 10)
            }
        }

        // Calculo do modulo 10
        $resto = $numtotal10 % 10;
        $digito = 10 - $resto;
        if ($resto == 0) {
            $digito = 0;
        }

        return $digito;
    }

    function modulo_11($num, $base = 9, $r = 0)
    {
        $soma = 0;
        $fator = 2;
        /* Separacao dos numeros */
        for ($i = strlen($num); $i > 0; $i--) {
            // pega cada numero isoladamente
            $numeros[$i] = substr($num, $i - 1, 1);
            // Efetua multiplicacao do numero pelo falor
            $parcial[$i] = $numeros[$i] * $fator;
            // Soma dos digitos
            $soma += $parcial[$i];
            if ($fator == $base) {
                // restaura fator de multiplicacao para 2
                $fator = 1;
            }
            $fator++;
        }
        /* Calculo do modulo 11 */
        if ($r == 0) {
            $soma *= 10;
            $digito = $soma % 11;
            if ($digito == 10) {
                $digito = 0;
            }
            return $digito;
        } elseif ($r == 1) {
            $resto = $soma % 11;
            return $resto;
        }
    }

    function monta_linha_digitavel($codigo)
    {
        // campo 1
        $banco    = substr($codigo, 0, 3);
        $moeda    = substr($codigo, 3, 1);
        $ccc      = substr($codigo, 19, 3);
        $ddnnum   = substr($codigo, 22, 2);
        $dv1      = $this->modulo_10($banco . $moeda . $ccc . $ddnnum);
        // campo 2
        $resnnum  = substr($codigo, 24, 6);
        $dac1     = substr($codigo, 30, 1); //$this->modulo_10($agencia.$conta.$carteira.$nnum);
        $dddag    = substr($codigo, 31, 3);
        $dv2      = $this->modulo_10($resnnum . $dac1 . $dddag);
        // campo 3
        $resag    = substr($codigo, 34, 1);
        $contadac = substr($codigo, 35, 6); //substr($codigo,35,5).$this->modulo_10(substr($codigo,35,5));
        $zeros    = substr($codigo, 41, 3);
        $dv3      = $this->modulo_10($resag . $contadac . $zeros);
        // campo 4
        $dv4      = substr($codigo, 4, 1);
        // campo 5
        $fator    = substr($codigo, 5, 4);
        $valor    = substr($codigo, 9, 10);

        $campo1 = substr($banco . $moeda . $ccc . $ddnnum . $dv1, 0, 5) . '.' . substr($banco . $moeda . $ccc . $ddnnum . $dv1, 5, 5);
        $campo2 = substr($resnnum . $dac1 . $dddag . $dv2, 0, 5) . '.' . substr($resnnum . $dac1 . $dddag . $dv2, 5, 6);
        $campo3 = substr($resag . $contadac . $zeros . $dv3, 0, 5) . '.' . substr($resag . $contadac . $zeros . $dv3, 5, 6);
        $campo4 = $dv4;
        $campo5 = $fator . $valor;

        return "$campo1 $campo2 $campo3 $campo4 $campo5";
    }

    function geraCodigoBanco($numero)
    {
        $parte1 = substr($numero, 0, 3);
        $parte2 = $this->modulo_11($parte1);
        return $parte1 . "-" . $parte2;
    }
}
