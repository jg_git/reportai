<?php
foreach ($menus as $menu) {
    if (is_null($menu['parent'])) {
        ?>
        <li class="nav-item dropdown mr-4">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <?= $menu['nome'] ?>
                <!--<i class="fa fa-xs fa-chevron-down" style="color: #159450"></i>-->
                <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <?php
                foreach ($menus as $submenu) {
                    if ($menu['id'] == $submenu['parent']) {
                        ?>
                        <div class="submenu"><?= $submenu['nome'] ?></div>
                        <?php
                        foreach ($role->modules as $module) {
                            if ($submenu['id'] == $module->menu_id) {
                                $link = explode(':', $module->menu_link);
                                ?>
                                <a href="<?= $this->Url->build(['controller' => $link[0], 'action' => $link[1]]) . $module->parameters ?>" class="dropdown-item"><?= $module->menu_display ?></a>
                                <?php
                            }
                        }
                    }
                }
                ?>
            </div>
        </li>
        <?php
    }
}
?>
