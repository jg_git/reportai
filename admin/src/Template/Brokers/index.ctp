<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Corretoras']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('razao_social', ['label' => 'Razão Social']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('nome_fantasia', ['label' => 'Nome Fantasia']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('cnpj', ['label' => 'CNPJ']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th>Razão Social</th>
                        <th>Nome Fantasia</th>
                        <th>CNPJ</th>
                    </tr>
                    <?php
                    if (sizeof($brokers) > 0) {
                        foreach ($brokers as $broker) {
                            ?>
                            <tr>
                                <td><?= $this->Html->link($broker->razao_social, ['action' => 'edit', $broker->id]) ?></td>
                                <td><?= $broker->nome_fantasia ?></td>
                                <td><?= $broker->cnpj ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    