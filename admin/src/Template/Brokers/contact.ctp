<script>
    $(document).ready(function () {
        $("#form-contact-<?= $anchor ?> input[name=cep]").inputmask("99999-999");
        $("#form-contact-<?= $anchor ?> input[name=cpf]").inputmask("999.999.999-99");
        $("#form-contact-<?= $anchor ?> input[name=telefone]").inputmask("(99) 9999-9999");
        $("#form-contact-<?= $anchor ?> input[name=celular]").inputmask("(99) 99999-9999");
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($contact, [
    'id' => 'form-contact-' . $anchor,
    'data-id' => $contact->isNew() ? 'new' : $contact->id,
    'data-error' => sizeof($contact->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <?= $this->Form->control('nome', ['type' => 'text', 'label' => 'Nome']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cpf', ['type' => 'text', 'label' => 'CPF']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('email', ['type' => 'text', 'label' => 'Email']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?= $this->Form->control('telefone', ['type' => 'text', 'label' => 'Telefone']) ?>
    </div>
    <div class="col-md-3">
        <?= $this->Form->control('celular', ['type' => 'text', 'label' => 'Celular']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
