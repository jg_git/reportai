<?=

$this->Form->control('Sales.' . $sale_index . '.driver_id', ['type' => 'select',
    'options' => $drivers,
    'label' => 'Motorista',
    'empty' => true,
    'data-sale-index' => $sale_index])
?>