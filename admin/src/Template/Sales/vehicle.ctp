<?php

$vehicle_options = [];
foreach ($vehicles as $vehicle) {
    $vehicle_options[$vehicle->id] = 'Placa: ' . $vehicle->placa;
}

echo $this->Form->control('Sales.' . $sale_index . '.vehicle_id', ['type' => 'select',
    'options' => $vehicle_options,
    'label' => 'Veículo',
    'empty' => true,
    'data-sale-index' => $sale_index]);
