<style>
    @font-face {
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
        src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
    }

    body {
        font-family: Verdana;
        font-size: 9px;
        margin-left: 0.0cm;
        margin-right: 0.0cm;
    }
</style>
<?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
<hr>

<h5>Resumo de Vendas</h5>
<h5>Data Venda: <?= $data ?></h5>
<?php
$print = explode(',', $sale_list);
$order = 1;
foreach ($sales as $sale) {
    if (in_array($sale->id, $print)) {
        ?>
        <div><?= $order++ ?></div>
        Transportadora: <?= $sale->carrier->razao_social ?> - 
        Motorista: <?= $sale->driver->nome ?> - 
        Veículo: <?= $sale->vehicle->placa ?> - 
        <?php
        if ($sale->vehicle->tipo != 'C') {
            $volume = $sale->vehicle->quantidade_litros;
        } else {
            $volume = 0;
            foreach ($sale->vehicle->carts as $cart) {
                $volume += $cart->quantidade_litros;
            }
        }
        ?>
        Volume Veículo: <?= number_format($volume, 0, ',', '.') ?> - 
        Frete: <?= $this->Number->currency($sale->frete) ?>
        <br>
        <?php $sale_purchase = $sale->sales_purchases[0] ?>
        Distribuidora: <?= $sale_purchase->purchase->distributor->razao_social ?> - Usina: <?= $sale_purchase->purchase->plant->razao_social ?><br>
        <div class="credit">Clientes</div>
        <table class="table report-row">
            <tr>
                <td width="55%">Razão Social</td>
                <td width="10%" class="money">Volume</td>
                <td width="15%" class="money">Preço</td>
                <td width="20%">Boletos</td>
                <td width="10%">NF</td>
            </tr>
            <?php
            if (sizeof($sale->sale_details) > 0) {
                foreach ($sale->sale_details as $detail) {
                    ?>
                    <tr>
                        <td><?= $detail->presale->client->razao_social ?></td>
                        <td class="money"><?= number_format($detail->volume, 0, ',', '.') ?></td>
                        <td class="money">R$ <?= number_format($detail->presale->preco_venda, 4, ',', '.') ?></td>
                        <td>
                            <?php
                            $msg = $detail->presale->data_pagamento_1;
                            if (!is_null($detail->presale->data_pagamento_2)) {
                                $msg .= ', ' . $detail->presale->data_pagamento_2;
                            }
                            if (!is_null($detail->presale->data_pagamento_3)) {
                                $msg .= ', ' . $detail->presale->data_pagamento_3;
                            }
                            if (!is_null($detail->presale->data_pagamento_4)) {
                                $msg .= ', ' . $detail->presale->data_pagamento_4;
                            }
                            echo $msg;
                            ?>
                        </td>
                        <td><?= $detail->nota_fiscal ?></td>
                    </tr>
                    <?php
                }
            } else {
                ?>
                <tr class="no-data-found">
                    <td colspan="10">Nenhum registro encontrado</td>
                </tr>
            <?php } ?>
        </table>
        <hr>
    <?php
    }
}
?>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>