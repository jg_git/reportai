<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {
        $("input[name*=data]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom",
            autoclose: true
        });

        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            if ($(this).attr("name") === "select_all") {
                $("input[name=sale_printout]").each(function () {
                    $(this).iCheck("check");
                });
            }
        }).on('ifUnchecked', function (e) {
            if ($(this).attr("name") === "select_all") {
                $("input[name=sale_printout]").each(function () {
                    $(this).iCheck("uncheck");
                });
            }
        });

        $("input[name=data_venda]").change(function () {
            data = $(this).val().split("/");
            d = new Date(data[2], data[1] - 1, data[0]);
            data_venda = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
            window.location = "<?= $this->Url->build(['controller' => 'Sales', 'action' => 'edit']) ?>/" + data_venda;
        });
// ===== Adiciona adiciona mascaras de valores e volumes =====
<?php
if (sizeof($sales) > 0) {
    foreach ($sales as $sale_index => $sale) {
        ?>
                $("#sales-<?= $sale_index ?>-frete")
                        .maskMoney({thousands: '.', decimal: ','})
                        .maskMoney('mask', <?= $sale->frete ?>);
        <?php
        foreach ($sale->purchases as $purchase_index => $purchase) {
            ?>
                    $("#sales-<?= $sale_index ?>-purchases-<?= $purchase_index ?>-joindata-spread")
                            .maskMoney({thousands: '.', decimal: ','})
                            .maskMoney('mask', <?= $purchase->_joinData->spread ?>);
            <?php
        }
        if (!is_null($sale->sale_details)) {
            if (sizeof($sale->sale_details) > 0) {
                foreach ($sale->sale_details as $detail_index => $detail) {
                    ?>
                            $("#sales-<?= $sale_index ?>-sale-details-<?= $detail_index ?>-volume")
                                    .maskMoney({thousands: '.', decimal: ',', precision: 0})
                                    .maskMoney('mask', <?= $detail->volume / 1000 ?>);
                    <?php
                    $found = false;
                    foreach ($detail->receivables as $receivable) {
                        if ($receivable->baixa) {
                            $found = true;
                        }
                    }
                    if ($found) {
                        ?>
                                $("div[id=detail-<?= $sale_index ?>-<?= $detail_index ?>]").find("input").prop("readOnly", true);
                                $("div[id=detail-<?= $sale_index ?>-<?= $detail_index ?>]").find("select").prop("disabled", true);
                                $("div[id=detail-<?= $sale_index ?>-<?= $detail_index ?>]").find(".fa-trash").remove();
                                $("#trash-sale-<?= $sale_index ?>").remove();
                        <?php
                    }
                }
            }
        }
    }
}
?>
// ===== Inicializa arrays para controle =====
        var distributors = [];
<?php
foreach ($distributors as $distributor) {
    ?>
            distributors.push({
                id: <?= $distributor->id ?>,
                spread: <?= $distributor->spread ?>
            });
    <?php
}
?>
        var carriers = [];
<?php
foreach ($carriers as $carrier) {
    ?>
            carriers.push({
                id: <?= $carrier->id ?>,
                frete: <?= $carrier->frete ?>
            });
    <?php
}
?>
        var vehicles = [];
<?php
foreach ($carriers as $carrier) {
    foreach ($carrier->vehicles as $vehicle) {
        if ($vehicle->tipo != 'C') {
            ?>
                    vehicles.push({
                        id: <?= $vehicle->id ?>,
                        volume_veiculo: <?= $vehicle->quantidade_litros ?>});
            <?php
        } else {
            $volume = 0;
            foreach ($vehicle->carts as $cart) {
                $volume += $cart->quantidade_litros;
            }
            ?>
                    vehicles.push({
                        id: <?= $vehicle->id ?>,
                        volume_veiculo: <?= $volume ?>});
            <?php
        }
    }
}
?>
        var purchases = [];
<?php
foreach ($purchases as $purchase) {
    ?>
            purchases.push({
                id: <?= $purchase->id ?>,
                distributor_id: <?= $purchase->distributor_id ?>,
                numero_contrato: "<?= $purchase->numero_contrato ?>",
                usina: "<?= $purchase->plant->nome_fantasia ?>",
                distribuidora: "<?= $purchase->distributor->nome_fantasia ?>",
                volume_comprado: <?= $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito ?>,
                volume_vendido: <?= $purchase->volume_vendido ?>,
                created: <?= strtotime(str_replace('/', '-', $purchase->created)) ?>
            });
    <?php
}
?>
<?php
if (!is_null($purchases_in_use)) {
    foreach ($purchases_in_use as $purchase) {
        ?>
                purchases.push({
                    id: <?= $purchase->id ?>,
                    distributor_id: <?= $purchase->distributor_id ?>,
                    numero_contrato: "<?= $purchase->numero_contrato ?>",
                    usina: "<?= $purchase->plant->nome_fantasia ?>",
                    distribuidora: "<?= $purchase->distributor->nome_fantasia ?>",
                    volume_comprado: <?= $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito ?>,
                    volume_vendido: <?= $purchase->volume_vendido ?>,
                    created: <?= strtotime(str_replace('/', '-', $purchase->created)) ?>
                });
        <?php
    }
}
?>
        var presales = [];
<?php
foreach ($presales as $presale) {
    ?>
            presales.push({
                id: <?= $presale->id ?>,
                client_id: <?= $presale->client_id ?>,
                nome_fantasia: "<?= $presale->client->nome_fantasia ?>",
                data_pedido: "<?= $presale->data_pedido ?>",
                volume_pedido: <?= $presale->volume_pedido ?>,
                volume_vendido: <?= $presale->volume_vendido ?>,
                preco_venda: <?= $presale->preco_venda ?>,
                preco_mercado: <?= $presale->preco_mercado ?>
            });
    <?php
}
?>
        var sales = [];
<?php
foreach ($sales as $sale_index => $sale) {
    ?>
            ps = [];
    <?php foreach ($sale->purchases as $purchase_index => $purchase) { ?>
                ps.push({
                    id: <?= $purchase->id ?>,
                    purchase_index: <?= $purchase_index ?>,
                    volume: <?= $purchase->_joinData->volume ?>
                });
    <?php } ?>
            ds = [];
    <?php foreach ($sale->sale_details as $detail_index => $detail) { ?>
                ds.push({
                    id: <?= $detail->id ?>,
                    detail_index: <?= $detail_index ?>,
                    volume: <?= $detail->volume ?>
                });
    <?php } ?>
            sales.push({
                id: <?= !$sale->isNew() ? $sale->id : 0 ?>,
                sale_index: <?= $sale_index ?>,
                carrier_id: <?= !$sale->isNew() ? $sale->carrier_id : 0 ?>,
                driver_id: <?= !$sale->isNew() ? $sale->driver_id : 0 ?>,
                vehicle_id: <?= !$sale->isNew() ? $sale->vehicle_id : 0 ?>,
                purchases: ps,
                details: ds,
                volume_total: <?= !$sale->isNew() ? $sale->volume_total : 0 ?>
            });
    <?php
}
?>
// ===== Escolha de transportadora =====
        $("#sales-list").on("change", "select[id$=carrier-id]", function () {
            sindex = $(this).data("sale-index");
            carrier_id = parseInt($(this).val());
            for (i = 0; i < sales.length; i++) {
                if (sales[i].sale_index === sindex) {
                    sales[i].carrier_id = carrier_id;
                    break;
                }
            }
            $("#driver-spinner-" + sindex).show();
            $("#driver-" + sindex).hide();
            $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'driver']) ?>/" + $(this).val() + "/" + sindex,
                    function (data) {
                        $("#driver-spinner-" + sindex).hide();
                        $("#driver-" + sindex).show();
                        $("#driver-" + sindex).html(data);
                    });
            $("#vehicle-spinner-" + sindex).show();
            $("#vehicle-" + sindex).hide();
            $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'vehicle']) ?>/" + $(this).val() + "/" + sindex,
                    function (data) {
                        $("#vehicle-spinner-" + sindex).hide();
                        $("#vehicle-" + sindex).show();
                        $("#vehicle-" + sindex).html(data);
                    });
        });
// ===== Escolha de veiculo =====
        $("#sales-list").on("change", "select[id$=vehicle-id]", function () {
            sindex = $(this).data("sale-index");
            vehicle_id = parseInt($(this).val());
            vehicle = vehicles.find(x => x.id === vehicle_id);
            $("#capacidade-" + sindex).html(formatMoney(vehicle.volume_veiculo, 0));
            for (n = 0; n < sales.length; n++) {
                if (sales[n].sale_index === sindex) {
                    sales[n].vehicle_id = vehicle_id;
                    break;
                }
            }
        });
// ===== Adiciona nova venda =====
        var sale_index = <?= sizeof($sales) ?>;
        $("#button-add-sale").click(function () {
            $("#no-data").hide();
            $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'newSale']) ?>/" + sale_index,
                    function (data) {
                        $("#sales-list").append(data);
                        $("#sales-" + sale_index + "-data-venda").val("<?= $data_venda ?>");
                        $("#sales-" + sale_index + "-frete")
                                .maskMoney({thousands: '.', decimal: ','})
                                .maskMoney('mask', 0);
                        sales.push({
                            id: "new",
                            sale_index: sale_index,
                            carrier_id: 0,
                            driver_id: 0,
                            vehicle_id: 0,
                            purchases: [],
                            details: [],
                            volume_total: 0
                        });
                        sale_index++;
                    });
        });
// ===== Adiciona contrato (estoque) =====
        $("tr[data-purchase-id]").click(function () {
            sindex = parseInt($("#sale-index").val());
            pindex = parseInt($("#purchase-index-" + sindex).val());
            purchase_id = $(this).data("purchase-id");
            if (sales.find(x => x.sale_index === sindex).purchases.find(x => x.id === purchase_id) === undefined) {
                purchase = purchases.find(x => x.id === purchase_id);
                sales.find(x => x.sale_index === sindex).purchases.push({id: purchase.id, purchase_index: pindex, volume: 0});
                $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'newPurchase']) ?>/" + sindex + "/" + pindex + "/" + purchase_id,
                        function (data) {
                            $("#purchases-" + sindex).append(data);
                            $("#sales-" + sindex + "-purchases-" + pindex + "-joindata-volume")
                                    .maskMoney({thousands: '.', decimal: ',', precision: 0})
                                    .maskMoney('mask', 0);
                        });
                $("#purchase-index-" + sindex).val(++pindex);
                if (sales.find(x => x.sale_index === sindex).vehicle_id !== 0) {
                    $("#button-presales-" + sindex).prop("disabled", false);
                }
                $(".modal").modal('hide');
            } else {
                swal({
                    title: "Contrato já adicionado a essa venda",
                    confirmButtonColor: "#079dff",
                    confirmButtonText: "OK",
                    confirmButtonClass: "btn btn-lg btn-primary",
                    buttonsStyling: false
                });
            }
        });
// ===== Adiciona detalhe de venda =====
        $("#presales-table").on("click", "tr[data-presale-id]", function () {
            sindex = parseInt($("#sale-index").val());
            dindex = parseInt($("#detail-index-" + sindex).val());
            presale_id = $(this).data("presale-id");
            $("#no-data-" + sindex).hide();

            presale = presales.find(x => x.id === presale_id);
            presale_volume_restante = presale.volume_pedido - presale.volume_vendido;
            sale = sales.find(x => x.sale_index === sindex);

            if (presale_volume_restante <= (vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo - sale.volume_total)) {

                // verifica se todos os contratos tem volume suficiente para completar esta venda
                volume_disponivel_contratos = 0;
                for (i = 0; i < sale.purchases.length; i++) {
                    p = purchases.find(x => x.id === sale.purchases[i].id);
                    volume_disponivel_contratos += p.volume_comprado - p.volume_vendido;
                }

                if (volume_disponivel_contratos >= presale_volume_restante) {
                    $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'newDetail']) ?>/" + sindex + "/" + dindex + "/" + presale_id,
                            function (data) {
                                $("#details-" + sindex).append(data);
                                // mostra dados do detalhe de venda
                                $("#client-" + sindex + "-" + dindex).html(presale.nome_fantasia);
                                $("#sales-" + sindex + "-sale-details-" + dindex + "-presale-id").val(presale_id);
                                $("#sales-" + sindex + "-sale-details-" + dindex + "-volume").val(presale_volume_restante);
                                $("#sales-" + sindex + "-sale-details-" + dindex + "-volume")
                                        .maskMoney({thousands: '.', decimal: ',', precision: 0})
                                        .maskMoney('mask', presale_volume_restante / 1000);

                                volume_pedido = presale_volume_restante;

                                while (volume_pedido > 0) {
                                    // procura contrato mais antigo para subtrair o volume deste detalhe de venda
                                    ps = null;
                                    for (i = 0; i < sale.purchases.length; i++) {
                                        p = purchases.find(x => x.id === sale.purchases[i].id);
                                        if (p.volume_comprado - p.volume_vendido > 0) {
                                            if (ps !== null) {
                                                if (p.created < purchases.find(x => x.id === ps.id).created) {
                                                    ps = sale.purchases[i];
                                                }
                                            } else {
                                                ps = sale.purchases[i];
                                            }
                                        }
                                    }

                                    // ajusta estoque no contrato
                                    pp = purchases.find(x => x.id === ps.id);
                                    if (pp.volume_comprado - pp.volume_vendido > volume_pedido) {
                                        volume_agora = volume_pedido;
                                    } else {
                                        volume_agora = pp.volume_comprado - pp.volume_vendido;
                                    }
                                    pp.volume_vendido += volume_agora;
                                    estoque = pp.volume_comprado - pp.volume_vendido;

                                    // ajusta o volume no contrato selecionado
                                    ps.volume += volume_agora;
                                    $("#purchase-volume-" + sindex + "-" + ps.purchase_index).html(formatMoney(ps.volume, 0));
                                    $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-volume").val(ps.volume);

                                    // mostra o estoque atual em todas as vendas que tem esse contrato
                                    for (i = 0; i < sales.length; i++) {
                                        p = sales[i].purchases.find(x => x.id === ps.id);
                                        if (p !== undefined) {
                                            $("#estoque-" + sales[i].sale_index + "-" + p.purchase_index).html(formatMoney(estoque, 0));
                                        }
                                    }

                                    // ajusta o valor de spread
                                    spread = ps.volume * distributors.find(x => x.id === pp.distributor_id).spread;
                                    $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-spread").val(spread);
                                    $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-spread")
                                            .maskMoney({thousands: '.', decimal: ',', precision: 2})
                                            .maskMoney('mask', spread);


                                    // mostra o estoque ajustado na lista de contratos
                                    $("#volume-disponivel-" + ps.id).html(formatMoney(estoque, 0));

                                    volume_pedido -= volume_agora;
                                }

                                // ajusta a tabela de pre-vendas
                                presale.volume_vendido += presale_volume_restante;
                                if (presale.volume_pedido - presale.volume_vendido === 0) {
                                    i = 0;
                                    while (presales[i].id !== presale.id) {
                                        i++;
                                    }
                                    presales.splice(i, 1);
                                    $("tr[data-presale-id=" + presale_id + "]").remove();
                                    if ($("tr[data-presale-id]").length === 0) {
                                        $("#presales-done").show();
                                    }
                                } else {
                                    $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-vendido]").html(formatMoney(presale.volume_vendido, 0));
                                    $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-restante]").html(formatMoney(presale.volume_pedido - presale.volume_vendido, 0));
                                }

                                // mostra volume total da venda
                                sale.volume_total += presale_volume_restante;
                                sale.details.push({
                                    id: "new",
                                    detail_index: dindex,
                                    volume: presale_volume_restante
                                });
                                $("#volume-total-" + sindex).removeClass("red blue");
                                $("#volume-total-" + sindex).addClass((vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo > sale.volume_total) ? "red" : "blue");
                                $("#volume-total-" + sindex).html(formatMoney(sale.volume_total, 0));
                                $("#sales-" + sindex + "-volume-total").val(sale.volume_total);

                                // ajusta o valor de frete
                                frete = sale.volume_total * carriers.find(x => x.id === sale.carrier_id).frete;
                                $("#sales-" + sindex + "-frete").val(frete);

                                $("#detail-index-" + sindex).val(++dindex);
                            });
                } else {
                    swal({
                        title: "Estoque insuficiente",
                        text: "Estoque disponível em todos os contratos não é suficiente para completar essa pré-venda. Adicione mais um contrato.",
                        confirmButtonColor: "#079dff",
                        confirmButtonText: "OK",
                        confirmButtonClass: "btn btn-lg btn-primary",
                        buttonsStyling: false
                    });
                }
            } else {
                swal({
                    title: "Volume Excedente",
                    text: "Volume da pré venda excede o volume restante do veículo",
                    showCancelButton: true,
                    confirmButtonColor: "#079dff",
                    cancelButtonColor: "lightgray",
                    confirmButtonText: "Adicionar com volume disponível",
                    cancelButtonText: "Cancelar",
                    confirmButtonClass: "btn btn-lg btn-primary",
                    cancelButtonClass: "btn btn-lg btn-outline-dark mr-3",
                    buttonsStyling: false,
                    reverseButtons: true,
                    allowOutsideClick: false
                }).then((result) => {
                    if (result.value) {
                        // verifica se todos os contratos tem volume suficiente para completar esta venda
                        volume_disponivel_contratos = 0;
                        for (i = 0; i < sale.purchases.length; i++) {
                            p = purchases.find(x => x.id === sale.purchases[i].id);
                            volume_disponivel_contratos += p.volume_comprado - p.volume_vendido;
                        }

                        if (volume_disponivel_contratos >= presale_volume_restante) {
                            volume_agora = vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo - sale.volume_total;

                            $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'newDetail']) ?>/" + sindex + "/" + dindex + "/" + presale_id,
                                    function (data) {
                                        $("#details-" + sindex).append(data);
                                        // mostra dados do detalhe de venda
                                        $("#client-" + sindex + "-" + dindex).html(presale.nome_fantasia);
                                        $("#sales-" + sindex + "-sale-details-" + dindex + "-presale-id").val(presale_id);
                                        $("#sales-" + sindex + "-sale-details-" + dindex + "-volume").val(volume_agora);
                                        $("#sales-" + sindex + "-sale-details-" + dindex + "-volume")
                                                .maskMoney({thousands: '.', decimal: ',', precision: 0})
                                                .maskMoney('mask', volume_agora / 1000);

                                        volume_pedido = volume_agora;

                                        while (volume_pedido > 0) {
                                            // procura contrato mais antigo para subtrair o volume deste detalhe de venda
                                            ps = null;
                                            for (i = 0; i < sale.purchases.length; i++) {
                                                p = purchases.find(x => x.id === sale.purchases[i].id);
                                                if (p.volume_comprado - p.volume_vendido > 0) {
                                                    if (ps !== null) {
                                                        if (p.created < purchases.find(x => x.id === ps.id).created) {
                                                            ps = sale.purchases[i];
                                                        }
                                                    } else {
                                                        ps = sale.purchases[i];
                                                    }
                                                }
                                            }

                                            // ajusta estoque no contrato
                                            pp = purchases.find(x => x.id === ps.id);
                                            if (pp.volume_comprado - pp.volume_vendido > volume_pedido) {
                                                volume_agora = volume_pedido;
                                            } else {
                                                volume_agora = pp.volume_comprado - pp.volume_vendido;
                                            }
                                            pp.volume_vendido += volume_agora;
                                            estoque = pp.volume_comprado - pp.volume_vendido;

                                            // ajusta o volume no contrato selecionado
                                            ps.volume += volume_agora;
                                            $("#purchase-volume-" + sindex + "-" + ps.purchase_index).html(formatMoney(ps.volume, 0));
                                            $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-volume").val(ps.volume);

                                            // mostra o estoque atual em todas as vendas que tem esse contrato
                                            for (i = 0; i < sales.length; i++) {
                                                p = sales[i].purchases.find(x => x.id === ps.id);
                                                if (p !== undefined) {
                                                    $("#estoque-" + sales[i].sale_index + "-" + p.purchase_index).html(formatMoney(estoque, 0));
                                                }
                                            }

                                            // mostra o estoque ajustado na lista de contratos
                                            $("#volume-disponivel-" + ps.id).html(formatMoney(estoque, 0));

                                            volume_pedido -= volume_agora;
                                            sale.volume_total += volume_agora;

                                            // ajusta o valor de spread
                                            spread = ps.volume * distributors.find(x => x.id === pp.distributor_id).spread;
                                            $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-spread").val(spread);
                                        }

                                        // escode a prevenda escolhida e caso nao haja mais mostra mensagem 
                                        presale.volume_vendido += vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo - sale.volume_total;
                                        if (presale.volume_pedido - presale.volume_vendido === 0) {
                                            i = 0;
                                            while (presales[i].id !== presale.id) {
                                                i++;
                                            }
                                            presales.splice(i, 1);
                                            $("tr[data-presale-id=" + presale_id + "]").remove();
                                            if ($("tr[data-presale-id]").length === 0) {
                                                $("#presales-done").show();
                                            }
                                        } else {
                                            $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-vendido]").html(formatMoney(presale.volume_vendido, 0));
                                            $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-restante]").html(formatMoney(presale.volume_pedido - presale.volume_vendido, 0));
                                        }

                                        // mostra volume total da venda
                                        sale.details.push({
                                            id: "new",
                                            detail_index: dindex,
                                            volume: volume_agora
                                        });
                                        $("#volume-total-" + sindex).removeClass("red blue");
                                        $("#volume-total-" + sindex).addClass((vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo > sale.volume_total) ? "red" : "blue");
                                        $("#volume-total-" + sindex).html(formatMoney(sale.volume_total, 0));
                                        $("#sales-" + sindex + "-volume-total").val(sale.volume_total);

                                        // ajusta o valor de frete
                                        frete = sale.volume_total * carriers.find(x => x.id === sale.carrier_id).frete;
                                        $("#sales-" + sindex + "-frete").val(frete);

                                        $("#detail-index-" + sindex).val(++dindex);
                                    });
                        } else {
                            swal({
                                title: "Estoque insuficiente",
                                text: "Estoque disponível em todos os contratos não é suficiente para completar essa pré-venda. Adicione mais um contrato.",
                                confirmButtonColor: "#079dff",
                                confirmButtonText: "OK",
                                confirmButtonClass: "btn btn-lg btn-primary",
                                buttonsStyling: false
                            });
                        }
                    }
                });
            }
            $(".modal").modal('hide');
        });
// ===== Remove detalhe de venda =====
        $("#sales-list").on("click", "i[data-detail-anchor]", function () {
            anchor = $(this).data("detail-anchor");
            sindex = $(this).data("sale-index");
            dindex = $(this).data("detail-index");

            volume = parseInt($("div[data-detail-anchor=" + anchor + "]").find("input[id$=volume]").val().replace(/\./g, "").replace(",", "."));
            presale_id = parseInt($("div[data-detail-anchor=" + anchor + "]").find("input[id$=presale-id]").val());

            sale = sales.find(x => x.sale_index === sindex);
            sale.volume_total -= volume;

            // ajusta o frete
            frete = sale.volume_total * carriers.find(x => x.id === sale.carrier_id).frete;
            $("#sales-" + sindex + "-frete").val(frete);

            // mostra o volume total da venda
            $("#volume-total-" + sindex).removeClass("red blue");
            $("#volume-total-" + sindex).addClass((vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo > sale.volume_total) ? "red" : "blue");
            $("#volume-total-" + sindex).html(formatMoney(sale.volume_total, 0));
            $("#sales-" + sindex + "-volume-total").val(sale.volume_total);

            // procura contrato mais recente para adicionar o volume deste detalhe de venda
            ps = null;
            for (i = 0; i < sale.purchases.length; i++) {
                if (sale.purchases[i].volume >= volume) {
                    if (ps !== null) {
                        if (purchases.find(x => x.id === sale.purchases[i].id).created > purchases.find(x => x.id === ps.id).created) {
                            ps = sale.purchases[i];
                        }
                    } else {
                        ps = sale.purchases[i];
                    }
                }
            }

            // ajusta o volume no contrato selecionado
            ps.volume -= volume;
            $("#purchase-volume-" + sindex + "-" + ps.purchase_index).html(formatMoney(ps.volume, 0));
            $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-volume").val(ps.volume);

            // ajusta estoque no contrato
            pp = purchases.find(x => x.id === ps.id);
            pp.volume_vendido -= volume;
            estoque = pp.volume_comprado - pp.volume_vendido;

            // mostra o estoque atual em todas as vendas que tem esse contrato
            for (i = 0; i < sales.length; i++) {
                p = sales[i].purchases.find(x => x.id === ps.id);
                if (p !== undefined) {
                    $("#estoque-" + sales[i].sale_index + "-" + p.purchase_index).html(formatMoney(estoque, 0));
                }
            }

            // mostra o estoque ajustado na lista de contratos
            $("#volume-disponivel-" + ps.id).html(formatMoney(estoque, 0));

            // ajusta o valor de spread
            spread = ps.volume * distributors.find(x => x.id === pp.distributor_id).spread;
            $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-spread").val(spread);

            // adiciona pre venda para a tabela de pre vendas disponiveis
            presale = presales.find(x => x.id === presale_id);
            if (presale === undefined) {
                $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'presale']) ?>/" + presale_id,
                        function (data, code) {
                            if (checkstatus(code)) {
                                presales.push({
                                    id: data.id,
                                    client_id: data.client_id,
                                    nome_fantasia: data.nome_fantasia,
                                    data_pedido: data.data_pedido,
                                    volume_pedido: data.volume_pedido,
                                    volume_vendido: data.volume_vendido - volume,
                                    preco_venda: data.preco_venda,
                                    preco_mercado: data.preco_mercado
                                });
                                presale = presales.find(x => x.id === presale_id);
                                found = false;
                                $("tr[data-presale-id]").each(function () {
                                    if (presale.data_pedido <= $(this).data("data-pedido")) {
                                        $(this).before(
                                                '<tr data-presale-id="' + presale.id + '">' +
                                                '<td style="text-align: center">' + convertDateDMY(presale.data_pedido) + '</td>' +
                                                '<td>' + presale.nome_fantasia + '</td>' +
                                                '<td>' + formatMoney(presale.preco_venda, 0) + '</td>' +
                                                '<td class="money">' + formatMoney(presale.volume_pedido, 0) + '</td>' +
                                                '<td id="presale-volume-vendido" class="money">' + formatMoney(presale.volume_vendido, 0) + '</td>' +
                                                '<td id="presale-volume-restante" class="money">' + formatMoney(presale.volume_pedido - presale.volume_vendido, 0) + '</td>' +
                                                '</tr>'
                                                );
                                        found = true;
                                        return false;
                                    }
                                });
                                if (!found) {
                                    $("#presales-table").append(
                                            '<tr data-presale-id="' + presale.id + '">' +
                                            '<td style="text-align: center">' + convertDateDMY(presale.data_pedido) + '</td>' +
                                            '<td>' + presale.nome_fantasia + '</td>' +
                                            '<td>' + formatMoney(presale.preco_venda, 0) + '</td>' +
                                            '<td class="money">' + formatMoney(presale.volume_pedido, 0) + '</td>' +
                                            '<td id="presale-volume-vendido" class="money">' + formatMoney(presale.volume_vendido, 0) + '</td>' +
                                            '<td id="presale-volume-restante" class="money">' + formatMoney(presale.volume_pedido - presale.volume_vendido, 0) + '</td>' +
                                            '</tr>'
                                            );
                                }
                                $("#presales-no-data-found").hide();
                                $("#presales-done").hide();
                                detail_id = sale.details.find(x => x.detail_index === dindex).id;
                                if (detail_id !== "new") {
                                    $("#delete-detail-list").append("<option value='" + detail_id + "' selected></option>");
                                }
                                $("div[data-detail-anchor=" + anchor + "]").remove();
                            }
                        });
            } else {
                presale.volume_vendido -= volume;
                $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-vendido]").html(formatMoney(presale.volume_vendido, 0));
                $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-restante]").html(formatMoney(presale.volume_pedido - presale.volume_vendido, 0));
                $("#presales-no-data-found").hide();
                $("#presales-done").hide();
                detail_id = sale.details.find(x => x.detail_index === dindex).id;
                if (detail_id !== "new") {
                    $("#delete-detail-list").append("<option value='" + detail_id + "' selected></option>");
                }
                $("div[data-detail-anchor=" + anchor + "]").remove();
            }

        });
// ===== Remove venda =====
        $("#sales-list").on("click", "i[data-sale-anchor]", function () {
            sale_anchor = $(this).data("sale-anchor");
            sindex = $(this).data("sale-index");

            volume = 0;
            $("div[data-sale-anchor=" + sale_anchor + "]").find("i[data-detail-anchor]").each(function () {
                detail_anchor = $(this).data("detail-anchor");
                volume += parseInt($("div[data-detail-anchor=" + detail_anchor + "]").find("input[id$=volume]").val().replace(/\./g, "").replace(",", "."));
                presale_id = parseInt($("div[data-detail-anchor=" + detail_anchor + "]").find("input[id$=presale-id]").val());
                // adiciona pre venda para a tabela de pre vendas disponiveis
                presale = presales.find(x => x.id === presale_id);
                if (presale === undefined) {
                    $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'presale']) ?>/" + presale_id,
                            function (data, code) {
                                if (checkstatus(code)) {
                                    presales.push({
                                        id: data.id,
                                        client_id: data.client_id,
                                        nome_fantasia: data.nome_fantasia,
                                        data_pedido: data.data_pedido,
                                        volume_pedido: data.volume_pedido,
                                        volume_vendido: data.volume_vendido - volume,
                                        preco_venda: data.preco_venda,
                                        preco_mercado: data.preco_mercado
                                    });
                                    presale = presales.find(x => x.id === presale_id);

                                    found = false;
                                    $("tr[data-presale-id]").each(function () {
                                        if (data.data_pedido <= $(this).data("data-pedido")) {
                                            $(this).before(
                                                    '<tr data-presale-id="' + presale.id + '">' +
                                                    '<td style="text-align: center">' + convertDateDMY(presale.data_pedido) + '</td>' +
                                                    '<td>' + presale.nome_fantasia + '</td>' +
                                                    '<td>' + formatMoney(presale.preco_venda, 0) + '</td>' +
                                                    '<td class="money">' + formatMoney(presale.volume_pedido, 0) + '</td>' +
                                                    '<td id="presale-volume-vendido" class="money">' + formatMoney(presale.volume_vendido, 0) + '</td>' +
                                                    '<td id="presale-volume-restante" class="money">' + formatMoney(presale.volume_pedido - presale.volume_vendido, 0) + '</td>' +
                                                    '</tr>'
                                                    );
                                            found = true;
                                            return false;
                                        }
                                    });
                                    if (!found) {
                                        $("#presales-table").append(
                                                '<tr data-presale-id="' + presale.id + '">' +
                                                '<td style="text-align: center">' + convertDateDMY(presale.data_pedido) + '</td>' +
                                                '<td>' + presale.nome_fantasia + '</td>' +
                                                '<td>' + formatMoney(presale.preco_venda, 0) + '</td>' +
                                                '<td class="money">' + formatMoney(presale.volume_pedido, 0) + '</td>' +
                                                '<td id="presale-volume-vendido" class="money">' + formatMoney(presale.volume_vendido, 0) + '</td>' +
                                                '<td id="presale-volume-restante" class="money">' + formatMoney(presale.volume_pedido - presale.volume_vendido, 0) + '</td>' +
                                                '</tr>'
                                                );
                                    }

                                    $("#presales-no-data-found").hide();
                                    $("#presales-done").hide();
                                }
                            });
                } else {
                    presale.volume_vendido -= volume;
                    $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-vendido]").html(formatMoney(presale.volume_vendido, 0));
                    $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-restante]").html(formatMoney(presale.volume_pedido - presale.volume_vendido, 0));
                }
            });

            sale = sales.find(x => x.sale_index === sindex);
            if (sale.id !== "new") {
                $("#delete-sale-list").append("<option value='" + sale.id + "' selected></option>");
            }
            ps = sale.purchases;
            for (n = 0; n < ps.length; n++) {
                purchase = purchases.find(x => x.id === ps[n].id);
                purchase.volume_vendido -= volume;
                estoque = purchase.volume_comprado - purchase.volume_vendido;
                // mostra o estoque atual em todas as vendas que tem esse contrato
                for (i = 0; i < sales.length; i++) {
                    p = sales[i].purchases.find(x => x.id === ps[n].id);
                    if (p !== undefined) {
                        $("#estoque-" + sales[i].sale_index + "-" + p.purchase_index).html(formatMoney(estoque, 0));
                    }
                }
                // mostra estoque ajustado na lista de contratos
                $("#volume-disponivel-" + purchase.id).html(formatMoney(estoque, 0));
            }
            // remove venda do array
            for (n = 0; n < sales.length; n++) {
                if (sales[n].sale_index === sindex) {
                    sales.splice(n, 1);
                    break;
                }
            }

            $("div[data-sale-anchor=" + sale_anchor + "]").remove();
        });
// ===== Modifica volume de pre vendas =====
        $("#sales-list").on("change", "input[id$=volume]", function () {
            sindex = $(this).data("sale-index");
            dindex = $(this).data("detail-index");
            presale_id = $(this).data("presale-id");
            volume = parseInt($(this).val().replace(/\./g, "").replace(",", "."));
            sale = sales.find(x => x.sale_index === sindex);

            detail = sale.details.find(x => x.detail_index === dindex);
            adjustment = volume - detail.volume;
            detail.volume = volume;

            sale.volume_total += adjustment;

            // ajusta o frete
            frete = sale.volume_total * carriers.find(x => x.id === sale.carrier_id).frete;
            $("#sales-" + sindex + "-frete").val(frete);

            // mostra o volume total da venda
            $("#volume-total-" + sindex).removeClass("red blue");
            $("#volume-total-" + sindex).addClass((vehicles.find(x => x.id === sale.vehicle_id).volume_veiculo > sale.volume_total) ? "red" : "blue");
            $("#volume-total-" + sindex).html(formatMoney(sale.volume_total, 0));
            $("#sales-" + sindex + "-volume-total").val(sale.volume_total);

            // procura contrato mais recente para adicionar o volume deste detalhe de venda
            ps = null;
            for (i = 0; i < sale.purchases.length; i++) {
                if (sale.purchases[i].volume >= volume) {
                    if (ps !== null) {
                        if (purchases.find(x => x.id === sale.purchases[i].id).created > purchases.find(x => x.id === ps.id).created) {
                            ps = sale.purchases[i];
                        }
                    } else {
                        ps = sale.purchases[i];
                    }
                }
            }

            // ajusta o volume no contrato selecionado
            ps.volume += adjustment;
            $("#purchase-volume-" + sindex + "-" + ps.purchase_index).html(formatMoney(ps.volume, 0));
            $("#sales-" + sindex + "-purchases-" + ps.purchase_index + "-joindata-volume").val(ps.volume);

            // ajusta estoque no contrato
            pp = purchases.find(x => x.id === ps.id);
            pp.volume_vendido += adjustment;
            estoque = pp.volume_comprado - pp.volume_vendido;

            // mostra o estoque atual em todas as vendas que tem esse contrato
            for (i = 0; i < sales.length; i++) {
                p = sales[i].purchases.find(x => x.id === ps.id);
                if (p !== undefined) {
                    $("#estoque-" + sales[i].sale_index + "-" + p.purchase_index).html(formatMoney(estoque, 0));
                }
            }

            // mostra o estoque ajustado na lista de contratos
            $("#volume-disponivel-" + ps.id).html(formatMoney(estoque, 0));

            // adiciona pre venda para a tabela de pre vendas disponiveis
            presale = presales.find(x => x.id === presale_id);
            if (presale === undefined) {
                $.get("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'presale']) ?>/" + presale_id,
                        function (data, code) {
                            if (checkstatus(code)) {
                                presales.push({
                                    id: data.id,
                                    client_id: data.client_id,
                                    nome_fantasia: data.nome_fantasia,
                                    data_pedido: data.data_pedido,
                                    volume_pedido: data.volume_pedido,
                                    volume_vendido: data.volume_vendido + volume,
                                    preco_venda: data.preco_venda,
                                    preco_mercado: data.preco_mercado
                                });
                                presale = presales.find(x => x.id === presale_id);
                                found = false;
                                $("tr[data-presale-id]").each(function () {
                                    if (presale.data_pedido <= $(this).data("data-pedido")) {
                                        $(this).before(
                                                '<tr data-presale-id="' + presale.id + '">' +
                                                '<td style="text-align: center">' + convertDateDMY(presale.data_pedido) + '</td>' +
                                                '<td>' + presale.nome_fantasia + '</td>' +
                                                '<td>' + formatMoney(presale.preco_venda, 0) + '</td>' +
                                                '<td class="money">' + formatMoney(presale.volume_pedido, 0) + '</td>' +
                                                '<td id="presale-volume-vendido" class="money">' + formatMoney(presale.volume_vendido, 0) + '</td>' +
                                                '<td id="presale-volume-restante" class="money">' + formatMoney(presale.volume_pedido - presale.volume_vendido, 0) + '</td>' +
                                                '</tr>'
                                                );
                                        found = true;
                                        return false;
                                    }
                                });
                                if (!found) {
                                    $("#presales-table").append(
                                            '<tr data-presale-id="' + presale.id + '">' +
                                            '<td style="text-align: center">' + convertDateDMY(presale.data_pedido) + '</td>' +
                                            '<td>' + presale.nome_fantasia + '</td>' +
                                            '<td>' + formatMoney(presale.preco_venda, 0) + '</td>' +
                                            '<td class="money">' + formatMoney(presale.volume_pedido, 0) + '</td>' +
                                            '<td id="presale-volume-vendido" class="money">' + formatMoney(presale.volume_vendido, 0) + '</td>' +
                                            '<td id="presale-volume-restante" class="money">' + formatMoney(presale.volume_pedido - presale.volume_vendido, 0) + '</td>' +
                                            '</tr>'
                                            );
                                }
                                $("#presales-no-data-found").hide();
                                $("#presales-done").hide();
                                detail_id = sale.details.find(x => x.detail_index === dindex).id;
                                if (detail_id !== "new") {
                                    $("#delete-detail-list").append("<option value='" + detail_id + "' selected></option>");
                                }
                                $("div[data-detail-anchor=" + anchor + "]").remove();
                            }
                        });
            } else {
                presale.volume_vendido += adjustment;
                $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-vendido]").html(formatMoney(presale.volume_vendido, 0));
                $("tr[data-presale-id=" + presale_id + "]").find("td[id=presale-volume-restante]").html(formatMoney(presale.volume_pedido - presale.volume_vendido, 0));
                $("#presales-no-data-found").hide();
                $("#presales-done").hide();
                detail_id = sale.details.find(x => x.detail_index === dindex).id;
                if (detail_id !== "new") {
                    $("#delete-detail-list").append("<option value='" + detail_id + "' selected></option>");
                }
                $("div[data-detail-anchor=" + anchor + "]").remove();
            }
        });

        $("#pdf").click(function () {
            data = $("input[name=data_venda]").val().split("/");
            d = new Date(data[2], data[1] - 1, data[0]);
            data_venda = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
            sale_list = "";
            $("input[name=sale_printout]").each(function () {
                if ($(this).is(":checked")) {
                    sale_list += $(this).data("sale-id") + ",";
                }
            });
            sale_list = sale_list.substring(0, (sale_list.length - 1));
            window.open("<?= $this->Url->build(['controller' => 'Sales', 'action' => 'printout']) ?>/" + data_venda + "/" + sale_list + "/report.pdf");
        });

    });

</script>

<style>
    .red {
        color: red;
    }
    .blue {
        color: blue;
    }
</style>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Vendas']
]);
echo $this->Breadcrumbs->render();
?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <?= $this->Form->control('data_venda', ['type' => 'text', 'label' => 'Data Venda', 'default' => $data_venda]) ?>
                    </div>
                </div>
                <?= $this->Form->create(sizeof($sales) > 0 ? $sales : null, ['id' => 'form-sales']) ?>
                <div id="sales-list">
                    <?php if (sizeof($sales) > 0) { ?>
                        <?php
                        $carrier_options = [];
                        foreach ($carriers as $carrier) {
                            $carrier_options[$carrier->id] = $carrier->nome_fantasia;
                        }
                        foreach ($sales as $sale_index => $sale) {
                            $driver_options = [];
                            $vehicle_options = [];
                            if (!is_null($sale->carrier_id)) {
                                foreach ($carriers as $carrier) {
                                    if ($carrier->id == $sale->carrier_id) {
                                        foreach ($carrier->drivers as $driver) {
                                            $driver_options[$driver->id] = $driver->nome;
                                        }
                                        foreach ($carrier->vehicles as $vehicle) {
                                            $vehicle_options[$vehicle->id] = 'Placa: ' . $vehicle->placa;
                                        }
                                    }
                                }
                            }
                            $sale_anchor = rand();
                            ?>
                            <div class="row" id="sale-<?= $sale_index ?>" data-sale-anchor="<?= $sale_anchor ?>">
                                <div class="col">
                                    <div class="border border-dark bg-primary p-3 mb-2">
                                        <?= $this->Form->control('Sales.' . $sale_index . '.id', ['type' => 'hidden']) ?>
                                        <?= $this->Form->control('Sales.' . $sale_index . '.data_venda', ['type' => 'hidden']) ?>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="row">
                                                    <div class="col">
                                                        <?=
                                                        $this->Form->control('Sales.' . $sale_index . '.carrier_id', [
                                                            'type' => 'select',
                                                            'options' => $carrier_options,
                                                            'label' => 'Transportadora',
                                                            'empty' => true,
                                                            'data-sale-index' => $sale_index])
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div id="driver-<?= $sale_index ?>" class="col">
                                                        <?=
                                                        $this->Form->control('Sales.' . $sale_index . '.driver_id', [
                                                            'type' => 'select',
                                                            'options' => $driver_options,
                                                            'label' => 'Motorista',
                                                            'empty' => true,
                                                            'data-sale-index' => $sale_index])
                                                        ?>
                                                    </div>
                                                    <div id="driver-spinner-<?= $sale_index ?>" class="col" style="display: none">
                                                        <?= $this->Html->image('spinner.gif') ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div id="vehicle-<?= $sale_index ?>" class="col">
                                                        <?=
                                                        $this->Form->control('Sales.' . $sale_index . '.vehicle_id', [
                                                            'type' => 'select',
                                                            'options' => $vehicle_options,
                                                            'label' => 'Veículo',
                                                            'empty' => true,
                                                            'data-sale-index' => $sale_index])
                                                        ?>
                                                    </div>
                                                    <div id="vehicle-spinner-<?= $sale_index ?>" class="col" style="display: none">
                                                        <?= $this->Html->image('spinner.gif') ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="row mb-3">
                                                    <div class="col">
                                                        <button type="button" 
                                                                class="btn btn-primary mr-2" 
                                                                data-toggle="modal" 
                                                                data-target="#purchases" 
                                                                onclick="$('#sale-index').val(<?= $sale_index ?>)">Estoque</button>
                                                        <button id="button-presales-<?= $sale_index ?>"
                                                                type="button" 
                                                                class="btn btn-primary" 
                                                                data-toggle="modal" 
                                                                data-target="#presales"
                                                                onclick="$('#sale-index').val(<?= $sale_index ?>)">Pré Vendas</button>
                                                        <div class="pull-right standout">
                                                            <?= $sale_index + 1 ?> - ID <?= $sale->id ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row mb-3">
                                                    <div class="col-md-3 col-sm-12">
                                                        <div class="row">
                                                            <div class="col">
                                                                <label>Volume Veículo</label>
                                                                <div id="capacidade-<?= $sale_index ?>" class="standout">
                                                                    <?php
                                                                    if (!is_null($sale->vehicle)) {
                                                                        $vehicle = $sale->vehicle;
                                                                        if ($vehicle->tipo != 'C') {
                                                                            echo number_format($vehicle->quantidade_litros, 0, ',', '.');
                                                                        } else {
                                                                            $volume = 0;
                                                                            foreach ($vehicle->carts as $cart) {
                                                                                $volume += $cart->quantidade_litros;
                                                                            }
                                                                            echo number_format($volume, 0, ',', '.');
                                                                        }
                                                                    } else {
                                                                        echo '-------';
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="col">
                                                                <label>Volume Vendido</label>
                                                                <?php
                                                                if ($sale->vehicle->tipo != 'C') {
                                                                    $volume = $vehicle->quantidade_litros;
                                                                } else {
                                                                    $volume = 0;
                                                                    foreach ($vehicle->carts as $cart) {
                                                                        $volume += $cart->quantidade_litros;
                                                                    }
                                                                }
                                                                ?>                                                        
                                                                <div id="volume-total-<?= $sale_index ?>" class="standout <?= $volume > $sale->volume_total ? 'red' : 'blue' ?>">
                                                                    <?= number_format($sale->volume_total, 0, ',', '.') ?>
                                                                </div>
                                                                <?= $this->Form->control('Sales.' . $sale_index . '.volume_total', ['type' => 'hidden']) ?>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-8 mt-2">
                                                                <?= $this->Form->control('Sales.' . $sale_index . '.frete', ['type' => 'text', 'label' => 'Frete (R$)']) ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 col-sm-12">
                                                        <label>Contratos</label>
                                                        <div id="purchases-<?= $sale_index ?>">
                                                            <?php
                                                            if (!is_null($sale->sale_details)) {
                                                                echo $this->Form->control('purchase_index_' . $sale_index, ['type' => 'hidden', 'value' => sizeof($sale->purchases)]);
                                                            } else {
                                                                echo $this->Form->control('purchase_index_' . $sale_index, ['type' => 'hidden', 'value' => 0]);
                                                            }
                                                            ?>
                                                            <?php
                                                            foreach ($sale->purchases as $purchase_index => $purchase) {
                                                                $purchase_anchor = rand();
                                                                ?>
                                                                <?= $this->Form->control('Sales.' . $sale_index . '.purchases.' . $purchase_index . '.id', ['type' => 'hidden']) ?>
                                                                <div class="row" data-purchase-anchor="<?= $purchase_anchor ?>">
                                                                    <div class="col-md-5">
                                                                        <span class="standout"><?= $purchase->numero_contrato ?></span>
                                                                        <br>
                                                                        <span class="mr-3">Usina: <?= $purchase->plant->nome_fantasia ?></span><br>
                                                                        <span>Distribuidora: <?= $purchase->distributor->nome_fantasia ?></span>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-12">
                                                                        <label>Volume</label>
                                                                        <div id="purchase-volume-<?= $sale_index ?>-<?= $purchase_index ?>" class="standout"><?= number_format($purchase->_joinData->volume, 0, ',', '.') ?></div>
                                                                        <?= $this->Form->control('Sales.' . $sale_index . '.purchases.' . $purchase_index . '._joinData.volume', ['type' => 'hidden']) ?>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-12">
                                                                        <label>Estoque</label>
                                                                        <div id="estoque-<?= $sale_index ?>-<?= $purchase_index ?>" class="standout">
                                                                            <?= number_format($purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito - $purchase->volume_vendido, 0, ',', '.') ?>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12">
                                                                        <?= $this->Form->control('Sales.' . $sale_index . '.purchases.' . $purchase_index . '._joinData.spread', ['type' => 'text', 'label' => 'Spread (R$)']) ?>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-1 col-sm-12">
                                                        <i id="trash-sale-<?= $sale_index ?>" class="fa fa-trash" 
                                                           data-sale-anchor="<?= $sale_anchor ?>" 
                                                           data-sale-index="<?= $sale_index ?>">
                                                        </i>
                                                    </div>
                                                </div>
                                                <hr>
                                                <div class="row mb-1">
                                                    <div class="col-md-4 col-sm-12">Cliente</div>
                                                    <div class="col-md-2 col-sm-12">Nota Fiscal</div>
                                                    <div class="col-md-2 col-sm-12">Volume Vendido</div>
                                                    <div class="col-md-2 col-sm-12">Conta</div>
                                                </div>
                                                <div id="details-<?= $sale_index ?>">
                                                    <?php
                                                    if (!is_null($sale->sale_details)) {
                                                        echo $this->Form->control('detail_index_' . $sale_index, ['type' => 'hidden', 'value' => sizeof($sale->sale_details)]);
                                                    } else {
                                                        echo $this->Form->control('detail_index_' . $sale_index, ['type' => 'hidden', 'value' => 0]);
                                                    }
                                                    if (!is_null($sale->sale_details)) {
                                                        if (sizeof($sale->sale_details) > 0) {
                                                            foreach ($sale->sale_details as $detail_index => $detail) {
                                                                $sale_anchor = rand();
                                                                ?>
                                                                <div id="detail-<?= $sale_index ?>-<?= $detail_index ?>" 
                                                                     class="row" 
                                                                     data-detail-anchor="<?= $sale_anchor ?>" 
                                                                     data-detail-index="<?= $detail_index ?>">
                                                                         <?= $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.id', ['type' => 'hidden']) ?>
                                                                    <div class="col-md-4 col-sm-12">
                                                                        <div style="font-size: larger; font-weight: bold"><?= $detail->presale->client->nome_fantasia ?></div>
                                                                        <?=
                                                                        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.presale_id', [
                                                                            'type' => 'hidden'
                                                                        ])
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-12">
                                                                        <?=
                                                                        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.nota_fiscal', [
                                                                            'type' => 'text',
                                                                            'label' => false
                                                                        ])
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-2 col-sm-12">
                                                                        <?=
                                                                        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.volume', [
                                                                            'type' => 'text',
                                                                            'label' => false,
                                                                            'data-sale-index' => $sale_index,
                                                                            'data-detail-index' => $detail_index,
                                                                            'data-presale-id' => $detail->presale_id
                                                                        ])
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12">
                                                                        <?=
                                                                        $this->Form->control('Sales.' . $sale_index . '.sale_details.' . $detail_index . '.account_id', [
                                                                            'type' => 'select',
                                                                            'options' => $accounts,
                                                                            'label' => false,
                                                                            'empty' => true])
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-1 col-sm-12 mt-2">
                                                                        <i class="fa fa-trash" 
                                                                           data-detail-anchor="<?= $sale_anchor ?>" 
                                                                           data-sale-index="<?= $sale_index ?>" 
                                                                           data-detail-index="<?= $detail_index ?>">
                                                                        </i>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                            }
                                                        } else {
                                                            ?>
                                                            <div id="no-data-<?= $sale_index ?>" class="p-3 no-data-found">
                                                                Nenhum detalhe encontrado
                                                            </div>
                                                            <?php
                                                        }
                                                    } else {
                                                        ?>
                                                        <div id="no-data-<?= $sale_index ?>" class="p-3 no-data-found">
                                                            Nenhum detalhe encontrado
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div id="no-data" class="border border-dark bg-primary p-3 mb-2 no-data-found">
                            Nenhuma venda encontrada
                        </div>
                    <?php } ?>
                </div>
                <div class="row">
                    <div class="col">
                        <i id="button-add-sale" class="fa fa-2x fa-plus-square"></i>
                    </div>
                </div>
                <?= $this->Form->control('delete-sale-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->control('delete-detail-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-lg btn-primary pull-right mr-2" data-toggle="modal" data-target="#printout-selection">Resumo</button>
            </div>
        </div>
    </div>
</div>

<?= $this->Form->control('sale_index', ['type' => 'hidden']) ?>

<!-- Lista de contratos -->
<div class="modal fade" id="purchases" tabindex="-1" role="dialog" aria-labelledby="purchasesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 70% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="purchasesModalLabel">Estoque</h5>
            </div>
            <div class="modal-body">
                <table class="table table-hover extra-slim-row">
                    <tr>
                        <th>Contrato</th>
                        <th>Distribuidora</th>
                        <th>Usina</th>
                        <th class="money">Volume Comprado</th>
                        <th class="money">Preço Compra</th>
                        <th class="money">Volume Disponível</th>
                        <th style="text-align: center">Data Criação</th>
                    </tr>
                    <?php
                    if (sizeof($purchases) > 0) {
                        foreach ($purchases as $purchase) {
                            $estoque_comprado = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito;
                            $estoque_disponivel = $estoque_comprado - $purchase->volume_vendido;
                            if ($estoque_disponivel > 0) {
                                ?>
                                <tr data-purchase-id="<?= $purchase->id ?>">
                                    <td><?= $purchase->numero_contrato ?></td>
                                    <td><?= $purchase->distributor->nome_fantasia ?></td>
                                    <td><?= $purchase->plant->nome_fantasia ?></td>
                                    <td class="money"><?= number_format($estoque_comprado, 0, ',', '.') ?></td>
                                    <td class="money"><?= $this->Number->currency($purchase->preco_litro + $purchase->preco_litro_adicional, null, ['places' => 4]) ?></td>
                                    <td id="volume-disponivel-<?= $purchase->id ?>" class="money"><?= number_format($estoque_disponivel, 0, ',', '.') ?></td>
                                    <td style="text-align: center"><?= $purchase->created ?></td>
                                </tr>
                                <?php
                            }
                        }
                    } else {
                        ?>
                        <tr class="no-data-found">
                            <td colspan="10">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Lista de pre vendas -->
<div class="modal fade" id="presales" tabindex="-1" role="dialog" aria-labelledby="presalesModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 50% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="presalesModalLabel">Pré Vendas</h5>
            </div>
            <div class="modal-body">
                <table id="presales-table" class="table table-hover extra-slim-row">
                    <tr>
                        <th style="text-align: center">Data Pedido</th>
                        <th>Cliente</th>
                        <th class="money">Preço Venda</th>
                        <th class="money">Volume Pedido</th>
                        <th class="money">Volume Vendido</th>
                        <th class="money">Volume Restante</th>
                    </tr>
                    <?php
                    if (sizeof($presales) > 0) {
                        foreach ($presales as $presale) {
                            ?>
                            <tr data-presale-id="<?= $presale->id ?>" data-data-pedido="<?= date('Y-m-d', strtotime(str_replace('/', '-', $presale->data_pedido))) ?>">
                                <td style="text-align: center"><?= $presale->data_pedido ?></td>
                                <td><?= $presale->client->nome_fantasia ?></td>
                                <td class="money"><?= number_format($presale->preco_venda, 4, ',', '.') ?></td>
                                <td class="money"><?= number_format($presale->volume_pedido, 0, ',', '.') ?></td>
                                <td id="presale-volume-vendido" class="money"><?= number_format($presale->volume_vendido, 0, ',', '.') ?></td>
                                <td id="presale-volume-restante" class="money"><?= number_format($presale->volume_pedido - $presale->volume_vendido, 0, ',', '.') ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="presales-no-data-found" class="no-data-found">
                            <td colspan="5">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                    <tr id="presales-done" class="no-data-found" style="display: none">
                        <td colspan="5">Todas as pré-vendas foram associadas</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="printout-selection" role="dialog">
    <div class="modal-dialog modal-lg" style="max-width: 60% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="printout-selectionModalLabel">Resumo Para Impressão</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-hover extra-slim-row">
                    <tr>
                        <th style="text-align: center">
                            <?= $this->Form->control('select_all', ['type' => 'checkbox', 'label' => false, 'checked']) ?>
                        </th>
                        <th>Transportadora</th>
                        <th>Motorista</th>
                        <th>Veículo</th>
                        <th>Usina</th>
                    </tr>
                    <?php
                    if (sizeof($sales) > 0) {
                        foreach ($sales as $sale) {
                            $purchase = $sale->purchases[0];
                            ?>
                            <tr>
                                <td style="text-align: center">
                                    <?= $this->Form->control('sale_printout', ['type' => 'checkbox', 'label' => false, 'data-sale-id' => $sale->id, 'checked']) ?>
                                </td>
                                <td><?= $sale->carrier->razao_social ?></td>
                                <td><?= $sale->driver->nome ?></td>
                                <td><?= $sale->vehicle->placa ?></td>
                                <td><?= $purchase->plant->razao_social ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr class="no-data-found">
                            <td colspan="10">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <div class="modal-footer">
                <button id="pdf" type="button" class="btn btn-primary pull-right">Gerar PDF</button>
            </div>
        </div>
    </div>
</div>

<?php
//dump($presales);
?>
