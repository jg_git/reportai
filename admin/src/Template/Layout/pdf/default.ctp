<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>RBL</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <?= $this->Html->css('custom-style.css?v=' . rand(), ['fullBase' => true]); ?>
    </head>
    <body>
        <?= $this->fetch('content') ?>
    </body>
</html>