<div class="row" id="conciliation-<?= $sale_index ?>-<?= $purchase_index ?>-<?= $conciliation_index ?>">
    <div class="col-md-6 col-sm-12">
        <?=
        $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.sales_purchases_conciliations.' . $conciliation_index . '.nota_fiscal', [
            'type' => 'text',
            'label' => false
        ])
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?=
        $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.sales_purchases_conciliations.' . $conciliation_index . '.volume_conciliado', [
            'type' => 'text',
            'label' => false,
            'data-sale-index' => $sale_index,
            'data-purchase-index' => $purchase_index
        ])
        ?>
    </div>
    <div class="col-md-1 col-sm-12">
        <i class="fa fa-trash mt-2" 
           data-sale-index="<?= $sale_index ?>" 
           data-purchase-index="<?= $purchase_index ?>"
           data-conciliation-index="<?= $conciliation_index ?>">
        </i>
    </div>
</div>
