<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<?= $this->Html->script('/js/jQuery.blockUI.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {

        $("input[name*=data]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom",
            autoclose: true
        });

        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            $("#sales-" + $(this).data("sale-index") + "-data-conciliado").val("<?= $today ?>");
        }).on('ifUnchecked', function (e) {
            $("#sales-" + $(this).data("sale-index") + "-data-conciliado").val("");
        });

        $("input[name=data_venda]").change(function () {
            data = $(this).val().split("/");
            d = new Date(data[2], data[1] - 1, data[0]);
            data_venda = d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
            window.location = "<?= $this->Url->build(['controller' => 'Conciliations', 'action' => 'edit']) ?>/" + data_venda;
        });

        $(".fa-plus-square").click(function () {
            sale_index = $(this).data("sale-index");
            purchase_index = $(this).data("purchase-index");
            conciliation_index = $(this).data("conciliation-index");
            $(this).data("conciliation-index", conciliation_index + 1);
            $.get("<?= $this->Url->build(['controller' => 'Conciliations', 'action' => 'newConciliation']) ?>/" + sale_index + "/" + purchase_index + "/" + conciliation_index,
                    function (data, code) {
                        $("#conciliation-list-" + sale_index + "-" + purchase_index).append(data);
                        $("#sales-" + sale_index + "-sales-purchases-" + purchase_index + "-sales-purchases-conciliations-" + conciliation_index + "-volume-conciliado")
                                .maskMoney({thousands: '.', decimal: ',', precision: 0});
                    });
        });

        $("#sales").on("change", "input[id$=volume-conciliado]", function () {
            sale_index = $(this).data("sale-index");
            purchase_index = $(this).data("purchase-index");
            volume_conciliado = 0;

            $("#conciliation-list-" + sale_index + "-" + purchase_index).find("input[id$=volume-conciliado]").each(function () {
                volume = parseInt($(this).val().replace(/\./g, "").replace(",", "."));
                volume_conciliado += volume;
            });

            $("#volume-conciliado-" + sale_index + "-" + purchase_index).html(formatMoney(volume_conciliado, 0));
            $("#sales-" + sale_index + "-sales-purchases-" + purchase_index + "-volume-conciliado").val(parseInt(volume_conciliado));
        });

        $("#sales").on("click", ".fa-trash", function () {
            sale_index = $(this).data("sale-index");
            purchase_index = $(this).data("purchase-index");
            conciliation_index = $(this).data("conciliation-index");

            $("div[id=conciliation-" + sale_index + "-" + purchase_index + "-" + conciliation_index).remove();

            volume_conciliado = 0;
            $("#conciliation-list-" + sale_index + "-" + purchase_index).find("input[id$=volume-conciliado]").each(function () {
                volume = parseInt($(this).val().replace(/\./g, "").replace(",", "."));
                volume_conciliado += volume;
            });

            $("#volume-conciliado-" + sale_index + "-" + purchase_index).html(formatMoney(volume_conciliado, 0));
            $("#sales-" + sale_index + "-sales-purchases-" + purchase_index + "-volume-conciliado").val(volume_conciliado);
        });

<?php
foreach ($sales as $sale_index => $sale) {
    foreach ($sale->sales_purchases as $purchase_index => $purchase) {
        foreach ($purchase->sales_purchases_conciliations as $conciliation_index => $conciliation) {
            $volume = $conciliation->volume_conciliado;
            if ($volume < 10) { // para consertar um bug no mask money para valor com precisao zero
                $volume /= 10;
            } else if ($volume < 100) {
                $volume /= 100;
            } else {
                $volume /= 1000;
            }
            ?>
                    $("#sales-<?= $sale_index ?>-sales-purchases-<?= $purchase_index ?>-sales-purchases-conciliations-<?= $conciliation_index ?>-volume-conciliado")
                            .maskMoney({thousands: '.', decimal: ',', precision: 0})
                            .maskMoney('mask', <?= $volume ?>);
            <?php
            if ($sale->conciliado) {
                ?>
                        $("#sales-<?= $sale_index ?>-sales-purchases-<?= $purchase_index ?>-sales-purchases-conciliations-<?= $conciliation_index ?>-nota-fiscal").prop("readOnly", true);
                        $("#sales-<?= $sale_index ?>-sales-purchases-<?= $purchase_index ?>-sales-purchases-conciliations-<?= $conciliation_index ?>-volume-conciliado").prop("readOnly", true);
                <?php
            }
        }
    }
}
?>

        $("a[id=estorno]").click(function () {
            sale_index = $(this).data("sale-index");
            $("div[id=conciliado][data-sale-index=" + sale_index + "]").show();
            $("a[id=estorno][data-sale-index=" + sale_index + "]").hide();
            $("input[type=checkbox][data-sale-index=" + sale_index + "]").iCheck("uncheck");
            $("div[id=sale][data-sale-index=" + sale_index + "]").find("input[type=text]").prop("readOnly", false);
            $("div[id=sale][data-sale-index=" + sale_index + "]").find("i[id=remove]").show();
            $("div[id=sale][data-sale-index=" + sale_index + "]").find("i[id=add]").show();
        });

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Conciliação de Vendas']
]);
echo $this->Breadcrumbs->render();
?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2 col-sm-12">
                        <?= $this->Form->control('data_venda', ['type' => 'text', 'label' => 'Data Pedido', 'default' => $data_venda]) ?>
                    </div>
                </div>
                <?= $this->Form->create(sizeof($sales) > 0 ? $sales : null, ['id' => 'form-conciliations']) ?>
                <div id="sales">
                    <?php
                    if (sizeof($sales) > 0) {
                        foreach ($sales as $sale_index => $sale) {
                            ?>
                            <div class="row" id="sale" data-sale-index="<?= $sale_index ?>">
                                <div class="col">
                                    <div class="border border-dark bg-primary p-3 mb-2">
                                        <div class="row">
                                            <div class="col">
                                                <div class="pull-right standout"><?= $sale_index + 1 ?> - ID <?= $sale->id ?></div>
                                            </div>
                                        </div>
                                        <?= $this->Form->control('Sales.' . $sale_index . '.id', ['type' => 'hidden']) ?>
                                        <?= $this->Form->control('Sales.' . $sale_index . '.data_venda', ['type' => 'hidden']) ?>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-12">
                                                <div class="row">
                                                    <div class="col">
                                                        Transportadora: <?= $sale->carrier->razao_social ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        Veículo: <?= $sale->vehicle->marca_modelo ?> - Placa: <?= $sale->vehicle->placa ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        Motorista: <?= $sale->driver->nome ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <label>Contratos</label>
                                                <?php foreach ($sale->purchases as $purchase_index => $purchase) { ?>
                                                    <?= $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.id', ['type' => 'hidden']) ?>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12">
                                                            <div class="row">
                                                                <div class="col">
                                                                    <div class="standout">
                                                                        <?= $purchase->numero_contrato ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    Usina: <?= $purchase->plant->razao_social ?>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col">
                                                                    Distribuidora: <?= $purchase->distributor->razao_social ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12">
                                                            <label>Volume Vendido</label>
                                                            <div class="standout">
                                                                <?= number_format($purchase->_joinData->volume, 0, ',', '.') ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3 col-sm-12">
                                                            <label>Volume Conciliado</label>
                                                            <div id="volume-conciliado-<?= $sale_index ?>-<?= $purchase_index ?>" class="standout">
                                                                <?= number_format($purchase->_joinData->volume_conciliado, 0, ',', '.') ?>
                                                            </div>
                                                            <?= $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.volume_conciliado', ['type' => 'hidden']) ?>
                                                        </div>
                                                    </div>
                                                    <div id="conciliation-area-<?= $sale_index ?>-<?= $purchase_index ?>" class="mt-3">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-12">
                                                                <label>Nota Fiscal</label>
                                                            </div>
                                                            <div class="col-md-3 col-sm-12">
                                                                <label>Volume</label>
                                                            </div>
                                                        </div>
                                                        <div id="conciliation-list-<?= $sale_index ?>-<?= $purchase_index ?>">
                                                            <?php
                                                            $sales_purchases = $sale->sales_purchases[$purchase_index];
                                                            foreach ($sales_purchases->sales_purchases_conciliations as $conciliation_index => $conciliation) {
                                                                ?>
                                                                <div class="row" id="conciliation-<?= $sale_index ?>-<?= $purchase_index ?>-<?= $conciliation_index ?>">
                                                                    <?=
                                                                    $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.sales_purchases_conciliations.' . $conciliation_index . '.id', [
                                                                        'type' => 'hidden'])
                                                                    ?>
                                                                    <div class="col-md-6 col-sm-12">
                                                                        <?=
                                                                        $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.sales_purchases_conciliations.' . $conciliation_index . '.nota_fiscal', [
                                                                            'type' => 'text',
                                                                            'label' => false])
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-3 col-sm-12">
                                                                        <?=
                                                                        $this->Form->control('Sales.' . $sale_index . '.sales_purchases.' . $purchase_index . '.sales_purchases_conciliations.' . $conciliation_index . '.volume_conciliado', [
                                                                            'type' => 'text',
                                                                            'label' => false,
                                                                            'data-sale-index' => $sale_index,
                                                                            'data-purchase-index' => $purchase_index
                                                                        ])
                                                                        ?>
                                                                    </div>
                                                                    <div class="col-md-1 col-sm-12">
                                                                        <i class="fa fa-trash mt-2" 
                                                                           id="remove"
                                                                           data-sale-index="<?= $sale_index ?>" 
                                                                           data-purchase-index="<?= $purchase_index ?>"
                                                                           data-conciliation-index="<?= $conciliation_index ?>"
                                                                           style="<?= $sale->conciliado ? 'display:none' : '' ?>">
                                                                        </i>
                                                                    </div>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col">
                                                                <i class="fa fa-2x fa-plus-square"
                                                                   id="add"
                                                                   data-sale-index="<?= $sale_index ?>"
                                                                   data-purchase-index="<?= $purchase_index ?>"
                                                                   data-conciliation-index="<?= sizeof($sales_purchases->sales_purchases_conciliations) ?>"
                                                                   style="<?= $sale->conciliado ? 'display:none' : '' ?>">
                                                                </i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                <?php } ?>
                                            </div>
                                            <div class="col-md-2 col-sm-12 pt-4">
                                                <div id="conciliado" data-sale-index="<?= $sale_index ?>" style="<?= $sale->conciliado ? 'display:none' : '' ?>">
                                                    <?=
                                                    $this->Form->control('Sales.' . $sale_index . '.conciliado', [
                                                        'type' => 'checkbox',
                                                        'label' => 'Conciliado',
                                                        'data-sale-index' => $sale_index
                                                    ])
                                                    ?>
                                                </div>
                                                <?php
                                                if ($sale->conciliado) {
                                                    $role = $this->request->getSession()->read('Security.role');
                                                    if ($role['super_user']) {
                                                        ?>
                                                        <a href="javascript: void(0)" 
                                                           id="estorno" 
                                                           class="btn btn-sm btn-primary" 
                                                           data-sale-index="<?= $sale_index ?>">ESTORNO
                                                        </a>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <?= $this->Form->control('Sales.' . $sale_index . '.data_conciliado', ['type' => 'hidden']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div id="no-data" class="border border-dark bg-primary p-3 mb-2 no-data-found">
                            Nenhuma venda encontrada
                        </div>
                    <?php } ?>
                </div>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
