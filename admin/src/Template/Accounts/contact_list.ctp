<table id="contact" class="table table-hover">
    <?php
    if (isset($account->distributor_contacts) and sizeof($account->distributor_contacts) > 0) {
        foreach ($account->distributor_contacts as $contact) {
            $anchor = rand();
            ?>
            <tr id="view-contact-<?= $anchor ?>">
                <td>
                    <span id="view-contact-area-<?= $anchor ?>" style="font-size: 14px">
                        <?= $contact->nome ?>
                    </span>
                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('contact', '<?= $anchor ?>', <?= $contact->id ?>)"></i>
                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr id="no-contact">
            <td class="no-data-found">Nenhum Contato Cadastrado</td>
        </tr>
        <?php
    }
    ?>
    <tr id="contact-add">
        <td>
            <i class="fa fa-plus-square" data-type="contact"></i>
        </td>
    </tr>
</table>