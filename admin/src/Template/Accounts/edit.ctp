<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {
        $("#basic").load("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'basic', $account->isNew() ? 'new' : $account->id]) ?>", function () {

        });

        $("#contact-list").on("click", ".fa-plus-square", function () {
            anchor = anchorize();
            $("#no-contact").remove();
            $("#contact-add").before(
                    '<tr id="edit-contact-' + anchor + '">' +
                    '<td>' +
                    '<div id="edit-contact-area-' + anchor + '"></div>' +
                    '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'contact\', \'' + anchor + '\', \'new\')"></i>' +
                    '</td>' +
                    '</tr>'
                    );
            $("#edit-contact-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'contact']) ?>/<?= $account->isNew() ? 'new' : $account->id ?>/" +
                    anchor);
        });

        $("#save").click(function () {
            HoldOn.open({theme: "sk-cube-grid"});
            var basic = $.Deferred();
            var contact = $.Deferred();
            account_id = $("#form-basic").data("id");
            $.post("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'basic']) ?>/" + account_id,
                    $("#form-basic").serialize(),
                    function (data) {
                        $("#basic").html(data);
                        if ($("<div>" + data + "</div>").find("#form-basic").data("error") === "N") {
                            basic.resolve("OK");
                            if ($("input[name=tipo]:checked").val() === "I") {
                                account_id = $("#form-basic").data("id");
                                var request = [];
                                $("form[id|='form-contact']").each(function () {
                                    anchor = $(this).data("anchor");
                                    request.push($.post("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'contact']) ?>/" + account_id + "/" + anchor,
                                            $("#form-contact-" + anchor).serialize()));
                                });
                                if ($("#form-delete-contact option").length > 0) {
                                    request.push($.post("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'contact-delete']) ?>/" + account_id,
                                            $("#form-delete-contact").serialize()));
                                }
                                if (request.length > 0) {
                                    $.when.apply(undefined, request).then(function () {
                                        $("#contact-list").load("<?= $this->Url->build(['controller' => 'Accounts', 'action' => 'contact-list']) ?>/" + account_id, function () {
                                            contact.resolve("OK");
                                        });
                                    });
                                } else {
                                    contact.resolve("OK");
                                }
                            } else {
                                contact.resolve("OK");
                            }
                        } else {
                            basic.resolve("NG");
                            contact.resolve("NG");
                        }
                    });
            $.when(basic, contact).done(function (v1, v2) {
                $("#save").attr("disabled", false);
                console.log("basic: " + v1 + " contact: " + v2);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK") {
                    toast("Cadastro salvo com sucesso");
                }
            });
        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Essa operação não poderá ser revertida',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                switch (type) {
                    case "contact":
                        $("#edit-contact-" + anchor).remove();
                        $("#view-contact-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-contact-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                }
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Contas', 'url' => ['action' => 'index']],
    ['title' => 'Cadastro de Conta']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div id="basic"></div>
                <div id="distributor-contact-list">
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <h5 class="mt-3 mb-3">Contatos da Distribuidora</h5>
                            <span id="contact-list">
                                <table id="contact" class="table table-hover">
                                    <?php
                                    if (isset($account->distributor_contacts) and sizeof($account->distributor_contacts) > 0) {
                                        foreach ($account->distributor_contacts as $contact) {
                                            $anchor = rand();
                                            ?>
                                            <tr id="view-contact-<?= $anchor ?>">
                                                <td>
                                                    <span id="view-contact-area-<?= $anchor ?>" style="font-size: 14px">
                                                        <?= $contact->nome ?>
                                                    </span>
                                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('contact', '<?= $anchor ?>', <?= $contact->id ?>)"></i>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr id="no-contact">
                                            <td class="no-data-found">Nenhum Contato Cadastrado</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                    <tr id="contact-add">
                                        <td>
                                            <i class="fa fa-plus-square" data-type="contact"></i>
                                        </td>
                                    </tr>
                                </table>
                            </span>
                            <?= $this->Form->create(null, ['id' => 'form-delete-contact']) ?>
                            <?= $this->Form->control('delete-contact-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <?= $this->element('button_save', ['id' => 'save', 'submit' => 'N']) ?>
                <?= $this->element('button_delete', ['id' => $account->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>
    </div>
</div>
