<script>
    $(document).ready(function () {
        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });

        $("input[name=tipo]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on("ifClicked", function (e) {
            switch ($(this).val()) {
                case "I":
                    $("select[name=distributor_id]").prop("disabled", false);
                    $("select[name=bank_id]").prop("disabled", true);

                    $("#distributor").show();
                    $("#distributor-contact-list").show();
                    $("#bank").hide();
                    $("#bank-info").hide();
                    break;
                case "F":
                    $("select[name=distributor_id]").prop("disabled", true);
                    $("select[name=bank_id]").prop("disabled", false);

                    $("#distributor").hide();
                    $("#distributor-contact-list").hide();
                    $("#bank").show();
                    $("#bank-info").show();
                    break;
                case "C":
                    $("select[name=distributor_id]").prop("disabled", true);
                    $("select[name=bank_id]").prop("disabled", true);

                    $("#distributor").hide();
                    $("#distributor-contact-list").hide();
                    $("#bank").hide();
                    $("#bank-info").hide();
                    break;
            }
        });
<?php
switch ($account->tipo) {
    case "I":
        ?>
                $("#bank").hide();
                $("#bank-info").hide();
        <?php
        break;
    case "F":
        ?>
                $("#distributor").hide();
                $("#distributor-contact-list").hide();
        <?php
        break;
    case "C":
        ?>
                $("#bank").hide();
                $("#bank-info").hide();
                $("#distributor").hide();
                $("#distributor-contact-list").hide();
        <?php
        break;
}
?>
        $("input[name=telefone_gerente]").inputmask({
            mask: ['(99) 9999-9999', '(99) 99999-9999'],
            keepStatic: true
        });
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($account, [
    'id' => 'form-basic',
    'data-id' => $account->isNew() ? 'new' : $account->id,
    'data-error' => sizeof($account->getErrors()) > 0 ? 'Y' : 'N'])
?>
<div class="row mb-3 mt-3">
    <div class="col-md-3 col-sm-12 pl-5">
        <?= $this->Form->control('ativa', ['type' => 'checkbox', 'label' => 'Ativa']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->radio('tipo', $account_record_types) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('titular', ['type' => 'text', 'label' => 'Titular/Nome']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <div id="distributor">
            <?=
            $this->Form->control('distributor_id', [
                'options' => $distributors,
                'label' => 'Distribuidora',
                'disabled' => $account->tipo == 'I' ? false : true])
            ?>
        </div>
        <div id="bank">
            <?=
            $this->Form->control('bank_id', [
                'options' => $banks,
                'label' => 'Banco',
                'disabled' => $account->tipo == 'F' ? false : true])
            ?>
        </div>
    </div>
</div>
<div id="bank-info">
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <?= $this->Form->control('numero_agencia', ['type' => 'text', 'label' => 'Número da Agencia']) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $this->Form->control('numero_conta', ['type' => 'text', 'label' => 'Número da Conta']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-sm-12">
            <?= $this->Form->control('nome_gerente', ['type' => 'text', 'label' => 'Nome do Gerente']) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $this->Form->control('telefone_gerente', ['type' => 'text', 'label' => 'Telefone do Gerente']) ?>
        </div>
        <div class="col-md-3 col-sm-12">
            <?= $this->Form->control('email_gerente', ['type' => 'text', 'label' => 'Email do Gerente']) ?>
        </div>
    </div>
</div>
<?= $this->Form->end() ?>