<?= $this->Html->css('signin.css') ?>

    <?= $this->Form->create('post', ['class' => 'form-signin']) ?>

    <?= $this->Html->image('logoReportAi.png', ['class' => 'mb-6 img-fluid', 'alt' => 'ReportAí', 'width' => '400', 'height' => '70']) ?>

<div class="form-modelo">
    <h1 class="h6 mb-3 font-bold ">Faça login para iniciar sua sessão</h1>

    <?= $this->Flash->render() ?>

    <div class="form-group">
        <!-- <label>Usuário</label> -->
        <?= $this->Form->control('email', ['class' => 'form-control', 'placeholder' => 'Digite o email', 'label' => false]) ?>
    </div>

    <div class="form-group">
        <!-- <label>Senha</label> -->
        <?= $this->Form->control('password', ['class' => 'form-control', 'placeholder' => 'Digite a senha', 'label' => false]) ?>
    </div>

    <?= $this->Form->button(__('login'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>

    <?= $this->Form->end() ?>
</div>