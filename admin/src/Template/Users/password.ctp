<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Senha']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?= $this->Form->create($user) ?>
                <fieldset>
                    <legend>Senha</legend>
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('password', ['label' => 'Senha']) ?>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('confirm_password', ['type' => 'password', 'label' => 'Confirmação de Senha']) ?>
                        </div>
                    </div>
                </fieldset>
                <?= $this->element('button_save') ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>