<script>
    $(document).ready(function () {

        $("a[data-page]").click(function () {
            $.get("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'historico']) ?>/" + $(this).data("page"),
                    function (data, code) {
                        if (checkstatus(code)) {
                            $("div[id=historico-lista]").html(data);
                        }
                    });
        });

        $("a[data-wrapping]").click(function () {
            console.log("here");
            swal({
                title: "Deseja estornar essa transferência?",
                showCancelButton: true,
                confirmButtonColor: "#079dff",
                cancelButtonColor: "lightgray",
                confirmButtonText: "SIM",
                cancelButtonText: "NÃO",
                confirmButtonClass: "btn btn-lg btn-primary",
                cancelButtonClass: "btn btn-lg btn-outline-dark mr-3",
                buttonsStyling: false,
                reverseButtons: true,
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    $.get("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'transferencia-estorno']) ?>/" + $(this).data("wrapping"),
                            function (data, code) {
                                if (checkstatus(code)) {
                                    if (data.status === "OK") {
                                        window.location = "<?= $this->Url->build(['action' => 'edit']) ?>";
                                    } else {
                                        swal({
                                            title: "Estorno Não Executado",
                                            text: "Existem contas a pagar associadas a essa transferência que foram dadas baixa",
                                            confirmButtonColor: "#079dff",
                                            confirmButtonText: "OK",
                                            confirmButtonClass: "btn btn-lg btn-primary",
                                            buttonsStyling: false
                                        });
                                    }
                                }
                            });
                }
            });
        });

    });
</script>

<table class="table table-hover slim-row">
    <tr>
        <th></th>
        <th>Contrato Destino</th>
        <th>Contratos Origem</th>
        <th class="money">Volume Total</th>
        <th>Data</th>
        <th></th>
    </tr>
    <?php
    $max = 0;
    $min = null;
    foreach ($transfers as $t) {
        if ($t->wrapping > $max) {
            $max = $t->wrapping;
        }
        if (is_null($min) or $t->wrapping < $min) {
            $min = $t->wrapping;
        }
    }
    $i = $max;
    while ($i >= $min) {
        foreach ($transfers as $t) {
            if (is_null($t->destination_purchase_id) and $t->wrapping == $i) {
                $volume = 0;
                $msg = '';
                foreach ($transfers as $t2) {
                    if ($t2->wrapping == $i) {
                        if (is_null($t2->destination_purchase_id)) {
                            $volume += $t2->volume;
                        } else {
                            $msg .= $t2->purchase->numero_contrato . ' - ' .
                                    number_format(abs($t2->volume), 0, ',', '.') . 'L - ' .
                                    'Frete ' . $this->Number->currency($t2->frete) .
                                    '<br>';
                        }
                    }
                }
                ?>
                <tr>
                    <td><?= $i ?></td>
                    <td><?= $t->purchase->numero_contrato ?></td>
                    <td><?= rtrim($msg, '<br>') ?></td>
                    <td class="money"><?= number_format($volume, 0, ',', '.') ?></td>
                    <td><?= $t->created ?></td>
                    <td>
                        <?php
                        $role = $this->request->getSession()->read('Security.role');
                        if ($role['super_user'] and in_array($i, $recent)) {
                            ?>
                            <a href="javascript:void(0)" data-transfer="yes" data-wrapping="<?= $t->wrapping ?>">ESTORNO</a>
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
                $i--;
            }
        }
    }
    ?>
</table>
<div class="paginator">
    <ul class="pagination justify-content-center">
        <li class="page-item<?= $page + 10 > $last ? ' disabled' : '' ?>">
            <a href="javascript:void(0)" class="page-link" data-page="<?= $page + 10 ?>">< Anterior</a>
        </li>
        <li class="page-item<?= $page - 11 < 0 ? ' disabled' : '' ?>">
            <a href="javascript:void(0)" class="page-link" data-page="<?= $page - 10 ?>">Próximo ></a>
        </li>
    </ul>
</div>
<?php
//dump($recent);
?>