<script>
    $(document).ready(function () {
        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        
        $("input[type=radio]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
        
        $("input[name=frete]").maskMoney({thousands: '.', decimal: ',', precision: 2});
        
        $("#button-transitar").click(function () {
            if ($("input[name=origin_purchase_id]:checked").val() !== undefined) {
                $("input[name=volume]").val($("input[name=origin_purchase_id]:checked").data("estoque"));
                $("input[name=volume]").maskMoney({thousands: '.', decimal: ',', precision: 0}).maskMoney('mask', $(this).val());
                $("#transitar").modal({show: true});
            } else {
                swal({
                    title: "Contrato origem não selecionado",
                    confirmButtonColor: "#079dff",
                    confirmButtonText: "OK",
                    confirmButtonClass: "btn btn-lg btn-primary",
                    buttonsStyling: false
                });
            }
        });
        
        $("#button-transito").click(function () {
            $("input[name=purchase_id]").val($("input[name=origin_purchase_id]:checked").val());
            $.post("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'transito']) ?>",
                    $("#form-transit").serialize(),
                    function (data, code) {
                        window.location = "<?= $this->Url->build(['action' => 'edit']) ?>";
                    });
        });
        
        $("#button-transferir").click(function () {
            $("#purchase-transfers option").each(function () {
                $(this).remove();
            });
            $("input[id=to-transfer]").each(function () {
                if ($(this).is(":checked")) {
                    $("#purchase-transfers").append("<option value='" + $(this).data("transfer-id") + "' selected></option>");
                }
            });
            if ($("#purchase-transfers option").length > 0) {
                $("#transferir").modal({show: true});
            } else {
                swal({
                    title: "Nenhum contrato em trânsito selecionado",
                    confirmButtonColor: "#079dff",
                    confirmButtonText: "OK",
                    confirmButtonClass: "btn btn-lg btn-primary",
                    buttonsStyling: false
                });
            }
        });
        
        $("#button-transferencia").click(function () {
            switch ($("input[name=tipo_transferencia]:checked").val()) {
                case "existente":
                    if ($("input[name=destination_purchase_id]:checked").val() !== undefined) {
                        $.post("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'transferencia']) ?>",
                                $("#form-transfer").serialize(),
                                function (data, code) {
                                    window.location = "<?= $this->Url->build(['action' => 'edit']) ?>";
                                });
                    } else {
                        swal({
                            title: "Nenhum contrato destino selecionado",
                            confirmButtonColor: "#079dff",
                            confirmButtonText: "OK",
                            confirmButtonClass: "btn btn-lg btn-primary",
                            buttonsStyling: false
                        });
                    }
                    break;
                case "novo":
                    if ($("input[name=numero_contrato]").val() !== "") {
                        $.post("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'transferencia']) ?>",
                                $("#form-transfer").serialize(),
                                function (data, code) {
                                    window.location = "<?= $this->Url->build(['action' => 'edit']) ?>";
                                });
                    } else {
                        swal({
                            title: "Número de contrato não especificado",
                            confirmButtonColor: "#079dff",
                            confirmButtonText: "OK",
                            confirmButtonClass: "btn btn-lg btn-primary",
                            buttonsStyling: false
                        });
                    }
                    break;
            }
        });
        
        $("a[data-transit-id]").click(function () {
            swal({
                title: "Deseja estornar essa transferência em trânsito?",
                showCancelButton: true,
                confirmButtonColor: "#079dff",
                cancelButtonColor: "lightgray",
                confirmButtonText: "SIM",
                cancelButtonText: "NÃO",
                confirmButtonClass: "btn btn-lg btn-primary",
                cancelButtonClass: "btn btn-lg btn-outline-dark mr-3",
                buttonsStyling: false,
                reverseButtons: true,
                allowOutsideClick: false
            }).then((result) => {
                if (result.value) {
                    $.get("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'transito-estorno']) ?>/" + $(this).data("transit-id"),
                            function (data, code) {
                                if (checkstatus(code)) {
                                    window.location = "<?= $this->Url->build(['action' => 'edit']) ?>";
                                }
                            });
                }
            });
        });
        
        $("button[id=button-historico]").click(function () {
            $.get("<?= $this->Url->build(['controller' => 'StockTransfers', 'action' => 'historico']) ?>",
                    function (data, code) {
                        if (checkstatus(code)) {
                            $("div[id=historico-lista]").html(data);
                        }
                    });
        });
        
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Transferências de Estoque']
]);
echo $this->Breadcrumbs->render();
?>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-md-8 col-sm-12">
                        <h5 class="mt-3 mb-3">Contrato Origem</h5>
                        <table class="table table-hover slim-row">
                            <tr>
                                <th></th>
                                <th>Contrato</th>
                                <th>Usina</th>
                                <th class="money">Estoque</th>
                                <th class="money">Preço Litro</th>
                                <th class="money">Valor</th>
                            </tr>
                            <?php
                            if (isset($purchases) and sizeof($purchases) > 0) {
                                foreach ($purchases as $purchase) {
                                    $estoque = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito - $purchase->volume_vendido;
                                    ?>
                                    <tr>
                                        <td><input type="radio" name="origin_purchase_id" value="<?= $purchase->id ?>" data-estoque="<?= $estoque ?>"></td>
                                        <td><?= $purchase->numero_contrato ?></td>
                                        <td><?= $purchase->plant->razao_social ?></td>
                                        <td class="money"><?= number_format($estoque, 0, ',', '.') ?></td>
                                        <td class="money"><?= $this->Number->currency($purchase->preco_litro + $purchase->preco_litro_adicional, null, ['places' => 4]) ?></td>
                                        <td class="money"><?= $this->Number->currency($estoque * ($purchase->preco_litro + $purchase->preco_litro_adicional)) ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td colspan="6">
                                        <button id="button-transitar" class="btn btn-lg btn-primary pull-right">Transitar</button>
                                    </td>
                                </tr>
                                <?php
                            } else {
                                ?>
                                <tr class="no-data-found">
                                    <td colspan="6">Nenhum registro encontrado</td>
                                </tr>
                            <?php } ?>
                        </table>
                        <h5 class="mt-3 mb-3">Volumes em Trânsito</h5>
                        <table class="table table-hover slim-row">
                            <tr>
                                <th></th>
                                <th>Contrato</th>
                                <th>Usina Origem</th>
                                <th>Usina Destino</th>
                                <th class="money">Volume em Trânsito</th>
                                <th></th>
                            </tr>
                            <?php
                            if (isset($transits) and sizeof($transits) > 0) {
                                foreach ($transits as $transit) {
                                    ?>
                                    <tr>
                                        <td><?= $this->Form->control('to_transfer', ['type' => 'checkbox', 'label' => false, 'data-transfer-id' => $transit->id]) ?></td>
                                        <td><?= $transit->purchase->numero_contrato ?></td>
                                        <td><?= $transit->purchase->plant->razao_social ?></td>
                                        <td><?= $transit->plant->razao_social ?></td>
                                        <td class="money"><?= number_format($transit->volume * -1, 0, ',', '.') ?></td>
                                        <td>
                                            <?php
                                            $role = $this->request->getSession()->read('Security.role');
                                            if ($role['super_user']) {
                                                ?>
                                                <a href="javascript:void(0)" data-transit="yes" data-transit-id="<?= $transit->id ?>">ESTORNO</a>
                                                <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td colspan="9">
                                        <button id="button-transferir" class="btn btn-lg btn-primary pull-right">Transferir</button>
                                    </td>
                                </tr>
                                <?php
                            } else {
                                ?>
                                <tr class="no-data-found">
                                    <td colspan="9">Nenhum registro encontrado</td>
                                </tr>
                            <?php } ?>
                        </table>
                        <button type="button" id="button-historico" class="btn btn-outline-dark pull-right" data-toggle="modal" data-target="#historico">Histórico</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Transito-->
<div class="modal fade" id="transitar" tabindex="-1" role="dialog" aria-labelledby="devolucaoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 50% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="devolucaoModalLabel">Transitar</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null, ['id' => 'form-transit']) ?>
                <?= $this->Form->control('purchase_id', ['type' => 'hidden']) ?>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <?= $this->Form->control('plant_id', ['type' => 'select', 'options' => $plants, 'label' => 'Usina Destino']) ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <?= $this->Form->control('carrier_id', ['type' => 'select', 'options' => $carriers, 'label' => 'Transportadora']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('volume', ['type' => 'text', 'label' => 'Volume']) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('frete', ['type' => 'text', 'label' => 'Frete']) ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
            <div class="modal-footer">
                <button id="button-transito" type="button" class="btn btn-lg btn-primary">Colocar em Trânsito</button>
            </div>
        </div>
    </div>
</div>
<!--Transferencia-->
<div class="modal fade" id="transferir" tabindex="-1" role="dialog" aria-labelledby="devolucaoModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 50% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="devolucaoModalLabel">Transferir</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="mt-3 mb-3">Contrato Destino</h5>
                <?= $this->Form->create(null, ['id' => 'form-transfer']) ?>
                <hr>
                <div class="mt-3 mb-3">
                    <input type="radio" name="tipo_transferencia" value="existente" checked> <span style="font-weight: bold">PARA CONTRATO EXISTENTE</span>
                </div>
                <table class="table table-hover slim-row">
                    <tr>
                        <th></th>
                        <th>Contrato</th>
                        <th>Usina</th>
                        <th class="money">Estoque</th>
                        <th class="money">Preço Litro</th>
                        <th class="money">Valor</th>
                    </tr>
                    <?php
                    if (isset($purchases) and sizeof($purchases) > 0) {
                        foreach ($purchases as $purchase) {
                            $estoque = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_transito - $purchase->volume_vendido;
                            ?>
                            <tr>
                                <td>
                                    <input type="radio" name="destination_purchase_id" value="<?= $purchase->id ?>">
                                </td>
                                <td><?= $purchase->numero_contrato ?></td>
                                <td><?= $purchase->plant->razao_social ?></td>
                                <td class="money"><?= number_format($estoque, 0, ',', '.') ?></td>
                                <td class="money"><?= $this->Number->currency($purchase->preco_litro + $purchase->preco_litro_adicional, null, ['places' => 4]) ?></td>
                                <td class="money"><?= $this->Number->currency($estoque * ($purchase->preco_litro + $purchase->preco_litro_adicional)) ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                        <?php
                    } else {
                        ?>
                        <tr class="no-data-found">
                            <td colspan="6">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <hr>
                <div class="mt-3 mb-3">
                    <input type="radio" name="tipo_transferencia" value="novo"> <span style="font-weight: bold">PARA CONTRATO NOVO</span>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('numero_contrato', ['type' => 'text', 'label' => 'Número Contrato']) ?>
                    </div>
                </div>
                <hr>
                <?= $this->Form->control('purchase_transfers', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
                <button id="button-transferencia" type="button" class="btn btn-lg btn-primary pull-right">Efetuar Tranferência</button>
            </div>
        </div>
    </div>
</div>
<!--Historico-->
<div class="modal fade" id="historico" role="dialog">
    <div class="modal-dialog modal-lg" style="max-width: 60% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="historicoModalLabel">Histórico</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="historico-lista"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>