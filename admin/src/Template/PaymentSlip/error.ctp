<script>
    $(document).ready(function() {
        $("a[data-slip-id]").click(function() {
            event.preventDefault();
            $.post("<?= $this->Url->build(['controller' => 'PaymentSlip', 'action' => 'cancel']) ?>",
                $("#form-cancel-" + $(this).data("slip-id")).serialize(),
                function(data, code) {
                    $("#error-area").html(data);
                });
        });
    });
</script>

<?php if (sizeof($registration_slips) > 0) { ?>
    <h3>Erros de Registro</h3>
    <table class="table table-hover">
        <tr>
            <th width="10%">Tipo</th>
            <th width="10%"><?= $this->Paginator->sort('data_vencimento', 'Data Vencimento') ?></th>
            <th width="10%"><?= $this->Paginator->sort('valor', 'Valor') ?></th>
            <th width="30%">Descrição</th>
            <th width="30%">Mensagem do Banco</th>
            <th width="10%"></th>
        </tr>
        <?php
        foreach ($registration_slips as $slip) {
            $receivable = $slip->receivable;
        ?>
            <tr>
                <td><?= $receivable_types[$receivable->tipo] ?></td>
                <td><?= $receivable->data_vencimento ?></td>
                <td style=" text-align: right"><?= $this->Number->currency($receivable->valor) ?></td>
                <td>
                    <?php
                    switch ($receivable->tipo) {
                        case 'VE':
                            $msg = ' Data Venda: ' . $receivable->sale_detail->sale->data_venda . '<br>' .
                                'Cliente: ' . $receivable->sale_detail->presale->client->razao_social . '<br>' .
                                'NF: ' . $receivable->sale_detail->nota_fiscal;
                            echo $msg;
                            break;
                    }
                    ?>
                </td>
                <td>
                    <?php
                    $s = $slip->response;

                    $crl = 0;
                    $ss = false;
                    echo "<pre>";
                    for ($c = 0; $c < strlen($s); $c++) {
                        if ($s[$c] == '}' || $s[$c] == ']') {
                            $crl--;
                            echo "\n";
                            echo str_repeat(' ', ($crl * 2));
                        }
                        if ($s[$c] == '"' && ($s[$c - 1] == ',' || $s[$c - 2] == ',')) {
                            echo "\n";
                            echo str_repeat(' ', ($crl * 2));
                        }
                        if ($s[$c] == '"' && !$ss) {
                            if ($s[$c - 1] == ':' || $s[$c - 2] == ':')
                                echo '<span style="color:red;">';
                            else
                                echo '<span style="color:gray;">';
                        }
                        echo $s[$c];
                        if ($s[$c] == '"' && $ss)
                            echo '</span>';
                        if ($s[$c] == '"')
                            $ss = !$ss;
                        if ($s[$c] == '{' || $s[$c] == '[') {
                            $crl++;
                            echo "\n";
                            echo str_repeat(' ', ($crl * 2));
                        }
                    }
                    // echo $s[$c];
                    ?>
                </td>
                <td>
                    <?php if ($slip->is_canceled) { ?>
                        <span class="badge badge-danger">CANCELADO</span>
                    <?php } else { ?>
                        <?= $this->Form->create(null, ['id' => 'form-cancel-' . $slip->id]) ?>
                        <?= $this->Form->control('slip_id', ['type' => 'hidden', 'value' => $slip->id]) ?>
                        <a href="javascript:void(0)" class="btn btn-primary" data-slip-id="<?= $slip->id ?>">Cancelar</a>
                        <?= $this->Form->end() ?>
                    <?php } ?>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>

<?php if (sizeof($pdf_slips) > 0) { ?>
    <h3>Erros de Geração de PDF</h3>
    <div class="alert alert-danger">Favor consultar o suporte</div>
    <table class="table table-hover">
        <tr>
            <th>Tipo</th>
            <th><?= $this->Paginator->sort('data_vencimento', 'Data Vencimento') ?></th>
            <th><?= $this->Paginator->sort('valor', 'Valor') ?></th>
            <th>Descrição</th>
        </tr>
        <?php
        foreach ($pdf_slips as $slip) {
            $receivable = $slip->receivable;
        ?>
            <tr>
                <td><?= $receivable_types[$receivable->tipo] ?></td>
                <td><?= $receivable->data_vencimento ?></td>
                <td style=" text-align: right"><?= $this->Number->currency($receivable->valor) ?></td>
                <td>
                    <?php
                    switch ($receivable->tipo) {
                        case 'VE':
                            $msg = ' Data Venda: ' . $receivable->sale_detail->sale->data_venda . '<br>' .
                                'Cliente: ' . $receivable->sale_detail->presale->client->razao_social . '<br>' .
                                'NF: ' . $receivable->sale_detail->nota_fiscal;
                            echo $msg;
                            break;
                    }
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
<?php } ?>