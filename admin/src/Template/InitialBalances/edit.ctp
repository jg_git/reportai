<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('input[name=dc]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat',
                increaseArea: '20%'
            });
        }, 500);
        $("#valor").maskMoney({thousands: '.', decimal: ','}).maskMoney('mask', <?= $balance->valor ?>);
        $("input[name=data_saldo]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Saldos Iniciais', 'url' => ['action' => 'index']],
    ['title' => 'Saldo Inicial']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <?=
                $this->Form->create($balance, [
                    'id' => 'form-balance',
                    'data-id' => $balance->isNew() ? 'new' : $balance->id,
                    'data-error' => sizeof($balance->getErrors()) > 0 ? 'Y' : 'N'])
                ?>
                <div class="row">
                    <div class="col-md-9 col-sm-12">
                        <?= $this->Form->control('account_id', ['type' => 'select', 'options' => $accounts, 'label' => 'Conta', 'disabled' => !$balance->isNew()]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-12 mt-4 pl-5">
                        <?= $this->Form->control('dc', ['type' => 'radio', 'options' => $ledger_entry_types, 'label' => false]) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('valor', ['type' => 'text', 'label' => 'Valor']) ?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <?php
                        if (!$balance->isNew()) {
                            echo $this->Form->control('data_saldo', ['type' => 'text', 'label' => 'Data do Saldo']);
                        } else {
                            echo $this->Form->control('data_saldo', ['type' => 'text', 'label' => 'Data do Saldo', 'value' => date('d/m/Y')]);
                        }
                        ?>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col">
                        <?= $this->Form->control('observacao', ['type' => 'textarea', 'label' => 'Observação']) ?>
                    </div>
                </div>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_delete', ['id' => $balance->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>    
    </div>
</div>
