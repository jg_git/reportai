<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Saldos Iniciais']
]);
echo $this->Breadcrumbs->render();
?>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover">
                    <tr>
                        <th>Conta</th>
                        <th>Tipo</th>
                        <th>Valor</th>
                    </tr>
                    <?php
                    if (sizeof($balances) > 0) {
                        foreach ($balances as $balance) {
                            $ac = $balance->account;
                            ?>
                            <tr>
                                <td>
                                    <?php
                                    switch ($ac->tipo) {
                                        case 'I' :
                                            $msg = '[Interna] ' . $ac->distributor->razao_social;
                                            break;
                                        case 'F':
                                            $msg = '[Financeira] ' . $ac->bank->nome . ' - ' . $ac->titular . ' - ' . $ac->numero_agencia . ' - ' . $ac->numero_conta;
                                            break;
                                        case 'C':
                                            $msg = '[Caixa] ' . $ac->titular;
                                            break;
                                    }
                                    echo $this->Html->link($msg, ['controller' => 'InitialBalances', 'action' => 'edit', $balance->id]);
                                    ?>
                                </td>
                                <td><?= $ledger_entry_types[$balance->dc] ?></td>
                                <td><?= $this->Number->currency($balance->valor) ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>
