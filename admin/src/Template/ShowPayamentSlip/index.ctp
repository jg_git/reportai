<!-- <script>
    $(document).ready(function() {

        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
    });
</script> -->
<script>
function hideAll(){
    confirm("Você deseja imprimir todos os boletos selecionados?");
}
    </script>

<?php

use App\Controller\ShowPayamentSlipController;

$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Impressão de boletos']
]);
echo $this->Breadcrumbs->render();

?>
<div class="col"><div class="col">
    <div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
    <h3><center>IMPRESSÃO DE BOLETOS</center></h3>
    <?= $this->Form->create(null, ['url' => ['action' => 'gerarPdfSelecionados']]); ?>
    <?= $this->Form->submit(
        'BAIXAR BOLETOS SELECIONADOS',
             ['onclick' => 'hideAll()', 'class' => 'btn btn-outline-primary statement']
    ); ?>
</div>
  <div class="col-md-12 col-sm-12">
    <table class="table table-hover extra-slim-row">

        <thead>
        <th>SELECIONAR</th>
        <th> Nosso Numero </th>
        <th> Data Vencimento</th>
        <th>Data Emissão</th>
        <th>Ação</th>
        </thead>

        <?php foreach ($payamentSlips as $payamentSlip) : ?>
            <tr <?= $payamentSlip->boleto_emitido ? ' style="background-color: lightgray"' : '' ?> >
                <td style="text-align: center">
                    <?php
                    echo $this->Form->control('flag.'.$payamentSlip->id, [
                        'type' => 'checkbox',
                        'label' => false,
                        'data-payable-id' => $payamentSlip->id,
                        
                    ]);
                    ?>
                </td>
                <td><?= $payamentSlip->nosso_numero ?></td>
                <td><?= $payamentSlip->data_vencimento ?></td>
                <td><?= $payamentSlip->data_emissao ?></td>
                <td>
                    <?= $this->Form->button(
                        'DOWNLOAD DO BOLETO',
                        array('type' => 'submit', 'formaction' => ['show-payament-slip/gerarPdf/',$payamentSlip->id], 'class' => 'btn btn-outline-primary statement')
                    ); ?>
                </td>
                </tr>
        <?php endforeach; ?>

        <?php if (count($payamentSlips) == 0) : ?>
            <tr>
                <td colspan="6" class="no-data-found">Nenhum registro encontrado</td>
            </tr>
        <?php
        endif; ?>

    </table>
    </div>
<?php echo $this->Form->end(); ?>