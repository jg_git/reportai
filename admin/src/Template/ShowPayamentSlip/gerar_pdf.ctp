<!-- <script>

var fromPHP=<//?$payamentSlips ?>;

</script> -->

<?php
// +--------------------------------------------------------------------------------------------------------------+


// ------------------------- DADOS DINÂMICOS DO SEU CLIENTE PARA A GERAÇÃO DO BOLETO (FIXO OU VIA GET) -------------------- //
// Os valores abaixo podem ser colocados manualmente ou ajustados p/ formulário c/ POST, GET ou de BD (MySql,Postgre,etc)	//



foreach ($payamentSlips as $payamentSlip) {

	$datanew3 =   str_replace("/", "-", $payamentSlip->data_vencimento);
	$datanew2 = strtotime($datanew3);
	$datanew = strtotime($payamentSlip->data_emissao);

	$data_documento = str_replace("/", "-", $datanew);

	// DADOS DO BOLETO PARA O SEU CLIENTE
	$dias_de_prazo_para_pagamento = 5;
	$taxa_boleto = 0;
	$data_venc = date("d/m/Y",  $datanew2); //date("d/m/Y", time() + ($dias_de_prazo_para_pagamento * 86400)); // Prazo de X dias OU informe data: "13/04/2006";
	$valor_cobrado = $valor_cobrado = $payamentSlip->valor_cobrado; // Valor - REGRA: Sem pontos na milhar e tanto faz com "." ou "," ou com 1 ou 2 ou sem casa decimal
	$valor_cobrado = str_replace(",", ".", $valor_cobrado);
	$numero_formatado = substr($payamentSlip->valor_cobrado, 0, 15) . "." . substr($payamentSlip->valor_cobrado, 15);
	// dump($numero_formatado);
	// die();
	// $valor_cobrado2 = number_format($num / 100);
	$valor_boleto = number_format($numero_formatado + $taxa_boleto, 2, ',', '');
	$dadosboleto["nosso_numero"] = $payamentSlip->nosso_numero . $payamentSlip->nosso_numero_digito_verificador; // Nosso numero - REGRA: Máximo de 8 caracteres! Itau api doc -> ? 123/45678901-5
	$dadosboleto["numero_documento"] = $payamentSlip->nota_fiscal; // Num do pedido ou nosso numero



	$dadosboleto["data_vencimento"] = $data_venc; // Data de Vencimento do Boleto - REGRA: Formato DD/MM/AAAA
	$dadosboleto["data_documento"] =  date("d/m/Y", $data_documento); // date("d/m/Y"); // Data de emissão do Boleto
	$dadosboleto["data_processamento"] = date("d/m/Y"); // Data de processamento do boleto (opcional)
	$dadosboleto["valor_boleto"] = $valor_boleto; // Valor do Boleto - REGRA: Com vírgula e sempre com duas casas depois da virgula
	// DADOS DO SEU CLIENTE
	$dadosboleto["sacado"] = $payamentSlip->nome_pagador;
	$dadosboleto["endereco1"] = $payamentSlip->logradouro_pagador;
	$dadosboleto["endereco2"] = $payamentSlip->cidade_pagador . ' - ' . $payamentSlip->uf_pagador . ' - ' . $payamentSlip->cep_pagador;
	// INFORMACOES PARA O CLIENTE
	$dadosboleto["demonstrativo1"] = "Pagamento de compra com a RBL";
	$dadosboleto["demonstrativo2"] = "Pagamento referente <br>Taxa bancária - R$ " . number_format($taxa_boleto, 2, ',', '');
	$dadosboleto["demonstrativo3"] = "BoletoRBL";

	// INSTRUÇÕES PARA O CAIXA
	$dadosboleto["instrucoes1"] = "- Sr. Caixa, cobrar multa de 2% após o vencimento";
	$dadosboleto["instrucoes2"] = "- Receber até 10 dias após o vencimento";
	$dadosboleto["instrucoes3"] = "- AS INFORMAÇÕES
DESTE BOLETO SÃO DE EXCLUSIVA RESPONSABILIDADE DO BENEFICIÁRIO ";
	$dadosboleto["instrucoes4"] = "&nbsp;Em caso de dúvidas entre em contato conosco: gerencialrbl@outlook.com";

	// DADOS OPCIONAIS DE ACORDO COM O BANCO OU CLIENTE
	$dadosboleto["quantidade"] = "";
	$dadosboleto["valor_unitario"] = "";
	$dadosboleto["aceite"] = "";
	$dadosboleto["especie"] = "R$";
	$dadosboleto["especie_doc"] = "";

	// ---------------------- DADOS FIXOS DE CONFIGURAÇÃO DO SEU BOLETO --------------- //
	// DADOS DA SUA CONTA - ITAÚ


	$dadosboleto["agencia"] = "6452"; // Num da agencia, sem digito
	$dadosboleto["conta"] = "11978"; // Num da conta, sem digito
	$dadosboleto["conta_dv"] = "4"; // Digito do Num da conta
	// DADOS PERSONALIZADOS - ITAÚ
	$dadosboleto["carteira"] = "109"; // Código da Carteira: pode ser 175, 174, 104, 109, 178, ou 157

	// SEUS DADOS
	$dadosboleto["identificacao"] = "BoletoRBL - CB Sistema de Boletos";
	$dadosboleto["cpf_cnpj"] = " 34530660/0001-04";
	$dadosboleto["endereco"] = "Dr Leonce Pinheiro, 270";
	$dadosboleto["cidade_uf"] = "Itapetininga / SP";
	$dadosboleto["cedente"] = "GLOBAL CB R C COBRANCA EIRELI";

	ob_start();

	// NÃO ALTERAR!
	require_once("include/funcoes_itau.php");
	require_once("include/layout_sicredi.php");

	$content = ob_get_clean();

	// convert
	require_once(dirname(__FILE__) . '/html2pdf/html2pdf.class.php');
	try {
		$html2pdf = new HTML2PDF('P', 'A4', 'fr', array(0, 0, 0, 0));
		/* Abre a tela de impressão */
		//$html2pdf->pdf->IncludeJS("print(true);");

		$html2pdf->pdf->SetDisplayMode('real');

		/* Parametro vuehtml = true desabilita o pdf para desenvolvimento do layout */
		$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
		
// $html2pdf.addPage();
		/* Abrir no navegador */
		ob_end_clean();
		// $html2pdf->Output('boleto.pdf');
		$html2pdf->Output($payamentSlip->nome_pagador . '_' . $payamentSlip->data_emissao . '_boleto.pdf', 'D');

		/* Salva o PDF no servidor para enviar por email */
		//$html2pdf->Output('caminho/boleto.pdf', 'F');

		/* Força o download no browser */
	} catch (HTML2PDF_exception $e) {
		echo $e;
		exit;
	}


} 


// $this->setAction('index');