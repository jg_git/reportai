<!-- <script>
    $(document).ready(function() {

        $("input:checkbox").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });
    });
</script> -->

<?php

use App\Controller\ShowPayamentSlipController;

$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Impressão de boletos']
]);
echo $this->Breadcrumbs->render();

?>
<div class="col"><div class="col">
    <h3>IMPRESSÃO DE BOLETOS</h3>
    <?= $this->Form->create(null, ['url' => ['action' => 'gerarPdfSelecionados']]); ?>
    <?= $this->Form->submit(
        'BAIXAR BOLETOS SELECIONADOS',
        ['action' => 'gerarPdfSelecionados'],
        ['confirm' => 'Deseja Imprimir Todos os Boletos Selecionados?', 'class' => 'btn btn-outline-primary statement']
    ); ?>
</div>

    <table class="table table-hover extra-slim-row">

        <thead>
        <th>SELECIONAR</th>
        <th> Nosso Numero </th>
        <th> Data Vencimento</th>
        <th>Data Emissão</th>
        <th>Ação</th>
        </thead>

        <?php foreach ($payamentSlips as $payamentSlip) : ?>
            <tr <?= $payamentSlip->boleto_emitido ? ' style="background-color: lightgray"' : '' ?> >
                <td style="text-align: center">
                    <?php
                    echo $this->Form->control('flag.'.$payamentSlip->nosso_numero, [
                        'type' => 'checkbox',
                        'label' => false,
                        'data-payable-id' => $payamentSlip->id,
                    ]);
                    ?>
                </td>
                <td><?= $payamentSlip->nosso_numero ?></td>
                <td><?= $payamentSlip->data_vencimento ?></td>
                <!-- <td <//?= '$this->element(\'numero_formatado\');' ?></td> -->
                <td><?= $payamentSlip->data_emissao ?></td>
                <td>
                    <?= $this->Form->button(
                        'DOWNLOAD DO BOLETO',
                        array('type' => 'submit', 'formaction' => ['gerarPdf/',$payamentSlip->id])
                    ); ?>
                </td>
                
            </tr>
        <?php endforeach; ?>

        <?php if (count($payamentSlips) == 0) : ?>
            <tr>
                <td colspan="6" class="no-data-found">Nenhum registro encontrado</td>
            </tr>
        <?php
        endif; ?>

    </table>
<?php echo $this->Form->end(); ?>