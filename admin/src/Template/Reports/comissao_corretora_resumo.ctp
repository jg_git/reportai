<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Comissões de Corretoras</h4>
<h5 class="mt-3 mb-3">Período: <?= $data['data_referencia_range'] ?></h5>
<h5 class="mt-3 mb-3">Comissões Corretoras (Resumido)</h5>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th width="25%">Corretora</th>
        <th width="55%">Contratos</th>
        <th width="20%" class="money">Comissão</th>
    </tr>
    <?php
    if (isset($rows) and sizeof($rows) > 0) {
        foreach ($rows as $row) {
            ?>
            <tr>
                <td><?= $row['corretora'] ?></td>
                <td><?= rtrim($row['contratos'], ', ') ?></td>
                <td class="money"><?= $this->Number->currency($row['comissao']) ?></td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>
<hr>
<h5 class="mt-3 mb-3">Comissões Corretoras (Detalhado)</h5>
<?php
if (isset($rows) and sizeof($rows) > 0) {
    foreach ($rows as $row) {
        $purchases = $row['purchases'];
        ?>
        <h5 class="mt-3 mb-3">Corretora: <?= $row['corretora'] ?></h5>
        <table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
            <tr>
                <th width="50%" style="text-align: center">Contrato</th>
                <th class="money">Valor</th>
            </tr>
            <?php
            foreach ($purchases as $purchase) {
                $total = 0;
                $contrato = '';
                foreach ($comissoes as $comissao) {
                    if ($comissao->sales_purchase->purchase->broker_id == $row['broker_id']) {
                        if ($comissao->sales_purchase->purchase_id == $purchase) {
                            $total += $comissao->valor;
                            $contrato = $comissao->sales_purchase->purchase->numero_contrato;
                        }
                    }
                }
                foreach ($comissao_transferencias as $comissao) {
                    if ($comissao->purchase_transfer->purchase->broker_id == $row['broker_id']) {
                        if ($comissao->purchase_transfer->purchase_id == $purchase) {
                            $total += $comissao->valor;
                            $contrato = $comissao->purchase_transfer->purchase->numero_contrato;
                        }
                    }
                }
                ?>
                <tr>
                    <td style="text-align: center"><?= $contrato ?></td>
                    <td class="money"><?= $this->Number->currency($total) ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }
} else {
    ?>
    Nenhum registro encontrado
<?php } ?>


<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>
