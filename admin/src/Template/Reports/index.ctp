<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {

        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function (e) {
            $("input[name=" + $(this).attr("name") + "_value]").val("Y");
        }).on('ifUnchecked', function (e) {
            $("input[name=" + $(this).attr("name") + "_value]").val("N");
        });

        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        var engine = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: "<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'city']) ?>/%QUERY",
                wildcard: '%QUERY'
            }
        });
        $("#cidade").typeahead(null, {
            source: engine,
            display: "name",
            templates: {
                notFound: '<div style="margin-left:10px">Cidade não encontrada</div>',
                suggestion: Handlebars.compile(
                        '<div class="typeahead-item">' +
                        '{{name}}<br>' +
                        '</div>')
            }
        }).on('typeahead:asyncrequest', function () {
            $('#cidade').addClass("loading");
        }).on('typeahead:asynccancel typeahead:asyncreceive', function () {
            $('#cidade').removeClass("loading");
        });

        routing = [
            {key: "evolucao-patrimonial", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'evolucao-patrimonial']) ?>"},
            {key: "demonstrativo", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'demonstrativo']) ?>"},
            {key: "resumo-diario", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'resumo-diario']) ?>"},
            {key: "comissao-corretora-resumo", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'comissao-corretora-resumo']) ?>"},
            {key: "comissao-vendedor-resumo", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'comissao-vendedor-resumo']) ?>"},
            {key: "spread-resumo", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'spread-resumo']) ?>"},
            {key: "frete-resumo", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'frete-resumo']) ?>"},
            {key: "estoque-abertura", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'estoque-abertura']) ?>"},
            {key: "compra", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'compra']) ?>"},
            {key: "perdas-sobras", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'perdas-sobras']) ?>"},
            {key: "vendas-cliente", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'vendas-cliente']) ?>"},
            {key: "vendas-vendedor", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'vendas-vendedor']) ?>"},
            {key: "cliente", route: "<?= $this->Url->build(['controller' => 'Reports', 'action' => 'cliente']) ?>"}
        ];

        for (i = 0; i < routing.length; i++) {
            createDatePicker(routing[i].key, "data_referencia", configRangePicker);
        }

        $("button[data-route]").click(function () {
            route = $(this).data("route");
            $("#pdf-type").val(route);
            switch (route) {
                case "estoque-abertura":
                case "cliente":
                    $("#report").modal();
                    $.post(routing.find(x => x.key === route).route,
                            $("#form-" + route).serialize(),
                            function (data, code) {
                                $("#report-area").html(data);
                            });
                    break;
                default:
                    if ($("#form-" + route + " input[name=data_referencia_de]").val() !== "" && $("#form-" + route + " input[name=data_referencia_ate]").val() !== "") {
                        $("#report").modal();
                        $.post(routing.find(x => x.key === route).route,
                                $("#form-" + route).serialize(),
                                function (data, code) {
                                    $("#report-area").html(data);
                                });
                    } else {
                        swal({
                            title: "Período Não Especificado",
                            confirmButtonColor: "#079dff",
                            confirmButtonText: "OK",
                            confirmButtonClass: "btn btn-primary",
                            buttonsStyling: false
                        });
                    }
            }
        });

        $("#pdf").click(function () {
            route = $("#pdf-type").val();
            switch (route) {
                case "estoque-abertura":
                    window.open(routing.find(x => x.key === route).route + "/report.pdf");
                    break
                case "cliente":
                    window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                            "?cidade=" + $("#form-" + route + " input[name=cidade]").val(),
                            "_blank"
                            );
                    break
                case "comissao-corretora-resumo":
                    if ($("#form-" + route + " select[name=corretora_id]").val() !== "") {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&broker_id=" + $("#form-" + route + " select[name=broker_id]").val() +
                                "&a_pagar_corretora=" + $("#form-" + route + " input[name=a_pagar_corretora_value]").val(),
                                "_blank"
                                );
                    } else {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&a_pagar_corretora=" + $("#form-" + route + " input[name=a_pagar_corretora_value]").val(),
                                "_blank"
                                );
                    }
                    break
                case "comissao-vendedor-resumo":
                    if ($("#form-" + route + " select[name=seller_id]").val() !== "") {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&seller_id=" + $("#form-" + route + " select[name=seller_id]").val() +
                                "&a_pagar_vendedor=" + $("#form-" + route + " input[name=a_pagar_vendedor_value]").val(),
                                "_blank"
                                );
                    } else {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&a_pagar_vendedor=" + $("#form-" + route + " input[name=a_pagar_vendedor_value]").val(),
                                "_blank"
                                );
                    }
                    break
                case "spread-resumo":
                    if ($("#form-" + route + " select[name=distributor_id]").val() !== "") {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&distributor_id=" + $("#form-" + route + " select[name=distributor_id]").val() +
                                "&a_pagar_spread=" + $("#form-" + route + " input[name=a_pagar_spread_value]").val(),
                                "_blank"
                                );
                    } else {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&a_pagar_spread=" + $("#form-" + route + " input[name=a_pagar_spread_value]").val(),
                                "_blank"
                                );
                    }
                    break
                case "frete-resumo":
                    if ($("#form-" + route + " select[name=carrier_id]").val() !== "") {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&carrier_id=" + $("#form-" + route + " select[name=carrier_id]").val() +
                                "&a_pagar_frete=" + $("#form-" + route + " input[name=a_pagar_frete_value]").val(),
                                "_blank"
                                );
                    } else {
                        window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                                "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                                "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                                "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                                "&a_pagar_frete=" + $("#form-" + route + " input[name=a_pagar_frete_value]").val(),
                                "_blank"
                                );
                    }
                    break
                case "compra":
                    window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                            "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                            "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                            "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val() +
                            "&distributor_id=" + $("#form-" + route + " select[name=distributor_id]").val() +
                            "&plant_id=" + $("#form-" + route + " select[name=plant_id]").val(),
                            "_blank"
                            );
                    break
                default:
                    window.open(routing.find(x => x.key === route).route + "/report.pdf" +
                            "?data_referencia_range=" + $("#form-" + route + " input[name=data_referencia_range]").val() +
                            "&data_referencia_de=" + $("#form-" + route + " input[name=data_referencia_de]").val() +
                            "&data_referencia_ate=" + $("#form-" + route + " input[name=data_referencia_ate]").val(),
                            "_blank"
                            );
            }
        });

    });

    function createDatePicker(form, name, configRangePicker) {
        $("#form-" + form + " input[name=" + name + "_range]").daterangepicker(configRangePicker, function (start, end, label) {

        }).on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("#form-" + form + " input[name=" + name + "_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("#form-" + form + " input[name=" + name + "_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function (ev, picker) {
            $(this).val("");
            $("#form-" + form + " input[name=" + name + "_de]").val("");
            $("#form-" + form + " input[name=" + name + "_ate]").val("");
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Relatórios']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <h5 class="mt-3 mb-3">Balancetes</h5>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-evolucao-patrimonial']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="evolucao-patrimonial">Evolução Patrimonial</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-demonstrativo']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="demonstrativo">Demonstrativo de Resultado Exercício</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-resumo-diario']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="resumo-diario">Resumo Diário</button>
            </div>
        </div>
        <hr>
        <h5 class="mt-3 mb-3">Financeiros e Administrativos</h5>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-comissao-corretora-resumo']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->control('broker_id', ['type' => 'select', 'options' => $brokers, 'label' => 'Corretora', 'empty' => true]) ?>
                <?= $this->Form->control('a_pagar_corretora', ['type' => 'checkbox', 'label' => 'A Pagar']) ?>
                <?= $this->Form->control('a_pagar_corretora_value', ['type' => 'hidden', 'value' => 'N']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary mt-2" data-route="comissao-corretora-resumo">Comissões Corretoras</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-comissao-vendedor-resumo']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->control('seller_id', ['type' => 'select', 'options' => $sellers, 'label' => 'Vendedor', 'empty' => true]) ?>
                <?= $this->Form->control('a_pagar_vendedor', ['type' => 'checkbox', 'label' => 'A Pagar']) ?>
                <?= $this->Form->control('a_pagar_vendedor_value', ['type' => 'hidden', 'value' => 'N']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary mt-2" data-route="comissao-vendedor-resumo">Comissões Vendedores</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-spread-resumo']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->control('distributor_id', ['type' => 'select', 'options' => $distributors, 'label' => 'Distribuidora', 'empty' => true]) ?>
                <?= $this->Form->control('a_pagar_spread', ['type' => 'checkbox', 'label' => 'A Pagar']) ?>
                <?= $this->Form->control('a_pagar_spread_value', ['type' => 'hidden', 'value' => 'N']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary mt-2" data-route="spread-resumo">Spread</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-frete-resumo']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->control('carrier_id', ['type' => 'select', 'options' => $carriers, 'label' => 'Transportadora', 'empty' => true]) ?>
                <?= $this->Form->control('a_pagar_frete', ['type' => 'checkbox', 'label' => 'A Pagar']) ?>
                <?= $this->Form->control('a_pagar_frete_value', ['type' => 'hidden', 'value' => 'N']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary mt-2" data-route="frete-resumo">Frete</button>
            </div>
        </div>
        <hr>
        <h5 class="mt-3 mb-3">Estoque</h5>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-estoque-abertura']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="estoque-abertura">Estoque Abertura</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-compra']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->control('distributor_id', ['type' => 'select', 'options' => $distributors, 'label' => 'Distribuidora', 'empty' => true]) ?>
                <?= $this->Form->control('plant_id', ['type' => 'select', 'options' => $plants, 'label' => 'Usina', 'empty' => true]) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="compra">Compras</button>
            </div>
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-perdas-sobras']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="perdas-sobras">Perdas e Sobras</button>
            </div>
        </div>
        <hr>
        <h5 class="mt-3 mb-3">Vendas</h5>
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <?= $this->Form->create(null, ['id' => 'form-vendas-cliente']) ?>
                <?= $this->Form->control('data_referencia_range', ['type' => 'text', 'label' => 'Periodo']) ?>
                <?= $this->Form->control('data_referencia_de', ['type' => 'hidden']) ?>
                <?= $this->Form->control('data_referencia_ate', ['type' => 'hidden']) ?>
                <?= $this->Form->end() ?>
                <button type="button" class="btn btn-primary" data-route="vendas-cliente">Vendas Por Cliente</button>
            </div>
          
        </div>
    </div>
</div>

<div class="modal fade" id="report" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80% !important;">
        <div class="modal-content">
            <div class="modal-body">
                <div id="report-area" class="p-3"></div>
            </div>
            <div class="modal-footer">
                <?= $this->Form->control('pdf-type', ['type' => 'hidden']) ?>
                <button id="pdf" type="button" class="btn btn-primary">Gerar PDF</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>
