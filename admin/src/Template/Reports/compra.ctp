<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Compras</h4>
<h5 class="mt-3 mb-3">Período: <?= $data['data_referencia_range'] ?></h5>
<?php if (!is_null($distributor)) { ?>
    <h5 class="mt-3 mb-3">Distribuidora: <?= $distributor->razao_social ?></h5>
<?php } ?>
<?php if (!is_null($plant)) { ?>
    <h5 class="mt-3 mb-3">Usina: <?= $plant->razao_social ?></h5>
<?php } ?>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th style="text-align: center">Data</th>
        <?php if (is_null($distributor)) { ?>
            <th>Distribuidora</th>
        <?php } ?>
        <?php if (is_null($plant)) { ?>
            <th>Usina</th>
        <?php } ?>
        <th>Contrato</th>
        <th class="money">Volume</th>
        <th class="money">Custo</th>
        <th class="money">Total</th>
    </tr>
    <?php
    if (isset($purchases) and sizeof($purchases) > 0) {
        $total_volume = 0;
        $total_financeiro = 0;
        foreach ($purchases as $purchase) {
//            $estoque = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido;
            $total_volume += $purchase->volume_comprado;
//            $total_financeiro += $estoque * ($purchase->preco_litro + $purchase->preco_litro_adicional);
            $total_financeiro += $purchase->volume_comprado * $purchase->preco_litro;
            ?>
            <tr>
                <td style="text-align: center"><?= !is_null($purchase->data_compra) ? date('d/m/Y', strtotime(str_replace('/', '-', $purchase->data_compra))) : '' ?></td>
                <?php if (is_null($distributor)) { ?>
                    <td><?= $purchase->distributor->razao_social ?></td>
                <?php } ?>
                <?php if (is_null($plant)) { ?>
                    <td><?= $purchase->plant->razao_social ?></td>
                <?php } ?>
                <td><?= $purchase->numero_contrato ?></td>
                <td class="money"><?= number_format($purchase->volume_comprado, 0, ',', '.') ?></td>
                <td class="money"><?= $this->Number->currency($purchase->preco_litro, null, ['places' => 4]) ?></td>
                <td class="money"><?= $this->Number->currency($purchase->volume_comprado * $purchase->preco_litro) ?></td>
            </tr>
            <?php
        }
        ?>
        <tr style="font-weight: bold">
            <?php if (is_null($distributor) and is_null($plant)) { ?>
                <td colspan="4" class="money">Total</td>
            <?php } else if (!is_null($distributor) and ! is_null($plant)) { ?>
                <td colspan="2" class="money">Total</td>
            <?php } else { ?>
                <td colspan="3" class="money">Total</td>
            <?php } ?>
            <td class="money"><?= number_format($total_volume, 0, ',', '.') ?></td>
            <td></td>
            <td class="money"><?= $this->Number->currency($total_financeiro) ?></td>
        </tr>
        <?php
    } else {
        ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>