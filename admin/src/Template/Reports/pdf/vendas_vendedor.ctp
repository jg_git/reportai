<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Vendas por Vendedor</h4>
<h5 class="mt-3 mb-3">Período: <?= $data['data_referencia_range'] ?></h5>

<?php
foreach ($rows as $seller_id => $row) {
    ?>
    <div class="row">
        <div class="col-md-6 col-sm-12">
            <h5>Vendedor: <?= $row['vendedor'] ?></h5>
            <h5>Volume Total: <?= number_format($row['volume'], 0, ',', '.') ?></h5>
        </div>
    </div>
    <table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
        <tr>
            <th style="text-align: center">Data Venda</th>
            <th>Cliente</th>
            <th class="money">Volume</th>
        </tr>
        <?php
        foreach ($sales as $sale) {
            foreach ($sale->sale_details as $detail) {
                if ($seller_id == $detail->presale->client->seller_id) {
                    ?>
                    <tr>
                        <td style="text-align: center"><?= $sale->data_venda ?></td>
                        <td><?= $detail->presale->client->razao_social ?></td>
                        <td class="money"><?= number_format($detail->volume, 0, ',', '.') ?></td>
                    </tr>
                    <?php
                }
            }
        }
        ?>
    </table>
    <?php
}
?>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>