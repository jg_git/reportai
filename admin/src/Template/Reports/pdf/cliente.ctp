<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Clientes</h4>
<?php if ($cidade != '') { ?>
    <h5 class="mt-3 mb-3">Cidade: <?= $cidade ?></h5>
<?php } ?>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th>Razão Social</th>
        <th>CNPJ</th>
        <th>Emails</th>
        <th>Telefones</th>
    </tr>
    <?php
    if (isset($clients) and sizeof($clients) > 0) {
        foreach ($clients as $client) {
            ?>
            <tr>
                <td><?= $client->razao_social ?></td>
                <td><?= $client->cnpj ?></td>
                <td>
                    <?php
                    $msg = '';
                    if (!is_null($client->client_emails)) {
                        foreach ($client->client_emails as $email) {
                            $msg .= $email->email . ', ';
                        }
                    }
                    echo rtrim($msg, ', ');
                    ?>
                </td>
                <td>
                    <?php
                    $msg = '';
                    if (!is_null($client->client_phones)) {
                        foreach ($client->client_phones as $phone) {
                            $msg .= '(' . $phone->ddd . ') ' . $phone->telefone . ', ';
                        }
                    }
                    echo rtrim($msg, ', ');
                    ?>
                </td>
                <?php
            }
        } else {
            ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>