<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Perdas e Sobras</h4>
<h5 class="mt-3 mb-3">Posição: <?= $data['data_referencia_range'] ?></h5>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th width="20%" style="text-align: center">Data</th>
        <th width="16%" class="money">Vendas</th>
        <th width="16%" class="money">Venda Conciliada</th>
        <th width="16%" class="money">Saldo</th>
    </tr>
    <?php
    $vendas_total = 0;
    $estoque_inicial_total = 0;
    $compras_total = 0;
    $estoque_final_total = 0;
    $saldo_total = 0;
    foreach ($resumos as $resumo) {
        $vendas_total += $resumo['vendas'];
        $estoque_inicial_total += $resumo['vendas_conciliadas'];
        // $compras_total += $resumo['compras'];
        // $estoque_final_total += $resumo['estoque_final'];
        $saldo = $resumo['vendas'] - $resumo['vendas_conciliadas'];
        $saldo_total += $saldo;
        ?>
        <tr>
            <td style="text-align: center"><?= date('d/m/Y', strtotime($resumo['data'])) ?></td>
            <td class="money"><?= number_format($resumo['vendas'], 0, ',', '.') ?></td>
            <td class="money"><?= number_format($resumo['vendas_conciliadas'], 0, ',', '.') ?></td>
            <td class="money"><?= number_format($saldo, 0, ',', '.') ?></td>
        </tr>
    <?php
    }
    ?>
    <tr style="background-color: lightblue; font-weight: bold">
        <td>Total</td>
        <td class="money"><?= number_format($vendas_total, 0, ',', '.') ?></td>
        <td class="money"><?= number_format($estoque_inicial_total, 0, ',', '.') ?></td>
        <td class="money"><?= number_format($saldo_total, 0, ',', '.') ?></td>
    </tr>
</table>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>