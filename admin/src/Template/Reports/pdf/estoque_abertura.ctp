<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Estoque de Abertura</h4>
<h5 class="mt-3 mb-3">Data: <?= date('d/m/Y', strtotime("+1 days")) ?></h5>
<?php
$volume_total = 0;
$valor_total = 0;
foreach ($rows as $distributor_id => $row) {
    ?>
    <h5 class="mt-3 mb-3">Distribuidora: <?= $row['distribuidora'] ?> - Volume: <?= number_format($row['volume'], 0, ',', '.') ?></h5>
    <table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
        <tr>
            <th width="40%">Usina</th>
            <th width="15%">Contrato</th>
            <th width="15%" class="money">Custo</th>
            <th width="15%" class="money">Volume</th>
            <th width="15%" class="money">Valor</th>
        </tr>
        <?php
        foreach ($purchases as $purchase) {
            if ($purchase->distributor_id == $distributor_id and in_array($purchase->id, $contratos)) {
                $estoque = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_vendido;
                $volume_total += $estoque;
                $valor_total += ($purchase->preco_litro + $purchase->preco_litro_adicional) * $estoque;
                ?>
                <tr>
                    <td><?= $purchase->plant->razao_social ?></td>
                    <td><?= $purchase->numero_contrato ?></td>
                    <td class="money"><?= $this->Number->currency($purchase->preco_litro + $purchase->preco_litro_adicional, null, ['places' => 4]) ?></td>
                    <td class="money"><?= number_format($estoque, 0, ',', '.') ?></td>
                    <td class="money"><?= $this->Number->currency(($purchase->preco_litro + $purchase->preco_litro_adicional) * $estoque) ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </table>
    <?php
}
?>
<div style="font-weight: bold; text-align: right">
    Estoque Total: <?= number_format($volume_total, 2, ',', '.') ?><br>
    Valor Total: <?= $this->Number->currency($valor_total) ?><br>
    Preço Médio: <?= $this->Number->currency($valor_total / $volume_total) ?>
</div>
    
<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>