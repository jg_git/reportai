<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Evolução Patrimonial</h4>

<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <td width="50%">Posição</td>   
        <td width="25%" class="money"><?= date('d/m/Y', strtotime($data['data_referencia_de'])) ?></td>
        <td width="25%" class="money"><?= date('d/m/Y', strtotime($data['data_referencia_ate'])) ?></td>
    </tr>
    <?php
    $saldo_ativo_de = 0;
    $saldo_ativo_ate = 0;
    foreach ($saldos as $saldo) {
        if ($saldo['saldo_de'] > 0) {
            $saldo_ativo_de += $saldo['saldo_de'];
        }
        if ($saldo['saldo_ate'] > 0) {
            $saldo_ativo_ate += $saldo['saldo_ate'];
        }
    }
    foreach ($contas_receber as $entry) {
        $saldo_ativo_de += $entry['saldo_de'];
        $saldo_ativo_ate += $entry['saldo_ate'];
    }
    foreach ($ativo_estoques as $estoque) {
        $saldo_ativo_de += $estoque['estoque_de'] * $estoque['preco_litro_de'];
        $saldo_ativo_ate += $estoque['estoque_ate'] * $estoque['preco_litro_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: blue; color: white">
        <td>Ativo</td>
        <td class="money"><?= $this->Number->currency($saldo_ativo_de) ?></td>
        <td class="money"><?= $this->Number->currency($saldo_ativo_ate) ?></td>
    </tr>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($saldos as $entry) {
        $subtotal_de += $entry['saldo_de'] > 0 ? $entry['saldo_de'] : 0;
        $subtotal_ate += $entry['saldo_ate'] > 0 ? $entry['saldo_ate'] : 0;
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Contas</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    foreach ($saldos as $saldo) {
        if (($saldo['saldo_de'] > 0) or ( $saldo['saldo_ate'] > 0)) {
            ?>
            <tr>
                <td><?= $saldo['conta'] ?></td>
                <td class="money"><?= $saldo['saldo_de'] > 0 ? $this->Number->currency($saldo['saldo_de']) : '---' ?></td>
                <td class="money"><?= $saldo['saldo_ate'] > 0 ? $this->Number->currency($saldo['saldo_ate']) : '---' ?></td>
            </tr>
            <?php
        }
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($contas_receber as $entry) {
        $subtotal_de += $entry['saldo_de'];
        $subtotal_ate += $entry['saldo_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Contas a Receber</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    foreach ($contas_receber as $entry) {
        ?>
        <tr>
            <td><?= $entry['conta'] ?></td>
            <td class="money"><?= $entry['saldo_de'] > 0 ? $this->Number->currency($entry['saldo_de']) : '---' ?></td>
            <td class="money"><?= $entry['saldo_ate'] > 0 ? $this->Number->currency($entry['saldo_ate']) : '---' ?></td>
        </tr>
        <?php
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($ativo_estoques as $entry) {
        $subtotal_de += $entry['estoque_de'] * $entry['preco_litro_de'];
        $subtotal_ate += $entry['estoque_ate'] * $entry['preco_litro_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Estoques</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    $distributors = [];
    foreach ($ativo_estoques as $estoque) {
        if (!array_key_exists($estoque['distributor_id'], $distributors)) {
            $distributors[$estoque['distributor_id']] = $estoque['distribuidora'];
        }
    }
    foreach ($distributors as $distributor_id => $distributor) {
        ?>
        <tr>
            <td colspan="3"><b>Distribuidora: <?= $distributor ?></b></td>
        </tr>
        <?php
        foreach ($ativo_estoques as $estoque) {
            if ($estoque['distributor_id'] == $distributor_id) {
                $valor_de = $estoque['estoque_de'] * $estoque['preco_litro_de'];
                $valor_ate = $estoque['estoque_ate'] * $estoque['preco_litro_ate'];
                ?>
                <tr>
                    <td><?= $estoque['numero_contrato'] ?> - <?= $estoque['usina'] ?></td>
                    <td class="money"><?= $valor_de > 0 ? $this->Number->currency($valor_de) : '---' ?></td>
                    <td class="money"><?= $valor_ate > 0 ? $this->Number->currency($valor_ate) : '---' ?></td>
                </tr>
                <?php
            }
        }
    }
    ?>
    <?php
    $saldo_passivo_de = 0;
    $saldo_passivo_ate = 0;
    foreach ($saldos as $saldo) {
        if ($saldo['saldo_de'] < 0) {
            $saldo_passivo_de += abs($saldo['saldo_de']);
        }
        if ($saldo['saldo_ate'] < 0) {
            $saldo_passivo_ate += abs($saldo['saldo_ate']);
        }
    }
    foreach ($passivo_estoques as $inventory) {
        $saldo_passivo_de += $inventory['valor_de'];
        $saldo_passivo_ate += $inventory['valor_ate'];
    }
    foreach ($comissoes_corretoras as $comissao) {
        $saldo_passivo_de += $comissao['valor_de'];
        $saldo_passivo_ate += $comissao['valor_ate'];
    }
    foreach ($comissoes_vendedores as $comissao) {
        $saldo_passivo_de += $comissao['valor_de'];
        $saldo_passivo_ate += $comissao['valor_ate'];
    }
    foreach ($spreads as $spread) {
        $saldo_passivo_de += $spread['valor_de'];
        $saldo_passivo_ate += $spread['valor_ate'];
    }
    foreach ($fretes as $frete) {
        $saldo_passivo_de += $frete['valor_de'];
        $saldo_passivo_ate += $frete['valor_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: blue; color: white">
        <td>Passivo</td>
        <td class="money"><?= $this->Number->currency($saldo_passivo_de) ?></td>
        <td class="money"><?= $this->Number->currency($saldo_passivo_ate) ?></td>
    </tr>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($saldos as $entry) {
        $subtotal_de += $entry['saldo_de'] < 0 ? $entry['saldo_de'] : 0;
        $subtotal_ate += $entry['saldo_de'] < 0 ? $entry['saldo_de'] : 0;
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Contas</td>
        <td class="money"><?= $this->Number->currency(abs($subtotal_de)) ?></td>
        <td class="money"><?= $this->Number->currency(abs($subtotal_ate)) ?></td>
    </tr>
    <?php
    foreach ($saldos as $saldo) {
        if (($saldo['saldo_de'] < 0) or ( $saldo['saldo_ate'] < 0)) {
            ?>
            <tr>
                <td><?= $saldo['conta'] ?></td>
                <td class="money"><?= $saldo['saldo_de'] < 0 ? $this->Number->currency(abs($saldo['saldo_de'])) : '---' ?></td>
                <td class="money"><?= $saldo['saldo_ate'] < 0 ? $this->Number->currency(abs($saldo['saldo_ate'])) : '---' ?></td>
            </tr>
            <?php
        }
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($passivo_estoques as $entry) {
        $subtotal_de += $entry['valor_de'];
        $subtotal_ate += $entry['valor_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Estoques</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    $distributors = [];
    foreach ($passivo_estoques as $inventory) {
        if (!array_key_exists($inventory['distributor_id'], $distributors)) {
            $distributors[$inventory['distributor_id']] = $inventory['distributor'];
        }
    }
    foreach ($distributors as $distributor_id => $distributor) {
        ?>
        <tr><td colspan="3">Distribuidora: <?= $distributor ?></td></tr>
        <?php
        foreach ($passivo_estoques as $inventory) {
            if ($inventory['distributor_id'] == $distributor_id) {
                ?>
                <tr>
                    <td><?= $inventory['numero_contrato'] ?> - <?= $inventory['usina'] ?></td>
                    <td class="money"><?= $inventory['valor_de'] > 0 ? $this->Number->currency($inventory['valor_de']) : '-----' ?></td>
                    <td class="money"><?= $inventory['valor_ate'] > 0 ? $this->Number->currency($inventory['valor_ate']) : '-----' ?></td>
                </tr>
                <?php
            }
        }
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($comissoes_corretoras as $entry) {
        $subtotal_de += $entry['valor_de'];
        $subtotal_ate += $entry['valor_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Comissões Corretoras</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    foreach ($comissoes_corretoras as $comissao) {
        ?>
        <tr>
            <td><?= $comissao['corretora'] ?></td>
            <td class="money"><?= $comissao['valor_de'] > 0 ? $this->Number->currency($comissao['valor_de']) : '-----' ?></td>
            <td class="money"><?= $comissao['valor_ate'] > 0 ? $this->Number->currency($comissao['valor_ate']) : '-----' ?></td>
        </tr>
        <?php
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($comissoes_vendedores as $entry) {
        $subtotal_de += $entry['valor_de'];
        $subtotal_ate += $entry['valor_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Comissões Vendedores</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    foreach ($comissoes_vendedores as $comissao) {
        ?>
        <tr>
            <td><?= $comissao['vendedor'] ?></td>
            <td class="money"><?= $comissao['valor_de'] > 0 ? $this->Number->currency($comissao['valor_de']) : '-----' ?></td>
            <td class="money"><?= $comissao['valor_ate'] > 0 ? $this->Number->currency($comissao['valor_ate']) : '-----' ?></td>
        </tr>
        <?php
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($spreads as $entry) {
        $subtotal_de += $entry['valor_de'];
        $subtotal_ate += $entry['valor_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Spreads</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    foreach ($spreads as $spread) {
        if (($spread['valor_de'] != 0) or ( $spread['valor_ate'] != 0)) {
            ?>
            <tr>
                <td><?= $spread['distribuidora'] ?></td>
                <td class="money"><?= $spread['valor_de'] > 0 ? $this->Number->currency($spread['valor_de']) : '-----' ?></td>
                <td class="money"><?= $spread['valor_ate'] > 0 ? $this->Number->currency($spread['valor_ate']) : '-----' ?></td>
            </tr>
            <?php
        }
    }
    ?>
    <?php
    $subtotal_de = 0;
    $subtotal_ate = 0;
    foreach ($fretes as $entry) {
        $subtotal_de += $entry['valor_de'];
        $subtotal_ate += $entry['valor_ate'];
    }
    ?>
    <tr style="font-weight: bold; background-color: lightsteelblue">
        <td>Fretes</td>
        <td class="money"><?= $this->Number->currency($subtotal_de) ?></td>
        <td class="money"><?= $this->Number->currency($subtotal_ate) ?></td>
    </tr>
    <?php
    foreach ($fretes as $frete) {
        ?>
        <tr>
            <td><?= $frete['transportadora'] ?></td>
            <td class="money"><?= $frete['valor_de'] > 0 ? $this->Number->currency($frete['valor_de']) : '-----' ?></td>
            <td class="money"><?= $frete['valor_ate'] > 0 ? $this->Number->currency($frete['valor_ate']) : '-----' ?></td>
        </tr>
        <?php
    }
    ?>
    <?php
    $patrimonio_liquido_de = $saldo_ativo_de - $saldo_passivo_de;
    $patrimonio_liquido_ate = $saldo_ativo_ate - $saldo_passivo_ate;
    ?>
    <tr style="font-weight: bold; background-color: blue; color: white">
        <td>Patrimônio Líquido</td>
        <td class="money"><?= $this->Number->currency($patrimonio_liquido_de) ?></td>
        <td class="money"><?= $this->Number->currency($patrimonio_liquido_ate) ?></td>
    </tr>
    <tr style="font-weight: bold; background-color: blue; color: white">
        <td>Lucro/Prejuízo Acumulado</td>
        <td class="money"></td>
        <td class="money"><?= $this->Number->currency($patrimonio_liquido_ate - $patrimonio_liquido_de) ?></td>
    </tr>
</table>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>

<?php
//dump($passivo_estoques)
?>