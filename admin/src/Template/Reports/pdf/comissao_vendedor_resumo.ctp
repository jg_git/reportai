<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Comissões de Vendedores</h4>
<h5 class="mt-3 mb-3">Período: <?= $data['data_referencia_range'] ?></h5>
<h5 class="mt-3 mb-3">Comissões Vendedores (Resumido)</h5>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th width="50%">Vendedor</th>
        <th class="money" width="25%">Volume</th>
        <th class="money" width="25%">Valor</th>
    </tr>
    <?php
    if (isset($rows) and sizeof($rows) > 0) {
        foreach ($rows as $row) {
            ?>
            <tr>
                <td><?= $row['vendedor'] ?></td>
                <td class="money"><?= number_format($row['volume'], 0, ',', '.') ?></td>
                <td class="money"><?= $this->Number->currency($row['comissao']) ?></td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>
<hr>
<h5 class="mt-3 mb-3">Comissões Vendedores (Detalhado)</h5>
<?php
if (isset($rows) and sizeof($rows) > 0) {
    foreach ($rows as $row) {
        ?>
        <h5 class="mt-3 mb-3">Vendedor: <?= $row['vendedor'] ?></h5>
        <table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
            <tr>
                <th width="10%" style="text-align: center">Data Venda</th>
                <th width="50%">Cliente</th>
                <th width="10%" class="money">Volume</th>
                <th width="10%" class="money">Preço Mercado</th>
                <th width="10%" class="money">Preço Venda</th>
                <th width="10%" class="money">Comissao</th>
            </tr>
            <?php
            foreach ($payables as $payable) {
                if ($payable->sale_detail->presale->client->seller_id == $row['seller_id']) {
                    ?>
                    <tr>
                        <td style="text-align: center">
                            <?= date('d/m/Y', strtotime(str_replace('/', '-', $payable->sale_detail->sale->data_venda))) ?>
                        </td>
                        <td><?= $payable->sale_detail->presale->client->razao_social ?></td>
                        <td class="money"><?= number_format($payable->sale_detail->volume, 0, ',', '.') ?></td>
                        <td class="money"><?= $this->Number->currency($payable->sale_detail->presale->preco_mercado, null, ['places' => 4]) ?></td>
                        <td class="money"><?= $this->Number->currency($payable->sale_detail->presale->preco_venda, null, ['places' => 4]) ?></td>
                        <td class="money"><?= $this->Number->currency($payable->valor) ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
        <?php
    }
} else {
    ?>
    Nenhum registro encontrado
<?php } ?>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>