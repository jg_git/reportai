<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<div class="row justify-content-center">
    <div class="col-md-8 col-sm-12">
        <h4 class="mt-3 mb-3">Demonstrativo de Resultado Exercício</h4>
        <h5 class="mt-3 mb-3">Posição: <?= $data['data_referencia_range'] ?></h5>
        <?php
        $subtotal_cmv = $estoque_inicial + $compras_total - $estoque_final - $devolucoes;

        $faturamento_bruto = 0;
        foreach ($sales as $sale) {
            foreach ($sale->sale_details as $detail) {
                $faturamento_bruto += $detail->volume * $detail->presale->preco_venda;
            }
        }

        $totals = [];
        for ($i = 3; $i <= 25; $i++) {
            $totals[$i] = 0;
        }
        foreach ($ledger_entries as $entry) {
            $totals[$entry->ledger_entry_type_id] += $entry->valor;
        }
        ?>
        <table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
            <tr style="font-weight: bold">
                <td>Estoque Inicial</td>
                <td class="money"><?= $this->Number->currency($estoque_inicial) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Compras do Período</td>
                <td class="money"><?= $this->Number->currency($compras_total) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Estoque Final</td>
                <td class="money"><?= $this->Number->currency($estoque_final) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Devoluções de Estoque</td>
                <td class="money"><?= $this->Number->currency($devolucoes) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Subtotal (C.M.V.)</td>
                <td class="money"><?= $this->Number->currency($subtotal_cmv) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Faturamento Bruto</td>
                <td class="money"><?= $this->Number->currency($faturamento_bruto) ?></td>
            </tr>
            <?php
            $receita_bruta = $faturamento_bruto - $subtotal_cmv;
            ?>
            <tr style="background-color: lightblue; font-weight: bold">
                <td>Receita Bruta</td>
                <td class="money"><?= $this->Number->currency($receita_bruta) ?></td>
            </tr>
            <!-- ======= Outras Entradas ======= -->
            <tr style="font-weight: bold">
                <td colspan="2"><div style="font-weight: bold">Outras Entradas</div></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Juros de Boletos/Clientes</td>
                <td class="money"><?= $this->Number->currency($totals[3]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Rendimentos Financeiros</td>
                <td class="money"><?= $this->Number->currency($totals[4]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Resíduos</td>
                <td class="money"><?= $this->Number->currency($totals[5]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Outras Receitas</td>
                <td class="money"><?= $this->Number->currency($totals[6]) ?></td>
            </tr>
            <?php
            $total_outras_entradas = 0;
            for ($i = 3; $i <= 6; $i++) {
                $total_outras_entradas += $totals[$i];
            }
            ?>
            <tr style="background-color: lightblue; font-weight: bold">
                <td>Total Outras Entradas</td>
                <td class="money"><?= $this->Number->currency($total_outras_entradas) ?></td>
            </tr>
            <!-- ======= Saidas ======= -->
            <tr>
                <td colspan="2"><div style="font-weight: bold">Saídas</div></td>
            </tr>
            <?php
            $total_comissoes_vendedores = 0;
            foreach ($comissoes_vendedores as $cv) {
                $total_comissoes_vendedores += $cv['total'];
            }
            $total_saidas = $total_comissoes_vendedores;
            ?>
            <tr style="font-weight: bold">
                <td>Comissões - Vendedor</td>
                <td class="money"><?= $this->Number->currency($total_comissoes_vendedores) ?></td>
            </tr>
            <?php
            foreach ($comissoes_vendedores as $cv) {
                ?>
                <tr>
                    <td class="pl-5"><?= $cv['razao_social'] ?></td>
                    <td class="money"><?= $this->Number->currency($cv['total']) ?></td>
                </tr>
                <?php
            }
            ?>
            <?php
            $total_comissoes_corretoras = 0;
            foreach ($comissoes_corretoras as $cc) {
                $total_comissoes_corretoras += $cc['total'];
            }
            $total_saidas += $total_comissoes_corretoras;
            ?>
            <tr style="font-weight: bold">
                <td>Comissões - Corretora</td>
                <td class="money"><?= $this->Number->currency($total_comissoes_corretoras) ?></td>
            </tr>
            <?php
            foreach ($comissoes_corretoras as $cc) {
                ?>
                <tr>
                    <td class="pl-5"><?= $cc['razao_social'] ?></td>
                    <td class="money"><?= $this->Number->currency($cc['total']) ?></td>
                </tr>
                <?php
            }
            ?>
            <?php
            $total_fretes = 0;
            foreach ($fretes as $f) {
                $total_fretes += $f['total'];
            }
            $total_saidas += $total_fretes;
            ?>
            <tr style="font-weight: bold">
                <td>Fretes</td>
                <td class="money"><?= $this->Number->currency($total_fretes) ?></td>
            </tr>
            <?php
            foreach ($fretes as $f) {
                if ($f['total'] > 0) {
                    ?>
                    <tr>
                        <td class="pl-5"><?= $f['razao_social'] ?></td>
                        <td class="money"><?= $this->Number->currency($f['total']) ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            <?php
            $total_spread = 0;
            foreach ($spreads as $s) {
                $total_spread += $s['total'];
            }
            $total_saidas += $total_spread;
            ?>
            <tr style="font-weight: bold">
                <td>Spread</td>
                <td class="money"><?= $this->Number->currency($total_spread) ?></td>
            </tr>
            <?php
            foreach ($spreads as $s) {
                if ($s['total'] > 0) {
                    ?>
                    <tr>
                        <td class="pl-5"><?= $s['razao_social'] ?></td>
                        <td class="money"><?= $this->Number->currency($s['total']) ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr style="font-weight: bold">
                <td>Despesas Bancárias</td>
                <td class="money"><?= $this->Number->currency($totals[10]) ?></td>
            </tr>
            <?php
            $accounts = [];
            foreach ($ledger_entries as $entry) {
                if ($entry->ledger_entry_type_id == 10) {
                    $account_id = $entry->account_id;
                    if (array_key_exists($account_id, $accounts)) {
                        $account = $accounts[$account_id];
                        $account['saldo'] += $entry->valor;
                        $accounts[$account_id] = $account;
                    } else {
                        $ac = $entry->account;
                        switch ($ac->tipo) {
                            case 'I' :
                                $msg = $ac->distributor->razao_social;
                                break;
                            case 'F':
                                $msg = $ac->bank->nome . ' - ' . $ac->titular . ' - ' . $ac->numero_agencia . ' - ' . $ac->numero_conta;
                                break;
                            case 'C':
                                $msg = $ac->titular;
                                break;
                        }
                        $account['nome'] = $msg;
                        $account['saldo'] = $entry->valor;
                        $accounts[$account_id] = $account;
                    }
                }
            }
            foreach ($accounts as $account) {
                ?>
                <tr>
                    <td class="pl-5"><?= $account['nome'] ?></td>
                    <td class="money"><?= $this->Number->currency($account['saldo']) ?></td>
                </tr>
                <?php
            }
            ?>
            <tr style="font-weight: bold">
                <td>Juros</td>
                <td class="money"><?= $this->Number->currency($totals[11]) ?></td>
            </tr>
            <?php
            $accounts = [];
            foreach ($ledger_entries as $entry) {
                if ($entry->ledger_entry_type_id == 11) {
                    $account_id = $entry->account_id;
                    if (array_key_exists($account_id, $accounts)) {
                        $account = $accounts[$account_id];
                        $account['saldo'] += $entry->valor;
                        $accounts[$account_id] = $account;
                    } else {
                        $ac = $entry->account;
                        switch ($ac->tipo) {
                            case 'I' :
                                $msg = $ac->distributor->razao_social;
                                break;
                            case 'F':
                                $msg = $ac->bank->nome . ' - ' . $ac->titular . ' - ' . $ac->numero_agencia . ' - ' . $ac->numero_conta;
                                break;
                            case 'C':
                                $msg = $ac->titular;
                                break;
                        }
                        $account['nome'] = $msg;
                        $account['saldo'] = $entry->valor;
                        $accounts[$account_id] = $account;
                    }
                }
            }
            foreach ($accounts as $account) {
                ?>
                <tr>
                    <td class="pl-5"><?= $account['nome'] ?></td>
                    <td class="money"><?= $this->Number->currency($account['saldo']) ?></td>
                </tr>
                <?php
            }
            ?>
            <tr style="font-weight: bold">
                <td>Serviços de Terceiros - Contador</td>
                <td class="money"><?= $this->Number->currency($totals[12]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Serviços de Terceiros - Consultoria</td>
                <td class="money"><?= $this->Number->currency($totals[13]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Impostos - PIS</td>
                <td class="money"><?= $this->Number->currency($totals[14]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Impostos - COFINS</td>
                <td class="money"><?= $this->Number->currency($totals[15]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Impostos - IRPJ</td>
                <td class="money"><?= $this->Number->currency($totals[16]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Impostos - CSLL</td>
                <td class="money"><?= $this->Number->currency($totals[17]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Impostos - ISS</td>
                <td class="money"><?= $this->Number->currency($totals[18]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Custo de Armazenagem</td>
                <td class="money"><?= $this->Number->currency($totals[24]) ?></td>
            </tr>            
            <tr style="font-weight: bold">
                <td>Descontos Clientes</td>
                <td class="money"><?= $this->Number->currency($totals[25]) ?></td>
            </tr>
            <tr style="font-weight: bold">
                <td>Outras Despesas</td>
                <td class="money"><?= $this->Number->currency($totals[19]) ?></td>
            </tr>
            <?php
            for ($i = 10; $i <= 19; $i++) {
                $total_saidas += $totals[$i];
            }
            $total_saidas += $totals[24];
            $total_saidas += $totals[25];
            ?>
            <tr style="background-color: lightblue; font-weight: bold">
                <td>Total Saídas</td>
                <td class="money"><?= $this->Number->currency($total_saidas) ?></td>
            </tr>
            <tr style="background-color: lightblue; font-weight: bold">
                <td>Lucro Líquido</td>
                <td class="money"><?= $this->Number->currency($receita_bruta + $total_outras_entradas - $total_saidas) ?></td>
            </tr>
        </table>
    </div>
</div>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>

<?php
//dump($comissoes_vendedores);
?>