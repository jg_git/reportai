<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Resumo Diário</h4>
<h5 class="mt-3 mb-3">Posição: <?= $data['data_referencia_range'] ?></h5>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th style="text-align: center">Data</th>
        <th class="money">Volume</th>
        <th class="money">Faturamento Bruto</th>
        <th class="money">Custo Produto</th>
        <th class="money">Spread</th>
        <th class="money">Frete</th>
        <th class="money">Com. Corretora</th>
        <th class="money">Com. Vendedor</th>
        <th class="money">Lucro Diário</th>
    </tr>
    <?php
    $volume_total = 0;
    $faturamento_bruto_total = 0;
    $custo_produto_total = 0;
    $spread_total = 0;
    $frete_total = 0;
    $comissao_corretora_total = 0;
    $comissao_vendedor_total = 0;
    $lucro_diario_total = 0;
    if (isset($resumos) and sizeof($resumos) > 0) {
        foreach ($resumos as $resumo) {
            $volume_total += $resumo['volume'];
            $faturamento_bruto_total += $resumo['faturamento_bruto'];
            $custo_produto_total += $resumo['custo_produto'];
            $spread_total += $resumo['spread'];
            $frete_total += $resumo['frete'];
            $comissao_corretora_total += $resumo['comissao_corretora'];
            $comissao_vendedor_total += $resumo['comissao_vendedor'];
            $lucro_diario = $resumo['faturamento_bruto'] - $resumo['custo_produto'] - $resumo['spread'] - $resumo['frete'] - $resumo['comissao_corretora'] - $resumo['comissao_vendedor'];
            $lucro_diario_total += $lucro_diario;
            ?>
            <tr>
                <td style="text-align: center"><?= date('d/m/Y', strtotime($resumo['data'])) ?></td>
                <td class="money"><?= number_format($resumo['volume'], 0, ',', '.') ?></td>
                <td class="money"><?= $this->Number->currency($resumo['faturamento_bruto']) ?></td>
                <td class="money"><?= $this->Number->currency($resumo['custo_produto']) ?></td>
                <td class="money"><?= $this->Number->currency($resumo['spread']) ?></td>
                <td class="money"><?= $this->Number->currency($resumo['frete']) ?></td>
                <td class="money"><?= $this->Number->currency($resumo['comissao_corretora']) ?></td>
                <td class="money"><?= $this->Number->currency($resumo['comissao_vendedor']) ?></td>
                <td class="money"><?= $this->Number->currency($lucro_diario) ?></td>
            </tr>
            <?php
        }
    }
    ?>
    <tr style="background-color: lightblue; font-weight: bold">
        <td>Total</td>
        <td class="money"><?= number_format($volume_total, 0, ',', '.') ?></td>
        <td class="money"><?= $this->Number->currency($faturamento_bruto_total) ?></td>
        <td class="money"><?= $this->Number->currency($custo_produto_total) ?></td>
        <td class="money"><?= $this->Number->currency($spread_total) ?></td>
        <td class="money"><?= $this->Number->currency($frete_total) ?></td>
        <td class="money"><?= $this->Number->currency($comissao_corretora_total) ?></td>
        <td class="money"><?= $this->Number->currency($comissao_vendedor_total) ?></td>
        <td class="money"><?= $this->Number->currency($lucro_diario_total) ?></td>
    </tr>
</table>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>
