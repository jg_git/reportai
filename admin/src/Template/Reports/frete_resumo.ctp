<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Frete</h4>
<h5 class="mt-3 mb-3">Período: <?= $data['data_referencia_range'] ?></h5>
<h5 class="mt-3 mb-3">Fretes (Resumido)</h5>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th>Transportadora</th>
        <th class="money">Frete</th>
    </tr>
    <?php
    if (isset($rows) and sizeof($rows) > 0) {
        foreach ($rows as $row) {
            if ($row['frete'] > 0) {
                ?>
                <tr>
                    <td><?= $row['transportadora'] ?></td>
                    <td class="money"><?= $this->Number->currency($row['frete']) ?></td>
                </tr>
                <?php
            }
        }
    } else {
        ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>
<hr>
<h5 class="mt-3 mb-3">Fretes (Detalhado)</h5>

<?php
if (isset($rows) and sizeof($rows) > 0) {
    foreach ($rows as $row) {
        ?>
        <h5 class="mt-3 mb-3">Transportadora: <?= $row['transportadora'] ?></h5>
        <table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
            <?php if ($row['tipo'] == 'RG') { ?>
                <tr>
                    <th style="text-align: center" width="10%">Data Venda</th>
                    <th width="20%">Usina</th>
                    <th width="10%">Veículo</th>
                    <th width="15%">Motorista</th>
                    <th width="25%">Destino</th>
                    <th width="10%" class="money">Volume</th>
                    <th width="10%" class="money">Valor</th>
                </tr>
            <?php } else { ?>
                <tr>
                    <th style="text-align: center" width="10%">Data Venda</th>
                    <th width="50%">Usina</th>
                    <th width="20%" class="money">Volume</th>
                    <th width="20%" class="money">Valor</th>
                </tr>
            <?php } ?>
            <?php
            foreach ($fretes as $frete) {
                if ($frete->sale->carrier_id == $row['carrier_id']) {
                    ?>
                    <tr>
                        <td style="text-align: center">
                            <?= !is_null($frete->sale) ? date('d/m/Y', strtotime(str_replace('/', '-', $frete->sale->data_venda))) : '' ?>
                        </td>
                        <td><?= $frete->sale->purchases[0]->plant->razao_social ?></td>
                        <td><?= $frete->sale->vehicle->placa ?></td>
                        <td><?= $frete->sale->driver->nome ?></td>
                        <td>
                            <?php
                            $msg = '';
                            $city = [];
                            foreach ($frete->sale->sale_details as $detail) {
                                $cidade = $detail->presale->client->cidade;
                                if (!in_array($cidade, $city)) {
                                    $msg .= $cidade . ', ';
                                    $city[] = $cidade;
                                }
                            }
                            echo rtrim($msg, ', ');
                            ?>
                        </td>
                        <td class="money"><?= number_format($frete->sale->volume_total, 0, ',', '.') ?></td>
                        <td class="money"><?= $this->Number->currency($frete->valor) ?></td>
                    </tr>
                    <?php
                }
            }
            foreach ($frete_transferencias as $frete) {
                if ($frete->purchase_transfer->carrier_id == $row['carrier_id']) {
                    ?>
                    <tr>
                        <td style="text-align: center">
                            <?= date('d/m/Y', strtotime(str_replace('/', '-', $frete->purchase_transfer->created))) ?>
                        </td>
                        <td><?= $frete->purchase_transfer->plant->razao_social ?></td>
                        <td class="money"><?= number_format(abs($frete->purchase_transfer->volume), 0, ',', '.') ?></td>
                        <td class="money"><?= $this->Number->currency($frete->valor) ?></td>
                    </tr>
                    <?php
                }
            }
            ?>
        </table>
        <?php
    }
} else {
    ?>
    Nenhum registro encontrado
<?php } ?>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>
