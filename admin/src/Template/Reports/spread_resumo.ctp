<?php if ($this->request->is('get')) { ?>
    <style>
        @font-face {
            font-family: Verdana;
            font-style: normal;
            font-weight: normal;
            src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
        }

        body {
            font-family: Verdana;
            font-size: 10px;
            margin-left: 0.0cm;
            margin-right: 0.0cm;
        }
    </style>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
<?php } ?>

<h4 class="mt-3 mb-3">Relatório de Spread</h4>
<h5 class="mt-3 mb-3">Período: <?= $data['data_referencia_range'] ?></h5>
<table class="table table-hover <?= $this->request->is('post') ? 'extra-slim-row' : 'report-row' ?>">
    <tr>
        <th>Distribuidora</th>
        <th class="money">Volume</th>
        <th class="money">Spread</th>
    </tr>
    <?php
    if (isset($rows) and sizeof($rows) > 0) {
        foreach ($rows as $row) {
            if ($row['spread'] > 0) {
                ?>
                <tr>
                    <td><?= $row['distribuidora'] ?></td>
                    <td class="money"><?= number_format($row['volume'], 0, ',', '.') ?></td>
                    <td class="money"><?= $this->Number->currency($row['spread']) ?></td>
                </tr>
                <?php
            }
        }
    } else {
        ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>

<?php if ($this->request->is('get')) { ?>
    Impresso por <?= $this->request->getSession()->read('Auth.User')['nome'] ?> em <?= date('d/m/Y H:i:s') ?>
<?php } ?>
