<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {

        $("#basic").load("<?= $this->Url->build(['action' => 'basic', $cart->isNew() ? 'new' : $cart->id]) ?>");

        $("#orifice").on("click", ".fa-edit", function() {
            anchor = $(this).data("anchor");
            if ($("#edit-orifice-area-" + anchor + " form[id|=form-orifice]").length === 0) {
                $("#edit-orifice-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Carts', 'action' => 'orifice']) ?>/<?= $cart->isNew() ? 'new' : $cart->id ?>/" +
                    $(this).data("id") + "/" +
                    anchor,
                    function() {
                        $("#form-orifice-" + anchor + " input[name=quantidade_litros]").maskMoney({
                            thousands: '.',
                            decimal: ',',
                            precision: 0
                        });
                    });
            }
            $("#edit-orifice-" + anchor).show();
            $("#view-orifice-" + anchor).hide();
        });

        $("#orifice").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-orifice-area-" + anchor + " span[id=numero]").html($("#form-orifice-" + anchor + " input[id=numero]").val());
            $("#view-orifice-area-" + anchor + " span[id=quantidade-litros]").html(formatMoney($("#form-orifice-" + anchor + " input[id=quantidade-litros]").val(), 0));
            $("#edit-orifice-" + anchor).hide();
            $("#view-orifice-" + anchor).show();
        });

        $("body").on("change", "select[name=quantidade_bocas]", function() {
            quantidade_bocas = parseInt($(this).val());
            bocas = $("tr[id|=view-orifice]").length;
            if (quantidade_bocas > bocas) {
                for (i = 1; i <= quantidade_bocas - bocas; i++) {
                    anchor = anchorize();
                    $("#no-orifice").remove();
                    $("#orifice").append(
                        '<tr id="edit-orifice-' + anchor + '">' +
                        '<td>' +
                        '<div id="edit-orifice-area-' + anchor + '"></div>' +
                        '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" data-orifice-id="new" onclick="remove(\'orifice\', \'' + anchor + '\', \'new\')"></i>' +
                        '<i class="fa fa-check-square pull-right mr-2" data-type="orifice" data-anchor="' + anchor + '"></i>' +
                        '</td>' +
                        '</tr>' +
                        '<tr id="view-orifice-' + anchor + '" style="display:none" data-anchor="' + anchor + '">' +
                        '<td>' +
                        '<span id="view-orifice-area-' + anchor + '" style="font-size: 14px">' +
                        'Número da Boca: <span class="mr-2" id="numero"></span>' +
                        'Quantidade de Litros: <span class="mr-2" id="quantidade-litros"></span>' +
                        '</span>' +
                        '<i class="fa fa-edit pull-right" data-type="orifice" data-anchor="' + anchor + '" data-id="new"></i>' +
                        '</td>' +
                        '</tr>'
                    );
                    $.get("<?= $this->Url->build(['controller' => 'Carts', 'action' => 'orifice']) ?>/" +
                        "<?= $cart->isNew() ? 'new' : $cart->id ?>" + "/" + "new" + "/" + anchor,
                        function(data, code) {
                            anc = $("<div>" + data + "</div>").find("form[id|=form-orifice]").data("anchor");
                            $("#edit-orifice-area-" + anc).html(data);
                            $("#form-orifice-" + anc + " input[name=quantidade_litros]").maskMoney({
                                thousands: '.',
                                decimal: ',',
                                precision: 0
                            });
                        });
                }
            } else if (quantidade_bocas < bocas) {
                i = 1;
                $("tr[id|=view-orifice]").each(function() {
                    if (i++ > quantidade_bocas) {
                        anchor = $(this).data("anchor");
                        console.log(bocas + " " + quantidade_bocas + " " + anchor);
                        id = $("#trash-" + anchor).data("orifice-id");
                        if (id !== "new") {
                            $("#delete-orifice-list").append("<option value='" + id + "' selected></option>");
                        }
                        $("tr[id=edit-orifice-" + anchor + "]").remove();
                        $("tr[id=view-orifice-" + anchor + "]").remove();
                    }
                });
            }
        });

        //        $(".fa-plus-square").click(function () {
        //            anchor = anchorize();
        //            switch ($(this).data("type")) {
        //                case "orifice":
        //                    $("#no-orifice").remove();
        //                    $("#orifice-add").before(
        //                            '<tr id="edit-orifice-' + anchor + '">' +
        //                            '<td>' +
        //                            '<div id="edit-orifice-area-' + anchor + '"></div>' +
        //                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" data-orifice-id="new" onclick="remove(\'orifice\', \'' + anchor + '\', \'new\')"></i>' +
        //                            '<i class="fa fa-check-square pull-right mr-2" data-type="orifice" data-anchor="' + anchor + '"></i>' +
        //                            '</td>' +
        //                            '</tr>' +
        //                            '<tr id="view-orifice-' + anchor + '" style="display:none" data-anchor="' + anchor + '">' +
        //                            '<td>' +
        //                            '<span id="view-orifice-area-' + anchor + '" style="font-size: 14px">' +
        //                            'Número da Boca: <span class="mr-2" id="numero"></span>' +
        //                            'Quantidade de Litros: <span class="mr-2" id="quantidade-litros"></span>' +
        //                            '</span>' +
        //                            '<i class="fa fa-edit pull-right" data-type="orifice" data-anchor="' + anchor + '" data-id="new"></i>' +
        //                            '</td>' +
        //                            '</tr>'
        //                            );
        //                    $("#edit-orifice-area-" + anchor).load(
        //                            "<?= $this->Url->build(['controller' => 'Carts', 'action' => 'orifice']) ?>/<?= $cart->isNew() ? 'new' : $cart->id ?>/" +
        //                            "new" + "/" +
        //                            anchor);
        //                    break;
        //            }
        //        });

        $("#save").click(function() {
            HoldOn.open({
                theme: "sk-cube-grid"
            });
            var basic = $.Deferred();
            var orifice = $.Deferred();
            cart_id = $("#form-basic").data("id");
            $.post("<?= $this->Url->build(['action' => 'basic']) ?>/" + cart_id,
                $("#form-basic").serialize(),
                function(data) {
                    $("#basic").html(data);
                    if ($("<div>" + data + "</div>").find("#form-basic").data("error") === "N") {
                        basic.resolve("OK");
                        cart_id = $("<div>" + data + "</div>").find("#form-basic").data("id");
                        //======== Salvar bocas ======
                        var req_orifice = [];
                        $("form[id|='form-orifice']").each(function() {
                            anchor = $(this).data("anchor");
                            req_orifice.push($.post("<?= $this->Url->build(['controller' => 'Carts', 'action' => 'orifice']) ?>/" + cart_id + "/" + $(this).data("id") + "/" + anchor,
                                $("#form-orifice-" + anchor).serialize()));
                        });
                        if (req_orifice.length > 0) {
                            $.when.apply(undefined, req_orifice).then(function() {
                                var objects = arguments;
                                var saved = true;
                                var data = [];
                                if (Array.isArray(objects[0])) {
                                    for (i = 0; i < objects.length; i++) {
                                        data.push(objects[i][0]);
                                    }
                                } else {
                                    data.push(objects[0]);
                                }
                                for (i = 0; i < data.length; i++) {
                                    anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-orifice']").data("anchor");
                                    id = $("<div>" + data[i] + "</div>").find("form[id|='form-orifice']").data("id");
                                    $("#edit-orifice-area-" + anchor).html(data[i]);
                                    $("#trash-" + anchor).attr("onclick", "remove('orifice', '" + anchor + "', " + id + ")");
                                    if ($("<div>" + data[i] + "</div>").find("#form-orifice-" + anchor).data("error") === "N") {} else {
                                        saved = false;
                                    }
                                }
                                orifice.resolve(saved ? "OK" : "NG");
                            });
                        } else {
                            orifice.resolve("OK");
                        }
                        if ($("#form-delete-orifice option").length > 0) {
                            $.post("<?= $this->Url->build(['controller' => 'Carts', 'action' => 'orifice-delete']) ?>",
                                $("#form-delete-orifice").serialize());
                        }
                    } else {
                        basic.resolve("NG");
                        orifice.resolve("NG");
                    }
                });
            $.when(basic, orifice).done(function(v1, v2) {
                $("#save").attr("disabled", false);
                console.log("basic: " + v1 + " orifice: " + v2);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK") {
                    toast("Cadastro salvo com sucesso");
                }
            });

        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Essa operação não poderá ser revertida',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                switch (type) {
                    case "orifice":
                        $("#edit-orifice-" + anchor).remove();
                        $("#view-orifice-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-orifice-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                }
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Carretas', 'url' => ['action' => 'index']],
    ['title' => 'Carreta']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div id="basic"></div>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <h5 class="mt-3">Bocas</h5>
                <table id="orifice" class="table table-hover">
                    <?php
                    if (isset($cart->cart_orifices) and sizeof($cart->cart_orifices) > 0) {
                        foreach ($cart->cart_orifices as $orifice) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-orifice-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-orifice-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" data-orifice-id="<?= $orifice->id ?>" onclick="remove('orifice', '<?= $anchor ?>', <?= $orifice->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="orifice" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-orifice-<?= $anchor ?>" data-anchor="<?= $anchor ?>">
                                <td>
                                    <span id="view-orifice-area-<?= $anchor ?>" style="font-size: 14px">
                                        Número da Boca: <span class="mr-2" id="numero"><?= $orifice->numero ?></span>
                                        Quantidade de Litros: <span class="mr-2" id="quantidade-litros"><?= number_format($orifice->quantidade_litros, 0, ',', '.') ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="orifice" data-anchor="<?= $anchor ?>" data-id="<?= $orifice->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr id="no-orifice">
                            <td class="no-data-found">Nenhuma Boca Cadastrada</td>
                        </tr>
                    <?php
                    }
                    ?>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-orifice']) ?>
                <?= $this->Form->control('delete-orifice-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->element('button_delete', ['id' => $cart->id]) ?>
                <?= $this->element('button_return') ?>
                <button class="btn btn-lg btn-primary nohover pull-left" onClick="redirectReport()">GERAR PDF</button>
                <script>
                    function redirectReport() {
                        //     window.open("/" + $(this).data("client-id") + "/report.pdf")
                        window.print();
                    }
                    //window.open(routing.find(x => x.key === route).route + "/report.pdf"); 
                </script>
            </div>
        </div>
    </div>
</div>