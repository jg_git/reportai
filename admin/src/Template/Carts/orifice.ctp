<?= $this->Flash->render() ?>
<?=
$this->Form->create($orifice, [
    'id' => 'form-orifice-' . $anchor,
    'data-id' => $orifice->isNew() ? 'new' : $orifice->id,
    'data-error' => sizeof($orifice->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <?= $this->Form->control('numero', ['type' => 'text', 'label' => 'Número da Boca']) ?>
    </div>
    <div class="col-md-6 col-sm-12">
        <?= $this->Form->control('quantidade_litros', ['type' => 'text', 'label' => 'Quantidade de Litros']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
