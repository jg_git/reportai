<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Bancos', 'url' => ['action' => 'index']],
    ['title' => 'Banco']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <?=
                $this->Form->create($bank, [
                    'id' => 'form-bank',
                    'data-id' => $bank->isNew() ? 'new' : $bank->id,
                    'data-error' => sizeof($bank->getErrors()) > 0 ? 'Y' : 'N'])
                ?>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <?= $this->Form->control('codigo', ['type' => 'text', 'label' => 'Número']) ?>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <?= $this->Form->control('nome', ['type' => 'text', 'label' => 'Nome']) ?>
                    </div>
                </div>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_delete', ['id' => $bank->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>
    </div>
</div>





