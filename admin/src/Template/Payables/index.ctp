<?= $this->Html->css('/node_modules/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/plugins/daterangepicker/daterangepicker.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/moment.min.js', ['block' => 'script-block']) ?>
<?= $this->Html->script('/plugins/daterangepicker/daterangepicker.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function() {
        $("input[type=checkbox]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });

        $("input[name=data_baixa]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR"
        });

        var configRangePicker = {
            locale: {
                "format": "DD/MM/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Limpar",
                "customRangeLabel": "Customizado",
                "daysOfWeek": ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
                "monthNames": ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            },
            autoUpdateInput: false,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Últimos 7 dias': [moment().subtract(6, 'days'), moment()],
                'Últimos 30 dias': [moment().subtract(29, 'days'), moment()],
                'Este Mês': [moment().startOf('month'), moment().endOf('month')],
                'Mês Passado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        $("input[name=vencimento_range]").daterangepicker(configRangePicker, function(start, end, label) {

        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=data_vencimento_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=data_vencimento_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=data_vencimento_de]").val("");
            $("input[name=data_vencimento_ate]").val("");
        });

        $("input[name=settle_range]").daterangepicker(configRangePicker, function(start, end, label) {

        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format("DD/MM/YYYY") + " - " + picker.endDate.format("DD/MM/YYYY"));
            $("input[name=data_baixa_de]").val(picker.startDate.format("YYYY-MM-DD"));
            $("input[name=data_baixa_ate]").val(picker.endDate.format("YYYY-MM-DD"));
        }).on('cancel.daterangepicker', function(ev, picker) {
            $(this).val("");
            $("input[name=data_baixa_de]").val("");
            $("input[name=data_baixa_ate]").val("");
        });



        $("input[name=all]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function(e) {
            checklist = "";
            $("input[type=checkbox][name=flag]").each(function() {
                $(this).off("ifChecked");
                $(this).iCheck("check");
                checklist += $(this).data("payable-id") + ",";
                $(this).on('ifChecked', function(e) {
                    $.getJSON("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/A/" + $(this).data("payable-id"),
                        function(data, code) {
                            if (checkstatus(code)) {
                                $("div[id=itens]").html(data.itens);
                                $("span[id=total]").html(formatMoney(data.total, 2));
                            }
                        });
                });
            });
            $.getJSON("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/LA/" + checklist.replace(/,+$/, ''),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens]").html(data.itens);
                        $("span[id=total]").html(formatMoney(data.total, 2));
                    }
                });
        }).on('ifUnchecked', function(e) {
            checklist = "";
            $("input[type=checkbox][name=flag]").each(function() {
                $(this).off("ifUnchecked");
                $(this).iCheck("uncheck");
                checklist += $(this).data("payable-id") + ",";
                $(this).on('ifUnchecked', function(e) {
                    $.getJSON("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/R/" + $(this).data("payable-id"),
                        function(data, code) {
                            if (checkstatus(code)) {
                                $("div[id=itens]").html(data.itens);
                                $("span[id=total]").html(formatMoney(data.total, 2));
                            }
                        });
                });
            });
            $.getJSON("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/LR/" + checklist.replace(/,+$/, ''),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens]").html(data.itens);
                        $("span[id=total]").html(formatMoney(data.total, 2));
                    }
                });
        });

        <?php
        $found = false;
        foreach ($payables as $payable) {
            if (!in_array($payable->id, $cart)) {
                $found = true;
            }
        }
        if (!$found) {
            ?>
        $("input[name=all]").iCheck("check");
        <?php
        }
        ?>

        $("input[name=flag]").iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        }).on('ifChecked', function(e) {
            $.getJSON("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/A/" + $(this).data("payable-id"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens]").html(data.itens);
                        $("span[id=total]").html(formatMoney(data.total, 2));
                    }
                });
        }).on('ifUnchecked', function(e) {
            $.getJSON("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/R/" + $(this).data("payable-id"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens]").html(data.itens);
                        $("span[id=total]").html(formatMoney(data.total, 2));
                    }
                });
        });

        $("#settle").click(function() {
            $.post("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'settle']) ?>",
                $("#form-settle").serialize(),
                function(data, code) {
                    window.location.href = "<?= $this->Url->build(['controller' => 'Payables', 'action' => 'index']) ?>";
                });
        });

        $("button[id=clear-cart]").click(function() {
            $.get("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'cart']) ?>/C",
                function(data, code) {
                    if (checkstatus(code)) {
                        $("div[id=itens]").html(data.itens);
                        $("span[id=total]").html(formatMoney(data.total, 2));
                        $("input[name=flag]").iCheck("uncheck");
                    }
                });
        });

        $("select[name=tipo]").change(function() {
            tipo = $(this).val();
            $.get("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'selector']) ?>/" + ((tipo !== "") ? tipo : "XX"),
                function(data, code) {
                    if (checkstatus(code)) {
                        $("#selector").html(data);
                    }
                });
        });

        <?php
        if ($this->request->is('get')) {
            if (!is_null($this->request->getQuery('tipo'))) {
                $default = '';
                switch ($this->request->getQuery('tipo')) {
                    case 'CC':
                        $default = $this->request->getQuery('broker_id');
                        break;
                    case 'CT':
                        $default = $this->request->getQuery('broker_transfer_id');
                        break;
                    case 'SP':
                        $default = $this->request->getQuery('distributor_id');
                        break;
                    case 'PC':
                        $default = $this->request->getQuery('plant_id');
                        break;
                    case 'FR':
                        $default = $this->request->getQuery('carrier_id');
                        break;
                    case 'FT':
                        $default = $this->request->getQuery('carrier_transfer_id');
                        break;
                    case 'CV':
                        $default = $this->request->getQuery('seller_id');
                        break;
                }
                ?>
        $.get("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'selector']) ?>/<?= $this->request->getQuery('tipo') ?>/<?= $default ?>",
            function(data, code) {
                if (checkstatus(code)) {
                    $("#selector").html(data);
                }
            });
        <?php
            }
            if (!is_null($this->request->getQuery('broker_purchase_id'))) {
                ?>
        $.get("<?= $this->Url->build(['controller' => 'Payables', 'action' => 'brokerPurchases']) ?>/<?= $this->request->getQuery('broker_id') ?>/<?= $this->request->getQuery('broker_purchase_id') ?>",
            function(data, code) {
                if (checkstatus(code)) {
                    $("div[id=purchases]").html(data);
                }
            });
        <?php
            }
        }
        ?>

    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Contas a Pagar']
]);
echo $this->Breadcrumbs->render();
?>

<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                            <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                            <div class="row">
                                <div class="col-md-2 col-sm-12">
                                    <?= $this->Form->control('vencimento_range', ['type' => 'text', 'label' => 'Data de Vencimento']) ?>
                                    <?= $this->Form->control('data_vencimento_de', ['type' => 'hidden']) ?>
                                    <?= $this->Form->control('data_vencimento_ate', ['type' => 'hidden']) ?>
                                </div>
                                <div class="col-md-2 col-sm-12">
                                    <?= $this->Form->control('settle_range', ['type' => 'text', 'label' => 'Data da Baixa']) ?>
                                    <?= $this->Form->control('data_baixa_de', ['type' => 'hidden']) ?>
                                    <?= $this->Form->control('data_baixa_ate', ['type' => 'hidden']) ?>
                                </div>
                                <div class="col-md-3 col-sm-12">
                                    <?= $this->Form->control('tipo', ['type' => 'select', 'options' => $payable_types, 'label' => 'Tipo', 'empty' => true]) ?>
                                </div>
                                <div class="col-md-2 col-sm-12 mt-4">
                                    <?= $this->Form->control('pendentes', ['type' => 'checkbox', 'label' => 'Pendentes']) ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <div id="selector"></div>
                                </div>
                                <div class="col-md-3">
                                    <div id="purchases"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <?= $this->element('button_filter') ?>
                                    <?= $this->element('button_clear') ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-primary mt-2 mb-3" data-toggle="modal" data-target="#baixa">Dar Baixa</button>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <table class="table table-hover">
                            <tr>
                                <td class="standout" width="30%">Itens</td>
                                <td class="money">
                                    <div id="itens" class="standout"><?= sizeof($cart) - 1 ?></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="standout">Total</td>
                                <td class="money standout">R$ <span id="total"><?= number_format($total, 2, ',', '.') ?></span></td>
                            </tr>
                        </table>
                        <button id="clear-cart" type="button" class="btn btn-primary pull-right">Limpar</button>
                    </div>
                    <div class="col-md-9 col-sm-12">
                        <table class="table table-hover extra-slim-row">
                            <tr>
                                <th style="text-align: center">
                                    <?= $this->Form->control('all', ['type' => 'checkbox', 'label' => false]) ?>
                                </th>
                                <th>Tipo</th>
                                <th>Data Vencimento</th>
                                <th>Data Baixa</th>
                                <th class="money">Valor</th>
                                <th>Descrição</th>
                            </tr>
                            <?php
                            if (sizeof($payables) > 0) {
                                foreach ($payables as $payable) {
                                    ?>
                            <tr<?= $payable->baixa ? ' style="background-color: lightgray"' : '' ?>>
                                <td style="text-align: center">
                                    <?php
                                            if (!$payable->baixa) {
                                                echo $this->Form->control('flag', [
                                                    'type' => 'checkbox',
                                                    'label' => false,
                                                    'data-payable-id' => $payable->id,
                                                    'checked' => in_array($payable->id, $cart)
                                                ]);
                                            }
                                            ?>
                                </td>
                                <td><?= $payable_types[$payable->tipo] ?></td>
                                <td><?= $payable->data_vencimento ?></td>
                                <td>
                                    <div id="data-baixa-<?= $payable->id ?>"><?= $payable->data_baixa ?></div>
                                </td>
                                <td style="text-align: right"><?= $this->Number->currency($payable->valor) ?></td>
                                <td>
                                    <?php
                                            switch ($payable->tipo) {
                                                case 'CC':
                                                    $purchase =  $payable->sales_purchase->purchase;
                                                    $msg = 'Contrato: ' . $purchase->numero_contrato . '<br>' .
                                                        ' Data Venda: ' . $payable->sales_purchase->sale->data_venda . '<br>' .
                                                        ' Corretora: ' . (!is_null($purchase->broker) ? $purchase->broker->razao_social : '');
                                                    echo $msg;
                                                    break;
                                                case 'CT':
                                                    $msg = 'Contrato: ' . $payable->purchase_transfer->purchase->numero_contrato . '<br>' .
                                                        ' Data Transferência: ' . date('d/m/Y', strtotime(str_replace('/', '-', $payable->purchase_transfer->created))) . '<br>' .
                                                        ' Corretora: ' . $payable->purchase_transfer->purchase->broker->razao_social;
                                                    echo $msg;
                                                    break;
                                                case 'FR':
                                                    $contracts = '';
                                                    foreach ($payable->sale->purchases as $purchase) {
                                                        $contracts .= $purchase->numero_contrato . ' - ' . $purchase->plant->razao_social . '<br>';
                                                    }
                                                    $msg = 'Contrato: ' . $contracts .
                                                        ' Data Venda: ' . $payable->sale->data_venda . '<br>' .
                                                        ' Transportadora: ' . $payable->sale->carrier->razao_social;
                                                    echo $msg;
                                                    break;
                                                case 'FT':
                                                    $msg = 'Contrato: ' . $payable->purchase_transfer->purchase->numero_contrato .
                                                        ' Data Transferência: ' . date('d/m/Y', strtotime(str_replace('/', '-', $payable->purchase_transfer->created))) . '<br>' .
                                                        ' Transportadora: ' . $payable->purchase_transfer->carrier->razao_social;
                                                    echo $msg;
                                                    break;
                                                case 'SP':
                                                    $msg = 'Contrato: ' . $payable->sales_purchase->purchase->numero_contrato .
                                                        ' Data Venda: ' . $payable->sales_purchase->sale->data_venda . '<br>' .
                                                        ' Usina: ' . $payable->sales_purchase->purchase->plant->razao_social . '<br>' .
                                                        ' Distribuidora: ' . $payable->sales_purchase->purchase->distributor->razao_social;
                                                    echo $msg;
                                                    break;
                                                case 'CV':
                                                    $contracts = '';
                                                    foreach ($payable->sale_detail->sale->purchases as $purchase) {
                                                        $contracts .= $purchase->numero_contrato . ' - ' . $purchase->plant->razao_social . '<br>';
                                                    }
                                                    $msg = 'Contrato: ' . $contracts .
                                                        ' Data Venda: ' . $payable->sale_detail->sale->data_venda . '<br>' .
                                                        ' Cliente: ' . $payable->sale_detail->presale->client->razao_social . '<br>' .
                                                        ' Vendedor: ' . $payable->sale_detail->presale->client->seller->razao_social;
                                                    echo $msg;
                                                    break;
                                                case 'PC':
                                                    $msg = 'Contrato: ' . $payable->purchase_payment->purchase->numero_contrato .
                                                        ' Data Compra: ' . $payable->purchase_payment->purchase->data_compra . '<br>' .
                                                        ' Usina: ' . $payable->purchase_payment->purchase->plant->razao_social;
                                                    echo $msg;
                                                    break;
                                            }
                                            ?>
                                </td>
                                </tr>
                                <?php
                                    }
                                } else {
                                    ?>
                                <tr>
                                    <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                                </tr>
                                <?php } ?>
                        </table>
                        <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="baixa" tabindex="-1" role="dialog" aria-labelledby="baixaModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 50% !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="baixaModalLabel">Baixa</h5>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null, ['id' => 'form-settle']) ?>
                <div class="row">
                    <div class="col">
                        <?= $this->Form->control('account_id', ['type' => 'select', 'options' => $accounts, 'label' => 'Conta Origem']) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <?= $this->Form->control('payment_method_id', ['type' => 'select', 'options' => $payment_methods, 'label' => 'Forma de Pagamento']) ?>
                    </div>
                    <div class="col">
                        <?= $this->Form->control('data_baixa', ['type' => 'text', 'label' => 'Data Baixa', 'value' => date('d/m/Y')]) ?>
                    </div>
                </div>
                <?= $this->Form->end() ?>
                <hr>
                <button id="settle" type="button" class="btn btn-primary pull-right">Dar Baixa</button>
            </div>
        </div>
    </div>
</div>

<?php
//dump($payables);
?>