<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<script>
function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
   $(document).ready(function() {
        phone_types = [];
        <?php foreach ($phone_types as $key => $type) { ?>
            phone_types["<?= $key ?>"] = "<?= $type ?>";
        <?php } ?>

        $("#part1").load("<?= $this->Url->build(['action' => 'part1', $client->isNew() ? 'new' : $client->id]) ?>");

        $("#email").on("click", ".fa-edit", function() {
            anchor = $(this).data("anchor");
            if ($("#edit-email-area-" + anchor + " form[id|=form-email]").length === 0) {
                $("#edit-email-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Clients', 'action' => 'email']) ?>/<?= $client->isNew() ? 'new' : $client->id ?>/" +
                    $(this).data("id") + "/" +
                    anchor);
            }
            $("#edit-email-" + anchor).show();
            $("#view-email-" + anchor).hide();
        });

        $("#phone").on("click", ".fa-edit", function() {
            anchor = $(this).data("anchor");
            if ($("#edit-phone-area-" + anchor + " form[id|=form-phone]").length === 0) {
                $("#edit-phone-area-" + anchor).load(
                    "<?= $this->Url->build(['controller' => 'Clients', 'action' => 'phone']) ?>/<?= $client->isNew() ? 'new' : $client->id ?>/" +
                    $(this).data("id") + "/" +
                    anchor);
            }
            $("#edit-phone-" + anchor).show();
            $("#view-phone-" + anchor).hide();
        });


        $("#email").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-email-area-" + anchor).html($("#form-email-" + anchor + " input[id=email]").val());
            var email = $("#form-email-" + anchor + " input[id=email]").val();
            if (validateEmail(email)) {
                $("#edit-email-" + anchor).hide();
                $("#view-email-" + anchor).show();
            }else{
                swal({
                    title: 'Erro',
                    text: 'E-mail inválido',
                    confirmButtonColor: '#079dff',
                    confirmButtonText: 'CANCELAR',
                    confirmButtonClass: 'btn btn-lg btn-primary',
                    buttonsStyling: false,
                    reverseButtons: true
                });
            }
        });

        $("#phone").on("click", ".fa-check-square", function() {
            anchor = $(this).data("anchor");
            $("#view-phone-area-" + anchor + " span[id=type]").html(phone_types[$("#form-phone-" + anchor + " input[name=tipo]:checked").val()]);
            $("#view-phone-area-" + anchor + " span[id=ddd]").html("(" + $("#form-phone-" + anchor + " input[id=ddd]").val() + ")");
            $("#view-phone-area-" + anchor + " span[id=phone]").html($("#form-phone-" + anchor + " input[id=telefone]").val());

            var dddteste = $("#form-phone-" + anchor + " input[id=ddd]").val();
            var telteste = $("#form-phone-" + anchor + " input[id=telefone]").val();
            if (isNaN(dddteste) || telteste.includes("_")){
                swal({
                    title: 'Erro',
                    text: 'Dados do telefone inválidos, favor revisá-los',
                    confirmButtonColor: '#079dff',
                    confirmButtonText: 'CANCELAR',
                    confirmButtonClass: 'btn btn-lg btn-primary',
                    buttonsStyling: false,
                    reverseButtons: true
                });
            }else{
                $("#edit-phone-" + anchor).hide();
                $("#view-phone-" + anchor).show();
            }
        });

        $(".fa-plus-square").click(function() {
            anchor = anchorize();
            switch ($(this).data("type")) {
                case "email":
                    $("#no-email").remove();
                    $("#email-add").before(
                        '<tr id="edit-email-' + anchor + '">' +
                        '<td>' +
                        '<div id="edit-email-area-' + anchor + '"></div>' +
                        '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'email\', \'' + anchor + '\', \'new\')"></i>' +
                        '<i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="' + anchor + '"></i>' +
                        '</td>' +
                        '</tr>' +
                        '<tr id="view-email-' + anchor + '" style="display:none">' +
                        '<td>' +
                        '<span id="view-email-area-' + anchor + '" style="font-size: 14px">' +
                        '</span>' +
                        '<i class="fa fa-edit pull-right" data-type="email" data-anchor="' + anchor + '" data-id="new"></i>' +
                        '</td>' +
                        '</tr>'
                    );
                    $("#edit-email-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Clients', 'action' => 'email']) ?>/<?= $client->isNew() ? 'new' : $client->id ?>/" +
                        "new" + "/" +
                        anchor);
                    break;
                case "phone":
                    $("#no-phone").remove();
                    $("#phone-add").before(
                        '<tr id="edit-phone-' + anchor + '">' +
                        '<td>' +
                        '<div id="edit-phone-area-' + anchor + '"></div>' +
                        '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'phone\', \'' + anchor + '\', \'new\')"></i>' +
                        '<i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="' + anchor + '"></i>' +
                        '</td>' +
                        '</tr>' +
                        '<tr id="view-phone-' + anchor + '" style="display:none">' +
                        '<td>' +
                        '<span id="view-phone-area-' + anchor + '" style="font-size: 14px">' +
                        '<span class="mr-2" id="ddd"></span>' +
                        '<span class="mr-2" id="phone"></span>' +
                        '<span class="mr-2" id="type"></span>' +
                        '</span>' +
                        '<i class="fa fa-edit pull-right" data-type="phone" data-anchor="' + anchor + '" data-id="new"></i>' +
                        '</td>' +
                        '</tr>'
                    );
                    $("#edit-phone-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Clients', 'action' => 'phone']) ?>/<?= $client->isNew() ? 'new' : $client->id ?>/" +
                        "new" + "/" +
                        anchor);
                    break;
            }
        });

        $("#save").click(function() {
            HoldOn.open({
                theme: "sk-cube-grid"
            });
            var part1 = $.Deferred();
            // console.log(part1);
            // var part2 = $.Deferred();
            var email = $.Deferred();
            var phone = $.Deferred();
            client_id = $("#form-part1").data("id");
            $.post("<?= $this->Url->build(['action' => 'part1']) ?>/" + client_id,
                $("#form-part1").serialize(),
                function(data) {
                    $("#part1").html(data);
                    if ($("<div>" + data + "</div>").find("#form-part1").data("error") === "N") {
                        part1.resolve("OK");
                        client_id = $("<div>" + data + "</div>").find("#form-part1").data("id");
                        //     $("#form-part2").serialize(),
                        //     function(data) {
                        //         $("#part2").html(data);
                        //         if ($("<div>" + data + "</div>").find("#form-part2").data("error") === "N") {
                        //             part2.resolve("OK");
                        //         } else {
                        //             part2.resolve("NG");
                        //         }
                        //     });
                        //======== Salvar emails ======
                        var req_email = [];
                        $("form[id|='form-email']").each(function() {
                            anchor = $(this).data("anchor");
                            req_email.push($.post("<?= $this->Url->build(['controller' => 'Clients', 'action' => 'email']) ?>/" + client_id + "/" + $(this).data("id") + "/" + anchor,
                                $("#form-email-" + anchor).serialize()));
                        });
                        if (req_email.length > 0) {
                            $.when.apply(undefined, req_email).then(function() {
                                var objects = arguments;
                                var saved = true;
                                var data = [];
                                if (Array.isArray(objects[0])) {
                                    for (i = 0; i < objects.length; i++) {
                                        data.push(objects[i][0]);
                                    }
                                } else {
                                    data.push(objects[0]);
                                }
                                for (i = 0; i < data.length; i++) {
                                    anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-email']").data("anchor");
                                    id = $("<div>" + data[i] + "</div>").find("form[id|='form-email']").data("id");
                                    $("#edit-email-area-" + anchor).html(data[i]);
                                    if(id !== "new"){
                                        $("#trash-" + anchor).attr("onclick", "remove('email', '" + anchor + "', " + id + ")");
                                    }else{
                                        $("#trash-" + anchor).attr("onclick", "remove('email', '" + anchor + "', 'new')");
                                    }
                                    
                                    if ($("<div>" + data[i] + "</div>").find("#form-email-" + anchor).data("error") === "N") {} else {
                                        saved = false;
                                    }
                                }
                                email.resolve(saved ? "OK" : "NG");
                            });
                        } else {
                            email.resolve("OK");
                        }
                        if ($("#form-delete-email option").length > 0) {
                            $.post("<?= $this->Url->build(['controller' => 'Clients', 'action' => 'email-delete']) ?>",
                                $("#form-delete-email").serialize());
                        }
                        //======== Salvar telefones ======
                        var req_phone = [];
                        $("form[id|='form-phone']").each(function() {
                            anchor = $(this).data("anchor");
                            req_phone.push($.post("<?= $this->Url->build(['controller' => 'Clients', 'action' => 'phone']) ?>/" + client_id + "/" + $(this).data("id") + "/" + anchor,
                                $("#form-phone-" + anchor).serialize()));
                        });
                        if (req_phone.length > 0) {
                            $.when.apply(undefined, req_phone).then(function() {
                                var objects = arguments;
                                var saved = true;
                                var data = [];
                                if (Array.isArray(objects[0])) {
                                    for (i = 0; i < objects.length; i++) {
                                        data.push(objects[i][0]);
                                    }
                                } else {
                                    data.push(objects[0]);
                                }
                                for (i = 0; i < data.length; i++) {
                                    anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-phone']").data("anchor");
                                    id = $("<div>" + data[i] + "</div>").find("form[id|='form-phone']").data("id");
                                    $("#edit-phone-area-" + anchor).html(data[i]);
                                    if(id !== "new"){
                                        $("#trash-" + anchor).attr("onclick", "remove('phone', '" + anchor + "', " + id + ")");
                                    }else{
                                        $("#trash-" + anchor).attr("onclick", "remove('phone', '" + anchor + "', 'new')");
                                    }
                                    if ($("<div>" + data[i] + "</div>").find("#form-phone-" + anchor).data("error") === "N") {} else {
                                        saved = false;
                                    }
                                }
                                phone.resolve(saved ? "OK" : "NG");
                            });
                        } else {
                            phone.resolve("OK");
                        }
                        if ($("#form-delete-phone option").length > 0) {
                            $.post("<?= $this->Url->build(['controller' => 'Clients', 'action' => 'phone-delete']) ?>",
                                $("#form-delete-phone").serialize());
                        }
                    } else {
                        part1.resolve("NG");
                        // part2.resolve("NG");
                        email.resolve("NG");
                        phone.resolve("NG");
                    }
                });
            $.when(part1, email, phone).done(function(v1, v3, v4) {
                $("#save").attr("disabled", false);
                console.log("part1: " + v1 + "email: " + v3 + " phone: " + v4);
                HoldOn.close();
                if (v1 === "OK" && v3 === "OK" && v4 === "OK") {
                    toast("Cadastro salvo com sucesso");
                }
            });

        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Este registro será removido após as alterações serem salvas, tem certeza que deseja remover?',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                switch (type) {
                    case "email":
                        $("#edit-email-" + anchor).remove();
                        $("#view-email-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-email-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "phone":
                        $("#edit-phone-" + anchor).remove();
                        $("#view-phone-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-phone-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                }
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Clientes', 'url' => ['action' => 'index?ativo=1']],
    ['title' => 'Cliente']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div id="part1"></div>
        <div class="row">
            <div class="col-md-3">
                <p class="mt-3">Emails Secundários</p>
                <table id="email" class="table table-hover">
                    <?php
                    if (isset($client->client_emails) and sizeof($client->client_emails) > 0) {
                        foreach ($client->client_emails as $email) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-email-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-email-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('email', '<?= $anchor ?>', <?= $email->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-email-<?= $anchor ?>">
                                <td>
                                    <span id="view-email-area-<?= $anchor ?>" style="font-size: 14px">
                                        <?= $email->email ?>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="email" data-anchor="<?= $anchor ?>" data-id="<?= $email->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr id="no-email">
                            <td class="no-data-found">Nenhum Email Cadastrado</td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr id="email-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="email"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-email']) ?>
                <?= $this->Form->control('delete-email-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-5">
                <p class="mt-3">Telefones</p>
                <table id="phone" class="table table-hover">
                    <?php
                    if (isset($client->client_phones) and sizeof($client->client_phones) > 0) {
                        foreach ($client->client_phones as $phone) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-phone-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-phone-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('phone', '<?= $anchor ?>', <?= $phone->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-phone-<?= $anchor ?>">
                                <td>
                                    <span id="view-phone-area-<?= $anchor ?>" style="font-size: 14px">
                                        <span class="mr-2" id="ddd">(<?= $phone->ddd ?>)</span>
                                        <span class="mr-2" id="phone"><?= $phone->telefone ?></span>
                                        <span class="mr-2" id="type"><?= $phone_types[$phone->tipo] ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="phone" data-anchor="<?= $anchor ?>" data-id="<?= $phone->id ?>"></i>
                                </td>
                            </tr>
                        <?php
                            }
                        } else {
                            ?>
                        <tr id="no-phone">
                            <td class="no-data-found">Nenhum Telefone Cadastrado</td>
                        </tr>
                    <?php
                    }
                    ?>
                    <tr id="phone-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="phone"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-phone']) ?>
                <?= $this->Form->control('delete-phone-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <!-- <div id="part2"></div> -->
        <div class="row mt-4">
            <div class="col">
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->element('button_return_client') ?>
            </div>
        </div>
        
    </div>
</div>