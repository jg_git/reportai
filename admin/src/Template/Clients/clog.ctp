<?=
$this->Form->create($log, [
    'id' => 'form-log-' . $anchor,
    'type' => 'file',
    'data-id' => $log->isNew() ? 'new' : $log->id,
    'data-error' => sizeof($log->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor
])
?>
<div class="row">
    <div class="col">
        <?= $this->Form->control('descricao', ['type' => 'textarea', 'label' => 'Descrição']) ?>
    </div>
</div>
<div class="row">
    <div class="col">
        <?= $this->Form->control('file', ['type' => 'file', 'label' => '']); ?>
    </div>
</div>
<?= $this->Form->end() ?>
<?php if (!$log->isNew()) { ?>
    <div id="created" style="display:none"><?= $log->modified ?></div>
    <div id="user" style="display:none"><?= $log->user->nome ?></div>
    <div id="attach" style="display:none"><?= $this->Html->link($log->display, '/files/ClientLogs/file/' . $log->file, ['target' => '_blank']) ?></div>
<?php } ?>
