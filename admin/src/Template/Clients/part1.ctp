<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('#tipo-pj input[type=radio]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat',
                increaseArea: '20%'
            }).on('ifClicked', function (e) {
                if ($(this)[0]['name'] === 'tipo') {
                    switch ($(this).val()) {
                        case "J":
                            $("label[for=razao-social]").html('<p style="display: inline"> Razão Social</p>');
                            $("label[for=cnpj-cpf]").html('<p style="display: inline">CNPJ</p>');
                            $("#nome-fantasia").prop("disabled", false);
                            $("#cnpj-cpf").inputmask("99.999.999/9999-99");
                            break;
                        case "F":
                            $("label[for=razao-social]").html('<p style="display: inline"> Nome do cliente</p>');
                            $("label[for=cnpj-cpf]").html('<p style="display: inline"> CPF</p>');
                            $("#nome-fantasia").val('');
                            $("#nome-fantasia").prop("disabled", true);
                            $("#cnpj-cpf").inputmask("999.999.999-99");
                            break;
                    }
                }
            });
        }, 500);

<?php if ($client->tipo == 'J') { ?>
            $("input[name=cnpj_cpf]").inputmask("99.999.999/9999-99");
<?php } else { ?>
            $("input[name=cnpj_cpf]").inputmask("999.999.999-99");
<?php } ?>
        $("input[name=inscricao_estadual]").inputmask("999.999.999.999");
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($client, [
    'id' => 'form-part1',
    'data-id' => $client->isNew() ? 'new' : $client->id,
    'data-error' => sizeof($client->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Cliente</h5>
<div class="row mb-3 mt-3">
    <div id="tipo-pj" class="col-md-3 col-sm-12">
        <?php
            $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            if(strpos($checagem, "view")==true){
            ?>
                <?= $this->Form->radio('tipo', $entity_types, ['default' => 'J', 'disabled' => true])?>
            <?php }else{ ?>
                <?= $this->Form->radio('tipo', $entity_types, ['default' => 'J'])?>
            <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
            ?>
                <?= $this->Form->control('razao_social', ['type' => 'text', 'label' => $client->tipo == 'J' ? 'Razão Social' : 'Nome do cliente', 'disabled' => true])?>
            <?php }else{ ?>
            
                <div style="margin-bottom: .5rem">
                <?php 
                    if ($client->tipo=='J'){
                        echo '<label for="razao-social" style="display: inline; margin-bottom: .5rem">Razão Social</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    }
                    else{
                        echo '<label for="razao-social" style="display: inline; margin-bottom: .5rem">Nome do Cliente</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    }
                ?>
            </div>
                <?= $this->Form->control('razao_social', ['type' => 'text', 'label' => false ])?>
            <?php } ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
            ?>
                <?= $this->Form->control('nome_fantasia', ['type' => 'text', 'label' => 'Nome Fantasia', 'disabled' => true])?>  
            <?php }else{ ?>
                <?= $this->Form->control('nome_fantasia', ['type' => 'text', 'label' => 'Nome Fantasia', 'disabled' => $client->tipo == 'J' ? false : true])?>  
            <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
    <?php
            if(strpos($checagem, "view")==true){
            ?> 
                <?= $this->Form->control('cnpj_cpf', ['type' => 'text', 'label' => ($client->tipo == 'J') ? 'CNPJ' : 'CPF', 'disabled' => true])?>
            <?php }else{ ?>
            <div style="margin-bottom: .5rem">
                <?php 
                    if ($client->tipo=='J'){
                        echo '<label for="cnpj-cpf" style="display: inline; margin-bottom: .5rem">CNPJ</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    }
                    else{
                        echo '<label for="cnpj-cpf" style="display: inline; margin-bottom: .5rem">CPF</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                    }
                ?>
            </div>
                <?= $this->Form->control('cnpj_cpf', ['type' => 'text', 'label' => false ])?>
            <?php } ?>
            
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('inscricao_estadual', ['type' => 'text', 'label' => 'Inscrição Estadual', 'disabled' => true]);
            }else{
                echo $this->Form->control('inscricao_estadual', ['type' => 'text', 'label' => 'Inscrição Estadual']);
            }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('inscricao_municipal', ['type' => 'text', 'label' => 'Inscrição Municipal', 'disabled' => true]);
            }else{
                echo $this->Form->control('inscricao_municipal', ['type' => 'text', 'label' => 'Inscrição Municipal']);
            }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('email', ['type' => 'text', 'label' => 'Email Principal', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="email" style="display: inline; margin-bottom: .5rem">Email Principal</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('email', ['type' => 'text', 'label' => false ]);
            }
        ?>
    </div>
</div>


<script>
    function initMap(myLatLng) {
        if (!myLatLng) {
            var vlat = parseFloat($("#form-part1 input[id=latitude]").val());
            var vlng = parseFloat($("#form-part1 input[id=longitude]").val());
            var myLatLng = {
                lat: -23.5965987,
                lng: -48.0706708
            };
            myLatLng.lat = vlat;
            myLatLng.lng = vlng;
        }
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 16,
            center: myLatLng,
        });
        new google.maps.Marker({
            position: myLatLng,
            map,
            title: "Endereço",
        });
    }
    $(document).ready(function() {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });

        var vlat = parseFloat($("#form-part1 input[id=latitude]").val());
        var vlng = parseFloat($("#form-part1 input[id=longitude]").val());
        var milat = { lat: -23.5965987, lng: -48.0706708 };
        milat.lat = vlat;
        milat.lng = vlng;
        initMap(milat);
    });

    $("#form-part1 input[id=cep]").inputmask("99999-999");

    $("#form-part1 input[id=cep]").focusout(function() {
        var cep_value = $(this).val();
        var cep_stripped = cep_value.replace(/[^A-Za-z0-9]/g, '');
        var error = false;
        $.getJSON("<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'cep']) ?>" + "/" + cep_stripped, function(data) {
            $.each(data, function(key, val) {
                switch (key) {
                    case "logradouro":
                        $("#form-part1 input[id=logradouro]").val(val);
                        break;
                    case "bairro":
                        $("#form-part1 input[id=bairro]").val(val);
                        break;
                    case "localidade":
                        $("#form-part1 input[id=cidade]").val(val);
                        break;
                    case "uf":
                        $("#form-part1 select[id=estado]").val(val);
                        break;
                    case "latitude":
                        $("#form-part1 input[id=latitude]").val(val);
                        break;
                    case "longitude":
                        $("#form-part1 input[id=longitude]").val(val);
                    case "email_adm":
                        $("#form-part1 input[id=email_adm]").val(val);
                    case "url_dashboard":
                        $("#form-part1 input[id=url_dashboard]").val(val);
                    case "erro":
                        error = true;
                }
            });
            if (!error) {
                $("#form-part1 input[id=numero]").focus();
            }
        });


    });

    ////////////////////////////////////////////////////////////////////////

    $("#form-part1 input").focusout(function() {
        var numero_value = $("#form-part1 input[id=numero]").val();
        var city_value = $("#form-part1 input[id=cidade]").val();
        var street_value = $("#form-part1 input[id=logradouro]").val();
        var state_value = $("#form-part1 input[id=estado]").val();
        var hood_value = $("#form-part1 input[id=bairro]").val()

        var address = numero_value + " " + street_value.toUpperCase() + ", " + city_value + ", Brazil";
        var apiKey = "AIzaSyAlijF-i2tTacU13MYfDCfOjTnB-WA46so";
        var error = false;
        $.getJSON("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "CA&key=" + apiKey, function(data) {
            $.each(data, function(key, val) {
                // $("#form-part1 input[id=latitude]").val(val[0]["geometry"]["location"]["lat"]);
                // $("#form-part1 input[id=latitude]").prop("disabled", false);

                // $("#form-part1 input[id=longitude]").val(val[0]["geometry"]["location"]["lng"]);
                // $("#form-part1 input[id=longitude]").prop("disabled", false);

                $("#form-part1 input[id=latitude]").val(val[0]["geometry"]["location"]["lat"]);
                $("#form-part1 input[id=latitude]").prop("disabled", false);

                $("#form-part1 input[id=longitude]").val(val[0]["geometry"]["location"]["lng"]);
                $("#form-part1 input[id=longitude]").prop("disabled", false);
                if($("#form-part1 input[id=latitude]").change()){
                    var vlat = parseFloat(val[0]["geometry"]["location"]["lat"]);
                    var vlng = parseFloat(val[0]["geometry"]["location"]["lng"]);
                    var milat = { lat: -23.5965987, lng: -48.0706708 };
                    milat.lat = vlat;
                    milat.lng = vlng;
                    initMap(milat);
                }
                if($("#form-part1 input[id=longitude]").change()){
                    var vlat = parseFloat(val[0]["geometry"]["location"]["lat"]);
                    var vlng = parseFloat(val[0]["geometry"]["location"]["lng"]);
                    var milat = { lat: -23.5965987, lng: -48.0706708 };
                    milat.lat = vlat;
                    milat.lng = vlng;
                    initMap(milat);
                }
            });
        });
    });
</script>

<?= $this->Flash->render() ?>
<h5 class="mt-3">Endereço</h5>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php
        $checagem = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('cep', ['type' => 'text', 'label' => 'CEP', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="cep" style="display: inline; margin-bottom: .5rem">CEP</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('cep', ['type' => 'text', 'label' => false]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('logradouro', ['type' => 'text', 'label' => 'Logradouro', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="logradouro" style="display: inline; margin-bottom: .5rem">Logradouro</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('logradouro', ['type' => 'text', 'label' => false]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('numero', ['type' => 'text', 'label' => 'Número', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="numero" style="display: inline; margin-bottom: .5rem">Número</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('numero', ['type' => 'text', 'label' => false]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento', 'disabled' => true]);
        } else {
            echo $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento']);
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('bairro', ['type' => 'text', 'label' => 'Bairro', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="bairro" style="display: inline; margin-bottom: .5rem">Bairro</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('bairro', ['type' => 'text', 'label' => false]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('cidade', ['type' => 'text', 'label' => 'Cidade', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="cidade" style="display: inline; margin-bottom: .5rem">Cidade</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('cidade', ['type' => 'text', 'label' => false]);
        }

        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('estado', ['options' => $states, 'label' => 'Estado', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="estado" style="display: inline; margin-bottom: .5rem">Estado</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('estado', ['options' => $states, 'label' => false]);
        }

        ?>
    </div>
    <!-- <div class="col-md-3 col-sm-12">
        <div style="margin-top: .3rem; height: 80px; width: auto; display: flex; align-items: center; justify-content: center">
            <div class="btn btn-lg btn-dark" id="pesquisar">CONSULTAR LATITUDE E LONGITUDE</div>
        </div>
    </div> -->
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('latitude', ['type' => 'text', 'label' => 'Latitude', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="latitude" style="display: inline; margin-bottom: .5rem">Latitude</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('latitude', ['type' => 'text', 'label' => false, 'disabled' => $client->isNew()]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('longitude', ['type' => 'text', 'label' => 'Longitude', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="longitude" style="display: inline; margin-bottom: .5rem">Longitude</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('longitude', ['type' => 'text', 'label' => false, 'disabled' => $client->isNew()]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('contato', ['type' => 'text', 'label' => 'Nome para contato', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="contato" style="display: inline; margin-bottom: .5rem">Nome para contato</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('contato', ['type' => 'text', 'label' => false]);
        }
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('email_adm', ['type' => 'text', 'label' => 'Email do Administrador Inicial do Cliente', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="email-adm" style="display: inline; margin-bottom: .5rem">Email do Administrador Inicial do Cliente</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('email_adm', ['type' => 'text', 'label' => false]);
        }
        ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?php
        if (strpos($checagem, "view") == true) {
            echo $this->Form->control('url_dashboard', ['type' => 'text', 'label' => 'URL de Dashboard do Cliente', 'disabled' => true]);
        } else {
            echo '<div style="margin-bottom: .5rem">';
            echo '<label for="url-dashboard" style="display: inline; margin-bottom: .5rem">URL de Dashboard do Cliente</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
            echo '</div>';
            echo $this->Form->control('url_dashboard', ['type' => 'text', 'label' => false]);
        } ?>
    </div>
</div>
<br>
<br>
<div class="row">
    <div class="col-md-3 col-sm-12">
    </div>
    <div class="col-md-6 col-sm-12">
        <div id="map" style="height: 20rem">
        </div>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlijF-i2tTacU13MYfDCfOjTnB-WA46so&callback=initMap&libraries=&v=weekly" async></script>
    </div>
</div>
<?= $this->Form->end() ?>