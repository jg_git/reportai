<script>
    $(document).ready(function () {
        $('.icheck-flat input').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            radioClass: 'iradio_flat',
            increaseArea: '20%'
        });

        $("#form-contact-<?= $anchor ?> input[name=cep]").inputmask("99999-999");
        $("#form-contact-<?= $anchor ?> input[name=cpf]").inputmask("999.999.999-99");
        $("#form-contact-<?= $anchor ?> input[name=telefone]").inputmask("(99) 9999-9999");
        $("#form-contact-<?= $anchor ?> input[name^=celular]").inputmask("(99) 99999-9999");
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($contact, [
    'id' => 'form-contact-' . $anchor,
    'data-id' => $contact->isNew() ? 'new' : $contact->id,
    'data-error' => sizeof($contact->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row mb-2">
    <div id="tipo-contato" class="col-md-3 col-sm-12">
        <?= $this->Form->radio('tipo', $contact_types) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-12">
        <?= $this->Form->control('nome', ['type' => 'text', 'label' => 'Nome']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cpf', ['type' => 'text', 'label' => 'CPF']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('email', ['type' => 'text', 'label' => 'Email']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('telefone', ['type' => 'text', 'label' => 'Telefone']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('celular1', ['type' => 'text', 'label' => 'Celular 1']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('celular2', ['type' => 'text', 'label' => 'Celular 2']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('celular3', ['type' => 'text', 'label' => 'Celular 3']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
