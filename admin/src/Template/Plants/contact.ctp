<script>
    $(document).ready(function () {
        $("#form-contact-<?= $anchor ?> input[id=cep]").inputmask("99999-999");
        $("#cpf").inputmask("999.999.999-99");
        $("#form-contact-<?= $anchor ?> input[id=telefone]").inputmask("(99) 9999-9999");
        $("#form-contact-<?= $anchor ?> input[id=celular]").inputmask("(99) 99999-9999");

        $("#form-contact-<?= $anchor ?> input[id=cep]").focusout(function () {
            if (!$("#form-contact-<?= $anchor ?> input[id=logradouro]").val()) {
                var cep_value = $(this).val();
                var cep_stripped = cep_value.replace(/[^A-Za-z0-9]/g, '');
                var error = false;
                $.getJSON("<?= $this->Url->build(['controller' => 'Ajax', 'action' => 'cep']) ?>" + "/" + cep_stripped, function (data) {
                    $.each(data, function (key, val) {
                        switch (key) {
                            case "logradouro":
                                $("#form-contact-<?= $anchor ?> input[id=logradouro]").val(val);
                                break;
                            case "bairro":
                                $("#form-contact-<?= $anchor ?> input[id=bairro]").val(val);
                                break;
                            case "localidade":
                                $("#form-contact-<?= $anchor ?> input[id=cidade]").val(val);
                                break;
                            case "uf":
                                $("#form-contact-<?= $anchor ?> select[id=estado]").val(val);
                                break;
                            case "erro":
                                error = true;
                        }
                    });
                    if (!error) {
                        $("#form-contact-<?= $anchor ?> input[id=numero]").focus();
                    }
                });
            }
        });
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($contact, [
    'id' => 'form-contact-' . $anchor,
    'data-id' => $contact->isNew() ? 'new' : $contact->id,
    'data-error' => sizeof($contact->getErrors()) > 0 ? 'Y' : 'N',
    'data-anchor' => $anchor])
?>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('nome', ['type' => 'text', 'label' => 'Nome']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cpf', ['type' => 'text', 'label' => 'CPF']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('email', ['type' => 'text', 'label' => 'Email']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('telefone', ['type' => 'text', 'label' => 'Telefone']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('celular', ['type' => 'text', 'label' => 'Celular']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cep', ['type' => 'text', 'label' => 'CEP']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('logradouro', ['type' => 'text', 'label' => 'Logradouro']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('numero', ['type' => 'text', 'label' => 'Número']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('complemento', ['type' => 'text', 'label' => 'Complemento']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('bairro', ['type' => 'text', 'label' => 'Bairro']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cidade', ['type' => 'text', 'label' => 'Cidade']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('estado', ['options' => $states, 'label' => 'Estado']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
