<h4>Número Contrato: <?= $purchase->numero_contrato ?></h4>
<h5>Estoque Atual: <?= number_format($purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_vendido, 0, ',', '.') ?>
</h5>
<table class="table table-hover extra-slim-row">
    <tr>
        <th>Data</th>
        <th>Descrição</th>
        <th class="money">Volume</th>
    </tr>
    <?php
    if (sizeof($ledger) > 0) {
        foreach ($ledger as $entry) {
            ?>
            <tr>
                <td><?= $entry['data'] ?></td>
                <td><?= $entry['descricao'] ?></td>
                <td class="money"><?= number_format($entry['volume'], 0, ',', '.') ?> <?= $entry['dc'] ?></td>
            </tr>
        <?php
            }
        } else {
            ?>
        <tr class="no-data-found">
            <td colspan="10">Nenhum registro encontrado</td>
        </tr>
    <?php } ?>
</table>


<!-- <//?php dump($ledger); ?> -->