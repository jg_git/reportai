<?php
for ($i = 0; $i < $parcelas; $i++) {
    $vencimento = strtotime($data_inicio . '+' . $i . ' months');
    $anchor = rand();
    ?>
<tr id="edit-payment-<?= $anchor ?>" data-index="<?= $i ?>" data-anchor="<?= $anchor ?>">
        <?= $this->Form->control('purchase_payments.' . $i . '.tipo', ['type' => 'hidden', 'value' => 'ES']) ?>
        <td><?= $this->Form->control('purchase_payments.' . $i . '.valor_provisionado', ['type' => 'text', 'label' => false]) ?></td>
        <td><?= $this->Form->control('purchase_payments.' . $i . '.data_vencimento', ['type' => 'text', 'value' => date('d/m/Y', $vencimento), 'label' => false]) ?></td>
        <td><i class="fa fa-2x fa-arrow-circle-o-right" data-index="<?= $i ?>"></i></td>
        <td><?= $this->Form->control('purchase_payments.' . $i . '.valor_pago', ['type' => 'text', 'label' => false]) ?></td>
        <td><?= $this->Form->control('purchase_payments.' . $i . '.data_pagamento', ['type' => 'text', 'label' => false]) ?></td>
        <td><?= $this->Form->control('purchase_payments.' . $i . '.pago', ['type' => 'checkbox', 'label' => false]) ?></td>
        <td><i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('<?= $anchor ?>')"></i></td>
    </tr>
<?php } ?>
