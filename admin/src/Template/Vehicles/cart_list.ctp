<table id="cart" class="table table-hover">
    <?php
    if (isset($carts) and sizeof($carts) > 0) {
        foreach ($carts as $cart) {
            $anchor = rand();
            ?>
            <tr id="view-cart-<?= $anchor ?>">
                <td>
                    <span id="view-cart-area-<?= $anchor ?>" style="font-size: 14px">
                        Placa: <?= $cart->placa ?>
                    </span>
                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('cart', '<?= $anchor ?>', <?= $cart->id ?>)"></i>
                </td>
            </tr>
            <?php
        }
    } else {
        ?>
        <tr id="no-cart">
            <td class="no-data-found">Nenhuma Carreta Cadastrada</td>
        </tr>
        <?php
    }
    ?>
    <tr id="cart-add">
        <td>
            <i class="fa fa-plus-square" data-type="cart"></i>
        </td>
    </tr>
</table>
