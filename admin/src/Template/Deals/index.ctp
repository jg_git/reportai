<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<script>
    $(document).ready(function () {
        $("button").focus(function(){
            var item = $('#cnpjcpf').val();
            item = item.replace(' ','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace(',','');
            $('#cnpjcpf').val(item);
        });

        $("button").click(function(){
            var item = $('#cnpjcpf').val();
            item = item.replace(' ','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('.','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('/','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace('-','');
            item = item.replace(',','');
            $('#cnpjcpf').val(item);
        });
    });
</script>

<?php
//PÁGINA ADICIONADA POR MATHEUS MIELLY - SERÁ MODIFICADA FUTURAMENTE
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Dados Contratos']
]);
echo $this->Breadcrumbs->render();
?>


<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col">
                <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                    <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                    
                    <div class="row">

                        <div id="cnpj-cpf" class="col-md-2 col-sm-12">
                            <?= $this->Form->control('cnpjcpf', ['type' => 'text', 'label' => 'CNPJ/CPF']) ?>
                        </div>

                        <div class="col-md-3 col-sm-12">
                            <?= $this->Form->control('razao_social', ['label' => 'Nome do Cliente']) ?>
                        </div>
                        
                        <div class="col-md-3 col-sm-12 pt-4">
                            <?= $this->element('button_filter') ?>
                            <?= $this->element('button_clear') ?>
                        </div>
                    
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="row pl-3 pt-3 pr-3 pb-2 mb-2">
                    <div class="col-md-12 col-sm-12">
                        <?= $this->element('button_new', []) ?>
                        </div>
                </div>
                <table class="table table-hover table-responsive-sm">
                    <tr>
                        <th><?= $this->Paginator->sort('Clients.cnpj_cpf', 'CPF/CNPJ') ?></th>
                        <th><?= $this->Paginator->sort('Clients.razao_social', 'Nome do Cliente') ?></th>
                        <th><?= $this->Paginator->sort('Deals.valor', 'Valor do Contrato') ?></th>
                        <th><?= $this->Paginator->sort('Deals.prazo', 'Prazo do Contrato (meses)') ?></th>
                        <th>Ações</th>
                    </tr>
                    <?php
                    $contador=0;
                    if (sizeof($deals) > 0) {
                        foreach ($deals as $deal) {
                            
                            $cpf_prealterado = $deal->client->cnpj_cpf;
                            if(strlen($cpf_prealterado)>11){
                                $cpf_alterado = substr($cpf_prealterado,0,2).'.'.substr($cpf_prealterado,2,3).'.'.substr($cpf_prealterado,5,3).'/'.substr($cpf_prealterado,8,4).'-'.substr($cpf_prealterado,12,2);
                            }else{
                                $cpf_alterado = substr($cpf_prealterado,0,3).'.'.substr($cpf_prealterado,3,3).'.'.substr($cpf_prealterado,6,3).'-'.substr($cpf_prealterado,9,2);
                            }

                            
                            if ($deal->client->ativo!='0'){
                                $contador=$contador+1;
                            ?>
                            <tr>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $cpf_alterado ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $deal->client->razao_social ?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $this->Number->currency($deal->valor)?></div></nav></td>
                                <td><nav class="navbar navbar-expand-sm" style="padding-left: 0"><div class="d-flex align-items-center justify-content-center h-100"><?= $deal->prazo ?></div></nav></td>
                                <td style="padding-bottom: 0; padding-left: 0">
                                    <nav class="navbar navbar-expand-sm" style="padding-top: 0; padding-left: 5px" >
                                        <div class="d-flex align-items-center justify-content-center h-100">
                                            <ul class="navbar-nav ml-auto">
                                                <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle" id="navbarDropdown">
                                                        <i class="fa fa-cog"></i>
                                                        <i class="fa fa-xs fa-chevron-down" style="color: #000000"></i>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-eye']) . ' ' . $this->Html->tag('span', 'Ver', ['class' => 'notification-text']), ['controller' => 'Deals', 'action' => 'view', $deal->id], ['class' => 'dropdown-item', 'escape' => false]) ?>
                                                            <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-edit']) . ' ' . $this->Html->tag('span', 'Editar', ['class' => 'notification-text']), ['controller' => 'Deals', 'action' => 'edit', $deal->id], ['class' => 'dropdown-item', 'escape' => false]) ?>

                                                            <?php 
                                                                if(implode(', ', $deal->deal_payments) == ''){
                                                                    echo $this->element('a_delete', ['id' => $deal->id]);
                                                                }else{
                                                                    echo $this->element('a_ncdelete', ['id' => $deal->id]);
                                                                }
                                                            ?>
                                                    </div>
                                                </li>
                                            </ul> 
                                        </div>
                                    </nav>         
                                </td>
                            </tr>
                            <?php
                                }
                            }
                        } 
                    if($contador==0) {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>    