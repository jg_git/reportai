<script>
    $(document).ready(function () {
        
        $("input[name=data_vigencia_inicio]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
        $("#valor").maskMoney({thousands: '.', decimal: ','}).maskMoney('mask', <?= $deal->valor ?>);
        $("input[name=data_vigencia_termino]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });

        $("input[name=data_firmacao_contrato]").datepicker({
            format: "dd/mm/yyyy",
            todayBtn: true,
            language: "pt-BR",
            orientation: "bottom"
        });
    });   
</script>

<?= $this->Flash->render() //PÁGINA CRIADA POR MATHEUS MIELLY?>
<?=
$this->Form->create($deal, [
    'id' => 'form-part1',
    'data-id' => $deal->isNew() ? 'new' : $deal->id,
    'data-error' => sizeof($deal->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Contrato</h5>
<div class="row mb-3 mt-3">
</div>
<div class="row">
    <div class="col-md-4 col-sm-12">
        <?php
            $checagem= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";;
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'label' => 'Nome Cliente', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="client_id" style="display: inline; margin-bottom: .5rem">Cliente</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('client_id', ['type' => 'select', 'options' => $clients, 'empty' => 'SELECIONE UM CLIENTE', 'label' => false]);
            }
        ?>
    </div>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('data_firmacao_contrato', ['type' => 'text', 'label' => 'Contrato Firmado em', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="data_firmacao_contrato" style="display: inline; margin-bottom: .5rem">Data Firmação do Contrato</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('data_firmacao_contrato', ['type' => 'text', 'label' => false]);
            }
        ?>    
    </div>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){
                echo $this->Form->control('valor', ['type' => 'text', 'label' => 'Valor Total do Contrato', 'disabled' => true ]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="valor" style="display: inline; margin-bottom: .5rem">Valor</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('valor', ['type' => 'text', 'label' => false]);
            }
        ?>
    </div>
</div>
<div class="row">
        <?php
            if(strpos($checagem, "view")==true){
                echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_inicio', ['type' => 'text', 'label' => 'Vigência do Contrato -  De', 'disabled' => true]).'</div>';    
                echo '<div class="col-md-4 col-sm-12">'.$this->Form->control('data_vigencia_termino', ['type' => 'text', 'label' => 'Até', 'disabled' => true]).'</div>';
            }
            else{
                echo '<div class="col-md-4 col-sm-12">'.'<div style="margin-bottom: .5rem">';
                echo '<label for="data_vigencia_inicio" style="display: inline; margin-bottom: .5rem">Vigência do Contrato - De</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('data_vigencia_inicio', ['type' => 'text', 'label' => false]).'</div>';
                
                echo '<div class="col-md-4 col-sm-12">'.'<div style="margin-bottom: .5rem">';
                echo '<label for="data_vigencia_termino" style="display: inline; margin-bottom: .5rem">Até</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo $this->Form->control('data_vigencia_termino', ['type' => 'text', 'label' => false]).'</div>';
            }
        ?>
    <div class="col-md-4 col-sm-12">
        <?php
            if(strpos($checagem, "view")==true){ 
                echo$this->Form->control('n_max_usuario', ['type' => 'number', 'label' => 'Limite Máximo de Usuários do Cliente', 'disabled' => true]);
            }else{
                echo '<div style="margin-bottom: .5rem">';
                echo '<label for="n_max_usuario" style="display: inline; margin-bottom: .5rem">Limite Máximo de Usuários do Cliente</label> <p style="color: red; display: inline; margin-bottom: .5rem"> * </p>';
                echo '</div>';
                echo$this->Form->control('n_max_usuario', ['type' => 'number', 'label' => false]);
            }
        ?>
    </div>
</div>
<?= $this->Form->end() ?>
