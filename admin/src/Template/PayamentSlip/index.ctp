<script>
    $(document).ready(function() {});
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Emissão de Boletos']
]);
echo $this->Breadcrumbs->render();
?>

<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3><center>EMISSÃO E REGISTRO DE BOLETOS</center></h3>
                        <span>Serão exibidos a baixo os boletos apenas com data de vencimento compatível</span>

                        <?= $this->Form->postbutton(
                            'REGISTRAR E EMITIR TODOS OS BOLETOS',

                            ['action' => 'gerarTodosBoletos'],
                            ['confirm' => 'Deseja Imprimir Todos os Boletos?', 'class' => 'btn btn-outline-primary statement']



                        )
                        ?>
                        <br>
                        <?php
                        // $notas = [];


                        // foreach ($receivables as $receivable) {
                        //     $notas[$receivable->id] = $receivable->sale_detail->nota_fiscal;

                        //     
                        ?>

                        <?php
                        // }



                        ?>
                        <!-- <div class="row">
                            <div class="col-md-4 col-sm-12"> -->
                        <!-- <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                                    <//?= $this->Form->create(null, ['valueSources' => 'query']) ?>

                                    <//?= $this->Form->control('receivable_id', ['type' => 'select', 'options' => $notas, 'label' => 'Nota Fiscal']) ?>
                                    <//?= $this->Form->end() ?>

                                </div>

                                <//?= $this->element('button_new', []) ?> -->



                        <!-- </div> -->
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <table class="table table-hover extra-slim-row">
                            <tr>
                                <!-- <th style="text-align: center">
                                        <//?= $this->Form->control('all', ['type' => 'checkbox', 'label' => false]) ?>
                                    </th> -->

                                <th> Tipo </th>
                                <th><?= $this->Paginator->sort('data_vencimento', 'Data Vencimento') ?></th>


                                <th><?= $this->Paginator->sort('valor', 'Valor') ?></th>


                                <th>Descrição</th>
                                <th>Ação</th>
                            </tr>
                            <?php

                            foreach ($receivables as $receivable) {
                                ?>
                                <tr<?= $receivable->baixa ? ' style="background-color: lightgray"' : '' ?>>

                                    <td><?= $receivable_types[$receivable->tipo] ?></td>
                                    <td><?= $receivable->data_vencimento ?></td>


                                    <td style=" text-align: right"><?= $this->Number->currency($receivable->valor) ?></td>

                                    <td>
                                        <?php
                                            switch ($receivable->tipo) {
                                                case 'VE':
                                                    $msg = ' Data Venda: ' . $receivable->sale_detail->sale->data_venda . '<br>' .
                                                        'Cliente: ' . $receivable->sale_detail->presale->client->razao_social . '<br>' .
                                                        'NF: ' . $receivable->sale_detail->nota_fiscal;
                                                    echo $msg;
                                                    break;
                                            }
                                            ?>
                                    </td>
                                    <td>

                                        <?= $this->Form->postbutton(
                                                'REGISTRAR BOLETO',

                                                ['action' => 'gerarboleto', $receivable->id],
                                                ['confirm' => 'Deseja registrar o Boleto?', 'class' => 'btn btn-outline-primary statement']



                                            )
                                            ?>

                                        <!-- <button type="button" class="btn btn-outline-primary statement" data-purchase-id="<//?= $receivable->id ?>">Gerar Boleto</button> -->

                                    </td>
                                    </tr>
                                <?php
                                }
                                ?>

                                <?php if ($receivables->count() > 0) { } else {
                                    ?>
                                    <tr>
                                        <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                                    </tr>
                                <?php
                                }
                                ?>
                                <?php  ?>

                        </table>
                        <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>