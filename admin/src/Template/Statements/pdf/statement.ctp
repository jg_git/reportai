<style>
    @font-face {
        font-family: Verdana;
        font-style: normal;
        font-weight: normal;
        src: url(http://themes.googleusercontent.com/static/fonts/opensans/v8/cJZKeOuBrn4kERxqtaUH3aCWcynf_cDxXwCLxiixG1c.ttf) format('truetype');
    }

    body {
        font-family: Verdana;
        font-size: 10px;
        margin-left: 0.5cm;
        margin-right: 0.5cm;
    }
</style>

<div>
    <?= $this->Html->image('rbl-logo-header.png', ['fullBase' => true]) ?>
    <hr>
    <?php if (!is_null($balance)) { ?>
        <div>
            EXTRATO CONTA CORRENTE:<br>
            <?php
                switch ($account->tipo) {
                    case 'I':
                        $msg = $account->distributor->razao_social;
                        break;
                    case 'F':
                        $msg = $account->bank->nome . ' - ' . $account->titular . ' - ' . $account->numero_agencia . ' - ' . $account->numero_conta;
                        break;
                    case 'C':
                        $msg = $account->titular;
                        break;
                }
                echo $msg;
                ?>
        </div>
        <div>DATA DE INÍCIO: <?= $date_start ?></div>
        <div>DATA FINAL: <?= $date_end ?></div>
        <div>SALDO INICIAL EM <?= $date_start ?>:
            <span class="<?= $balance > 0 ? 'credit' : ($balance < 0 ? 'debit' : '') ?>">
                <?= $this->Number->currency(abs($balance)) ?> <?= $balance > 0 ? 'C' : ($balance < 0 ? 'D' : '') ?>
            </span>
        </div>
        <br>
        <table class="table table-hover report-row">
            <tr>
                <th width="10%">DATA</th>
                <th width="25%">DESCRIÇÃO</th>
                <th width="20%">TIPO LANÇAMENTO</th>
                <th width="25%">D/C</th>
                <th width="20%">VALOR R$</th>

            </tr>
            <!-- $data = array_merge($entries, compact('entrief')); -->
            <!-- $data = array_merge($candidate, ['entries' => $entrief]); -->
            <?php
                ini_set('max_execution_time', 360);
                // if (isset($entries) and sizeof($entries) > 0) {
                //     foreach ($entrief as $entryf) {
                //         foreach ($entries as $entry) {

                //             // var_dump($entry['descricao']);
                //             var_dump($entryf['descricao']);
                //             var_dump($entry['id']);
                //         }
                //         // var_dump($entryf);
                //         // die();


                //         //    $entrief->descricao;
                //     }
                //     // die();
                // }





                if (isset($entries) and sizeof($entries) > 0) {
                    foreach ($entries as $entry) {


                        if ($entry->ledger_entry_type->dc == 'D') {
                            $balance -= $entry->valor;
                        } else {
                            $balance += $entry->valor;
                        }
                        ?>

                    <tr>
                        <td><?= $entry->data_lancamento ?></td>
                        <td><?php



                                        if ($entry->descricao == '') {
                                            echo $entry->source_transfer ? $entry->source_transfer->descricao : '';
                                            echo $entry->destination_transfer  ?  $entry->destination_transfer->descricao : '';
                                        } else {
                                            echo $entry ? $entry->descricao : '';
                                        }
                                        // if (!($entry->source_transfer->descricao) and ($entry->destionation_transfer->descricao)) {

                                        // };

                                        //->descricao 
                                        ?></td>
                        <!-- <//?php die(); ?> -->
                        <td><?= $entry->ledger_entry_type->descricao ?></td>
                        <td><?= $entry->ledger_entry_type->dc ?></td>
                        <td style="text-align:right"><?= number_format($entry->valor, 2, ',', '.') ?></td>

                    </tr>
                <?php
                        }
                    } else {
                        ?>
                <tr class="no-data-found">
                    <td colspan="5">Nenhum registro encontrado</td>
                </tr>
            <?php } ?>
        </table>
        <div>SALDO FINAL EM <?= $date_end ?>:
            <span class="<?= $balance > 0 ? 'credit' : ($balance < 0 ? 'debit' : '') ?>">
                <?= $this->Number->currency(abs($balance)) ?> <?= $balance > 0 ? 'C' : ($balance < 0 ? 'D' : '') ?>
            </span>
        </div>
    <?php } ?>
</div>