<tr data-index="<?= $index ?>">
    <?= $this->Form->control('Presales.' . $index . '.data_pedido', ['type' => 'hidden']) ?>
    <td>
        <?= $this->Form->control('client-' . $index, ['type' => 'text', 'label' => false, 'data-client' => $index]) ?>
        <?= $this->Form->control('Presales.' . $index . '.client_id', ['type' => 'hidden', 'label' => false]) ?>
    </td>
    <td><?= $this->Form->control('Presales.' . $index . '.volume_pedido', ['type' => 'text', 'label' => false]) ?></td>
    <td><?= $this->Form->control('Presales.' . $index . '.preco_venda', ['type' => 'text', 'label' => false]) ?></td>
    <td><?= $this->Form->control('Presales.' . $index . '.preco_mercado', ['type' => 'text', 'label' => false]) ?></td>
    <td>
        <table style="border: 0px">
            <?php
            $count = 0;
            for ($i = 1; $i <= 4; $i++) {
                ?>
                <tr id="payment-<?= $index ?>-<?= $i ?>" style="border: 0px;<?= $i > 1 ? 'display:none;' : '' ?>">
                    <td width="80%" style="border: 0px; <?= ($i > 1) ? 'padding-top:5px;padding-right:0px;padding-bottom:0px;padding-left:0px;' : 'padding:0px;' ?> ">
                        <?= $this->Form->control('Presales.' . $index . '.data_pagamento_' . $i, ['type' => 'text', 'label' => false, 'data-index' => $index]) ?>
                    </td>
                    <td width="20%" style="border: 0px;">
                        <?php if ($i > 1) { ?>
                            <i class="fa fa-trash delete-payment" data-index="<?= $index ?>" data-payment="<?= $i ?>"></i>
                        <?php } ?>
                        <?php if ($i == 1) { ?>
                            <i id="button-add-date" class="fa fa-plus-circle" data-index="<?= $index ?>"></i>
                        <?php } ?>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
    </td>
    <td><?= $this->Form->control('Presales.' . $index . '.retira', ['type' => 'checkbox', 'label' => false, 'data-index' => $index]) ?></td>
    <td class="standout" style="text-align: right">0</td>
    <td><i class="fa fa-trash mt-2 remove-presale" data-index="<?= $index ?>" data-presale-id="new"></i></td>
    <td></td>
    <td></td>
</tr>