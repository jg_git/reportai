<?php $uid = rand() ?>
<script>
    $(document).ready(function () {
        $("#delete-<?= $uid ?>").click(function () {
            swal({
                title: 'Atenção, este registro não pode ser removido!',
                text: 'Este cliente possui contratos abertos e não pode ser removido.',
                confirmButtonColor: '#079dff',
                confirmButtonText: 'CANCELAR',
                confirmButtonClass: 'btn btn-lg btn-primary',
                buttonsStyling: false,
                reverseButtons: true
            });
        });
    });
</script>
<?= $this->Form->create('', ['id' => 'form-delete-' . $uid, 'url' => ['action' => isset($action) ? $action : 'delete', $id]]) ?>
<?= $this->Form->end() ?>
<a href="#" id="delete-<?= $uid ?>" class="dropdown-item">
    <i class="fa fa-trash"></i>
    <span class="notification-text">
        Excluir
    </span>
</a>

