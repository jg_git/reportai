<div class="row">
    <div class="col">
        <div class="paginator">
            <ul class="pagination justify-content-center">
                <?= $paginator->first('<< ' . 'Primeiro') ?>
                <?= $paginator->prev('< ' . 'Anterior') ?>
                <?= $paginator->numbers() ?>
                <?= $paginator->next('Próximo' . ' >') ?>
                <?= $paginator->last('Último' . ' >>') ?>
            </ul>
            <p class="pull-right">
                <?= $paginator->counter(['format' => __('Página {{page}} de {{pages}}, mostrando {{current}} registro(s) do total de {{count}}')]) ?>
            </p>
        </div>
    </div>
</div>