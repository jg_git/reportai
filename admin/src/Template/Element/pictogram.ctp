<div class="card m-1 p-2" style="width: 12rem">
    <div class="card-body">
        <div class="text-center mb-2">
            <?php
            if ($pictogram->file != '') {
                echo $this->Thumb->resize('../files/Pictograms/file/' . $pictogram->file, ['height' => 100, 'width' => 100]);
            } else {
                echo $this->Thumb->resize('no-image.jpg', ['height' => 80, 'width' => 80]);
            }
            ?>
        </div>
    </div>
    <div class="text-center"><?= $pictogram->nome ?></div>
</div>
