<?php $uid = rand() ?>
<script>
    $(document).ready(function () {
        $("#delete-<?= $uid ?>").click(function () {
            swal({
                title: 'Redefinir a Senha?',
                text: 'Esta operação irá redefinir a sua senha.',
                showCancelButton: true,
                confirmButtonColor: '#079dff',
                cancelButtonColor: 'lightgray',
                confirmButtonText: 'REDEFINIR',
                cancelButtonText: 'CANCELAR',
                confirmButtonClass: 'btn btn-lg btn-primary',
                cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
                buttonsStyling: false,
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    $("#form-delete-<?= $uid ?>").submit();
                }
            });
        });
    });
</script>
<?= $this->Form->create('', ['id' => 'form-delete-' . $uid, 'url' => ['action' => isset($action) ? $action : 'delete', $id]]) ?>
<?= $this->Form->end() ?>
<a href="#" id="delete-<?= $uid ?>" class="dropdown-item">
    <i class="fa fa-asterisk"></i>
    <span class="notification-text">
        Redefinir Senha
    </span>
</a>

