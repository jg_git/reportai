<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<script>
    $(document).ready(function () {
        $.toast({
            heading: 'Aviso',
            text: '<?= $message ?>',
            showHideTransition: 'slide',
            icon: 'warning',
            loaderBg: '#f0ad4e'
        });
    });
</script>
