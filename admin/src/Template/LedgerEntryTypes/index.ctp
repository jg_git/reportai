<script>
    $(document).ready(function () {
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Tipos de Lançamentos']
]);
echo $this->Breadcrumbs->render();
?>

<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="border rounded pl-3 pt-3 pr-3 pb-2 mb-2">
                            <?= $this->Form->create(null, ['valueSources' => 'query']) ?>
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <?= $this->Form->control('descricao', ['label' => 'Descrição']) ?>
                                </div>
                                <div class="col-md-8 col-sm-12 mt-4">
                                    <?= $this->element('button_filter') ?>
                                    <?= $this->element('button_clear') ?>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                        </div>
                    </div>
                </div>
                <?= $this->element('button_new', []) ?>
                <table class="table table-hover">
                    <tr>
                        <th>Tipo</th>
                        <th>Descrição</th>
                    </tr>
                    <?php
                    if (sizeof($types) > 0) {
                        foreach ($types as $type) {
                            ?>
                            <tr>
                                <td><?= $ledger_entry_types[$type->dc] ?></td>
                                <td><?= $type->editable ? $this->Html->link($type->descricao, ['action' => 'edit', $type->id]) : $type->descricao ?></td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr>
                            <td colspan="10" class="no-data-found">Nenhum registro encontrado</td>
                        </tr>
                    <?php } ?>
                </table>
                <?= $this->element('paginator', ['paginator' => $this->Paginator]) ?>
            </div>
        </div>
    </div>
</div>
