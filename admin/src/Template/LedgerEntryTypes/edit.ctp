<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('input[name=dc]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat',
                increaseArea: '20%'
            });
        }, 500);
    });
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Tipos de Lançamentos', 'url' => ['action' => 'index']],
    ['title' => 'Tipo de Lançamento']
]);
echo $this->Breadcrumbs->render();
?>

<?= $this->Flash->render() ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <?=
                $this->Form->create($type, [
                    'id' => 'form-type',
                    'data-id' => $type->isNew() ? 'new' : $type->id,
                    'data-error' => sizeof($type->getErrors()) > 0 ? 'Y' : 'N'])
                ?>
                <div class="row mb-3">
                    <div class="col-md-3 col-sm-12 mt-4 pl-5">
                        <?= $this->Form->control('dc', ['type' => 'radio', 'options' => $ledger_entry_types, 'label' => false]) ?>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <?= $this->Form->control('descricao') ?>
                    </div>
                </div>
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->Form->end() ?>
                <?= $this->element('button_delete', ['id' => $type->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>    
    </div>
</div>
