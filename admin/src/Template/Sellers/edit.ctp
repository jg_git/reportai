<?= $this->Html->script('/node_modules/inputmask/dist/jquery.inputmask.bundle.js', ['block' => 'script-block']) ?>

<?= $this->Html->css('/node_modules/HoldOn-js/src/css/HoldOn.min.css', ['block' => 'css-block']) ?>
<?= $this->Html->script('/node_modules/HoldOn-js/src/js/HoldOn.min.js', ['block' => 'script-block']) ?>

<?= $this->Html->script('/bower_components/typeahead.js/dist/typeahead.bundle.js?v=1.1', ['block' => 'script-block']) ?>
<?= $this->Html->script('/bower_components/typeahead.js/dist/handlebars-v4.0.11.js?v=1.0', ['block' => 'script-block']) ?>

<script>
    $(document).ready(function () {
        phone_types = [];
        phone_types["F"] = "Fixo";
        phone_types["C"] = "Celular";

        account_types = [];
        account_types["C"] = "Conta Corrente";
        account_types["P"] = "Poupança";

        $("#part1").load("<?= $this->Url->build(['action' => 'part1', $seller->isNew() ? 'new' : $seller->id]) ?>");
        $("#part2").load("<?= $this->Url->build(['action' => 'part2', $seller->isNew() ? 'new' : $seller->id]) ?>");

        $("#email").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-email-area-" + anchor + " form[id|=form-email]").length === 0) {
                $("#edit-email-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'email']) ?>/<?= $seller->isNew() ? 'new' : $seller->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-email-" + anchor).show();
            $("#view-email-" + anchor).hide();
        });

        $("#phone").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-phone-area-" + anchor + " form[id|=form-phone]").length === 0) {
                $("#edit-phone-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'phone']) ?>/<?= $seller->isNew() ? 'new' : $seller->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-phone-" + anchor).show();
            $("#view-phone-" + anchor).hide();
        });

        $("#account").on("click", ".fa-edit", function () {
            anchor = $(this).data("anchor");
            if ($("#edit-account-area-" + anchor + " form[id|=form-account]").length === 0) {
                $("#edit-account-area-" + anchor).load(
                        "<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'account']) ?>/<?= $seller->isNew() ? 'new' : $seller->id ?>/" +
                        $(this).data("id") + "/" +
                        anchor);
            }
            $("#edit-account-" + anchor).show();
            $("#view-account-" + anchor).hide();
        });

        $("#email").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-email-area-" + anchor).html($("#form-email-" + anchor + " input[id=email]").val());
            $("#edit-email-" + anchor).hide();
            $("#view-email-" + anchor).show();
        });

        $("#phone").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-phone-area-" + anchor + " span[id=type]").html(phone_types[$("#form-phone-" + anchor + " input[name=tipo]:checked").val()]);
            $("#view-phone-area-" + anchor + " span[id=ddd]").html("(" + $("#form-phone-" + anchor + " input[id=ddd]").val() + ")");
            $("#view-phone-area-" + anchor + " span[id=phone]").html($("#form-phone-" + anchor + " input[id=telefone]").val());
            $("#edit-phone-" + anchor).hide();
            $("#view-phone-" + anchor).show();
        });

        $("#account").on("click", ".fa-check-square", function () {
            anchor = $(this).data("anchor");
            $("#view-account-area-" + anchor + " span[id=banco]").html($("#form-account-" + anchor + " select[name=bank_id] option:selected").text());
            $("#view-account-area-" + anchor + " span[id=agencia]").html($("#form-account-" + anchor + " input[name=numero_agencia]").val());
            $("#view-account-area-" + anchor + " span[id=conta]").html($("#form-account-" + anchor + " input[name=numero_conta]").val());
            $("#view-account-area-" + anchor + " span[id=tipo-conta]").html(account_types[$("#form-account-" + anchor + " input[name=tipo_conta]:checked").val()]);
            $("#edit-account-" + anchor).hide();
            $("#view-account-" + anchor).show();
        });

        $(".fa-plus-square").click(function () {
            anchor = anchorize();
            switch ($(this).data("type")) {
                case "email":
                    $("#no-email").remove();
                    $("#email-add").before(
                            '<tr id="edit-email-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-email-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'email\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-email-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-email-area-' + anchor + '">' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="email" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-email-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'email']) ?>/<?= $seller->isNew() ? 'new' : $seller->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
                case "phone":
                    $("#no-phone").remove();
                    $("#phone-add").before(
                            '<tr id="edit-phone-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-phone-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'phone\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-phone-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-phone-area-' + anchor + '" style="font-size: 14px">' +
                            '<span class="mr-2" id="ddd"></span>' +
                            '<span class="mr-2" id="phone"></span>' +
                            '<span class="mr-2" id="type"></span>' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="phone" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-phone-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'phone']) ?>/<?= $seller->isNew() ? 'new' : $seller->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
                case "account":
                    $("#no-account").remove();
                    $("#account-add").before(
                            '<tr id="edit-account-' + anchor + '">' +
                            '<td>' +
                            '<div id="edit-account-area-' + anchor + '"></div>' +
                            '<i id="trash-' + anchor + '" class="fa fa-trash pull-right" onclick="remove(\'account\', \'' + anchor + '\', \'new\')"></i>' +
                            '<i class="fa fa-check-square pull-right mr-2" data-type="account" data-anchor="' + anchor + '"></i>' +
                            '</td>' +
                            '</tr>' +
                            '<tr id="view-account-' + anchor + '" style="display:none">' +
                            '<td>' +
                            '<span id="view-account-area-' + anchor + '" style="font-size: 14px">' +
                            '<span class="mr-2" id="banco"></span>' +
                            'Agência: <span class="mr-2" id="agencia"></span>' +
                            'Conta: <span class="mr-2" id="conta"></span>' +
                            '<span class="mr-2" id="tipo-conta"></span>' +
                            '</span>' +
                            '<i class="fa fa-edit pull-right" data-type="account" data-anchor="' + anchor + '" data-id="new"></i>' +
                            '</td>' +
                            '</tr>'
                            );
                    $("#edit-account-area-" + anchor).load(
                            "<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'account']) ?>/<?= $seller->isNew() ? 'new' : $seller->id ?>/" +
                            "new" + "/" +
                            anchor);
                    break;
            }
        });

        $("#save").click(function () {
            HoldOn.open({theme: "sk-cube-grid"});
            var part1 = $.Deferred();
            var part2 = $.Deferred();
            var email = $.Deferred();
            var phone = $.Deferred();
            var account = $.Deferred();
            seller_id = $("#form-part1").data("id");
            $.post("<?= $this->Url->build(['action' => 'part1']) ?>/" + seller_id,
                    $("#form-part1").serialize(),
                    function (data) {
                        $("#part1").html(data);
                        if ($("<div>" + data + "</div>").find("#form-part1").data("error") === "N") {
                            part1.resolve("OK");
                            seller_id = $("<div>" + data + "</div>").find("#form-part1").data("id");
                            $.post("<?= $this->Url->build(['action' => 'part2']) ?>/" + seller_id,
                                    $("#form-part2").serialize(),
                                    function (data) {
                                        $("#part2").html(data);
                                        if ($("<div>" + data + "</div>").find("#form-part2").data("error") === "N") {
                                            part2.resolve("OK");
                                        } else {
                                            part2.resolve("NG");
                                        }
                                    });
                            //======== Salvar emails ======
                            var req_email = [];
                            $("form[id|='form-email']").each(function () {
                                anchor = $(this).data("anchor");
                                req_email.push($.post("<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'email']) ?>/" + seller_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-email-" + anchor).serialize()));
                            });
                            if (req_email.length > 0) {
                                $.when.apply(undefined, req_email).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-email']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-email']").data("id");
                                        $("#edit-email-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('email', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-email-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    email.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                email.resolve("OK");
                            }
                            if ($("#form-delete-email option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'email-delete']) ?>",
                                        $("#form-delete-email").serialize());
                            }
                            //======== Salvar telefones ======
                            var req_phone = [];
                            $("form[id|='form-phone']").each(function () {
                                anchor = $(this).data("anchor");
                                req_phone.push($.post("<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'phone']) ?>/" + seller_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-phone-" + anchor).serialize()));
                            });
                            if (req_phone.length > 0) {
                                $.when.apply(undefined, req_phone).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-phone']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-phone']").data("id");
                                        $("#edit-phone-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('phone', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-phone-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    phone.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                phone.resolve("OK");
                            }
                            if ($("#form-delete-phone option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'phone-delete']) ?>",
                                        $("#form-delete-phone").serialize());
                            }
                            //======== Salvar contas ======
                            var req_account = [];
                            $("form[id|='form-account']").each(function () {
                                anchor = $(this).data("anchor");
                                req_account.push($.post("<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'account']) ?>/" + seller_id + "/" + $(this).data("id") + "/" + anchor,
                                        $("#form-account-" + anchor).serialize()));
                            });
                            if (req_account.length > 0) {
                                $.when.apply(undefined, req_account).then(function () {
                                    var objects = arguments;
                                    var saved = true;
                                    var data = [];
                                    if (Array.isArray(objects[0])) {
                                        for (i = 0; i < objects.length; i++) {
                                            data.push(objects[i][0]);
                                        }
                                    } else {
                                        data.push(objects[0]);
                                    }
                                    for (i = 0; i < data.length; i++) {
                                        anchor = $("<div>" + data[i] + "</div>").find("form[id|='form-account']").data("anchor");
                                        id = $("<div>" + data[i] + "</div>").find("form[id|='form-account']").data("id");
                                        $("#edit-account-area-" + anchor).html(data[i]);
                                        $("#trash-" + anchor).attr("onclick", "remove('account', '" + anchor + "', " + id + ")");
                                        if ($("<div>" + data[i] + "</div>").find("#form-account-" + anchor).data("error") === "N") {
                                        } else {
                                            saved = false;
                                        }
                                    }
                                    account.resolve(saved ? "OK" : "NG");
                                });
                            } else {
                                account.resolve("OK");
                            }
                            if ($("#form-delete-account option").length > 0) {
                                $.post("<?= $this->Url->build(['controller' => 'Sellers', 'action' => 'account-delete']) ?>",
                                        $("#form-delete-account").serialize());
                            }
                        } else {
                            part1.resolve("NG");
                            part2.resolve("NG");
                            email.resolve("NG");
                            phone.resolve("NG");
                            account.resolve("NG");
                        }
                    });
            $.when(part1, part2, email, phone, account).done(function (v1, v2, v3, v4, v5) {
                $("#save").attr("disabled", false);
                console.log("part1: " + v1 + " part2: " + v2 + " email: " + v3 + " phone: " + v4 + " account: " + v5);
                HoldOn.close();
                if (v1 === "OK" && v2 === "OK" && v3 === "OK" && v4 === "OK" && v5 === "OK") {
                    toast("Cadastro salvo com sucesso");
                }
            });

        });
    });

    function remove(type, anchor, id) {
        swal({
            title: 'Remover?',
            text: 'Essa operação não poderá ser revertida',
            showCancelButton: true,
            confirmButtonColor: '#079dff',
            cancelButtonColor: 'lightgray',
            confirmButtonText: 'SIM',
            cancelButtonText: 'NÃO',
            confirmButtonClass: 'btn btn-lg btn-primary',
            cancelButtonClass: 'btn btn-lg btn-outline-dark mr-3',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                switch (type) {
                    case "email":
                        $("#edit-email-" + anchor).remove();
                        $("#view-email-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-email-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "phone":
                        $("#edit-phone-" + anchor).remove();
                        $("#view-phone-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-phone-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                    case "account":
                        $("#edit-account-" + anchor).remove();
                        $("#view-account-" + anchor).remove();
                        if (id !== "new") {
                            $("#delete-account-list").append("<option value='" + id + "' selected></option>");
                        }
                        break;
                }
            }
        });
    }
</script>

<?php
$this->Breadcrumbs->add([
    ['title' => $this->Html->tag('i', '', ['class' => 'fa fa-home']), 'url' => ['controller' => 'Home', 'action' => 'index']],
    ['title' => 'Cadastro de Vendedores', 'url' => ['action' => 'index']],
    ['title' => 'Vendedor']
]);
echo $this->Breadcrumbs->render();
?>

<div class="card">
    <div class="card-body">
        <div id="part1"></div>
        <div class="row">
            <div class="col-md-3">
                <h5 class="mt-3">Emails</h5>
                <table id="email" class="table table-hover">
                    <?php
                    if (isset($seller->seller_emails) and sizeof($seller->seller_emails) > 0) {
                        foreach ($seller->seller_emails as $email) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-email-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-email-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('email', '<?= $anchor ?>', <?= $email->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="email" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-email-<?= $anchor ?>">
                                <td>
                                    <span id="view-email-area-<?= $anchor ?>" style="font-size: 14px">
                                        <?= $email->email ?>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="email" data-anchor="<?= $anchor ?>" data-id="<?= $email->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-email">
                            <td class="no-data-found">Nenhum Email Cadastrado</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="email-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="email"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-email']) ?>
                <?= $this->Form->control('delete-email-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
            <div class="col-md-5">
                <h5 class="mt-3">Telefones</h5>
                <table id="phone" class="table table-hover">
                    <?php
                    if (isset($seller->seller_phones) and sizeof($seller->seller_phones) > 0) {
                        foreach ($seller->seller_phones as $phone) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-phone-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-phone-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('phone', '<?= $anchor ?>', <?= $phone->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="phone" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-phone-<?= $anchor ?>">
                                <td>
                                    <span id="view-phone-area-<?= $anchor ?>" style="font-size: 14px">
                                        <span class="mr-2" id="ddd">(<?= $phone->ddd ?>)</span>
                                        <span class="mr-2" id="phone"><?= $phone->telefone ?></span>
                                        <span class="mr-2" id="type"><?= $phone_types[$phone->tipo] ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="phone" data-anchor="<?= $anchor ?>" data-id="<?= $phone->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-phone">
                            <td class="no-data-found">Nenhum Telefone Cadastrado</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="phone-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="phone"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-phone']) ?>
                <?= $this->Form->control('delete-phone-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div id="part2"></div>
        <div class="row">
            <div class="col-md-9">
                <h5 class="mt-3">Contas Bancárias</h5>
                <table id="account" class="table table-hover">
                    <?php
                    if (isset($seller->seller_bank_accounts) and sizeof($seller->seller_bank_accounts) > 0) {
                        foreach ($seller->seller_bank_accounts as $account) {
                            $anchor = rand();
                            ?>
                            <tr id="edit-account-<?= $anchor ?>" style="display: none">
                                <td>
                                    <div id="edit-account-area-<?= $anchor ?>"></div>
                                    <i id="trash-<?= $anchor ?>" class="fa fa-trash pull-right" onclick="remove('account', '<?= $anchor ?>', <?= $account->id ?>)"></i>
                                    <i class="fa fa-check-square pull-right mr-2" data-type="account" data-anchor="<?= $anchor ?>"></i>
                                </td>
                            </tr>
                            <tr id="view-account-<?= $anchor ?>">
                                <td>
                                    <span id="view-account-area-<?= $anchor ?>" style="font-size: 14px">
                                        <span class="mr-2" id="banco"><?= $account->bank->codigo ?> - <?= $account->bank->nome ?></span>
                                        Agência: <span class="mr-2" id="agencia"><?= $account->numero_agencia ?></span>
                                        Conta: <span class="mr-2" id="conta"><?= $account->numero_conta ?></span>
                                        <span class="mr-2" id="tipo-conta"><?= $account_types[$account->tipo_conta] ?></span>
                                    </span>
                                    <i class="fa fa-edit pull-right" data-type="account" data-anchor="<?= $anchor ?>" data-id="<?= $account->id ?>"></i>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                        <tr id="no-account">
                            <td class="no-data-found">Nenhuma Conta Cadastrada</td>
                        </tr>
                        <?php
                    }
                    ?>
                    <tr id="account-add">
                        <td>
                            <i class="fa fa-plus-square" data-type="account"></i>
                        </td>
                    </tr>
                </table>
                <?= $this->Form->create(null, ['id' => 'form-delete-account']) ?>
                <?= $this->Form->control('delete-account-list', ['options' => [], 'style' => 'display:none', 'label' => false, 'multiple' => true]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <?= $this->element('button_save', ['id' => 'save']) ?>
                <?= $this->element('button_delete', ['id' => $seller->id]) ?>
                <?= $this->element('button_return') ?>
            </div>
        </div>
    </div>
</div>
