<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('#tipo-pj input[type=radio]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat',
                increaseArea: '20%'
            }).on('ifClicked', function (e) {
                if ($(this)[0]['name'] === 'tipo') {
                    switch ($(this).val()) {
                        case "J":
                            $("label[for=razao-social]").html('Razão Social');
                            $("label[for=cnpj-cpf]").html('CNPJ');
                            $("#nome-fantasia").prop("disabled", false);
                            $("#cnpj-cpf").inputmask("99.999.999/9999-99");
                            break;
                        case "F":
                            $("label[for=razao-social]").html('Nome');
                            $("label[for=cnpj-cpf]").html('CPF');
                            $("#nome-fantasia").val('');
                            $("#nome-fantasia").prop("disabled", true);
                            $("#cnpj-cpf").inputmask("999.999.999-99");
                            break;
                    }
                }
            });
        }, 500);

<?php if ($seller->tipo == 'J') { ?>
            $("input[name=cnpj_cpf]").inputmask("99.999.999/9999-99");
<?php } else { ?>
            $("input[name=cnpj_cpf]").inputmask("999.999.999-99");
<?php } ?>

        $("input[name=inscricao_estadual]").inputmask("999.999.999.999");
    });
</script>

<?= $this->Flash->render() ?>
<?=
$this->Form->create($seller, [
    'id' => 'form-part1',
    'data-id' => $seller->isNew() ? 'new' : $seller->id,
    'data-error' => sizeof($seller->getErrors()) > 0 ? 'Y' : 'N'])
?>
<h5 class="mt-3">Dados Básicos</h5>
<div class="row mb-3 mt-3">
    <div id="tipo-pj" class="col-md-3 col-sm-12">
        <?= $this->Form->radio('tipo', $entity_types, ['default' => 'J']) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('razao_social', ['type' => 'text', 'label' => $seller->tipo == 'J' ? 'Razão Social' : 'Nome']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('nome_fantasia', ['type' => 'text', 'label' => 'Nome Fantasia', 'disabled' => $seller->tipo == 'J' ? false : true]) ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('cnpj_cpf', ['type' => 'text', 'label' => ($seller->tipo == 'J') ? 'CNPJ' : 'CPF']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('inscricao_estadual', ['type' => 'text', 'label' => 'Inscrição Estadual']) ?>
    </div>
    <div class="col-md-3 col-sm-12">
        <?= $this->Form->control('inscricao_municipal', ['type' => 'text', 'label' => 'Inscrição Municipal']) ?>
    </div>
</div>
<?= $this->Form->end() ?>
