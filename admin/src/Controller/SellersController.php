<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class SellersController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['razao_social'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Sellers->find('search', ['search' => $data]);
        $sellers = $this->paginate($query);
        $this->set(compact('sellers'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $seller = $this->Sellers->get($id, ['contain' => [
                    'SellerEmails',
                    'SellerPhones',
                    'SellerBankAccounts',
                    'SellerBankAccounts.Banks'
            ]]);
        } else {
            $seller = $this->Sellers->newEntity();
        }
        $this->set(compact('seller'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $seller = $this->Sellers->get($id, ['contain' => []]);
        } else {
            $seller = $this->Sellers->newEntity();
            $seller->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }
            $seller = $this->Sellers->patchEntity($seller, $data, ['validate' => 'part1']);
            if ($this->Sellers->save($seller)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('seller'));
    }

    public function part2($id) {
        if ($id != 'new') {
            $seller = $this->Sellers->get($id);
        } else {
            $seller = $this->Sellers->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $seller = $this->Sellers->patchEntity($seller, $data, ['validate' => 'part2']);
            if ($this->Sellers->save($seller)) {
                $seller = $this->Sellers->get($seller->id);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('seller'));
    }

    public function email($seller_id, $id, $anchor) {
        $table = TableRegistry::get('SellerEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->seller_id = $seller_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($seller_id, $id, $anchor) {
        $table = TableRegistry::get('SellerPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->seller_id = $seller_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    public function account($seller_id, $id, $anchor) {
        $table = TableRegistry::get('SellerBankAccounts');
        if ($id != 'new') {
            $account = $table->get($id);
        } else {
            $account = $table->newEntity();
            $account->seller_id = $seller_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $table->patchEntity($account, $data);
            if ($table->save($account)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $bank_list = TableRegistry::get('Banks')->find()->orderAsc('nome');
        $banks = [];
        foreach ($bank_list as $bank) {
            $banks[$bank->id] = $bank->codigo . ' - ' . $bank->nome;
        }
        $this->set(compact('account', 'banks', 'anchor'));
    }

    public function emailDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('SellerEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('SellerPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function accountDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-account-list'] as $id) {
            $table = TableRegistry::get('SellerBankAccounts');
            $account = $table->get($id);
            $table->delete($account);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $seller = $this->Sellers->get($id);
        if (TableRegistry::get('Clients')->find()->where(['seller_id' => $id])->count() == 0) {
            if ($this->Sellers->delete($seller)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
            return $this->redirect(['action' => 'index']);
        } else {
            $this->Flash->error('Vendedor não pode ser removido pois está sendo associado um ou mais clientes');
            return $this->redirect(['action' => 'edit', $id]);
        }
    }

}
