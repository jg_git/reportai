<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class PayablesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
                'pendentes' => '0',
            ]
        ]);
    }

    public function index()
    {
        $session = $this->request->getSession();
        if (strpos($this->request->env('HTTP_REFERER'), 'payables') === false) {
            $session->delete('cartp');
        }
        $cart = explode(',', $session->read('cartp'));
        $cartdb = TableRegistry::get('Payables')->find()->where(['id IN' => $cart]);
        $total = 0;
        foreach ($cartdb as $item) {
            $total += $item->valor;
        }
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        if (array_key_exists('pendentes', $data)) {
            $data['baixa'] = 0;
        }
        $query = $this->Payables->find('search', ['search' => $data])
            ->contain([
                'Sales',
                'Sales.Purchases',
                'Sales.Purchases.Brokers',
                'Sales.Purchases.Plants',
                'Sales.Purchases.Distributors',
                'Sales.Carriers',
                'SaleDetails',
                'SaleDetails.Sales',
                'SaleDetails.Sales.Purchases',
                'SaleDetails.Sales.Purchases.Brokers',
                'SaleDetails.Sales.Purchases.Plants',
                'SaleDetails.Presales',
                'SaleDetails.Presales.Clients',
                'SaleDetails.Presales.Clients.Sellers',
                'PurchasePayments',
                'PurchasePayments.Purchases',
                'PurchasePayments.Purchases.Plants',
                'SalesPurchases',
                'SalesPurchases.Sales',
                'SalesPurchases.Purchases',
                'SalesPurchases.Purchases.Plants',
                'SalesPurchases.Purchases.Distributors',
                'SalesPurchases.Purchases.Brokers',
                'PurchaseTransfers',
                'PurchaseTransfers.Purchases',
                'PurchaseTransfers.Purchases.Brokers',
                'PurchaseTransfers.Carriers',
            ])->orderAsc('Payables.data_vencimento');
        $payables = $this->paginate($query);
        $accounts = $this->getAccounts();
        $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('payables', 'accounts', 'payment_methods', 'cart', 'total'));
    }

    public function settle()
    {
        $data = $this->request->getData();
        $session = $this->request->getSession();
        $cart = explode(',', $session->read('cartp'));
        foreach ($cart as $payable_id) {
            if ($payable_id != '' and !is_null($payable_id)) {
                $payable = TableRegistry::get('Payables')->get($payable_id);

                $entry = TableRegistry::get('LedgerEntries')->newEntity();
                $entry->account_id = $data['account_id'];
                $entry->data_lancamento = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
                $entry->payment_method_id = $data['payment_method_id'];
                switch ($payable->tipo) {
                    case 'CC':
                        $entry->ledger_entry_type_id = 8;
                        $entry->payable_id = $payable->id;
                        break;
                    case 'CT':
                        $entry->ledger_entry_type_id = 8;
                        $entry->payable_id = $payable->id;
                        break;
                    case 'FR':
                        $entry->ledger_entry_type_id = 9;
                        $entry->payable_id = $payable->id;
                        break;
                    case 'FT':
                        $entry->ledger_entry_type_id = 9;
                        $entry->payable_id = $payable->id;
                        break;
                    case 'SP':
                        $entry->ledger_entry_type_id = 20;
                        $entry->payable_id = $payable->id;
                        break;
                    case 'CV':
                        $entry->ledger_entry_type_id = 7;
                        $entry->payable_id = $payable->id;
                        break;
                    case 'PC':
                        $entry->ledger_entry_type_id = 22;
                        $entry->payable_id = $payable->id;
                        break;
                }
                $entry->valor = $payable->valor;
                TableRegistry::get('LedgerEntries')->save($entry);

                $payable->data_baixa = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
                $payable->baixa = 1;
                $this->Payables->save($payable);
            }
        }
        $session->delete('cartp');

        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function selector($tipo, $default = null)
    {

        switch ($tipo) {
            case 'CC':
                $options = TableRegistry::get('Brokers')->find('list')->orderAsc('razao_social');
                break;
            case 'CT':
                $options = TableRegistry::get('Brokers')->find('list')->orderAsc('razao_social');
                break;
            case 'SP':
                $options = TableRegistry::get('Distributors')->find('list')->orderAsc('razao_social');
                break;
            case 'PC':
                $options = TableRegistry::get('Plants')->find('list')->orderAsc('razao_social');
                break;
            case 'FR':
                $options = TableRegistry::get('Carriers')->find('list')->orderAsc('razao_social');
                break;
            case 'FT':
                $options = TableRegistry::get('Carriers')->find('list')->orderAsc('razao_social');
                break;
            case 'CV':
                $options = TableRegistry::get('Sellers')->find('list')->orderAsc('razao_social');
                break;
        }
        $this->set(compact('tipo', 'options', 'default'));
    }

    public function brokerPurchases($broker_id, $default = null)
    {
        $conn = ConnectionManager::get('default');
        $purchases = [];
        $ps = $conn->execute(
            'SELECT DISTINCT(pu.numero_contrato) as contrato, pu.id FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'WHERE p.baixa = 0 ' .
                'AND pu.broker_id = ? ' .
                'ORDER BY pu.numero_contrato ASC', [$broker_id]
        )->fetchAll('assoc');
        foreach ($ps as $p) {
            $purchases[$p['id']] = $p['contrato'];
        }
        $this->set(compact('purchases', 'default'));
    }

    public function cart($do, $id = null)
    {
        $session = $this->request->getSession();

        switch ($do) {
            case 'A':
                $cart = explode(',', $session->read('cartp'));
                if (!in_array($id, $cart)) {
                    $cart[] = $id;
                }
                $session->write('cartp', implode(',', $cart));
                break;
            case 'R':
                $cart = explode(',', $session->read('cartp'));
                if (($index = array_search($id, $cart)) !== false) {
                    unset($cart[$index]);
                    $session->write('cartp', implode(',', $cart));
                }
                break;
            case 'C':
                $session->delete('cartp');
                break;
            case 'LA':
                $ids = explode(',', $id);
                $cart = explode(',', $session->read('cartp'));
                foreach ($ids as $id) {
                    if (!in_array($id, $cart)) {
                        $cart[] = $id;
                    }
                }
                $session->write('cartp', implode(',', $cart));
                break;
            case 'LR':
                $ids = explode(',', $id);
                $cart = explode(',', $session->read('cartp'));
                foreach ($ids as $id) {
                    if (($index = array_search($id, $cart)) !== false) {
                        unset($cart[$index]);
                    }
                }
                $session->write('cartp', implode(',', $cart));
                break;
        }

        $cart = explode(',', $session->read('cartp'));
        $cartdb = TableRegistry::get('Payables')->find()->where(['id IN' => $cart]);
        $total = 0;
        foreach ($cartdb as $item) {
            $total += $item->valor;
        }

        $json['status'] = 'OK';
        $json['itens'] = sizeof($cart) - 1;
        $json['total'] = $total;
        $json['cart'] = $session->read('cartp');
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }
}
