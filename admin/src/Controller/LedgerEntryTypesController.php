<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LedgerEntryTypesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['descricao'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->LedgerEntryTypes->find('search', ['search' => $data]);
        $types = $this->paginate($query);
        $this->set(compact('types'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $type = $this->LedgerEntryTypes->get($id, ['contain' => []]);
        } else {
            $type = $this->LedgerEntryTypes->newEntity();
            $type->editable = 1;
            $type->dc = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $type = $this->LedgerEntryTypes->patchEntity($type, $data);
            if ($this->LedgerEntryTypes->save($type)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $type->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('type'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $type = $this->LedgerEntryTypes->get($id);
        if ($this->LedgerEntryTypes->delete($type)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
