<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\Validation\Validator;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout', 'login']);
    }

    public function index()
    {
        $query = $this->Users->find('list')->where('role_id = 1');
        $admins = $this->paginate($query);
        $this->paginate = [
            'sortWhitelist' => [
                'Roles.nome',
                'Users.nome',
                'Users.email',
                'Users.ativo'
            ],
        ];
        $users = $this->paginate($this->Users->find('all', ['contain' => ['Roles']]));
        $this->set(compact('admins', 'users'));
    }

    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $password = substr(str_shuffle(MD5(microtime())), 0, 10);
            $password = 'Vistoria2021##';
            // dump($password);
            // die();
            $user->password = $password;
            if (isset($user->email)){
                $user->email = strtolower($user->email);
            }
            $user->telefone_um = $user->telefone_um;
            $user->telefone_dois = $user->telefone_dois;
            if ($this->Users->save($user)) {
                //(new Email('default'))->setTemplate('new_account')
                //    ->setEmailFormat('text')
                //    ->to($user->email)
                //    ->subject('[RBL] Nova Conta Criada')
                //    ->viewVars(['password' => $password])
                //    ->send();
                $this->Flash->success('Usuário adicionado com sucesso');
                $this->redirect(['action' => 'edit', $user->id]);
            }
        }
        parent::checkErrors($user->getErrors());
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
    }

    public function edit($id = null)
    {
        $user = $this->Users->get($id, ['contain' => ['Roles']]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->telefone_um = $user->telefone_um;
            $user->telefone_dois = $user->telefone_dois;
            if (isset($user->email)){
                $user->email = strtolower($user->email);
            }
            if ($this->Users->save($user)) {
                $this->Flash->success('Usuário gravado com sucesso');
            }
        }
        parent::checkErrors($user->getErrors());
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $this->set(compact('user', 'roles'));
    }

    public function password()
    {
        $suser = $this->request->session()->read('Auth.User');
        $user = $this->Users->get($suser['id']);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success('Senha gravada com sucesso');
            } else {
                $this->Flash->error('Erro na gravação de senha');
            }
        }
        $this->set(compact('user'));
    }

    public function authorization()
    {
        $rolesTable = TableRegistry::get('Roles');
        $newrole = $rolesTable->newEntity();
        $roles = $rolesTable->find()->contain('Modules');
        $modules = TableRegistry::get('Modules')->find()->orderAsc('nome');
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $newrole = $rolesTable->patchEntity($newrole, $this->request->getData());
            $rolesCheck = $rolesTable->find()->contain('Modules')->where('nome LIKE "'.$newrole->nome.'"');
            // $this->Flash->error($rolesCheck);
            $checagem=0;
            foreach($rolesCheck as $check){
                $checagem = 1;
            }
            if($checagem==0){
                if ($rolesTable->save($newrole)) {
                    $data = $this->request->getData();
                    $pivotTable = TableRegistry::get('RolesModules');
                    foreach ($modules as $module) {
                        if ($data['check-' . $module->id]) {
                            $pivot = $pivotTable->newEntity();
                            $pivot->role_id = $newrole->id;
                            $pivot->module_id = $module->id;
                            $pivotTable->save($pivot);
                        }
                    }
                    $this->Flash->success('Perfil adicionado com sucesso');
                    return $this->redirect(['action' => 'authorization']);
                } else {
                    $this->Flash->error('Erro na gravação do perfil');
                }
            }else{
                $this->Flash->error('Nome de perfil já cadastrado');
            }
        }
        $this->set(compact('roles', 'modules', 'newrole'));
    }

    public function authorizationSwitch($role, $module, $checked)
    {
        $pivotTable = TableRegistry::get('RolesModules');
        $pivot = $pivotTable->find()->where(['role_id =' => $role])->andWhere(['module_id =' => $module])->first();
        if ($pivot) {
            if ($checked == 'N') {
                $pivotTable->delete($pivot);
            }
        } else {
            if ($checked == 'Y') {
                $pivot = $pivotTable->newEntity();
                $pivot->role_id = $role;
                $pivot->module_id = $module;
                $pivotTable->save($pivot);
            }
        }
        $this->request->getSession()->delete('Security.role');
        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function superUser($role_id, $value)
    {
        $role = TableRegistry::get('Roles')->get($role_id);
        $role->super_user = $value == 'Y' ? 1 : 0;
        TableRegistry::get('Roles')->save($role);
        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if(strpos($id, 'i') == false){
            $user = $this->Users->get($id);
            if ($this->Users->delete($user)) {
                $this->Flash->success('Usuário removido com sucessao');
            } else {
                $this->Flash->error('Erro na remoção de usuário');
            }
        }else{
            $id = str_replace('i','', $id);
            $password = 'Vistoria2021##';
            $user = $this->Users->get($id);
            $user->password = $password;
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success('Senha redefinida com sucesso');
                $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('Erro na gravação de senha');
            }
        }
        return $this->redirect(['action' => 'index']);
    }

    public function deleteRole($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if ($this->Users->find('all', ['conditions' => ['role_id = ' => $id]])->count() == 0) {
            $pivotTable = TableRegistry::get('Roles');
            $role = $pivotTable->get($id);
            if ($pivotTable->delete($role)) {
                $this->Flash->success('Função removida com sucesso');
                return $this->redirect(['action' => 'authorization']);
            } else {
                $this->Flash->error('Erro na remoção de função');
            }
        } else {
            $this->Flash->error('Função não pode ser removida pois está sendo usada.');
        }
        return $this->redirect(['action' => 'authorization']);
    }

    public function login()
    {
        if ($this->Cookie->read('remember')) {
            $user = $this->Users->findByHash($this->Cookie->read('remember'))->first();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
        }
        if ($this->request->is('post')) {
            if (isset($this->request->getData()['remember'])) {
                $remember = true;
                unset($this->request->getData()['remember']);
            } else {
                $remember = false;
            }
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['ativo']) {
                    $this->Auth->setUser($user);
                    if ($remember) {
                        $hash = Security::hash(rand());
                        $this->Cookie->config([
                            'expires' => '+30 days',
                            'httpOnly' => true
                        ]);
                        $this->Cookie->write('remember', $hash);
                        $user = $this->Users->get($user['id']);
                        $user->hash = $hash;
                        $this->Users->save($user);
                    }
                    return $this->redirect($this->Auth->redirectUrl());
                } else {
                    $this->Flash->error('Conta Inativa');
                }
            } else {
                $this->Flash->error('Email ou senha incorretos.');
            }
        }
    }

    public function logout()
    {
        $session = $this->request->getSession();
        $session->delete('Security.menus');
        $session->delete('Security.role');
        $this->Cookie->delete('remember');
        return $this->redirect($this->Auth->logout());
    }
}
