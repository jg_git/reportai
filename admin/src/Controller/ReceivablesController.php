<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ReceivablesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
                'pendentes' => '0',
            ]
        ]);
    }

    public function index() {
        $session = $this->request->getSession();
        if (strpos($this->request->env('HTTP_REFERER'), 'receivables') === false) {
            $session->delete('cartr');
        }
        $cart = explode(',', $session->read('cartr'));
        $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        $total = 0;
        foreach ($cartdb as $item) {
            $total += $item->valor;
        }
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = [];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
            }
        }
        if (array_key_exists('pendentes', $data)) {
            $data['baixa'] = 0;
        }
        $query = $this->Receivables->find('search', ['search' => $data])->contain([
            'SaleDetails',
            'SaleDetails.Sales',
            'SaleDetails.Sales.Purchases',
            'SaleDetails.Sales.Purchases.Brokers',
            'SaleDetails.Sales.Purchases.Plants',
            'SaleDetails.Presales',
            'SaleDetails.Presales.Clients',
            'SaleDetails.Presales.Clients.Sellers'
        ])->orderAsc('Receivables.data_vencimento');
        $receivables = $this->paginate($query);
        $accounts = $this->getAccounts();
        $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('receivables', 'accounts', 'payment_methods', 'cart', 'total'));
    }

    public function settle() {
        $data = $this->request->getData();
        $session = $this->request->getSession();
        $cart = explode(',', $session->read('cartr'));
        foreach ($cart as $receivable_id) {
            if ($receivable_id != '' and ! is_null($receivable_id)) {
                $receivable = TableRegistry::get('Receivables')->get($receivable_id);

                $entry = TableRegistry::get('LedgerEntries')->newEntity();
                $entry->account_id = $data['account_id'];
                $entry->data_lancamento = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
                $entry->payment_method_id = $data['payment_method_id'];
                switch ($receivable->tipo) {
                    case 'VE':
                        $entry->ledger_entry_type_id = 21;
                        $entry->receivable_id = $receivable->id;
                        break;
                }
                $entry->valor = $receivable->valor;
                TableRegistry::get('LedgerEntries')->save($entry);

                $receivable->data_baixa = date('Y-m-d', strtotime(str_replace('/', '-', $data['data_baixa'])));
                $receivable->baixa = 1;
                $this->Receivables->save($receivable);
            }
        }
        $session->delete('cartr');
        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function cart($do, $id = null) {
        $session = $this->request->getSession();

        switch ($do) {
            case 'A':
                $cart = explode(',', $session->read('cartr'));
                if (!in_array($id, $cart)) {
                    $cart[] = $id;
                }
                $session->write('cartr', implode(',', $cart));
                break;
            case 'R':
                $cart = explode(',', $session->read('cartr'));
                if (($index = array_search($id, $cart)) !== false) {
                    unset($cart[$index]);
                    $session->write('cartr', implode(',', $cart));
                }
                break;
            case 'C':
                $session->delete('cartr');
                break;
            case 'LA':
                $ids = explode(',', $id);
                $cart = explode(',', $session->read('cartr'));
                foreach ($ids as $id) {
                    if (!in_array($id, $cart)) {
                        $cart[] = $id;
                    }
                }
                $session->write('cartr', implode(',', $cart));
                break;
            case 'LR':
                $ids = explode(',', $id);
                $cart = explode(',', $session->read('cartr'));
                foreach ($ids as $id) {
                    if (($index = array_search($id, $cart)) !== false) {
                        unset($cart[$index]);
                    }
                }
                $session->write('cartr', implode(',', $cart));
                break;
        }

        $cart = explode(',', $session->read('cartr'));
        $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        $total = 0;
        foreach ($cartdb as $item) {
            $total += $item->valor;
        }

        $json['status'] = 'OK';
        $json['itens'] = sizeof($cart) - 1;
        $json['total'] = $total;
        $json['cart'] = $session->read('cartr');
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

}
