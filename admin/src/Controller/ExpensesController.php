<?php

namespace App\Controller;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

class ExpensesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index()
    {
        $accounts = $this->getAccounts();
        $this->set(compact('accounts'));
    }
    // função que irá ser chamada na hora do clique do botão. expenses.ctb
    public function expenses()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $balance = null;
        if (array_key_exists('date_start', $data) and array_key_exists('date_end', $data)) {
            $ib = TableRegistry::get('InitialBalances')->find()->first();

            $conn = ConnectionManager::get('default');
            //      $stmt = $conn->execute('select sum(l.valor) as amount from ledger_entries l ' .
            //  'inner join accounts a on l.account_id = a.id ' .
            //          'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
            //          'where t.dc = "C" ' .
            //        'and l.ledger_entry_type_id = 19 ' . //id 19 outras despesas
            //      'and l.data_lancamento >= ? ' .
            //    'and l.data_lancamento < ?', [date('Y-m-d', strtotime(str_replace('/', '-', $ib->data_saldo))), $data['date_start']]);
            //$resp = $stmt->fetchAll('assoc');
            //$credit = is_null($resp[0]['amount']) ? 0 : $resp[0]['amount'];

            $stmt = $conn->execute('select sum(l.valor) as amount from ledger_entries l ' .
                //     'inner join accounts a on l.account_id = a.id ' .
                //  'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                'where l.ledger_entry_type_id = 19 ' .
                'and l.data_lancamento >= ? ' .
                //   'and l.data_lancamento < ?', [$data['date_start'], $data['date_end']]);
                'and l.data_lancamento < ?', [$data['date_end'], $data['date_start']]);
            $resp = $stmt->fetchAll('assoc');
            $debit = is_null($resp[0]['amount']) ? 0 : $resp[0]['amount'];
            //   $stmt = $conn->execute('select sum(l.valor) as amount from ledger_entries l ' .
            //     'inner join accounts a on l.account_id = a.id ' .
            //           'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
            //          'where t.dc = "D" ' .
            //          'and l.ledger_entry_type_id = 19 ' .
            //          'and l.data_lancamento >= ? ' .
            //            'and l.data_lancamento < ?', [date('Y-m-d', strtotime(str_replace('/', '-', $ib->data_saldo))), $data['date_start']]);
            //   $resp = $stmt->fetchAll('assoc');
            //    $debit = is_null($resp[0]['amount']) ? 0 : $resp[0]['amount'];

            $balance = ($ib->valor * ($ib->dc == 'D' ? 1 : -1)) + $debit;
            $balance = $debit;
            //obter dados
            //funcao que ordena tudo
            //  $entries = TableRegistry::get('LedgerEntries')->find()->where(['ledger_entry_type_id ' => 19])->contain([
            //      'Accounts',
            //      'Accounts.Distributors',
            //       'Accounts.Banks',
            //       'LedgerEntryTypes'
            //    ])
            //        ->orderAsc('data_lancamento')->toList();
            $dates_start = date('d/m/Y', strtotime($data['date_start']));
            $dates_end = date('d/m/Y', strtotime($data['date_end']));
          
            $entries = TableRegistry::get('LedgerEntries')->find()
                ->where([
                    'ledger_entry_type_id =' => 19,
                    'data_lancamento >=' => $data['date_start'],
                    'data_lancamento <=' => $data['date_end']
                ])->contain([
                    'Accounts',
                    'Accounts.Distributors',
                    'Accounts.Banks',
                    'LedgerEntryTypes'
                ])
                ->orderAsc('data_lancamento')->toList();





            //  $account = TableRegistry::get('Accounts')->get($data['account_id'], ['contain' => ['Distributors', 'Banks']]);
        }

        $date_start = date('d/m/Y', strtotime($data['date_start']));
        $date_end = date('d/m/Y', strtotime($data['date_end']));
        $this->set(compact('balance', 'entries', 'account', 'date_start', 'date_end'));
    }

    /* public function check($id) {
        $ib = TableRegistry::get('InitialBalances')->find()->where(['account_id' => $id])->first();
        $json['status'] = !is_null($ib) ? 'OK' : 'NG';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    } */
}
