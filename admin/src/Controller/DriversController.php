<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DriversController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['nome'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Drivers->find('search', ['search' => $data]);
        $drivers = $this->paginate($query);
        $this->set(compact('drivers'));
    }

    public function edit($id)
    {
        if ($id != 'new') {
            $driver = $this->Drivers->get($id, ['contain' => []]);
        } else {
            $driver = $this->Drivers->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $driver = $this->Drivers->patchEntity($driver, $data);
            if ($this->Drivers->save($driver)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $driver->id]);
            } else {
                //   debug($driver);
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('driver'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driver = $this->Drivers->get($id);
        if ($this->Drivers->delete($driver)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }
}
