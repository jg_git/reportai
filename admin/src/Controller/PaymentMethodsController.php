<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class PaymentMethodsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['descricao'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->PaymentMethods->find('search', ['search' => $data]);
        $methods = $this->paginate($query);
        $this->set(compact('methods'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $method = $this->PaymentMethods->get($id, ['contain' => []]);
        } else {
            $method = $this->PaymentMethods->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $method = $this->PaymentMethods->patchEntity($method, $data);
            if ($this->PaymentMethods->save($method)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $method->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('method'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $method = $this->PaymentMethods->get($id);
        if ($this->PaymentMethods->delete($method)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
