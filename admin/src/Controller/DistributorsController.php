<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class DistributorsController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['razao_social', 'nome_fantasia'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Distributors->find('search', ['search' => $data]);
        $distributors = $this->paginate($query);
        $this->set(compact('distributors'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $distributor = $this->Distributors->get($id, ['contain' => [
                    'DistributorEmails',
                    'DistributorPhones',
                    'DistributorContacts',
                    'DistributorBankAccounts',
                    'DistributorBankAccounts.Banks',
            ]]);
        } else {
            $distributor = $this->Distributors->newEntity();
        }
        $this->set(compact('distributor'));
    }

    public function part1($id) {
        if ($id != 'new') {
            $distributor = $this->Distributors->get($id, ['contain' => []]);
        } else {
            $distributor = $this->Distributors->newEntity();
            $distributor->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }
            $distributor = $this->Distributors->patchEntity($distributor, $data, ['validate' => 'part1']);
            if ($this->Distributors->save($distributor)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('distributor'));
    }

    public function part2($id) {
        if ($id != 'new') {
            $distributor = $this->Distributors->get($id, ['contain' => []]);
        } else {
            $distributor = $this->Distributors->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $distributor = $this->Distributors->patchEntity($distributor, $data, ['validate' => 'part2']);
            if ($this->Distributors->save($distributor)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('distributor'));
    }

    public function email($distributor_id, $id, $anchor) {
        $table = TableRegistry::get('DistributorEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->distributor_id = $distributor_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($distributor_id, $id, $anchor) {
        $table = TableRegistry::get('DistributorPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->distributor_id = $distributor_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    public function account($distributor_id, $id, $anchor) {
        $table = TableRegistry::get('DistributorBankAccounts');
        if ($id != 'new') {
            $account = $table->get($id);
        } else {
            $account = $table->newEntity();
            $account->distributor_id = $distributor_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $account = $table->patchEntity($account, $data);
            if ($table->save($account)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $bank_list = TableRegistry::get('Banks')->find()->orderAsc('nome');
        $banks = [];
        foreach ($bank_list as $bank) {
            $banks[$bank->id] = $bank->codigo . ' - ' . $bank->nome;
        }
        $this->set(compact('account', 'banks', 'anchor'));
    }

    public function contact($distributor_id, $id, $anchor) {
        $table = TableRegistry::get('DistributorContacts');
        if ($id != 'new') {
            $contact = $table->get($id);
        } else {
            $contact = $table->newEntity();
            $contact->distributor_id = $distributor_id;
            $contact->tipo = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $contact = $table->patchEntity($contact, $data);
            if ($table->save($contact)) {
                
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('contact', 'anchor'));
    }

    public function emailDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('DistributorEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('DistributorPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function accountDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-account-list'] as $id) {
            $table = TableRegistry::get('DistributorBankAccounts');
            $account = $table->get($id);
            $table->delete($account);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function contactDelete() {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-contact-list'] as $id) {
            $table = TableRegistry::get('DistributorContacts');
            $contact = $table->get($id);
            $table->delete($contact);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $distributor = $this->Distributors->get($id);
        if ($this->Distributors->delete($distributor)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
