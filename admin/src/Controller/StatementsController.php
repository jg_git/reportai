<?php

namespace App\Controller;

use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;

class StatementsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }
    public function exportpdf()
    {
        $invoice = $this->Countries->find();
        $this->viewBuilder()->setClassName('CakePdf.Pdf');
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename' => 'certificate_'
            ],

        ]);
        $this->set('invoice', $invoice);
    }
    public function index()
    {
        $accounts = $this->getAccounts();
        $this->set(compact('accounts'));
    }

    public function statement()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $balance = null;
        if (array_key_exists('account_id', $data) and array_key_exists('date_start', $data) and array_key_exists('date_end', $data)) {
            $ib = TableRegistry::get('InitialBalances')->find()->where(['account_id' => $data['account_id']])->first();

            $conn = ConnectionManager::get('default');
            $stmt = $conn->execute('select sum(l.valor) as amount from ledger_entries l ' .
                'inner join accounts a on l.account_id = a.id ' .
                'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                'where t.dc = "C" ' .
                'and l.account_id  = ? ' .
                'and l.data_lancamento >= ? ' .
                'and l.data_lancamento < ?', [$data['account_id'], date('Y-m-d', strtotime(str_replace('/', '-', $ib->data_saldo))), $data['date_start']]);
            $resp = $stmt->fetchAll('assoc');
            $credit = is_null($resp[0]['amount']) ? 0 : $resp[0]['amount'];

            $stmt = $conn->execute('select sum(l.valor) as amount from ledger_entries l ' .
                'inner join accounts a on l.account_id = a.id ' .
                'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                'where t.dc = "D" ' .
                'and l.account_id  = ? ' .
                'and l.data_lancamento >= ? ' .
                'and l.data_lancamento < ?', [$data['account_id'], date('Y-m-d', strtotime(str_replace('/', '-', $ib->data_saldo))), $data['date_start']]);
            $resp = $stmt->fetchAll('assoc');
            $debit = is_null($resp[0]['amount']) ? 0 : $resp[0]['amount'];

            $balance = ($ib->valor * ($ib->dc == 'C' ? 1 : -1)) + $credit - $debit;

            $entries = TableRegistry::get('LedgerEntries')->find('search', ['search' => $data])->contain([
                'Accounts',
                'Accounts.Distributors',
                'Accounts.Banks',
                'LedgerEntryTypes',
                'SourceTransfers',
                'DestinationTransfers',
                //'SourceTransfers.SourceAccounts',



            ])
                ->orderAsc('data_lancamento')->toList();
            $account = TableRegistry::get('Accounts')->get($data['account_id'], ['contain' => ['Distributors', 'Banks']]);
            // $transfers_origem = TableRegistry::get('Transfers')->get($data['conta_origem_id'], ['contain' => ['LedgerEntries']]);
            // $transfers_destino = TableRegistry::get('Transfers')->get($data['conta_destino_id'], ['contain' => ['LedgerEntries']]);

        }

        $date_start = date('d/m/Y', strtotime($data['date_start']));
        $date_end = date('d/m/Y', strtotime($data['date_end']));
        // $entrief = TableRegistry::get('Transfers')->find()->where(['conta_origem_id' => $data['account_id']])->orderAsc('data_transferencia')->toList();

        $this->set(compact('balance', 'entries', 'account', 'date_start', 'date_end'));
    }

    public function check($id)
    {
        $ib = TableRegistry::get('InitialBalances')->find()->where(['account_id' => $id])->first();
        $json['status'] = !is_null($ib) ? 'OK' : 'NG';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }
}
