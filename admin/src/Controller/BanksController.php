<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class BanksController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['nome'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Banks->find('search', ['search' => $data]);
        $banks = $this->paginate($query);
        $this->set(compact('banks'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $bank = $this->Banks->get($id);
        } else {
            $bank = $this->Banks->newEntity();
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $bank = $this->Banks->patchEntity($bank, $data);
            if ($this->Banks->save($bank)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $bank->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('bank'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $bank = $this->Banks->get($id);
        if ($this->Banks->delete($bank)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
