<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ClientsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }

    public function index()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['cnpj_cpf', 'razao_social', 'logradouro', 'cidade', 'estado', 'email'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->Clients->find('search', ['search' => $data])->contain(['Deals']);
        $clients = $this->paginate($query);
        $this->set(compact('clients'));
    }

    public function edit($id)
    {
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => [
                'ClientEmails',
                'ClientPhones',
                //'ClientLogs' => ['sort' => ['ClientLogs.modified' => 'DESC']],
                //'ClientLogs.Users'
            ]]);
        } else {
            $client = $this->Clients->newEntity();
        }
        $this->set(compact('client'));
    }

    public function view($id)
    {
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => [
                'ClientEmails',
                'ClientPhones',
                //'ClientLogs' => ['sort' => ['ClientLogs.modified' => 'DESC']],
                //'ClientLogs.Users'
            ]]);
        } else {
            $client = $this->Clients->newEntity();
        }
        $this->set(compact('client'));
    }

    public function part1($id)
    {
        if ($id != 'new') {
            $client = $this->Clients->get($id, ['contain' => []]);
        } else {
            $client = $this->Clients->newEntity();
            $client->tipo = 'J';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if (!array_key_exists('nome_fantasia', $data)) {
                $data['nome_fantasia'] = '';
            }

            $client = $this->Clients->patchEntity($client, $data, ['validate' => 'part1']);
            $client->ativo = '2';

            $client = $this->Clients->patchEntity($client, $data, ['validate' => 'part2']);
            $client = $this->Clients->patchEntity($client, $data, ['validate' => 'part1']);
            
            $client->endereco_full = $client->logradouro . " " . $client->cidade . " " . $client->estado;
            // $cpf_prealteracoes = $client->cnpj_cpf;
            // $cpf_alteracoes = str_replace([',', '.', '-', '/'], '', $cpf_prealteracoes);
            // if (strlen($cpf_alteracoes) < 13) {
            //     $client->cnpj_cpf2 = [""];
            // }else{
            //     $client->cnpj_cpf2 = $cpf_alteracoes;
            // }
            // $documento = $client->cnpj_cpf;
            // $documento = str_replace(".","",$documento);
            // $documento = str_replace("-","",$documento);

            // if($client->tipo == "J"){
            //     $documento = str_replace("/","",$$documento);
            // }

            // $client->cnpj_cpf = $documento;

            // if ($id != 'new') {
            //     $client = $this->Clients->get($id, ['contain' => []]);
            // } else {
            //     $client = $this->Clients->newEntity();
            // }

            if ($this->Clients->save($client)) {
            } else {
                $this->Flash->error('Campos obrigatórios não preenchidos');
            }
        }
        $this->set(compact('client'));
    }

    // public function part2($id)
    // {
    //     if ($id != 'new') {
    //         $client = $this->Clients->get($id, ['contain' => []]);
    //     } else {
    //         $client = $this->Clients->newEntity();
    //     }
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $data = $this->request->getData();
    //         $client = $this->Clients->patchEntity($client, $data, ['validate' => 'part2']);
    //         $client->endereco_full = $client->logradouro . " " . $client->cidade . " " . $client->estado;
    //         if ($this->Clients->save($client)) {
    //         }
    //         else {
    //             $this->Flash->error('Campos obrigatórios não preenchidos');
    //         }
    //     }
    //     $this->set(compact('client'));
    // }

    public function email($client_id, $id, $anchor)
    {
        $table = TableRegistry::get('ClientEmails');
        if ($id != 'new') {
            $email = $table->get($id);
        } else {
            $email = $table->newEntity();
            $email->client_id = $client_id;
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $email = $table->patchEntity($email, $data);
            if ($table->save($email)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('email', 'anchor'));
    }

    public function phone($client_id, $id, $anchor)
    {
        $table = TableRegistry::get('ClientPhones');
        if ($id != 'new') {
            $phone = $table->get($id);
        } else {
            $phone = $table->newEntity();
            $phone->client_id = $client_id;
            $phone->tipo = 'F';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $phone = $table->patchEntity($phone, $data);
            if ($table->save($phone)) {
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $this->set(compact('phone', 'anchor'));
    }

    // public function contact($client_id, $id, $anchor) {
    //     $table = TableRegistry::get('ClientContacts');
    //     if ($id != 'new') {
    //         $contact = $table->get($id);
    //     } else {
    //         $contact = $table->newEntity();
    //         $contact->client_id = $client_id;
    //         $contact->tipo = 'D';
    //     }
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $data = $this->request->getData();
    //         $contact = $table->patchEntity($contact, $data);
    //         if ($table->save($contact)) {
    //         } else {
    //             $this->Flash->error('Erro no salvamento do cadastro');
    //         }
    //     }
    //     $this->set(compact('contact', 'anchor'));
    // }

    // public function clog($client_id, $id, $anchor) {
    //     $user = $this->request->session()->read('Auth.User');
    //     $table = TableRegistry::get('ClientLogs');
    //     if ($id != 'new') {
    //         $log = $table->get($id, ['contain' => ['Users']]);
    //     } else {
    //         $log = $table->newEntity();
    //         $log->client_id = $client_id;
    //         $log->user_id = $user['id'];
    //     }
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $log = $table->patchEntity($log, $this->request->getData());
    //         if ($log->getOriginal('file') != $log->file) {
    //             array_map('unlink', glob(getcwd() . '/files/ClientLogs/file/' . '*' . $log->getOriginal('file')));
    //         }
    //         if ($table->save($log)) {
    //             $newid = $log->id;
    //             $log = $table->get($newid, ['contain' => ['Users']]);
    //         } else {
    //             $this->Flash->error('Erro no salvamento do cadastro');
    //         }
    //     }
    //     $this->set(compact('log', 'anchor'));
    // }

    public function emailDelete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        if (gettype($data['delete-email-list']) == 'array') {
            foreach ($data['delete-email-list'] as $id) {
                $table = TableRegistry::get('ClientEmails');
                $email = $table->get($id);
                $table->delete($email);
            }
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    public function phoneDelete()
    {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->request->getData();
        foreach ($data['delete-phone-list'] as $id) {
            $table = TableRegistry::get('ClientPhones');
            $phone = $table->get($id);
            $table->delete($phone);
        }
        $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    }

    // public function contactDelete() {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $data = $this->request->getData();
    //     foreach ($data['delete-contact-list'] as $id) {
    //         $table = TableRegistry::get('ClientContacts');
    //         $contact = $table->get($id);
    //         $table->delete($contact);
    //     }
    //     $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    // }

    // public function logDelete() {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $data = $this->request->getData();
    //     foreach ($data['delete-log-list'] as $id) {
    //         $table = TableRegistry::get('ClientLogs');
    //         $log = $table->get($id);
    //         array_map('unlink', glob(getcwd() . '/files/ClientLogs/file/' . '*' . $log->file));
    //         $table->delete($log);
    //     }
    //     $this->redirect(['controller' => 'Utility', 'action' => 'status', 'OK']);
    // }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if (strpos($id, 'i')) {
            $id = str_replace('i', '', $id);
            $this->request->allowMethod(['post', 'deactivate']);
            $clientsTable = TableRegistry::getTableLocator()->get('Clients');
            $dealsTable = TableRegistry::getTableLocator()->get('Deals');
            $client = $clientsTable->get($id);
            $query_deactivate = "api_clientID=" . $id;
            $response = file_get_contents('http://projetovistoria.com.br/webservices/APIdesativaUsers.php?' . $query_deactivate);
            foreach ($dealsTable as $deal) {
                if ($deal->client_id == $id) {
                    $deal->ativo = '0';
                    $dealsTable->save($deal);
                }
            }
            $client->ativo = '0';
            $client->ultra_ativo = '0';
            if ($clientsTable->save($client)) {
                $this->Flash->success('Usuário desativado com sucesso');
            } else {
                $this->Flash->error('Falha na desativação do usuário');
            }
        } else if (strpos($id, 'add')) {
            ini_set("allow_url_fopen", 1);
            $id = str_replace('add', '', $id);
            $clientsTable = TableRegistry::getTableLocator()->get('Clients');
            $client = $clientsTable->get($id);
            $nome = $client->razao_social;
            $nome = str_replace(' ', '%20', $nome);
            $query_add = "api_ativo=1&api_nome=" . $nome . "&api_email=" . $client->email . "&api_password=Vistoria2021%23%23&api_roleID=1&api_clientID=" . $client->id;
            $response = file_get_contents('http://projetovistoria.com.br/webservices/APIcadastrarUsers.php?' . $query_add);
            $checagem = json_decode($response, true);
            $checagemNv2 = '' . implode(',', $checagem);
            if ($checagemNv2 == '1' or $checagemNv2 == 1) {
                $this->Flash->success('Usuário ativado com sucesso');
            } else {
                $this->Flash->success('Usuário reativado com sucesso');
                $response2 = file_get_contents('http://projetovistoria.com.br/webservices/APIreativarUsers.php?api_clientID=' . $id);
            }
            $clientsTable = TableRegistry::getTableLocator()->get('Clients');
            $dealsTable = TableRegistry::getTableLocator()->get('Deals');
            $client = $clientsTable->get($id);
            foreach ($dealsTable as $deal) {
                if ($deal->client_id == $id) {
                    $deal->ativo = null;
                    $dealsTable->save($deal);
                }
            }
            $client->ativo = '1';
            $client->ultra_ativo = '1';
            $clientsTable->save($client);
        } else {
            $client = $this->Clients->get($id, ['contain' => ['ClientLogs']]);
            foreach ($client->client_logs as $log) {
                array_map('unlink', glob(getcwd() . '/files/ClientLogs/file/' . '*' . $log->file));
            }
            if ($this->Clients->delete($client)) {
                $this->Flash->success('Cadastro removido com sucesso');
                $response = file_get_contents('http://projetovistoria.com.br/webservices/APIexcluirUsers.php?api_clientID=' . $id);
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        }
        $checagem = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        return $this->redirect($this->referer());
    }

    public function deleteIndex($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        $this->Clients->delete($client);
    }
}
