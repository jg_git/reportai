<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class StockTransfersController extends AppController {

    public function edit() {
        $purchases = TableRegistry::get('Purchases')->find()
                ->where(['volume_comprado + volume_transferido - volume_devolvido - volume_transito - volume_vendido > 0'])
                ->contain(['Plants'])
                ->toList();
        $transits = TableRegistry::get('PurchaseTransfers')->find()
                ->where([
                    'PurchaseTransfers.transito = 1'
                ])
                ->contain([
                    'Purchases',
                    'Purchases.Plants',
                    'Plants',
                    'Carriers'
                ])
                ->toList();
        $plants = TableRegistry::get('Plants')->find('list');
        $carriers = TableRegistry::get('Carriers')->find('list');
        $this->set(compact('purchases', 'transits', 'plants', 'carriers'));
    }

    public function transito() {
   //
        // try {
            
            $data = $this->request->getData();
            $volume = intval(str_replace(',', '.', str_replace('.', '', $data['volume'])));
            $purchase = TableRegistry::get('Purchases')->get($data['purchase_id']);
    
            $transfer = TableRegistry::get('PurchaseTransfers')->newEntity();
            $transfer->purchase_id = $purchase->id;
            $transfer->transito = 1;
            $transfer->volume = $volume * -1;
            $transfer->frete = floatval(str_replace(',', '.', str_replace('.', '', $data['frete'])));
            $transfer->carrier_id = $data['carrier_id'];
            $transfer->plant_id = $data['plant_id'];
            TableRegistry::get('PurchaseTransfers')->save($transfer);
            $this->log('Cheguei aqui', 'debug'); 
            if ($transfer->frete > 0) {
                $payable = TableRegistry::get('Payables')->newEntity();
                $payable->tipo = 'FT';
                $payable->data_vencimento = date('Y-m-t');
                $payable->valor = $transfer->frete;
                $payable->purchase_transfer_id = $transfer->id;
                TableRegistry::get('Payables')->save($payable);
            }
    
            $transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['purchase_id' => $purchase->id, 'transito = 1']);
            $volume = 0;
            foreach ($transfers as $transfer) {
                $volume += $transfer->volume;
            }
            $purchase->volume_transito += $volume * -1;
            TableRegistry::get('Purchases')->save($purchase);
    
            $json['status'] = 'OK';
            $this->set(['json' => $json, '_serialize' => 'json']);
            $this->RequestHandler->renderAs($this, 'json');
        // } catch (\Exception $error) {
        //     $this->log($error->getMessage());
        // }     
    }

    public function transitoEstorno($id) {
        $transit = TableRegistry::get('PurchaseTransfers')->get($id);

        $purchase = TableRegistry::get('Purchases')->get($transit->purchase_id);
        $purchase->volume_transito -= abs($transit->volume);
        TableRegistry::get('Purchases')->save($purchase);

        $payable = TableRegistry::get('Payables')->find()->where(['purchase_transfer_id' => $transit->id])->first();
        TableRegistry::get('Payables')->delete($payable);

        TableRegistry::get('PurchaseTransfers')->delete($transit);
        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function transferencia() {
        $data = $this->request->getData();

        switch ($data['tipo_transferencia']) {
            case 'existente':
                $destino = TableRegistry::get('Purchases')->get($data['destination_purchase_id']);
                $estoque = $destino->volume_comprado + $destino->volume_transferido - $destino->volume_devolvido - $destino->volume_vendido;
                $somatoria = $estoque * ($destino->preco_litro + $destino->preco_litro_adicional);
                $volume_total = $estoque;

                $w = TableRegistry::get('PurchaseTransfers')->find()->orderDesc('wrapping')->first();
                $wrapping = $w->wrapping + 1;

                foreach ($data['purchase_transfers'] as $transit_id) {
                    $transfer = TableRegistry::get('PurchaseTransfers')->get($transit_id, ['contain' => ['Purchases']]);
                    $transfer->transito = 0;
                    $transfer->wrapping = $wrapping;
                    $transfer->destination_purchase_id = $destino->id;
                    TableRegistry::get('PurchaseTransfers')->save($transfer);

                    if ($transfer->purchase->corretada) {
                        $payable = TableRegistry::get('Payables')->newEntity();
                        $payable->tipo = 'CT';
                        $payable->data_vencimento = date('Y-m-t');
                        $payable->valor = round(abs($transfer->volume) * 0.003, 2);
                        $payable->purchase_transfer_id = $transfer->id;
                        TableRegistry::get('Payables')->save($payable);
                    }

                    $purchase = TableRegistry::get('Purchases')->get($transfer->purchase_id);
                    $transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['purchase_id' => $purchase->id]);
                    $vtf = 0;
                    $vtr = 0;
                    foreach ($transfers as $t) {
                        if ($t->transito) {
                            $vtr += $t->volume;
                        } else {
                            $vtf += $t->volume;
                        }
                    }
                    $purchase->volume_transito = $vtr * -1;
                    $purchase->volume_transferido = $vtf;
                    TableRegistry::get('Purchases')->save($purchase);

                    $somatoria += (abs($transfer->volume) * ($purchase->preco_litro + $purchase->preco_litro_adicional)) + $transfer->frete;
                    $volume_total += abs($transfer->volume);

                    $t = TableRegistry::get('PurchaseTransfers')->newEntity();
                    $t->purchase_id = $destino->id;
                    $t->volume = abs($transfer->volume);
                    $t->wrapping = $wrapping;
                    TableRegistry::get('PurchaseTransfers')->save($t);
                }

                $transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['purchase_id' => $destino->id, 'transito = 0']);
                $volume = 0;
                foreach ($transfers as $transfer) {
                    $volume += $transfer->volume;
                }
                $destino->volume_transferido = $volume;
                $destino->preco_litro_adicional = round($somatoria / $volume_total, 4) - $destino->preco_litro;
                TableRegistry::get('Purchases')->save($destino);

                $t = TableRegistry::get('PurchaseTransfers')->find()->where(['purchase_id' => $destino->id])->orderDesc('created')->first();
                $t->preco_litro_adicional = $destino->preco_litro_adicional;
                TableRegistry::get('PurchaseTransfers')->save($t);
                break;
            case 'novo':
                $destino = TableRegistry::get('Purchases')->newEntity();
                $destino->numero_contrato = $data['numero_contrato'];
                $destino->data_compra = date('Y-m-d');
                TableRegistry::get('Purchases')->save($destino);

                $plant_id = null;
                $distributor_id = null;
                $somatoria = 0;
                $volume = 0;

                $w = TableRegistry::get('PurchaseTransfers')->find()->orderDesc('wrapping')->first();
                $wrapping = $w->wrapping + 1;
                foreach ($data['purchase_transfers'] as $transit_id) {
                    $transfer = TableRegistry::get('PurchaseTransfers')->get($transit_id, ['contain' => ['Purchases']]);
                    $transfer->transito = 0;
                    $transfer->wrapping = $wrapping;
                    $transfer->destination_purchase_id = $destino->id;
                    // $transfer->preco_litro_adicional = $transfer->frete / ($transfer->volume * -1);
                    $transfer->preco_litro_adicional = 0;
                    TableRegistry::get('PurchaseTransfers')->save($transfer);

                    if ($transfer->purchase->corretada) {
                        $payable = TableRegistry::get('Payables')->newEntity();
                        $payable->tipo = 'CT';
                        $payable->data_vencimento = date('Y-m-t');
                        $payable->valor = round(abs($transfer->volume) * 0.003, 2);
                        $payable->purchase_transfer_id = $transfer->id;
                        TableRegistry::get('Payables')->save($payable);
                    }

                    $purchase = TableRegistry::get('Purchases')->get($transfer->purchase_id);
                    $transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['purchase_id' => $purchase->id]);
                    $vtf = 0;
                    $vtr = 0;
                    foreach ($transfers as $t) {
                        if ($t->transito) {
                            $vtr += $t->volume;
                        } else {
                            $vtf += $t->volume;
                        }
                    }
                    $purchase->volume_transito = $vtr * -1;
                    $purchase->volume_transferido = $vtf;
                    TableRegistry::get('Purchases')->save($purchase);

                    $plant_id = $transfer->plant_id;
                    $distributor_id = $purchase->distributor_id;
                    $somatoria += (abs($transfer->volume) * ($purchase->preco_litro + $purchase->preco_litro_adicional)) + $transfer->frete;
                    $volume += abs($transfer->volume);

                    $t = TableRegistry::get('PurchaseTransfers')->newEntity();
                    $t->purchase_id = $destino->id;
                    $t->volume = abs($transfer->volume);
                    $t->preco_litro_adicional = 0;
                    $t->wrapping = $wrapping;
                    TableRegistry::get('PurchaseTransfers')->save($t);
                }

                $destino->plant_id = $plant_id;
                $destino->distributor_id = $distributor_id;
                $destino->preco_litro = round($somatoria / $volume, 4);

                $transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['purchase_id' => $destino->id, 'transito = 0']);
                $volume = 0;
                foreach ($transfers as $transfer) {
                    $volume += $transfer->volume;
                }
                $destino->volume_transferido = $volume;
                TableRegistry::get('Purchases')->save($destino);
                break;
        }

        $json['status'] = 'OK';
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function historico($page = null) {
        $transfers = TableRegistry::get('PurchaseTransfers')->find()->orderAsc('wrapping')->toList();
        $recent = [];
        foreach ($transfers as $t) {
            if (is_null($t->destination_purchase_id)) {
                $recent[$t->purchase_id] = $t->wrapping;
            }
        }
        $t = TableRegistry::get('PurchaseTransfers')->find()->orderDesc('wrapping')->first();
        $last = $t->wrapping;
        if (is_null($page)) {
            $page = $last;
        }
        $transfers = TableRegistry::get('PurchaseTransfers')->find()
                ->where([
                    'transito = 0',
                    'wrapping <=' => $page,
                    'wrapping >' => $page - 10
                ])
                ->contain([
                    'Purchases',
                ])
                ->toList();
        $this->set(compact('transfers', 'page', 'last', 'recent'));
    }

    public function transferenciaEstorno($wrapping) {
        $transfers = TableRegistry::get('PurchaseTransfers')->find()->where(['wrapping' => $wrapping]);
        // verifica se o estorno pode ser feito, transferencias com contas a pagar quitadas não pode
        foreach ($transfers as $t) {
            if (!is_null($t->destination_purchase_id)) {
                $payables = TableRegistry::get('Payables')->find()->where(['purchase_transfer_id' => $t->id]);
                $found = false;
                foreach ($payables as $payable) {
                    if ($payable->baixa) {
                        $found = true;
                    }
                }
            }
        }

        if (!$found) {
            $destination_purchase_id = null;

            foreach ($transfers as $t) {
                if (is_null($t->destination_purchase_id)) {
                    $destination_purchase_id = $t->purchase_id;
                    TableRegistry::get('PurchaseTransfers')->delete($t);
                } else {
                    $payables = TableRegistry::get('Payables')->find()->where(['purchase_transfer_id' => $t->id]);
                    foreach ($payables as $payable) {
                        TableRegistry::get('Payables')->delete($payable);
                    }

                    $purchase = TableRegistry::get('Purchases')->get($t->purchase_id);
                    $purchase->volume_transferido += abs($t->volume);
                    TableRegistry::get('Purchases')->save($purchase);

                    TableRegistry::get('PurchaseTransfers')->delete($t);
                }
            }

            $transfers = TableRegistry::get('PurchaseTransfers')->find()->orderAsc('wrapping')->toList();
            $pt = null;
            for ($i = $transfers[sizeof($transfers) - 1]->id; $i > 0; $i--) {
                foreach ($transfers as $t3) {
                    if ($t3->id == $i) {
                        if ($t3->purchase_id == $destination_purchase_id and is_null($t3->destination_purchase_id) and ! is_null($t3->preco_litro_adicional)) {
                            $pt = $t3;
                            break 2;
                        }
                    }
                }
            }

            $preco_litro_adicional = !is_null($pt) ? $pt->preco_litro_adicional : 0;

            $purchase = TableRegistry::get('Purchases')->get($destination_purchase_id);
            // $purchase->preco_litro_adicional = $preco_litro_adicional;
            $purchase->preco_litro_adicional = 0;
            TableRegistry::get('Purchases')->save($purchase);

            /*
              // recalcula os precos por litro
              $transfers = TableRegistry::get('PurchaseTransfers')->find()->orderAsc('wrapping')->toList();
              $wrapping = 0;
              foreach ($transfers as $t1) {
              if ($t1->wrapping != $wrapping) {
              $wrapping = $t1->wrapping;
              $this->log("------------------------------- wrapping = $wrapping", 'debug');

              $destination_purchase_id = null;
              $reference_date = null;
              for ($n = 0; $n < sizeof($transfers); $n++) {
              $t2 = $transfers[$n];
              if ($t2->wrapping == $wrapping and ! is_null($t2->destination_purchase_id)) {
              $destination_purchase_id = $t2->destination_purchase_id;
              }
              if ($t2->wrapping == $wrapping and is_null($t2->destination_purchase_id)) {
              $reference_date = $t2->created;
              }
              }

              $destino = TableRegistry::get('Purchases')->get($destination_purchase_id);

              $transferidos = $conn->execute(
              'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_transfers ' .
              'WHERE created <= ? ' .
              'AND transito = 0 ' .
              'GROUP BY purchase_id', [$reference_date])->fetchAll('assoc');
              $devolvidos = $conn->execute(
              'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_devolutions ' .
              'WHERE created <= ? ' .
              'GROUP BY purchase_id', [$reference_date])->fetchAll('assoc');
              $vendidos_conciliado = $conn->execute(
              'SELECT sp.purchase_id, sum(sp.volume_conciliado) as volume FROM sales_purchases sp ' .
              'INNER JOIN sales s ON s.id = sp.sale_id ' .
              'WHERE s.created < ? ' .
              'AND s.conciliado = 1 ' .
              'GROUP BY sp.purchase_id', [$reference_date])->fetchAll('assoc');
              $vendidos_nao_conciliado = $conn->execute(
              'SELECT sp.purchase_id, sum(sp.volume) as volume FROM sales_purchases sp ' .
              'INNER JOIN sales s ON s.id = sp.sale_id ' .
              'WHERE s.created < ? ' .
              'AND s.conciliado = 0 ' .
              'GROUP BY sp.purchase_id', [$reference_date])->fetchAll('assoc');
              $vendido = 0;
              foreach ($vendidos_conciliado as $v) {
              if ($v['purchase_id'] == $purchase->id) {
              $vendido = $v['volume'];
              break;
              }
              }
              foreach ($vendidos_nao_conciliado as $v) {
              if ($v['purchase_id'] == $purchase->id) {
              $vendido += $v['volume'];
              break;
              }
              }
              $transferido = 0;
              foreach ($transferidos as $t) {
              if ($t['purchase_id'] == $purchase->id) {
              $transferido = $t['total_volume'];
              break;
              }
              }
              $devolvido = 0;
              foreach ($devolvidos as $d) {
              if ($d['purchase_id'] == $purchase->id) {
              $devolvido = $d['total_volume'];
              break;
              }
              }
              $estoque = $destino->volume_comprado + $transferido - $devolvido - $vendido;

              // procura ultimo preco litro adicional disponivel
              $pt = null;
              for ($i = $t1->id; $i > 0; $i--) {
              foreach ($transfers as $t3) {
              if ($t3->id == $i) {
              if ($t3->purchase_id == $t2->purchase_id and is_null($t3->destination_purchase_id) and ! is_null($t3->preco_litro_adicional)) {
              $pt = $t3;
              }
              }
              }
              }

              $somatoria = $estoque * ($destino->preco_litro + !is_null($pt) ? $pt->preco_litro_adicional : 0);
              $volume_total = $estoque;

              for ($n = 0; $n < sizeof($transfers); $n++) {
              $t2 = $transfers[$n];
              if ($t2->wrapping == $wrapping and ! is_null($t2->destination_purchase_id)) {
              $this->log("id = $t2->id", 'debug');
              $this->log("purchase_id = $t2->purchase_id", 'debug');
              $p1 = TableRegistry::get('Purchases')->get($t2->purchase_id);
              $this->log("preco litro = $p1->preco_litro", 'debug');
              // procura ultimo preco litro adicional disponivel
              $pt = null;
              for ($i = $t2->id; $i > 0; $i--) {
              foreach ($transfers as $t3) {
              if ($t3->id == $i) {
              if ($t3->purchase_id == $t2->purchase_id and is_null($t3->destination_purchase_id) and ! is_null($t3->preco_litro_adicional)) {
              $pt = $t3;
              }
              }
              }
              }
              $preco_litro_adicional = !is_null($pt) ? $pt->preco_litro_adicional : 0;
              $this->log("preco litro adicional = $preco_litro_adicional", 'debug');
              $somatoria += (abs($t2->volume) * ($p1->preco_litro + $preco_litro_adicional)) + $t2->frete;
              $volume_total += abs($t2->volume);
              $this->log("somatoria = $somatoria volume = $volume_total ", 'debug');
              }
              }
              $novo_preco = round($somatoria / $volume_total, 4);
              $preco_litro_adicional = $novo_preco - $destino->preco_litro;
              $this->log("novo_preco = $novo_preco destino preco litro = $destino->preco_litro ajuste = $preco_litro_adicional", 'debug');
              foreach ($transfers as $t2) {
              if ($t2->wrapping == $wrapping and is_null($t2->destination_purchase_id)) {
              $t2->preco_litro_adicional = $preco_litro_adicional;
              TableRegistry::get('PurchaseTransfers')->save($t2);
              break;
              }
              }
              }
              }
             */
            $json['status'] = 'OK';
        } else {
            $json['status'] = 'NG';
        }
        $this->set(['json' => $json, '_serialize' => 'json']);
        $this->RequestHandler->renderAs($this, 'json');
    }

}
