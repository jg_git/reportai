<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\TableRegistry;
use Cake\I18n\Date;

class ReportsController extends AppController
{

    public function index()
    {
        $brokers = TableRegistry::get('Brokers')->find('list')->orderAsc('razao_social');
        $sellers = TableRegistry::get('Sellers')->find('list')->orderAsc('razao_social');
        $carriers = TableRegistry::get('Carriers')->find('list')->orderAsc('razao_social');
        $distributors = TableRegistry::get('Distributors')->find('list')->orderAsc('razao_social');
        $plants = TableRegistry::get('Plants')->find('list')->orderAsc('razao_social');
        $this->set(compact('brokers', 'sellers', 'carriers', 'distributors', 'plants'));
    }

    // ============== Balancetes ==============

    public function evolucaoPatrimonial()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $conn = ConnectionManager::get('default');
        // ====== ATIVOS ======
        // Saldo de contas
        $stmt = $conn->execute(
            'select a.id, a.tipo, d.razao_social, b.nome as nome_banco, a.titular, a.numero_agencia, a.numero_conta, ' .
                'i.data_saldo, i.dc, i.valor as initial ' .
                'from initial_balances i ' .
                'inner join accounts a on i.account_id = a.id ' .
                'left join distributors d on a.distributor_id = d.id ' .
                'left join banks b on a.bank_id = b.id ' .
                'order by nome_banco, d.razao_social, a.titular asc'
        );
        $accounts = $stmt->fetchAll('assoc');

        $tipos = ['F', 'C', 'I'];

        $saldos = [];
        foreach ($tipos as $tipo) {
            foreach ($accounts as $account) {
                if ($account['tipo'] == $tipo) {
                    $saldo['tipo'] = $account['tipo'];

                    switch ($account['tipo']) {
                        case 'I':
                            $msg = $account['razao_social'] . ' - ' . $account['titular'];
                            break;
                        case 'F':
                            $msg = $account['nome_banco'] . ' - ' . $account['titular'] . ' - ' . $account['numero_agencia'] . ' - ' . $account['numero_conta'];
                            break;
                        case 'C':
                            $msg = $account['titular'];
                            break;
                    }

                    $saldo['conta'] = $msg;

                    $b = $account['dc'] == 'D' ? -1 * $account['initial'] : $account['initial'];

                    $stmt = $conn->execute(
                        'select sum(l.valor) as sum from ledger_entries l ' .
                            'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                            'where t.dc = "C" ' .
                            'and l.account_id = ? ' .
                            'and l.data_lancamento >= ?' .
                            'and l.data_lancamento < ?',
                        [$account['id'], $account['data_saldo'], $data['data_referencia_de']]
                    );
                    $resp = $stmt->fetchAll('assoc');
                    $credit = $resp[0]['sum'];

                    $stmt = $conn->execute(
                        'select sum(l.valor) as sum from ledger_entries l ' .
                            'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                            'where t.dc = "D" ' .
                            'and l.account_id = ? ' .
                            'and l.data_lancamento >= ?' .
                            'and l.data_lancamento < ?',
                        [$account['id'], $account['data_saldo'], $data['data_referencia_de']]
                    );
                    $resp = $stmt->fetchAll('assoc');
                    $debit = $resp[0]['sum'];

                    $saldo['saldo_de'] = round($b + $credit - $debit, 2);

                    $stmt = $conn->execute(
                        'select sum(l.valor) as sum from ledger_entries l ' .
                            'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                            'where t.dc = "C" ' .
                            'and l.account_id = ? ' .
                            'and l.data_lancamento >= ?' .
                            'and l.data_lancamento < ?',
                        [$account['id'], $account['data_saldo'], $data['data_referencia_ate']]
                    );
                    $resp = $stmt->fetchAll('assoc');
                    $credit = $resp[0]['sum'];

                    $stmt = $conn->execute(
                        'select sum(l.valor) as sum from ledger_entries l ' .
                            'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                            'where t.dc = "D" ' .
                            'and l.account_id = ? ' .
                            'and l.data_lancamento >= ?' .
                            'and l.data_lancamento < ?',
                        [$account['id'], $account['data_saldo'], $data['data_referencia_ate']]
                    );
                    $resp = $stmt->fetchAll('assoc');
                    $debit = $resp[0]['sum'];

                    $saldo['saldo_ate'] = round($b + $credit - $debit, 2);

                    $saldos[] = $saldo;
                }
            }
        }
        // Contas a receber
        $contas_receber = [];
        $resp = $conn->execute(
            'SELECT a.id, a.tipo, b.nome as banco, a.numero_agencia, a.numero_conta, a.titular, d.razao_social, sum(valor) AS total ' .
                'FROM receivables r ' .
                'INNER JOIN sale_details s ON s.id = r.sale_detail_id ' .
                'INNER JOIN sales sa ON s.sale_id = sa.id ' .
                'INNER JOIN accounts a ON a.id = s.account_id ' .
                'LEFT JOIN banks b ON b.id = a.bank_id ' .
                'LEFT JOIN distributors d ON d.id = a.distributor_id ' .
                'WHERE r.tipo = "VE" ' .
                'AND baixa = 0 ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.account_id ' .
                'UNION ALL ' .
                'SELECT a.id, a.tipo, b.nome as banco,a.numero_agencia, a.numero_conta, a.titular, d.razao_social, sum(valor) AS total ' .
                'FROM receivables r ' .
                'INNER JOIN sale_details s ON s.id = r.sale_detail_id ' .
                'INNER JOIN sales sa ON s.sale_id = sa.id ' .
                'INNER JOIN accounts a ON a.id = s.account_id ' .
                'LEFT JOIN banks b ON b.id = a.bank_id ' .
                'LEFT JOIN distributors d ON d.id = a.distributor_id ' .
                'WHERE r.tipo = "VE" ' .
                'AND baixa = 1 ' .
                'AND data_baixa >= ? ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.account_id',
            [
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de']
            ]
        )->fetchAll('assoc');
        foreach ($resp as $entry) {
            if (array_key_exists($entry['id'], $contas_receber)) {
                $contas_receber[$entry['id']]['saldo_de'] += $entry['total'];
            } else {
                if ($entry['tipo'] == 'F') {
                    $account_name = $entry['banco'] . ' - ' . $entry['numero_agencia'] . ' - ' . $entry['numero_conta'];
                } elseif ($entry['tipo'] == 'I') {
                    $account_name = $entry['razao_social'];
                } else {
                    $account_name = $entry['titular'];
                }
                $contas_receber[$entry['id']] = [
                    'conta' => $account_name,
                    'saldo_de' => $entry['total'],
                    'saldo_ate' => 0
                ];
            }
        }

        $resp = $conn->execute(
            'SELECT a.id, a.tipo, b.nome as banco, a.titular, a.numero_agencia, a.numero_conta, d.razao_social, sum(valor) AS total ' .
                'FROM receivables r ' .
                'INNER JOIN sale_details s ON s.id = r.sale_detail_id ' .
                'INNER JOIN sales sa ON s.sale_id = sa.id ' .
                'INNER JOIN accounts a ON a.id = s.account_id ' .
                'LEFT JOIN banks b ON b.id = a.bank_id ' .
                'LEFT JOIN distributors d ON d.id = a.distributor_id ' .
                'WHERE r.tipo = "VE" ' .
                'AND baixa = 0 ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.account_id ' .
                'UNION ALL ' .
                'SELECT a.id, a.tipo, b.nome as banco,  a.titular,a.numero_agencia, a.numero_conta, d.razao_social, sum(valor) AS total ' .
                'FROM receivables r ' .
                'INNER JOIN sale_details s ON s.id = r.sale_detail_id ' .
                'INNER JOIN sales sa ON s.sale_id = sa.id ' .
                'INNER JOIN accounts a ON a.id = s.account_id ' .
                'LEFT JOIN banks b ON b.id = a.bank_id ' .
                'LEFT JOIN distributors d ON d.id = a.distributor_id ' .
                'WHERE r.tipo = "VE" ' .
                'AND baixa = 1 ' .
                'AND data_baixa >= ? ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.account_id',
            [
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate']
            ]
        )->fetchAll('assoc');
        foreach ($resp as $entry) {
            if (array_key_exists($entry['id'], $contas_receber)) {
                $contas_receber[$entry['id']]['saldo_ate'] += $entry['total'];
            } else {
                $contas_receber[$entry['id']] = [
                    // $entry['tipo'] == 'F' ? 'conta' == $entry['banco'] . ' - ' . $entry['numero_agencia'] . ' - ' . $entry['numero_conta'] : $entry['razao_social'],
                    'conta' => $entry['tipo'] == 'F' ? $entry['banco'] . ' - ' . $entry['numero_agencia'] . ' - ' . $entry['numero_conta'] : ($entry['tipo'] == 'C' ? $entry['titular'] : $entry['razao_social']),
                    'saldo_de' => 0,
                    'saldo_ate' => $entry['total']
                ];
            }
        }
        // Estoques ativo
        $purchases = TableRegistry::get('Purchases')->find()
            ->where(['data_compra <' => $data['data_referencia_de']])
            ->contain(['Distributors', 'Plants']);
        $transferidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_transfers ' .
                'WHERE created < ? ' .
                'AND transito = 0 ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_de']]
        )->fetchAll('assoc');
        $devolvidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_devolutions ' .
                'WHERE created < ? ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_de']]
        )->fetchAll('assoc');
        $vendidos_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume_conciliado) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda < ? ' .
                'AND s.conciliado = 1 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_de']]
        )->fetchAll('assoc');
        $vendidos_nao_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda < ? ' .
                'AND s.conciliado = 0 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_de']]
        )->fetchAll('assoc');
        $ativo_estoques = [];
        foreach ($purchases as $purchase) {
            $vendido = 0;
            foreach ($vendidos_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido = $v['volume'];
                    break;
                }
            }
            foreach ($vendidos_nao_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido += $v['volume'];
                    break;
                }
            }
            $transferido = 0;
            foreach ($transferidos as $t) {
                if ($t['purchase_id'] == $purchase->id) {
                    $transferido = $t['total_volume'];
                    break;
                }
            }
            $devolvido = 0;
            foreach ($devolvidos as $d) {
                if ($d['purchase_id'] == $purchase->id) {
                    $devolvido = $d['total_volume'];
                    break;
                }
            }
            $estoque = $purchase->volume_comprado + $transferido - $devolvido - $vendido;
            if ($estoque > 0) {
                $adjustment = TableRegistry::get('PurchaseTransfers')->find()
                    ->where([
                        'purchase_id' => $purchase->id,
                        'created <' => $data['data_referencia_de']
                    ])->orderDesc('created')->first();
                $e['distributor_id'] = $purchase->distributor_id;
                $e['distribuidora'] = $purchase->distributor->razao_social;
                $e['usina_id'] = $purchase->plant_id;
                $e['usina'] = $purchase->plant->razao_social;
                $e['numero_contrato'] = $purchase->numero_contrato;
                //                $e['preco_litro'] = $purchase->preco_litro + $purchase->preco_litro_adicional;
                $e['preco_litro_de'] = $purchase->preco_litro + (!is_null($adjustment) ? $adjustment->preco_litro_adicional : 0);
                $e['preco_litro_ate'] = 0;
                $e['estoque_de'] = $estoque;
                $e['estoque_ate'] = 0;
                $ativo_estoques[$purchase->id] = $e;
            }
        }

        $purchases = TableRegistry::get('Purchases')->find()
            ->where(['data_compra <' => $data['data_referencia_ate']])
            ->contain(['Distributors', 'Plants']);
        $transferidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_transfers ' .
                'WHERE created < ? ' .
                'AND transito = 0 ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_ate']]
        )->fetchAll('assoc');
        $devolvidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_devolutions ' .
                'WHERE created < ? ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_ate']]
        )->fetchAll('assoc');
        $vendidos_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume_conciliado) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda < ? ' .
                'AND s.conciliado = 1 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_ate']]
        )->fetchAll('assoc');
        $vendidos_nao_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda < ? ' .
                'AND s.conciliado = 0 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_ate']]
        )->fetchAll('assoc');
        foreach ($purchases as $purchase) {
            $vendido = 0;
            foreach ($vendidos_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido = $v['volume'];
                    break;
                }
            }
            foreach ($vendidos_nao_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido += $v['volume'];
                    break;
                }
            }
            $transferido = 0;
            foreach ($transferidos as $t) {
                if ($t['purchase_id'] == $purchase->id) {
                    $transferido = $t['total_volume'];
                    break;
                }
            }
            $devolvido = 0;
            foreach ($devolvidos as $d) {
                if ($d['purchase_id'] == $purchase->id) {
                    $devolvido = $d['total_volume'];
                    break;
                }
            }
            $estoque = $purchase->volume_comprado + $transferido - $devolvido - $vendido;
            if ($estoque > 0) {
                $adjustment = TableRegistry::get('PurchaseTransfers')->find()
                    ->where([
                        'purchase_id' => $purchase->id,
                        'created <' => $data['data_referencia_ate']
                    ])->orderDesc('created')->first();
                $found = false;
                foreach ($ativo_estoques as $purchase_id => $e) {
                    if ($purchase_id == $purchase->id) {
                        $e['preco_litro_ate'] = $purchase->preco_litro + (!is_null($adjustment) ? $adjustment->preco_litro_adicional : 0);
                        $e['estoque_ate'] = $estoque;
                        $ativo_estoques[$purchase_id] = $e;
                        $found = true;
                    }
                }
                if (!$found) {
                    $e['distributor_id'] = $purchase->distributor_id;
                    $e['distribuidora'] = $purchase->distributor->razao_social;
                    $e['usina_id'] = $purchase->plant_id;
                    $e['usina'] = $purchase->plant->razao_social;
                    $e['numero_contrato'] = $purchase->numero_contrato;
                    //                    $e['preco_litro'] = $purchase->preco_litro + $purchase->preco_litro_adicional;
                    $e['preco_litro_ate'] = $purchase->preco_litro + (!is_null($adjustment) ? $adjustment->preco_litro_adicional : 0);
                    $e['estoque_de'] = 0;
                    $e['estoque_ate'] = $estoque;
                    $ativo_estoques[$purchase->id] = $e;
                }
            }
        }
        // ====== PASSIVO ======
        // Estoques passivo
        $stmt = $conn->execute(
            'SELECT p.id, d.id AS distributor_id, d.razao_social AS distribuidor, pl.razao_social AS usina, ' .
                'pu.id as purchase_id, pu.numero_contrato, SUM(p.valor) as valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_payments pp ON pp.id = p.purchase_payment_id ' .
                'INNER JOIN purchases pu ON pu.id = pp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'INNER JOIN plants pl ON pl.id = pu.plant_id ' .
                'WHERE p.tipo = "PC" ' .
                'AND p.baixa = 0 ' .
                'AND pu.data_compra < ? ' .
                'GROUP BY pu.id ' .
                'UNION ' .
                'SELECT p.id, d.id AS distributor_id, d.razao_social AS distribuidor, pl.razao_social AS usina, ' .
                'pu.id as purchase_id, pu.numero_contrato, SUM(p.valor) as valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_payments pp ON pp.id = p.purchase_payment_id ' .
                'INNER JOIN purchases pu ON pu.id = pp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'INNER JOIN plants pl ON pl.id = pu.plant_id ' .
                'WHERE p.tipo = "PC" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND pu.data_compra < ? ' .
                'GROUP BY pu.id',
            [$data['data_referencia_de'], $data['data_referencia_de'], $data['data_referencia_de']]
        );
        $estoque_de = $stmt->fetchAll('assoc');
        $passivo_estoques = [];
        foreach ($estoque_de as $e) {
            $found = false;
            foreach ($passivo_estoques as $purchase_id => $pi) {
                if ($e['purchase_id'] == $purchase_id) {
                    $pi['valor_de'] += $e['valor'];
                    $passivo_estoques[$purchase_id] = $pi;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $inventory = [];
                $inventory['distributor_id'] = $e['distributor_id'];
                $inventory['distributor'] = $e['distribuidor'];
                $inventory['usina'] = $e['usina'];
                $inventory['numero_contrato'] = $e['numero_contrato'];
                $inventory['valor_de'] = $e['valor'];
                $inventory['valor_ate'] = 0;
                $passivo_estoques[$e['purchase_id']] = $inventory;
            }
        }
        $stmt = $conn->execute(
            'SELECT p.id, d.id AS distributor_id, d.razao_social AS distribuidor, pl.razao_social AS usina, ' .
                'pu.id as purchase_id, pu.numero_contrato, SUM(p.valor) as valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_payments pp ON pp.id = p.purchase_payment_id ' .
                'INNER JOIN purchases pu ON pu.id = pp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'INNER JOIN plants pl ON pl.id = pu.plant_id ' .
                'WHERE p.tipo = "PC" ' .
                'AND p.baixa = 0 ' .
                'AND pu.data_compra < ? ' .
                'GROUP BY pu.id ' .
                'UNION ' .
                'SELECT p.id, d.id AS distributor_id, d.razao_social AS distribuidor, pl.razao_social AS usina, ' .
                'pu.id as purchase_id, pu.numero_contrato, SUM(p.valor) as valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_payments pp ON pp.id = p.purchase_payment_id ' .
                'INNER JOIN purchases pu ON pu.id = pp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'INNER JOIN plants pl ON pl.id = pu.plant_id ' .
                'WHERE p.tipo = "PC" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND pu.data_compra < ? ' .
                'GROUP BY pu.id',
            [$data['data_referencia_ate'], $data['data_referencia_ate'], $data['data_referencia_ate']]
        );
        $estoque_ate = $stmt->fetchAll('assoc');
        foreach ($estoque_ate as $e) {
            $found = false;
            foreach ($passivo_estoques as $purchase_id => $pi) {
                if ($e['purchase_id'] == $purchase_id) {
                    $pi['valor_ate'] += $e['valor'];
                    $passivo_estoques[$purchase_id] = $pi;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $inventory = [];
                $inventory['distributor_id'] = $e['distributor_id'];
                $inventory['distributor'] = $e['distribuidor'];
                $inventory['usina'] = $e['usina'];
                $inventory['numero_contrato'] = $e['numero_contrato'];
                $inventory['valor_de'] = 0;
                $inventory['valor_ate'] = $e['valor'];
                $passivo_estoques[$e['purchase_id']] = $inventory;
            }
        }
        // Comissões corretoras
        $comissoes_de = $conn->execute(
            'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CC" ' .
                'AND p.baixa = 0 ' .
                'AND s.data_venda < ? ' .
                'GROUP BY pu.broker_id ' .
                'UNION ' .
                'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CC" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND s.data_venda < ? ' .
                'GROUP BY pu.broker_id ' .
                'UNION ' .
                'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN purchases pu ON pu.id = pt.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CT" ' .
                'AND p.baixa = 0 ' .
                'AND pt.created < ? ' .
                'GROUP BY pu.broker_id ' .
                'UNION ' .
                'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN purchases pu ON pu.id = pt.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CT" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND pt.created < ? ' .
                'GROUP BY pu.broker_id ',
            [
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
            ]
        )->fetchAll('assoc');
        $comissoes_ate = $conn->execute(
            'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CC" ' .
                'AND p.baixa = 0 ' .
                'AND s.data_venda < ? ' .
                'GROUP BY pu.broker_id ' .
                'UNION ' .
                'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CC" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND s.data_venda < ? ' .
                'GROUP BY pu.broker_id ' .
                'UNION ' .
                'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN purchases pu ON pu.id = pt.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CT" ' .
                'AND p.baixa = 0 ' .
                'AND pt.created < ? ' .
                'GROUP BY pu.broker_id ' .
                'UNION ' .
                'SELECT b.id as broker_id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN purchases pu ON pu.id = pt.purchase_id ' .
                'INNER JOIN brokers b ON b.id = pu.broker_id ' .
                'WHERE p.tipo = "CT" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND pt.created < ? ' .
                'GROUP BY pu.broker_id ',
            [
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
            ]
        )->fetchAll('assoc');
        $comissoes_corretoras = [];
        foreach ($comissoes_de as $c) {
            $found = false;
            foreach ($comissoes_corretoras as $broker_id => $cc) {
                if ($c['broker_id'] == $broker_id) {
                    $cc['valor_de'] += round($c['valor'], 2);
                    $comissoes_corretoras[$broker_id] = $cc;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $comissao = [];
                $comissao['corretora'] = $c['razao_social'];
                $comissao['valor_de'] = round($c['valor'], 2);
                $comissao['valor_ate'] = 0;
                $comissoes_corretoras[$c['broker_id']] = $comissao;
            }
        }
        foreach ($comissoes_ate as $c) {
            $found = false;
            foreach ($comissoes_corretoras as $broker_id => $cc) {
                if ($c['broker_id'] == $broker_id) {
                    $cc['valor_ate'] += round($c['valor'], 2);
                    $comissoes_corretoras[$broker_id] = $cc;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $comissao = [];
                $comissao['corretora'] = $c['razao_social'];
                $comissao['valor_de'] = 0;
                $comissao['valor_ate'] = round($c['valor'], 2);
                $comissoes_corretoras[$c['broker_id']] = $comissao;
            }
        }
        // Comissões vendedores
        //        $comissoes_de = $conn->execute(
        //                        'SELECT s.id AS seller_id, s.razao_social, sum(p.valor) AS valor ' .
        //                        'FROM payables p ' .
        //                        'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
        //                        'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
        //                        'INNER JOIN presales ps ON ps.id = sd.presale_id ' .
        //                        'INNER JOIN clients c ON c.id = ps.client_id ' .
        //                        'INNER JOIN sellers s ON s.id = c.seller_id ' .
        //                        'WHERE p.tipo = "CV" ' .
        //                        'AND (p.baixa = 0 OR (p.baixa = 1 AND p.data_baixa < ?)) ' .
        //                        'AND sa.data_venda < ? ' .
        //                        'GROUP BY s.id'
        //                        , [
        //                    $data['data_referencia_de'] . ' 23:59:59',
        //                    $data['data_referencia_de']
        //                ])->fetchAll('assoc');
        //        $comissoes_ate = $conn->execute(
        //                        'SELECT s.id AS seller_id, s.razao_social, sum(p.valor) AS valor ' .
        //                        'FROM payables p ' .
        //                        'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
        //                        'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
        //                        'INNER JOIN presales ps ON ps.id = sd.presale_id ' .
        //                        'INNER JOIN clients c ON c.id = ps.client_id ' .
        //                        'INNER JOIN sellers s ON s.id = c.seller_id ' .
        //                        'WHERE p.tipo = "CV" ' .
        //                        'AND (p.baixa = 0 OR (p.baixa = 1 AND p.data_baixa < ?)) ' .
        //                        'AND sa.data_venda < ? ' .
        //                        'GROUP BY s.id'
        //                        , [
        //                    $data['data_referencia_ate'] . ' 23:59:59',
        //                    $data['data_referencia_ate']
        //                ])->fetchAll('assoc');
        $comissoes_de = $conn->execute(
            'SELECT s.id AS seller_id, s.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
                'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
                'INNER JOIN presales ps ON ps.id = sd.presale_id ' .
                'INNER JOIN clients c ON c.id = ps.client_id ' .
                'INNER JOIN sellers s ON s.id = c.seller_id ' .
                'WHERE p.tipo = "CV" ' .
                'AND p.baixa = 0 ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.id ' .
                'UNION ' .
                'SELECT s.id AS seller_id, s.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
                'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
                'INNER JOIN presales ps ON ps.id = sd.presale_id ' .
                'INNER JOIN clients c ON c.id = ps.client_id ' .
                'INNER JOIN sellers s ON s.id = c.seller_id ' .
                'WHERE p.tipo = "CV" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.id ',
            [
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
            ]
        )->fetchAll('assoc');
        $comissoes_ate = $conn->execute(
            'SELECT s.id AS seller_id, s.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
                'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
                'INNER JOIN presales ps ON ps.id = sd.presale_id ' .
                'INNER JOIN clients c ON c.id = ps.client_id ' .
                'INNER JOIN sellers s ON s.id = c.seller_id ' .
                'WHERE p.tipo = "CV" ' .
                'AND p.baixa = 0 ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.id ' .
                'UNION ' .
                'SELECT s.id AS seller_id, s.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
                'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
                'INNER JOIN presales ps ON ps.id = sd.presale_id ' .
                'INNER JOIN clients c ON c.id = ps.client_id ' .
                'INNER JOIN sellers s ON s.id = c.seller_id ' .
                'WHERE p.tipo = "CV" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND sa.data_venda < ? ' .
                'GROUP BY s.id ',
            [
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
            ]
        )->fetchAll('assoc');
        $comissoes_vendedores = [];
        foreach ($comissoes_de as $c) {
            $comissao = [];
            $comissao['vendedor'] = $c['razao_social'];
            $comissao['valor_de'] = $c['valor'];
            $comissao['valor_ate'] = 0;
            $comissoes_vendedores[$c['seller_id']] = $comissao;
        }
        foreach ($comissoes_ate as $c) {
            $found = false;
            foreach ($comissoes_vendedores as $seller_id => $cc) {
                if ($c['seller_id'] == $seller_id) {
                    $cc['valor_ate'] = $c['valor'];
                    $comissoes_vendedores[$seller_id] = $cc;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $comissao = [];
                $comissao['vendedor'] = $c['razao_social'];
                $comissao['valor_de'] = 0;
                $comissao['valor_ate'] = $c['valor'];
                $comissoes_vendedores[$c['seller_id']] = $comissao;
            }
        }
        // Spread
        $stmt = $conn->execute(
            'SELECT d.id AS distributor_id, d.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'WHERE p.tipo = "SP" ' .
                'AND p.baixa = 0 ' .
                'AND s.data_venda < ? ' .
                'GROUP BY d.id ' .
                'UNION ' .
                'SELECT d.id AS distributor_id, d.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'WHERE p.tipo = "SP" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND s.data_venda < ? ' .
                'GROUP BY d.id',
            [$data['data_referencia_de'], $data['data_referencia_de'], $data['data_referencia_de']]
        );
        $spreads_de = $stmt->fetchAll('assoc');
        $stmt = $conn->execute(
            'SELECT d.id AS distributor_id, d.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'WHERE p.tipo = "SP" ' .
                'AND p.baixa = 0 ' .
                'AND s.data_venda < ? ' .
                'GROUP BY d.id ' .
                'UNION ' .
                'SELECT d.id AS distributor_id, d.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN distributors d ON d.id = pu.distributor_id ' .
                'WHERE p.tipo = "SP" ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'AND s.data_venda < ? ' .
                'GROUP BY d.id',
            [$data['data_referencia_ate'], $data['data_referencia_ate'], $data['data_referencia_ate']]
        );
        $spreads_ate = $stmt->fetchAll('assoc');
        $spreads = [];
        foreach ($spreads_de as $c) {
            $spread = [];
            $spread['distribuidora'] = $c['razao_social'];
            $spread['valor_de'] = $c['valor'];
            $spread['valor_ate'] = 0;
            $spreads[$c['distributor_id']] = $spread;
        }
        foreach ($spreads_ate as $c) {
            $found = false;
            foreach ($spreads as $distributor_id => $cc) {
                if ($c['distributor_id'] == $distributor_id) {
                    $cc['valor_ate'] += $c['valor']; //soma de spread(condicao diferente)
                    $spreads[$distributor_id] = $cc;
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $spread = [];
                $spread['distribuidora'] = $c['razao_social'];
                $spread['valor_de'] = 0;
                $spread['valor_ate'] = $c['valor'];
                $spreads[$c['distributor_id']] = $spread;
            }
        }
        // Fretes
        $stmt = $conn->execute(
            'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales s ON s.id = p.sale_id ' .
                'INNER JOIN carriers c ON c.id = s.carrier_id ' .
                'WHERE p.tipo = "FR" ' .
                'AND s.data_venda < ? ' .
                'AND p.baixa = 0 ' .
                'GROUP BY c.id ' .
                'UNION ' .
                'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales s ON s.id = p.sale_id ' .
                'INNER JOIN carriers c ON c.id = s.carrier_id ' .
                'WHERE p.tipo = "FR" ' .
                'AND s.data_venda < ? ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'GROUP BY c.id ' .
                'UNION ' .
                'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN carriers c ON c.id = pt.carrier_id ' .
                'WHERE p.tipo = "FT" ' .
                'AND pt.created < ? ' .
                'AND p.baixa = 0 ' .
                'GROUP BY c.id ' .
                'UNION ' .
                'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN carriers c ON c.id = pt.carrier_id ' .
                'WHERE p.tipo = "FT" ' .
                'AND pt.created < ? ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'GROUP BY c.id ',
            [
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de'],
                $data['data_referencia_de']
            ]
        );
        $fretes_de = $stmt->fetchAll('assoc');
        $stmt = $conn->execute(
            'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales s ON s.id = p.sale_id ' .
                'INNER JOIN carriers c ON c.id = s.carrier_id ' .
                'WHERE p.tipo = "FR" ' .
                'AND s.data_venda < ? ' .
                'AND p.baixa = 0 ' .
                'GROUP BY c.id ' .
                'UNION ' .
                'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales s ON s.id = p.sale_id ' .
                'INNER JOIN carriers c ON c.id = s.carrier_id ' .
                'WHERE p.tipo = "FR" ' .
                'AND s.data_venda < ? ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'GROUP BY c.id ' .
                'UNION ' .
                'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN carriers c ON c.id = pt.carrier_id ' .
                'WHERE p.tipo = "FT" ' .
                'AND pt.created < ? ' .
                'AND p.baixa = 0 ' .
                'GROUP BY c.id ' .
                'UNION ' .
                'SELECT c.id AS carrier_id, c.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN carriers c ON c.id = pt.carrier_id ' .
                'WHERE p.tipo = "FT" ' .
                'AND pt.created < ? ' .
                'AND p.baixa = 1 ' .
                'AND p.data_baixa >= ? ' .
                'GROUP BY c.id ',
            [
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate'],
                $data['data_referencia_ate']
            ]
        );
        $fretes_ate = $stmt->fetchAll('assoc');
        $fretes = [];
        foreach ($fretes_de as $c) {
            if ($c['valor'] > 0) {
                if (array_key_exists($c['carrier_id'], $fretes)) {
                    $frete = $fretes[$c['carrier_id']];
                    $frete['valor_de'] += $c['valor'];
                    $fretes[$c['carrier_id']] = $frete;
                } else {
                    $frete = [];
                    $frete['transportadora'] = $c['razao_social'];
                    $frete['valor_de'] = $c['valor'];
                    $frete['valor_ate'] = 0;
                    $fretes[$c['carrier_id']] = $frete;
                }
            }
        }
        foreach ($fretes_ate as $c) {
            if ($c['valor'] > 0) {
                if (array_key_exists($c['carrier_id'], $fretes)) {
                    $frete = $fretes[$c['carrier_id']];
                    $frete['valor_ate'] += $c['valor'];
                    $fretes[$c['carrier_id']] = $frete;
                } else {
                    $frete = [];
                    $frete['transportadora'] = $c['razao_social'];
                    $frete['valor_de'] = 0;
                    $frete['valor_ate'] = $c['valor'];
                    $fretes[$c['carrier_id']] = $frete;
                }
            }
        }
        $this->set(compact('data', 'saldos', 'contas_receber', 'ativo_estoques', 'passivo_estoques', 'comissoes_corretoras', 'comissoes_vendedores', 'spreads', 'fretes'));
    }

    public function demonstrativo()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $conn = ConnectionManager::get('default');
        // Estoques
        // ===== Estoque inicial =====
        $estoque_inicial = 0;
        $purchases = TableRegistry::get('Purchases')->find()
            ->where(['data_compra <' => $data['data_referencia_de']])
            ->contain(['Distributors', 'Plants']);
        $transferidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_transfers ' .
                'WHERE created < ? ' .
                'AND transito = 0 ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_de'] . ' 00:00:00']
        )->fetchAll('assoc');
        $devolvidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_devolutions ' .
                'WHERE created < ? ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_de'] . ' 00:00:00']
        )->fetchAll('assoc');
        $vendidos_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume_conciliado) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda < ? ' .
                'AND s.conciliado = 1 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_de']]
        )->fetchAll('assoc');
        $vendidos_nao_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda < ? ' .
                'AND s.conciliado = 0 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_de']]
        )->fetchAll('assoc');
        $ativo_estoques = [];
        foreach ($purchases as $purchase) {
            $vendido = 0;
            foreach ($vendidos_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido = $v['volume'];
                    break;
                }
            }
            foreach ($vendidos_nao_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido += $v['volume'];
                    break;
                }
            }
            $transferido = 0;
            foreach ($transferidos as $t) {
                if ($t['purchase_id'] == $purchase->id) {
                    $transferido = $t['total_volume'];
                    break;
                }
            }
            $devolvido = 0;
            foreach ($devolvidos as $d) {
                if ($d['purchase_id'] == $purchase->id) {
                    $devolvido = $d['total_volume'];
                    break;
                }
            }
            $estoque = $purchase->volume_comprado + $transferido - $devolvido - $vendido;


            if ($estoque > 0) {


                $adjustment = TableRegistry::get('PurchaseTransfers')->find()
                    ->where([
                        'purchase_id' => $purchase->id,
                        'created <=' => $data['data_referencia_de'] . ' 23:59:59'
                    ])->orderDesc('created')->first();


                $estoque_inicial += $estoque * ($purchase->preco_litro + (!is_null($adjustment) ? $adjustment->preco_litro_adicional : 0));
            }
        }
        //        $pagamentos_pendentes = $conn->execute(
        //                        'SELECT sum(pa.valor) as total FROM payables pa ' .
        //                        'JOIN purchase_payments pp ON pa.purchase_payment_id = pp.id ' .
        //                        'JOIN purchases pu ON pp.purchase_id = pu.id ' .
        //                        'WHERE pa.tipo = "PC" ' .
        //                        'AND pa.baixa = 0 ' .
        //                        'AND pu.data_compra < ? ' .
        //                        'UNION ' .
        //                        'SELECT sum(pa.valor) as total FROM payables pa ' .
        //                        'JOIN purchase_payments pp ON pa.purchase_payment_id = pp.id ' .
        //                        'JOIN purchases pu ON pp.purchase_id = pu.id ' .
        //                        'WHERE pa.tipo = "PC" ' .
        //                        'AND pa.baixa = 1 ' .
        //                        'AND pa.data_baixa > ? ' .
        //                        'AND pu.data_compra < ? ', [
        //                    $data['data_referencia_de'],
        //                    $data['data_referencia_de'],
        //                    $data['data_referencia_de'],
        //                ])->fetchAll('assoc');
        //        $total = 0;
        //        foreach ($pagamentos_pendentes as $pagamento) {
        //            $total += $pagamento['total'];
        //        }
        //        $estoque_inicial -= $total;
        // ===== Estoque final =====
        $estoque_final = 0;
        $purchases = TableRegistry::get('Purchases')->find()
            ->where(['data_compra <=' => $data['data_referencia_ate']])
            ->contain(['Distributors', 'Plants']);
        $transferidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_transfers ' .
                'WHERE created <= ? ' .
                'AND transito = 0 ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_ate'] . ' 23:59:59']
        )->fetchAll('assoc');
        $devolvidos = $conn->execute(
            'SELECT purchase_id, SUM(volume) AS total_volume FROM purchase_devolutions ' .
                'WHERE created <= ? ' .
                'GROUP BY purchase_id',
            [$data['data_referencia_ate'] . ' 23:59:59']
        )->fetchAll('assoc');
        $vendidos_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume_conciliado) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda <= ? ' .
                'AND s.conciliado = 1 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_ate']]
        )->fetchAll('assoc');
        $vendidos_nao_conciliado = $conn->execute(
            'SELECT sp.purchase_id, sum(sp.volume) as volume FROM sales_purchases sp ' .
                'INNER JOIN sales s ON s.id = sp.sale_id ' .
                'WHERE s.data_venda <= ? ' .
                'AND s.conciliado = 0 ' .
                'GROUP BY sp.purchase_id',
            [$data['data_referencia_ate']]
        )->fetchAll('assoc');
        $ativo_estoques = [];
        foreach ($purchases as $purchase) {
            $transferido = 0;
            foreach ($transferidos as $t) {
                if ($t['purchase_id'] == $purchase->id) {
                    $transferido = $t['total_volume'];
                    break;
                }
            }
            $devolvido = 0;
            foreach ($devolvidos as $d) {
                if ($d['purchase_id'] == $purchase->id) {
                    $devolvido = $d['total_volume'];
                    break;
                }
            }
            $vendido = 0;
            foreach ($vendidos_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido = $v['volume'];
                    break;
                }
            }
            foreach ($vendidos_nao_conciliado as $v) {
                if ($v['purchase_id'] == $purchase->id) {
                    $vendido += $v['volume'];
                    break;
                }
            }
            $estoque = $purchase->volume_comprado + $transferido - $devolvido - $vendido;
            if ($estoque > 0) {
                $adjustment = TableRegistry::get('PurchaseTransfers')->find()
                    ->where([
                        'purchase_id' => $purchase->id,
                        'created <=' => $data['data_referencia_ate'] . ' 23:59:59',
                        'preco_litro_adicional is not null'
                    ])->orderDesc('created')->first();
                $estoque_final += $estoque * ($purchase->preco_litro + (!is_null($adjustment) ? $adjustment->preco_litro_adicional : 0));
            }
        }
        //        $pagamentos_pendentes = $conn->execute(
        //                        'SELECT sum(pa.valor) as total FROM payables pa ' .
        //                        'JOIN purchase_payments pp ON pa.purchase_payment_id = pp.id ' .
        //                        'JOIN purchases pu ON pp.purchase_id = pu.id ' .
        //                        'WHERE pa.tipo = "PC" ' .
        //                        'AND pa.baixa = 0 ' .
        //                        'AND pu.data_compra <= ? ' .
        //                        'UNION ' .
        //                        'SELECT sum(pa.valor) as total FROM payables pa ' .
        //                        'JOIN purchase_payments pp ON pa.purchase_payment_id = pp.id ' .
        //                        'JOIN purchases pu ON pp.purchase_id = pu.id ' .
        //                        'WHERE pa.tipo = "PC" ' .
        //                        'AND pa.baixa = 1 ' .
        //                        'AND pa.data_baixa >= ? ' .
        //                        'AND pu.data_compra <= ? ', [
        //                    $data['data_referencia_ate'],
        //                    $data['data_referencia_ate'],
        //                    $data['data_referencia_ate'],
        //                ])->fetchAll('assoc');
        //        $total = 0;
        //        foreach ($pagamentos_pendentes as $pagamento) {
        //            $total += $pagamento['total'];
        //        }
        //        $estoque_final -= $total;
        // compras no periodo
        $purchases = TableRegistry::get('Purchases')->find()
            ->where([
                'data_compra >=' => $data['data_referencia_de'],
                'data_compra <=' => $data['data_referencia_ate']
            ])->toList();
        $compras_total = 0;
        foreach ($purchases as $purchase) {
            $compras_total += $purchase->volume_comprado * $purchase->preco_litro;
        }
        // vendas
        $sales = TableRegistry::get('Sales')->find()
            ->where([
                'data_venda >=' => $data['data_referencia_de'],
                'data_venda <=' => $data['data_referencia_ate']
            ])
            ->contain([
                'Carriers',
                'SalesPurchases',
                'SalesPurchases.Purchases',
                'SalesPurchases.Purchases.Distributors',
                'SaleDetails',
                'SaleDetails.Presales'
            ]);
        // devoluções
        $query = $conn->execute(
            'SELECT SUM(valor) AS total FROM ledger_entries ' .
                'WHERE ledger_entry_type_id = 23 ' .
                'AND data_lancamento >= ? ' .
                'AND data_lancamento <= ?',
            [$data['data_referencia_de'], $data['data_referencia_ate']]
        )->fetchAll('assoc');
        $devolucoes = $query[0]['total'];
        // comissoes vendedores
        $comissoes_vendedores = $conn->execute(
            'SELECT se.razao_social, sum(p.valor) as total ' .
                'FROM payables p ' .
                'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
                'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
                'INNER JOIN presales ps ON sd.presale_id = ps.id ' .
                'INNER JOIN clients c ON ps.client_id = c.id ' .
                'INNER JOIN sellers se ON c.seller_id = se.id ' .
                'WHERE p.tipo = "CV" ' .
                'AND sa.data_venda >= ? ' .
                'AND sa.data_venda <= ? ' .
                'GROUP BY se.id',
            [
                $data['data_referencia_de'],
                $data['data_referencia_ate']
            ]
        )->fetchAll('assoc');
        // comissoes corretoras
        $ccs = $conn->execute(
            'SELECT b.id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'INNER JOIN brokers b ON pu.broker_id = b.id ' .
                'WHERE p.tipo = "CC" ' .
                'AND s.data_venda >= ? ' .
                'AND s.data_venda <= ? ' .
                'GROUP BY b.id ' .
                'UNION ' .
                'SELECT b.id, b.razao_social, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'INNER JOIN purchases pu ON pt.purchase_id = pu.id ' .
                'INNER JOIN brokers b ON pu.broker_id = b.id ' .
                'WHERE p.tipo = "CT" ' .
                'AND DATE(pt.created) >= ? ' .
                'AND DATE(pt.created) <= ? ' .
                'GROUP BY b.id ',
            [
                $data['data_referencia_de'],
                $data['data_referencia_ate'],
                $data['data_referencia_de'],
                $data['data_referencia_ate'],
            ]
        )->fetchAll('assoc');
        $comissoes_corretoras = [];
        foreach ($ccs as $cc) {
            if (array_key_exists($cc['id'], $comissoes_corretoras)) {
                $c = $comissoes_corretoras[$cc['id']];
                $c['total'] += $cc['valor'];
                $comissoes_corretoras[$cc['id']] = $c;
            } else {
                $comissoes_corretoras[$cc['id']] = ['razao_social' => $cc['razao_social'], 'total' => $cc['valor']];
            }
        }
        $lccs = TableRegistry::get('LedgerEntries')->find()
            ->where([
                'ledger_entry_type_id = 8',
                'payable_id is null',
                'data_lancamento >=' => $data['data_referencia_de'],
                'data_lancamento <=' => $data['data_referencia_ate']
            ]);
        $t = 0;
        foreach ($lccs as $lcc) {
            $t += $lcc->valor;
        }
        if ($t > 0) {
            $comissoes_corretoras[0] = ['razao_social' => 'Entradas Manuais', 'total' => $t];
        }
        // fretes e spreads
        $fretes = [];
        $spreads = [];
        foreach ($sales as $sale) {
            foreach ($sale->sales_purchases as $sp) {
                $distributor = $sp->purchase->distributor;
                if (array_key_exists($distributor->id, $spreads)) {
                    $s = $spreads[$distributor->id];
                    $s['total'] += $sp->spread;
                    $spreads[$distributor->id] = $s;
                } else {
                    $spreads[$distributor->id] = ['razao_social' => $distributor->razao_social, 'total' => $sp->spread];
                }
            }
            $carrier = $sale->carrier;
            if (array_key_exists($carrier->id, $fretes)) {
                $c = $fretes[$carrier->id];
                $c['total'] += $sale->frete;
                $fretes[$carrier->id] = $c;
            } else {
                $fretes[$carrier->id] = ['razao_social' => $carrier->razao_social, 'total' => $sale->frete];
            }
        }
        $fts = TableRegistry::get('PurchaseTransfers')->find()->where([
            'destination_purchase_id is not null',
            'PurchaseTransfers.created >=' => $data['data_referencia_de'] . ' 00:00:00',
            'PurchaseTransfers.created <=' => $data['data_referencia_ate'] . ' 23:59:59'
        ])
            ->contain('Carriers');
        foreach ($fts as $ft) {
            $carrier = $ft->carrier;
            if (array_key_exists($carrier->id, $fretes)) {
                $c = $fretes[$carrier->id];
                $c['total'] += $ft->frete;
                $fretes[$carrier->id] = $c;
            } else {
                $fretes[$carrier->id] = ['razao_social' => $carrier->razao_social, 'total' => $ft->frete];
            }
        }
        // outras despesas
        $ledger_entries = TableRegistry::get('LedgerEntries')->find()
            ->where([
                'ledger_entry_type_id >=' => 3,
                'ledger_entry_type_id <=' => 25,
                'data_lancamento >= ' => $data['data_referencia_de'],
                'data_lancamento <= ' => $data['data_referencia_ate'],
            ])
            ->contain([
                'Accounts',
                'Accounts.Distributors',
                'Accounts.Banks',
                'Payables',
                'Payables.SaleDetails',
                'Payables.SaleDetails.Presales',
                'Payables.SaleDetails.Presales.Clients',
                'Payables.SaleDetails.Presales.Clients.Sellers',
                'Payables.Sales',
                'Payables.Sales.Carriers',
                'Payables.SalesPurchases.Purchases',
                'Payables.SalesPurchases.Purchases.Distributors',
                'Receivables'
            ]);

        $this->set(compact('data', 'estoque_inicial', 'estoque_final', 'compras_total', 'sales', 'devolucoes', 'ledger_entries', 'comissoes_vendedores', 'comissoes_corretoras', 'spreads', 'fretes'));
    }

    public function resumoDiario()
    {
        $conn = ConnectionManager::get('default');
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $resumos = [];
        $sales = TableRegistry::get('Sales')->find()
            ->where([
                'data_venda >=' => $data['data_referencia_de'],
                'data_venda <=' => $data['data_referencia_ate']
            ])
            ->contain([
                'SalesPurchases',
                'SalesPurchases.Purchases',
                'SaleDetails',
                'SaleDetails.Presales',
            ]);
        $payables_comissoes_vendedores = $conn->execute(
            'SELECT sa.data_venda, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sale_details sd ON sd.id = p.sale_detail_id ' .
                'INNER JOIN sales sa ON sd.sale_id = sa.id ' .
                'WHERE p.tipo = "CV" ' .
                'AND sa.data_venda >= ? ' .
                'AND sa.data_venda <= ? ' .
                'GROUP BY sa.data_venda',
            [
                $data['data_referencia_de'],
                $data['data_referencia_ate']
            ]
        )->fetchAll('assoc');
        $payables_comissoes_corretoras = $conn->execute(
            'SELECT s.data_venda, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN sales_purchases sp ON sp.id = p.sales_purchases_id ' .
                'INNER JOIN purchases pu ON pu.id = sp.purchase_id ' .
                'INNER JOIN sales s ON sp.sale_id = s.id ' .
                'WHERE p.tipo = "CC" ' .
                'AND s.data_venda >= ? ' .
                'AND s.data_venda <= ? ' .
                'GROUP BY s.data_venda ' .
                'UNION ' .
                'SELECT DATE(pt.created) as data_venda, sum(p.valor) AS valor ' .
                'FROM payables p ' .
                'INNER JOIN purchase_transfers pt ON pt.id = p.purchase_transfer_id ' .
                'WHERE p.tipo = "CT" ' .
                'AND DATE(pt.created) >= ? ' .
                'AND DATE(pt.created) <= ? ' .
                'GROUP BY DATE(pt.created)',
            [
                $data['data_referencia_de'],
                $data['data_referencia_ate'],
                $data['data_referencia_de'],
                $data['data_referencia_ate'],
            ]
        )->fetchAll('assoc');
        $data_referencia = $data['data_referencia_de'];
        while ($data_referencia <= $data['data_referencia_ate']) {
            $volume = 0;
            $faturamento_bruto = 0;
            $custo_produto = 0;
            $spread = 0;
            $frete = 0;
            $comissao_corretora = 0;
            $comissao_vendedor = 0;
            foreach ($sales as $sale) {
                if ($sale->data_venda == date('d/m/Y', strtotime($data_referencia))) {
                    foreach ($sale->sales_purchases as $sp) {
                        $volume += $sp->volume;

                        $adjustment = TableRegistry::get('PurchaseTransfers')->find()
                            ->where([
                                'purchase_id' => $sp->purchase_id,
                                'created <=' => $data_referencia . ' 23:59:59'
                            ])->orderDesc('created')->first();
                        $preco_litro_adicional = !is_null($adjustment) ? $adjustment->preco_litro_adicional : 0;
                        $custo_produto += ($sale->conciliado ? $sp->volume_conciliado : $sp->volume) * ($sp->purchase->preco_litro + $preco_litro_adicional);
                        //                        $custo_produto += ($sale->conciliado ? $sp->volume_conciliado : $sp->volume) * ($sp->purchase->preco_litro + $sp->purchase->preco_litro_adicional);
                        $spread += $sp->spread;
                    }
                    foreach ($sale->sale_details as $detail) {
                        $faturamento_bruto += $detail->volume * $detail->presale->preco_venda;
                    }
                    $frete += $sale->frete;
                }
            }
            foreach ($payables_comissoes_vendedores as $payable) {
                if ($data_referencia == date('Y-m-d', strtotime(str_replace('/', '-', $payable['data_venda'])))) {
                    $comissao_vendedor = round($payable['valor'], 2);
                }
            }
            foreach ($payables_comissoes_corretoras as $payable) {
                if ($data_referencia == date('Y-m-d', strtotime(str_replace('/', '-', $payable['data_venda'])))) {
                    $comissao_corretora += round($payable['valor'], 2);
                }
            }
            $resumo['data'] = $data_referencia;
            $resumo['volume'] = $volume;
            $resumo['faturamento_bruto'] = $faturamento_bruto;
            $resumo['custo_produto'] = $custo_produto;
            $resumo['spread'] = $spread;
            $resumo['frete'] = $frete;
            $resumo['comissao_corretora'] = $comissao_corretora;
            $resumo['comissao_vendedor'] = $comissao_vendedor;
            $resumos[] = $resumo;
            $data_referencia = date('Y-m-d', strtotime($data_referencia . ' +1 day'));
        }

        $this->viewBuilder()->options([
            'pdfConfig' => [
                'orientation' => 'landscape',
                'filename' => 'resumo.pdf',
            ],
        ]);

        $this->set(compact('data', 'resumos'));
    }

    // ============== Resumos ==============

    public function comissaoCorretoraResumo()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        // if ($data['a_pagar_corretora'] == 'Y' or $data['a_pagar_corretora'] == 'N') {
        //     $data['a_pagar_corretora'] = $data['a_pagar_corretora'] == 'Y' ? 1 : 0;
        // }
        $comissoes = TableRegistry::get('Payables')->find()
            ->matching('SalesPurchases.Sales')
            ->matching('SalesPurchases.Purchases')
            ->where([
                'Payables.tipo' => 'CC',
                'Purchases.corretada = 1',
                'Sales.data_venda >=' => $data['data_referencia_de'],
                'Sales.data_venda <=' => $data['data_referencia_ate'],
            ])
            ->contain([
                'SalesPurchases',
                'SalesPurchases.Sales',
                'SalesPurchases.Purchases',
                'SalesPurchases.Purchases.Brokers',
            ])->orderAsc('Sales.data_venda');
        if ($data['broker_id'] != '') {
            $comissoes->where(['Purchases.broker_id' => $data['broker_id']]);
        }
        if ($data['a_pagar_corretora']) {
            $comissoes->where(['Payables.baixa' => 0]);
        }
        $rows = [];
        foreach ($comissoes as $comissao) {
            $broker_id = $comissao->sales_purchase->purchase->broker_id;
            if (array_key_exists($broker_id, $rows)) {
                $row = $rows[$broker_id];
                if (!in_array($comissao->sales_purchase->purchase_id, $row['purchases'])) {
                    $row['purchases'][] = $comissao->sales_purchase->purchase_id;
                    $row['contratos'] .= $comissao->sales_purchase->purchase->numero_contrato . ', ';
                }
                $row['comissao'] += $comissao->valor;
            } else {
                $row['broker_id'] = $comissao->sales_purchase->purchase->broker_id;
                $row['corretora'] = $comissao->sales_purchase->purchase->broker->razao_social;
                $row['purchases'] = [$comissao->sales_purchase->purchase_id];
                $row['contratos'] = $comissao->sales_purchase->purchase->numero_contrato . ', ';
                $row['comissao'] = $comissao->valor;
            }
            $rows[$broker_id] = $row;
        }

        $comissao_transferencias = TableRegistry::get('Payables')->find()
            ->matching('PurchaseTransfers')
            ->matching('PurchaseTransfers.Purchases')
            ->where([
                'Payables.tipo' => 'CT',
                'PurchaseTransfers.created >=' => $data['data_referencia_de'] . ' 00:00:00',
                'PurchaseTransfers.created <=' => $data['data_referencia_ate'] . ' 23:59:59',
            ])
            ->contain([
                'PurchaseTransfers',
                'PurchaseTransfers.Purchases',
                'PurchaseTransfers.Purchases.Brokers',
            ]);
        if ($data['broker_id'] != '') {
            $comissao_transferencias->where(['Purchases.broker_id' => $data['broker_id']]);
        }
        if ($data['a_pagar_corretora']) {
            $comissao_transferencias->where(['Payables.baixa' => 0]);
        }
        foreach ($comissao_transferencias as $comissao) {
            $t = $comissao->purchase_transfer;
            $broker_id = $t->purchase->broker_id;
            if (array_key_exists($broker_id, $rows)) {
                $row = $rows[$broker_id];
                if (!in_array($t->purchase_id, $row['purchases'])) {
                    $row['purchases'][] = $t->purchase_id;
                    $row['contratos'] .= $t->purchase->numero_contrato . ', ';
                }
                $row['comissao'] += $comissao->valor;
            } else {
                $row['broker_id'] = $t->purchase->broker_id;
                $row['corretora'] = $t->purchase->broker->razao_social;
                $row['purchases'] = [$t->purchase_id];
                $row['contratos'] = $t->purchase->numero_contrato . ', ';
                $row['comissao'] = $comissao->valor;
            }
            $rows[$broker_id] = $row;
        }

        $this->set(compact('data', 'rows', 'comissoes', 'comissao_transferencias'));
    }

    public function comissaoVendedorResumo()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        if ($data['a_pagar_vendedor'] == 'Y' or $data['a_pagar_vendedor'] == 'N') {
            $data['a_pagar_vendedor'] = $data['a_pagar_vendedor'] == 'Y' ? 1 : 0;
        }
        $payables = TableRegistry::get('Payables')->find()
            ->matching('SaleDetails.Sales')
            ->matching('SaleDetails.Presales.Clients')
            ->where([
                'Payables.tipo' => 'CV',
                'Sales.data_venda >=' => $data['data_referencia_de'],
                'Sales.data_venda <=' => $data['data_referencia_ate'],
            ])
            ->contain([
                'SaleDetails',
                'SaleDetails.Sales',
                'SaleDetails.Presales',
                'SaleDetails.Presales.Clients',
                'SaleDetails.Presales.Clients.Sellers'
            ])
            ->orderAsc('Sales.data_venda');
        if ($data['seller_id'] != '') {
            $payables->where(['Clients.seller_id' => $data['seller_id']]);
        }
        if ($data['a_pagar_vendedor']) {
            $payables->where(['Payables.baixa' => 0]);
        }
        $rows = [];
        foreach ($payables as $payable) {
            $seller_id = $payable->sale_detail->presale->client->seller_id;
            if (array_key_exists($seller_id, $rows)) {
                $row = $rows[$seller_id];
                $row['volume'] += $payable->sale_detail->volume;
                $row['comissao'] += $payable->sale_detail->comissao;
            } else {
                $row['seller_id'] = $seller_id;
                $row['vendedor'] = $payable->sale_detail->presale->client->seller->razao_social;
                $row['volume'] = $payable->sale_detail->volume;
                $row['comissao'] = $payable->sale_detail->comissao;
            }
            $rows[$seller_id] = $row;
        }
        if (array_key_exists('seller_id', $data) and $data['seller_id'] != '') {
            $seller = TableRegistry::get('Sellers')->get($data['seller_id']);
        }
        $this->viewBuilder()->options([
            'pdfConfig' => [
                'orientation' => 'landscape',
                'filename' => 'resumo.pdf',
            ],
        ]);
        $this->set(compact('data', 'rows', 'payables', 'seller'));
    }

    public function spreadResumo()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        if ($data['a_pagar_spread'] == 'Y' or $data['a_pagar_spread'] == 'N') {
            $data['a_pagar_spread'] = $data['a_pagar_spread'] == 'Y' ? 1 : 0;
        }
        $payables = TableRegistry::get('Payables')->find()
            ->matching('SalesPurchases')
            ->matching('SalesPurchases.Sales')
            ->matching('SalesPurchases.Purchases')
            ->where([
                'Payables.tipo' => 'SP',
                'Sales.data_venda >=' => $data['data_referencia_de'],
                'Sales.data_venda <=' => $data['data_referencia_ate'],
            ])
            ->contain([
                'SalesPurchases',
                'SalesPurchases.Sales',
                'SalesPurchases.Purchases',
                'SalesPurchases.Purchases.Distributors',
            ]);
        if (array_key_exists('distributor_id', $data) and $data['distributor_id'] != '') {
            $payables->where(['Purchases.distributor_id' => $data['distributor_id']]);
        }
        if ($data['a_pagar_spread']) {
            $payables->where(['Payables.baixa' => 0]);
        }
        $rows = [];
        foreach ($payables as $payable) {
            $distributor_id = $payable->sales_purchase->purchase->distributor_id;
            if (array_key_exists($distributor_id, $rows)) {
                $row = $rows[$distributor_id];
                $row['volume'] += $payable->sales_purchase->volume;
                $row['spread'] += $payable->valor;
            } else {
                $row['distributor_id'] = $distributor_id;
                $row['distribuidora'] = $payable->sales_purchase->purchase->distributor->razao_social;
                $row['volume'] = $payable->sales_purchase->volume;
                $row['spread'] = $payable->valor;
            }
            $rows[$distributor_id] = $row;
        }
        $this->set(compact('data', 'rows'));
    }

    public function freteResumo()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        if ($data['a_pagar_frete'] == 'Y' or $data['a_pagar_frete'] == 'N') {
            $data['a_pagar_frete'] = $data['a_pagar_frete'] == 'Y' ? 1 : 0;
        }
        $fretes = TableRegistry::get('Payables')->find()
            ->matching('Sales')
            ->where([
                'Payables.tipo' => 'FR',
                'Payables.valor > 0',
                'Sales.data_venda >=' => $data['data_referencia_de'],
                'Sales.data_venda <=' => $data['data_referencia_ate'],
            ])
            ->contain([
                'Sales',
                'Sales.Purchases',
                'Sales.Purchases.Brokers',
                'Sales.Purchases.Plants',
                'Sales.Purchases.Distributors',
                'Sales.Carriers',
                'Sales.Vehicles',
                'Sales.Drivers',
                'Sales.SaleDetails',
                'Sales.SaleDetails.Presales',
                'Sales.SaleDetails.Presales.Clients',
            ])
            ->orderAsc('Sales.data_venda');
        if (array_key_exists('carrier_id', $data) and $data['carrier_id'] != '') {
            $fretes->where(['Sales.carrier_id' => $data['carrier_id']]);
        }
        if ($data['a_pagar_frete']) {
            $fretes->where(['Payables.baixa' => 0]);
        }
        $rows = [];
        foreach ($fretes as $frete) {
            $carrier_id = $frete->sale->carrier_id;
            if (array_key_exists($carrier_id, $rows)) {
                $row = $rows[$carrier_id];
                $row['frete'] += $frete->valor;
            } else {
                $row['tipo'] = 'RG';
                $row['carrier_id'] = $carrier_id;
                $row['transportadora'] = $frete->sale->carrier->razao_social;
                $row['frete'] = $frete->valor;
            }
            $rows[$carrier_id] = $row;
        }

        $frete_transferencias = TableRegistry::get('Payables')->find()
            ->matching('PurchaseTransfers')
            ->where([
                'Payables.tipo' => 'FT',
                'PurchaseTransfers.created >=' => $data['data_referencia_de'] . ' 00:00:00',
                'PurchaseTransfers.created <=' => $data['data_referencia_ate'] . ' 23:59:59'
            ])
            ->contain([
                'PurchaseTransfers',
                'PurchaseTransfers.Carriers',
                'PurchaseTransfers.Plants',
            ]);
        if (array_key_exists('carrier_id', $data) and $data['carrier_id'] != '') {
            $frete_transferencias->where(['PurchaseTransfers.carrier_id' => $data['carrier_id']]);
        }
        if ($data['a_pagar_frete']) {
            $frete_transferencias->where(['Payables.baixa' => 0]);
        }
        $frete_transferencias->toList();
        foreach ($frete_transferencias as $frete_transferencia) {
            $carrier_id = $frete_transferencia->purchase_transfer->carrier_id;
            if (array_key_exists($carrier_id, $rows)) {
                $row = $rows[$carrier_id];
                $row['frete'] += $frete_transferencia->valor;
            } else {
                $row['tipo'] = 'TR';
                $row['carrier_id'] = $carrier_id;
                $row['transportadora'] = $frete_transferencia->purchase_transfer->carrier->razao_social;
                $row['frete'] = $frete_transferencia->valor;
            }
            $rows[$carrier_id] = $row;
        }
        $this->set(compact('data', 'rows', 'fretes', 'frete_transferencias'));
    }

    // ============== Estoque ==============

    public function estoqueAbertura()
    {
        ini_set('memory_limit', '1024M');
        //
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        // $date = new Date($data['data_referencia_ate']);
        // $date = date("Y-m-d", strtotime($data['data_referencia_ate']));
        // $dates =  date("d/m/Y", strtotime($data['data_referencia_ate']));
        // $datef =  date("m/d/Y", strtotime($data['data_referencia_ate']));

        $purchases = TableRegistry::get('Purchases')->find()
            ->where([

                'volume_comprado + volume_transferido - volume_devolvido - volume_vendido > 0'
            ])
            //certo
            ->matching('Plants')
            ->orderAsc('Plants.razao_social')
            ->orderAsc('Purchases.created')
            ->contain([
                'Distributors',
                'Plants'
            ]);





        $rows = [];
        $contratos = [];
        // ignore below because its for the table name / total volume show on the label

        foreach ($purchases as $purchase) {
            //Encontra todas as sals purchase do purchase id tal
            // $sales_purchases = TableRegistry::get('SalesPurchases')->find()
            //     ->where(['purchase_id' => $purchase->id], ['DATE(SalesPurchases.created) <=' => $date,])
            //     ->contain([
            //         'Sales',
            //         'SalesPurchasesConciliations',
            //         'Purchases',
            //         // ['Purchases' => function (Query $q) {
            //         //     return $q
            //         //         ->select(['purchase_id',])
            //         //         ->where(['data_compra <=' => this . $date,]);
            //         // }]

            //         //                   'Purchases' => function (Query $q) {
            //         // return $q->where(['data_compra <=' => $date]); } ,

            //     ]);

            $contratos[] = $purchase->id;
            $distributor_id = $purchase->distributor_id;
            if (array_key_exists($distributor_id, $rows)) {
                $row = $rows[$distributor_id];
                $row['volume'] += $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_vendido;
            } else {
                $row['distribuidora'] = $purchase->distributor->razao_social;
                $row['volume'] = $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_vendido;
            }
            $rows[$distributor_id] = $row;
        }



        $this->set(compact('rows',  'purchases', 'contratos', 'datef', 'dates', 'date'));
    }



    public function compra()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $purchases = TableRegistry::get('Purchases')->find('search', ['search' => $data])->contain([
            'Distributors',
            'Plants'
        ])
            ->orderAsc('data_compra')
            ->toList();
        $distributor = null;
        $plant = null;
        if ($data['distributor_id'] != '') {
            $distributor = TableRegistry::get('Distributors')->get($data['distributor_id']);
        }
        if ($data['plant_id'] != '') {
            $plant = TableRegistry::get('Plants')->get($data['plant_id']);
        }
        $this->set(compact('data', 'purchases', 'distributor', 'plant'));
    }

    public function perdasSobras()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $conn = ConnectionManager::get('default');
        $resumos = [];
        $data_referencia = $data['data_referencia_de'];
        $sales = TableRegistry::get('Sales')->find()
            ->where([
                'data_venda >=' => $data['data_referencia_de'],
                'data_venda <=' => $data['data_referencia_ate']
            ]);
        $purchases = TableRegistry::get('Purchases')->find()
            ->where([
                'data_compra >=' => $data['data_referencia_de'],
                'data_compra <=' => $data['data_referencia_ate']
            ]);
        while ($data_referencia <= $data['data_referencia_ate']) {
            $resumo['data'] = $data_referencia;
            $vendas = 0;
            //  foreach ($sales as $sale) {
            //         if ($sale->data_venda == date('d/m/Y', strtotime($data_referencia))) {
            //             $vendas += $sale->conciliado ? $sale->volume_conciliado : $sale->volume_total;
            //         }
            //     }

            foreach ($sales as $sale) {
                if ($sale->data_venda == date('d/m/Y', strtotime($data_referencia))) {
                    $vendas += $sale->volume ? $sale->volume : $sale->volume_total;
                }
            }
            $resumo['vendas'] = $vendas;

            $vendas_conciliadas = 0;
            foreach ($sales as $sale) {
                if ($sale->data_venda == date('d/m/Y', strtotime($data_referencia))) {
                    $vendas_conciliadas += $sale->conciliado ? $sale->volume_conciliado : $sale->volume_total;
                }
            }
            $resumo['vendas_conciliadas'] = $vendas_conciliadas;
            $stmt = $conn->execute('SELECT SUM(p.volume_comprado) AS volume ' .
                'FROM purchases p ' .
                'WHERE data_compra < ?', [$data_referencia]);
            $comprado = $stmt->fetchAll('assoc');
            $stmt = $conn->execute('SELECT SUM(s.volume_total) AS volume ' .
                'FROM sales s ' .
                'WHERE conciliado = 0 ' .
                'AND data_venda < ?', [$data_referencia]);
            $nao_conciliado = $stmt->fetchAll('assoc');
            $stmt = $conn->execute('SELECT SUM(s.volume_conciliado) AS volume ' .
                'FROM sales s ' .
                'WHERE conciliado = 1 ' .
                'AND data_venda < ?', [$data_referencia]);
            $conciliado = $stmt->fetchAll('assoc');
            $resumo['estoque_inicial'] = $comprado[0]['volume'] - $nao_conciliado[0]['volume'] - $conciliado[0]['volume'];

            $compras = 0;
            foreach ($purchases as $purchase) {
                if ($purchase->data_compra == date('d/m/Y', strtotime($data_referencia))) {
                    $compras += $purchase->volume_comprado + $purchase->volume_transferido - $purchase->volume_devolvido - $purchase->volume_devolvido;
                }
            }
            $resumo['compras'] = $compras;

            $stmt = $conn->execute('SELECT SUM(p.volume_comprado) AS volume ' .
                'FROM purchases p ' .
                'WHERE data_compra <= ?', [$data_referencia]);
            $comprado = $stmt->fetchAll('assoc');
            $stmt = $conn->execute('SELECT SUM(s.volume_total) AS volume ' .
                'FROM sales s ' .
                'WHERE conciliado = 0 ' .
                'AND data_venda <= ?', [$data_referencia]);
            $nao_conciliado = $stmt->fetchAll('assoc');
            $stmt = $conn->execute('SELECT SUM(s.volume_conciliado) AS volume ' .
                'FROM sales s ' .
                'WHERE conciliado = 1 ' .
                'AND data_venda <= ?', [$data_referencia]);
            $conciliado = $stmt->fetchAll('assoc');
            $resumo['estoque_final'] = $comprado[0]['volume'] - $nao_conciliado[0]['volume'] - $conciliado[0]['volume'];

            $resumos[] = $resumo;

            $data_referencia = date('Y-m-d', strtotime($data_referencia . ' +1 day'));
        }

        $this->set(compact('data', 'resumos'));
    }

    // ============== Vendas ==============

    public function vendasCliente()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $details = TableRegistry::get('SaleDetails')->find()
            ->matching('Sales')
            ->matching('Presales.Clients')
            ->where([
                'Sales.data_venda >=' => $data['data_referencia_de'],
                'Sales.data_venda <=' => $data['data_referencia_ate'],
            ])
            ->contain([
                'Presales',
                'Presales.Clients'
            ])
            ->orderAsc('Clients.razao_social');
        $rows = [];
        foreach ($details as $detail) {
            $client_id = $detail->presale->client_id;
            if (array_key_exists($client_id, $rows)) {
                $row = $rows[$client_id];
                $row['volume'] += $detail->volume;
                $row['valor'] += $detail->volume * $detail->presale->preco_venda;
            } else {
                $row['cliente'] = $detail->presale->client->razao_social;
                $row['volume'] = $detail->volume;
                $row['valor'] = $detail->volume * $detail->presale->preco_venda;
            }
            $rows[$client_id] = $row;
        }
        $this->set(compact('data', 'rows'));
    }

    public function vendasVendedor()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $sales = TableRegistry::get('Sales')->find('search', ['search' => $data])->contain([
            'SaleDetails',
            'SaleDetails.Presales',
            'SaleDetails.Presales.Clients',
            'SaleDetails.Presales.Clients.Sellers'
        ])->toList();
        $rows = [];
        foreach ($sales as $sale) {
            foreach ($sale->sale_details as $detail) {
                $seller_id = $detail->presale->client->seller_id;
                if (!is_null($seller_id)) {
                    if (array_key_exists($seller_id, $rows)) {
                        $row = $rows[$seller_id];
                        $row['volume'] += $detail->volume;
                    } else {
                        $row['vendedor'] = $detail->presale->client->seller->razao_social;
                        $row['volume'] = $detail->volume;
                    }
                    $rows[$seller_id] = $row;
                }
            }
        }

        $this->set(compact('data', 'rows', 'sales'));
    }

    // ============== Clientes ==============

    public function cliente()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $clients = TableRegistry::get('Clients')->find('search', ['search' => $data])->contain([
            'ClientEmails',
            'ClientPhones'
        ])->toList();
        $cidade = $data['cidade'];
        $this->set(compact('clients', 'cidade'));
    }
}
