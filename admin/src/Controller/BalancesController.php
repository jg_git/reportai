<?php

namespace App\Controller;

use Cake\Datasource\ConnectionManager;

class BalancesController extends AppController {

    public function report() {
        $conn = ConnectionManager::get('default');
        $accounts = $conn->execute(
                'select a.id, a.tipo, d.razao_social, b.nome as nome_banco, a.titular, a.numero_agencia, a.numero_conta, i.data_saldo, i.dc, i.valor as initial ' .
                'from initial_balances i ' .
                'inner join accounts a on i.account_id = a.id ' .
                'left join distributors d on a.distributor_id = d.id ' .
                'left join banks b on a.bank_id = b.id ' . 
                'where a.ativa = 1 ' .
                'order by nome_banco, d.razao_social, a.titular asc'
        )->fetchAll('assoc');
        $balances = [];
        foreach ($accounts as $account) {
            $balance['tipo'] = $account['tipo'];
            $balance['nome'] = $account['tipo'] == 'I' ? $account['razao_social'] : $account['nome_banco'];
            $balance['titular'] = $account['titular'];
            $balance['numero_agencia'] = $account['numero_agencia'];
            $balance['numero_conta'] = $account['numero_conta'];
            $b = $account['dc'] == 'D' ? -1 * $account['initial'] : $account['initial'];

            $stmt = $conn->execute('select sum(l.valor) as sum from ledger_entries l ' .
                    'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                    'where t.dc = "C" ' .
                    'and l.account_id = ? ' .
                    'and l.data_lancamento >= ?', [$account['id'], $account['data_saldo']]);
            $resp = $stmt->fetchAll('assoc');
            $credit = $resp[0]['sum'];

            $stmt = $conn->execute('select sum(l.valor) as sum from ledger_entries l ' .
                    'inner join ledger_entry_types t on l.ledger_entry_type_id = t.id ' .
                    'where t.dc = "D" ' .
                    'and l.account_id = ? ' .
                    'and l.data_lancamento >= ?', [$account['id'], $account['data_saldo']]);
            $resp = $stmt->fetchAll('assoc');
            $debit = $resp[0]['sum'];

            $balance['balance'] = $b + $credit - $debit;
            $balances[] = $balance;
        }
        $this->set(compact('balances'));
    }

}
