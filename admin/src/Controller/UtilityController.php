<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;

class UtilityController extends AppController {

    public function isAuthorized($user) {
        $action = $this->request->getParam('action');
        return true;
    }

    public function initialize() {
        parent::initialize();
        $this->viewBuilder()->setLayout('ajax');
    }

    public function status($status) {
        $this->set('status', $status);
    }

}
