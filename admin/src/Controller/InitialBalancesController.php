<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class InitialBalancesController extends AppController {

    public function initialize() {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => [
            ]
        ]);
    }

    public function index() {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $fields = ['descricao'];
        foreach ($fields as $field) {
            if (array_key_exists($field, $data)) {
                $data[$field] = explode(' ', $data[$field]);
            }
        }
        $query = $this->InitialBalances->find('search', ['search' => $data])->contain(['Accounts', 'Accounts.Distributors', 'Accounts.Banks']);
        $balances = $this->paginate($query);
        $accounts = $this->getAccounts();
        $this->set(compact('balances', 'accounts'));
    }

    public function edit($id) {
        if ($id != 'new') {
            $balance = $this->InitialBalances->get($id, ['contain' => []]);
        } else {
            $balance = $this->InitialBalances->newEntity();
            $balance->dc = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $balance = $this->InitialBalances->patchEntity($balance, $data);
            if ($this->InitialBalances->save($balance)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $balance->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $accounts = $this->getAccounts();
        $this->set(compact('balance', 'accounts'));
    }

    public function delete($id = null) {
        $this->request->allowMethod(['post', 'delete']);
        $balance = $this->InitialBalances->get($id);
        if ($this->InitialBalances->delete($balance)) {
            $this->Flash->success('Cadastro removido com sucesso');
        } else {
            $this->Flash->error('Erro na remoção do cadastro');
        }
        return $this->redirect(['action' => 'index']);
    }

}
