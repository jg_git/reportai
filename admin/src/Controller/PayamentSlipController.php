<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Client\FormData;
use Cake\I18n\Date;

class PayamentSlipController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
        ]);
    }

    // public function isAuthorized($user)
    // {
    //     return true;
    // }
    //  $response = $http->post('https://oauth.itau.com.br/identify/connect/token', [
    //     'grant_type' => 'client_credentials',
    //     'client_id' => '-vpAwJj2xv4e0',
    //     'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'
    // ]);

    public function obterTokenItau()
    {

        if ($this->Cookie->read('token')) {
        } else {
            $this->Cookie->config('path', '/');
            $this->Cookie->config([
                'expires' => '+4 hours',
                'httpOnly' => true
            ]);
        }

        $session = $this->request->session();

        if ($this->Cookie->read('token')) {
        } else {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://oauth.itau.com.br/identity/connect/token",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "grant_type=client_credentials&scope=readonly",
                CURLOPT_HTTPHEADER => array(
                    "Accept: */*",
                    "Accept-Encoding: gzip, deflate",
                    "Authorization: Basic LXZwQXdKajJ4djRlMDpmQmVXWUkwUlFYaXdiSkp2Y01YRlJBZ3gzRldMV0dWblNkWVZoRTFUcFQtbXRMakI3WVlDZ21XVGhQNFRnby1RdTd3MTQxMGlvc2p6TXhhdnNtbEVmZzI=",
                    "Cache-Control: no-cache",
                    "Connection: keep-alive",
                    "Content-Length: 44",
                    "Content-Type: application/x-www-form-urlencoded",
                    "Cookie: TS01f0d093=0151b32392ffa39f1467c030c40e31dbad40970be96218716ba90d0cb64127f151441ce1b9",
                    "Host: oauth.itau.com.br",
                    "Postman-Token: 1052079a-e442-4838-aff3-1911cdecd3a2,c0a0cfc7-afad-4782-9912-b8cb01e081e6",
                    "User-Agent: PostmanRuntime/7.20.1",
                    "cache-control: no-cache"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                // $response = json_encode($response);
                $resposta =  json_decode($response, true);
                $this->Cookie->write('token', $resposta['access_token']);
                //  $session->write('token', $resposta['access_token']);
            }
        }
    }

    public function emitirParaItau($PayamentData)
    {
        // $json_object = [];
        // $json_object->tipo_ambiente =  "1";
        // $json = json_encode($json_object);

        $access_token = $this->Cookie->read('token');
        $nome_pagador = mb_strimwidth($PayamentData->nome_pagador, 0, 30, "");
        // $cidade_pagador = mb_strimwidth($PayamentData->cidade_pagador, 0, 5, "");
        $cidade_pagador = $PayamentData->cidade_pagador;
        $data_antiga = $PayamentData->data_emissao;
        $data_un = str_replace('/', '-', $data_antiga);
        $data_unix = strtotime($data_un);
        $data_emissao = date('Y-m-d', $data_unix);
        $data_venc = $PayamentData->data_vencimento;
        $data_vencf = str_replace('/', '-', $data_venc);
        $data_vencat = strtotime($data_vencf);
        $data_vencimento = date('Y-m-d', $data_vencat);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://gerador-boletos.itau.com.br/router-gateway-app/public/codigo_barras/registro",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"tipo_ambiente\": 1,\n    \"tipo_registro\": 1,\n    \"tipo_cobranca\": 1,\n    \"tipo_produto\": \"00006\",\n    \"subproduto\": \"00008\",\n    \"beneficiario\": {\n        \"cpf_cnpj_beneficiario\": \"34530660000104\",\n        \"agencia_beneficiario\": \"6452\",\n        \"conta_beneficiario\": \"0011978\",\n        \"digito_verificador_conta_beneficiario\": \"4\"\n    },\n    \"identificador_titulo_empresa\": \"171294                   \",\n    \"titulo_aceite\": \"S\",\n    \"pagador\": {\n        \"cpf_cnpj_pagador\": \"$PayamentData->cpf_cnpj_pagador\",\n        \"nome_pagador\": \"$nome_pagador\",\n        \"logradouro_pagador\": \"$PayamentData->logradouro_pagador\",\n        \"cidade_pagador\": \"$cidade_pagador\",\n        \"uf_pagador\": \"$PayamentData->uf_pagador\",\n        \"cep_pagador\": \"$PayamentData->cep_pagador\"\n    },\n    \"tipo_carteira_titulo\": 109,\n    \"moeda\": {\n        \"codigo_moeda_cnab\": \"09\"\n    },\n    \"nosso_numero\": \"$PayamentData->nosso_numero\",\n    \"digito_verificador_nosso_numero\": \"$PayamentData->digito_verificador_nosso_numero\",\n    \"data_vencimento\": \"$data_vencimento\",\n    \"valor_cobrado\": \"$PayamentData->valor_cobrado\",\n    \"especie\": \"99\",\n    \"data_emissao\": \"$data_emissao\",\n    \"tipo_pagamento\": 3,\n    \"indicador_pagamento_parcial\": false,\n    \"juros\": {\n        \"tipo_juros\": 5\n    },\n    \"multa\": {\n        \"tipo_multa\": 3\n    },\n    \"grupo_desconto\": [\n        {\n            \"tipo_desconto\": 0\n        }\n    ],\n    \"recebimento_divergente\": {\n        \"tipo_autorizacao_recebimento\": \"3\"\n    }\n}",
            CURLOPT_HTTPHEADER => array(
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",

                "Content-Type: application/json",
                "Host: gerador-boletos.itau.com.br",

                "accept: application/vnd.itau",
                "access_token: $access_token",
                "cache-control: no-cache",
                "identificador: 34530660000104",
                "itau-chave: 9a6a013b-54df-49a5-bf99-f674761f5775"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
            $entity = $this->PayamentSlips->get($PayamentData->id);
            $result = $this->Articles->delete($entity);
            $this->Flash->success('Erro ao registrar esse boleto');
            echo "Não foi possível imprimir este boleto: " . $PayamentData->nosso_numero . $PayamentData->digito_verificador_nosso_numero;
        } else {
            $this->Flash->success('Boleto Emitido e Registrado com Sucesso! Nosso Numero: ' .  $PayamentData->nosso_numero . $PayamentData->digito_verificador_nosso_numero);
            $this->setAction('index');

            if ($response) {
                echo "Boleto Emitido e Registrado Com Sucesso! Nosso Numero: " . $PayamentData->nosso_numero . $PayamentData->digito_verificador_nosso_numero;
            } else {
                echo "Falha ao registrar esse boleto";
            }
        }
    }

    public function index()
    {
        if ($this->Cookie->read('token')) {
        } else {
            $this->obterTokenItau();
        }

        //   'body' => ['grant_type' => 'client_credentials', 'client_id' => 'vpAwJj2xv4e0', 'client_secret' => 'fBeWYI0RQXiwbJJvcMXFRAgx3FWLWGVnSdYVhE1TpT-mtLjB7YYCgmWThP4Tgo-Qu7w1410iosjzMxavsmlEfg2'],

        // $session = $this->request->getSession();
        // if (strpos($this->request->env('HTTP_REFERER'), 'receivables') === false) {
        //     $session->delete('cartr');
        // }
        // $cart = explode(',', $session->read('cartr'));
        // $cartdb = TableRegistry::get('Receivables')->find()->where(['id IN' => $cart]);
        // $total = 0;
        // foreach ($cartdb as $item) {
        //     $total += $item->valor;
        // }
        // $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        // $fields = [];
        // foreach ($fields as $field) {
        //     if (array_key_exists($field, $data)) {
        //         $data[$field] = date('Y-m-d', strtotime(str_replace('/', '-', $data[$field])));
        //     }
        // }
        // if (array_key_exists('pendentes', $data)) {
        //     $data['baixa'] = 0;
        // }
        // $query = $this->Receivables->find('search', ['search' => $data])->contain([
        //     'SaleDetails',
        //     'SaleDetails.Sales',
        //     'SaleDetails.Sales.Purchases',
        //     'SaleDetails.Sales.Purchases.Brokers',
        //     'SaleDetails.Sales.Purchases.Plants',
        //     'SaleDetails.Presales',
        //     'SaleDetails.Presales.Clients',
        //     'SaleDetails.Presales.Clients.Sellers'
        // ])->orderAsc('Receivables.data_vencimento');
        // $receivables = $this->paginate($query);
        //->where(['nota_fiscal !=' => '']),

        $receivables = TableRegistry::get('Receivables')->find('all')->where([
            'Receivables.boleto_gerado' => 0,
            'Receivables.baixa' => 0,
            'Receivables.data_vencimento >=' => new Date()
        ])->contain([
            'SaleDetails',
            'SaleDetails.Accounts',
            'SaleDetails.Sales',
            'SaleDetails.Sales.Purchases',
            'SaleDetails.Sales.Purchases.Brokers',
            'SaleDetails.Sales.Purchases.Plants',
            'SaleDetails.Presales',
            'SaleDetails.Presales.Clients',
            'SaleDetails.Presales.Clients.Sellers'
        ])->matching('SaleDetails.Accounts', function ($q) {
            // return $q->where(['SaleDetails.Accounts.titular !=' => '']);
            return $q->where(['titular LIKE' => '%Boleto%']);
        })->orderAsc('Receivables.data_vencimento');

        $accounts = $this->getAccounts();
        $receivables = $this->paginate($receivables);
        $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('receivables', 'accounts', 'payment_methods', 'cart', 'total'));

        // $http = new Client([
        //     'headers' => ['Authorization' => 'Bearer ' . $accessToken]
        // ]);
        // $response = $http->get('https://example.com/api/profile/1');
        //
    }

    public function edit()
    {
        $teste = 'teste';
        // dump($teste);
    }

    public function isAuthorized($user)
    {
        return true;
    }

    public function gerarBoleto()
    {
        function modulo_10($num)
        {
            $numtotal10 = 0;
            $fator = 2;
            // Separacao dos numeros
            for ($i = strlen($num); $i > 0; $i--) {
                // pega cada numero isoladamente
                $numeros[$i] = substr($num, $i - 1, 1);
                // Efetua multiplicacao do numero pelo (falor 10)
                // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
                $temp = $numeros[$i] * $fator;
                $temp0 = 0;
                foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                    $temp0 += $v;
                }
                $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
                // monta sequencia para soma dos digitos no (modulo 10)
                $numtotal10 += $parcial10[$i];
                if ($fator == 2) {
                    $fator = 1;
                } else {
                    $fator = 2; // intercala fator de multiplicacao (modulo 10)
                }
            }

            // várias linhas removidas, vide função original
            // Calculo do modulo 10
            $resto = $numtotal10 % 10;
            $digito = 10 - $resto;
            if ($resto == 0) {
                $digito = 0;
            }

            return $digito;
        }

        function formata_numero($numero, $loop, $insert, $tipo = "geral")
        {
            if ($tipo == "geral") {
                $numero = str_replace(",", "", $numero);
                while (strlen($numero) < $loop) {
                    $numero = $insert . $numero;
                }
            }
            if ($tipo == "valor") {
                /*	retira as virgulas	formata o numero    preenche com zeros		*/
                $numero = str_replace(",", "", $numero);
                while (strlen($numero) < $loop) {
                    $numero = $insert . $numero;
                }
            }
            if ($tipo == "convenio") {
                while (strlen($numero) < $loop) {
                    $numero = $numero . $insert;
                }
            }
            return $numero;
        }

        if ($this->Cookie->read('token')) {
            $this->loadModel('PayamentSlip');
            $id = $this->request->params['pass'][0];
            $data = TableRegistry::get('Receivables')->find('all')
                ->where(['Receivables.id' => $id])
                ->contain([
                    'SaleDetails',
                    'SaleDetails.Presales',
                    'SaleDetails.Presales.Clients'
                ]);

            foreach ($data as $datat) {
            }

            if ($this->request->is(['patch', 'post', 'put'])) {
                // $PayamentData = [];
                $daystosum = '3';
                $date = new Date();
                // $datesum = date('Y/m/d', strtotime($date . ' + ' . $daystosum . ' days'));

                if ($data) {
                    $PayamentData = $this->PayamentSlip->newEntity();
                    $datahoje = new Date();
                    // $PayamentData->tipo_ambiente = '1';

                    $PayamentData->receivable_id = $id;
                    $PayamentData->tipo_cobranca = '1';
                    // $valor_cobrado = number_format($valor_cobrado,'2','','');
                    // $valor_cobrado = str_replace(",", ".", $valor_cobrado);
                    $valor_format = number_format($datat->valor, 2, '.', '');
                    $valor_format2 = str_replace(".", "", $valor_format);
                    $valor_cobrado = (str_pad($valor_format2, 17, '0', STR_PAD_LEFT));
                    // dump($valor_cobrado);

                    $PayamentData->valor_cobrado = $valor_cobrado;
                    $PayamentData->tipo_produto = '000001';
                    $PayamentData->sub_produto = '000001';
                    $PayamentData->titulo_aceite = 'S';
                    //valor a preencher
                    // $PayamentData->nosso_numero = '55234515';
                    // $PayamentData->digito_verificador_nosso_numero = '0';

                    //carteira?
                    $PayamentData->tipo_carteira_titulo = '109';
                    $PayamentData->data_vencimento = $datat->data_vencimento;
                    $dias_de_prazo_para_pagamento = 5;
                    $datavencida = date("Y-m-d", time() + ($dias_de_prazo_para_pagamento * 86400));
                    $datan = date("Y-m-d");
                    strtotime($datan);
                    strtotime($datavencida);

                    // if ($datat->data_vencimento <= $datahoje) {
                    //     $PayamentData->data_vencimento = $datavencida;
                    // } else {
                    //     $PayamentData->data_vencimento = $datan;
                    // }

                    //valor a preencher especie -> recibo (05)
                    $PayamentData->especie = '05';
                    $PayamentData->data_emissao = $datat->created;
                    $PayamentData->tipo_pagamento = '1';
                    $PayamentData->indicador_pagamento_parcial = 'false';
                    $PayamentData->tipo_multa = '1';
                    $PayamentData->tipo_desconto = '1';
                    $PayamentData->tipo_autorizacao_recebimento = '1';

                    //dados a serem fixos depois.
                    $PayamentData->cpf_cnpj_beneficiario = '34530660000104';
                    $PayamentData->agencia_beneficiario = '6452';
                    $PayamentData->conta_beneficiario = '11978';
                    $PayamentData->dv_conta_beneficiario = '4';
                    $PayamentData->nome_pagador = $datat->sale_detail->presale->client->razao_social;
                    $cpf_cnpj_pagador = str_replace('/', '', $datat->sale_detail->presale->client->cnpj);
                    $cpf_cnpj_pagador = str_replace('-', '', $cpf_cnpj_pagador);
                    $cpf_cnpj_pagador = str_replace('.', '', $cpf_cnpj_pagador);
                    $cpf_cnpj_pagador_formatado = (str_pad($cpf_cnpj_pagador, 14, '0', STR_PAD_LEFT));

                    $PayamentData->cpf_cnpj_pagador = $cpf_cnpj_pagador_formatado;

                    $PayamentData->logradouro_pagador = $datat->sale_detail->presale->client->logradouro;
                    $PayamentData->bairro_pagador = $datat->sale_detail->presale->client->bairro;
                    $PayamentData->cidade_pagador = $datat->sale_detail->presale->client->cidade;
                    $PayamentData->uf_pagador = $datat->sale_detail->presale->client->estado;
                    $cep = str_replace('-', '',  $datat->sale_detail->presale->client->cep);
                    $PayamentData->cep_pagador = $cep;
                    $PayamentData->nota_fiscal = $datat->sale_detail->nota_fiscal;

                    // $PayamentData->tipo_produto = '1';
                    // $PayamentData->tipo_produto = '1';
                    // $PayamentData->tipo_produto = '1';
                    // $PayamentData->tipo_produto = '1';
                    //
                    // $result = $PayamentSlip->find('all')->last();
                    // $id = $this->PayamentSlips->id + 1;
                    // $numerocru = (str_pad($id, 8, '0', STR_PAD_LEFT));

                    $nnum = formata_numero($id, 8, 0);
                    $dadosboleto["agencia"] = "6452"; // Num da agencia, sem digito
                    $dadosboleto["conta"] = "11978"; // Num da conta, sem digito
                    //agencia é 4 digitos
                    $agencia = formata_numero($dadosboleto["agencia"], 4, 0);
                    //conta é 5 digitos + 1 do dv
                    $conta = formata_numero($dadosboleto["conta"], 5, 0);
                    // $conta_dv = formata_numero($dadosboleto["conta_dv"], 1, 0);
                    $dadosboleto["carteira"] = "109";
                    //carteira 175
                    $carteira = $dadosboleto["carteira"];
                    //nosso_numero no maximo 8 digitos
                    // $nosso_numero_f = modulo_10($agenscia . $conta . $carteira . $nnum);

                    $digitoverificadorns =  modulo_10($agencia . $conta . $carteira . $nnum);

                    $PayamentData->nosso_numero = $nnum;
                    $PayamentData->digito_verificador_nosso_numero = $digitoverificadorns;

                    if ($this->PayamentSlip->save($PayamentData)) {
                        $receivablesTable = TableRegistry::getTableLocator()->get('Receivables');
                        $receivable = $receivablesTable->get($id); // Return article with id 12
                        $receivable->boleto_gerado = 1;
                        $receivablesTable->save($receivable);

                        $this->emitirParaItau($PayamentData);
                    } else {
                        $this->Flash->error('Erro no salvamento do boleto');
                    }
                } else {
                    $this->Flash->error('Não há boleto para imprimir!');
                }
            }
        } else {
            $this->Flash->error('Token do Itau Expirou! Recriando o Token');
            $this->obterTokenItau();
        }
    }

    public function gerarTodosBoletos()
    {
        function modulo_10s($num)
        {
            $numtotal10 = 0;
            $fator = 2;
            // Separacao dos numeros
            for ($i = strlen($num); $i > 0; $i--) {
                // pega cada numero isoladamente
                $numeros[$i] = substr($num, $i - 1, 1);
                // Efetua multiplicacao do numero pelo (falor 10)
                // 2002-07-07 01:33:34 Macete para adequar ao Mod10 do Itaú
                $temp = $numeros[$i] * $fator;
                $temp0 = 0;
                foreach (preg_split('//', $temp, -1, PREG_SPLIT_NO_EMPTY) as $k => $v) {
                    $temp0 += $v;
                }
                $parcial10[$i] = $temp0; //$numeros[$i] * $fator;
                // monta sequencia para soma dos digitos no (modulo 10)
                $numtotal10 += $parcial10[$i];
                if ($fator == 2) {
                    $fator = 1;
                } else {
                    $fator = 2; // intercala fator de multiplicacao (modulo 10)
                }
            }

            // várias linhas removidas, vide função original
            // Calculo do modulo 10
            $resto = $numtotal10 % 10;
            $digito = 10 - $resto;
            if ($resto == 0) {
                $digito = 0;
            }

            return $digito;
        }

        function formata_numeros($numero, $loop, $insert, $tipo = "geral")
        {
            if ($tipo == "geral") {
                $numero = str_replace(",", "", $numero);
                while (strlen($numero) < $loop) {
                    $numero = $insert . $numero;
                }
            }
            if ($tipo == "valor") {
                /*	retira as virgulas		formata o numero		preenche com zeros		*/
                $numero = str_replace(",", "", $numero);
                while (strlen($numero) < $loop) {
                    $numero = $insert . $numero;
                }
            }
            if ($tipo == "convenio") {
                while (strlen($numero) < $loop) {
                    $numero = $numero . $insert;
                }
            }
            return $numero;
        }
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $this->loadModel('PayamentSlip');
        $data = TableRegistry::get('Receivables')->find('all')
            ->where([
                'Receivables.boleto_gerado' => 0,
                'Receivables.baixa' => 0
            ])->contain([
                'SaleDetails',
                'SaleDetails.Sales',
                'SaleDetails.Sales.Purchases',
                'SaleDetails.Sales.Purchases.Brokers',
                'SaleDetails.Sales.Purchases.Plants',
                'SaleDetails.Presales',
                'SaleDetails.Presales.Clients',
                'SaleDetails.Presales.Clients.Sellers'
            ])->matching('SaleDetails.Accounts', function ($q) {
                return $q->where(['titular LIKE' => '%Boleto%']);
            })->orderAsc('Receivables.data_vencimento');

        $accounts = $this->getAccounts();
        // $receivables = $this->paginate($data);

        // while ($this->receivables->next) {
        foreach ($data as $datat) {
            // $PayamentData = [];
            $daystosum = '3';
            $date = new Date();
            // $datesum = date('Y/m/d', strtotime($date . ' + ' . $daystosum . ' days'));

            if ($data) {
                $PayamentData = $this->PayamentSlip->newEntity();

                // $PayamentData->tipo_ambiente = '1';

                $PayamentData->receivable_id = $datat->id;
                $PayamentData->tipo_cobranca = '1';
                // $valor_cobrado = number_format($valor_cobrado,'2','','');
                // $valor_cobrado = str_replace(",", ".", $valor_cobrado);
                $valor_format = number_format($datat->valor, 2, '.', '');
                $valor_format2 = str_replace(".", "", $valor_format);
                $valor_cobrado = (str_pad($valor_format2, 17, '0', STR_PAD_LEFT));
                $PayamentData->valor_cobrado = $valor_cobrado;
                // dump($valor_cobrado);

                $PayamentData->tipo_produto = '000001';
                $PayamentData->sub_produto = '000001';
                $PayamentData->titulo_aceite = 'S';

                $nnum = formata_numeros($datat->id, 8, 0);
                $dadosboleto["agencia"] = "6452"; // Num da agencia, sem digito
                $dadosboleto["conta"] = "11978"; // Num da conta, sem digito
                //agencia é 4 digitos
                $agencia = formata_numeros($dadosboleto["agencia"], 4, 0);
                //conta é 5 digitos + 1 do dv
                $conta = formata_numeros($dadosboleto["conta"], 5, 0);
                // $conta_dv = formata_numero($dadosboleto["conta_dv"], 1, 0);
                $dadosboleto["carteira"] = "109";
                //carteira 175
                $carteira = $dadosboleto["carteira"];
                //nosso_numero no maximo 8 digitos

                // $nosso_numero_f = modulo_10($agenscia . $conta . $carteira . $nnum);

                $digitoverificadorns =  modulo_10s($agencia . $conta . $carteira . $nnum);

                $PayamentData->nosso_numero = $nnum;
                $PayamentData->digito_verificador_nosso_numero = $digitoverificadorns;

                $PayamentData->tipo_carteira_titulo = '1';
                $datahoje = new Date();
                $dias_de_prazo_para_pagamento = 5;
                $datavencida = date("Y-m-d", time() + ($dias_de_prazo_para_pagamento * 86400));
                $datan = date("Y-m-d");
                strtotime($datan);
                strtotime($datavencida);

                // $data_venc = date("d/m/Y", $datahoje);
                // $datan = strtotime($datat->data_vencimento);
                // $data_vencimentoorigin =   date("Y-m-d", $datat->data_vencimento);

                $PayamentData->data_vencimento = $datat->data_vencimento;

                $PayamentData->especie = '1';
                $PayamentData->data_emissao = $datat->created;
                $PayamentData->tipo_pagamento = '1';
                $PayamentData->indicador_pagamento_parcial = 'true';
                $PayamentData->tipo_multa = '1';
                $PayamentData->tipo_desconto = '1';
                $PayamentData->tipo_autorizacao_recebimento = '1';
                //dados a serem fixos depois.
                $PayamentData->cpf_cnpj_beneficiario = '999999999';
                $PayamentData->agencia_beneficiario = '6452';
                $PayamentData->conta_beneficiario = '11978';
                $PayamentData->digito_verificador_conta_beneficiario = '4';

                $PayamentData->nome_pagador = $datat->sale_detail->presale->client->razao_social;
                $cpf_cnpj_pagador = str_replace('/', '', $datat->sale_detail->presale->client->cnpj);
                $cpf_cnpj_pagador = str_replace('-', '', $cpf_cnpj_pagador);
                $cpf_cnpj_pagador = str_replace('.', '', $cpf_cnpj_pagador);
                $cpf_cnpj_pagador_formatado = (str_pad($cpf_cnpj_pagador, 14, '0', STR_PAD_LEFT));
                $PayamentData->cpf_cnpj_pagador = $cpf_cnpj_pagador_formatado;
                $PayamentData->logradouro_pagador = $datat->sale_detail->presale->client->logradouro;
                $PayamentData->bairro_pagador = $datat->sale_detail->presale->client->bairro;
                $PayamentData->cidade_pagador = $datat->sale_detail->presale->client->cidade;
                $PayamentData->uf_pagador = $datat->sale_detail->presale->client->estado;
                $cep = str_replace('-', '',  $datat->sale_detail->presale->client->cep);
                $PayamentData->cep_pagador = $cep;
                $PayamentData->nota_fiscal = $datat->sale_detail->nota_fiscal;
                // $PayamentData->tipo_produto = '1';
                // $PayamentData->tipo_produto = '1';
                // $PayamentData->tipo_produto = '1';
                // $PayamentData->tipo_produto = '1';

                if ($this->PayamentSlip->save($PayamentData)) {
                    $receivablesTable = TableRegistry::getTableLocator()->get('Receivables');
                    $receivable = $receivablesTable->get($datat->id);
                    $this->emitirParaItau($PayamentData);
                    $receivable->boleto_gerado = 1;
                    $receivablesTable->save($receivable);
                } else {
                    $this->Flash->error('Erro no salvamento do boleto');
                }
            }
            // $this->receivables->next;
            // }
        }

        $this->set(compact('receivables', 'accounts', 'payment_methods', 'cart', 'total'));

        // $http = new Client([
        //     'headers' => ['Authorization' => 'Bearer ' . $accessToken]
        // ]);
        // $response = $http->get('https://example.com/api/profile/1');
        //
    }
}
