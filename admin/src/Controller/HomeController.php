<?php

namespace App\Controller;

use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;

class HomeController extends AppController {

    public function isAuthorized($user) {
        return true;
    }

    public function index() {
        
    }

    public function cc() {
        Cache::delete('menus');
        $session = $this->request->getSession();
        $session->delete('Security.menus');
        $session->delete('Security.role');
        array_map('unlink', glob(getcwd() . '/../tmp/cache/models/*'));
        array_map('unlink', glob(getcwd() . '/../tmp/cache/persistent/*'));
        $this->redirect($this->referer());
    }

    public function test() {

    }

}
