<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LedgerEntriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Search.Prg', [
            'actions' => ['index'],
            'emptyValues' => []
        ]);
    }
    public function filter()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $query = $this->LedgerEntries->find('search', ['search' => $data])->contain([
            'Accounts',
            'Accounts.Distributors',
            'Accounts.Banks',
            'LedgerEntryTypes'
        ])->orderAsc('data_lancamento');
        $entries = $this->paginate($query);
        $accounts = $this->getAccounts();
        $types = TableRegistry::get('LedgerEntryTypes')->find()->toList();
        foreach ($types as $type) {
            $entry_types[$type->id] = '[' . $this->ledger_entry_types[$type->dc] . '] ' . $type->descricao;
        }
        $this->set(compact('entries', 'accounts', 'entry_types'));
    }

    public function index()
    {
        $data = $this->request->is('get') ? $this->request->query : $this->request->getData();
        $query = $this->LedgerEntries->find('search', ['search' => $data])->contain([
            'Accounts',
            'Accounts.Distributors',
            'Accounts.Banks',
            'LedgerEntryTypes'
        ])->orderAsc('data_lancamento');
        $entries = $this->paginate($query);
        $accounts = $this->getAccounts();
        $types = TableRegistry::get('LedgerEntryTypes')->find()->toList();
        foreach ($types as $type) {
            $entry_types[$type->id] = '[' . $this->ledger_entry_types[$type->dc] . '] ' . $type->descricao;
        }
        $this->set(compact('entries', 'accounts', 'entry_types'));
    }

    public function edit($id)
    {
        if ($id != 'new') {
            $entry = $this->LedgerEntries->get($id, ['contain' => []]);
        } else {
            $entry = $this->LedgerEntries->newEntity();
            $entry->dc = 'D';
        }
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $entry = $this->LedgerEntries->patchEntity($entry, $data);
            if ($this->LedgerEntries->save($entry)) {
                $this->Flash->success('Cadastro salvo com sucesso');
                $this->redirect(['action' => 'edit', $entry->id]);
            } else {
                $this->Flash->error('Erro no salvamento do cadastro');
            }
        }
        $accounts = $this->getAccounts();
        $types = TableRegistry::get('LedgerEntryTypes')->find()->toList();
        foreach ($types as $type) {
            $entry_types[$type->id] = '[' . $this->ledger_entry_types[$type->dc] . '] ' . $type->descricao;
        }
        $payment_methods = TableRegistry::get('PaymentMethods')->find('list');
        $this->set(compact('entry', 'accounts', 'entry_types', 'payment_methods'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        if (TableRegistry::get('Transfers')->find()->where(['OR' => ['source_ledger_entry_id' => $id, 'destination_ledger_entry_id' => $id]])->count() == 0) {
            $entry = $this->LedgerEntries->get($id);
            if ($this->LedgerEntries->delete($entry)) {
                $this->Flash->success('Cadastro removido com sucesso');
            } else {
                $this->Flash->error('Erro na remoção do cadastro');
            }
        } else {
            $this->Flash->error('Efetue a remoção pela tela de transferências', ['params' => ['title' => 'Registro não pode ser removido por ser uma transferência']]);
        }
        return $this->redirect(['action' => 'index']);
    }
}
